@echo off 
goto START

1.	Edit "SDK_DIR" with path of MSG tool into SDK. Check if all files( *.c and *.h) have the same name used into messages folder. Add or replace this file into "messages" folder.
     
2.	Into INGEDEV: 
     a.	Go to properties of current project, and follow the next path: Telium Plus > Build Configurations > Build Steps
     b.	Add the next line into Pre-Build-Steps Command: ./messages/CompileMessages.bat
     c.	If you want, Add a little description of tool.

3.  IMPORTANT: By using this file, the messages always will be compiled and will be generated the *.c and *h files, but the copy into the project depend of changes into the spanish file, because is the default language.

:START


REM Set current absolute path
@cd /D %~dp0

REM Delete old files
@echo Delete temporal files
del CoreMessagesDef.c
del CoreMessageDefinitions.h

REM 1- convert h file to MSG file, default language
"C:\Program Files\TeliumSDK\SDK11.20.3.PatchD\Tools\FW_TPLUS\MSG tool\BuildMSG" -c -tmsgApp_es.h -hCoreMessageDefinitions.h DEFAULT.MSG

REM 2- create English.c file to include in the sample
"C:\Program Files\TeliumSDK\SDK11.20.3.PatchD\Tools\FW_TPLUS\MSG tool\MSG2C" DEFAULT.MSG CoreMessagesDef.c

REM 4- convert h files to MSG file
"C:\Program Files\TeliumSDK\SDK11.20.3.PatchD\Tools\FW_TPLUS\MSG tool\BuildMSG" -c -tmsgApp_en.h -hCoreMessageDefinitions.h MESSAGES.MSG
"C:\Program Files\TeliumSDK\SDK11.20.3.PatchD\Tools\FW_TPLUS\MSG tool\BuildMSG" -tmsgApp_es.h -hCoreMessageDefinitions.h MESSAGES.MSG
REM 5- copy the "MessagesDefinitions.h" file to the Inc directory of your project

REM 4- Check if file "CoreMessagesDef.c" or "CoreMessageDefinitions.h" has any modification
fc /b CoreMessagesDef.c ..\Src\CoreMessagesDef.c >NUL 
    if not errorlevel 1 fc /b CoreMessageDefinitions.h ..\Inc\CoreMessageDefinitions.h >NUL 
        if not errorlevel 1 goto EQUAL
                 
echo Modified!!

REM 3- Copy the C file to project
copy /y CoreMessagesDef.c ..\Src
REM 3- Copy the H file to project
copy /y CoreMessageDefinitions.h ..\Inc

goto END

:EQUAL
@echo Without Modifications!!

:END