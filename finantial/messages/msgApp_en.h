// #LANGUAGE EN
// #FONFILE /LOCAL/MANAGER.SGN
// #MSGCODING ISO8859
// #FILETYPE 1
// #APPLITYPE 6e

/* el tag "#APPLITYPE" debe tener el mismo application type que la aplicaci�n que lo usara */
/* Debe tener almenos 1 espacio(no tab) de separacion entre el mensaje y la definicion*/

/* ------------------------------------------------------------------------ */
/* Transactions messages                                                    */
/* ------------------------------------------------------------------------ */
#message 1000 MSG_APPNAME 							"iPOS-App"
#message 1001 MSG_CLIENT_NAME 						"INGENICO CARCAAN"
#message 1002 MSG_TABLE_MANAGER 					"TABLE MANAGER"
#message 1003 MSG_GENERAL_CONF 						"CONFIGURATION"
#message 1004 MSG_PABX 								"PABX"
#message 1005 MSG_IPARAM 							"INGELOAD"
#message 1006 MSG_EMPARAM	 						"ESTATE MANAGER"
#message 1007 MSG_CONFIG_ETH 						"ETHERNET/IP"
#message 1008 MSG_GPRS_APN 							"APN ADDRESS"
#message 1009 MSG_GPRS_USR 							"PPP USER NAME"
#message 1010 MSG_GPRS_PASS 						"PPP PASSWORD"
#message 1011 MSG_TIMEOUT 							"TERMINAL TIMEOUT"
#message 1012 MSG_TERMINALID 						"TERMINAL ID"
#message 1013 MSG_COMM_TYPE 						"COMMUNICATION TYPE"
#message 1014 MSG_DIAL 								"DIAL"
#message 1015 MSG_ETH 								"ETHERNET"
#message 1016 MSG_GPRS 								"GPRS/3G"
#message 1017 MSG_NII 								"NII"
#message 1018 MSG_RETRIES 							"CONNECT TRIES"
#message 1019 MSG_CONNECT_TO 						"CONNECT TIMEOUT"
#message 1020 MSG_RESPONSE_TO 						"RESPONSE TIMEOUT"
#message 1021 MSG_PRIMARY_PHONE 					"PRIMARY PHONE NUMBER"
#message 1022 MSG_SECUNDARY_PHONE 					"SECUNDARY PHONE NUMBER"
#message 1023 MSG_HOST_IP 							"HOST IP"
#message 1024 MSG_HOST_PORT 						"HOST PORT"
#message 1025 MSG_ENABLE 							"ENABLE"
#message 1026 MSG_DISABLE 							"DISABLE"
#message 1027 MSG_SSL 								"SSL"
#message 1028 MSG_REPORT 							"REPORT"
#message 1029 MSG_SAVE_CHANGES 						"SAVE CHANGES?"
#message 1030 MSG_YESNO 							"YES/NO"
#message 1031 MSG_LOADINGTABLES 					"LOADING TABLES"
#message 1032 MSG_INIT 								"INITIALAZING"
#message 1033 MSG_PLEASE_WAIT 						"PLEASE WAIT"
#message 1034 MSG_CONNECTING 						"CONNECTING..."
#message 1035 MSG_TRY_AGAIN 						"TRY AGAING"
#message 1036 MSG_INIT_OK 							"INIT COMPLETED OK"
#message 1037 MSG_PACKAGE 							"PACKAGE"
#message 1038 MSG_TECH_PASSWORD 					"TECHNICIAN PASSWORD"
#message 1039 MSG_PLEASE_INIT 						"PLEASE INITIALIZE"
#message 1040 MSG_MERCH_GROUP 						"STORE"
#message 1041 MSG_TRANSACTION 						"TRANSACTION"
#message 1042 MSG_NO_ALLOWED 						"NOT ALLOWED"
#message 1043 MSG_SWIPE_INSERT 						"INSERT/SWIPE"
#message 1044 MSG_CARD 								"CARD"
#message 1045 MSG_MANUAL_ENTRY 						"ENTER CARD NUMBER"
#message 1046 MSG_INSERT_CARD 						"INSERT"
#message 1047 MSG_EXPIRATION_DATE 					"EXPIRATION DATE"
#message 1048 MSG_MMYY 								"MM/YY"
#message 1049 MSG_MERCHANT 							"MERCHANT"
#message 1050 MSG_CURRENCY 							"CURRENCY"
#message 1051 MSG_LOCAL 							"LOCAL"
#message 1052 MSG_DOLAR 							"DOLAR"
#message 1053 MSG_CVV2 								"SECURITY CODE CVV2"
#message 1054 MSG_NO_PRESENT 						"NO PRESENT"
#message 1055 MSG_PRESENT 							"PRESENT"
#message 1056 MSG_UNREADABLE 						"UNREADABLE"
#message 1057 MSG_NO_PROVIDED 						"NOT PROVIDED"
#message 1058 MSG_LAST_4 							"LAST 4 OF PAN"
#message 1059 MSG_ENTER_AMOUNT 						"ENTER AMOUNT"
#message 1060 MSG_ENTER_TAX 						"ENTER TAX"
#message 1061 MSG_ENTER_TIP 						"ENTER TIP"
#message 1062 MSG_ENTER_AUTHCODE 					"ENTER_AUTH_CODE"
#message 1063 MSG_INVOICE 							"INVOICE"
#message 1064 MSG_AMOUNT 							"AMOUNT"
#message 1065 MSG_TAX 								"TAX"
#message 1066 MSG_TIP 								"TIP"
#message 1067 MSG_TOTAL 							"TOTAL"
#message 1068 MSG_CONFIRMATION 						"CORRECT? YES/NO"
#message 1069 MSG_CLIENT_RECEIPT 					"COSTUMER RECEIPT? YES/NO"
#message 1070 MSG_SEARCHING_TRAN 					"SEARCHING TRANSACTION..."
#message 1071 MSG_MERCHANT_PASSWORD 				"MERCHANT PASSWORD"
#message 1072 MSG_VOID_COFIRMATION 					"VOID? YES/NO"
#message 1073 MSG_NEW_AMOUNT 						"NEW AMOUNT"
#message 1074 MSG_NEW_TIP 							"NEW TIP"
#message 1075 MSG_NEW_TAX 							"NEW TAX"
#message 1076 MSG_TRANSACTION_ADJUSTED 				"TRANSACTION ADJUSTED"
#message 1077 MSG_PRINT_AUTH_CODE 					"AUTH:"
#message 1078 MSG_PRINT_RRN 						"RRN:"
#message 1079 MSG_PIRNT_SIGNATURE 					"SIGNATURE:________________________________"
#message 1080 MSG_DISCLAIMER_LINE1 					"I AGREE TO CARD EMISOR TO PAY"
#message 1081 MSG_DISCLAIMER_LINE2 					"THE TRANSACION AMOUNT REGARD CONTRACT"
#message 1082 MSG_DISCLAIMER_LINE3 					"OF USE OF THIS CARD"
#message 1083 MSG_TIP_GUIDE 						"** TIP GUIDE **"
#message 1084 MSG_CLIENT_COPY 						"CLIENT COPY"
#message 1085 MSG_MERCHANT_COPY 					"MERCHANT COPY"
#message 1086 MSG_DUPLICATE 						"** DUPLICATE **"
#message 1087 MSG_PRINT_VOID 						"** VOID **"
#message 1088 MSG_BATCH 							"BATCH NUMBER"
#message 1089 MSG_TRANSACTION_LIST 					"TRANSACTION LIST"
#message 1090 MSG_TRAN_LIST_HEADER 					"DATE  HOUR  T INV.   CARD            TOTAL"
#message 1091 MSG_REPORT_SALES 						"SALES          "
#message 1092 MSG_REPORT_REFUNDS 					"REFUNDS        "
#message 1093 MSG_BASE 								"BASE"
#message 1094 MSG_CARD_TOTALS 					"CARD TOTALS"
#message 1095 MSG_GENERAL_TOTALS 					"GENERAL TOTALS"
#message 1096 MSG_REPRINT 							"REPRINT"
#message 1097 MSG_SETTLE 							"SETTLE"
#message 1098 MSG_TOTALS_REPORT 					"TOTALS REPORT"
#message 1099 MSG_DETAIL_REPORT 					"DETAIL REPORT"
#message 1100 MSG_MERCHANT_MENU 					"MERCHANT MENU"
#message 1101 MSG_PRINTING 							"PRINTING..."
#message 1102 MSG_EMPTY_BATCH 						"EMPTY BATCH"
#message 1103 MSG_ALL 								"ALL"
#message 1104 MSG_TERMINAL_PASSWORD 				"TERMINAL PASSWORD"
#message 1105 MSG_SWIPE_CARD 						"SWIPE CARD"
#message 1106 MSG_DELETE_BATCH 						"DELETE BATCH"
#message 1107 MSG_BATCH_DELETED 					"BATCH DELETED"
#message 1108 MSG_CONF_DIAL "DIAL CONFIGURATION"
#message 1109 MSG_DETECT_TONE "DETECT TONE"
#message 1110 MSG_DETECT_BUSY "DETECT BUSSY LINE"
#message 1111 MSG_MODE_ASYN "ASYN"
#message 1112 MSG_MODE_FASTCONNECT "FAST CONNECT"
#message 1113 MSG_MODE_V22V22BIS "V22 V22BIS"
#message 1114 MSG_BLIND_DAIL_PAUSE "BLIND DIAL PAUSE"
#message 1115 MSG_TONE_TO "WAIT DIAL TONE TO"
#message 1116 MSG_PABX_PAUSE "PABX PAUSE"
#message 1117 MSG_MODEM_MODE "MODEM MODE"
#message 1118 MSG_CONF_GPRS "CONF 3G/GPRS"
#message 1129 MSG_CONFIGHARDWARE 					"CONF. HARDWARE"

#message 2001 ERR_LOAD_TABLE_MAMAGER 				"CANNOT LOAD TM"
#message 2002 MSG_ERROR_INGELOAD 					"ERROR INGELAOD"
#message 2003 MSG_ERROR_INGELOAD2 					"SETUP PARAMETERS"
#message 2004 MSG_ERROR_INGELAOD_NO_TERMINAL 		"TERMINAL DOESNT EXISTS"
#message 2005 MSG_ERROR_INIT 						"INIT ERROR"
#message 2006 MSG_ERROR_COMMUNICATION 				"COMMUNICATION ERROR"
#message 2007 MSG_ERROR_BAD_PASSWORD 				"INCORRECT PASSWORD"
#message 2008 MSG_ERROR_TERMINAL_NO_CONF 			"TERMINAL NOT CONFIGURED"
#message 2009 MSG_ERROR_NO_MANUAL_ENTRY 			"MANUAL ENTRY NO ALLOWED"
#message 2010 MSG_ERROR_CARD_READ 					"CARD READER ERROR"
#message 2011 MSG_ERROR_TRAN 						"TRANSACTION ERROR"
#message 2012 MSG_ERROR_NO_SUPPORTED_CARD 			"NO SUPPORTED CARD"
#message 2013 MSG_ERROR_EXPIRED_CARD 				"EXPIRED CARD"
#message 2014 MSG_ERROR_LAST_4 						"ERROR LAST 4 OF PAN"
#message 2015 MSG_ERROR_BIG_AMOUNT 					"AMOUNT NOT PERMITED"
#message 2016 MSG_ERROR_BIG_TAX 					"TAX AMOUNT NO PERMITED"
#message 2017 MSG_ERROR_BIG_TIP 					"TIP AMOUNT NO PERMITED"
#message 2018 MSG_ERROR_NO_VOID 					"VOID NOT ALLOWED"
#message 2019 MSG_ERROR_NO_TRAN_BATCH 				"NO TRANSACTION IN BATCH"
#message 2020 MSG_ERROR_ADJUST_NOT_ALLOWED 			"ADJUST NOT ALLOWED"
#message 2021 MSG_ERROR_ADJUST_GREATER 				"ADJUST GREATER"
#message 2022 MSG_ERROR_SETTLE 						"SETTLE ERROR"
#message 2023 MSG_ERROR_TRANSACTION 				"TRANSACTION ERROR"
#message 2024 MSG_ERROR_DELETE_BATCH 				"CANT DELETE BATCH"

#message 3001 MSG_TRAN_SALE 						"SALE"
#message 3002 MSG_TRAN_REFUND 						"REFUND"
#message 3003 MSG_TRAN_VOID 						"VOID"
#message 3004 MSG_TRAN_ADJUST 						"ADJUST"
#message 3005 MSG_TRAN_REVERSE 						"REVERSE"
#message 3006 MSG_TRAN_TEST 						"ECHO TEST"
#message 3007 MSG_TRAN_SETTLE 						"SETTLE"
#message 3008 MSG_TRAN_QUICK_SALE 					"QUICK SALE"

#message 4001 MSG_RC_APPROVED 						"APPROVED"
#message 4002 MSG_RC_DECLINED 						"DECLINED"
#message 4003 MSG_RC_PLEASE_CALL 					"PLEASE CALL"
#message 4004 MSG_RC_INVALID_MERCHANT 				"INVALID MERCHANT"
#message 4005 MSG_RC_HOLD_CARD 						"HOLD CARD"
#message 4006 MSG_RC_INVALID_TRANSACTION 			"INVALID TRANSACTION"
#message 4007 MSG_RC_INVALID_AMOUNT 				"INVALID AMOUNT"
#message 4008 MSG_RC_INVALID_CARD 					"INVALID CARD"
#message 4009 MSG_RC_RETRY_TRANSACTION 				"RETRY TRANSACTION"
#message 4010 MSG_RC_EXPIRED_CARD 					"EXPIRED CARD"
#message 4011 MSG_RC_TRANSACTION_NOT_ALLOWED 		"TRANSACTION NOT ALLOWED"
#message 4012 MSG_RC_INVALID_TERMINAL 				"INVALID TERMINAL"
#message 4013 MSG_RC_NO_ISSUER 						"INOPERATIVE ISSUER"
#message 4014 MSG_RC_DUPLICATE_TRANSMITION 			"DUPLICATE TRANSMITION"
#message 4015 MSG_RC_BATCH_UPLOAD 					"BATCH UPLOAD"
#message 4016 MSG_RC_SYSTEM_FAILURE 				"SYSTEM FAILURE"
#message 4017 MSG_RC_COMMUNICATION_ERROR 			"COMMUNICAITON ERROR"
#message 4018 MSG_RC_TIME_OUT 						"TIME OUT - RETRY"
#message 4019 MSG_RC_PACK_ERROR 					"ERROR PACK MESSAGE"
#message 4020 MSG_RC_UNPACK_ERROR 					"ERROR UNPACK MESSAGE"
#message 4021 MSG_RC_EMV_DECLINED 					"DECLINED BY CARD"
#message 4022 MSG_RC_EMV_CANCELED 					"EMV TRAN - CANCELED"
#message 4023 MSG_RC_EMV_CARD_ERROR 				"EMV TRAN - CARD ERROR"
#message 4024 MSG_RC_EMV_PROCESSING_ERROR 			"EMV TRAN - PROCESS ERROR"
#message 5070 MSG_PCL                               "PCL"

#message 9000 MSG_TRACE_CONFIG_TITLE 				"TRACE_IP CONFIG"
#message 9001 MSG_DEBUG_STEPS_TITLE 				"DEBUG STEPS"
#message 9002 MSG_TRACE_UPDATE_TITLE 				"Update"
#message 9003 MSG_TRACE_UPDATE_L1 				"The terminal will"
#message 9004 MSG_TRACE_UPDATE_L2 				"reboot"

#message 10005 MSG_PAR_WO_MODIF_L1                  "PARAMETERS WITHOUT"
#message 10006 MSG_PAR_WO_MODIF_L2                  "CHANGES"
#message 10007 MSG_UNKNOW_ERROR                     "UNKOWN ERROR"
#message 10008 MSG_PAR_PROCESSING_ERR_L1            "PROCESSING ERROR"
#message 10009 MSG_PAR_PROCESSING_ERR_L2            "PACKAGE"
#message 10010 MSG_INGESTATE_OK                     "DOWNLOAD SUCCESSFUL"
#message 10011 MSG_INGESTATE_OFFLINE                "INITIALIZE OFFLINE?"
#message 10012 MSG_ERROR_INGESTATE                  "DOWNLOAD ERROR"
#message 10013 MSG_ERROR_EM_LINK_L1                  "CONNECT TERMINAL"
#message 10014 MSG_ERROR_EM_LINK_L2                  "TO THE NETWORK!"
#message 10015 MSG_ERROR_EM_COMM                 "COMUNICATION ERROR"
#message 10016 MSG_SSL_PROFILE                		"SSL PROFILE"
#message 10017 MSG_SSL_TLSv1_2                		"TLS V1.2"
#message 10018 MSG_SSL_TLSv1_1                		"TLS V1.1"
#message 10019 MSG_SSL_TLSv1                		"TLS V1.0"
#message 10020 MSG_SSL_SSLv3               			"SSL V3"
#message 10021 MSG_SSL_SSLv2               			"SSL V2"
#message 10022 MSG_SSL_SSLv23               		"SSL V2 or V3"
#message 10023 MSG_ERROR_EM_NO_INST               	"NO INSTALLATION DONE"
#message 10024 MSG_ERROR_EM_BAD_ACT               	"UNKNOWN ACTIVITY"
#message 10025 MSG_ERROR_EM_ARG_MISS               	"ARGUMENT MISSING"
#message 10026 MSG_ERROR_EM_ARG_INV               	"INVALID ARGUMENT"
#message 10027 MSG_ERROR_EM_PPP_CONFIG_FAIL        	"PPP CONFIGURATION FAILED"
#message 10028 MSG_ERROR_EM_CON               		"CONNECTION ERROR"
#message 10029 MSG_ERROR_EM_CON_LOST               	"CONNECTION LOST"
#message 10030 MSG_SSL_PROFILE_L2                	"CASE-SENSITIVE"
#message 10031 MSG_SSL_ERROR_CONF               	"ERROR SAVING THE TMS CONFIGURATION"

#message 10100 MSG_PPP_USR                          "PPP USER"
#message 10101 MSG_PPP_PASS                         "PPP PASSWORD"

#message 10200 MSG_ENTER_ID_NUMBER                  "ENTER ID NUMBER"
#message 10201 MSG_MAIN_ACCOUNT                     "MAIN"
#message 10202 MSG_SAVING_ACCOUNT                   "SAVINGS"
#message 10203 MSG_CURRENT_ACCOUNT                  "CHECKING"
#message 10204 MSG_ACCOUNT_TYPE                     "ACCOUNT TYPE"
#message 10205 MSG_NUM_REFERENCE                    "REFERENCE NUMBER"
#message 10206 MSG_INVALID_CARD                     "INVALID CARD"
#message 10207 MSG_SERVER                           "SERVER NUMBER"
