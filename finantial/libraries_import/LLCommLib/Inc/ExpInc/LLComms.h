/*****************************************************************************
 Property of INGENICO
 *****************************************************************************/
/*
 * PROJECT  : CarCAAn Base Application
 * MODULE   : LL Communications
 * FILEMANE : LL_Comms.c
 * PURPOSE  : Contains functions to perform communications using Dial, Eht
 *            and Gprs connections
 *
 * HISTORY
 *
 * date 		author 		modifications
 * 2012-02-22	@mdm		Creation
 *
 *****************************************************************************/

#if !defined( LLCOMMS_H )
#define LLCOMMS_H

/** Library version that this header file refers to */
#define LLCOMMS_VERSION  "02.02"

#include "CoreDefines.h"
#include "SSL_.h"

/*---******* DEFINES *****************************************************---*/
// LLC Connection Type
typedef enum LLC_ConnectionTypes
{
	LLC_DISABLE = 0,
	LLC_DIAL,
	LLC_IP,
	LLC_GPRS,
	LLC_WIFI,
	LLC_PCL,
	LLC_SERIAL,
	LLC_SSL_SERVER,

}LLC_ConnectionTypes;


// LLC
#define LLC_CNX_PRIM		1
#define LLC_CNX_SEC			2

// LLC Returns
#define	LLC_RET_OK					0
#define LLC_RET_CONNECTED			1
#define LLC_RET_HANGUP				2

// LLC Errors
#define	LLC_ERR_NOIPS				1	// No IPs to configure
#define	LLC_ERR_CNXCONFIG			2	// Error to perform the configuration of LL Connection
#define LLC_ERR_NETWORK				3	// Network error
#define LLC_ERR_NOTCONNECTED		4
#define LLC_ERR_SEND				5
#define LLC_ERR_RECEIVE				6
#define LLC_ERR_NO_PROFILE			7

// LLC_STATUS
#define LLC_STATUS_CONNECTING		1
#define LLC_STATUS_CONNECTED		2
#define LLC_STATUS_ERROR			3
#define LLC_STATUS_HANGUP			4
#define LLC_STATUS_NOINIT			5

#define LLC_ENABLE					1
#define LLC_DISABLE					0

#define LLC_MODEM_MODE_ASYNC		1
#define LLC_MODEM_MODE_FASTCONNECT 	2
#define LLC_MODEM_MODE_V22_V22BIS	3

#define LLC_TONE					1
#define LLC_PULSE					2

#define LLC_V22_1200				1
#define LLC_V22BIS_2400				2
#define LLC_V32_9600				3


typedef struct TABLE_HOST_CONNECTION
{
	// general settings
	uint8		CommType;		// communication type (LLC_DISABLE | LLC_DIAL |  LLC_IP | LLC_GPRS | LLC_WIFI)
	uint8		Retries;		// retries number to connect to host
	uint8		TimeoutConn;	// timeout to connect to host
	uint8		TimeoutAns;		// timeout to receive response from host

	// dial settings
	char		Phone1[25];		// phone number 1
	char		Phone2[25];		// phone number 2

	// Serial settings(COM/USB)
	uint8 		SerialPort;		//Serial port
								//USB -[LL_PHYSICAL_V_USB, LL_PHYSICAL_V_USB_HOST, LL_PHYSICAL_V_USB_BASE, LL_PHYSICAL_V_USB_BASE_HOST_1, LL_PHYSICAL_V_USB_BASE_HOST_2, LL_PHYSICAL_V_USB_HOST_CL, LL_PHYSICAL_V_USB_HOST_CL, LL_PHYSICAL_V_USB_HOST_PP]
								//COM -[LL_PHYSICAL_V_COM0, LL_PHYSICAL_V_COM1, LL_PHYSICAL_V_COM2, LL_PHYSICAL_V_COM3, LL_PHYSICAL_V_COM10, LL_PHYSICAL_V_COM11]

	uint32 		SerialBaud;		//Baud rate -[LL_PHYSICAL_V_BAUDRATE_300, LL_PHYSICAL_V_BAUDRATE_1200, LL_PHYSICAL_V_BAUDRATE_2400...LL_PHYSICAL_V_BAUDRATE_115200]
	uint8 		SerialDBits;	//Data bits -[LL_PHYSICAL_V_7_BITS, LL_PHYSICAL_V_8_BITS]
	uint8 		SerialStopB;	//Stop bits -[LL_PHYSICAL_V_1_STOP, LL_PHYSICAL_V_2_STOP]
	uint8 		SerialParity;	//Parity	-[LL_PHYSICAL_V_NO_PARITY, LL_PHYSICAL_V_EVEN_PARITY, LL_PHYSICAL_V_ODD_PARITY]

	// IP1 settings
	char		Address1[200];	// Address 1 (IP or URL)
	char		Port1[6];		// 	 port number
	uint8		bSSL1;			// 	 flag to enable/disable SSL (0 = disable, 1 = enable)
	char		SSLProf1[15];	// 	 SSL profile name
	uint8		SSLVersion1;	// 	 SSL version (SSLv2, SSLv3, TLSv1, SSLv23, TLSv1_1, TLSv1_2)
								//     Default = 0 (SSLv3)
	long int	SSLCipher1;		//	 Bitmap for Mask of supported cipher
								//     (SSL_RSA, SSL_DSS, SSL_DES, SSL_3DES, SSL_RC4, SSL_RC2, SSL_AES, SSL_MD5, SSL_SHA1, SSL_SHA256, SSL_SHA384)
								//     Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)
	long int 	SSLExportMask1;	//   Bitmap for mask between export mode and the cipher strength
								//     Export information (SSL_NOT_EXP, SSL_EXPORT)
								//     Cipher strength information (SSL_STRONG_NONE, SSL_EXP40, SSL_MICRO, SSL_EXP56, SSL_MINI, SSL_LOW, SSL_MEDIUM, SSL_HIGH)
								//     Default = 0 (SSL_HIGH | SSL_NOT_EXP)

	// IP2 settings
	char		Address2[200];	// Address 2 (IP or URL)
	char		Port2[6];		// 	 port number
	uint8		bSSL2;			// 	 flag to enable/disable SSL (0 = disable, 1 = enable)
	char		SSLProf2[15];	// 	 SSL profile name
	uint8		SSLVersion2;	// 	 SSL version (SSLv2, SSLv3, TLSv1, SSLv23, TLSv1_1, TLSv1_2)
								//     Default = 0 (SSLv3)
	long int	SSLCipher2;		//	 Bitmap for Mask of supported cipher
								//     (SSL_RSA, SSL_DSS, SSL_DES, SSL_3DES, SSL_RC4, SSL_RC2, SSL_AES, SSL_MD5, SSL_SHA1, SSL_SHA256, SSL_SHA384)
								//     Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)
	long int 	SSLExportMask2;	//   Bitmap for mask between export mode and the cipher strength
								//     Export information (SSL_NOT_EXP, SSL_EXPORT)
								//     Cipher strength information (SSL_STRONG_NONE, SSL_EXP40, SSL_MICRO, SSL_EXP56, SSL_MINI, SSL_LOW, SSL_MEDIUM, SSL_HIGH)
								//     Default = 0 (SSL_HIGH | SSL_NOT_EXP)

	// general IP settings
	uint8		bSSLUseSignedProfile;// 1: Use signed profile(Store on SWAP disk like *.PGN). RECOMMENDED!!
									 // 0: Use unsigned profile(Store on HOST disk like *.PEM)
	uint8		bAddHexLength;	// flag to add hex length
	uint8		bAddAsciiLength;// flag to add ASCII length(Only works if bAddHexLength == 0!!!!!)

	// SSL Server mode
	uint8 		SSLServMustVerify;
	uint8 		SSLServUseCache;
	char		SSLServProf[15];	// 	 SSL profile name
	char		SSLServPort[6];		// 	 port number

}HOST_CONNECT;

typedef struct TABLE_CONFIG
{
	// host connections
	HOST_CONNECT First_Connection;		// configuration of fist connection
	HOST_CONNECT Second_Connection;		// configuration of second connection

	// dial settings
	uint8	Dial_Tone;					// Dial tone [1=Tone, 2=Pulse]
	uint8	Dial_bDetectTone;			// Detect dial tone [0=Disable - 1=Enable] (0=Blind Dial)
	uint8	Dial_bDetectBussy;			// Detect if line is busy [0=Disable - 1=Enable]
	uint8	Dial_ModemMode;				// Use FastConnect [LLC_MODEM_MODE_ASYNC, LLC_MODEM_MODE_FASTCONNECT, LLC_MODEM_MODE_V22_V22BIS]
	uint8	Dial_BlindDialPause;		// Pause before dial when blind dial is enable
	uint8	Dial_DialToneTO;			// Pause before dial tone detection
	char	Dial_PABX[10];				// PABX Code
	uint8	Dial_PABX_Pause;			// PABX pause in seconds
	uint8	Dial_Modulation;			// Line modulation [ LLC_V22_1200 | LLC_V22BIS_2400 | LLC_V32_9600]

	// GPRS/3G settings
	char 	GPRS_APN[100];				// GPRS/3G apn
	char 	GPRS_Usr[25];				// user for GPRS/3G PPP
	char	GPRS_Pass[25];				// password for GPRS/3G PPP

	// Custom messages
	char	TXT_Dial1[25];				// message to show when is dialing first number
	char	TXT_Dial2[25];				// message to show when is dialing second number
	char	TXT_IP1[25];				// message to show when is connecting first ip via ETH
	char	TXT_IP2[25];				// message to show when is connecting second ip via ETH
	char	TXT_Serial1[25];			// message to show when is connecting first serial COM
	char	TXT_Serial2[25];			// message to show when is connecting second serial COM
	char	TXT_SSLServ1[25];			// message to show when is connecting first SSL Server
	char	TXT_SSLServ2[25];			// message to show when is connecting second SSL Server
	char	TXT_GPRS1[25];				// message to show when is connecting first ip via GPRS/3G
	char	TXT_GPRS2[25];				// message to show when is connecting second ip via GPRS/3G
	char	TXT_Send[25];				// message to show when is sending data to host
	char	TXT_Receive[25];			// message to show when is receiving data from host
	char	TXT_DisconnectGPRS[25];		// message to show when is disconnecting from GPRS network

	uint8 	bHangUp;					// hangup flag
	LL_HANDLE hLLHandle;				// Linklayer handle to be set to the current config
	uint8 	nCommType;					// current communication type being used
	HOST_CONNECT *CurrenConfig;         // pointer to the current configuration being used
	uint8 	nRetries;					// retry counter
	int16 	nLastError;					// last comm error status

	SSL_HANDLE hSslHandle;         		// pointer to the current configuration SSL server

} LLCOMS_CONFIG;

/*---******* PUBLIC FUNCTIONS  *******************************************---*/
/*****************************************************************************
 * Func Name:		LLC_StartConnection
 * Description: 	Initialize LLC Thead
 * Parameters:
 *   - LLCOMS_CONFIG *Confing pointer to configuration struct
 * Returns:
 *   - RET_OK		The operation has been successfully processed
 *   - RET_NOK		Error in operation, for detail see nLastError
 **************************************************************************--*/
int16 LLC_StartConnection(LLCOMS_CONFIG *stpConfig);


/*****************************************************************************
 * Func Name:		LLC_WaitConnection
 * Description: 	With for Connection, TIME OUT or ERROR
 * Parameters:
 *   - bool - flag to enable show status messages
 * Returns:
 *   - LLC_STATUS_CONNECTED		Connected to host
 *   - LLC_STATUS_ERROR			Error in connection, , for detail see nLastError
 *   - LLC_STATUS_HANGUP		HangUP is setted
 **************************************************************************--*/
int16 LLC_WaitConnection(LLCOMS_CONFIG *stpConfig, bool ShowMessages);


/*****************************************************************************
 * Func Name:		LLC_EndConnection
 * Description: 	HangUp connection
 * Parameters:
 *   - none
 * Returns:
 *   - LLC_RET_OK	HangUp Ok
 **************************************************************************--*/
int16 LLC_EndConnection(LLCOMS_CONFIG *stpConfig);

/*****************************************************************************
 * Func Name:		LLC_Send
 * Description: 	Send message to host
 * Parameters:
 *   - TXBuffer		Buffer to be send
 *   - TXSize		Size of TXBuffer
 * Returns:
 *   - LLC_RET_OK	Send Ok
 *   - LLC_ERR_SEND Error in send buffer, for detail see nLastError
 **************************************************************************--*/
int16 LLC_Send(LLCOMS_CONFIG *stpConfig, uint8 * TXBuffer,  int TXSize);


/*****************************************************************************
 * Func Name:		LLC_Send
 * Description: 	Send message to host
 * Parameters:
 * 	 - LLCOMS_CONFIG *stpConfig: Pointer to communication configuration
 *   - RXBuffer: Buffer to put receive data
 *   - RXSize: Size of data
 *   - int tm: Personalized timeout. Can be
 *   	-1: Take timeout configured on LLC_StartConnection
 *   	>= 0: Timeout personalized timeout in hundredth of seconds.
 * Returns:
 *   - LLC_RET_OK		Send OK
 *   - LLC_ERR_RECEIVE 	Error in receive buffer, for detail see nLastError
 **************************************************************************--*/
int16 LLC_Receive(LLCOMS_CONFIG *stpConfig, uint8 * RXBuffer,  int *RXSize, int tm);

/*****************************************************************************
 * Func Name:		LLC_GetStatus
 * Description: 	Get status of current network configuration
 * Parameters:
 *   - LLCOMS_CONFIG *stpConfig: Pointer to communication configuration
 * Returns:
 *   - Status of network
 *   	<For Link layer>
 *   	- LL_STATUS_CONNECTED
 *   	- LL_STATUS_DISCONNECTED
 *   	- LL_STATUS_CONNECTING
 *   	- LL_STATUS_DISCONNECTING
 *   	- LL_STATUS_PHYSICAL_DISCONNECTION
 *   	<For SSL Server>
 *   	- SSL_STATE_DISCONNECT
 *   	- SSL_STATE_TCP_CONNECTION
 *   	- SSL_STATE_TCP_CONNECT
 *   	- SSL_STATE_SSL_CONNECTION
 *   	- SSL_STATE_SSL_CONNECT
 *   	- SSL_STATE_CONNECT
 **************************************************************************--*/
int LLC_GetStatus(LLCOMS_CONFIG *stpConfig);

/*****************************************************************************
 * Func Name:		LLC_GetLastError
 * Description: 	Get last error
 * Parameters:
 * Returns:
 *   - LinkLayer Error
 **************************************************************************--*/
int16 LLC_GetLastError(LLCOMS_CONFIG *stpConfig, int16 *LastError );


/*****************************************************************************
 * Func Name:	LLC_SetSSLCA
 * Description: Configure SSL profile
 * Parameters:
 *   - SSLCert				Profile to be configured
 *   - int SSLVersion		(SSLv2, SSLv3, TLSv1, SSLv23, TLSv1_1, TLSv1_2)
 *   						Default = 0 (SSLv3)
 *
 *   - long int Cipher 		(SSL_RSA, SSL_DSS, SSL_DES, SSL_3DES, SSL_RC4, SSL_RC2, SSL_AES, SSL_MD5, SSL_SHA1, SSL_SHA256, SSL_SHA384)
 *   					 	Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)
 *
 *   - long int ExportMask	Export information (SSL_NOT_EXP, SSL_EXPORT)
 *   						Cipher strength information (SSL_STRONG_NONE, SSL_EXP40, SSL_MICRO, SSL_EXP56, SSL_MINI, SSL_LOW, SSL_MEDIUM, SSL_HIGH)
 *   						Default = 0 (SSL_HIGH | SSL_NOT_EXP)
 * 	 - uint8 FlagSSLProfSigned Flag to use signed or not profile
 * Returns:
 *   - RET_OK	The operation has been successfully processed
 *   - RET_NOK	Error in configuration, for detail see nLastError
 **************************************************************************--*/
int LLC_SetSSLCA(char *SSLCert, int SSLVersion, long int Cipher, long int ExportMask, uint8 FlagSSLProfSigned);



/*****************************************************************************
 * Func Name:		LLC_StopGPRS
 * Description: 	stop gprs connection
 * Parameters:
 *   - none
 * Returns:
 *   - none
 **************************************************************************--*/
void LLC_StopGPRS(LLCOMS_CONFIG *stpConfig);

#endif //LLCOMMS_H

