#define PACKED __attribute__((aligned (1), packed))


/** Library version that this header file refers to */
#define EMLIB_VERSION  "01.01"

#include "CoreDefines.h"

#define EM_CONNTYPE_ERROR					-101  //!< Connection type unknown
#define EM_IPADDRESS_ERROR					-102 //!< IP Address Error

#define L_EM_GPRS_LOGIN_NAME				22
#define L_EM_GPRS_PASSWORD					22
#define L_EM_GPRS_APN						100
#define L_EM_IP								16
#define L_EM_PORT 							6
#define L_EM_TMS_ID 						10
#define L_EM_PABX							16
#define L_EM_PHONE 							35
#define L_EM_SSL_PROFILE					11

typedef enum
{
	AutoStart_ON,						//!< Restart terminal after TMS call.
	AutoStart_OFF						//!< No restart terminal after TMS call.
} AutoRestart;


typedef struct tagTABLE_EM_CONF
{
	char 	iP_Host[L_EM_IP];  				/*!<  ip address xxx.xxx.xxx.xxx*/
	char	Port[L_EM_PORT];				/*!<  Port number xxxx*/
	char    type_modem;						/*!< modem type : \li V22 for compatibility, \li VIP for new interface.*/
	char 	tms_ID[L_EM_TMS_ID];			/*!< tms identifier  */
	char 	ConnType;						/*!< connection type : TMSGPRS, TMSIP, TMSIPRTC, RTC */
//	AutoRestart 	autoRestart;			/*!<  AutoStart_ON - AutoStart_OFF*/
	char  	login[L_EM_GPRS_LOGIN_NAME];	/*!< APN or PPP Login*/
	char  	password[L_EM_GPRS_PASSWORD];	/*!< APN or PPP password*/
	char  	apn[L_EM_GPRS_APN];				/*!< APN Data*/
	char	Phone[L_EM_PHONE];				/*!< phone number only in  TMSIPRTC or RTC */

	//SSL configuration
	char  	SSLProfile[L_EM_SSL_PROFILE];	/*!< SSL profile.set to NULL for SSL OFF, and != NULL for SSL ON*/
	uint8	iS_SSLVersion;					// SSL version - Default = 0 (SSLv3)(see details in LLComms.h)
	long int iS_SSLCipher;					// SSL cipher mode - Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)(see details in LLComms.h)
	long int iS_SSLExportMask;				// SSL export and mask - Default = 0 (SSL_HIGH | SSL_NOT_EXP)(see details in LLComms.h)
	uint8    FlagSSLProfSigned;				// Flag to define if the profile is signed or not (Not signed leaves it on HOST if not the certificate needs to be put on swap signed)

}PACKED EM_CONF;

// table length
#define L_EM_CONF sizeof(EM_CONF)

/** ------------------------------------------------------------------------------
 * Function Name:	PerformRemoteIP
 * Author:			@DFVC
 * Date:		 	Oct 27, 2015
 * Description:		This function calls the TMS for remote software downloading.
 * Parameters:
 *  - EMconf Parameters for connection
 * Return:
 * - STATUS_MDP_OK
 * - STATUS_MDP_PAS_MAJ
 * - STATUS_MDP_INCID
 * - CALL_HOST_ARG_MISSING
 * - CALL_HOST_ARG_INVALID
 * - CALL_HOST_PPP_CONFIG_FAIL
 * - CALL_HOST_CONNECTION_FAIL
 * - CALL_HOST_CONNECTION_LOST
 * - EM_CONNTYPE_ERROR
 * - EM_IPADDRESS_ERROR
 * Notes:
 */
int PerformRemoteIP(EM_CONF EMConf, AutoRestart autoRestart);

/** ------------------------------------------------------------------------------
 * Function Name:	TMS_Call
 * Author:			@DFVC
 * Date:		 	Mar 30, 2016
 * Description:
 * Parameters:
 *  - autoRestart, indicate if terminal make restart when call ended
 * Return:
 * - STATUS_MDP_OK
 * - STATUS_MDP_PAS_MAJ
 * - STATUS_MDP_INCID
 * - CALL_HOST_ARG_MISSING
 * - CALL_HOST_ARG_INVALID
 * - CALL_HOST_PPP_CONFIG_FAIL
 * - CALL_HOST_CONNECTION_FAIL
 * - CALL_HOST_CONNECTION_LOST
 * - EM_CONNTYPE_ERROR
 * - EM_IPADDRESS_ERROR
 * Notes:
 */
int TMS_Call(AutoRestart autoRestart);

/** ------------------------------------------------------------------------------
 * Function Name:	TMS_Read_Parameters
 * Author:			@DFVC
 * Date:		 	Mar 30, 2016
 * Description:		Read TMS parameters
 * Parameters:
 *  - EMConf, EM_CONF struct pointer
 * Return:
 *  - none
 * Notes:
 */
void TMS_Read_Parameters(EM_CONF *EMConf);

/** ------------------------------------------------------------------------------
 * Function Name:	TMS_Save_Parameters
 * Author:			@DFVC
 * Date:		 	Mar 30, 2016
 * Description:
 * Parameters:
 *  - EMConf, EM_CONF struct pointer
 * Return:
 * - PSQ_Init_TMS_Parameters_OK
 * - PSQ_Init_TMS_Parameters_Unavailable
 * - PSQ_Init_TMS_Parameters_Syntax_Error
 * Notes:
 */
int  TMS_Save_Parameters(EM_CONF *EMConf);

/** ------------------------------------------------------------------------------
 * Function Name:	_FUN_ConvertIpAsciiToInt
 * Author:			@DFVC
 * Date:		 	Mar 31, 2016
 * Description:		Parse the address to ip and port
 * Parameters:
 *  - addr, Puntero de la ip en scii xxx.xxx.xxx.xxx:xxxx
 *  - o_pnAddress, retorna la ip numerica
 *  - o_pnAddress, retorna el ip numerico
 * Return:
 *  - 1 if OK; 0 on error
 * Notes:
 */
int _FUN_ConvertIpAsciiToInt(const char *addr, unsigned int *o_pnAddress, unsigned int *o_pipPort);

/** ------------------------------------------------------------------------------
 * Function Name:
 * Author:			@DFVC
 * Date:		 	Mar 31, 2016
 * Description:
 * Parameters:
 *  - ip, Entero que contiene la ip
 *  IPascii, Retorna la ip en ascii XXX.XXX.XXX.XXX
 * Return:
 *  - none
 * Notes:
 */
void _FUN_ConvertIntToIpAscii(int ip, char *IPascii);
