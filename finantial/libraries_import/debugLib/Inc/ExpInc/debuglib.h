#ifndef DEBUGLIB_H
#define DEBUGLIB_H

enum debugTarget_e
{
   DEBUG_PRN,
   DEBUG_COM,
   DEBUG_TELIUM
};

#ifdef DEBUG_ON
#define IFDEBUG(x) x
#else
#define IFDEBUG(x)
#endif


void debugTeliumSAP (trace_sap_id_t trace_sap_id);
void debugPrintfTelium(char *fmt, ...);
void debugHexTelium (byte  *zbData, int lenData);
void debugGetSemaphoreReal(void);
void debugReleaseSemaphoreReal(void);
void debugPrintfReal (enum debugTarget_e eType, char *fmt, ...);
void debugPrintfMixReal (enum debugTarget_e eType, byte  *zbData, int lenData, byte withspace, char *fmt, ...);
void debugHexDumpReal (enum debugTarget_e eType, void *data, int length);
void debugAssignHandleReal (enum debugTarget_e eType, Telium_File_t *Handle);

#if 0
void debugTeliumPrintf (char *fmt, ...);
void debugTeliumMix  (byte  *zbData, int lenData, byte withspace, char *fmt, ...);
void debugTeliumHexdump (void *data, int length);
#endif


#ifdef DEBUG_ON
#define debugGetSemaphore debugGetSemaphoreReal
#else
#define debugPrintf REMOVE_THIS_DEBUG;
#endif

#ifdef DEBUG_ON
#define debugReleaseSemaphore debugReleaseSemaphoreReal
#else
#define debugPrintf REMOVE_THIS_DEBUG;
#endif


#ifdef DEBUG_ON
#define debugPrintf debugPrintfReal
#else
#define debugPrintf REMOVE_THIS_DEBUG;
#endif

#ifdef DEBUG_ON
#define debugPrintfMix debugPrintfMixReal
#else
#define debugPrintfMix REMOVE_THIS_DEBUG;
#endif

#ifdef DEBUG_ON
#define debugHexDump debugHexDumpReal
#else
#define debugHexDump REMOVE_THIS_DEBUG;
#endif

#ifdef DEBUG_ON
#define debugAssignHandle debugAssignHandleReal
#else
#define debugPrintf REMOVE_THIS_DEBUG;
#endif



#endif

