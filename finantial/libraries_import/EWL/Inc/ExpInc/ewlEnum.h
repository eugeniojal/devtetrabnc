#ifndef EWLENUM_H
#define EWLENUM_H

/** Library version that this header file refers to */
#define EWL_VERSION  "00.23"


/** \c larlib.log channel list */
enum ewlLogChannels_t {
    EWL_LOG_KERNEL              = 230,      /**< Log interactions with EMV kernel */
    EWL_LOG_INTERFACE_PARAMETER = 231,      /**< Log parameters input/output */
    EWL_LOG_CARD_COMMUNICATION  = 232,      /**< Log communication with card */
    EWL_LOG_FUNCTION            = 233,      /**< Generic log channel for EWL functions */
    EWL_LOG_FUNCTION_TASK       = 234,      /**< Generic log channel for EWL functions working in a task eg: ewlCancel */
};


/** Default or maximum tag for tags */
enum ewlTagsLength_t {
    EMV_CERTIFICATE_SERIAL_NUMBER_LEN                   = 3,
    EMV_TRACK1_DATA_LEN                                 = 76 + 1, // with null terminator
    EMV_TRACK2_DATA_LEN                                 = 37 + 1, // with null terminator
};


/** Supported card technologies */
typedef enum {
    EWL_TECHNOLOGY_UNKNOWN              = 0,    /**< Card is unknown */
    EWL_TECHNOLOGY_CONTACT              = 1,    /**< Card is contact EMV */

    #ifdef EWL_ENABLE_KERNEL_PAYWAVE
    EWL_TECHNOLOGY_MSD                  = 2,    /**< Card is c'less MSD */
    EWL_TECHNOLOGY_QVSDC                = 3,    /**< Card is c'less qVSDC or MSD (if card do not support qVSDC we can use MSD)*/
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYPASS
    EWL_TECHNOLOGY_PAYPASS_MAG          = 4,    /**< Card is c'less paypass magnetic stripe */
    EWL_TECHNOLOGY_PAYPASS_MCHIP        = 5,    /**< Card is c'less paypass MChip */
    #endif

    #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    EWL_TECHNOLOGY_EXPRESSPAY_MAG       = 6,    /**< Card is c'less expresspay magnetic stripe */
    EWL_TECHNOLOGY_EXPRESSPAY_EMV       = 7,    /**< Card is c'less expresspay EMV or Magnetic Stripe (if card do not support EMV we can use magnetic stripe)*/
    #endif

    #ifdef EWL_ENABLE_KERNEL_PURE
    EWL_TECHNOLOGY_PURE                 = 10,   /**< Card is c'less PURE */
    #endif

    EWL_TECHNOLOGY_DIRECT_CONTACT       = 11,   /**< Card is contact direct command */
    EWL_TECHNOLOGY_DIRECT_CONTACTLESS   = 12,    /**< Card is c'less direct command */

    #ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
    EWL_TECHNOLOGY_DISCOVER_PAS         = 13,   /**< Card is c'less discover PAS (if card do not support PAS we can use ZIP oe MSTRIPE) */
    EWL_TECHNOLOGY_DISCOVER_ZIP         = 14,   /**< Card is c'less discover ZIP ( out parameter only ) */
    EWL_TECHNOLOGY_DISCOVER_MAG         = 16,   /**< Card is c'less discover MSTRIPE (parameter only )*/
    #endif

} ewlTechnology_t;

/** Interface type values */
typedef enum {
    EWL_CONTACT      = 0,        /**< Transaction execute though contact interface. */
    EWL_SAM1         = 1,        /**< SAM1 reader */
    EWL_SAM2         = 2,        /**< SAM2 reader */
    EWL_SAM3         = 3,        /**< SAM3 reader */
    EWL_SAM4         = 4,        /**< SAM4 reader */
    EWL_CONTACTLESS  = 5         /**< Transaction execute though contactless interface. */
} ewlInterfaceType_t;

/** Error values */
typedef enum {
    EWL_OK                                          =  0,       /**<  0     Operation successful */
    EWL_WARNING_CARD_UNKONW                         =  0x8000,  /**< +32768 Operation complete but the card is not the same present in the first tap. */
    EWL_ERR_CARD_MUTE                               = -0x8000,  /**< -32768 Card do not answer to poweron*/
    EWL_ERR_ALL_APP_BLOCKED                         = -0x8001,  /**< -32769 All applications in the card are blocked */
    EWL_ERR_NO_COMPATIBLE_APP                       = -0x8002,  /**< -32770 There is not any matching between card and terminal application */
    EWL_ERR_ALL_APP_INVALIDATED                     = -0x8003,  /**< -32771 All applications are invalidated, returns 6A81*/
    EWL_ERR_ALL_APP_ERROR                           = -0x8004,  /**< -32772 All applications in the card are blocked or has some error or are invalidated */
    EWL_ERR_INSUFFICIENT_OUT_BUFFER                 = -0x8005,  /**< -32773 Insufficient out buffer */
    EWL_ERR_USER_TIMEOUT                            = -0x8006,  /**< -32774 Timeout in card holder operations */
    EWL_ERR_USER_CANCEL                             = -0x8007,  /**< -32775 Transaction canceled by cardholder */
    EWL_ERR_USER_BY_PASS                            = -0x8008,  /**< -32776 Undefined error in cardholder operation */
    EWL_ERR_GATHERING_PARAMETERS                    = -0x8009,  /**< -32777 Error during data gathering on a call back */
    EWL_ERR_INTERNAL_FLOW                           = -0x800A,  /**< -32778 Some internal call was execute in wrong sequence */
    EWL_ERR_INTERNAL_INVALID_FORMAT                 = -0x800B,  /**< -32779 Invalid value inside of library  */
    EWL_ERR_INTERNAL_VALUE_OVERFLOW                 = -0x800C,  /**< -32780 Internal value overflow */
    EWL_ERR_TAG_NOT_FOUND                           = -0x800D,  /**< -32781 Tag not found */
    EWL_ERR_NO_RESOURCE                             = -0x800E,  /**< -32782 No enough resources (including device handles) to execute this function */
    EWL_ERR_MISSING_CONTACTLESS_CARD_MANDATORY_DATA = -0x800F,  /**< -32783 Missing mandatory contactless card data */
    EWL_ERR_MISSING_MANDATORY_PARAMETER             = -0x8010,  /**< -32784 Missing mandatory parameter */
    EWL_ERR_INVALID_CARD_DATA                       = -0x8011,  /**< -32785 Error in a card data */
    EWL_ERR_INVALID_PARAMETER                       = -0x8012,  /**< -32786 At least one of the input parameter or tag is not valid." */
    EWL_ERR_INVALID_DOL                             = -0x8013,  /**< -32787 Invalid data object list */
    EWL_ERR_REDUNDANT_CARD_DATA                     = -0x8014,  /**< -32788 Card data is duplicated */
    EWL_ERR_NOT_IMPLEMENTED                         = -0x8015,  /**< -32789 Option is not implemented eg: new contactless card type */
    EWL_ERR_SWITCH_INTERFACE                        = -0x8016,  /**< -32790 Card must be processed using other interface */
    EWL_ERR_INVALID_CALL                            = -0x8017,  /**< -32791 Function call in wrong sequence */
    EWL_ERR_MISSING_COMPONENT                       = -0x8018,  /**< -32792 Kernel or DLL necessary to operation is missing */
    EWL_ERR_KERNEL_LINK                             = -0x8019,  /**< -32793 An error occurred into the kernel interface (linked to the custom application) */
    EWL_ERR_CLESS_DRIVER_NOT_OPEN                   = -0x801A,  /**< -32794 CLESS driver is not open */
    EWL_ERR_RESELECT                                = -0x801B,  /**< -32795 Generate AC returns 6985 */
    EWL_ERR_CARD_INVALID_ANSWER                     = -0x801C,  /**< -32796 Invalid card answer eg: card response transaction approved when terminal advice a denial, Gernerate AC returns some invalid SW1SW2 [2]*/
    EWL_ERR_INTERNAL_PARAMETER                      = -0x801D,  /**< -32797 Invalid internal parameter*/
    EWL_ERR_INTERNAL_TLVTREE                        = -0x801E,  /**< -32798 Error in tlvTree processing inside of library*/
    EWL_ERR_INTERNAL_MISSING_DATA                   = -0x801F,  /**< -32799 Missing internal parameter */
    EWL_ERR_IS_EASY_ENTRY                           = -0x8020,  /**< -32800 The technology of this card is Easy Entry */
    EWL_ERR_RF_PROBLEM                              = -0x8021,  /**< -32801 CLESS Communication problem */
    EWL_ERR_INTERNAL_INSUFFICIENT_BUFFER            = -0x8022,  /**< -32802 Insufficient internal buffer */
    EWL_ERR_NOT_ALLOWED                             = -0x8023,  /**< -32803 Operation not allowed */
    EWL_ERR_INVALID_HANDLE                          = -0x8024,  /**< -32804 Invalid handle */
    EWL_ERR_INVALID_CARD                            = -0x8025,  /**< -32805 Invalid card eg:. Missing mandatory data as PAN , user set a card data instead read*/
    EWL_ERR_ALL_APP_NON_EMV                         = -0x8026,  /**< -32806 All applications in the card are non EMV app  eg. (GPO returns 6985) or missing a mandatory data that makes transaction impossible (paypass EWL_TAG_CLESS_TRANSACTION_LIMIT or EWL_TAG_CLESS_CVM_LIMIT )*/
    EWL_ERR_CLESS_COLLISION                         = -0x8027,  /**< -32807 Two or more cless card were approached */
    EWL_ERR_CLESS_DETECTION_PROBLEM                 = -0x8028,  /**< -32808 CLESS Problem in detection phase*/
    EWL_ERR_CLESS_TYPE_NOT_SUPPORTED                = -0x8029,  /**< -32809 CLESS is not a EMV payament card (MIFARE, Calypso, others). If  this error error do not call ewlCBRemoveCard_t. */
    EWL_ERR_MAGSTRIPE_NOT_ALLOWED                   = -0x802A,  /**< -32810 Can not process PayPass Mag Stripe Transaction*/
    EWL_ERR_MCHIP_NOT_ALLOWED                       = -0x802B,  /**< -32811 Can not process PayPass MChip Transaction */
    EWL_ERR_MOBILE_ON_DEVICE_CVM                    = -0x802C,  /**< -32812 It is a mobile CVM on device */
    EWL_ERR_KERNEL_DISABLE                          = -0x802D,  /**< -32813 Kernel disable */
    EWL_ERR_CARD_REMOVED                            = -0x802E,  /**< -32814 Card removed during the processing*/
    EWL_ERR_PINPAD_OFF_ORDER                        = -0x802F,  /**< -32815 Pinpad off order */
    EWL_ERR_CARD_BLOCKED                            = -0x8030,  /**< -32816 Card blocked (PPSE return 0x6A81) */
    EWL_ERR_USE_OTHER_CARD                          = -0x8031,  /**< -32817 Use other card (dicover DPAS possible answer) */
    EWL_ERR_USE_CONTACT                             = -0x8032,  /**< -32818 Ignore cless interface and start a contact transaction immediately  (qVSDC50)*/
    //EWL_ERR_DLL_PROBLEM                           = -0x8033,  /**< -32819  Reserved*/

    EWL_ERR_KERNEL                                  = -0x8FFE,  /**< -36862 Internal kernel error */
    EWL_ERR_INTERNAL                                = -0x8FFF   /**< -36863 Unknown library internal error */

} ewlStatus_t;

/** Kernel status */
typedef enum {
    EWL_STATUS_UNKNOWN              = 0, /**< Undefined status. */
    EWL_STATUS_APPROVED_OFFLINE     = 1, /**< Transaction approved in offline mode. */
    EWL_STATUS_APPROVED_ONLINE      = 2, /**< Transaction approved in online mode. */
    EWL_STATUS_DENIAL_OFFLINE       = 3, /**< Transaction denial in offline mode. */
    EWL_STATUS_DENIAL_ONLINE        = 4, /**< Transaction denial in online mode. */
    EWL_STATUS_GO_ONLINE            = 5, /**< Terminal must contact host. */
} ewlTransactionStatus_t;

/** Host answer */
typedef enum {
    EWL_HOST_UNKNOW     = 0,    /**< Host not contacted (unable communicate with host). */
    EWL_HOST_APPROVED   = 1,    /**< Host approve transaction. */
    EWL_HOST_DENIAL     = 2,    /**< Host denial transaction. */
} ewlHostAnswer_t;

/** Display message IDs */
typedef enum {
    EWL_DISPLAY_OFFLINE_LAST_ATTEPMPT       = 1, /**< Warn card holder that next wrong offline pin will blocked the card */
    EWL_DISPLAY_OFFLINE_PIN_OK              = 2, /**< Warn card holder that offline pin was accepted */
    EWL_DISPLAY_OFFLINE_PIN_ERROR           = 3, /**< Warn card holder that offline pin was not accepted */
    EWL_DISPLAY_OFFLINE_PIN_BLOCKED         = 4, /**< Warn card holder that pin offline was blocked */
    EWL_DISPLAY_APPLICATION_BLOCKED         = 5, /**< Warn card holder that selected application is blocked */
    EWL_DISPLAY_APPLICATION_ERROR           = 6, /**< Warn card holder that selected application has some error */
    EWL_DISPLAY_APPLICATION_INVALIDATED     = 7, /**< Warn card holder that selected application is invalidated */
    EWL_DISPLAY_APPLICATION_NON_EMV         = 8  /**< Warn card holder that selected application is non EMV */
} ewlDisplay_t;

/** Device Type [1] */
typedef enum{
    EWL_DEVICE_CARD             = 0,    /**< It is a normal credit card */
    EWL_DEVICE_CARD_MINI        = 1,    /**< It is a credit card with reduced dimension */
    EWL_DEVICE_NON_CARD         = 2,    /**< It is non card cless device (key fobs,watcher, etc) */
    EWL_DEVICE_MOBILE           = 3,    /**< It is a mobile device (mobile phone, tablet etc) working as cless card */
    EWL_DEVICE_UNDEFINED        = 4     /**< There is a rule to define the type but one or more tags in not available */
} ewlDeviceType_t;

/* [1] This definition was obtained from CIELO (Brazil) and may be subject to reevaluation*/
/* [2] If this occur in ewlFinishChip follow instruction 9.3 Book 3 v4.3, If
 *     it occurs after the second GENERATE AC command, all processing for the transaction has been completed, and the cryptogram returned
       shall be treated as an AAC*/
#endif
