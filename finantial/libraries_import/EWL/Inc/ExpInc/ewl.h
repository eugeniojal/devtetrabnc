#ifndef EWL_H_
#define EWL_H_
/**
 * @file ewl.h
 * @brief EMV Wrapper Library
 *
 * @mainpage
 *
 * \ref EWL (<em>EMV Wrapper Library</em>) provides an abstraction over
 * Telium's <em>EMV Level 2</em> and <em>Easy Path to C'less</em> libraries
 * for processing EMV transactions.
 *
 * @section Usage
 *
 * The fundamental usage of EWL requires the following calls, in specific order:
 *
 * -# \ref ewlCreate(): to create the handle with internal data structures;
 * -# \ref ewlAddAID(): to add the list of AIDs that the application support;
 *  - See \ref ewlAIDSetStruct_t for information about each AID;
 * -# \ref ewlGetCard(): to execute EMV selection process (up to <em>Read Records</em>);
 * -# \ref ewlGoOnChip(): to execute all authentication steps and generate first AC;
 * -# \ref ewlFinishChip(): to process host response (if any) and generate second AC.
 *
 * Besides, the application will probably need to  make some calls to \ref ewlSetParameter() and
 * \ref ewlGetParameter() to store and extract values from the EWL handle.  Each parameter is
 * associated with a \e tag, some tags are from the EMV / C'less specifications, others are
 * proprietary and of internal use to EWL, and others are reserved for use by the caller
 * application.
 *
 * @todo Example code of EWL setup and minimal callbacks.
 *
 * @section Modules
 *
 * The library is broken into modules:
 * - \ref Definitions : General typedefs, enums and other constants;
 * - \ref EMV : High-level EMV processing API
 * - \ref DirectChip : Direct access to Chip through APDU commands.
 *
 * Check out the root module \ref EWL for more information.
 *
 * @section Requirements
 *
 * This library requires that the following modules be linked together with
 * the target application:
 * - <em>EMVAPI</em> (EasyPath to EMV (EMV API) 22.10.0.00 or higher)
 * - <em>LarLib</em> (1.8 or higher)
 *
 * The target terminal must also include the follows modules:
 * - <em>Kernel EMV [3065]</em>  (4.67 or higher)[1]
 * - <em>EMV new API [844585]</em> (1.01 or higher)[1]
 * - <em>C'Less add on [3655]</em> (4.01 or higher)[2]
 * - <em>C'Less Entry Point [813354]</em> (0.25 or higher)[3]
 *
 * If the project works with PayWave, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_PAYWAVE in project symbols</em>
 * - <em>Include payWaveInterface in the project </em> (EasyPath to C'Less 5.6.2.00 or higher)
 * - <em>Load Kernel PayWave [813350] in the terminal</em> (2.04 or higher)[3]
 *
 * If the project works with PayPass, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_PAYPASS in project symbols</em>
 * - <em>Include PayPassInterface in the project </em> (EasyPath to C'Less 5.6.2.00 or higher)
 * - <em>Load Kernel PayPass3 [844241] in the terminal</em> (3.73 or higher)[3]
 *
 * If the project works with ExpressPay, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_EXPRESSPAY in project symbols</em>
 * - <em>Include ExpressPayInterface in the project </em> (EasyPath to C'Less 5.6.2.00 or higher)
 * - <em>Load Kernel ExpressPay3 [844297] in the terminal</em> (3.08 or higher)[3]
 *
 * If the project works with PURE, developer must:s
 * - <em>Set the define \c EWL_ENABLE_KERNEL_PURE in project symbols</em>
 * - <em>Include PUREInterface in the project </em>  (Not integrated to EasyPath to C'Less yet)
 * - <em>Load Kernel PURE [844565] in the terminal</em> (1.03 or higher)[4]
 *
 * If the project works whit any cless technology and is a T2 terminal, be sure if the manager is
 * set to accept cless transactions (F -> Telium Manager -> Initialization -> Parameters-> Contactless) or
 * use the function PSQ_update_ClessReader to force this situation.
 *
 * [1] - EasyPath to EMV (EMV API) (22.10.0.00 or higher)
 * [2] - Telium SDK (9.20.1.02 or higher)
 * [3] - EasyPath to C'Less (5.6.2.00 or higher)
 * [4] - Not integrate yet
 *
 * The target terminal must also has support to contactless and this feature must
 * be enabled in Telium Manager / Initialization / Parameters / Contactless
 *
 * @addtogroup EWL
 * @{
 */

#include <larlib/all.h>
#include <TlvTree.h>


#include <EmvLib_Tags.h>

#include "GTL_SharedExchange.h"
#include "Cless_LowLevel.h"

#ifdef EWL_ENABLE_KERNEL_PAYWAVE
    #include "PayWave_Tags.h"
    #include "PayWave_API.h"
#endif

#ifdef EWL_ENABLE_KERNEL_PAYPASS
    #include "PayPass_Tags.h"
    #include "PayPass3_API.h"
    #include "PayPass_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    #include "ExpressPay_Tags.h"
    #include "ExpressPay3_API.h"
    #include "ExpressPay_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_PURE
    #include "PURE_Tags.h"
    #include "PURE_API.h"
    #include "PURE_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
    #include "DiscoverDPAS_Tags.h"
    #include "DiscoverDPAS_API.h"
    #include "DiscoverDPAS_API_Common.h"
#endif

#include "ewlEnum.h"
#include "ewlTags.h"


/** @addtogroup Definitions
 * @{
 */

/** Certificate request structure */
typedef struct {
    unsigned char RID[EWL_TAG_RID_LEN];     /**< Card application RID. */
    unsigned int index;                     /**< Certification authority public key index. */
} ewlGetCertificate_t;

/** Certificate revoked request structure */
typedef struct {
    unsigned char RID[EWL_TAG_RID_LEN];     /**< Card application RID */
    unsigned char index;                /**< Certification authority public key index */
    unsigned char certificateSN[EMV_CERTIFICATE_SERIAL_NUMBER_LEN]; /**< Revoked certificate serial number */
} ewlSetRevoked_t;

/** Handle to an instance of EWL.
 * Multiple EWL handles may be open at one time, but they must not point to the same device.
 */
typedef struct ewlObject_t ewlObject_t;

/** Type used for a tag ID */
typedef uint32_t ewlTag_t;

/**
 * Select application callback.
 *
 * May be called during \ref ewlGetCard() to select or confirm the correct
 * card application that shall be used.
 *
 * @param handle EWL Handle.
 * @param[out] selected Shall be set to the ID of selected card application
 * @param aidList List of valid card application labels
 * @param nAIDs Number of entries in \p aidList array
 *
 * @note If \p nAIDs is equal to 1, it indicates that terminal application must
 *  only confirm if cardholder wants continue this transaction using the given
 *  valid application.
 *
 * @return EWL_OK if application was selected by user (\ref ewlGetCard() goes on)
 * @return Other (\ref ewlGetCard() is interrupted and returns the value)
 */
typedef ewlStatus_t (*ewlCBSelectionMenu_t)(ewlObject_t *handle, unsigned int *selected,
                                            const char **aidList, unsigned int nAIDs);

/**
 * Get application parameters.
 *
 * May be called during \ref ewlGetCard(), before final selection,
 * to inform the library of EMV parameter dependent on selected application.
 *
 * The \p dataList array of \p nDataListElement elements is a list of all the
 * \p data values (from \ref ewlAIDSetStruct_t) of the possible candidate
 * applications.
 *
 * \p nDataListElement will only be more than one if two entries in the
 * AID candidate list (added through \ref ewlAddAID()) match to the same
 * application on the card.
 *
 * The parameters shall be set by calls to \ref ewlSetParameter() on \p handle.
 *
 * @param handle            EWL Handle.
 * @param dataList          List of data associated with the card application.
 * @param nDataListElement  Number of elements in the dataList.
 *
 * @return EWL_OK if parameter was set by user (\ref ewlGetCard() goes on)
 * @return Other (\ref ewlGetCard() is interrupted and returns the value)
 *
 * \note If possible do not put HMI functions in this callback, this can affect directly  the C'Less transactions
 *
 */
typedef ewlStatus_t (*ewlCBGetParameters_t)(ewlObject_t   *handle, const unsigned long *dataList,
                                            unsigned int  nDataListElement);

/**
 * Get certification authority public key.
 *
 * May be called during \ref ewlGoOnChip(), before authentication step, and should
 * return the public key of the certification authority (if one exist).
 *
 * @param handle            EWL Handle.
 * @param[out] certificate  Authority public key.
 *
 * @return EWL_OK normal key processinq, even key not found (\ref ewlGoOnChip() goes on)
 * @return If any other, ewlGoOnChip() is interrupted and returns the value)
 *
 * @note Missing public keys is not a fatal error, but transaction may be aborted due to
 *  authentication failure.
 */
typedef ewlStatus_t (*ewlCBGetPublicKey_t)(ewlObject_t *handle, ewlGetCertificate_t *certificate);

/**
 * Get PIN online.
 *
 * May be called during \ref ewlGoOnChip(), during cardholder verification, to capture
 * the cardholder online PIN.
 *
 * The callback may store the PIN value in one of the \c EWL_TAG_USER_DATA fields,
 * or in any other external memory.  EWL does not use the online PIN value for processing.
 *
 * @param handle                EWL Handle.
 *
 * @return EWL_OK               PIN online collected without problems  (\ref ewlGoOnChip() goes on)
 * @return EWL_ERR_USER_TIMEOUT Timeout (\ref ewlGoOnChip() is interrupted and returns \ref EWL_USER_TIMEOUT)
 * @return EWL_ERR_USER_CANCEL  Operation canceled by user   (function ewlGoOnChip is interrupted and returns EWL_USER_CANCEL)
 * @return EWL_ERR_USER_BY_PASS Operation bypassed by user   (function ewlGoOnChip goes on but considers pin online bypassed as defined in specification )
 * @return Other                (function ewlGoOnChip consider pinpad problem and follow EMV flow)
 *
 * @note If this function is not defined, EWL will behave as if \ref EWL_CB_BYPASS was returned.
 */
typedef ewlStatus_t (*ewlCBGetPinOnLine_t) (ewlObject_t *handle);

/**
 * Get PIN offline call back.
 *
 * May be called during \ref ewlGoOnChip(), during cardholder verification, to inform
 * application to collect off-line PIN.
 *
 * @param handle               EWL Handle
 * @param[out] numDigits       Number of digits read in PIN offline operation
 *
 * @return EWL_OK            PIN offline collected without problems  (function ewlGoOnChip goes on )
 * @return EWL_ERR_USER_TIMEOUT       Timeout                      (function ewlGoOnChip is interrupted and returns EWL_USER_TIMEOUT)
 * @return EWL_ERR_USER_CANCEL      Operation canceled by user   (function ewlGoOnChip is interrupted and returns EWL_USER_CANCEL)
 * @return EWL_ERR_USER_BY_PASS       Operation bypassed by user   (function ewlGoOnChip goes on but considers pin online bypassed as defined in specification )
 * @return Other               (function ewlGoOnChip consider pinpad problem and follow EMV flow)
 *
 * \note If this function is not defined, EWL considers as answer EWL_CB_BYPASS.
 */
typedef ewlStatus_t (*ewlCBGetPinOffLine_t)(ewlObject_t *handle, unsigned char *numDigits);

/**
 * End of cardholder verification process.
 *
 * Called during \ref ewlGoOnChip(), at the end cardholder verification, should be used to validate
 * if the transaction should continue or not (for example, asking the user to confirm amount).
 *
 * @return EWL_OK           User confirm the end of CVM process (function ewlGoOnChip goes on )
 * @return EWL_ERR_USER_TIMEOUT      Timeout                      (function ewlGoOnChip is interrupted and returns EWL_USER_TIMEOUT)
 * @return EWL_ERR_USER_CANCEL     Operation canceled by user   (function ewlGoOnChip is interrupted and returns EWL_USER_CANCEL)
 * @return Other               (function ewlGoOnChip consider pinpad problem and follow EMV flow)
 *
 * \note If this function is not defined, EWL considers as answer EWL_CB_OK.
 */
typedef ewlStatus_t (*ewlCBFinishCVMProcessing_t)(ewlObject_t *handle);

/**
 * Display message.
 *
 * May be called during \ref ewlGetCard() or \ref ewlGoOnChip() to ask for a message to be shown to the user.
 * See \ref ewlDisplay_t for the list of messages.
 *
 * @param handle    EWL Handle
 * @param msgCode   Message code
 */
typedef void (*ewlCBDisplay_t)(ewlObject_t *handle, ewlDisplay_t msgCode);

/**
 * Request change of the contact-less LEDs.
 *
 * Called during c'less transactions to control the state of the LEDs.
 *
 * @param handle    EWL Handle.
 * @param step      Led step (0, 1, 2, 3 or 4).
 */
typedef void (*ewlCBLeds_t)(ewlObject_t *handle, unsigned int step);


/**
 * Request remove card from c'less field.
 *
 * Called during c'less transactions request remove card from c'less field.
 *
 * @param handle    EWL Handle.
 * @param status    Inform transaction status .
 */
typedef void (*ewlCBRemoveCard_t)(ewlObject_t *handle, int status);

/** Collection of all callbacks */
typedef struct {
    ewlCBDisplay_t                ewlDisplay;               /**< Show message */
    ewlCBSelectionMenu_t          ewlSelectionMenu;         /**< AID selection menu */
    ewlCBGetParameters_t          ewlGetParameter;          /**< Ask for application parameters */
    ewlCBGetPublicKey_t           ewlGetPublicKey;          /**< Get certification authority public key */
    ewlCBGetPinOnLine_t           ewlGetPinOnline;          /**< Get PIN online */
    ewlCBGetPinOffLine_t          ewlGetPinOffLine;         /**< Get PIN offline */
    ewlCBFinishCVMProcessing_t    ewlFinishCVMProcessing;   /**< Finish CVM process */
    ewlCBLeds_t                   ewlLeds;                  /**< Contact-less LEDs control */
    ewlCBRemoveCard_t             ewlRemoveCard;            /**< Warn to remove card */
} ewlCBFunctions_t;

/** Signature for C'less kernel functions that accept the transaction status as parameter */
typedef int (*ewlKernelFunction_t)(T_SHARED_DATA_STRUCT *);

/** Signature to the C'less kernel function to clear all transaction state */
typedef int (*ewlKernelFunctionNoParameter_t)(void);

/** List of functions that must be exported by a C'less kernel to be integrated in EWL.
 * They mimic the ones provided by the contactless kernels in the Telium architecture.
 * For more detailed information see the specific kernel interface documentation.
 */
typedef struct  {
    ewlKernelFunction_t             info;                       /**< Get kernel */
    ewlKernelFunction_t             debugManagement;            /**< Management of kernel debug features */
    ewlKernelFunction_t             doTransaction;              /**< Performs transaction with the presented card */
    ewlKernelFunction_t             getAllData;                 /**< Retrieve the entire kernel tag database */
    ewlKernelFunction_t             getData;                    /**< Get tag from kenel */
    ewlKernelFunction_t             loadData;                   /**< Load tag value in kenel */
    ewlKernelFunction_t             resumeTransaction;          /**< Resume a transaction that has been interrupted */
    ewlKernelFunction_t             afterTransaction;           /**< Perform necessary actions after a transaction */
    ewlKernelFunctionNoParameter_t  cancel;                     /**< Cancel Transaction */
    ewlKernelFunctionNoParameter_t  close;                      /**< Clear and initialize the kernel */
} ewlKernelFunctions_t;

/** Callbacks for each supported kernel.
 * If a specific contacless kernel is not supported on this application, set all callbacks
 * to \c NULL */
typedef struct {
    ewlKernelFunctions_t          ewlPayWave;               /**< PayWave */
    ewlKernelFunctions_t          ewlPayPass;               /**< PayPass */
    ewlKernelFunctions_t          ewlExpressPay;            /**< ExpressPay */
    ewlKernelFunctions_t          ewlPure;                  /**< Pure */
    ewlKernelFunctions_t          ewlDiscoverPAS;           /**< Discover PAS */

} ewlCBKernel_t;

#ifdef EWL_ENABLE_KERNEL_PAYWAVE
/** qVSDC specific AID data */
typedef struct {
    unsigned long   transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
    unsigned char   ttq[EWL_PAYWAVE_TTQ_LEN];   /**< Terminal transaction qualifier */
} ewlAIDContactlessqVSDCSetStruct_t;

/** MDS specific AID data */
typedef struct {
    unsigned long   transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
    unsigned char   ttq[EWL_PAYWAVE_TTQ_LEN];   /**< Terminal transaction qualifier */
} ewlAIDContactlessMSDSetStruct_t;
#endif

#ifdef EWL_ENABLE_KERNEL_PAYPASS
/** PayPass specific Magnetic AID data */
typedef struct {
    unsigned long   transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessPAYPASSMAGSetStruct_t;

/** PayPass specific MChip AID data */
typedef struct {
    unsigned long   transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessMCHIPSetStruct_t;
#endif

#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
/** ExpressPay specific Magnetic AID data */
typedef struct {
    unsigned long   transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessExpressPayMAGSetStruct_t;

/** ExpressPay specific EMV AID data */
typedef struct {
    unsigned long   transactionLimitValue;  /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;       /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;  /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;      /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessExpressPayEMVSetStruct_t;
#endif

#ifdef EWL_ENABLE_KERNEL_PURE
/** PURE specific EMV AID data */
typedef struct {
    unsigned long   transactionLimitValue;  /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;       /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;  /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;      /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessPURESetStruct_t;
#endif

#ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
/** D-PAS (Discover Payment Application Specification) specific EMV AID data */
typedef struct {
    unsigned long   transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    unsigned long   cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    unsigned long   contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool            zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
    unsigned char   ttq[EWL_DISCOVERPAS_TTQ_LEN];   /**< Terminal transaction qualifier */
} ewlAIDContactlessDiscoverPAS_t;
#endif

/** Specific EMV contact AID data
 * @note Currently EMV does not need any specific parameters. */
typedef struct {
    /* EMV contact operation do not need additional parameters */
} ewlAIDContactStruct_t;

/** Additional data associated with an AID */
typedef union {

    #ifdef EWL_ENABLE_KERNEL_PAYWAVE
    ewlAIDContactlessqVSDCSetStruct_t           qVSDC;          /**< qVDSC */
    ewlAIDContactlessMSDSetStruct_t             MSD;            /**< MSD */
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYPASS
    ewlAIDContactlessPAYPASSMAGSetStruct_t      PayPassMag;     /**< PayPass Magnetic */
    ewlAIDContactlessMCHIPSetStruct_t           PayPassMChip;   /**< PayPass MChip */
    #endif

    #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    ewlAIDContactlessExpressPayMAGSetStruct_t   ExpressPayMag;  /**< ExpressPay Magnetic */
    ewlAIDContactlessExpressPayMAGSetStruct_t   ExpressPayEMV;  /**< ExpressPay EMV */
    #endif

    #ifdef EWL_ENABLE_KERNEL_PURE
    ewlAIDContactlessPURESetStruct_t            PURE;           /**< PURE */
    #endif

    #ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
    ewlAIDContactlessDiscoverPAS_t              DiscoverPAS;   /* Discover PAS (D-PAS) */
    #endif

    ewlAIDContactStruct_t                       contact;        /**< EMV contact */
} ewlAIDAdditionalParameter_t;

/** Information about an AID.  Used as parameter for \ref ewlAddAID().
 * The \c data field is used by EWL during callbacks to indicate which entry on the AID
 * candidate list is being referenced, but it is not forwarded to the kernel or chip.
 */
typedef struct{
    unsigned int                aidLen;                         /**< AID length in bytes */
    unsigned char               aid[EWL_EMV_AID_CARD_MAX_LEN];  /**< AID */
    unsigned long               userData;                           /**< Extra information associated to AID (for example table index) */
    ewlTechnology_t             technology;                     /**< Technology associated to AID [1]*/
    ewlAIDAdditionalParameter_t aditionalParameter;             /**< Technology-specific additional data */
} ewlAIDSetStruct_t;

/* @} */

/** @addtogroup EMV EMV High-level API
 * @{
 */

/**
 * Create an EWL instance.
 *
 * @param callbacks List of callback functions
 * @param callbacks List to kernel function
 *
 * Copy the follow structure in your code and use as kernel functions parameter.
 * static ewlCBKernel_t ewlKernelFunctions = {
 *   {
 *   #ifdef EWL_ENABLE_KERNEL_PAYWAVE
 *     payWave_GetInfos,
 *     payWave_DebugManagement,
 *     payWave_DoTransaction,
 *     payWave_GetAllData,
 *     payWave_GetData,
 *     payWave_LoadData,
 *     payWave_ResumeTransaction,
 *     payWave_AfterTransaction,
 *     payWave_Cancel,
 *     payWave_Clear,
 *   #else
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *   #endif
 *   },
 *
 *   {
 *   #ifdef EWL_ENABLE_KERNEL_PAYPASS
 *     PayPass3_GetInfos,
 *     PayPass3_DebugManagement,
 *     PayPass3_DoTransaction,
 *     PayPass3_GetAllData,
 *     PayPass3_GetData,
 *     PayPass3_LoadData,
 *     PayPass3_ResumeTransaction,
 *     NULL,
 *     PayPass3_Cancel,
 *     PayPass3_Clear,
 *   #else
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *   #endif
 *   },
 *
 *   {
 *   #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
 *     ExpressPay3_GetInfos,
 *     ExpressPay3_DebugManagement,
 *     ExpressPay3_DoTransaction,
 *     ExpressPay3_GetAllData,
 *     ExpressPay3_GetData,
 *     ExpressPay3_LoadData,
 *     ExpressPay3_ResumeTransaction,
 *     ExpressPay3_DoTransactionPostProcessing,
 *     ExpressPay3_Cancel,
 *     ExpressPay3_Clear,
 *   #else
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *   #endif
 *   },
 *
 *   {
 *   #ifdef EWL_ENABLE_KERNEL_PURE
 *     PURE_GetInfos,
 *     PURE_DebugManagement,
 *     PURE_DoTransaction,
 *     PURE_GetAllData,
 *     PURE_GetData,
 *     PURE_LoadData,
 *     PURE_ResumeTransaction,
 *     NULL,
 *     PURE_Cancel,
 *     PURE_Clear,
 *   #else
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *   #endif
 *   },
 *
 *   {
 *   #ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
 *     DiscoverDPAS_GetInfos,
 *     DiscoverDPAS_DebugManagement,
 *     DiscoverDPAS_DoTransaction,
 *     DiscoverDPAS_GetAllData,
 *     DiscoverDPAS_GetData,
 *     DiscoverDPAS_LoadData,
 *     DiscoverDPAS_ResumeTransaction,
 *     DiscoverDPAS_DoTransactionPostProcessing,
 *     DiscoverDPAS_Cancel,
 *     DiscoverDPAS_Clear,
 *   #else
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *     NULL,
 *   #endif
 *   }
 * };
 *
 *
 * @return NULL if could not create a new EWL instance, a new EWL instance otherwise.
 *
 * @note Each EWL \e transaction must be executed in a different instance.
 *  Re-using the same EWL instance for multiple transactions is not supported and
 *  has undefined results.
 */
ewlObject_t *ewlCreate(const ewlCBFunctions_t *callbacks, const ewlCBKernel_t *functions );

/**
 * Add an AID to the candidate list.
 *
 * @param handle          EWL Handle
 * @param pAID            AID to be added
 *
 * @note Even though EWL itself does not impose any hard-coded limits on the size
 *  of the AID candidate list, a specific kernel version might.  This case will
 *  only be detected during \ref ewlGetCard().
 *
 * @return See \ref ewlStatus_t
 */
ewlStatus_t ewlAddAID(ewlObject_t *handle, const ewlAIDSetStruct_t *pAID);

/**
 * Initialize EMV transaction and select application.
 *
 * Execute the following steps:
 * -# Recover the common list of AIDs from terminal and card;
 * -# Execute the final selection process;
 * -# Execute the GPO process;
 * -# Read card records.
 *
 * @param handle    EWL Handle
 * @param interface Terminal device interface to be used for this transaction
 *
 * @return See \ref ewlStatus_t.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlGetCard(ewlObject_t *handle, ewlInterfaceType_t interface);

/**
 * Execute card processing until first generate AC.
 *
 * Execute the following steps:
 * -# Data authentication.
 * -# Terminal risk management.
 * -# Processing restrictions
 * -# Cardholder Verification.
 * -# Terminal action analysis.
 * -# Card action analysis
 *
 * The results of all those steps will be stored as tags directly in \p handle.
 * Use \ref ewlGetParameter() to read their values.
 *
 * @param handle EWL Handle
 * @return See \ref ewlStatus_t.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlGoOnChip(ewlObject_t *handle);

/**
 * Finish Chip
 *
 * Execute the following steps:
 * -# Issuer Authentication;
 * -# Script Processing (0x71);
 * -# Completion;
 * -# Script Processing (0x72)
 *
 * @param handle EWL Handle
 * @return See \ref ewlStatus_t.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlFinishChip(ewlObject_t *handle);

/**
 * Destroy an EWL object.
 *
 * @param handle EWL Hand�le to be destroyed
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
void ewlDestroy(ewlObject_t *handle);

/**
 * Set the value of a parameter on an EWL instance.
 *
 * Each EWL instance has, associated with it, a collection of \e parameters
 * associated with all steps of a transaction.  Some of those parameters
 * are required by EMV, others are of internal use.
 *
 * Repeated calls with the same \p tag value overwrite the previous
 * values.
 *
 * This call does not check the contents of \p data, only store it on
 * the data dictionary associated with \p handle.
 *
 * @param handle    EWL Handle
 * @param tag       Tag ID of parameter to be set
 * @param data      Value of parameter
 * @param dataLen   Size of \p data in bytes
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetParameter(ewlObject_t *handle, ewlTag_t tag, const void *data, int dataLen);

/**
 * Read a parameter value from \p handle dictionary
 *
 * @param handle        EWL Handle
 * @param[out] data     Where to store the recovered parameter value
 * @param maxLen        Max number of bytes that can be written to \p data
 * @param tag           Tag ID of parameter to be read
 *
 * @return If >= 0, the actual number of bytes written to \p data
 * @return If < 0, error (see \ref ewlStatus_t)
 *
 * @note If you only need to retrieve the \e size of stored \p tag,
 *  call with <tt>maxLen = 0</tt> and <tt>data = NULL</tt>.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
int ewlGetParameter(ewlObject_t *handle, void *data, unsigned int maxLen, ewlTag_t tag);

/**
 * Set a \p dateTime_t parameter on \p handle
 *
 * Convert \p time to EMV BCD format (3-byte HHMMSS) and call \p ewlSetParameter() with \p tag.
 *
 * @param handle          EWL Handle
 * @param tag             Tag that will be set.
 * @param time            Pointer to time structure to store
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetParameterTime(ewlObject_t *handle, ewlTag_t tag, const dateTime_t *time);

/**
 * Set a \p dateDate_t parameter on \p handle
 *
 * Convert \p date to EMV BCD format (3-byte YYMMDD) and call \p ewlSetParameter() with \p tag.
 *
 * @param handle          EWL Handle
 * @param tag             Tag that will be set.
 * @param date            Pointer to date structure to store
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetParameterDate(ewlObject_t *handle, ewlTag_t tag, const dateDate_t *date);

/**
 * Set an amount parameter in EWL.
 *
 * The actual behavior depends on \p tag.
 *
 * For the <em>transaction amount</em> (authorized or other) tags (\c AG_EMV_AMOUNT_AUTH_BIN,
 * \c TAG_EMV_AMOUNT_AUTH_NUM, \c TAG_EMV_AMOUNT_OTHER_BIN or
 * \c TAG_EMV_AMOUNT_OTHER_NUM), what will be actually stored depends on the
 * absolute value of \p value: according to EMV rules, if <tt>value \> 0xFFFFFFFF</tt> then
 * stores the \c _NUM tag with value \c 0xFFFFFFFF and the actual value as BCD in the \c _BIN
 * tag.  Otherwise store \c value as given into both \c _NUM and \c _BIN tags.
 *
 * Certain tags (\c EWL_TAG_CLESS_TRANSACTION_LIMIT, \c EWL_TAG_CLESS_FLOOR_LIMIT, \c EWL_TAG_CLESS_CVM_LIMIT,
 * \c TAG_EMV_TERMINAL_FLOOR_LIMIT) are forcibly 32-bit, and if \p value cannot be represented
 * in 32-bits, an error is returned.
 *
 * For all other tags, store \p value in either 32 or 64-bits, the smallest size that fits.
 *
 * @param handle          Handle of object in use.
 * @param tag             Tag that will be set.
 * @param value           value that will be storage in EWL object.
 *
 * @return See \ref ewlStatus_t
 *
 * \note this function Cannot be used if the handle was created using ewlDirectCreate
 */
ewlStatus_t ewlSetParameterAmount(ewlObject_t *handle, ewlTag_t tag, uint64_t value);

/**
 * Check if the transaction can be done using defined interface.
 *
 * @param handle          Handle of object in use.
 * @param interface       Interface to be tested.

 * @return see \ref ewlStatus_t.
 *
 * \note This function can be used only after application set terminal AID list and transaction amount.
 *
 * \note this function Cannot be used if the handle was created using ewlDirectCreate
 *
 */
ewlStatus_t ewlTransactionAllowed(ewlObject_t *handle, ewlInterfaceType_t interface);

/* @} */

/** @addtogroup DirectChip Direct Chip API
 * @{
 *
 * The Direct Chip functions allow applications to execute direct APDU commands to
 * a Contact or Contact-less smart-card.
 *
 * An \ref ewlObject_t handle created with \ref ewlDirectCreate() cannot be used for
 * regular EMV processing, neither the opposite.  Even though the underlying data
 * structure handle is the same, this set of functions is mutually exclusive.
 */

/**
 * Create an EWL object where we can use APDU direct command.
 *
 * @param interface       Interface which will be used in direct commands.
 *
 * @return NULL           Cannot create a new  EWL environment.
 * @return Other          environment successfully created.
 */
ewlObject_t *ewlDirectCreate(ewlInterfaceType_t interface);

/**
 * Execute a smart-card <em>Power-On</em> command.
 *
 * @param handle          EWL Handle
 * @param out             Pointer to output buffer.
 * @param maxlen          Max length of output buffer.
 *
 * @return >= 0  length of ATR (contact) or UID (contact-less).
 * @return < 0   see \ref ewlStatus_t.
 *
 */
int ewlDirectPowerOn(ewlObject_t *handle, unsigned char *out, unsigned int maxlen);

/**
 * Execute a smartcard power off.
 *
 * @param handle          Handle of object in use.
 *
 * \note this function Cannot be used if the handle was created using ewlCreate
 */
void ewlDirectPowerOff(ewlObject_t *handle);

/**
 * Send an \e APDU command to smart-card.
 *
 * @param handle          Handle of object in use.
 * @param out             Smart-card answer.
 * @param maxOutLen       Max length expected as card answer.
 * @param in              Command to be send to smart-card.
 * @param inLen           Command length in bytes.
 *
 * @return >= 0  length of card answer.
 * @return < 0   see \ref ewlStatus_t.
 *
 * \note this function Cannot be used if the handle was created using ewlCreate
 */
int ewlDirectCommand(ewlObject_t *handle, unsigned char *out, unsigned int maxOutLen,
                     const unsigned char *in, unsigned int inLen );

/**
 * Close an EWL created using ewlDirectCreate.
 *
 * @param handle          Handle of object to be close.
 *
 * \note this function Cannot be used if the handle was created using ewlCreate
 */
void ewlDirectDestroy(ewlObject_t *handle);

/* @} */

/**
 * Add an element to the list of revoked keys.
 *
 * @param handle EWL Handle
 * @param data
 * @return
 */
ewlStatus_t ewlSetRevoked(ewlObject_t *handle, const ewlSetRevoked_t *data );

/* @} */

/**
 * Check if is necessary a second tap in contacless transaction.
 *
 * @param handle          Handle of object in use.;;
 *
 * \note this function must be call before after all parameters provided by the host are sent to the library.
 *
 */
ewlStatus_t ewlSecondTapCheck(ewlObject_t *handle );



/**
 * Set the value of a non EMV parameter on an EWL instance.
 *
 * Each EWL instance has, associated with it, a collection of \e arbitrary data
 * associated with all steps of a transaction. Those arbitrary parameters are
 * reserved for the application.
 *
 * Repeated calls with the same \p tag can not be done
 *
 * This call does not allowed set the same parameter several times for each context
 *
 * @param handle    EWL Handle
 * @param tag       Tag ID of parameter to be set
 * @param data      Value of parameter
 * @param dataLen   Size of \p data in bytes
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetNonEMVParameter(ewlObject_t *handle, ewlTag_t tag, const void *data, int dataLen);

/**
 * Cancel an EWL instance.
 *
 * Interrupt a EWL function during their execution.
 *
 * void ewlCancel(ewlObject_t *handle)
 *
 *
 * @param handle    EWL Handle
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 *            This function is not necessary in normal flow, use only in task situations.
 *
 */

void ewlCancel(ewlObject_t *handle);


/**
 * Reset an EWL instance.
 *
 * Reset a EWL flow without lost the setted parametes.
 *
 * ewlReset (ewlObject_t *handle, bool keepAID)
 *
 *
 * @param handle    EWL Handle
 * @param keepAID   If true keep parameter and aid list, if false keep only parameters
 *
 */

void ewlReset (ewlObject_t *handle, bool keepAID);

/* [1] This field can not be set as EWL_TECHNOLOGY_DISCOVER_ZIP or EWL_TECHNOLOGY_DISCOVER_MAG, the kernel will
       detect itself, in both case we need start as  EWL_TECHNOLOGY_DISCOVER_PAS*/
#endif
