/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			UILib
 * Header file:		UILib.h
 * Description:		Goal UILib functions
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_UILIB
#define _H_UILIB

/** Library version that this header file refers to */
#define UILIB_VERSION  "01.14"

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include <GL_GraphicLib.h>

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

typedef enum UILib_LINE
{
	UI_TITLE = 0x00,
	UI_LINE1 = 0x01,
	UI_LINE2 = 0x02,
	UI_LINE3 = 0x03,
	UI_LINE4 = 0x04,
	UI_LINE5 = 0x05,

}UI_LINE;

typedef enum UILib_ALIGN
{
	UI_ALIGN_CENTER = 0x00,
	UI_ALIGN_LEFT	= 0x01,
	UI_ALIGN_RIGHT  = 0x02,
} UI_ALIGN;

typedef enum UILib_Font
{
	UI_FONT_SMALL = 0x00,
	UI_FONT_LIGHT = 0x01,
	UI_FONT_NORMAL = 0x02,
	UI_FONT_DOUBLE = 0x03,
	UI_FONT_BIG = 0x04,

	UI_FONT_LIGHT_REVERSE = 0x05,
	UI_FONT_NORMAL_REVERSE = 0x06,
	UI_FONT_DOUBLE_REVERSE = 0x07,
	UI_FONT_SMALL_REVERSE = 0x08,
	UI_FONT_BIG_REVERSE = 0x09,
}UI_FONT;

typedef enum UILib_PrintMode
{
	UI_PRINT_MODE_LEGACY = 0x00,
	UI_PRINT_MODE_GOAL

}UI_PRINT_MODE;

extern T_GL_HGRAPHIC_LIB hGoal;

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	UI_Create_GOAL_Instance
 * Description:		Create GOAL instance if not created yet
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
void UI_Create_GOAL_Instance(void);

/* --------------------------------------------------------------------------
 * Function Name:	UI_Destroy_GOAL_Instance
 * Description:	Destroy GOAL instance
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
void UI_Destroy_GOAL_Instance(void);

/* --------------------------------------------------------------------------
 * Function Name:	UI_SetLine_Display
 * Description:		Set one or two text in a specific line in display structure
 * Parameters:
 *  - UI_LINE pLine - line to be set
 * 	- char *pText - text for one text or left text for two texts
 * 	- UI_ALIGN pAlign - Align for pText
 * 	- char *pTextRight - text for right text
 * Return:
 *  - none
 * Notes:
 * 	- Unattended screen size: 4*16.
 *
 */
void UI_SetLine(UI_LINE pLine, char *pText, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_SetLine_Two_Texts
 * Description:		Set two text in a specific line in display structure
 * Parameters:
 *  - UI_LINE pLine - line to be setted
 * 	- char *pText - text for one text or left text for two texts
 * 	- char *pTextRight - text for right text
 * Return:
 *  - none
 * Notes:
 * 	- Unattended screen size: 4*16.
 *
 */
void UI_SetLine_2Texts(UI_LINE pLine, char *pText, char *pTextRight);


/* --------------------------------------------------------------------------
* Function Name: 	UI_ClearLine_Display
* Description:		Clear text of specific line
* Parameters:
*  - UI_LINE pLine - line to be clear
* Return:
*  - none
* Notes:
*/
void UI_ClearLine(UI_LINE pLine);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ClearAllLines_Display
 * Description:		Clear all display lines
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
void UI_ClearAllLines(void);


/* --------------------------------------------------------------------------
 * Function Name:	UI_SetColors
 * Description:		Set colors por UILib
 * Parameters:
 *  - T_GL_COLOR pTitleForeColor - Fore color for title
 *  - T_GL_COLOR pTitleBackColor - Back color for title
 *  - T_GL_COLOR pLineForeColor - Fore color for lines
 *  - T_GL_COLOR pLineBackColor - Back color for lines
 *  - T_GL_COLOR pProcessForeColor - Fore color for process message
 *  - T_GL_COLOR pProcessBackColor - Back color for process message
 * Return:
 * Notes:
 */
void UI_SetColors(	T_GL_COLOR pTitleForeColor,
					T_GL_COLOR pTitleBackColor,
					T_GL_COLOR pLineForeColor,
					T_GL_COLOR pLineBackColor,
					T_GL_COLOR pProcessForeColor,
					T_GL_COLOR pProcessBackColor	);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowCleanMessage
 * Description:		Show message in screen without window style
 * Parameters:
 *  - unsigned int ptime - time in seconds
 * Return:
 *  - none
 * Notes:
 * 	- Unattended screen size: 4*16.
 */
void UI_ShowCleanMessage(unsigned int pTime);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowIdleImage
 * Description:		Show image for idle screen
 * Parameters:
 *  - char *pPath 	source of image. Example "file://flash/HOST/IMAGE.BMP"
 * Return:
 *  - none
 * Notes:
 */
void UI_ShowIdleImage(char *pSource);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowProcessScreen
 * Description:		Show process screen
 * Parameters:
 *  - char * pMessage - message to show
 * Return:
 *  - none
 * Notes:
 *  - UI_DIPS_CONFIG most be configured
 *
 * 	- Unattended screen size: 4*16, It shows:
 * 	   TITLE
 * 	   LINE2 (or LINE1, if LINE2 is empty)
 * 	   LINE3 (or LINE4, if LINE3 is empty)
 * 	   MESSAGE PROVIDED (AS FOOTER)
 * 	- AJAF, 20160404
 */
void UI_ShowProcessScreen(char * pMessage);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowInfoScreen
 * Description:		Show info screen
 * Parameters:
 *  - char * pMessage - message to show
 * Return:
 *  - none
 * Notes:
 *  - UI_DIPS_CONFIG most be configured
 *
 * 	- Unattended screen size: 4*16, It shows:
 * 	   TITLE
 * 	   LINE2 (or LINE1, if LINE2 is empty)
 * 	   LINE3 (or LINE4, if LINE3 is empty)
 * 	   MESSAGE PROVIDED (AS FOOTER)
 * 	- AJAF, 20160407
 */
void UI_ShowInfoScreen(char * pMessage);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowTextInput
 * Description:		perform text input
 * Parameters:
 *  - char *pBuffer
 *  - int pMin
 *  - int pMax
 *  - int pTimeOut
 *  - bool pNumeric
 *  - UI_ALIGN pAlign
 * Return:
 * Notes:
 */
int UI_ShowTextInput(char *pBuffer, int pMin, int pMax, int pTimeOut, bool pNumeric, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowPasswordInput
 * Description:		perform for password
 * Parameters:
 *  - char *pBuffer
 *  - int pMin
 *  - int pMax
 *  - int pTimeOut
 *  - bool pNumeric
 *  - UI_ALIGN pAlign
 * Return:
 * Notes:
 */
int UI_ShowPasswordInput(char *pBuffer, int pMin, int pMax, int pTimeOut, bool pNumeric, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowAmountInput
 * Description:		perform amount input
 * Parameters:
 *  - char *pBuffer - where to put the amount
 *  - int pMin - minimun length of input
 *  - int pMax - maximum length of input
 *  - char *pCurr - currency simbol
 *  - bool pbDec - true if amount is with 2 decimals, false if no decimals
 *  - int pTimeOut - time out in seconds
 *  - UI_ALIGN pAlign
 * Return:
 *  0 	- input OK
 *  -1	- input cancel or time out expired
 * Notes:
 */
int UI_ShowAmountInput(char *pBuffer, int pMin, int pMax, char *pCurr, bool pbDec, int pTimeOut, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowIPInput
 * Description:		prompt for IP
 * Parameters:
 *  - char *pBuffer - where to put data
 *  - int pTimeOut - time out
 *  - UI_ALIGN pAlign - text align of input
 * Return:
 * Notes:
 */
int UI_ShowIPInput(char *pBuffer, int pTimeOut, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowExpDateInput
 * Description:		perform expiration date input
 * Parameters:
 *  - char *pBuffer
 *  - int pTimeOut
 *  - UI_ALIGN pAlign
 * Return:
 *  0 	- input OK
 *  -1	- input cancel or time out expired
 * Notes:
 */
int UI_ShowExpDateInput(char *pBuffer, int pTimeOut, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowDateInput
 * Description:		perform date input
 * Parameters:
 *  - char *pBuffer
 *  - int pTimeOut
 *  - UI_ALIGN pAlign
 * Return:
 *  0 	- input OK
 *  -1	- input cancel or time out expired
 * Notes:
 */
int UI_ShowDateInput(char *pBuffer, int pTimeOut, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowHourInput
 * Description:		perform date input
 * Parameters:
 *  - char *pBuffer
 *  - int pTimeOut
 *  - UI_ALIGN pAlign
 * Return:
 *  0 	- input OK
 *  -1	- input cancel or time out expired
 * Notes:
 */
int UI_ShowHourInput(char *pBuffer, int pTimeOut, UI_ALIGN pAlign);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowMessage
 * Description:		show message
 * Parameters:
 *  - cahr *pTitle - title of message
 *  - char *pLine1 - line 1 of message
 *  - char *pLIne2 - line 2 of message
 *  - T_GL_DIALOG_ICON pIcon - icon to show
 *  - T_GL_BUTTONS pButtons - buttons to enable
 *  - int pTimeOut - time out
 * Return:
 * Notes:
 */
ulong UI_ShowMessage(char *pTitle, char *pLine1, char *pLIne2, T_GL_DIALOG_ICON pIcon, T_GL_BUTTONS pButtons, int pTimeOut);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowConfirmationScreen
 * Description:		Show process screen
 * Parameters:
 *  - char * pMessage - confirmation message
 *  - int pTimeOut - time out
 * Return:
 * - (0) -  Confirmation OK (Green Key)
 * - (-1) - cancel (Red Key)
 * - (-2) - time out
 * - (-3)	Error Handle, just for unattended
 * Notes:
 *  - UI_DIPS_CONFIG most be configured
 */
int UI_ShowConfirmationScreen(char * pMessage, int pTimeOut);

/* --------------------------------------------------------------------------
 * Function Name:	UI_MenuReset
 * Description:		Reset menu memory for a new menu
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
void UI_MenuReset(void);


/* --------------------------------------------------------------------------
 * Function Name:	UI_MenuAddItem
 * Description:		Add item to dynamic menu
 * Parameters:
 *  - char* pLabel - label to menu item in string (ends with /0)
 *  - int pValue - ret value if item is selected
 * Return:
 *  - 0 item added OK
 *  - -1 cannot add item
 * Notes:
 */
int UI_MenuAddItem(char* pLabel, int pValue);


/* --------------------------------------------------------------------------
 * Function Name:	UI_MenuRun
 * Description:
 * Parameters:
 * Return:
 *  0		user cancel or tiem out
 *  -1		no items in menu
 *  -2		cancel
 *  -3		time out
 * Notes:
 */
int UI_MenuRun(char *pTitle, int pTimeOut, int pSelectedValue);

/* --------------------------------------------------------------------------
 * Function Name:	UI_SetPrinterMode
 * Description:		Set printer mode(Legacy or GOAL)
 * Parameters:
 *  - UI_PRINT_MODE Mode: UI_PRINT_MODE_LEGACY or UI_PRINT_MODE_GOAL
 *  - other value -> Set GOAL by default
 * Return:
 * 	- Nothing
 * Notes:
 */
void UI_SetPrinterMode(UI_PRINT_MODE Mode);

/* --------------------------------------------------------------------------
 * Function Name:	UI_OpenPrinter
 * Description:		Open printer handle and load FONT_42.PGN
 * Parameters:
 *  - none
 * Return:
 * 	0	Printer opened and FONT_42 loaded
 * 	-1	Cannot open printer handle
 * 	-2	Cannot load FONT_42
 * Notes:
 */
int UI_OpenPrinter(void);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ClosePrinter
 * Description:		Close printer handle
 * Parameters:
 * Return:
 * Notes:
 */
void UI_ClosePrinter(void);


/* --------------------------------------------------------------------------
 * Function Name:	UI_PrintLineFeed
 * Description:		Print line feed
 * Parameters:
 *  - int lines - number of lines of feed
 * Return:
 *  0	lines printed OK
 *  -1	cannot print lines
 * Notes:
 */
int UI_PrintLineFeed(int lines);


/* --------------------------------------------------------------------------
 * Function Name:	UI_PrintLine
 * Description:		Print line
 * Parameters:
 *  - char *Line - line to be printed (up to 42 characters)
 * Return:
 *  0	line printed OK
 *  -1	cannot print line
 * Notes:
 */
int UI_PrintLine(char *Line, UI_FONT tFont, UI_ALIGN Aling );

/* --------------------------------------------------------------------------
 * Function Name:	UI_PrintSTLine
 * Description:		Print line with strike-through
 * Parameters:
 *  - char *Line - line to be printed (up to 42 characters)
 *  - UI_ALIGN Align - align of text
 * Return:
 *  0	line printed OK
 *  -1	cannot print line
 * Notes:
 */
int UI_PrintSTLine(char *Line, UI_FONT tFont, UI_ALIGN Align, char symbol );

/* --------------------------------------------------------------------------
 * Function Name:	UI_Print2Fields
 * Description:		Print 2 fields with title, first align to left and second
 * 					align to right
 * Parameters:
 *  - char *F1Title - title of field 1
 *  - char *F1Text - text of field 1
 *  - UI_FONT F1Font - font of field 1
 *  - char *F2Title - title of field 2
 *  - char *F2Text - text of field 2
 *  - UI_FONT F2Font - font of field 1
 * Return:
 *  0	line printed OK
 *  -1	cannot print line
 * Notes:
 */
int UI_Print2Fields(char *F1Title, char *F1Text, UI_FONT F1Font, char *F2Title, char *F2Text, UI_FONT F2Font);


/* --------------------------------------------------------------------------
 * Function Name:	UI_PrintImage
 * Description:		Print image
 * Parameters:
 *  - char *pSource - source of image, example: "file://flash/HOST/PRINTER.BMP"
 * Return:
 *  0	image printed OK
 *  -1	cannot print image
 * Notes:
 */
int UI_PrintImage(char *pSource);


/* --------------------------------------------------------------------------
 * Function Name:	UI_BackupAllLines
 * Description:		Backup all configured lines
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
void UI_BackupAllLines(void);


/* --------------------------------------------------------------------------
 * Function Name:	UI_RestoreAllLines
 * Description:		Restore all backup lines
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
void UI_RestoreAllLines(void);


/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowForcedAmountInput
 * Description:		perform forced amount input
 * Parameters:
 *  - char *pBuffer - where to put the amount
 *  - int pMin - minimun length of input
 *  - int pMax - maximum length of input
 *  - char *pCurr - currency simbol
 *  - bool pbDec - true if amount is with 2 decimals, false if no decimals
 *  - int pTimeOut - time out in seconds
 *  - UI_ALIGN pAlign
 * Return:
 *  0 	- input OK
 *  -1	- input cancel or time out expired
 * Notes:
 */
int UI_ShowForcedAmountInput(char *pBuffer, int pMin, int pMax, char *pCurr, bool pbDec, int pTimeOut, UI_ALIGN pAlign);

/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowIPInput
 * Description:		prompt for IP
 * Parameters:
 *  - char *Text - Text on left of input data
 *  			IGNORED IN UNATTENDED.
 *  - char *pBuffer - where to put data
 *  - int pTimeOut - time out
 *  - UI_ALIGN pAlign - text align of input
 *  - char *pMask - mask of input
 *  			IGNORED IN UNATTENDED.
 *  - char *pUserChar - char enable for input
 *  			FOR UNATTENDED, JUST SPECIFY NOT NUMERIC CHARS
 *  - T_GL_MODE mode - Mode of input control
 *  			IGNORED IN UNATTENDED.
 * Return:
 *  0 	IP entered OK
 *  -1 	cancel or time out expired
 *  -2	bad IP entered
 *
 *  NOTE: UNATTENDED ALWAYS WAIT FOR NUMERIC DATA.
 *
 * Notes:
 */
int UI_ShowGeneralInput(char *Text, char *pBuffer, int pTimeOut, UI_ALIGN pAlign, char strMask[], char strUserChars[], T_GL_MODE mode);

/* --------------------------------------------------------------------------
 * Function Name:	UI_SetDecimalSymbol
 * Description:		Set decimal and thousand symbol separator for input amount
 * Parameters:
 * - char DecimalSymbol: Decimal symbol. Example ','
 * - char ThousandSymbol: Thousand symbol. Example '.'
 * Return:
 * - nothing
 * Notes:
 */
void UI_SetDecimalSymbol(char DecimalSymbol, char ThousandSymbol);

/* --------------------------------------------------------------------------
 * Function Name:	UI_SetBeepDialogs
 * Description:		Allow enable(TRUE)/disable(FALSE) sound of message.
 * Author:			@jbotero
 * Parameters:
 * - Nothing
 * Return:
 * - Nothing
 * Notes:
 */
void UI_SetBeepDialogs(char IsBeep);

/* --------------------------------------------------------------------------
 * Function Name:	UI_SetMenuShowOneOption
 * Description:		When menu has only one option, allow show it or select by
 * 					default the only option.
 * Author:			@jbotero
 * Parameters:
 * - char IsBypass: (TRUE) When is only one option, select it by default
 * 					(FALSE) When is only one option, show menu with the option
 * Return:
 * - Nothing
 * Notes:
 * - UILib set by default Menu Bypass: Select by default the only one option
 * - API must be called after of UI_Create_GOAL_Instance to take
 * effect.
 * - Possible to change the beep mode in any moment
 */
void UI_SetMenuShowOneOption(char IsBypass);

/* --------------------------------------------------------------------------
 * Function Name:	UI_SetButtonMessages
 * Description:		Set messages on button OK & cancel for touch terminals
 * Author:			@jbotero
 * Parameters:
 * - char * pMsgOk: Message for OK button.Up 39 characters
 * - char * pMsgCancel: Message for Cancel button.Up 39 characters
 * Return:
 * - Nothing
 * Notes:
 * - UILib set by default message: OK("ACEPTAR") CANCEL("CANCELAR")
 * - API must be called after of UI_Create_GOAL_Instance to take
 * effect.
 * - Possible to change messages any moment
 */
void UI_SetButtonMessages(char * pMsgOk, char * pMsgCancel);

/* --------------------------------------------------------------------------
 * Function Name:	UI_BindManyKey
 * Description:		Allow map keys on terminal with different chars
 * Parameters:
 * - T_GL_WCHAR teliumKey: Example -> GL_KEY_DOT
 * - T_GL_WCHAR * keys: Example -> key_dot ->
 * 		T_GL_WCHAR key_dot[] = {'#','*','=',':','$','?',',','\'','"','\\','-',' ','+','%'}
 * - unsigned long count sizeof(key_dot)/sizeof(T_GL_WCHAR)
 *
 * Return: Nothing
 * Notes:
 *
 * Example: UI_BindManyKey(teliumKey, keys, count);
 *
 * Where:
 * - teliumKey -> Key to modify
 * - keys -> Array of values to allow the key when is pressed
 * - count ->Number of elements in key_dot array
 */
void UI_BindManyKey (T_GL_WCHAR teliumKey, T_GL_WCHAR * keys, unsigned long count);

/* --------------------------------------------------------------------------
 * Function Name:	UI_ShowImageMessage
 * Description:		Manage the screen for show multi line and image(Image + 3 lines)
 * Parameters:
 * - char * UrlImage: URL Image.
 * 		B/W Max. size (W/H): 128x20
 * 		COL Max. size (W/H): 320x70
 * - unsigned int pTime: Time of delay activity in seconds.
 * 		Example:
 * 			- pTime = 0 ( No delay )
 * 			- pTime = 1 ( 1 second )
 * -char ImgPosition: Image position GL_ALIGN_TOP(Text below) or GL_ALIGN_BOTTOM(Text above)
 * -char ShowHeader: Flag 	(1) show header
 * 							(2) Hide header
 * 							(-1) Last value
 * -char ShowFooter: Flag 	(1) show  Footer
 * 							(2) show Footer
 * 							(-1) Last value
 * Return:
 *  - true(key valid)
 *  - false(Key cancel or timeout)
 * Notes:
 * Use UI_SetLine for message. Allow title(UI_TITLE) and 3 lines(UI_LINE1, UI_LINE2, UI_LINE3)
 * Is UI_TITLE is empty, don't show title
 */
void UI_ShowImageMessage( char * UrlImage, unsigned int pTime, char ImgPosition, char ShowHeader, char ShowFooter );

/* ***************************************************************************
 * UNATTENDED PROTOTYPES
 * ***************************************************************************/

//IDLE FORMAT STRUCTURE
typedef struct {
	char	*strTextIdle;
	char	*BMPFile;		/* MONOCROMATIC MAX SIZE: 127 *63 px (X*Y)*/
	int		iBMPReverse;
	int		iHeaderStatus;
}IUN_IDLE_FORMAT;

//KEYBOARD EVENT CALLBACK
typedef int (*UI_KeyboardEventIUN_Callback)(char );


/**
 * Activates the Keyboard event for iUN
 *
 * @param Callback_KeyboardEventIUN
 * @return
 *
 * Sample of Use:
int after_reset(NO_SEGMENT no, void *p1, S_TRANSOUT *param_out)
{
 	 ...
 	 ...
	 if(iUN_isIUN()) {
		 IUN_IDLE_FORMAT iUNIdleFormat;

		 iUNIdleFormat.BMPFile = "/HOST/TBKBG.BMP";
		 iUNIdleFormat.iBMPReverse = _OFF_;
		 iUNIdleFormat.iHeaderStatus = _ON_;
		 iUNIdleFormat.strTextIdle =	"TRANSBANK\n"
		 	 	 						"Unattended";

		 UI_ServiceRegisterIUN();
		 UI_activateIdleIUN(&iUNIdleFormat);
		 UI_activateKeyboardEventIUN((UI_KeyboardEventIUN_Callback) IUN_Keyboard_event);
	 }
 */
extern int iUN_activateKeyboardEvent(UI_KeyboardEventIUN_Callback Callback_KeyboardEventIUN);

/**
 * Registers service for unattended
 *
 * Sample of Use:
int after_reset(NO_SEGMENT no, void *p1, S_TRANSOUT *param_out)
{
 	 ...
 	 ...
	 if(iUN_isIUN()) {
		 IUN_IDLE_FORMAT iUNIdleFormat;

		 iUNIdleFormat.BMPFile = "/HOST/TBKBG.BMP";
		 iUNIdleFormat.iBMPReverse = _OFF_;
		 iUNIdleFormat.iHeaderStatus = _ON_;
		 iUNIdleFormat.strTextIdle =	"TRANSBANK\n"
		 	 	 						"Unattended";

		 UI_ServiceRegisterIUN();
		 iUN_setIdle(&iUNIdleFormat);
	 }
}
 */
extern void iUN_ServiceRegister(void);

/**
 * Verifies if its a Unattended
 * @return
 */
extern bool iUN_isIUN(void);

/**
 * Sets the Idle message to use.
 *
 * USE ONLY WHEN IDLE_MESSAGE manager entry point works on unattended
 *
 * @param iUNIdleFormat
			char *strTextIdle:		Used if BMP is NULL
			char *BMPFile;			BMP Path file		MONOCROMATIC MAX SIZE: 127 *63 px (X*Y)
			int	iBMPReverse			_ON_ (activation) or _OFF_ (disactivation)
			int iHeaderStatus		_ON_ (activation) or _OFF_ (disactivation)
 *
 * @return
 *
 * Sample of Use:
int idle_message(NO_SEGMENT no, void *p1, void *p2)
{
	 if(iUN_isIUN()) {
		 IUN_IDLE_FORMAT iUNIdleFormat;

		 iUNIdleFormat.BMPFile = "/HOST/TBKBG.BMP";
		 iUNIdleFormat.iBMPReverse = _OFF_;
		 iUNIdleFormat.iHeaderStatus = _ON_;
		 iUNIdleFormat.strTextIdle =	"TRANSBANK\n"
		 	 	 						"Unattended";

		 iUN_setIdle(&iUNIdleFormat);
	 } else {
		 char BMPColorFile[] = "/HOST/CO_EVERT.BMP";

		 UI_ShowIdleImage(BMPColorFile);
	 }
}
 *
 */
extern int iUN_setIdle(IUN_IDLE_FORMAT *iUNIdleFormat);

/**
 * Activates the Idle message
 * @return
 *
 * Sample of Use:
int after_reset(NO_SEGMENT no, void *p1, S_TRANSOUT *param_out)
{
 	 ...
 	 ...
	 if(iUN_isIUN()) {
		 IUN_IDLE_FORMAT iUNIdleFormat;

		 iUNIdleFormat.BMPFile = "/HOST/TBKBG.BMP";
		 iUNIdleFormat.iBMPReverse = _OFF_;
		 iUNIdleFormat.iHeaderStatus = _ON_;
		 iUNIdleFormat.strTextIdle =	"TRANSBANK\n"
		 	 	 						"Unattended";

		 UI_ServiceRegisterIUN();
		 iUN_setIdle(&iUNIdleFormat);
	 }
}
 */
extern int iUN_activateIdle(IUN_IDLE_FORMAT *iUNIdleFormat);



#define UI_activateKeyboardEventIUN	iUN_activateKeyboardEvent
#define UI_ServiceRegisterIUN		iUN_ServiceRegister
#define UI_isIUN					iUN_isIUN
#define UI_setIdleIUN				iUN_setIdle
#define UI_activateIdleIUN			iUN_activateIdle


extern void UI_BasicWindow(void);

#endif //_H_UILIB
