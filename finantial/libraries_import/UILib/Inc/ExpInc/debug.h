#ifndef _H_UILIB_DEBUG_
#define _H_UILIB_DEBUG_

#define _DEBUG_CONSOLE		1
#define _DEBUG_TRACE 		2
#define _DEBUG_FILE 		3

/** \c larlib.log channel list */
enum STMLogChannels_t {
    STM_LOG_STEPS              = 240,
	LOG_FS_CUSTOM			   = 241,
	LOG_DEV_MSG				   = 242
};

#define ERR_FS_NOT_FOUND -3
#define ERR_MSG_LENGTH -1
#define ERR_MSG_FAILED -2
#define MSG_DEBUG_OK 0

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	debugControluiLib
 * Description:		sets de output for the debuggin process
 * Parameters:
 *  - int outputPath -Posible outputs for debugging (_DEBUG_CONSOLE,_DEBUG_FILE, _DEBUG_TRACE)
 * Return:
 * -  none
 */
void debugControluiLib (int outputPath);


/* --------------------------------------------------------------------------
 * Function Name:	resetFileDebug
 * Description:		resets the file used for debug
 * Parameters:
 *  - none
 * Return:
 * - FS_OK - if successful .
 * - FS_KO - if error .
 * - FS_NOACCESS - if the application does not have the access rights.
 * - FS_FILEUSED - if the file is being used.
 * - ERR_FS_NOT_FOUND - if the file does not exist.
 */
int resetFileDebug (void);

/* --------------------------------------------------------------------------
 * Function Name:	messageDebug
 * Description:		shows a log message defined in the parameters in the debug channel
 * Parameters:
 *  - const char *msg - message to be displayed
 * Return:
 *  - MSG_DEBUG_OK - if the message is shown in the log
 *  - ERR_MSG_FAILED - if the message failed to show in the log
 *  - ERR_MSG_LENGTH - if the message was bigger than the max value
 */
int messageDebug(const char *msg);

/* --------------------------------------------------------------------------
 * Function Name:	modifyTraceIP
 * Description:		modifies the ip and port of the TRACE_IP.TXT used for traces on tetra
 * Parameters:
 *  - const char * ipTrace -value with the ip and port in one char array
 * Return:
 * -  none
 */
void modifyTraceIP(const char * ipTrace);

#endif
