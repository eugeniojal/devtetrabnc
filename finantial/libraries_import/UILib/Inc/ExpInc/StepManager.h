/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		StepManager.h
 * Description:		Funtions for step manger
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_STEPMANAGER
#define _H_STEPMANAGER

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define INPUT_NONE			0x00	// No input
#define INPUT_AMOUNT		0x01	// Amount (currency optional)
#define INPUT_AMOUNT_WOD	0x02	// Amount without decimals (currency optional)
#define	INPUT_TEXT			0x03	// Alpha numeric
#define INPUT_NUMERIC_TEXT	0x04	// Numeric
#define INPUT_PASSWORD		0x05	// Numeric password
#define INPUT_PASSWORD_AN	0x06	// Alpha numeric password
#define INPUT_IP			0x07	// IP address
#define INPUT_EXPDATE		0x08	// Expiration date MM/YY
#define INPUT_DATE			0x09	// Date YYYY/MM/DD
#define INPUT_HOUR			0x10 	// HH:MM:SS
#define INPUT_NUM_DEC		0x12
//+ FORCED
#define INPUT_AMOUNT_FORCED 0x13	// forced amount entry
//- FORCED


typedef struct SMT_Step
{
	unsigned short Step_Index;
	char Step_InputType;
	short Step_Min;
	short Step_Max;
	short Step_TimeOut;
	void *Step_pResult;
	bool (*Step_PreFunc)(void);
	int (*Step_PostFunc)(char *);
}SMT_STEP;

// Return function
#define STM_RET_OK			0
#define STM_RET_ERROR		-1
#define STM_RET_CANCEL		-2
#define STM_RET_NEXT		-3	// go to next step
#define STM_RET_AGAIN		-4	// repeat step
#define PANEL_CONTROL		14
#define L_buffer	200

#define PIN_DES			0
#define PIN_TDES		1
#define PIN_DUKPT_DES	2
#define PIN_DUKPT_TDES	3


/* ***************************************************************************
 * VARIABLES
 * ***************************************************************************/
extern char Step_Buffer[L_buffer];


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int STM_RunTransaction(unsigned short pStepList[], SMT_STEP Step_List[]);


/* --------------------------------------------------------------------------
 * Function Name:	STM_SetCurrency
 * Description:		Set currency symbol for AMOUNT inputs
 * Parameters:
 *  - char *Curr	Pointer to currency symbol
 * Return:
 *  - none
 * Notes:
 */
void STM_SetCurrency(char *Curr);


/* --------------------------------------------------------------------------
 * Function Name:	STM_CustomInputFields
 * Description:		Customize input parameters for current step
 * Parameters:
 *  - short Min - new minimum (0 to use default)
 *  - short Max - new maximum (0 to use default)
 *  - short TimeOut - new time out (0 to use default)
 *  - char - input type (-1 to use default)
 * Return:
 *  - node
 * Notes:
 */
void STM_CustomInputFields(short Min, short Max, short TimeOut, char Input);

#endif //_H_STEPMANAGER
