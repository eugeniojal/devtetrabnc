#ifndef TELIUM_PARAMS_H
#define TELIUM_PARAMS_H

/** Library version that this header file refers to */
#define TPARAMS_VERSION  "01.01"

typedef enum {
	PARAMS_OK,
	PARAMS_ERROR_TAG_FORMAT,
	PARAMS_ERROR_FILE_NOT_FOUND,
	PARAMS_ERROR_WRONG_PARAMS_DATA,
	PARAMS_ERROR_TLVTREE,
}PARAMS_Status;


typedef struct{
	char* Tag;
	char* Value;
	int length;
}Node;

typedef struct{
	Node* parent;
	Node** childs;
}param_node;



int PARAMS_OpenTLVTree(char* ParamFile, TLV_TREE_NODE ParamsTree) ;

int PARAMS_OpenBERTLV(char* ParamFile, char* TlvBuffer, int* nSize);


#endif
