/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			Cyrpto Lib
 * Header file:		base64.h
* Description:
 * Author:			aambrosio 20180208
 * --------------------------------------------------------------------------*/
#ifndef _H_BASE64
#define _H_BASE64

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/


/* ***************************************************************************
 * TABLE: GENERAL CONFIGURATION
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/**
 * @brief Encode data using base 64 format
 * @param Src data source pointer
 * @param Len data length
 * @param OutLen output data length
 * @return output data pointer on success, NULL otherwise
 */
unsigned char * EncodeBase64(const unsigned char * const Src, const size_t Len,
	size_t * const OutLen);

/**
 * @brief Decode data using base 64 format
 * @param Src data source pointer
 * @param Len data source length
 * @param OutLen output data length
 * @return output data pointer on success, NULL otherwise
 */
unsigned char * DecodeBase64(const unsigned char * const Src, const size_t Len,
	size_t * const OutLen);


/**
 * Get the RSA Public Key data from the specified buffer.
 * + Buffer must come from PEM file
 *
 * @param pRSAPublicKeyBuffer
 * @param iBufferSize
 * @param stRSAPK
 * @param iIndexUsed
			0	"-----BEGIN RSA PUBLIC KEY-----",
			1	"-----BEGIN PUBLIC KEY-----"
 * @return
 * 		-10		Input data error.
 * 		-20		Error decoding base 64.
 *	  	-31		Input data error.
 *	  	-32		Error getting SEQUENCE
 *	  	-33		Error getting MODULUS
 *	  	-34		Error getting EXPONENT
 *
 *	@AJAF, February 2018
 */
int getRSAPublicKeyFromBuffer (uchar *pRSAPublicKeyBuffer, size_t iBufferSize, RsaPublicKey_ST *stRSAPK, int iIndexUsed);

/**
 * Get the RSA Public Key data from the specified file.
	char strRSAHeader[][30+1] = {
			"-----BEGIN RSA PUBLIC KEY-----"
			,"-----BEGIN PUBLIC KEY-----"
			,"-----BEGIN CERTIFICATE-----"
	};
	char strRSAFooter[][30+1] = {
			"-----END RSA PUBLIC KEY-----"
			,"-----END PUBLIC KEY-----"
			,"-----END CERTIFICATE-----"
	};
 *
 * @param strFileName	PEM file
 * @param stRSAPK
 * @return
 * 		OK(0)	if success.
 * 		-100		Error reading file
 * 		-201		Header error
 * 		-202		Footer error
 * 		-310		Input data error.
 * 		-320		Error decoding base 64.
 *	  	-331		Input data error.
 *	  	-332		Error getting SEQUENCE
 *	  	-333		Error getting MODULUS
 *	  	-334		Error getting EXPONENT
 *
 *	@AJAF, February 2018
 */
int getRSAPublicKey_File (char *strFileName, RsaPublicKey_ST *stRSAPK);

#endif //_H_BASE64
