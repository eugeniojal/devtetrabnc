/*
 * Rsa.h
 *
 *  Created on: 05/05/2015
 *      Author: squijano
 */

typedef unsigned char         uint8;
typedef unsigned short        uint16;
typedef unsigned long         uint32;
typedef signed char           int8;
typedef signed short          int16;
typedef signed long           int32;

#ifndef RSA_H_
#define RSA_H_


/***** ERROR CODES ******/
#define	RSA_RET_OK				0
#define	RSA_RET_ERRSIGNAL		-1
#define	RSA_RET_INVALIDKEY		-2
//#define	RSA_RET_INVALIDPADDING	-3

typedef struct
{
	uint16 u16Bits; /* length in bits of modulus */
	uint8 *pu8Modulus; /* modulus */
	uint32 u32Exponent; /* public exponent */
} RsaPublicKey_ST;

typedef struct
{
	uint16 u16Bits; /* length in bits of modulus */
	uint8 *pu8Exponent; /* private exponent */
	uint8 *pu8Prime[2]; /* prime factors */
} RsaPrivateKey_ST;

typedef struct
{
	uint8 *pu8Block;
	uint32 u32Length;
	bool bEncodePKCS1;	//TRUE	For encoding data with PKCS1 v1.5, default RSA data format before ciphering.
						//FALSE	Block data must be formated by the application.
	uchar paddingChar;	//Padding char to use, if NULL, 0xFF padding will be used
} RsaData_ST;

typedef struct
{
	uint8 *pu8ResultBlock;
	uint32 u32Length;
	bool bBase64Enconded;
} RsaBlockResp_ST;


/**
 * Set the Maximum RSA modulus size in BITS.
 * + If this function is not called, the default size is MAX_RSA_MODULUS_BITS.
 * + This value is not saved in any batch, its saved in RAM memory.
 * + if MY_MAX_RSA_MODULUS_BITS must be changed, this function must be called at least
 * 		once while the POS is turned on.
 *
 * @param newMAXRSAModulus
 */
void setMAXRSAModulusSize(unsigned int newMAXRSAModulus);

/**
 * Get the Maximum RSA modulus size in BITS.
 *
 * @param newMAXRSAModulus
 */
unsigned int getMAXRSAModulusSize(void);

/**
 * Raw RSA public-key operation.
 *
 * @param resp
 * 		Output has same length as modulus.
 * @param data
 * 		Data must have the same length or less than modulus.
 * 		Data must be already padded.
 * @param key
 * @return
 * 		RSA_RET_OK
 * 		RSA_RET_ERRSIGNAL
 * 		RSA_RET_INVALIDKEY
 * 		-4	Error enconding base 64
 *
 */
int16 RsaPublicBlock ( RsaBlockResp_ST *resp, RsaData_ST *data, RsaPublicKey_ST *key );

/**
 * Raw RSA private-key operation. Output has same length as modulus.

 *
 * @param resp
 * @param data
 	 	Requires input < modulus.
		Assumes inputLen < length of modulus.
 * @param key
 * @return
 * 		RSA_RET_OK
 * 		RSA_RET_ERRSIGNAL
 * 		RSA_RET_INVALIDKEY
 *
 */
int16 RsaPrivateBlock ( RsaBlockResp_ST *resp, RsaData_ST *data, RsaPrivateKey_ST *key );

/**
 * Raw RSA public-key operation.
	char strRSAHeader[] = "-----BEGIN RSA PUBLIC KEY-----";
	char strRSAFooter[] = "-----END RSA PUBLIC KEY-----";
 *
 * @param resp
 * 		Output has same length as modulus.
 * @param data
 * 		Data must have the same length or less than modulus.
 * 		Data must be already padded.
 * @param strFileName	PEM file
 * @return
 * 		RSA_RET_OK
 * 		RSA_RET_ERRSIGNAL
 * 		RSA_RET_INVALIDKEY
 * 		-4			Error enconding base 64
 * 		-100		Error reading file
 * 		-201		Header error
 * 		-202		Footer error
 * 		-310		Input data error.
 * 		-320		Error decoding base 64.
 *	  	-331		Input data error.
 *	  	-332		Error getting SEQUENCE
 *	  	-333		Error getting MODULUS
 *	  	-334		Error getting EXPONENT
 *
 */
int RsaPublicBlockFromFile ( RsaBlockResp_ST *resp, RsaData_ST *data, char *strFileName );

#endif /* RSA_H_ */
