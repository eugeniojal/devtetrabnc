/***
 * SEC_KeyManager.h
* This module contains the functions that perform the encipherment of the PIN code,
* for online presentation.
 *
 * Manage the Keys:
 * - Load WK.
 * - Generate the PINBLOCK
 * - Call GOAL to ask for PIN.
 *
 * CARCAAN
 * Antonio Ambrosio
 * September 2013
 *
 * History
 * 2015-02		mamata			use Antonio Ambrosio source codes to create the CrytoLib
 * 2015-04		jbotero &		add support to data encryption and mac calculation
 * 				carodriguez
 *
 *
 *
 */

/* README
 *
 *
 * Recommendations to use this module:
 * - Use the following Schemes:
 *   1. TlvLoadKey.sgn
 *   2. TlvKeyVerif.sgn
 * - Sign these schemes with the client profile
 * - Add these schemes to project
 * - Sign the application ussing (including) these schemes.
 *
 *
 */
#define CRYPTO_VERSION  "02.04"

#include "SEC_interface.h"
#include "tlvVar_def.h"

/*+************* #DEFINES **************************************************+*/

/*+************* STRUCTURES ************************************************+*/

typedef enum EKeyType
{
	SKM_PIN_KEY,
	SKM_DATA_KEY,
}CRYPTOLIB_KEY_TYPE;

typedef enum EOperationMode
{
	SKM_ECB,
	SKM_CBC,
	SKM_CFB,
}CRYPTOLIB_OP_MODE;

typedef enum EPinBlockFormat
{
	FORMAT_F0,
	FORMAT_F0_8,
	FORMAT_F1,
	FORMAT_F2,
	FORMAT_F3,
	FORMAT_F3_8,

}CRYPTOLIB_PIN_BLOCK_FORMAT;


/*+************* STRUCTURES ************************************************+*/

typedef struct
{
	unsigned char				PinBlock[9];				// OUT: pin block
	unsigned char				DUKPT_KSN[20];				// OUT: DUKPT KSN
	char						Title[25];
	char						Line1[25];					// pin entry - line 1
	char						Line2[25];					// pin entry - line 2
	char						PPmsgIdle1[25];				// pin entry - line 1
	char						PPmsgIdle2[25];				// pin entry - line 2
	int							iFirstCharTimeOut;			// First char timeout, SECONDS
	int							iInterCharTimeOut;			// Inter char timeout, SECONDS
	unsigned char				PAN[25];					// card number
	unsigned char				EncryptedWorkingKey[24];	// encrypted working eky
	unsigned char				MinDigits;					// Min number of PIN digits to entry
	unsigned char				MaxDigits;					// Max number of PIN digits to entry
	unsigned char				OptionsLine2ForPin;			//Flag for external Pinpad like IPP220 with 2 lines
																//---(0)Line 1 without modifications.Line 2, concatenate with Entry PIN. Example "INGRESE PIN:" -> "INGRESE PIN:****"
																//---(1)Line 1 without modifications.Line 2, Overwrite  with entry PIN. Example "INGRESE PIN:" -> "****"
																//---(2)Scroll. When enter PIN, pass message of Line 2 to Line 1, and line 2 show only PIN.

}CRYPTOLIB_PIN_GENERAL_PARAMS;

typedef struct _STM_PinEntry
{
	CRYPTOLIB_PIN_GENERAL_PARAMS	*PINGeneralParams;
	CRYPTOLIB_PIN_BLOCK_FORMAT		PinBlockFormat;
	T_SEC_DATAKEY_ID				stKeyInfo;
	T_SEC_DATAKEY_ID				stMasterKeyInfo;
}CRYPTOLIB_PIN_PARAMETERS;


typedef struct
{
	unsigned char		MacResult[8];				// Pointer to the output data buffer
	unsigned char		*pInputData;				// Pointer to the input data buffer
	int					LenInputData;				// Length of the input buffer
	unsigned char		EncryptedWorkingKey[24];	// Pointer to the working key buffer
	unsigned char		*pInitialValue;
} CRYPTOLIB_MAC_GENERAL_PARAMS;

typedef struct STM_MAC
{
	CRYPTOLIB_MAC_GENERAL_PARAMS	*MACGeneralParams;
	T_SEC_DATAKEY_ID				stKeyInfo;
	T_SEC_DATAKEY_ID				stMasterKeyInfo;
} CRYPTOLIB_MAC_PARAMETERS;


typedef struct
{
	unsigned char		*pOutputData;				// Pointer to the output data buffer
	unsigned int		LenOutputData;				// Length of the output buffer
	unsigned char		*pInputData;				// Pointer to the input data buffer
	int					LenInputData;				// Length of the input buffer
	unsigned char		EncryptedWorkingKey[24];	// Pointer to the working key buffer
	unsigned char		*pInitialValue;
	T_SEC_OPER_FUNCT	iOper;
	CRYPTOLIB_OP_MODE	OperationMode;
	unsigned char		DUKPT_KSN[10];
	int 				DUKPT_KeyAdvance;			// Automatic (C_EXTDUKPT_AUTO_MODE) or forced (C_EXTDUKPT_FORCED_MODE) mode
} CRYPTOLIB_DATA_GENERAL_PARAMS;

typedef struct STM_DATA
{
	CRYPTOLIB_DATA_GENERAL_PARAMS	*DATAGeneralParams;
	T_SEC_DATAKEY_ID				stKeyInfo;
	T_SEC_DATAKEY_ID				stMasterKeyInfo;
} CRYPTOLIB_DATA_PARAMETERS;

typedef struct STM_KEYLIST
{
	T_SEC_DATAKEY_ID				stKeyInfo;

} CRYPTOLIB_KEYINFO;

/*+************* VARIABLES *************************************************+*/

/*+************* PUBLIC FUNCTION PROTOTYPES ***************************************+*/

/* --------------------------------------------------------------------------
 * Function Name:	CryptoLib_Init
 * Description:		initializes the Crypto library. needs to called from after reset only once per app
 * Author:			lcalvano
 * Parameters:
 * Return:
 * Notes:
 */
int CryptoLib_Init(void);

/** --------------------------------------------------------------
 * Functcion Name:	CryptoLib_FreeKey
 * Autor:           @gsanabria
 * Date:            Aug 26, 2015
 * Description:		Borra una llave se probo con Master Sesion
 * Parameters:
 * @param stpKeyInfo
 * @param KEYType
 * Return:
 * - none
 * Notes:
 */
int CryptoLib_FreeKey(T_SEC_DATAKEY_ID *stpKeyInfo, CRYPTOLIB_KEY_TYPE KeyType);

/** --------------------------------------------------------------------------
 * Function Name:	CryptoLib_IsMKInjected
 * Description:		Checks if key is injected at specific position
 * Author:			@carodriguez 03/09/2015
 * Parameters:
 * @param stpMasterKeyInfo
 * @param strKCV, KCV returned if the key is loaded
 * @param KEYType
 * @return
 * Notes:
 */
int CryptoLib_IsKeyInjected(T_SEC_DATAKEY_ID *stpMasterKeyInfo, uchar *strKCV, CRYPTOLIB_KEY_TYPE KeyType);

/**
 * Ask for the PIN ONLINE
 * - Inject the WK.
 * - Ask for the PIN
 * - Generate the PINBLOCK
 *
 * @param PINEntryParams
 *
 * The following two parameters are just necessary if no EWK is loaded.
 * @param strEWK,	Working Key which will be used to encrypt the PINBLOCK. NULL if EWK is already loaded.
 * @param MKIndex.	Index of Scheme mode for MK
 * @return
 */
int CryptoLib_ShowPINEntry(CRYPTOLIB_PIN_PARAMETERS *PINEntryParams);

/**
 * Compute the MAC
 * - Inject the WK if master session key type.
 * - Compute MAC
 * - Generate MAC result
 *
 * @param MacEntryParams
 * @return
 */
int CryptoLib_ComputeMAC(CRYPTOLIB_MAC_PARAMETERS *MacEntryParams);

/**
 * Cyphers Data
 * - Inject the WK if key is master session.
 * - Compute cyphering
 * - Generate cyphering result
 *
 * @param DataEntryParams
 * @return
 */
int CryptoLib_DataEncryption( CRYPTOLIB_DATA_PARAMETERS *DataEntryParams);

/**
 * Check if terminal is on ALERT INTERUPTION or UNAUTHORIZED.
 * If is the case, delete all application on terminal
 *
 * @param Nothing
 * @return RET_OK
 *
 * Note: Terminal reboot if tamper detection was detected
 */
int CryptoLib_VerifyBoosterState(void);

/**
 * List all keys injected on terminal
 *
 * @param Tree created with root 0. If return == 0:
 * - First level  -> Root = tag(0)
 * - Second level -> Secret Area = SecretAreaId -> Parent is root
 * - Third level -> All Key info = tag(0 to n) -> Parent is Secret Area
 * 		Data of each key tag contain struct T_SEC_DATAKEY_ID
 * - Fourth level -> KCV of each Key = tag(0) -> Parent is KeyInfo
 * 		Data of each KCV tag contain string with KCV value 3 bytes Hex format
 * 		(KCV is no present for DUKPT keys)
 * @param bool bAlsoWK: Flag to add working keys also
 * - TRUE: Add working keys
 * - FALSE: Ignore
 *
 * @return
 * 	0: OK
 * 	-1: Fail internal memory
 * 	-2: Fail SEC_listSecureId API, check scheme
 * 	-3: Fail SEC_listTLVKeys API, check scheme
 *	-4: Fail SEC_KeyVerify API, check scheme
 *
 *	Note:
 *	- Open schemes library before to use API
 *	- Use schemes STLList_Tlv, TlvKeyVerif
 */
int CryptoLib_ListAllKeys(TLV_TREE_NODE TreeKeys, bool bAlsoWK);
