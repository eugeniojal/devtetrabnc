/***
 * SEC_KeyManager.h
* This module contains the functions that perform the encipherment of the PIN code,
* for online presentation.
 *
 * Manage the Keys:
 * - Load WK.
 * - Generate the PINBLOCK
 * - Call GOAL to ask for PIN.
 *
 * CARCAAN
 * Antonio Ambrosio
 * September 2013
 *
 * History
 * 2015-02		mamata			use Antonio Ambrosio source codes to create the CrytoLib
 * 2015-04		jbotero &		add support to data encryption and mac calculation
 * 				carodriguez
 *
 *
 *
 */

/* README
 *
 *
 * Recommendations to use this module:
 * - Use the following Schemes:
 *   1. TlvLoadKey.sgn
 *   2. TlvKeyVerif.sgn
 * - Sign these schemes with the client profile
 * - Add these schemes to project
 * - Sign the application ussing (including) these schemes.
 *
 *
 */


#include "SEC_interface.h"

/*+************* #DEFINES **************************************************+*/
//+ PIN_BYPASS @damaro Oct 10, 2019 Added Pin-bypass support
#define CRYPTOLIB_USER_BY_PASS -4
//- PIN_BYPASS

/*+************* STRUCTURES ************************************************+*/

typedef enum EKeyType
{
	SKM_PIN_KEY,
	SKM_DATA_KEY,
}CRYPTOLIB_KEY_TYPE;

typedef enum EOperationMode
{
	SKM_ECB,
	SKM_CBC,
}CRYPTOLIB_OP_MODE;

typedef enum EPinBlockFormat
{
	FORMAT_F0,
	FORMAT_F0_8,
	FORMAT_F1,
	FORMAT_F2,
	FORMAT_F3,
	FORMAT_F3_8,

}CRYPTOLIB_PIN_BLOCK_FORMAT;


/*+************* STRUCTURES ************************************************+*/

typedef struct
{
	unsigned char				PinBlock[9];				// OUT: pin block
	unsigned char				DUKPT_KSN[20];				// OUT: DUKPT KSN
	char						Title[25];
	char						Line1[25];					// pin entry - line 1
	char						Line2[25];					// pin entry - line 2
	char						PPmsgIdle1[25];				// pin entry - line 1
	char						PPmsgIdle2[25];				// pin entry - line 2
	int							iFirstCharTimeOut;			// First char timeout, SECONDS
	int							iInterCharTimeOut;			// Inter char timeout, SECONDS
	unsigned char				PAN[25];					// card number
	unsigned char				EncryptedWorkingKey[24];	// encrypted working eky
	unsigned char				MinDigits;					// Min number of PIN digits to entry
	unsigned char				MaxDigits;					// Max number of PIN digits to entry
	//+ PIN_BYPASS @damaro Oct 11, 2019 Added Pin-bypass support
	bool						isPinBypassSupported;
	//- PIN_BYPASS
}CRYPTOLIB_PIN_GENERAL_PARAMS;

typedef struct _STM_PinEntry
{
	CRYPTOLIB_PIN_GENERAL_PARAMS	*PINGeneralParams;
	CRYPTOLIB_PIN_BLOCK_FORMAT		PinBlockFormat;
	T_SEC_DATAKEY_ID				stKeyInfo;
	T_SEC_DATAKEY_ID				stMasterKeyInfo;
}CRYPTOLIB_PIN_PARAMETERS;


typedef struct
{
	unsigned char		MacResult[8];				// Pointer to the output data buffer
	unsigned char		*pInputData;				// Pointer to the input data buffer
	int					LenInputData;				// Length of the input buffer
	unsigned char		EncryptedWorkingKey[24];	// Pointer to the working key buffer
	unsigned char		*pInitialValue;
} CRYPTOLIB_MAC_GENERAL_PARAMS;

typedef struct STM_MAC
{
	CRYPTOLIB_MAC_GENERAL_PARAMS	*MACGeneralParams;
	T_SEC_DATAKEY_ID				stKeyInfo;
	T_SEC_DATAKEY_ID				stMasterKeyInfo;
} CRYPTOLIB_MAC_PARAMETERS;


typedef struct
{
	unsigned char		*pOutputData;				// Pointer to the output data buffer
	unsigned int		LenOutputData;				// Length of the output buffer
	unsigned char		*pInputData;				// Pointer to the input data buffer
	int					LenInputData;				// Length of the input buffer
	unsigned char		EncryptedWorkingKey[24];	// Pointer to the working key buffer
	unsigned char		*pInitialValue;
	T_SEC_OPER_FUNCT	iOper;
	CRYPTOLIB_OP_MODE	OperationMode;
} CRYPTOLIB_DATA_GENERAL_PARAMS;

typedef struct STM_DATA
{
	CRYPTOLIB_DATA_GENERAL_PARAMS	*DATAGeneralParams;
	T_SEC_DATAKEY_ID				stKeyInfo;
	T_SEC_DATAKEY_ID				stMasterKeyInfo;
} CRYPTOLIB_DATA_PARAMETERS;

//+ CTRLVERSION @aambrosio 09/08/2016, TLVTREE LIBRARY VERSION CONTROL
#ifndef TAG_LIB_INFO
///Tags definition
 #define	TAG_LIB_INFO			0x0100	//TLVTree MAIN NODE
 #define	TAG_LIB_NAME			0x0101	//Length: 6.	5 Name + 1 '_'
 #define	TAG_LIB_VERSION			0x0102	//Length: 6.
 #define	TAG_LIB_DATE			0x0103	//Length: 8.	AAAAMMDD
 #define	TAG_LIB_SDK				0x0104	//Length: 8.
 #define	TAG_LIB_UNATTENDED		0x0105	//Length: 8.
 #define	TAG_LIB_TECHNOLOGY		0x0106	//Length: 2.	T2/T3
 #define	TAG_LIB_RELEASEMODE		0x0107	//Length: 1.	TRUE if release mode.
 #define	TAG_LIB_EP2CLESS		0x0108	//Length: 8.
 #define	TAG_LIB_EP2EMV			0x0109	//Length: 8.

///Error codes definition
 #define	ERR_LIBVERSION_OK					0
 #define	ERR_LIBVERSION_TLVNOTINITILIZED		-1
 #define	ERR_LIBVERSION_ERRADDINGCHILD		-2
#endif
//- CTRLVERSION

/*+************* VARIABLES *************************************************+*/

/*+************* PUBLIC FUNCTION PROTOTYPES ***************************************+*/

/* --------------------------------------------------------------------------
 * Function Name:	CryptoLib_Init
 * Description:		initializes the Crypto library. needs to called from after reset only once per app
 * Author:			lcalvano
 * Parameters:
 * Return:
 * Notes:
 */
int CryptoLib_Init(void);

/** --------------------------------------------------------------
 * Functcion Name:	CryptoLib_FreeKey
 * Autor:           @gsanabria
 * Date:            Aug 26, 2015
 * Description:		Borra una llave se probo con Master Sesion
 * Parameters:
 * @param stpKeyInfo
 * @param KEYType
 * Return:
 * - none
 * Notes:
 */
int CryptoLib_FreeKey(T_SEC_DATAKEY_ID *stpKeyInfo, CRYPTOLIB_KEY_TYPE KeyType);

/** --------------------------------------------------------------------------
 * Function Name:	CryptoLib_IsMKInjected
 * Description:		Checks if key is injected at specific position
 * Author:			@carodriguez 03/09/2015
 * Parameters:
 * @param stpMasterKeyInfo
 * @param strKCV, KCV returned if the key is loaded
 * @param KEYType
 * @return
 * Notes:
 */
int CryptoLib_IsKeyInjected(T_SEC_DATAKEY_ID *stpMasterKeyInfo, uchar *strKCV, CRYPTOLIB_KEY_TYPE KeyType);

/**
 * Ask for the PIN ONLINE
 * - Inject the WK.
 * - Ask for the PIN
 * - Generate the PINBLOCK
 *
 * @param PINEntryParams
 *
 * The following two parameters are just necessary if no EWK is loaded.
 * @param strEWK,	Working Key which will be used to encrypt the PINBLOCK. NULL if EWK is already loaded.
 * @param MKIndex.	Index of Scheme mode for MK
 * @return
 */
int CryptoLib_ShowPINEntry(CRYPTOLIB_PIN_PARAMETERS *PINEntryParams);

/**
 * Compute the MAC
 * - Inject the WK if master session key type.
 * - Compute MAC
 * - Generate MAC result
 *
 * @param MacEntryParams
 * @return
 */
int CryptoLib_ComputeMAC(CRYPTOLIB_MAC_PARAMETERS *MacEntryParams);

/**
 * Cyphers Data
 * - Inject the WK if key is master session.
 * - Compute cyphering
 * - Generate cyphering result
 *
 * @param DataEntryParams
 * @return
 */
int CryptoLib_DataEncryption( CRYPTOLIB_DATA_PARAMETERS *DataEntryParams);

/* --------------------------------------------------------------------------
 * Function Name:	CryptoLib_getLibInfo
 * Description:	Gets the library information.
 * Parameters:
 *  @param tlvLibVersion	output TLVTree info
 *
 * Return:
 * 	ERR_LIBVERSION	codes
 * Notes:
 * - tlvLibVersion must be initialized before calling this function
 * - tlvLibVersion must be released after used
 *
 * 28/07/2016
 * @aambrosio
 *
#define	TAG_LIB_NAME			"Crypt_"
#define	TAG_LIB_VERSION			"020501"
#define	TAG_LIB_DATE			"20171122"
#define	TAG_LIB_SDK				"11161PB"
#define	TAG_LIB_UNATTENDED		"06040001"

*/
int CryptoLib_getLibInfo(TLV_TREE_NODE tlvLibVersion);


#if defined(_ING_GNU_ARM_DEBUG_T2) || defined(_ING_GNU_ARM_DEBUG) ||defined(_ING_GNU_ARM_DEBUG_TETRA)
/************************************ TEST KEYS MANAGEMENT ***** JUST FOR MOCKUP ************************************************/
typedef struct
{
	T_SEC_DATAKEY_ID	stDataKeyId;	// Key Id structure
	uchar				ucKCV[3];		//KCV
}CRYPTO_KEYSLOADED;

#if defined(_ING_GNU_ARM_DEBUG_T2) || defined(_ING_GNU_ARM_DEBUG) //Telium 2
/**
 * Creates the secret AREA
 *
 * *** JUST FOR MOCKUP ***
 *
 * @param KeyType	Secure part (key type)
 * @param AreaID	Area Id to use
 * @param iCardId	Card Number to protect the Secret Area
 * @param iVARId	VAR Id to protect the Secret Area
 * @return
 */
int CryptoLib_createSecretArea(CRYPTOLIB_KEY_TYPE KeyType, long AreaID, int iCardId,
		int iVARId);
#endif

/**
 * Generic function to inject a Master key.
 *
 * *** JUST FOR MOCKUP ***
 *
 * @param strKey.			Key in Hex. mode
 * @param stpMasterKeyInfo	MK Data
 * @param KEYType.			Indicate if key is for PIN or DATA
 * @return
 */
int CryptoLib_InjectMK( unsigned char *strKey,
				   T_SEC_DATAKEY_ID *stpMasterKeyInfo,
				   CRYPTOLIB_KEY_TYPE KeyType);

/**
 * Generic function to inject a Master key.
 *
 * *** JUST FOR MOCKUP ***
 *
 * @param strKey.			Key in Hex. mode
 * @param stpMasterKeyInfo	MK Data
 * @param KEYType.			Indicate if key is for PIN or DATA
 * @return
 */
int CryptoLib_InjectMKWithKTK( unsigned char *strKey,
				   T_SEC_DATAKEY_ID *stpMasterKeyInfo,
				   T_SEC_DATAKEY_ID *stpKTKInfo,
				   CRYPTOLIB_KEY_TYPE KeyType);

/**
 * Generic function to inject a DUKPT key.
 *
 * *** JUST FOR MOCKUP ***
 *
 * @param InitDUKPTKey		DUKPT initial key.
 * @param InitKSNKey		KSN initial.
 * @param stDUKPTKey
 * @param stDUKPTRootKey
 * @param KeyType
 * @return
 */
int CryptoLib_InjectDUKPTInitial(
		unsigned char *InitDUKPTKey,
		char *InitKSNKey,
		T_SEC_DATAKEY_ID *stDUKPTKey,
		T_SEC_DATAKEY_ID *stDUKPTRootKey,
		CRYPTOLIB_KEY_TYPE KeyType );

/**
 * Clears the secure zone indicated
 *
 * @param KeyType
 */
void CryptoLib_getSecureZoneKeys(CRYPTOLIB_KEY_TYPE KeyType,
		CRYPTO_KEYSLOADED stKeysLoaded[], int *pMaxKeysList);

/**
 * Clears the secure zone indicated
 *
 * @param KeyType
 */
void CryptoLib_clearSecureZone(CRYPTOLIB_KEY_TYPE KeyType);

/************************************ TEST KEYS MANAGEMENT ***** JUST FOR MOCKUP ************************************************/
#endif //_ING_GNU_ARM_DEBUG
