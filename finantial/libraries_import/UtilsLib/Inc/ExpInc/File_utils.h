#ifndef __FILES_UTILS_H
#define __FILES_UTILS_H

#include "sdk_tplus.h"
#include "typ.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "fs_def.h"

#include "GL_CommonTypes.h"
#include "GL_Types.h"
#include "GL_Dir.h"
#include "GL_StringList.h"
#include "GL_File.h"

#include "TlvTree.h"

/** Library version that this header file refers to */
#define UTILSLIB_VERSION  "02.01"

#if(TETRA_HARDWARE == 1)
#define T_HFILE					T_GL_HFILE
#else
#define	T_HFILE					S_FS_FILE*
#endif //TETRA_HARDWARE

#define L_PATH							256
#define LZ_PATH							L_PATH + 1


#define DISK_FLASH_GL					"file://flash"
#define DISK_FLASH_DIR_GL				"dir://flash"

#define DISK_USB_GL						"file://umsd0a"
#define DISK_BASEUSB_GL					"file://umsd1a"

#define DISK_USB_DIR_GL					"dir://umsd0a"
#define DISK_BASEUSB_DIR_GL				"dir://umsd1a"

#define USB_DEVICE 						"/dev/umsd0a";
#define USB_BASEDEVICE					"/dev/umsd1a";

#define USB_VOLUME 						"/USB"


#define MAX_SIZE_TO_READ				3500000 	// Max Size umalloc

//extern functions

//!-------------------------------------------------------------------------------
//!	@brief Split a string with any character(Don't use GOAL, support UNATTENDED)
//!
//!	@param in char * strBuffer: Pointer Buffer to split.
//!	@param in long LengthBuffer: Length of buffer
//!	@param out TLV_TREE_NODE Tree: pointer to TLV to load Info result.
//!	@param in char character: Character to search or token.
//!	@param in bool IsAlsoEmptyFields:(true) Also Load in List empty fields
//!									 (false) Don't Load in List empty fields
//!
//!	@example 	strBuffer = "DISK_NAME1/FOLDER_NAME/FILE_NAME"
//!				character: '/'
//!				IsAlsoEmptyFields: true
//!
//!				Result: Tag 1 = DISK_NAME1
//!				Result: Tag 2 = FOLDER_NAME
//!				Result: Tag 3 = FILE_NAME
//!
//!	@return
//!		true -> OK!
//!		false -> Processing Error
//!-------------------------------------------------------------------------------

bool FL_Split( char * strBuffer, long LengthBuffer, TLV_TREE_NODE Tree, char character, bool IsAlsoEmptyFields);

//!-------------------------------------------------------------------------------
//!	@brief Split path using a character( Don't work if string have special characters )
//!
//!	@param in char * strBuffer: Pointer Buffer to split.
//!	@param out T_GL_HSTRINGLIST list: List returned with splitted data
//!	@param in char character: Character to search.
//!	@param in bool IsAlsoEmptyFields:(true) Also Load in List empty fields
//!									 (false) Don't Load in List empty fields
//!
//!	@example 	strBuffer = "DISK_NAME1/FOLDER_NAME/FILE_NAME"
//!				character: '/'
//!				IsAlsoEmptyFields: true
//!
//!				Result: GL_StringList_GetString( list, 0 ) = DISK_NAME1
//!				Result: GL_StringList_GetString( list, 1 ) = FOLDER_NAME
//!				Result: GL_StringList_GetString( list, 2 ) = FILE_NAME
//!
//!	@return
//!		true -> OK!
//!		false -> Processing Error
//!-------------------------------------------------------------------------------
extern bool FL_SplitPath( char * strBuffer, T_GL_HSTRINGLIST list, char character, bool IsAlsoEmptyFields);

//!-------------------------------------------------------------------------------
//!	@brief This function creates the disk for store file( store data structure )
//!
//!	@param in S_FS_PARAM_CREATE * xCfg: Parameters of disk.
//!	@param in, out unsigned long * ulSizeEachFichier: Pointer to size of each file
//!
//!	@return int code of result
//!-------------------------------------------------------------------------------
extern int FL_DiskCreate_ForFiles( S_FS_PARAM_CREATE * xCfg, unsigned long * ulSizeEachFichier );

//!-------------------------------------------------------------------------------
//!	@brief Destroy a disk.
//!
//!	@param in char * DiskPath: Path of disk. example "/DISK_NAME"
//!
//!	@return void
//!-------------------------------------------------------------------------------
extern int FL_DiskKill( char * DiskPath );

//!-------------------------------------------------------------------------------
//!	@brief This function detects if path is folder or file
//!
//!	@param in char * pStrPath: Path to check.
//!
//!	@example 	pStrPath = "file://flash/DISK_NAME/FILE_NAME"
//!				result = false
//!	@example 	pStrPath = "file://flash/DISK_NAME/FOLDER_NAME/"
//!				result = true
//!
//!	@return	bool true(Is Directory) or false( Is File)
//!-------------------------------------------------------------------------------
extern bool FL_Path_IsDirectory( char * pStrPath );

//!-------------------------------------------------------------------------------
//!	@brief This function removes any folder on flash disk into terminal
//!
//!	@param in char * pSrcFullPath: Folder Path to delete
//!			example: /DISK_NAME/FOLDER_1/FOLDER_2/...FOLDER_N or
//!			example: /DISK_NAME/FOLDER_1/FOLDER_2/...FOLDER_N/
//!
//!	@param in bool IsUsb: Folder is located in USB
//!
//!	@return	bool true(ok) or false( Error)
//!-------------------------------------------------------------------------------
extern bool FL_Dir_RemoveAll( char * pSrcFullPath, bool IsUsb );

//!-------------------------------------------------------------------------------
//!	@brief This function creates any folder on flash disk or USB
//!
//!	@param in char * pcFilePath: Folder Path to create
//!			example: /DISK_NAME/FOLDER_1/FOLDER_2/...FOLDER_N
//!	@param in bool IsMount: Internal Mount & unmount disk.
//!	@param in bool IsUsb: 	(true) If device is USB
//!						(false) Disk flash into terminal
//!
//!	@return	bool true(ok) or false( Error)
//!-------------------------------------------------------------------------------
extern int FL_Dir_CreatePath( char * pcFilePath, bool IsMount, bool IsUsb );

//!-------------------------------------------------------------------------------
//!	@brief It erases a files on disk
//!
//!	@param char * pcFilePath: File path Ej: "/DISK_NAME/FILE_NAME"
//!
//!	@return GL_SUCCESS if delete ok or
//!			other if Error
//!-------------------------------------------------------------------------------
extern int FL_Delete( char * pcFilePath );

//!-------------------------------------------------------------------------------
//!	@brief Get size of file
//!
//!	@param char * pcFilePath: Disk and File Path
//!				Ej: /DISK_NAME/FILE_NAME
//!
//!	@return Size File
//!-------------------------------------------------------------------------------
extern long FL_GetSize( char * pcFilePath );

//!-------------------------------------------------------------------------------
//!	@brief Get number of rows of file
//!
//!	@param char * pcFilePath: Disk and File Path
//!				Ej: /DISK_NAME/FILE_NAME
//!
//!	@param size_t SizeStruct: Size of each register(Size of struct stored)
//!
//!	@return number of rows
//!-------------------------------------------------------------------------------
extern long FL_GetNumRows( char * pcFilePath, size_t SizeStruct );

//!-------------------------------------------------------------------------------
//!	@brief Verifies if file exist
//!
//!	@param unsigned char * pStrPathFile
//!			example: /DISK_NAME/FILE_NAME
//!
//!	@return
//!		true -> OK!
//!		false -> Error or not found
//!-------------------------------------------------------------------------------
extern bool FL_Exist( char * pStrFileName );

//!-------------------------------------------------------------------------------
//!	@brief Insert a new record at the end of file.
//!
//!	@param char * pcFilePath: Disk and File Path
//!				Ej: /DISK_NAME/FILE_NAME
//!	@param void * pData: Pointer data to be stored.
//!	@param size_t lLenData: Length of data to be stored.
//!
//!	@return (-1) Error
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------

extern long FL_Insert( char * pcFilePath, void * pData, size_t lLenData );

//!-------------------------------------------------------------------------------
//!	@brief Update a existing record into file, by index
//!
//!	@param char * pcFilePath: Disk and File Path
//!				Ej: /DISK_NAME/FILE_NAME
//!	@param void * pData: Pointer data to be updated.
//!	@param size_t lLenData: Length of data to be updated.
//!	@param unsigned long ulIndex: Index record into file
//!
//!	@return (-1) Error or not found
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------
extern int FL_Update( char * pcFilePath, void * pData, size_t lLenData, unsigned long ulIndex );

//!-------------------------------------------------------------------------------
//!	@brief Get existing record into file, using index
//!
//!	@param char * pcFilePath: Disk and File Path
//!				Ej: /DISK_NAME/FILE_NAME
//!	@param void * pData: Pointer to load data.
//!	@param size_t lLenData: Length of data to be read.
//!	@param unsigned long ulIndex: Index record into file
//!
//!	@return (-1) Error or not found
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------
extern int FL_Select( char * pcFilePath, void * pData, size_t lLenData, unsigned long ulIndex );

//!-------------------------------------------------------------------------------
//!	@brief Get last record into file
//!
//!	@param char * pcFilePath: Disk and File Path
//!				Ej: /DISK_NAME/FILE_NAME
//!
//!	@param void * pData: Pointer data to be stored.
//!	@param size_t lLenData: Length of data to be stored.
//!
//!	@return (-1) Error or not found
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------
extern long FL_SelectLastRecord( char * pcFilePath, void * pData, size_t lLenData );

//!-------------------------------------------------------------------------------
//!	@brief Write a file, using offset
//!
//!	@param unsigned char * pStrPathFile: File Name. Ej: "/DISKNAME/FILENAME"
//!	@param void * pData: Data to Write
//!	@param unsigned long plLength: Length of data to Write
//!	@param unsigned long offset: Bytes of offset
//!
//!	@return GL_SUCCESS or GL_ERROR_FAILED
//!-------------------------------------------------------------------------------

extern int FL_UpdateOffset( unsigned char * pStrFileName, void * pData, unsigned long plLength, unsigned long offset );

//!-------------------------------------------------------------------------------
//!	@brief Read a file, using offset
//!
//!	@param unsigned char * pStrPathFile: File Name. Ej: "/DISKNAME/FILENAME"
//!	@param void * pData: Data to Write
//!	@param unsigned long plLength: Length of data to Write
//!	@param unsigned long offset: Bytes of offset
//!
//!	@return GL_SUCCESS or GL_ERROR_FAILED
//!-------------------------------------------------------------------------------

extern int FL_SelectOffset( unsigned char * pStrFileName, void * pData, unsigned long plLength, unsigned long offset );

//!-------------------------------------------------------------------------------
//!	@brief Save a file, using append
//!
//!	@param unsigned char * pStrPathFile: File Name. Ej: "/DISKNAME/FILENAME"
//!	@param void * pData: Data to Write
//!	@param unsigned long plLength: Length of data to Write
//!	@param bool IsAppend:
//!				false: Store on begin of File
//!				true:  Store on End of File
//!
//!	@return nothing
//!-------------------------------------------------------------------------------
extern int FL_WriteBuffer( unsigned char * pStrFileName, void * pData, unsigned long plLength, bool IsAppend );

#if(DIR_USB_UTILS == 1)
//!-------------------------------------------------------------------------------
//!	@brief Detect and mount a USB device.
//!
//!	@param nothing
//!
//!	@return int
//!		0: Device found and mount OK
//!		-1: Error
//!-------------------------------------------------------------------------------
extern int FL_USB_DetectAndMount( void );

//!-------------------------------------------------------------------------------
//!	@brief Unmount a USB device.
//!
//!	@param nothing
//!
//!	@return int
//!		0: Device mount OK
//!		-1: Is returned and the variable errno is set to indicate the error.
//!-------------------------------------------------------------------------------
extern int FL_USB_SystUnmount(void);

//!-------------------------------------------------------------------------------
//!	@brief Rename file
//!
//!	@param bool IsDeleteSrc: 	(true) Delete source file
//!								(false) Don't delete
//!	@param unsigned char * pStrPathFile: Path of source file
//!	@param unsigned char * pStrPathTempFile: Path of temporal File
//!
//!	@return bool 	true(OK)
//!					false(Error)
//!-------------------------------------------------------------------------------
extern bool FL_Copy_Rename( bool IsDeleteSrc, char * pStrSrcPath, char * pStrDstPath );

//!-------------------------------------------------------------------------------
//!	@brief Copy files from Terminal DISK to USB
//!
//!	@param in char * pAcDirpath: Source Path. example "/DISK_NAME"
//!	@param in char * pAcDstPath: Destiny Path. example "/FOLDER_NAME"
//!	@param in bool IsShowMsgErr: 	(true)Show message if error
//!									(false)Don't show message.
//!
//!	@return nothing
//!-------------------------------------------------------------------------------
extern bool FL_DISK_To_USB_Copy( char * pAcDirpath, char * pAcDstPath, bool IsShowMsgErr );

//!-------------------------------------------------------------------------------
//!	@brief Copy files from USB to Terminal DISK
//!
//!	@param in char * pAcDirpath: Source Path
//!	@param in char * pAcDstPath: Destiny Path
//!
//!	@return nothing
//!-------------------------------------------------------------------------------
extern void FL_USB_To_DISK_Copy( char * pAcDirpath, char * pAcDstPath );
#endif //DIR_USB_UTILS

#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Open File.
//!		The file must be close when isn't in use, usign 'FL_CloseFile'
//!
//!	@param in char * pUcPath: File path to open.
//!
//!	@return T_HFILE handler file, NULL if error.
//!-------------------------------------------------------------------------------
extern T_HFILE FL_WOC_OpenFile( char * pUcPath );
#else
//!-------------------------------------------------------------------------------
//!	@brief Open File.
//!		The file must be close when isn't in use, usign 'FL_CloseFile'
//!
//!	@param in char * pUcPath: File path to open.
//!
//!	@return T_HFILE handler file, NULL if error.
//!-------------------------------------------------------------------------------
T_HFILE FL_WOC_OpenFile( char * pUcPath );
#endif //TETRA_HARDWARE

#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Close File
//!		The file must be opened previously! using 'FL_OpenFile'
//!
//!	@param in T_HFILE hFile: Handler of file
//!	@param in bool IsShowMsgErr: 	(true) Show message if error
//!								(false) Don't show message
//!
//!	@return true(OK) or false(Error)
//!-------------------------------------------------------------------------------
bool FL_WOC_CloseFile( char * pUcPath, T_HFILE hFile );
#else
//!-------------------------------------------------------------------------------
//!	@brief Close File
//!		The file must be opened previously! using 'FL_OpenFile'
//!
//!	@param in T_HFILE hFile: Handler of file
//!	@param in bool IsShowMsgErr: 	(true) Show message if error
//!								(false) Don't show message
//!
//!	@return true(OK) or false(Error)
//!-------------------------------------------------------------------------------
bool FL_WOC_CloseFile( char * pUcPath, T_HFILE hFile );
#endif //TETRA_HARDWARE

#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Get Number of rows of file
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in T_HFILE hFile: Handler of file
//!	@param in size_t ulSzStr: Size of record.
//!	@param in bool IsShowMsgErr: 	(true) Show message if error
//!								(false) Don't show message
//!
//!	@return long: Number of rows
//!-------------------------------------------------------------------------------
extern long FL_WOC_GetNumRows( T_HFILE hFile, size_t ulSzStr );
#else
//!-------------------------------------------------------------------------------
//!	@brief Get Number of rows of file
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in T_HFILE hFile: Handler of file
//!	@param in size_t ulSzStr: Size of record.
//!	@param in bool IsShowMsgErr: 	(true) Show message if error
//!								(false) Don't show message
//!
//!	@return long: Number of rows
//!-------------------------------------------------------------------------------
long FL_WOC_GetNumRows( T_HFILE hFile, size_t ulSzStr );
#endif //TETRA_HARDWARE

#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Get existing record into file, using index
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in T_HFILE hFile: Handle File
//!	@param out void * pData: Pointer to load data.
//!	@param in size_t lLenData: Length of data to be read.
//!	@param in unsigned long ulIndex: Index record into file
//!
//!	@return (-1) Error
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------
extern long FL_WOC_SelectIndex( T_HFILE hFile, void * pData, size_t lLenData, unsigned long ulIndex );
#else
//!-------------------------------------------------------------------------------
//!	@brief Get existing record into file, using index
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in T_HFILE hFile: Handle File
//!	@param out void * pData: Pointer to load data.
//!	@param in size_t lLenData: Length of data to be read.
//!	@param in unsigned long ulIndex: Index record into file
//!
//!	@return (-1) Error
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------
long FL_WOC_SelectIndex( T_HFILE hFile, void * pData, size_t lLenData, unsigned long ulIndex );
#endif //TETRA_HARDWARE


#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Update record into file by index
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in bool IsTemp: (true) to use temporal file
//!	@param in void * pData: Pointer to data to save.
//!	@param in size_t ulSzStr: Length data to save.
//!	@param in unsigned long ulIndex: Index to write
//!
//!	@return (-1) Error
//!			(Other) Index of record
//!-------------------------------------------------------------------------------
extern long FL_WOC_UpdateIndex( T_HFILE hFile, void * pData, size_t ulSzStr, unsigned long ulIndex );
#else
//!-------------------------------------------------------------------------------
//!	@brief Update record into file by index
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in bool IsTemp: (true) to use temporal file
//!	@param in void * pData: Pointer to data to save.
//!	@param in size_t ulSzStr: Length data to save.
//!	@param in unsigned long ulIndex: Index to write
//!
//!	@return (-1) Error
//!			(Other) Index of record
//!-------------------------------------------------------------------------------

long FL_WOC_UpdateIndex( T_HFILE hFile, void * pData, size_t ulSzStr, unsigned long ulIndex );
#endif //TETRA_HARDWARE

#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Get existing record into file, using offset
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in T_HFILE hFile: Handle File
//!	@param out void * pData: Pointer to load data.
//!	@param in size_t lLenData: Length of data to be read.
//!	@param in unsigned long ulIndex: Index record into file
//!
//!	@return (GL_SUCCESS) OK
//!			(Other) Error
//!-------------------------------------------------------------------------------
extern int FL_WOC_SelectOffset( T_HFILE hFile, void * pData, size_t lLenData, unsigned long offset );
#else
//!-------------------------------------------------------------------------------
//!	@brief Get existing record into file, using offset
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in T_HFILE hFile: Handle File
//!	@param out void * pData: Pointer to load data.
//!	@param in size_t lLenData: Length of data to be read.
//!	@param in unsigned long ulIndex: Index record into file
//!
//!	@return (-1) Error
//!			(Others) index of record into file.
//!-------------------------------------------------------------------------------
int FL_WOC_SelectOffset( T_HFILE hFile, void * pData, size_t lLenData, unsigned long offset );
#endif

#if(TETRA_HARDWARE == 1)
//!-------------------------------------------------------------------------------
//!	@brief Update record into file by index
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in bool IsTemp: (true) to use temporal file
//!	@param in void * pData: Pointer to data to save.
//!	@param in size_t ulSzStr: Length data to save.
//!	@param in unsigned long ulIndex: Index to write
//!
//!	@return (GL_SUCCESS) OK
//!			(Other) Error
//!-------------------------------------------------------------------------------
extern int FL_WOC_UpdateOffset( T_HFILE hFile, void * pData, size_t ulSzStr, unsigned long offset );
#else
//!-------------------------------------------------------------------------------
//!	@brief Update record into file by offset
//!		The file must be opened previously! using 'FL_OpenFile'(Without open & close)
//!
//!	@param in bool IsTemp: (true) to use temporal file
//!	@param in void * pData: Pointer to data to save.
//!	@param in size_t ulSzStr: Length data to save.
//!	@param in unsigned long ulIndex: Index to write
//!
//!	@return (-1) Error
//!			(Other) Index of record
//!-------------------------------------------------------------------------------
int FL_WOC_UpdateOffset( T_HFILE hFile, void * pData, size_t ulSzStr, unsigned long offset );
#endif

#endif //__FILES_UTILS_H


