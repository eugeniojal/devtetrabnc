/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		_TLV_admin.h
* Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/
#ifndef _H_TLVADMIN
#define _H_TLVADMIN

#include "Common.h"
/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	Tlv_FindTag
 * Description:		Search tag into tree on index 0.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * Return:
 * 	- (RET_OK)  Tag exist
 * 	- (RET_NOK) Tag doesn't exist
 * Notes:
 */

int Tlv_FindTag(TLV_TREE_NODE Tree, unsigned int nTag);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValue
 * Description:		Set tag children value into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in void * value: pointer of data
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValue(TLV_TREE_NODE Tree, unsigned int nTag, void * value, unsigned int length);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_GetTagValue
 * Description:		Get tag children value into tree.
 * 	- If tags exist, return value and length.
 * 	- If tags doesn't exist, return error.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- out void * value: pointer of data
 * 	- out unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_GetTagValue(TLV_TREE_NODE Tree, unsigned int nTag, void * value, unsigned int * length);

int Tlv_GetTagValueN(TLV_TREE_NODE Tree, unsigned int nTag, void* value, unsigned int* length, int maxSize);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_ReleaseTag
 * Description:		Release a tag into tree.
 * 	- If tags exist, release tag.
 * 	- If tags doesn't exist, do anything.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_ReleaseTag(TLV_TREE_NODE Tree, unsigned int nTag);


/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueInteger
 * Description:		Set tag children value as an integer into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in int value: number with the value
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValueInteger(TLV_TREE_NODE Tree, unsigned int nTag, int value, unsigned int length);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueLong
 * Description:		Set tag children value as an long into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in int value: number with the value
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValueLong(TLV_TREE_NODE Tree, unsigned int nTag, long value, unsigned int length);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueString
 * Description:		Set tag children value string into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in char * value: pointer to the null terminated string to save in the tree
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */
int Tlv_SetTagValueString(TLV_TREE_NODE Tree, unsigned int nTag, char *value);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_FindChild
 * Description:		Find child in specific node (only search on first tree level)
 * Author:			mamata
 * Parameters:
 *  - const TLV_TREE_NODE hNode - node parent
 *  - unsigned int nTag - tag number
 * Return:
 *  - NULL - nTag do not find
 *  (other) - pointer to node
 * Notes:
 */
TLV_TREE_NODE Tlv_FindChild(const TLV_TREE_NODE hNode, unsigned int nTag);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_CopyTagBetweenTrees
 * Description:		Read tag from source Tree, and load value on destiny Tree
 * 	- If tree tag on destiny exist, overwrite information on same level.
 * 	- If tree tag on destiny doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE TreeSrc: Source Parent tree.
 * 	- in unsigned int nTagSrc: Source Tag id.
 * 	- in TLV_TREE_NODE TreeDst: Destiny Parent tree.
 * 	- in unsigned int nTagDst: Destiny Tag id.
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_CopyTagBetweenTrees(TLV_TREE_NODE TreeSrc, unsigned int nTagSrc, TLV_TREE_NODE TreeDst, unsigned int nTagDst);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueAmount
 * Description:		Set tag children value as an amount into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in amount value: number with the value
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */
int Tlv_SetTagValueAmount(TLV_TREE_NODE Tree, unsigned int nTag, amount value);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_GraftWithTag
 * Description:		Does a graft to a tree and also sets the tag for the new node added
 * Parameters:
 *TLV_TREE_NODE *hParent Tree where the child tree will be added
 *TLV_TREE_NODE hChild  Tree to be added to parent
 *int nTag : Tag to be used on the added child Tree
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Graft Made
 * Notes:
 *  * @note
 * - The node @a hChild must be a root node (e.g. it is not a child of an other node).
 * - The node @a hParent must not belong to the same branch as the node @a hChild.
 */
int Tlv_CustomGraftWithTag(TLV_TREE_NODE hParent,TLV_TREE_NODE hChild, int nTag);
/* --------------------------------------------------------------------------
 * Function Name:	Tlv_ReleaseTree
 * Description:		Release memory of an tree.
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) No memory was freed
 * Notes:
 */

int Tlv_ReleaseTree(TLV_TREE_NODE *Tree);
#endif //_H_TLVADMIN
