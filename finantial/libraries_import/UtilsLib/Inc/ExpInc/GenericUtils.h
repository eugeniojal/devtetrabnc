#ifndef __GENUTILS_H
#define __GENUTILS_H

/* ------------------------------------------------------------------------------
* Function Name:   	FL_GetXMLFiletoTlvTree
* Description:     	Inverse process of FL_SetXMLFiletoTlvTree API
* 					Read a file with TLVTree format and unserialize to process with
* 				   	TLVTree APIs
* Parameters:
*  	out *Tree: Tree pointer variable where will be unserialized
* 	in *Disk: File Disk name, for example = "/HOST"
*  	in *FullFileName: Complete path & file name, example = "/HOST/FILENAME.XML"
* Return:
*  - RET_OK: OK
*  - RET_NOK: Error!
* Notes:
*/
int FL_GetXMLFiletoTlvTree(TLV_TREE_NODE *Tree, char *Disk, char *FullFileName);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_SetXMLFiletoTlvTree
* Description:     	Inverse process of FL_GetXMLFiletoTlvTree API
* 					Write a file with TLVTree format using an unserialized tree
* Parameters:
* 	in Tree: Tree variable to be write on file
*  	in *Disk: File Disk name, for example = "/HOST"
*  	in *FullFileName: Complete path & file name, example = "/HOST/FILENAME.XML"
* Return:
*  - RET_OK: OK
*  - RET_NOK: Error!
* Notes:
*/
int FL_SetXMLFiletoTlvTree(TLV_TREE_NODE Tree, char *Disk, char *FullFileName);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_Path_Split
* Description:		get path and split it on 3 string: Disk, directory and file name
*
* Parameters:
*  	in char *pcFilePath: Input file path
*  	out char * strDiskName: Disk name
*  	out char * strDirName: Directory Name
*  	out char * strFileName: File name
* Return:
*	(-1) processing error
*	(0) OK
* Notes:
*/
int FL_Path_Split(char *pcFilePath, char * strDiskName, char * strDirName, char * strFileName);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_IsMatch
* Description:     	Check two strings where first string may contain wild card
* 					characters and second string is a normal string.
* 					If match, return TRUE. if not, return FALSE.
*
* 					For example, "g*ks" matches with "geeks" match. And
* 					string "ge?ks*" matches with "geeksforgeeks"
* 					(note '*' at the end of first string). But "g*k" doesn�t match
* 					with "gee" as character 'k' is not present in second string.
* Parameters:
*  	in char *first: Pattern wildcard
*  	in char * second: normal String
* Return:
*	(-1) Error or not found
*	(Others) index of record into file.
* Notes:
*/
bool FL_IsMatch(char *first, char * second);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_TAR_SearchFileMatch
* Description:     	Search complete file name into TAR, using wilcard pattern
* Parameters:
* 	in char * strTARPath: Path of TAR file
*  	in char * strPattern: Pattern wildcard
*  	out  char * strFileName: file name
* Return:
* 	(TRUE) OK
*	(FALSE) error.
* Notes:
*/
int FL_TAR_SearchFileMatch(char * strTARPath, char * strPattern, char * strFileName);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_TAR_GetSize
* Description:     	Get file size into TAR container
* Parameters:
*  	in char * pcFilePath: Disk and File Path Ej: /DISKNAME/TARFILE/FILE
* Return:
*	(-1) Error or not found
*	(Others) file size
* Notes:
*/
long FL_TAR_GetSize(char * pcFilePath);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_TAR_FileExist
* Description:     	Check if file into TAR exist.
* Parameters:
*  	in char * pcFilePath: Disk and File Path Ej: /DISKNAME/TARFILE/FILE
* Return:
*	(FALSE) Error or not found
*	(TRUE) OK, exist
* Notes:
*/
bool FL_TAR_FileExist(char * pcFilePath);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_TAR_Select
* Description:     	Read a file into TAR container
* Parameters:
*  	in char * pcFilePath: Disk and File Path Ej: /DISKNAME/TARFILE/FILE
* 	out void * pData: Pointer to load data.
* Return:
*	(-1) Error or not found
*	(Others) index of record into file.
* Notes:
*/
int FL_TAR_Select(char * pcFilePath, void * pData);

/* ------------------------------------------------------------------------------
* Function Name:   	FL_TAR_CopyFile
* Description:     	Copy file from TAR, to other disk
* Parameters:
*  	in char * pcSrcFilePath: Source path.  Example: /DISKNAME/TARFILE/FILE
* 	in char * pcDstFilePath: Destiny path  Example: /HOST/TARFILE/FILE
* Return:
*	(-1) Error or not found
*	(Others) index of record into file.
* Notes:
*/
int FL_TAR_CopyFile(char * pcSrcFilePath, char * pcDstFilePath);

#endif //__GENUTILS_H


