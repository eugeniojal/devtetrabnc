#ifndef _H_TIMER
#define _H_TIMER

//!-------------------------------------------------------------------------------
//!	@brief This function starts a timer number for iDelay/100 seconds.
//!	After starting a timer a function TimerGet() should be called to know
//! whether it is over or not.
//! The timer should be over after iDelay/100 seconds.
//!
//!	@param ucTimerNbr (I-) : Timer number 0..3
//!	@param iDelay (I-) : Initial timer. 100 = 1 second
//!
//!	@return >=0 : TimerStart done
//!			<0  : TimerStart failed
//!-------------------------------------------------------------------------------

extern int TimerStart( unsigned char ucTimerNbr, int iDelay);

//!-------------------------------------------------------------------------------
//!	@brief This function returns the state of the timer number.
//!
//!	@param ucTimerNbr (I-) : Timer number 0..3
//!
//!	@return >=0 : The number of seconds to finish
//!			<0  : TimerGet failed
//!-------------------------------------------------------------------------------
extern int TimerGet( unsigned char ucTimerNbr);

//!-------------------------------------------------------------------------------
//!	@brief This function should be called when the timer number is no more needed.
//!
//!	@param ucTimerNbr (I-) : Timer number 0..3
//!
//!	@return >=0 : TimerStop done
//!			<0  : TimerStop failed
//!-------------------------------------------------------------------------------
extern int TimerStop( unsigned char ucTimerNbr);

#endif //_H_TIMER
