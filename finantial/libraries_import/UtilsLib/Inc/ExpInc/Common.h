#pragma once

#define PACKED __attribute__((aligned (1), packed))

#define CHECK(CND,LBL) {if(!(CND)){goto LBL;}}

// signed integers
typedef char 				int8;
typedef short 				int16;
typedef long 				int32;
typedef long long			int64;

// unsigned integers
typedef unsigned char		uint8;
typedef unsigned short 		uint16;
typedef unsigned long 		uint32;
typedef unsigned long long	uint64;

// amount
typedef unsigned long long	amount;

#ifdef __TELIUM3__
#include "SPMCI_.h"
#else
#include "ipa280.h"
#endif

#ifndef RET_OK
#define RET_OK	0
#endif //_IPA280_DEF_H_
#define RET_NOK	-1
#define RET_CANCEL	-2

// tag sizes
#define LTAG_INT8					1
#define	LTAG_INT16					2
#define LTAG_INT32					4
#define LTAG_INT64					8
#define LTAG_UINT8					1
#define	LTAG_UINT16					2
#define LTAG_UINT32					4
#define LTAG_UINT64					8
#define LTAG_AMOUNT					8
#define LTAG_DATE_TIME				sizeof(DATE)


#define OPT_01 (0x01LL)
#define	OPT_02 ((0x01LL) << 1)
#define	OPT_03 ((0x01LL) << 2)
#define	OPT_04 ((0x01LL) << 3)
#define	OPT_05 ((0x01LL) << 4)
#define	OPT_06 ((0x01LL) << 5)
#define	OPT_07 ((0x01LL) << 6)
#define	OPT_08 ((0x01LL) << 7)
#define	OPT_09 ((0x01LL) << 8)
#define	OPT_10 ((0x01LL) << 9)
#define	OPT_11 ((0x01LL) << 10)
#define	OPT_12 ((0x01LL) << 11)
#define	OPT_13 ((0x01LL) << 12)
#define	OPT_14 ((0x01LL) << 13)
#define	OPT_15 ((0x01LL) << 14)
#define	OPT_16 ((0x01LL) << 15)
#define	OPT_17 ((0x01LL) << 16)
#define	OPT_18 ((0x01LL) << 17)
#define	OPT_19 ((0x01LL) << 18)
#define	OPT_20 ((0x01LL) << 19)
#define	OPT_21 ((0x01LL) << 20)
#define	OPT_22 ((0x01LL) << 21)
#define	OPT_23 ((0x01LL) << 22)
#define	OPT_24 ((0x01LL) << 23)
#define	OPT_25 ((0x01LL) << 24)
#define	OPT_26 ((0x01LL) << 25)
#define	OPT_27 ((0x01LL) << 26)
#define	OPT_28 ((0x01LL) << 27)
#define	OPT_29 ((0x01LL) << 28)
#define	OPT_30 ((0x01LL) << 29)
#define	OPT_31 ((0x01LL) << 30)
#define	OPT_32 ((0x01LL) << 31)
#define	OPT_33 ((0x01LL) << 32)
#define	OPT_34 ((0x01LL) << 33)
#define	OPT_35 ((0x01LL) << 34)
#define	OPT_36 ((0x01LL) << 35)
#define	OPT_37 ((0x01LL) << 36)
#define	OPT_38 ((0x01LL) << 37)
#define	OPT_39 ((0x01LL) << 38)
#define	OPT_40 ((0x01LL) << 39)
#define	OPT_41 ((0x01LL) << 40)
#define	OPT_42 ((0x01LL) << 41)
#define	OPT_43 ((0x01LL) << 42)
#define	OPT_44 ((0x01LL) << 43)
#define	OPT_45 ((0x01LL) << 44)
#define	OPT_46 ((0x01LL) << 45)
#define	OPT_47 ((0x01LL) << 46)
#define	OPT_48 ((0x01LL) << 47)
#define	OPT_49 ((0x01LL) << 48)
#define	OPT_50 ((0x01LL) << 49)
#define	OPT_51 ((0x01LL) << 50)
#define	OPT_52 ((0x01LL) << 51)
#define	OPT_53 ((0x01LL) << 52)
#define	OPT_54 ((0x01LL) << 53)
#define	OPT_55 ((0x01LL) << 54)
#define	OPT_56 ((0x01LL) << 55)
#define	OPT_57 ((0x01LL) << 56)
#define	OPT_58 ((0x01LL) << 57)
#define	OPT_59 ((0x01LL) << 58)
#define	OPT_60 ((0x01LL) << 59)
#define	OPT_61 ((0x01LL) << 60)
#define	OPT_62 ((0x01LL) << 61)
#define	OPT_63 ((0x01LL) << 62)
#define	OPT_64 ((0x01LL) << 63)


typedef struct DLL_INFO
{
	char DLLName[30];
	T_OSL_HDLL hDLL;
	char Version[10];
}DLL_INFO;






