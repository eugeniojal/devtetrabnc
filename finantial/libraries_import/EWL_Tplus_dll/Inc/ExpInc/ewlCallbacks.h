#ifndef EWLCALLBACKS_H
#define EWLCALLBACKS_H

#include "ewlStruct.h"

/**
 * Select application callback.
 *
 * May be called during \ref ewlGetCard() to select or confirm the correct
 * card application that shall be used.
 *
 * @param handle EWL Handle.
 * @param[out] selected Shall be set to the ID of selected card application
 * @param aidList List of valid card application labels
 * @param nAIDs Number of entries in \p aidList array
 *
 * @note If \p nAIDs is equal to 1, it indicates that terminal application must
 *  only confirm if cardholder wants continue this transaction using the given
 *  valid application.
 *
 * @return EWL_OK if application was selected by user (\ref ewlGetCard() goes on)
 * @return Other (\ref ewlGetCard() is interrupted and returns the value)
 */
typedef ewlStatus_t (*ewlCBSelectionMenu_t)(ewlObject_t *handle, uint16_t *selected,
                                            const char **aidList, uint16_t nAIDs);

/**
 * Get application parameters.
 *
 * May be called during \ref ewlGetCard(), before final selection,
 * to inform the library of EMV parameter dependent on selected application.
 *
 * The \p dataList array of \p nDataListElement elements is a list of all the
 * \p data values (from \ref ewlAIDSetStruct_t) of the possible candidate
 * applications.
 *
 * \p nDataListElement will only be more than one if two entries in the
 * AID candidate list (added through \ref ewlAddAID()) match to the same
 * application on the card.
 *
 * The parameters shall be set by calls to \ref ewlSetParameter() on \p handle.
 *
 * @param handle            EWL Handle.
 * @param dataList          List of data associated with the card application.
 * @param nDataListElement  Number of elements in the dataList.
 *
 * @return EWL_OK if parameter was set by user (\ref ewlGetCard() goes on)
 * @return Other (\ref ewlGetCard() is interrupted and returns the value)
 *
 * \note If possible do not put HMI functions in this callback, this can affect directly  the C'Less transactions
 *
 */
typedef ewlStatus_t (*ewlCBGetParameters_t)(ewlObject_t   *handle, const unsigned long *dataList,
                                            uint16_t  nDataListElement);

/**
 * Get certification authority public key.
 *
 * May be called during \ref ewlGoOnChip(), before authentication step, and should
 * return the public key of the certification authority (if one exist).
 *
 * @param handle            EWL Handle.
 * @param[out] certificate  Authority public key.
 *
 * @return EWL_OK normal key processinq, even key not found (\ref ewlGoOnChip() goes on)
 * @return If any other, ewlGoOnChip() is interrupted and returns the value)
 *
 * @note Missing public keys is not a fatal error, but transaction may be aborted due to
 *  authentication failure.
 */
typedef ewlStatus_t (*ewlCBGetPublicKey_t)(ewlObject_t *handle, ewlGetCertificate_t *certificate);

/**
 * Get PIN online.
 *
 * May be called during \ref ewlGoOnChip(), during cardholder verification, to capture
 * the cardholder online PIN.
 *
 * The callback may store the PIN value in one of the \c EWL_TAG_USER_DATA fields,
 * or in any other external memory.  EWL does not use the online PIN value for processing.
 *
 * @param handle                EWL Handle.
 *
 * @return EWL_OK               PIN online collected without problems  (\ref ewlGoOnChip() goes on)
 * @return EWL_ERR_USER_TIMEOUT Timeout (\ref ewlGoOnChip() is interrupted and returns \ref EWL_USER_TIMEOUT)
 * @return EWL_ERR_USER_CANCEL  Operation canceled by user   (function ewlGoOnChip is interrupted and returns EWL_USER_CANCEL)
 * @return EWL_ERR_USER_BY_PASS Operation bypassed by user   (function ewlGoOnChip goes on but considers pin online bypassed as defined in specification )
 * @return EWL_ERR_PINPAD_OFF_ORDER pinpad with problem (function ewlGoOnChip goes on but considers a pinpad problem)
 * @return Other                (function ewlGoOnChip consider pinpad problem and follow EMV flow)
 *
 * @note If this function is not defined, EWL will behave as if \ref EWL_CB_BYPASS was returned.
 */
typedef ewlStatus_t (*ewlCBGetPinOnLine_t) (ewlObject_t *handle);

/**
 * Get PIN offline call back.
 *
 * May be called during \ref ewlGoOnChip(), during cardholder verification, to inform
 * application to collect off-line PIN.
 *
 * @param handle               EWL Handle
 * @param[out] numDigits       Number of digits read in PIN offline operation
 *
 * @return EWL_OK            PIN offline collected without problems  (function ewlGoOnChip goes on )
 * @return EWL_ERR_USER_TIMEOUT       Timeout                      (function ewlGoOnChip is interrupted and returns EWL_USER_TIMEOUT)
 * @return EWL_ERR_USER_CANCEL      Operation canceled by user   (function ewlGoOnChip is interrupted and returns EWL_USER_CANCEL)
 * @return EWL_ERR_USER_BY_PASS       Operation bypassed by user   (function ewlGoOnChip goes on but considers pin online bypassed as defined in specification )
 * @return EWL_ERR_PINPAD_OFF_ORDER pinpad with problem (function ewlGoOnChip goes on but considers a pinpad problem)
 * @return Other               (function ewlGoOnChip consider pinpad problem and follow EMV flow)
 *
 * \note If this function is not defined, EWL considers as answer EWL_CB_BYPASS.
 */
typedef ewlStatus_t (*ewlCBGetPinOffLine_t)(ewlObject_t *handle, byte *numDigits);

/**
 * End of cardholder verification process.
 *
 * Called during \ref ewlGoOnChip(), at the end cardholder verification, should be used to validate
 * if the transaction should continue or not (for example, asking the user to confirm amount).
 *
 * @return EWL_OK           User confirm the end of CVM process (function ewlGoOnChip goes on )
 * @return EWL_ERR_USER_TIMEOUT      Timeout                      (function ewlGoOnChip is interrupted and returns EWL_USER_TIMEOUT)
 * @return EWL_ERR_USER_CANCEL     Operation canceled by user   (function ewlGoOnChip is interrupted and returns EWL_USER_CANCEL)
 * @return Other               (function ewlGoOnChip consider pinpad problem and follow EMV flow)
 *
 * \note If this function is not defined, EWL considers as answer EWL_CB_OK.
 */
typedef ewlStatus_t (*ewlCBFinishCVMProcessing_t)(ewlObject_t *handle);

/**
 * Display message.
 *
 * May be called during \ref ewlGetCard() or \ref ewlGoOnChip() to ask for a message to be shown to the user.
 * See \ref ewlDisplay_t for the list of messages.
 *
 * @param handle    EWL Handle
 * @param msgCode   Message code
 */
typedef void (*ewlCBDisplay_t)(ewlObject_t *handle, ewlDisplay_t msgCode);

/**
 * Request change of the contact-less LEDs.
 *
 * Called during c'less transactions to control the state of the LEDs.
 *
 * @param handle    EWL Handle.
 * @param step      Led step (0, 1, 2, 3 or 4).
 */
typedef void (*ewlCBLeds_t)(ewlObject_t *handle, uint16_t step);


/**
 * Request remove card from c'less field.
 *
 * Called during c'less transactions request remove card from c'less field.
 *
 * @param handle    EWL Handle.
 * @param status    Inform transaction status .
 */
typedef void (*ewlCBRemoveCard_t)(ewlObject_t *handle, int status);

/** Collection of all callbacks */
typedef struct {
    ewlCBDisplay_t                ewlDisplay;               /**< Show message */
    ewlCBSelectionMenu_t          ewlSelectionMenu;         /**< AID selection menu */
    ewlCBGetParameters_t          ewlGetParameter;          /**< Ask for application parameters */
    ewlCBGetPublicKey_t           ewlGetPublicKey;          /**< Get certification authority public key */
    ewlCBGetPinOnLine_t           ewlGetPinOnline;          /**< Get PIN online */
    ewlCBGetPinOffLine_t          ewlGetPinOffLine;         /**< Get PIN offline */
    ewlCBFinishCVMProcessing_t    ewlFinishCVMProcessing;   /**< Finish CVM process */
    ewlCBLeds_t                   ewlLeds;                  /**< Contact-less LEDs control */
    ewlCBRemoveCard_t             ewlRemoveCard;            /**< Warn to remove card */
} ewlCBFunctions_t;

#endif

