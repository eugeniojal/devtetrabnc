#ifndef EWLKERNEL_H
#define EWLKERNEL_H

#include <TlvTree.h>

/** Signature for C'less kernel functions that accept the transaction status as parameter */
typedef int (*ewlKernelFunction_t)(T_SHARED_DATA_STRUCT *);

/** Signature to the C'less kernel function to clear all transaction state */
typedef int (*ewlKernelFunctionNoParameter_t)(void);

/** List of functions that must be exported by a C'less kernel to be integrated in EWL.
 * They mimic the ones provided by the contactless kernels in the Telium architecture.
 * For more detailed information see the specific kernel interface documentation.
 */
typedef struct  {
    ewlKernelFunction_t             info;                       /**< Get kernel */
    ewlKernelFunction_t             debugManagement;            /**< Management of kernel debug features */
    ewlKernelFunction_t             doTransaction;              /**< Performs transaction with the presented card */
    ewlKernelFunction_t             getAllData;                 /**< Retrieve the entire kernel tag database */
    ewlKernelFunction_t             getData;                    /**< Get tag from kenel */
    ewlKernelFunction_t             loadData;                   /**< Load tag value in kenel */
    ewlKernelFunction_t             resumeTransaction;          /**< Resume a transaction that has been interrupted */
    ewlKernelFunction_t             afterTransaction;           /**< Perform necessary actions after a transaction */
    ewlKernelFunctionNoParameter_t  cancel;                     /**< Cancel Transaction */
    ewlKernelFunctionNoParameter_t  close;                      /**< Clear and initialize the kernel */
    ewlKernelFunctionNoParameter_t  clear;						/**< Clear context */
} ewlKernelFunctions_t;

typedef int (*ewlUnattendedOpen_t)( unsigned char ucChannel, char *cOption_p );
typedef int (*ewlUnattendedPowerOn_t) ( unsigned char ucChannel_p, unsigned short usPowerOnType_p, HISTORIC *pHisto_p );
typedef int (*ewlUnattendedClose_t)  ( unsigned char ucChannel_p);

typedef struct {
	ewlUnattendedOpen_t 	open;
	ewlUnattendedPowerOn_t 	powerOn;
	ewlUnattendedClose_t	close;
}ewlUnattendedFunction_t;

/** Callbacks for each supported kernel.
 * If a specific contacless kernel is not supported on this application, set all callbacks
 * to \c NULL */
typedef struct {
    ewlKernelFunctions_t          ewlPayWave;               /**< PayWave */
    ewlKernelFunctions_t          ewlPayPass;               /**< PayPass */
    ewlKernelFunctions_t          ewlExpressPay;            /**< ExpressPay */
    ewlKernelFunctions_t          ewlPure;                  /**< Pure */
    ewlKernelFunctions_t          ewlDiscoverPAS;           /**< Discover PAS */
    ewlKernelFunctions_t          ewlDiscoverZIP;           /**< Discover ZIP */
    ewlKernelFunctions_t		  ewlJSpeedy;     			/**< JSpeedy */
    ewlKernelFunctions_t		  ewlQuickPass;   			/**< QuickPass */

    ewlUnattendedFunction_t       ewlUnattended;			/**< Unattended used in iSelf Terminal*/

} ewlCBKernel_t;

#endif
