#ifndef EWL_H_
#define EWL_H_
/**
 * @file ewl.h
 * @brief EMV Wrapper Library
 *
 * @mainpage
 *
 * \ref EWL (<em>EMV Wrapper Library</em>) provides an abstraction over
 * Telium's <em>EMV Level 2</em> and <em>Easy Path to C'less</em> libraries
 * for processing EMV transactions.
 *
 * @section Usage
 *
 * The fundamental usage of EWL requires the following calls, in specific order:
 *
 * -# \ref ewlCreate(): to create the handle with internal data structures;
 * -# \ref ewlAddAID(): to add the list of AIDs that the application support;
 *  - See \ref ewlAIDSetStruct_t for information about each AID;
 * -# \ref ewlGetCard(): to execute EMV selection process (up to <em>Read Records</em>);
 * -# \ref ewlGoOnChip(): to execute all authentication steps and generate first AC;
 * -# \ref ewlFinishChip(): to process host response (if any) and generate second AC.
 *
 * Besides, the application will probably need to  make some calls to \ref ewlSetParameter() and
 * \ref ewlGetParameter() to store and extract values from the EWL handle.  Each parameter is
 * associated with a \e tag, some tags are from the EMV / C'less specifications, others are
 * proprietary and of internal use to EWL, and others are reserved for use by the caller
 * application.
 *
 * @todo Example code of EWL setup and minimal callbacks.
 *
 * @section Modules
 *
 * The library is broken into modules:
 * - \ref Definitions : General typedefs, enums and other constants;
 * - \ref EMV : High-level EMV processing API
 * - \ref DirectChip : Direct access to Chip through APDU commands.
 *
 * Check out the root module \ref EWL for more information.
 *
 * @section Requirements
 *
 * This library requires that the following modules be linked together with
 * the target application:
 * T2
 * - <em>EMVAPI</em> (EasyPath to EMV (EMV API) 22.10.0.00 or higher)
 * - <em>LarLib</em> (1.8 or higher)
 * T+/T2
 * - <em>EMVAPI</em> (EasyPath to EMV (EMV API) 30.8.3.03 or higher)
 * - <em>LarLib</em> (1.13 or higher)
 * T+/TETRA
 * - <em>EMVAPI</em> (EasyPath to EMV (EMV API) 30.8.3.03 or higher)
 * - <em>LarLib</em> (1.13 or higher)
 *
 * The target terminal must also include the follows modules:
 * T2
 * - <em>Kernel EMV [3065]</em>  (4.67 or higher)[1]
 * - <em>EMV new API [844585]</em> (1.01 or higher)[1]
 * - <em>C'Less add on [3655]</em> (4.01 or higher)[2]
 * - <em>C'Less Entry Point [813354]</em> (0.25 or higher)[3]
 * T+/T2
 * - <em>Kernel EMV [3065]</em>  (4.67 or higher)[1]
 * - <em>EMV new API [844585]</em> (1.01 or higher)[1]
 * - <em>C'Less add on [3655]</em> (4.01 or higher)[2]
 * - <em>C'Less Entry Point [813354]</em> (0.25 or higher)[3]
 * T+/TETRA
 * - <em>Kernel EMV [9992047128]</em>  ( 08.37.00  or higher)[5]
 * - <em>EMV new API [9992022437]</em> ( 03.06.00  or higher)[5]
 * - <em>C'Less Entry Point [9992044568 ]</em> (01.01.00 or higher)[6]
 *
 * If the project works with PayWave, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_PAYWAVE in project symbols</em>
 * T2
 * - <em>Include payWaveInterface in the project </em> (EasyPath to C'Less 5.6.2.00 or higher)
 * - <em>Load Kernel PayWave [813350] in the terminal</em> (2.04 or higher)[3]
 * T+/T2
 * - <em>Include Easy Path to Cless in the project </em> (EasyPath to C'Less 7.2.3.03 or higher)
 * - <em>Load Kernel PayWave [813350] in the terminal</em> (2.04 or higher)[3]
 * T+/TETRA
 * - <em>Include Easy Path to Cless in the project </em> (EasyPath to C'Less 7.2.3.03 or higher)
 * - <em>Load Kernel PayWave [9992044549] in the terminal</em> (06.04.00 or higher)[6]
 * - <em>Load Library Kernel PayWave [9992047151] in the terminal</em> (06.04.01 or higher)[6]
 *
 * If the project works with PayPass, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_PAYPASS in project symbols</em>
 * T2
 * - <em>Include PayPassInterface in the project </em> (EasyPath to C'Less 5.6.2.00 or higher)
 * - <em>Load Kernel PayPass3 [844241] in the terminal</em> (3.73 or higher)[3]
 * T+/T2
 * - <em>Include Easy Path to Cless in the project </em> (EasyPath to C'Less 7.2.3.03 or higher)
 * - <em>Load Kernel PayPass3 [844241] in the terminal</em> (3.73 or higher)[3]
 * T+/TETRA
 * - <em>Include Easy Path to Cless in the project </em> (EasyPath to C'Less 7.2.3.03 or higher)
 * - <em>Load Kernel PayPass3 [9992044549] in the terminal</em> (06.06.00 or higher)[6]
 * - <em>Load Library Kernel PayPass3 [9992047152] in the terminal</em> (06.06.01 or higher)[6]
 *
 * If the project works with ExpressPay, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_EXPRESSPAY in project symbols</em>
 * T2
 * - <em>Include ExpressPayInterface in the project </em> (EasyPath to C'Less 5.6.2.00 or higher)
 * - <em>Load Kernel ExpressPay3 [844297] in the terminal</em> (3.08 or higher)[3]
 * T+/T2
 * - <em>Include Easy Path to Cless in the project </em> (EasyPath to C'Less 7.2.3.03 or higher)
 * - <em>Load Kernel ExpressPay3 [844297] in the terminal</em> (3.08 or higher)[3]
 * T+/TETRA
 * - <em>Not integrate</em>
 *
 * If the project works with PURE, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_PURE in project symbols</em>
 * T2
 * - <em>Include PUREInterface in the project </em>  (Not integrated to EasyPath to C'Less yet)
 * - <em>Load Kernel PURE [844565] in the terminal</em> (1.03 or higher)
 * T+/T2
 * - <em>Include Easy Path to Cless in the project </em> (Easy Path to PURE 1.0.0.00)
 * - <em>Include PUREInterface in the project </em>  (Not integrated to EasyPath to C'Less yet)
 * - <em>Load Kernel PURE [844565] in the terminal</em> (3.08 or higher)[3]
 * T+/TETRA
 * - <em>Include Easy Path to Cless in the project </em> (Easy Path to PURE 1.0.0.00)
 * - <em>Include PUREInterface in the project </em>  (Not integrated to EasyPath to C'Less yet)
 * - <em>Load Kernel PURE [844565] in the terminal</em> (3.08 or higher)[3]
 *
 * If the project works with Contactless Discover PAS, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_DISCOVER_PAS in project symbols</em>
 * - <em>Set the define \c EWL_ENABLE_KERNEL_DISCOVER_ZIP in project symbols</em>
 * T2
 * - <em>Include DiscoverPASInterface in the project </em> (EasyPath to C'Less 5.10.0.00 or higher)
 * - <em>Include DiscoverInterface in the project </em> (EasyPath to C'Less 5.10.0.00 or higher)
 * - <em>Load Kernel Discover PAS [844557] in the terminal</em> (1.02 or higher)
 * - <em>Load Kernel Discover[813352] in the terminal</em> (1.01 or higher)
 * T+/T2
 * - <em>Include Easy Path to Cless in the project </em> (EasyPath to C'Less 7.2.3.03 or higher)
 * - <em>Load Kernel Discover PAS [844557] in the terminal</em> (1.02 or higher)
 * - <em>Load Kernel Discover[813352] in the terminal</em> (1.01 or higher)
 *
 * If the project works with Contactless JSpeedy, developer must:
 * - <em>Set the define \c EWL_ENABLE_KERNEL_JSPEEDY in project symbols</em>
 * T2
 * - <em>Not integrate</em>
 * T+/T2
 * - <em>Not integrate</em>
 * T+/TETRA
 * - <em>Not integrate</em>
 *
 * If the project works whit any cless technology and is a T2 terminal, be sure if the manager is
 * set to accept cless transactions (F -> Telium Manager -> Initialization -> Parameters-> Contactless) or
 * use the function PSQ_update_ClessReader to force this situation.
 *
 * If the project works with iSELF 250, developer must:
 * - <em>Include  in the project </em> (Telium Unattended 8.6.0.01 or higher)
 * - <em>Set the define \c EWL_ISELF_250 in project symbols</em>
 *
 *
 * Note: This library use event 14 in getCard
 *
 * [1] - T2 EasyPath to EMV (EMV API) (22.10.0.00 or higher)
 * [2] - T2 Telium SDK (9.20.1.02 or higher)
 * [3] - T2 EasyPath to C'Less (5.6.2.00 or higher)
 * [4] - Not integrate yet
 *
 * [5] - T+ EasyPath to EMV Kernel T3 (EMV API) (30.10.0.4 or higher)
 * [6] - T+ EasyPath to C'Less TransactionService_T3 (7.2.3.03 or higher)
 *
 * The target terminal must also has support to contactless and this feature must
 * be enabled in Telium Manager / Initialization / Parameters / Contactless
 *
 * @addtogroup EWL
 * @{
 */

#include <larlib/all.h>

#include "ewlEnum.h"
#include "ewlStructEvent.h"
#include "ewlTags.h"
#include "ewlKernel.h"
#include "ewlStruct.h"
#include "ewlCallbacks.h"
#include "ewlFunctions.h"
#include "ewlDirect.h"
#include "ewlParameters.h"

#endif

