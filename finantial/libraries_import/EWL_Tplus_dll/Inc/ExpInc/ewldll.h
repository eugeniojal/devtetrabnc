#ifndef EWLDLL_H
#define EWLDLL_H

#define EWL_IS_DLL

#include "ewl.h"

#define EWL_DLL_NUMBER  0x5410
#define EWL_DLL_LIB_VERSION  "00.35"



enum ewlDllProprietaryTags_t {
    EWL_TAG_DLL_VERSION                       = 0xDF83800C,     /**< EWL DLL version ( ASCII value length up to 5 bytes) [7]*/
    EWL_TAG_DLL_LIB_VERSION                   = 0xDF83800D,     /**< EWL DLL Lib version ( ASCII value length up to 5 bytes) [7]*/

    EWL_ERR_DLL_PROBLEM                       = -0x8033,  /**< -32819  Problem to load the ewlDLL*/

    EWL_TAG_DLL_VERSION_LEN                   = EWL_KERNEL_VERSION_LEN,     /**< [1] */
    EWL_TAG_DLL_LIB_VERSION_LEN               = EWL_KERNEL_VERSION_LEN      /**< [1] */
};


/**
 * Configures one or more tracing channels inside of EWLDLL
 * for further information please check the function logSetChannels in larlib\log.h
 *
 */
int ewlDllLogSetChannels(uint8_t firstChannel, int numChannels, uint16_t level,
                         logWriteFunction_t writeFunction,
                         logDumpFunction_t dumpFormatFunction,
                         void *context);


/**
 * Create an EWL instance in DLL context.
 *
 * @param callbacks List of callback functions
  *
 * @return NULL if could not create a new EWL instance, a new EWL instance otherwise.
 *
 * @note Each EWL \e transaction must be executed in a different instance.
 *  Re-using the same EWL instance for multiple transactions is not supported and
 *  has undefined results.
 */

ewlObject_t *ewlDllCreate(const ewlCBFunctions_t *callbacks);

// [1] - See footnote [1] in ewlTag.h
// [7] - See footnote [7] in ewlTag.h

#endif
