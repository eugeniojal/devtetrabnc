#ifndef EWLENUM_H
#define EWLENUM_H


/** Library version that this header file refers to */
#define EWL_VERSION  "1.14"

#include "Cless_Generic_Tags.h"

#define EWL_E_COLLISION_EVENT  15
#define EWL_COLLISION_EVENT  (1 <<  EWL_E_COLLISION_EVENT)

/** \c larlib.log channel list */
enum ewlLogChannels_t {
	EWL_LOG_TIMER				= 229, 		/**< Timer log */
	EWL_LOG_CARD_COMMUNICATION  = 230,      /**< Log communication with card */
    EWL_LOG_FUNCTION            = 231,      /**< Generic log channel for EWL functions  */
    EWL_LOG_FUNCTION_TASK       = 232,      /**< Generic log channel for EWL functions working in a task eg: ewlCancel */

    EWL_LOG_DEVELOPER           = 233,      /**< !!! INTERNAL USE ONLY !!! */
	EWL_LOG_TLV_MEMORY			= 234,		/**< !!! INTERNAL USE ONLY !!! */
	EWL_LOG_SHARED_MEMORY		= 235,		/**< !!! INTERNAL USE ONLY !!! */
};

/** Default or maximum tag for tags */
enum ewlTagsLength_t {
    EMV_CERTIFICATE_SERIAL_NUMBER_LEN                   = 3,
    EMV_TRACK1_DATA_LEN                                 = 76 + 1, // with null terminator
    EMV_TRACK2_DATA_LEN                                 = 37 + 1, // with null terminator
};

/** Supported card technologies, see tag EWL_TAG_TECHNOLOGY */
typedef enum {
    EWL_TECHNOLOGY_UNKNOWN              = 0,    /**< Card is unknown */
    EWL_TECHNOLOGY_CONTACT              = 1,    /**< Card is contact EMV */

    #if defined (EWL_ENABLE_KERNEL_PAYWAVE) || defined (EWL_IS_DLL)
    EWL_TECHNOLOGY_MSD                  = 2,    /**< Card is c'less MSD */
    EWL_TECHNOLOGY_QVSDC                = 3,    /**< Card is c'less qVSDC or MSD (if card do not support qVSDC we can use MSD)*/
    #endif

    #if defined (EWL_ENABLE_KERNEL_PAYPASS) || defined (EWL_IS_DLL)
    EWL_TECHNOLOGY_PAYPASS_MAG          = 4,    /**< Card is c'less paypass magnetic stripe */
    EWL_TECHNOLOGY_PAYPASS_MCHIP        = 5,    /**< Card is c'less paypass MChip */
    #endif

    #if defined (EWL_ENABLE_KERNEL_EXPRESSPAY) || defined (EWL_IS_DLL)
    EWL_TECHNOLOGY_EXPRESSPAY_MAG       = 6,    /**< Card is c'less expresspay magnetic stripe */
    EWL_TECHNOLOGY_EXPRESSPAY_EMV       = 7,    /**< Card is c'less expresspay EMV or Magnetic Stripe (if card do not support EMV we can use magnetic stripe)*/
    #endif

    #if defined (EWL_ENABLE_KERNEL_PURE) || defined (EWL_IS_DLL)
    EWL_TECHNOLOGY_PURE                 = 10,   /**< Card is c'less PURE */
    EWL_TECHNOLOGY_PURE_DATA            = 16,   /**< Card is c'less PURE in read or write mode (transaction type 0x78 or 0x79 )*/
    #endif

    EWL_TECHNOLOGY_DIRECT_CONTACT       = 11,   /**< Card is contact direct command */
    EWL_TECHNOLOGY_DIRECT_CONTACTLESS   = 12,    /**< Card is c'less direct command */

    #if defined (EWL_ENABLE_KERNEL_DISCOVER_PAS) || defined (EWL_IS_DLL)
    EWL_TECHNOLOGY_DISCOVER_PAS         = 13,   /**< Card is c'less discover PAS  */
    EWL_TECHNOLOGY_DISCOVER_ZIP         = 14,   /**< Card is c'less discover ZIP */
    EWL_TECHNOLOGY_DISCOVER_MAG         = 15,   /**< Card is c'less discover MSTRIPE (out parameter only )*/
    #endif

	#if defined (EWL_ENABLE_KERNEL_JSPEEDY) || defined (EWL_IS_DLL)
	EWL_TECHNOLOGY_JSPEEDY         		= 17,   /**< Card is c'less jspeedy  */
	EWL_TECHNOLOGY_JSPEEDY_LEGACY       = 18,   /**< Card is c'less jspeedy legacy */
	EWL_TECHNOLOGY_JSPEEDY_MAG         	= 19,   /**< Card is c'less jspeedy Magnetic stripe */
	#endif

	#if defined (EWL_ENABLE_KERNEL_QUICKPASS) || defined (EWL_IS_DLL)
	EWL_TECHNOLOGY_QUICKPASS         	= 20,   /**< Card is c'less quickpass*/
	#endif

	EWL_TECHNOLOGY_OTP_COLOMBIA			= 21, 	/**< Card OTP Colombia*/

} ewlTechnology_t;

/** Interface type values, see commands ewlGetCard/ewlTransactionAllowed/ewlDirectCommand and tag EWL_TAG_INTERFACE*/
typedef enum {
    EWL_CONTACT      = 0,        /**< Transaction execute though contact interface. */
    EWL_SAM1         = 1,        /**< SAM1 reader */
    EWL_SAM2         = 2,        /**< SAM2 reader */
    EWL_SAM3         = 3,        /**< SAM3 reader */
    EWL_SAM4         = 4,        /**< SAM4 reader */
    EWL_CONTACTLESS  = 5         /**< Transaction execute though contactless interface. */
} ewlInterfaceType_t;

/** Error values all functions returns and tag EWL_TAG_LAST_FLOW_ERROR */
typedef enum {
    EWL_OK                                          =  0,       /**<  0     Operation successful */
    EWL_WARNING_CARD_UNKONW                         =  0x8000,  /**< +32768 Operation complete but the card is not the same present in the first tap. */
    EWL_WARNING_CONTINUE                            =  0x8001,  /**< +32769 Continue flow. */
    EWL_ERR_CARD_MUTE                               = -0x8000,  /**< -32768 Card do not answer to poweron*/
    EWL_ERR_ALL_APP_BLOCKED                         = -0x8001,  /**< -32769 All applications in the card are blocked */
    EWL_ERR_NO_COMPATIBLE_APP                       = -0x8002,  /**< -32770 There is not any matching between card and terminal application */
    EWL_ERR_ALL_APP_INVALIDATED                     = -0x8003,  /**< -32771 All applications are invalidated, returns 6A81*/
    EWL_ERR_ALL_APP_ERROR                           = -0x8004,  /**< -32772 All applications in the card are blocked or has some error or are invalidated */
    EWL_ERR_INSUFFICIENT_OUT_BUFFER                 = -0x8005,  /**< -32773 Insufficient out buffer */
    EWL_ERR_USER_TIMEOUT                            = -0x8006,  /**< -32774 Timeout in card holder operations */
    EWL_ERR_USER_CANCEL                             = -0x8007,  /**< -32775 Transaction canceled by cardholder */
    EWL_ERR_USER_BY_PASS                            = -0x8008,  /**< -32776 Undefined error in cardholder operation */
    //EWL_ERR_GATHERING_PARAMETERS                  = -0x8009,  /**< -32777 Error during data gathering on a call back */   deprecated
    EWL_ERR_INTERNAL_FLOW                           = -0x800A,  /**< -32778 Some internal call was execute in wrong sequence */
    EWL_ERR_INTERNAL_INVALID_FORMAT                 = -0x800B,  /**< -32779 Invalid value inside of library  */
    EWL_ERR_INTERNAL_VALUE_OVERFLOW                 = -0x800C,  /**< -32780 Internal value overflow */
    EWL_ERR_TAG_NOT_FOUND                           = -0x800D,  /**< -32781 Tag not found */
    EWL_ERR_NO_RESOURCE                             = -0x800E,  /**< -32782 No enough resources (including device handles) to execute this function */
	EWL_ERR_MISSING_MANDATORY_CARD_DATA 			= -0x800F,  /**< -32783 Missing mandatory contactless card data */
    EWL_ERR_MISSING_MANDATORY_PARAMETER             = -0x8010,  /**< -32784 Missing mandatory parameter */
    EWL_ERR_INVALID_CARD_DATA                       = -0x8011,  /**< -32785 Error in a card data eg. tlv error, paywave card return 6984 in GPO*/
    EWL_ERR_INVALID_PARAMETER                       = -0x8012,  /**< -32786 At least one of the input parameter or tag is not valid." */
    EWL_ERR_INVALID_DOL                             = -0x8013,  /**< -32787 Invalid data object list */
    EWL_ERR_REDUNDANT_CARD_DATA                     = -0x8014,  /**< -32788 Card data is duplicated */
    EWL_ERR_NOT_IMPLEMENTED                         = -0x8015,  /**< -32789 Option is not implemented eg: new contactless card type */
    EWL_ERR_SWITCH_INTERFACE                        = -0x8016,  /**< -32790 Card must be processed using other interface */
    EWL_ERR_INVALID_CALL                            = -0x8017,  /**< -32791 Function call in wrong sequence */
    EWL_ERR_MISSING_COMPONENT                       = -0x8018,  /**< -32792 Kernel or DLL necessary to operation is missing */
    EWL_ERR_KERNEL_LINK                             = -0x8019,  /**< -32793 An error occurred into the kernel interface (linked to the custom application) */
    EWL_ERR_CLESS_DRIVER_NOT_OPEN                   = -0x801A,  /**< -32794 CLESS driver is not open */
    EWL_ERR_RESELECT                                = -0x801B,  /**< -32795 We need execute a reselect without the select AID this occur when Generate AC returns 6985 or in Discover PAS (AID Blocked) */
    EWL_ERR_CARD_INVALID_ANSWER                     = -0x801C,  /**< -32796 Invalid card answer eg: card response transaction approved when terminal advice a denial, Gernerate AC returns some invalid SW1SW2 [2]*/
    EWL_ERR_INTERNAL_PARAMETER                      = -0x801D,  /**< -32797 Invalid internal parameter*/
    EWL_ERR_INTERNAL_TLVTREE                        = -0x801E,  /**< -32798 Error in tlvTree processing inside of library*/
    EWL_ERR_INTERNAL_MISSING_DATA                   = -0x801F,  /**< -32799 Missing internal parameter */
    EWL_ERR_IS_EASY_ENTRY                           = -0x8020,  /**< -32800 The technology of this card is Easy Entry */
    EWL_ERR_RF_PROBLEM                              = -0x8021,  /**< -32801 CLESS Communication problem */
    EWL_ERR_INTERNAL_INSUFFICIENT_BUFFER            = -0x8022,  /**< -32802 Insufficient internal buffer */
    EWL_ERR_NOT_ALLOWED                             = -0x8023,  /**< -32803 Operation not allowed */
    EWL_ERR_INVALID_HANDLE                          = -0x8024,  /**< -32804 Invalid handle */
    EWL_ERR_INVALID_CARD                            = -0x8025,  /**< -32805 Invalid card eg:. Missing mandatory data as PAN (older Kernel, in kernel 6.XX there is a specific erro to this situation), user set a card data instead read, GPO return 6800 read record return a unexpected answer, first generate request a ARQC and card return Refer */

    EWL_ERR_ALL_APP_NON_EMV                         = -0x8026,  /**< -32806 All applications in the contact card are non EMV app eg GPO return 6985, final select return 6283*/

    EWL_ERR_CLESS_COLLISION                         = -0x8027,  /**< -32807 Two or more cless card were approached */
    EWL_ERR_CLESS_DETECTION_PROBLEM                 = -0x8028,  /**< -32808 CLESS Problem in detection phase */
    EWL_ERR_CLESS_TYPE_NOT_SUPPORTED                = -0x8029,  /**< -32809 CLESS is not a EMV payment card (MIFARE, Calypso, others). If  this error error do not call ewlCBRemoveCard_t. */
    EWL_ERR_MAGSTRIPE_NOT_ALLOWED                   = -0x802A,  /**< -32810 Can not process PayPass Mag Stripe Transaction */
    EWL_ERR_MCHIP_NOT_ALLOWED                       = -0x802B,  /**< -32811 Can not process PayPass MChip Transaction */
    EWL_ERR_MOBILE_ON_DEVICE_CVM                    = -0x802C,  /**< -32812 It is a mobile CVM on device */
    EWL_ERR_KERNEL_DISABLE                          = -0x802D,  /**< -32813 Kernel disable */
    EWL_ERR_CARD_REMOVED                            = -0x802E,  /**< -32814 Card removed during the processing */
    EWL_ERR_PINPAD_OFF_ORDER                        = -0x802F,  /**< -32815 Pinpad off order (use this function as return in pin-on line or offline if necessary) */
    EWL_ERR_CARD_BLOCKED                            = -0x8030,  /**< -32816 Card blocked */
    EWL_ERR_USE_OTHER_CARD                          = -0x8031,  /**< -32817 Use other card (dicover DPAS possible answer) */
    EWL_ERR_USE_CONTACT                             = -0x8032,  /**< -32818 Ignore cless interface and start a contact transaction immediately  (qVSDC50) */
    //EWL_ERR_DLL_PROBLEM                           = -0x8033,  /**< -32819  Reserved*/
    EWL_ERR_CARD_INVALIDATED                        = -0x8034,  /**< -32820  card return 6A81 on 1PAY.SYS.DDF01 or card mute, the error EWL_ERR_CARD_MUTE occur only if EWL_TAG_DISABLE_CONTATCT_CARD_ALIVE_TEST = FALSE*/

    EWL_ERR_CARD_CONDITIONS_NOT_SATISFIED           = -0x8035,  /**< -32821  clesscard reject transaction eg. (GPO or Final select returns 6985) or missing a mandatory data that makes transaction impossible (paypass EWL_TAG_CLESS_TRANSACTION_LIMIT or EWL_TAG_CLESS_CVM_LIMIT )*/

    EWL_ERR_SERVICE_NOT_ALLOWED                     = -0x8036,  /**< -32822 Service not allowed eg. bit service not allowed set in CID */
    EWL_ERR_CAN_NOT_FIGURE_OUT_TECHNOLOGY           = -0x8037,  /**< -32823 For some how the kernel do not figure out the card technology */
	EWL_ERR_TASK_PROBLEM 							= -0x8038,	/**< -32824 For some the ewl do not get start task */
	EWL_ERR_AID_TABLE_PROBLEM						= -0x8039,	/**< -32825 EWL can not process the AID table eg same AID to 2 different technology */
	EWL_ERR_CERTIFICATE_PROBLEM						= -0x803A,	/**< -32826 There is some problem in card certificate, eg paypass certificate expired*/
	EWL_ERR_TRANSACTION_LIMIT						= -0x803B,	/**< -32827 Value is above (or equal) transaction limit in PayWave and Discover */

    EWL_ERR_KERNEL                                  = -0x8FFE,  /**< -36862 Internal kernel error */
    EWL_ERR_INTERNAL                                = -0x8FFF,  /**< -36863 Unknown library internal error */

} ewlStatus_t;



/** Extended error value, EWL_TAG_LAST_EXTENDED_ERROR, in some application the error EWL_ERR_INVALID_CARD must be more specific*/
typedef enum {
    EWL_ERR_EXTENDED_NOT_DEFINED                    =  EWL_OK,
    EWL_ERR_EXTENDED_GPO_INVALID_SW1SW2             = -0x9000,
    EWL_ERR_EXTENDED_READ_RECORD_INVALID_SW1SW2     = -0x9001,
    EWL_ERR_EXTENDED_MISSING_MANDATORY_DATA         = -0x9002,
}ewlExtendedStatus_t;

/** Kernel status, see tag EWL_TAG_TRANSACTION_STATUS*/
typedef enum {
    EWL_STATUS_UNKNOWN              = 0, /**< Undefined status. */
    EWL_STATUS_APPROVED_OFFLINE     = 1, /**< Transaction approved in offline mode. */
    EWL_STATUS_APPROVED_ONLINE      = 2, /**< Transaction approved in online mode. */
    EWL_STATUS_DENIAL_OFFLINE       = 3, /**< Transaction denial in offline mode. */
    EWL_STATUS_DENIAL_ONLINE        = 4, /**< Transaction denial in online mode. */
    EWL_STATUS_GO_ONLINE            = 5, /**< Terminal must contact host. */
    EWL_STATUS_CONTINUE             = 6, /**< Transaction must continue eg: paypass refund or EMV refund. */
} ewlTransactionStatus_t;

/** Host answer, see tag EWL_TAG_HOST_ANSWER*/
typedef enum {
    EWL_HOST_UNKNOW     = 0,    /**< Host not contacted (unable communicate with host). */
    EWL_HOST_APPROVED   = 1,    /**< Host approve transaction. */
    EWL_HOST_DENIAL     = 2,    /**< Host denial transaction. */
} ewlHostAnswer_t;

/** Display message IDs, see callback  ewlCBDisplay_t*/
typedef enum {
    EWL_DISPLAY_OFFLINE_LAST_ATTEPMPT                           = 1,    /**< Warn card holder that next wrong offline pin will blocked the card */
    EWL_DISPLAY_OFFLINE_PIN_OK                                  = 2,    /**< Warn card holder that offline pin was accepted */
    EWL_DISPLAY_OFFLINE_PIN_ERROR                               = 3,    /**< Warn card holder that offline pin was not accepted */
    EWL_DISPLAY_OFFLINE_PIN_BLOCKED                             = 4,    /**< Warn card holder that pin offline was blocked */
    EWL_DISPLAY_APPLICATION_BLOCKED                             = 5,    /**< Warn card holder that selected application is blocked */
    EWL_DISPLAY_APPLICATION_ERROR                               = 6,    /**< Warn card holder that selected application has some error */
    EWL_DISPLAY_APPLICATION_INVALIDATED                         = 7,    /**< Warn card holder that selected application is invalidated */
    EWL_DISPLAY_APPLICATION_NON_EMV                             = 8,    /**< Warn card holder that selected application is non EMV */
    EWL_DISPLAY_OFFLINE_PIN_ALREADY_BLOCKED                     = 9,    /**< Warn card holder that offline pin was not submitted because pin already blocked (9F17 = 0) */
} ewlDisplay_t;

/** Device Type [1], see tag EWL_TAG_DEVICE_TYPE */
typedef enum{
    EWL_DEVICE_CARD             = 0,    /**< It is a normal credit card */
    EWL_DEVICE_CARD_MINI        = 1,    /**< It is a credit card with reduced dimension */
    EWL_DEVICE_NON_CARD         = 2,    /**< It is non card cless device (key fobs,watcher, etc) */
    EWL_DEVICE_MOBILE           = 3,    /**< It is a mobile device (mobile phone, tablet etc) working as cless card */
    EWL_DEVICE_UNDEFINED        = 4     /**< There is a rule to define the type but one or more tags in not available */
} ewlDeviceType_t;


/** Current transaction step, see EWL_TAG_STEP*/
typedef enum {
    EWL_STEP_WAITING_GET_CARD       = 0,   ///< Waiting for GetCard to execute
    EWL_STEP_WAITING_GO_ONCHIP      = 1,   ///< Executed GetCard, waiting for GoOnChip
    EWL_STEP_WAITING_FINISH_CHIP    = 2,   ///< Executed GoOnChip, waiting for FinishChip
    EWL_STEP_STOPPED                = 4,   ///< EWL process finished
    EWL_STEP_STOPPED_GETCARD        = 5,   ///< Abnormal stopped in GetCard Process
    EWL_STEP_STOPPED_GOONCHIP       = 6,   ///< Abnormal stopped in GoOnChip Process
    EWL_STEP_STOPPED_FINISHCHIP     = 7    ///< Abnormal stopped in FinishChip Process
} ewlStep_t;


/** Parameter used to check the component in function ewlTestComponents*/
typedef enum {

    EWL_TEST_KERNEL_CONTACT             = 0x0001,    /**< Test if all EMV contact components necessary are loaded */


    #ifdef EWL_ENABLE_KERNEL_PAYWAVE
    EWL_TEST_KERNEL_PAYWAVE             = 0x0002,    /**< Test if all PAYWAVE components necessary are loaded */
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYPASS
    EWL_TEST_KERNEL_PAYPASS             = 0x0004,    /**< Test if all PAYPASS components necessary are loaded */
    #endif

    #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    EWL_TEST_KERNEL_EXPRESSPAY          = 0x0008,    /**< Test if all EXPRESSPAY components necessary are loaded */
    #endif

    #ifdef EWL_ENABLE_KERNEL_PURE
    EWL_TEST_KERNEL_PURE                = 0x0010,    /**< Test if all PURE components necessary are loaded */
    #endif

    #ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
    EWL_TEST_KERNEL_DISCOVER_PAS        = 0x0020,    /**< Test if all DISCOVER DPAS components necessary are loaded */
    #endif

    #ifdef EWL_ENABLE_KERNEL_DISCOVER_ZIP
    EWL_TEST_KERNEL_DISCOVER_ZIP        = 0x0040,    /**< Test if all DISCOVER ZIP components necessary are loaded */
    #endif

	#ifdef EWL_ENABLE_KERNEL_JSPEEDY
	EWL_TEST_KERNEL_JSPEEDY        		= 0x0080,    /**< Test if all JSPEEDY components necessary are loaded */
	#endif

	#ifdef EWL_ENABLE_KERNEL_QUICKPASS
	EWL_TEST_KERNEL_QUICKPASS        	= 0x0100,    /**< Test if all QUICKPASS components necessary are loaded */
	#endif

    EWL_TEST_COMPONENTS =       EWL_TEST_KERNEL_CONTACT

                                #ifdef EWL_ENABLE_KERNEL_PAYWAVE
                                | EWL_TEST_KERNEL_PAYWAVE
                                #endif

                                #ifdef EWL_ENABLE_KERNEL_PAYPASS
                                | EWL_TEST_KERNEL_PAYPASS
                                #endif

                                #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
                                | EWL_TEST_KERNEL_EXPRESSPAY
                                #endif

                                #ifdef EWL_ENABLE_KERNEL_PURE
                                | EWL_TEST_KERNEL_PURE
                                #endif

                                #ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
                                | EWL_TEST_KERNEL_DISCOVER_PAS
                                #endif

                                #ifdef EWL_ENABLE_KERNEL_DISCOVER_ZIP
                                | EWL_TEST_KERNEL_DISCOVER_ZIP
                                #endif

								#ifdef EWL_ENABLE_KERNEL_JSPEEDY
								| EWL_TEST_KERNEL_JSPEEDY
								#endif

								#ifdef EWL_ENABLE_KERNEL_QUICKPASS
								| EWL_TEST_KERNEL_QUICKPASS
								#endif


}ewlTestKernelPresence_t;

/** Status of a chip application associated with an AID */
typedef enum {
    EWL_APPLICATION_OK          = 0,    ///< Application is OK
    EWL_APPLICATION_BLOCKED     = 1,    ///< Application is \e blocked
    EWL_APPLICATION_INVALIDATED = 2,    ///< Application has been \e invalidated
    EWL_APPLICATION_NON_EMV     = 3,    ///< Application is not EMV
    EWL_APPLICATION_ERROR       = 4     ///< Error accessing application
} ewlAIDStatus_t;


/** Reason of advice required by card */
typedef enum {
    EWL_ADVICE_REASON_NO_INFORMATION                = 0,
    EWL_ADVICE_REASON_SERVICE_NOT_ALLOWED           = 1,
    EWL_ADVICE_REASON_PIN_TRY_LIMIT_EXCEEDED        = 2,
    EWL_ADVICE_REASON_ISSUER_AUTHETICATION_FAILED   = 3,
    EWL_ADVICE_REASON_RFU                           = 4
}ewlAdviceReason_t;


/** Mask data to enable card detection type  */
/* eg: support only Iso A and B
 *
 * byte temp[EWL_TAG_CLESS_DETECTION_TYPE_LEN]
 * temp[EMV_DETECTION_BYTE_ISO_A] |= EMV_DETECTION_BIT_ISO_A;
 * temp[EMV_DETECTION_BYTE_ISO_B] |= EMV_DETECTION_BIT_ISO_B
 * ewlSetParameter(handle,EWL_TAG_CLESS_DETECTION_TYPE,temp,sizeof(temp));
 *
 */
typedef enum{
    /* Byte*/
	EMV_DETECTION_BYTE_ISO_A   				= DETECTION_BYTE_ISO_A,
	EMV_DETECTION_BYTE_ISO_B    			= DETECTION_BYTE_ISO_B,
	EMV_DETECTION_BYTE_ISO_BB   			= DETECTION_BYTE_ISO_BB,
	EMV_DETECTION_BYTE_MIFARE_A   			= DETECTION_BYTE_MIFARE_A,
	EMV_DETECTION_BYTE_STM   				= DETECTION_BYTE_STM,
	EMV_DETECTION_BYTE_FELICA  				= DETECTION_BYTE_FELICA,
	EMV_DETECTION_BYTE_MIFARE_ULTRA_LIGHT_A = DETECTION_BYTE_MIFARE_ULTRA_LIGHT_A,
	EMV_DETECTION_BYTE_CALYPSO_B_PRIME   	= DETECTION_BYTE_CALYPSO_B_PRIME,
	EMV_DETECTION_BYTE_MIFARE_4K_A   		= DETECTION_BYTE_MIFARE_4K_A,
	EMV_DETECTION_BYTE_STM_B   				= DETECTION_BYTE_STM,

	/* Bit */
	EMV_DETECTION_BIT_ISO_A   				= DETECTION_MASK_ISO_A,
	EMV_DETECTION_BIT_ISO_B    				= DETECTION_MASK_ISO_B,
	EMV_DETECTION_BIT_ISO_BB   				= DETECTION_MASK_ISO_BB,
	EMV_DETECTION_BIT_MIFARE_A   			= DETECTION_MASK_MIFARE_A,
	EMV_DETECTION_BIT_STM   				= DETECTION_MASK_STM,
	EMV_DETECTION_BIT_FELICA  				= DETECTION_MASK_FELICA,
	EMV_DETECTION_BIT_MIFARE_ULTRA_LIGHT_A  = DETECTION_MASK_MIFARE_ULTRA_LIGHT_A,
	EMV_DETECTION_BIT_CALYPSO_B_PRIME   	= DETECTION_MASK_CALYPSO_B_PRIME,
	EMV_DETECTION_BIT_MIFARE_4K_A   		= DETECTION_MASK_MIFARE_4K_A,
	EMV_DETECTION_BIT_STM_B   				= DETECTION_MASK_STM,

}ewlClessCardTypeMask_t;

typedef enum{
    EWL_TRANSACTION_TYPE_AUTORIZATION       = 0x00,
    EWL_TRANSACTION_TYPE_CASH               = 0x01,
    EWL_TRANSACTION_TYPE_DEBIT_ADJUSTMENT   = 0x02,
    EWL_TRANSACTION_TYPE_CASHBACK           = 0x09,
    EWL_TRANSACTION_TYPE_REFUND             = 0x20,
    EWL_TRANSACTION_TYPE_PURE_GET_DATA      = 0x78,
    EWL_TRANSACTION_TYPE_PURE_PUT_DATA      = 0x79
}ewltransactionType_t;


/* [1] This definition was obtained from CIELO (Brazil) and may be subject to reevaluation*/
/* [2] If this occur in ewlFinishChip follow instruction 9.3 Book 3 v4.3, If
 *     it occurs after the second GENERATE AC command, all processing for the transaction has been completed, and the cryptogram returned
 *     shall be treated as an AAC
 * [3] This error only occur if the tag EWL_TAG_ENABLE_EXTENDED_ERROR is TRUE
 */

#endif

