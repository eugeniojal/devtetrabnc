#ifndef EWLTAGS_H
#define EWLTAGS_H

#include <EmvLib_Tags.h>

#include "GTL_SharedExchange.h"
#include "Cless_LowLevel.h"

#ifdef EWL_ENABLE_KERNEL_PAYWAVE
    #include "payWave_Tags.h"
    #include "payWave_API.h"
#endif

#ifdef EWL_ENABLE_KERNEL_PAYPASS
    #include "PayPass_Tags.h"
    #include "PayPass3_API.h"
    #include "PayPass_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    #include "ExpressPay_Tags.h"
    #include "ExpressPay3_API.h"
    #include "ExpressPay_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_PURE
    #include "PURE_Tags.h"
    #include "PURE_API.h"
    #include "PURE_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
    #include "DiscoverDPAS_Tags.h"
    #include "DiscoverDPAS_API.h"
    #include "DiscoverDPAS_API_Common.h"
#endif

#ifdef EWL_ENABLE_KERNEL_DISCOVER_ZIP
    #include "Discover_Tags.h"
    #include "Discover_API.h"
#endif

#ifdef EWL_ENABLE_KERNEL_JSPEEDY
    #include "JCB_Tags.h"
    #include "JCB_API.h"
#endif

#ifdef EWL_ENABLE_KERNEL_QUICKPASS
    #include "QuickPass_Tags.h"
    #include "QuickPass_API.h"
#endif


enum ewlCommonLen_t {
    EWL_LABEL_MAX_LEN                           = 16,      /**< ASCII */
    EWL_TAC_LEN                                 = 5,
    EWL_IAC_LEN                                 = EWL_TAC_LEN,
    EWL_BCD_VALUE_LEN                           = 6,
    EWL_BINARY_VALUE_LEN                        = 4,
    EWL_KERNEL_VERSION_LEN                      = 5,      /**< ASCII */
	EWL_TTQ_LEN									= 4,

    #ifdef  __TELIUM3__
    EWL_KERNEL_VERSION_EXT_LEN                  = 8,      /**< ASCII */
    #endif

    EWL_AID_MAX_LEN                             = 16
};

// Note: Define tag and tag length was constructed follow the rule.
// EWL_kernel_name              -    Tag used in this library (kernel == TAG EWL internal tag)
// EWL_kernel_name_MAX_LEN      -    Tag maximum length used in this library. (this tag has variable length according to kernel specification)
// EWL_kernel_name_LEN          -    Tag length used in this library. (this tag has fixed length according to kernel specification)

/** Library proprietary tags */
enum ewlProprietaryTags_t {
    EWL_TAG_FORCE_ONLINE                            	    = 0xDF808000, /**< If \e true force EMV transaction to go on-line, even if the normal processing would be approved off-line  - default FALSE*/
    EWL_TAG_CLESS_TRANSACTION_LIMIT                 	    = 0xDF808001, /**< C'less Transaction Limit (BCD, length 6 bytes ) */
    EWL_TAG_CLESS_FLOOR_LIMIT                       	    = 0xDF808002, /**< C'less Floor Limit (BCD, length 6 bytes) */
    EWL_TAG_CLESS_CVM_LIMIT                         	    = 0xDF808003, /**< C'less CVM Limit (BCD, length 6 bytes) */
    EWL_TAG_TERMINAL_APPLICATION_VERSION_LIST       	    = 0xDF808004, /**< Application version list (hex , must be multiple of 2) */
    EWL_TAG_SUPPORT_ZERO_AMOUNT                     	    = 0xDF808005, /**< If \e true, allows the C'less transaction to start with zero amount - default false */
    EWL_TAG_TAC_DEFAULT                             	    = 0xDF808006, /**< TAC Default (Binary, length 5 bytes) */
    EWL_TAG_TAC_DENIAL                              	    = 0xDF808007, /**< TAC Denial (Binary, length 5 bytes) */
    EWL_TAG_TAC_ONLINE                              	    = 0xDF808008, /**< TAC Online (Binary, length 5 bytes) */
    EWL_TAG_PK_MODULUS                              	    = 0xDF808009, /**< Certification authority public Key Modulus (Binary, up to 248 bytes) */
    EWL_TAG_PK_EXP                                  	    = 0xDF80800A, /**< Certification authority public Key Exponent (Binary, up to 3 bytes) */
    EWL_TAG_PAN_IN_EXCEPTION_FILE                   	    = 0xDF80800B, /**< If \e true, PAN is in exception file */
    EWL_TAG_TARGET_PERC_RAND_SEL                    	    = 0xDF80800C, /**< Target percentage to be used for biased random selection (BCD value length 1 byte) */
    EWL_TAG_THRESHOLD_VALUE_BIASED_RAND_SEL         	    = 0xDF80800D, /**< Threshold value to be used for biased random selection (Binary value length 4 byte) */
    EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL         	    = 0xDF80800E, /**< Maximum target percentage to be used for biased random selection (BCD value length 1 byte) */
    EWL_TAG_DEFAULT_DDOL                            	    = 0xDF80800F, /**< Default DDOL  (Binary value length variable) */
    EWL_TAG_DEFAULT_TDOL                            	    = 0xDF808010, /**< Default TDOL  (Binary value length variable) */
    EWL_TAG_PK_LIST                                 	    = 0xDF808011, /**< Certification authority public key list (Hexadecimal value length variable) */
    EWL_TAG_HOST_ANSWER                             	    = 0xDF808012, /**< Host answer See: \ref ewlHostAnswer_t */
    EWL_TAG_CLESS_CAM_MANDATORY                     	    = 0xDF808014, /**< CAM mandatory in C'less operation (only PURE operations) */
    EWL_TAG_CLESS_EMV_CVM_SUPPORTED                 	    = 0xDF808015, /**< EMV CVM supported in C'less operation (only PURE operations) */
    EWL_TAG_CLESS_MOBILE_SUPPORT                    	    = 0xDF808016, /**< Terminal support mobile transaction only to Expresspay - default - TRUE*/
    EWL_TAG_CLESS_MOBILE_CVM_SUPPORT                	    = 0xDF808017, /**< Terminal support mobile CVM  - default TRUE*/
    EWL_TAG_CLESS_RETRIVAL_BALANCE                  	    = 0xDF808018, /**< Retrieval of Card Balance (only PURE operations) */
    EWL_TAG_CLESS_STOP_DECLINED_GPO                 	    = 0xDF808019, /**< Stop transaction in GPO if CCID indicate decline transaction (only PURE operations) */
    EWL_TAG_CLESS_AUTH_SELECT_BY_AQUIRE             	    = 0xDF80801A, /**< Application authentication transaction is select by Aquirer  (only PURE operations) */
    EWL_TAG_CLESS_SUPPORT_SECOND_TAP                	    = 0xDF80801B, /**< Support Double TAP (only PURE operations) */
    EWL_TAG_CLESS_SUPPORT_LONG_TAP                  	    = 0xDF80801C, /**< Support Long TAP (only PURE operations) */
    EWL_TAG_CLESS_SUPPORT_ECHO                      	    = 0xDF80801D, /**< Support ECHO Command (only PURE operations) */
    EWL_TAG_MANDATORY_TRM                           	    = 0xDF80801E, /**< Kernel must execute TRM ignoring AIP (PURE and EMV operations) - default TRUE */
    EWL_TAG_INTERNAL_TRANSACTION_TYPE               	    = 0xDF80801F, /**< Internal Transaction Type [9]*/
    EWL_TAG_SUBSEQUENT_PIN_BYPASS                   	    = 0xDF808020, /**< Enable subsequent pin bypass - default TRUE */
    EWL_TAG_SUPPORT_OTHER_CLESS_TYPES               	    = 0xDF808021, /**< Inform to kernel which the developer will process other kind of cless card in the application - default FALSE */
    EWL_TAG_CALLBACK_DISPLAY                        	    = 0xDF808022, /**< Set ewlDisplay Callback address [5]*/
    EWL_TAG_CALLBACK_SELECTION_MENU                 	    = 0xDF808023, /**< Set ewlSelectionMenu Callback address [5]*/
    EWL_TAG_CALLBACK_GET_PARAMETER                  	    = 0xDF808024, /**< Set ewlGetParameter Callback address  [5]*/
    EWL_TAG_CALLBACK_GET_PUBLIC_KEY                 	    = 0xDF808025, /**< Set ewlGetPublicKey Callback address [5]*/
    EWL_TAG_CALLBACK_GET_PIN_ONLINE                 	    = 0xDF808026, /**< Set ewlGetPinOnline Callback address [5]*/
    EWL_TAG_CALLBACK_GET_PIN_OFFLINE                	    = 0xDF808027, /**< Set ewlGetPinOffLine Callback address [5]*/
    EWL_TAG_CALLBACK_FINISH_CVM                     	    = 0xDF808028, /**< Set ewlFinishCVMProcessing Callback address [5]*/
    EWL_TAG_CALLBACK_LED                            	    = 0xDF808029, /**< Set ewlLeds Callback address [5]*/
    EWL_TAG_CALLBACK_REMOVE_CARD                    	    = 0xDF80802A, /**< Set ewlRemoveCard Callback address [5]*/
    EWL_TAG_FORCE_SELECTION                         	    = 0xDF80802B, /**< Force selection confirmation */
    EWL_TAG_MERCHANT_FORCED_ONLINE                  	    = 0xDF80802C, /**< Set flag merchant forced on line */
    EWL_TAG_EXPRESPAY_1_COMPATIBILITY               	    = 0xDF80802D, /**< Active Expresspay 1 compatibility - default FALSE [8] */
    EWL_TAG_LAST_TRANSACTION_AMOUNT                 	    = 0xDF80802E, /**< It is the amount of the last transaction performed with the same card (PAN and PAN Sequence Number matching), and present in the terminal transaction file. */
    EWL_TAG_DISABLE_CONTACT_IN_CLESS_TRANSAC        	    = 0xDF80802F, /**< If true, contact interface will not be checked in cless operations, this check was necessary because the test (qVSDC50) default FALSE (use contact task). */
    EWL_TAG_PAYPASS_EXCLUSIVE_MCHIP                 	    = 0xDF808030, /**< Deprecated see EWL_TAG_PAYPASS_EXCLUSIVE_MCHIP */
	EWL_TAG_EXCLUSIVE_CLESS_EMV							    = 0xDF808030, /**< Normally ewl allowed Mstrip or MSD when terminal configured to MCHIP/qVSDC, if this is set to TRUE, this is not more allowed ( eg MP92 Test 1 Scenario 01)  - default FALSE*/
    EWL_TAG_FORCE_5A_AND_5F24                       	    = 0xDF808032, /**< If there are not 5A or 5F24 in paywave technology we recover this information from track2 - default TRUE */
    EWL_TAG_FORBIDDEN_APPROVE_OFF_IN_2GEN           	    = 0xDF818033, /**< Avoid offline approved transaction in second generate, note the only way to this occur (approved off in second generate) is the terminal can not contact the host - default FALSE */
    EWL_TAG_PURE_IMPLEMENTATION_OPTIONS             	    = 0xDF818034, /**< Pure  implementation option default 0x80*/
    EWL_TAG_REFUND_RECOMMENDATION                   	    = 0xDF818035, /**< Execute a refund following EMV recommendation (do not pass through CVM and call AAC in first generate)  - default FALSE*/
    EWL_TAG_TERMINAL_SUPPORTED_LANGUAGES            	    = 0xDF818036, /**< List of languages supported by the terminal  (default "ptspenfrde" ) */
    EWL_TAG_CLESS_TRANSACTION_CDCVM_LIMIT           	    = 0xDF808037, /**< C'less Transaction Limit CDCVM (BCD, length 6 bytes ) */
	EWL_TAG_CLESS_ISSUER_AUTENTICATION_SUPPORT      	    = 0xDF808038, /**< Terminal support cless Issuer authentication - default TRUE */
	EWL_TAG_PAYPASS_DENIAL_UNDEFINE_SITUATION			    = 0xDF808039, /**< In paypass documentation if occur a CDA error after Generate AC the kernel enter in a undefined situation, if this flag == True, the kernel will return denial if enter in this situation (Book C-2 spec 2.6 pag 298) - default FALSE */
	EWL_TAG_CLESS_DETECTION_TYPE						    = 0xDF80803A, /**< Set the cless card type detected by EWL see ewlClessCardTypeMask_t set to 0xFF (this configuration do not works in SDK 11.12)*/
	EWL_TAG_CLESS_DETECTION_TYPE_FILTER					    = 0xDF80803B, /**< Set a internal filter, use only under EWL developer  instruction */
	EWL_TAG_DISABLE_CONTATCT_CARD_ALIVE_TEST			    = 0xDF80803C, /**< Disable contact alive test (default FALSE) */
	EWL_TAG_CARD_REMOVE_EVENT_INFO						    = 0xDF80803D, /**< Allowed the user detect the card remove during ewlCBSelectionMenu_t, ewlCBGetPinOnLine_t, ewlCBGetPinOffLine_t, ewlCBFinishCVMProcessing_t */
	EWL_TAG_EXPRESSPAY_ALLOW_EQUAL_TRANSACTION_LIMIT	    = 0xDF80803E, /**< Due specification divergence, we need plus 1 to Expresspay Transaction Limit in some situation (AXP EP 005) - default TRUE*/
	EWL_TAG_CLESS_MOBILE_SECOND_TRY                         = 0xDF80803F, /**< Set if is the second time which the cardholder must approach the card*/
    EWL_TAG_IGNORE_NO_CVM_IN_PAYPASS_TERMINAL_CAPABILITIES  = 0xDF818040, /**< If this flag is true EWl will ignore the bit no cvm in terminal capabilities - default TRUE*/
	EWL_TAG_THRESHOLD_VALUE_BIASED_RAND_SEL_NUM				= 0xDF818041, /**< Threshold value to be used for biased random selection (BCD value length 6 byte) */
	EWL_TAG_OTP_COLOMBIA_ACTIVE								= 0xDF818042, /**< Active NFC OTP to Colombia*/
	EWL_TAG_RRP_ENABLE										= 0xDF818043, /**< RRP enable or not - default TRUE (if this flag is false we are in disagreement with the paypass specification, do not disable unless explicit order or perfect knowledge of this issues and side effects )*/
	EWL_TAG_ENABLE_TRANSACTION_LIMIT_ERROR					= 0xDF818044, /**< Enable Transaction Error limit in PayWave and Discover insted of return EWL_ERR_NO_COMPATIBLE_APP - default FALSE*/

    // output tags
    EWL_TAG_REQUESTED_SIGNATURE                     	= 0xDF818000, /**< Flag, if TRUE transaction requested card holder signature */
    EWL_TAG_REQUESTED_PIN_ONLINE                    	= 0xDF818001, /**< Flag, if TRUE transaction requested pin online */
    EWL_TAG_REQUESTED_PIN_OFFLINE                   	= 0xDF818002, /**< Flag, if TRUE transaction requested pin offline */
    EWL_TAG_INTERFACE                               	= 0xDF818003, /**< Transaction interface See: \ref ewlInterfaceType_t */
    EWL_TAG_PIN_OFFLINE_REMAIN                      	= 0xDF818004, /**< Number of remaining attempts to pin offline (Hexadecimal value 1 byte) */
    EWL_TAG_PIN_IS_BLOCKED                          	= 0xDF818005, /**< Flag, if TRUE card is blocked */
    EWL_TAG_TRANSACTION_STATUS                      	= 0xDF818006, /**< Transaction status See: \ref ewlTransactionStatus_t */
    EWL_TAG_TRACK1_DATA                             	= 0xDF818007, /**< Track1 (ASCII up to 79 bytes without sentinels) available in some context */
    EWL_TAG_TRACK2_DATA                             	= 0xDF818008, /**< Track2 (ASCII up to 37 bytes without sentinels use separator '=' ) available in some context (if tag 57 present library will convert to ASCII )*/
    EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT       	= 0xDF818009, /**< Available offline spending amount ( BCD value length 6 bytes ) */
    EWL_TAG_ISSUER_SCRIPT_RESULT                    	= 0xDF81800A, /**< Issuer Script Results ( Binary value length variable) */
    EWL_TAG_PIN_BLOCKED_UPON_INITIAL_TRY            	= 0xDF81800B, /**< Flag, if TRUE pin offline was blocked before start transaction */
    EWL_TAG_LABEL                                   	= 0xDF81800C, /**< Label used (preference label,if possible, or label ) */
    EWL_TAG_TRACK1_DATA_ASSEMBLED                   	= 0xDF81800D, /**< Track1 (ASCII up to 79 bytes without sentinels) available in some context if not will be assembled using available data*/
    EWL_TAG_TRACK2_DATA_ASSEMBLED                   	= 0xDF81800E, /**< Track2 (ASCII up to 37 bytes without sentinels use separator '=' ) available in some context if not will be assembled using available data*/
    EWL_TAG_RID                                     	= 0xDF81800F, /**< Application RID */
    EWL_TAG_SECOND_TAP_REQUEST                      	= 0xDF818010, /**< Inform if the cless transaction will request second tap */
    EWL_TAG_SECOND_TAP_EXECUTED                     	= 0xDF818011, /**< Inform if the cless transaction second tap was executed with or without success*/
    EWL_TAG_DEVICE_TYPE                             	= 0xDF818012, /**< Return the cless device type, see ewlDeviceType_t */
    EWL_TAG_REQUESTED_ON_DEVICE_VERIFICATION        	= 0xDF818013, /**< Flag, if TRUE transaction requested on device verification */
    EWL_TAG_STEP                                    	= 0xDF818014, /**< Get EWL Step, see ewlStep_t */
    EWL_TAG_LAST_FLOW_ERROR                         	= 0xDF818015, /**< Get last error occurred in functions ewlGetCard, ewlGoOnChip, and ewlFinishChip */
    EWL_TAG_EXTENDED_ERROR                          	= 0xDF818016, /**< Enable extended error */
    EWL_TAG_PAN_ASCII                               	= 0xDF818017, /**< Get PAN in ASCII format */
    EWL_TAG_PAN_ASCII_ASSEMBLED                     	= 0xDF818018, /**< Get PAN in ASCII format if tag 5A not exist recover from track1 or track2 (this situation can occur only in paywave transactions)*/
    EWL_TAG_ADVICE_REQUESTED_BY_CARD                	= 0xDF818019, /**< Check if a advice was requested by card */
    EWL_TAG_ADVICE_REASON                           	= 0xDF81801A, /**< Return the reason of advice, if required.*/
    EWL_TAG_CLESS_KERNEL_STATUS                     	= 0xDF81801B, /**< Return cless kernel status.*/
	EWL_TAG_CLESS_BF0C_FROM_PPSE						= 0xDF81801C, /**< Is BC09 from PPSE response in Cless transactions[13] */
	EWL_TAG_CARD_COMMOM_CORE_IDENTIFIER                 = 0xDF81801D, /**< Common Core Identifier (part of 9F10)*/
	EWL_TAG_DERIVATION_KEY_INDEX                        = 0xDF81801E, /**< Derivation Key Index (part of 9F10)*/
	EWL_TAG_CARD_VERIFICATION_RESULT                    = 0xDF81801F, /**< Card verification result format A (part of 9F10)*/
	EWL_TAG_CARD_BIN								    = 0xDF818020, /**< Card BIN first digits of PAN */
	EWL_TAG_CARD_PAN_LAST_FOUR						    = 0xDF818021, /**< Card last 4 digits of PAN */
	EWL_TAG_CARD_AID									= 0xDF818022, /**< Return Card AID return aid selected (tag 4F primary or tag 84 ) */
	EWL_TAG_OTP_COLOMBIA_RESULT							= 0xDF818023, /**< Response of Colombias's OTP result */


    // input/output tags
    EWL_TAG_TECHNOLOGY                              	= 0xDF828001, /**< Card technology, see: \ref ewlTechnology_t */

    // versions tags
    EWL_TAG_LIBRARY_VERSION                         	= 0xDF838000, /**< EWL version ( ASCII value length up to 5 bytes ) */
    EWL_TAG_EMV_KERNEL_VERSION                      	= 0xDF838001, /**< EMV kernel version ( ASCII value length up to 5 bytes) [7] */
    EWL_TAG_EMV_API_VERSION                         	= 0xDF838002, /**< EMV API version ( ASCII value length up to 5 bytes) */
    EWL_TAG_ENTRY_POINT_VERSION                     	= 0xDF838003, /**< C'less entry point version ( ASCII value length up to 5 bytes) */
    EWL_TAG_PAYPASS_VERSION                         	= 0xDF838004, /**< PayPass kernel version ( ASCII value length up to 5 bytes) [7] */
    EWL_TAG_PAYWAVE_VERSION                         	= 0xDF838005, /**< PayWave kernel version ( ASCII value length up to 5 bytes) [7] */
    EWL_TAG_EXPRESSPAY_VERSION                      	= 0xDF838006, /**< ExpressPay2 kernel version ( ASCII value length up to 5 bytes) [7]*/
    EWL_TAG_PURE_VERSION                            	= 0xDF838008, /**< PURE kernel version ( ASCII value length up to 5 bytes) [7]*/
    EWL_TAG_MODULE_LIB_VERSION                      	= 0xDF838009, /**< EWL Module Lib version ( ASCII value length up to 5 bytes) [6]*/
    EWL_TAG_MODULE_VERSION                          	= 0xDF83800A, /**< EWL Module version ( ASCII value length up to 5 bytes) [6]*/
    EWL_TAG_DISCOVER_PAS_VERSION                    	= 0xDF83800B, /**< Discover PAS kernel version ( ASCII value length up to 5 bytes) [7]*/
    //EWL_TAG_DLL_VERSION                           	= 0xDF83800C, /**< Reserved */
    //EWL_TAG_DLL_LIB_VERSION                       	= 0xDF83800D, /**< Reserved */
    EWL_TAG_DISCOVER_ZIP_VERSION                    	= 0xDF83800E, /**< Discover ZIP kernel version ( ASCII value length up to 5 bytes) [7]*/
    EWL_TAG_DEBUG_ID                                    = 0xDF83800F, /**< Return the debug ID of EWL debug*/
	EWL_TAG_JSPEEDY_VERSION								= 0xDF838010, /**< JSpeedy kernel version ( ASCII value length up to 5 bytes) [7] */
	EWL_TAG_QUICKPASS_VERSION							= 0xDF838011, /**< quickpass kernel version ( ASCII value length up to 5 bytes) [7] */

    // generic user tags
    EWL_TAG_USER_DATA_1                             	= 0xDF848000, /**< Value defined by user (length variable) */
    EWL_TAG_USER_DATA_2                             	= 0xDF848001, /**< Value defined by user (length variable) */
    EWL_TAG_USER_DATA_3                             	= 0xDF848002, /**< Value defined by user (length variable) */
};

// If tag EWL_EXPRESSPAY_TRANSACTION_CAPABILITIES is not present this tag can be defined by :
// - EWL_EMV_TERMINAL_CAPABILITIES
// - EWL_EMV_TERMINAL_TYPE
// - EWL_TAG_CLESS_MOBILE_SUPPORT
// - EWL_TAG_CLESS_MOBILE_CVM_SUPPORT

// If tag EWL_PURE_TTPI  is not present this tag can be defined by :
// - EWL_EMV_TERMINAL_TYPE
// - EWL_EMV_TERMINAL_CAPABILITIES
// - EWL_TAG_CLESS_SUPPORT_ECHO
// - EWL_TAG_CLESS_SUPPORT_LONG_TAP
// - EWL_TAG_CLESS_SUPPORT_SECOND_TAP
// - EWL_TAG_CLESS_MOBILE_CVM_SUPPORT
// - EWL_TAG_CLESS_RETRIVAL_BALANCE
// - EWL_TAG_CLESS_STOP_DECLINED_GPO
// - EWL_TAG_CLESS_AUTH_SELECT_BY_AQUIRE
// - EWL_TAG_CLESS_CAM_MANDATORY
// - EWL_TAG_CLESS_EMV_CVM_SUPPORTED
// - EWL_TAG_MANDATORY_TRM


enum ewlProprietaryTagsLen_t {
    EWL_TAG_FORCE_ONLINE_LEN                            		= sizeof(bool),
    EWL_TAG_CLESS_TRANSACTION_LIMIT_LEN                 		= EWL_BCD_VALUE_LEN,
    EWL_TAG_CLESS_FLOOR_LIMIT_LEN                       		= EWL_BCD_VALUE_LEN,
    EWL_TAG_CLESS_CVM_LIMIT_LEN                         		= EWL_BCD_VALUE_LEN,
    //EWL_TAG_TERMINAL_APPLICATION_VERSION_LIST_LEN     		= undefined,
    EWL_TAG_SUPPORT_ZERO_AMOUNT_LEN                     		= sizeof(bool),
    EWL_TAG_TAC_DEFAULT_LEN                             		= EWL_TAC_LEN,
    EWL_TAG_TAC_DENIAL_LEN                              		= EWL_TAC_LEN,
    EWL_TAG_TAC_ONLINE_LEN                              		= EWL_TAC_LEN,
    EWL_TAG_PK_MODULUS_MAX_LEN                          		= 248,
    EWL_TAG_PK_EXP_MAX_LEN                              		= 3,
    EWL_TAG_PAN_IN_EXCEPTION_FILE_LEN                   		= sizeof(bool),
    EWL_TAG_TARGET_PERC_RAND_SEL_LEN                    		= 1,
    EWL_TAG_THRESHOLD_VALUE_BIASED_RAND_SEL_LEN         		= EWL_BINARY_VALUE_LEN,
    EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL_LEN         		= 1,
    //EWL_TAG_DEFAULT_DDOL_LEN                          		= undefined,
    //EWL_TAG_DEFAULT_TDOL                              		= undefined,
    //EWL_TAG_PK_LIST_LEN                               		= undefined,
    EWL_TAG_HOST_ANSWER_LEN                             		= sizeof(ewlHostAnswer_t),
    //EWL_TAG_CLESS_CVM_REQUIRED_LEN                    		= sizeof(bool),
    EWL_TAG_CLESS_CAM_MANDATORY_LEN                     		= sizeof(bool),
    EWL_TAG_CLESS_EMV_CVM_SUPPORTED_LEN                 		= sizeof(bool),
    EWL_TAG_CLESS_MOBILE_SUPPORT_LEN                    		= sizeof(bool),
    EWL_TAG_CLESS_MOBILE_CVM_SUPPORT_LEN                		= sizeof(bool),
    EWL_TAG_CLESS_RETRIVAL_BALANCE_LEN                  		= sizeof(bool),
    EWL_TAG_CLESS_STOP_DECLINED_GPO_LEN                 		= sizeof(bool),
    EWL_TAG_CLESS_SUPPORT_SECOND_TAP_LEN                		= sizeof(bool),
    EWL_TAG_CLESS_SUPPORT_LONG_TAP_LEN                  		= sizeof(bool),
    EWL_TAG_CLESS_SUPPORT_ECHO_LEN                      		= sizeof(bool),
    EWL_TAG_MANDATORY_TRM_LEN                           		= sizeof(bool),
    EWL_TAG_INTERNAL_TRANSACTION_TYPE_LEN               		= 1,
    EWL_TAG_SUBSEQUENT_PIN_BYPASS_LEN                   		= sizeof(bool),
    EWL_TAG_SUPPORT_OTHER_CLESS_TYPES_LEN               		= sizeof(bool),
    EWL_TAG_CALLBACK_DISPLAY_LEN                        		= sizeof(void*),
    EWL_TAG_CALLBACK_SELECTION_MENU_LEN                 		= sizeof(ewlStatus_t*),
    EWL_TAG_CALLBACK_GET_PARAMETER_LEN                  		= sizeof(ewlStatus_t*),
    EWL_TAG_CALLBACK_GET_PUBLIC_KEY_LEN                 		= sizeof(ewlStatus_t*),
    EWL_TAG_CALLBACK_GET_PIN_ONLINE_LEN                 		= sizeof(ewlStatus_t*),
    EWL_TAG_CALLBACK_GET_PIN_OFFLINE_LEN                		= sizeof(ewlStatus_t*),
    EWL_TAG_CALLBACK_FINISH_CVM_LEN                     		= sizeof(ewlStatus_t*),
    EWL_TAG_CALLBACK_LED_LEN                            		= sizeof(void*),
    EWL_TAG_CALLBACK_REMOVE_CARD_LEN                    		= sizeof(void*),
    EWL_TAG_FORCE_SELECTION_LEN                         		= sizeof(bool),
    EWL_TAG_MERCHANT_FORCED_ONLINE_LEN                  		= sizeof(bool),
    EWL_TAG_EXPRESPAY_1_COMPATIBILITY_LEN               		= sizeof(bool),
    EWL_TAG_LAST_TRANSACTION_AMOUNT_LEN                 		= 8,
    EWL_TAG_DISABLE_CONTACT_IN_CLESS_TRANSAC_LEN        		= sizeof(bool),
    EWL_TAG_PAYPASS_EXCLUSIVE_MCHIP_LEN                 		= sizeof(bool),
	EWL_TAG_EXCLUSIVE_CLESS_EMV_LEN								= sizeof(bool),
    EWL_TAG_FORCE_5A_AND_5F24_LEN                       		= sizeof(bool),
    EWL_TAG_FORBIDDEN_APPROVE_OFF_IN_2GEN_LEN           		= sizeof(bool),
    EWL_TAG_PURE_IMPLEMENTATION_OPTIONS_LEN             		= 1,
    EWL_TAG_REFUND_RECOMMENDATION_LEN                   		= sizeof(bool),
    EWL_TAG_TERMINAL_SUPPORTED_LANGUAGES_MAX_LEN        		= 64,
    EWL_TAG_CLESS_TRANSACTION_CDCVM_LIMIT_LEN           		= EWL_BCD_VALUE_LEN,
	EWL_TAG_CLESS_ISSUER_AUTENTICATION_SUPPORT_LEN	    		= sizeof(bool),
	EWL_TAG_PAYPASS_DENIAL_UNDEFINE_SITUATION_LEN       		= sizeof(bool),
	EWL_TAG_CLESS_DETECTION_TYPE_LEN							= sizeof(uint32_t), /** more information see ewlClessCardTypeMask_t*/
	EWL_TAG_CLESS_DETECTION_TYPE_FILTER_LEN             		= sizeof(uint32_t),
	EWL_TAG_DISABLE_CONTATCT_CARD_ALIVE_TEST_LEN				= sizeof(bool),
	EWL_TAG_CARD_REMOVE_EVENT_INFO_LEN							= sizeof(ewlEventInfo_t),
	EWL_TAG_EXPRESSPAY_ALLOW_EQUAL_TRANSACTION_LIMIT_LEN		= sizeof(bool),
	EWL_TAG_CLESS_MOBILE_SECOND_TRY_LEN                     	= sizeof(bool),
	EWL_TAG_IGNORE_NO_CVM_IN_PAYPASS_TERMINAL_CAPABILITIES_LEN  = sizeof(bool),
	EWL_TAG_THRESHOLD_VALUE_BIASED_RAND_SEL_NUM_LEN				= EWL_BCD_VALUE_LEN,
	EWL_TAG_OTP_COLOMBIA_ACTIVE_LEN								= sizeof(bool),
	EWL_TAG_RRP_ENABLE_LEN										= sizeof(bool),
	EWL_TAG_ENABLE_TRANSACTION_LIMIT_ERROR_LEN					= sizeof(bool),


    EWL_TAG_REQUESTED_SIGNATURE_LEN                     		= sizeof(bool),
    EWL_TAG_REQUESTED_PIN_ONLINE_LEN                    		= sizeof(bool),
    EWL_TAG_REQUESTED_PIN_OFFLINE_LEN                   		= sizeof(bool),
    EWL_TAG_INTERFACE_LEN                               		= sizeof(ewlInterfaceType_t),
    EWL_TAG_PIN_OFFLINE_REMAIN_LEN                      		= 1,
    EWL_TAG_PIN_IS_BLOCKED_LEN                          		= sizeof(bool),
    EWL_TAG_TRANSACTION_STATUS_LEN                     	 		= sizeof(ewlTransactionStatus_t),
    EWL_TAG_TRACK1_DATA_MAX_LEN                         		= 76,                       /**< [1] */
    EWL_TAG_TRACK2_DATA_MAX_LEN                         		= 37,                       /**< [1] */
    EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT_LEN       		= EWL_BCD_VALUE_LEN,
    EWL_TAG_ISSUER_SCRIPT_RESULT_LEN                    		= 128,                      /**< this length is define in book 4 version 4.3 item 6.3.9 */
    EWL_TAG_PIN_BLOCKED_UPON_INITIAL_TRY_LEN            		= sizeof(bool),
    EWL_TAG_LABEL_MAX_LEN                               		= EWL_LABEL_MAX_LEN,        /**< [1] */
    EWL_TAG_TRACK1_DATA_ASSEMBLED_MAX_LEN               		= 76,                       /**< [1] */
    EWL_TAG_TRACK2_DATA_ASSEMBLED_MAX_LEN               		= 36,                       /**< [1] */
    EWL_TAG_RID_LEN                                     		= 5,
    EWL_TAG_SECOND_TAP_REQUEST_LEN                      		= sizeof(bool),
    EWL_TAG_SECOND_TAP_EXECUTED_LEN                     		= sizeof(bool),
    EWL_TAG_DEVICE_TYPE_LEN                             		= sizeof(ewlDeviceType_t),
    EWL_TAG_REQUESTED_ON_DEVICE_VERIFICATION_LEN        		= sizeof(bool),
    EWL_TAG_STEP_LEN                                    		= sizeof(ewlStep_t),
    EWL_TAG_LAST_FLOW_ERROR_LEN                         		= sizeof(ewlStatus_t),
    EWL_TAG_EXTENDED_ERROR_LEN                          		= sizeof(ewlExtendedStatus_t),
    EWL_TAG_PAN_ASCII_MAX_LEN                           		= 19,                       /**< [1] */
    EWL_TAG_PAN_ASCII_ASSEMBLED_MAX_LEN                 		= 19,                       /**< [1] */
    EWL_TAG_ADVICE_REQUESTED_BY_CARD_LEN                		= sizeof(bool),
    EWL_TAG_ADVICE_REASON_LEN                           		= sizeof(ewlAdviceReason_t),
    EWL_TAG_CLESS_KERNEL_STATUS_LEN                     		= sizeof(int),
	EWL_TAG_CLESS_BF0C_FROM_PPSE_MAX_LEN						= 222,
    EWL_TAG_CARD_COMMOM_CORE_IDENTIFIER_LEN                 	= 1,
    EWL_TAG_DERIVATION_KEY_INDEX_LEN                        	= 1,
	EWL_TAG_CARD_VERIFICATION_RESULT_MAX_LEN                	= 8,
	EWL_TAG_CARD_BIN_LEN										= 3,
	EWL_TAG_CARD_PAN_LAST_FOUR_LEN								= 2,
	EWL_TAG_CARD_AID_MAX_LEN									= EWL_AID_MAX_LEN,
	EWL_TAG_OTP_COLOMBIA_RESULT_MAX_LEN							= 256,

    EWL_TAG_TECHNOLOGY_LEN                              		= sizeof(ewlTechnology_t),

    #ifdef __TELIUM3__
    EWL_TAG_LIBRARY_VERSION_LEN                         		= EWL_KERNEL_VERSION_LEN,           /**< [1] */
    EWL_TAG_EMV_KERNEL_VERSION_LEN                      		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
	EWL_TAG_EMV_API_VERSION_LEN                         		= EWL_KERNEL_VERSION_LEN,           /**< [1] */
	EWL_TAG_ENTRY_POINT_VERSION_LEN                     		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
    EWL_TAG_PAYPASS_VERSION_LEN                         		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
    EWL_TAG_PAYWAVE_VERSION_LEN                         		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
    EWL_TAG_EXPRESSPAY_VERSION_LEN                      		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
    EWL_TAG_PURE_VERSION_LEN                            		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
    EWL_TAG_MODULE_LIB_VERSION_LEN                      		= EWL_KERNEL_VERSION_LEN,           /**< [1] */
    EWL_TAG_MODULE_VERSION_LEN                          		= EWL_KERNEL_VERSION_LEN,           /**< [1] */
    EWL_TAG_DISCOVER_PAS_VERSION_LEN                    		= EWL_KERNEL_VERSION_EXT_LEN,       /**< [1] */
    //EWL_TAG_DLL_VERSION_LEN                           		= EWL_KERNEL_VERSION_LEN,           /**< Reserved */
    EWL_TAG_DISCOVER_ZIP_VERSION_LEN                    		= EWL_KERNEL_VERSION_LEN,           /**< [1] */
	EWL_TAG_JSPEEDY_VERSION_LEN                    				= EWL_KERNEL_VERSION_LEN,           /**< [1] */
	EWL_TAG_QUICKPASS_VERSION_LEN                    			= EWL_KERNEL_VERSION_LEN,           /**< [1] */
    #else

    EWL_TAG_LIBRARY_VERSION_LEN                         		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_EMV_KERNEL_VERSION_LEN                      		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_EMV_API_VERSION_LEN                         		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_ENTRY_POINT_VERSION_LEN                     		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_PAYPASS_VERSION_LEN                         		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_PAYWAVE_VERSION_LEN                         		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_EXPRESSPAY_VERSION_LEN                      		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_PURE_VERSION_LEN                            		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_MODULE_LIB_VERSION_LEN                      		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_MODULE_VERSION_LEN                          		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    EWL_TAG_DISCOVER_PAS_VERSION_LEN                    		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    //EWL_TAG_DLL_VERSION_LEN                           		= EWL_KERNEL_VERSION_LEN,   /**< Reserved */
    //EWL_TAG_DLL_LIB_VERSION_LEN                       		= EWL_KERNEL_VERSION_LEN,   /**< Reserved */
    EWL_TAG_DISCOVER_ZIP_VERSION_LEN                    		= EWL_KERNEL_VERSION_LEN,   /**< [1] */
	EWL_TAG_JSPEEDY_VERSION_LEN                    				= EWL_KERNEL_VERSION_LEN,   /**< [1] */
	EWL_TAG_QUICKPASS_VERSION_LEN                    			= EWL_KERNEL_VERSION_LEN,   /**< [1] */
    #endif


};

/** Library Common tags */
enum ewlCommonTags_t {
    EWL_EMV_ISSUER_IDENTIFICATION_NB                    = TAG_EMV_ISSUER_IDENTIFICATION_NB,                         /**< Tag 42 */

    EWL_EMV_AID_CARD                                    = TAG_EMV_AID_CARD,                                         /**< Tag 4F */

    EWL_EMV_APPLICATION_LABEL                           = TAG_EMV_APPLICATION_LABEL,                                /**< Tag 50 */

    EWL_EMV_TRACK_2_EQU_DATA                            = TAG_EMV_TRACK_2_EQU_DATA,                                 /**< Tag 57 */

    EWL_EMV_PAN                                         = TAG_EMV_APPLI_PAN,                                        /**< Tag 5A */

    EWL_EMV_CARDHOLDER_NAME                             = TAG_EMV_CARDHOLDER_NAME,                                  /**< Tag 5F20 */

    EWL_EMV_EXPIRATION_DATE                             = TAG_EMV_APPLI_EXPIRATION_DATE,                            /**< Tag 5F24 */
    EWL_EMV_APPLI_EFFECTIVE_DATE                        = TAG_EMV_APPLI_EFFECTIVE_DATE,                             /**< Tag 5F25 */

    EWL_EMV_ISSUER_COUNTRY_CODE                         = TAG_EMV_ISSUER_COUNTRY_CODE,                              /**< Tag 5F28 */

    EWL_EMV_TRANSACTION_CURRENCY_CODE                   = TAG_EMV_TRANSACTION_CURRENCY_CODE,                        /**< Tag 5F2A */

    EWL_EMV_LANGUAGE_PREFERENCE                         = TAG_EMV_LANGUAGE_PREFERENCE,                              /**< Tag 5F2D */

    EWL_EMV_SERVICE_CODE                                = TAG_EMV_SERVICE_CODE,                                     /**< Tag 5F30 */

    EWL_EMV_APPLI_PAN_SEQUENCE_NUMBER                   = TAG_EMV_APPLI_PAN_SEQUENCE_NUMBER,                        /**< Tag 5F34 */

    EWL_EMV_TRANSACTION_CURRENCY_EXPONENT               = TAG_EMV_TRANSACTION_CURRENCY_EXPONENT,                    /**< Tag 5F36 */

    EWL_EMV_ISSUER_URL                                  = TAG_EMV_ISSUER_URL,                                       /**< Tag 5F50 */

    EWL_EMV_IBAN                                        = TAG_EMV_IBAN,                                             /**< Tag 5F53 */
    EWL_EMV_BANK_IDENTIFIER_CODE                        = TAG_EMV_BANK_IDENTIFIER_CODE,                             /**< Tag 5F54 */
    EWL_EMV_ISSUER_COUNTRY_CODE_A2_FORMAT               = TAG_EMV_ISSUER_COUNTRY_CODE_A2_FORMAT,                    /**< Tag 5F55 */
    EWL_EMV_ISSUER_COUNTRY_CODE_A3_FORMAT               = TAG_EMV_ISSUER_COUNTRY_CODE_A3_FORMAT,                    /**< Tag 5F56 */
    EWL_EMV_ACCOUNT_TYPE                                = TAG_EMV_ACCOUNT_TYPE,                                     /**< Tag 5F57 */

    EWL_EMV_APPLI_TEMPLATE                              = TAG_EMV_APPLI_TEMPLATE,                                   /**< Tag 61 */

    EWL_EMV_FCI_TEMPLATE                                = TAG_EMV_FCI_TEMPLATE,                                     /**< Tag 6F */
    EWL_EMV_READ_RECORD_RESP_MESSAGE                    = TAG_EMV_READ_RECORD_RESP_MESSAGE,                         /**< Tag 70 */
    EWL_EMV_ISSUER_SCRIPT_TEMPLATE_1                    = TAG_EMV_ISSUER_SCRIPT_TEMPLATE_1,                         /**< Tag 71 */
    EWL_EMV_ISSUER_SCRIPT_TEMPLATE_2                    = TAG_EMV_ISSUER_SCRIPT_TEMPLATE_2,                         /**< Tag 72 */
    EWL_EMV_DICTIONARY_DISCR_TEMPLATE                   = TAG_EMV_DICTIONARY_DISCR_TEMPLATE,                        /**< Tag 73 */

    EWL_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_2          = TAG_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_2,               /**< Tag 77 */

    EWL_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_1          = TAG_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_1,               /**< Tag 80 */
    EWL_EMV_AMOUNT_AUTH_BIN                             = TAG_EMV_AMOUNT_AUTH_BIN,                                  /**< Tag 81 */
    EWL_EMV_AIP                                         = TAG_EMV_AIP,                                              /**< Tag 82 */
    EWL_EMV_COMMAND_TEMPLATE                            = TAG_EMV_COMMAND_TEMPLATE,                                 /**< Tag 83 */
    EWL_EMV_DF_NAME                                     = TAG_EMV_DF_NAME,                                          /**< Tag 84 */

    EWL_EMV_ISSUER_SCRIPT_COMMAND                       = TAG_EMV_ISSUER_SCRIPT_COMMAND,                            /**< Tag 86 */
    EWL_EMV_APPLI_PRIORITY_INDICATOR                    = TAG_EMV_APPLI_PRIORITY_INDICATOR,                         /**< Tag 87 */
    EWL_EMV_SFI                                         = TAG_EMV_SFI,                                              /**< Tag 88 */
    EWL_EMV_AUTHORISATION_CODE                          = TAG_EMV_AUTHORISATION_CODE,                               /**< Tag 89 */
    EWL_EMV_AUTHORISATION_RESPONSE_CODE                 = TAG_EMV_AUTHORISATION_RESPONSE_CODE,                      /**< Tag 8A */

    EWL_EMV_CDOL_1                                      = TAG_EMV_CDOL_1,                                           /**< Tag 8C */
    EWL_EMV_CDOL_2                                      = TAG_EMV_CDOL_2,                                           /**< Tag 8D */
    EWL_EMV_CVM_LIST_CARD                               = TAG_EMV_CVM_LIST_CARD,                                    /**< Tag 8E */
    EWL_EMV_CA_PUBLIC_KEY_INDEX_CARD                    = TAG_EMV_CA_PUBLIC_KEY_INDEX_CARD,                         /**< Tag 8F */
    EWL_EMV_ISSUER_PUBLIC_KEY_CERTIFICATE               = TAG_EMV_ISSUER_PUBLIC_KEY_CERTIFICATE,                    /**< Tag 90 */
    EWL_EMV_ISSUER_AUTHENTICATION_DATA                  = TAG_EMV_ISSUER_AUTHENTICATION_DATA,                       /**< Tag 91 */
    EWL_EMV_ISSUER_PUBLIC_KEY_REMAINDER                 = TAG_EMV_ISSUER_PUBLIC_KEY_REMAINDER,                      /**< Tag 92 */
    EWL_EMV_SIGNED_STATIC_APPLI_DATA                    = TAG_EMV_SIGNED_STATIC_APPLI_DATA,                         /**< Tag 93 */
    EWL_EMV_AFL                                         = TAG_EMV_AFL,                                              /**< Tag 94 */
    EWL_EMV_TVR                                         = TAG_EMV_TVR,                                              /**< Tag 95 */

    EWL_EMV_TDOL                                        = TAG_EMV_TDOL,                                             /**< Tag 97 */
    EWL_EMV_TC_HASH_VALUE                               = TAG_EMV_TC_HASH_VALUE,                                    /**< Tag 98 */
    EWL_EMV_TRANSACTION_PIN_DATA                        = TAG_EMV_TRANSACTION_PIN_DATA,                             /**< Tag 99 */
    EWL_EMV_TRANSACTION_DATE                            = TAG_EMV_TRANSACTION_DATE,                                 /**< Tag 9A */
    EWL_EMV_TSI                                         = TAG_EMV_TSI,                                              /**< Tag 9B */
    EWL_EMV_TRANSACTION_TYPE                            = TAG_EMV_TRANSACTION_TYPE,                                 /**< Tag 9C [9]*/
    EWL_EMV_DDF_NAME                                    = TAG_EMV_DDF_NAME,                                         /**< Tag 9D */

    EWL_EMV_ACQUIRER_IDENTIFIER                         = TAG_EMV_ACQUIRER_IDENTIFIER,                              /**< Tag 9F01 */
    EWL_EMV_AMOUNT_AUTH_NUM                             = TAG_EMV_AMOUNT_AUTH_NUM,                                  /**< Tag 9F02 */
    EWL_EMV_AMOUNT_OTHER_NUM                            = TAG_EMV_AMOUNT_OTHER_NUM,                                 /**< Tag 9F03 */
    EWL_EMV_AMOUNT_OTHER_BIN                            = TAG_EMV_AMOUNT_OTHER_BIN,                                 /**< Tag 9F04 */
    EWL_EMV_AID_TERMINAL                                = TAG_EMV_AID_TERMINAL,                                     /**< Tag 9F06 */
    EWL_EMV_APPLI_USAGE_CONTROL                         = TAG_EMV_APPLI_USAGE_CONTROL,                              /**< Tag 9F07 */
    EWL_EMV_APPLI_VERSION_NUMBER_CARD                   = TAG_EMV_APPLI_VERSION_NUMBER_CARD,                        /**< Tag 9F08 */
    EWL_EMV_APPLI_VERSION_NUMBER_TERM                   = TAG_EMV_APPLI_VERSION_NUMBER_TERM,                        /**< Tag 9F09 */

    EWL_EMV_IAC_DEFAULT                                 = TAG_EMV_IAC_DEFAULT,                                      /**< Tag 9F0D */
    EWL_EMV_IAC_DENIAL                                  = TAG_EMV_IAC_DENIAL,                                       /**< Tag 9F0E */
    EWL_EMV_IAC_ONLINE                                  = TAG_EMV_IAC_ONLINE,                                       /**< Tag 9F0F */
    EWL_EMV_ISSUER_APPLI_DATA                           = TAG_EMV_ISSUER_APPLI_DATA,                                /**< Tag 9F10 */
    EWL_EMV_ISSUER_CODE_TABLE_INDEX                     = TAG_EMV_ISSUER_CODE_TABLE_INDEX,                          /**< Tag 9F11 */
    EWL_EMV_APPLI_PREFERRED_NAME                        = TAG_EMV_APPLI_PREFERRED_NAME,                             /**< Tag 9F12 */
    EWL_EMV_LAST_ONLINE_ATC_REGISTER                    = TAG_EMV_LAST_ONLINE_ATC_REGISTER,                         /**< Tag 9F13 */
    EWL_EMV_LOWER_CONSECUTIVE_OFFLINE_LIMIT             = TAG_EMV_LOWER_CONSECUTIVE_OFFLINE_LIMIT,                  /**< Tag 9F14 */
    EWL_EMV_MERCHANT_CATEGORY_CODE                      = TAG_EMV_MERCHANT_CATEGORY_CODE,                           /**< Tag 9F15 */
    EWL_EMV_MERCHANT_IDENTIFIER                         = TAG_EMV_MERCHANT_IDENTIFIER,                              /**< Tag 9F16 */
    EWL_EMV_PIN_TRY_COUNTER                             = TAG_EMV_PIN_TRY_COUNTER,                                  /**< Tag 9F17 */
    EWL_EMV_ISSUER_SCRIPT_IDENTIFIER                    = TAG_EMV_ISSUER_SCRIPT_IDENTIFIER,                         /**< Tag 9F18 */

    EWL_EMV_TERMINAL_COUNTRY_CODE                       = TAG_EMV_TERMINAL_COUNTRY_CODE,                            /**< Tag 9F1A */
    EWL_EMV_TERMINAL_FLOOR_LIMIT                        = TAG_EMV_TERMINAL_FLOOR_LIMIT,                             /**< Tag 9F1B */
    EWL_EMV_TERMINAL_IDENTIFICATION                     = TAG_EMV_TERMINAL_IDENTIFICATION,                          /**< Tag 9F1C */
    EWL_EMV_TERMINAL_RISK_MANAGEMENT_DATA               = TAG_EMV_TERMINAL_RISK_MANAGEMENT_DATA,                    /**< Tag 9F1D */
    EWL_EMV_IFD_SERIAL_NUMBER                           = TAG_EMV_IFD_SERIAL_NUMBER,                                /**< Tag 9F1E */
    EWL_EMV_TRACK_1_DISCRET_DATA                        = TAG_EMV_TRACK_1_DISCRET_DATA,                             /**< Tag 9F1F */
    EWL_EMV_TRACK_2_DISCRET_DATA                        = TAG_EMV_TRACK_2_DISCRET_DATA,                             /**< Tag 9F20 */
    EWL_EMV_TRANSACTION_TIME                            = TAG_EMV_TRANSACTION_TIME,                                 /**< Tag 9F21 */
    EWL_EMV_CA_PUBLIC_KEY_INDEX_TERM                    = TAG_EMV_CA_PUBLIC_KEY_INDEX_TERM,                         /**< Tag 9F22 */
    EWL_EMV_UPPER_CONSECUTIVE_OFFLINE_LIMIT             = TAG_EMV_UPPER_CONSECUTIVE_OFFLINE_LIMIT,                  /**< Tag 9F23 */

    EWL_EMV_CRYPTOGRAM                                  = TAG_EMV_APPLICATION_CRYPTOGRAM,                           /**< Tag 9F26 */
    EWL_EMV_CID                                         = TAG_EMV_CRYPTOGRAM_INFO_DATA,                             /**< Tag 9F27 */

    EWL_EMV_ICC_PIN_ENCIPH_PK_CERTIFICATE               = TAG_EMV_ICC_PIN_ENCIPH_PK_CERTIFICATE,                    /**< Tag 9F2D */
    EWL_EMV_ICC_PIN_ENCIPH_PK_EXPONENT                  = TAG_EMV_ICC_PIN_ENCIPH_PK_EXPONENT,                       /**< Tag 9F2E */
    EWL_EMV_ICC_PIN_ENCIPH_PK_REMAINDER                 = TAG_EMV_ICC_PIN_ENCIPH_PK_REMAINDER,                      /**< Tag 9F2F */

    EWL_EMV_ISSUER_PK_EXPONENT                          = TAG_EMV_ISSUER_PK_EXPONENT,                               /**< Tag 9F32 */
    EWL_EMV_TERMINAL_CAPABILITIES                       = TAG_EMV_TERMINAL_CAPABILITIES,                            /**< Tag 9F33 */
    EWL_EMV_CVM_RESULTS                                 = TAG_EMV_CVM_RESULTS,                                      /**< Tag 9F34 */
    EWL_EMV_TERMINAL_TYPE                               = TAG_EMV_TERMINAL_TYPE,                                    /**< Tag 9F35 */
    EWL_EMV_ATC                                         = TAG_EMV_ATC,                                              /**< Tag 9F36 */
    EWL_EMV_UNPREDICTABLE_NUMBER                        = TAG_EMV_UNPREDICTABLE_NUMBER,                             /**< Tag 9F37 */
    EWL_EMV_PDOL                                        = TAG_EMV_PDOL,                                             /**< Tag 9F38 */
    EWL_EMV_POS_ENTRY_MODE                              = TAG_EMV_POS_ENTRY_MODE,                                   /**< Tag 9F39 */
    EWL_EMV_AMOUNT_REF_CURRENCY                         = TAG_EMV_AMOUNT_REF_CURRENCY,                              /**< Tag 9F3A */
    EWL_EMV_APPLI_REF_CURRENCY                          = TAG_EMV_APPLI_REF_CURRENCY,                               /**< Tag 9F3B */
    EWL_EMV_TRANSACTION_REF_CURRENCY_CODE               = TAG_EMV_TRANSACTION_REF_CURRENCY_CODE,                    /**< Tag 9F3C */
    EWL_EMV_TRANSACTION_REF_CURRENCY_EXPONENT           = TAG_EMV_TRANSACTION_REF_CURRENCY_EXPONENT,                /**< Tag 9F3D */

    EWL_EMV_ADD_TERMINAL_CAPABILITIES                   = TAG_EMV_ADD_TERMINAL_CAPABILITIES,                        /**< Tag 9F40 */
    EWL_EMV_TRANSACTION_SEQUENCE_COUNTER                = TAG_EMV_TRANSACTION_SEQUENCE_COUNTER,                     /**< Tag 9F41 */
    EWL_EMV_APPLI_CURRENCY_CODE                         = TAG_EMV_APPLI_CURRENCY_CODE,                              /**< Tag 9F42 */
    EWL_EMV_APPLI_REF_CURRENCY_EXPONENT                 = TAG_EMV_APPLI_REF_CURRENCY_EXPONENT,                      /**< Tag 9F43 */
    EWL_EMV_APPLI_CURRENCY_EXPONENT                     = TAG_EMV_APPLI_CURRENCY_EXPONENT,                          /**< Tag 9F44 */
    EWL_EMV_DATA_AUTHENTICATION_CODE                    = TAG_EMV_DATA_AUTHENTICATION_CODE,                         /**< Tag 9F45 */
    EWL_EMV_ICC_PK_CERTIFICATE                          = TAG_EMV_ICC_PK_CERTIFICATE,                               /**< Tag 9F46 */
    EWL_EMV_ICC_PK_EXPONENT                             = TAG_EMV_ICC_PK_EXPONENT,                                  /**< Tag 9F47 */
    EWL_EMV_ICC_PK_REMAINDER                            = TAG_EMV_ICC_PK_REMAINDER,                                 /**< Tag 9F48 */
    EWL_EMV_DDOL                                        = TAG_EMV_DDOL,                                             /**< Tag 9F49 */
    EWL_EMV_SDA_TAG_LIST                                = TAG_EMV_SDA_TAG_LIST,                                     /**< Tag 9F4A */
    EWL_EMV_SIGNED_DYNAMIC_APPLI_DATA                   = TAG_EMV_SIGNED_DYNAMIC_APPLI_DATA,                        /**< Tag 9F4B */
    EWL_EMV_ICC_DYNAMIC_NUMBER                          = TAG_EMV_ICC_DYNAMIC_NUMBER,                               /**< Tag 9F4C */
    EWL_EMV_LOG_ENTRY                                   = TAG_EMV_LOG_ENTRY,                                        /**< Tag 9F4D */
    EWL_EMV_MERCHANT_NAME_AND_LOCATION                  = TAG_EMV_MERCHANT_NAME_AND_LOCATION,                       /**< Tag 9F4E */
    EWL_EMV_LOG_FORMAT                                  = TAG_EMV_LOG_FORMAT,                                       /**< Tag 9F4F */

    EWL_EMV_TRANSACTION_CATEGORY_CODE                   = 0x9F53,                                                   /**< Tag 9F53 */

    EWL_EMV_FCI_ISSUER_DISCRET_DATA                     = TAG_EMV_FCI_ISSUER_DISCRET_DATA,                          /**< Tag BF0C */
};

enum ewlCommonTagsLen_t {

    EWL_EMV_ISSUER_IDENTIFICATION_NB_LEN                    = 3,                                                    /**< Tag 42 Len*/

    EWL_EMV_AID_CARD_MAX_LEN                                = EWL_AID_MAX_LEN,                                      /**< Tag 4F Len */

    EWL_EMV_APPLICATION_LABEL_MAX_LEN                       = EWL_LABEL_MAX_LEN,                                    /**< Tag 50 Len [1] */

    EWL_EMV_TRACK_2_EQU_DATA_MAX_LEN                        = 20,                                                   /**< Tag 57 Len*/

    EWL_EMV_PAN_MAX_LEN                                     = 10,                                                   /**< Tag 5A */

    EWL_EMV_CARDHOLDER_NAME_MAX_LEN                         = 26,                                                   /**< Tag 5F20 Len [1] */

    EWL_EMV_EXPIRATION_DATE_LEN                             = 3,                                                    /**< Tag 5F24 Len */
    EWL_EMV_APPLI_EFFECTIVE_DATE_LEN                        = 3,                                                    /**< Tag 5F25 Len*/

    EWL_EMV_ISSUER_COUNTRY_CODE_LEN                         = 2,                                                    /**< Tag 5F28 Len*/

    EWL_EMV_TRANSACTION_CURRENCY_CODE_LEN                   = 2,                                                    /**< Tag 5F2A Len */
    EWL_EMV_LANGUAGE_PREFERENCE_MAX_LEN                     = 8,                                                    /**< Tag 5F2D Len [1] */
    EWL_EMV_SERVICE_CODE_LEN                                = 2,                                                    /**< Tag 5F30 Len */

    EWL_EMV_APPLI_PAN_SEQUENCE_NUMBER_LEN                   = 1,                                                    /**< Tag 5F34 Len*/

    EWL_EMV_TRANSACTION_CURRENCY_EXPONENT_LEN               = 1,                                                    /**< Tag 5F36 Len */

    //EWL_EMV_ISSUER_URL_MAX_LEN                            = undefined                                             /**< Tag 5F50 Len [4] */

    EWL_EMV_IBAN_MAX_LEN                                    = 34,                                                   /**< Tag 5F53 Len */
    EWL_EMV_BANK_IDENTIFIER_CODE_MAX_LEN                    = 11,                                                   /**< Tag 5F54 Len */
    EWL_EMV_ISSUER_COUNTRY_CODE_A2_FORMAT_LEN               = 2,                                                    /**< Tag 5F55 Len */
    EWL_EMV_ISSUER_COUNTRY_CODE_A3_FORMAT_LEN               = 3,                                                    /**< Tag 5F56 Len [1] */
    EWL_EMV_ACCOUNT_TYPE_LEN                                = 1,                                                    /**< Tag 5F57 Len */

    EWL_EMV_APPLI_TEMPLATE_MAX_LEN                          = 252,                                                  /**< TAG 61 Len */

    EWL_EMV_FCI_TEMPLATE_MAX_LEN                            = 252,                                                  /**< TAG 6F Len */

    EWL_EMV_READ_RECORD_RESP_MESSAGE_MAX_LEN                = 252,                                                  /**< TAG 70 Len */
    //EWL_EMV_ISSUER_SCRIPT_TEMPLATE_1_MAX_LEN              = undefined,                                            /**< Tag 71 Len [4] */
    //EWL_EMV_ISSUER_SCRIPT_TEMPLATE_2_MAX_LEN              = undefined,                                            /**< Tag 72 Len [4] */
    EWL_EMV_DICTIONARY_DISCR_TEMPLATE_MAX_LEN               = 252,                                                  /**< TAG 73 Len */

    //EWL_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_2_MAX_LEN    = undefined,                                            /**< Tag 77 Len [4] */

    //EWL_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_1_MAX_LEN    = undefined,                                            /**< Tag 80 Len [4] */
    EWL_EMV_AMOUNT_AUTH_BIN_LEN                             = EWL_BINARY_VALUE_LEN,                                 /**< Tag 81 Len */
    EWL_EMV_AIP_LEN                                         = 2,                                                    /**< Tag 82 Len */
    //EWL_EMV_COMMAND_TEMPLATE_MAX_LEN                      = undefined,                                            /**< Tag 83 Len [4] */
    EWL_EMV_DF_NAME_MAX_LEN                                 = EWL_AID_MAX_LEN,                                      /**< Tag 84 Len */


    EWL_EMV_ISSUER_SCRIPT_COMMAND_MAX_LEN                   = 261,                                                  /**< Tag 86 Len */
    EWL_EMV_APPLI_PRIORITY_INDICATOR_LEN                    = 1,                                                    /**< Tag 87 Len */
    EWL_EMV_SFI_LEN                                         = 1,                                                    /**< Tag 88 Len */
    EWL_EMV_AUTHORISATION_CODE_LEN                          = 6,                                                    /**< Tag 89 Len [1] */
    EWL_EMV_AUTHORISATION_RESPONSE_CODE_LEN                 = 2,                                                    /**< Tag 8A Len [1] */

    EWL_EMV_CDOL_1_MAX_LEN                                  = 252,                                                  /**< Tag 8C Len */
    EWL_EMV_CDOL_2_MAX_LEN                                  = 252,                                                  /**< Tag 8D Len */
    EWL_EMV_CVM_LIST_CARD_MAX_LEN                           = 252,                                                  /**< Tag 8E Len */
    EWL_EMV_CA_PUBLIC_KEY_INDEX_CARD_LEN                    = 1,                                                    /**< Tag 8F Len */
    EWL_EMV_ISSUER_PUBLIC_KEY_CERTIFICATE_MAX_LEN           = 248,                                                  /**< Tag 80 Len */
    EWL_EMV_ISSUER_AUTHENTICATION_DATA_MAX_LEN              = 16,                                                   /**< Tag 91 Len */
    EWL_EMV_ISSUER_PUBLIC_KEY_REMAINDER_MAX_LEN             = 248,                                                  /**< Tag 92 Len */
    EWL_EMV_SIGNED_STATIC_APPLI_DATA_MAX_LEN                = 248,                                                  /**< Tag 93 Len */
    EWL_EMV_AFL_MAX_LEN                                     = 252,                                                  /**< Tag 94 Len */
    EWL_EMV_TVR_LEN                                         = EWL_TAC_LEN,                                          /**< Tag 95 Len */

    EWL_EMV_TDOL_MAX_LEN                                    = 252,                                                  /**< Tag 97 Len */
    EWL_EMV_TC_HASH_VALUE_LEN                               = 20,                                                   /**< Tag 98 Len */
    //EWL_EMV_TRANSACTION_PIN_DATA_MAX_LEN                    = undefined                                           /**< Tag 99 Len */
    EWL_EMV_TRANSACTION_DATE_LEN                            = 3,                                                    /**< Tag 9A Len */
    EWL_EMV_TSI_LEN                                         = 2,                                                    /**< Tag 9B Len */
    EWL_EMV_TRANSACTION_TYPE_LEN                            = 1,                                                    /**< Tag 9C Len */
    EWL_EMV_DDF_NAME_MAX_LEN                                = 16,                                                   /**< Tag 9D Len */

    EWL_EMV_ACQUIRER_IDENTIFIER_LEN                         = 6,                                                    /**< Tag 9F01 Len */
    EWL_EMV_AMOUNT_AUTH_NUM_LEN                             = EWL_BCD_VALUE_LEN,                                    /**< Tag 9F02 Len */
    EWL_EMV_AMOUNT_OTHER_NUM_LEN                            = EWL_BCD_VALUE_LEN,                                    /**< Tag 9F03 Len */
    EWL_EMV_AMOUNT_OTHER_BIN_LEN                            = EWL_BINARY_VALUE_LEN,                                 /**< Tag 9F04 Len */
    EWL_EMV_AID_TERMINAL_MAX_LEN                            = EWL_AID_MAX_LEN,                                      /**< Tag 9F06 Len */
    EWL_EMV_APPLI_USAGE_CONTROL_LEN                         = 2,                                                    /**< Tag 9F07 Len */
    EWL_EMV_APPLI_VERSION_NUMBER_CARD_LEN                   = 2,                                                    /**< Tag 9F08 Len */
    EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN                   = 2,                                                    /**< Tag 9F09 Len */

    EWL_EMV_IAC_DEFAULT_LEN                                 = 5,                                                    /**< Tag 9F0D Len */
    EWL_EMV_IAC_DENIAL_LEN                                  = 5,                                                    /**< Tag 9F0E Len */
    EWL_EMV_IAC_ONLINE_LEN                                  = 5,                                                    /**< Tag 9F0F Len */
    EWL_EMV_ISSUER_APPLI_DATA_MAX_LEN                       = 32,                                                   /**< Tag 9F10 Len */
    EWL_EMV_ISSUER_CODE_TABLE_INDEX_LEN                     = 1,                                                    /**< Tag 9F11 Len */
    EWL_EMV_APPLI_PREFERRED_NAME_MAX_LEN                    = EWL_LABEL_MAX_LEN,                                    /**< Tag 9F12 Len [1]*/
    EWL_EMV_LAST_ONLINE_ATC_REGISTER_LEN                    = 2,                                                    /**< Tag 9F13 Len */
    EWL_EMV_LOWER_CONSECUTIVE_OFFLINE_LIMIT_LEN             = 1,                                                    /**< Tag 9F14 Len */
    EWL_EMV_MERCHANT_CATEGORY_CODE_LEN                      = 2,                                                    /**< Tag 9F15 Len */
    EWL_EMV_MERCHANT_IDENTIFIER_LEN                         = 15,                                                   /**< Tag 9F16 Len [1]*/
    EWL_EMV_PIN_TRY_COUNTER_LEN                             = 1,                                                    /**< Tag 9F17 Len */
    EWL_EMV_ISSUER_SCRIPT_IDENTIFIER_LEN                    = 4,                                                    /**< Tag 9F18 Len */

    EWL_EMV_TERMINAL_COUNTRY_CODE_LEN                       = 2,                                                    /**< Tag 9F1A Len */
    EWL_EMV_TERMINAL_FLOOR_LIMIT_LEN                        = EWL_BINARY_VALUE_LEN,                                 /**< Tag 9F1B Len */
    EWL_EMV_TERMINAL_IDENTIFICATION_LEN                     = 8,                                                    /**< Tag 9F1C Len [1]*/
    EWL_EMV_TERMINAL_RISK_MANAGEMENT_DATA_MAX_LEN           = 8,                                                    /**< Tag 9F1D Len */
    EWL_EMV_IFD_SERIAL_NUMBER_LEN                           = 8,                                                    /**< Tag 9F1E Len */
    EWL_EMV_TRACK_1_DISCRET_DATA_MAX_LEN                    = 76 - 3,                                               /**< Tag 9F1F Len [1]*/
    EWL_EMV_TRACK_2_DISCRET_DATA_MAX_LEN                    = 37 - 1,                                               /**< Tag 9F20 Len [1]*/
    EWL_EMV_TRANSACTION_TIME_LEN                            = 3,                                                    /**< Tag 9F21 Len */
    EWL_EMV_CA_PUBLIC_KEY_INDEX_TERM_LEN                    = 1,                                                    /**< Tag 9F22 Len */
    EWL_EMV_UPPER_CONSECUTIVE_OFFLINE_LIMIT_LEN             = 1,                                                    /**< Tag 9F23 Len */

    EWL_EMV_CRYPTOGRAM_LEN                                  = 8,                                                    /**< Tag 9F26 Len */
    EWL_EMV_CID_LEN                                         = 1,                                                    /**< Tag 9F27 Len */

    EWL_EMV_ICC_PIN_ENCIPH_PK_CERTIFICATE_MAX_LEN           = 248,                                                  /**< Tag 9F2D Len */
    EWL_EMV_ICC_PIN_ENCIPH_PK_EXPONENT_MAX_LEN              = 3,                                                    /**< Tag 9F2E Len */
    EWL_EMV_ICC_PIN_ENCIPH_PK_REMAINDER_MAX_LEN             = 248,                                                  /**< Tag 9F2F Len */

    EWL_EMV_ISSUER_PK_EXPONENT_MAX_LEN                      = 3,                                                    /**< Tag 9F32 Len */
    EWL_EMV_TERMINAL_CAPABILITIES_LEN                       = 3,                                                    /**< Tag 9F33 Len */
    EWL_EMV_CVM_RESULTS_LEN                                 = 3,                                                    /**< Tag 9F34 Len */
    EWL_EMV_TERMINAL_TYPE_LEN                               = 1,                                                    /**< Tag 9F35 Len */
    EWL_EMV_ATC_LEN                                         = 2,                                                    /**< Tag 9F36 Len */
    EWL_EMV_UNPREDICTABLE_NUMBER_LEN                        = 4,                                                    /**< Tag 9F37 Len */
    //EWL_EMV_PDOL_MAX_LEN                                  = undefined,                                            /**< Tag 9F38 Len [4] */
    EWL_EMV_POS_ENTRY_MODE_LEN                              = 1,                                                    /**< Tag 9F39 Len */
    EWL_EMV_AMOUNT_REF_CURRENCY_LEN                         = 4,                                                    /**< Tag 9F3A Len */
    EWL_EMV_APPLI_REF_CURRENCY_MAX_LEN                      = 8,                                                    /**< Tag 9F3B Len */
    EWL_EMV_TRANSACTION_REF_CURRENCY_CODE_LEN               = 2,                                                    /**< Tag 9F3C Len */
    EWL_EMV_TRANSACTION_REF_CURRENCY_EXPONENT_LEN           = 1,                                                    /**< Tag 9F3D Len */

    EWL_EMV_ADD_TERMINAL_CAPABILITIES_LEN                   = 5,                                                    /**< Tag 9F40 Len */
    EWL_EMV_TRANSACTION_SEQUENCE_COUNTER_MAX_LEN            = 3,                                                    /**< Tag 9F41 Len */
    EWL_EMV_APPLI_CURRENCY_CODE_LEN                         = 2,                                                    /**< Tag 9F42 Len */
    EWL_EMV_APPLI_REF_CURRENCY_EXPONENT_MAX_LEN             = 4,                                                    /**< Tag 9F43 Len */
    EWL_EMV_APPLI_CURRENCY_EXPONENT_LEN                     = 1,                                                    /**< Tag 9F44 Len */
    EWL_EMV_DATA_AUTHENTICATION_CODE_LEN                    = 2,                                                    /**< Tag 9F45 Len */
    EWL_EMV_ICC_PK_CERTIFICATE_MAX_LEN                      = 248,                                                  /**< Tag 9F46 Len */
    EWL_EMV_ICC_PK_EXPONENT_MAX_LEN                         = 3,                                                    /**< Tag 9F47 Len */
    EWL_EMV_ICC_PK_REMAINDER_MAX_LEN                        = 248,                                                  /**< Tag 9F48 Len */
    EWL_EMV_DDOL_MAX_LEN                                    = 252,                                                  /**< Tag 9F49 Len */
    //EWL_EMV_SDA_TAG_LIST_MAX_LEN                          = undefined,                                            /**< Tag 9F4A Len [4] */
    EWL_EMV_SIGNED_DYNAMIC_APPLI_DATA_MAX_LEN               = 248,                                                  /**< Tag 9F4B Len */
    EWL_EMV_ICC_DYNAMIC_NUMBER_MAX_LEN                      = 8,                                                    /**< Tag 9F4C Len */
    EWL_EMV_LOG_ENTRY_LEN                                   = 2,                                                    /**< Tag 9F4D Len */
    //EWL_EMV_MERCHANT_NAME_AND_LOCATION_MAX_LEN            = undefined,                                            /**< Tag 9F4E Len [1] [4] */
    //EWL_EMV_LOG_FORMAT_MAX_LEN                            = undefined,                                            /**< Tag 9F4F Len [4] */

    EWL_EMV_TRANSACTION_CATEGORY_CODE_LEN                   = 1,                                                    /**< Tag 9F53 Len [1] */

    EWL_EMV_FCI_ISSUER_DISCRET_DATA_MAX_LEN                 = EWL_TAG_CLESS_BF0C_FROM_PPSE_MAX_LEN,                 /**< Tag BF0C Len */

};

#ifdef EWL_ENABLE_KERNEL_PAYWAVE
/** Library PayWave tags */
enum ewPayWaveTags_t  {
    EWL_PAYWAVE_APPLICATION_DEFAULT_ACTION              = TAG_PAYWAVE_APPLICATION_DEFAULT_ACTION,                   /**< Tag 9F52 */

    EWL_PAYWAVE_APPLICATION_PROGRAM_IDENTIFIER          = TAG_PAYWAVE_APPLICATION_PROGRAM_IDENTIFIER,               /**< Tag 9F5A */
    EWL_PAYWAVE_ISSUER_SCRIPT_RESULT                    = TAG_PAYWAVE_ISSUER_SCRIPT_RESULT,                         /**< Tag 9F5B [3] \ref EWL_TAG_ISSUER_SCRIPT_RESULT */

    EWL_PAYWAVE_AVAILABLE_OFFLINE_SPENDING_AMOUNT       = TAG_PAYWAVE_AVAILABLE_OFFLINE_SPENDING_AMOUNT,            /**< Tag 9F5D [3] \ref EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT */

    EWL_PAYWAVE_TTQ                                     = TAG_PAYWAVE_TERMINAL_TRANSACTION_QUALIFIERS,              /**< Tag 9F66 [2] [12]*/

    EWL_PAYWAVE_CARD_ADDITIONNAL_PROCESSES              = TAG_PAYWAVE_CARD_ADDITIONNAL_PROCESSES,                   /**< Tag 9F68 */
    EWL_PAYWAVE_CARD_AUTHENTICATION_RELATED_DATA        = TAG_PAYWAVE_CARD_AUTHENTICATION_RELATED_DATA,             /**< Tag 9F69 [2] */

    EWL_PAYWAVE_CARD_CVM_LIMIT                          = TAG_PAYWAVE_CARD_CVM_LIMIT,                               /**< Tag 9F6B [2] */
    EWL_PAYWAVE_CTQ                                     = TAG_PAYWAVE_CARD_TRANSACTION_QUALIFIERS,                  /**< Tag 9F6C [2] */
    EWL_PAYWAVE_FORM_FACTOR_INDICATOR                   = TAG_PAYWAVE_FORM_FACTOR_INDICATOR,                        /**< Tag 9F6E [2] */

    EWL_PAYWAVE_CUSTOMER_EXCLUSIVE_DATA                 = TAG_PAYWAVE_CUSTOMER_EXCLUSIVE_DATA,                      /**< Tag 9F7C [2] */
};

enum ewlPayWaveTagsLen_t {
    EWL_PAYWAVE_APPLICATION_DEFAULT_ACTION_LEN              = 4,                                                    /**< Tag 9F52 Len*/

    EWL_PAYWAVE_APPLICATION_PROGRAM_IDENTIFIER_MAX_LEN      = 16,                                                   /**< Tag 9F5A Len*/
    EWL_PAYWAVE_ISSUER_SCRIPT_RESULT_MAX_LEN                = 50,                                                   /**< Tag 9F5B Len [2] */

    EWL_PAYWAVE_AVAILABLE_OFFLINE_SPENDING_AMOUNT_LEN       = 6,                                                    /**< Tag 9F5D Len [2] */

    EWL_PAYWAVE_TTQ_LEN                                     = 4,                                                    /**< Tag 9F66 Len [2] */

    EWL_PAYWAVE_CARD_AUTHENTICATION_RELATED_DATA_MAX_LEN    = 16,                                                   /**< Tag 9F69 Len [2] */
    EWL_PAYWAVE_CARD_CVM_LIMIT_LEN                          = 6,                                                    /**< Tag 9F6B Len [2] */
    EWL_PAYWAVE_CTQ_LEN                                     = 2,                                                    /**< Tag 9F6C Len */
    EWL_PAYWAVE_VLP_RESET_THRESHOLD_LEN                     = 6,                                                    /**< Tag 9F6D Len [2] */
    EWL_PAYWAVE_FORM_FACTOR_INDICATOR_LEN                   = 4,                                                    /**< Tag 9F6E Len [2] */

    EWL_PAYWAVE_VLP_FUNDS_LIMIT_LEN                         = 6,                                                    /**< Tag 9F77 Len [2] */
    EWL_PAYWAVE_VLP_SINGLE_TRANSACTION_LIMIT_LEN            = 6,                                                    /**< Tag 9F78 Len [2] */

    EWL_PAYWAVE_CUSTOMER_EXCLUSIVE_DATA_MAX_LEN             = 32,                                                   /**< Tag 9F7C Len [2] */
};
#endif


#ifdef EWL_ENABLE_KERNEL_PAYPASS
/** Library PayPass tags */
enum ewPayPassTags_t  {
    EWL_PAYPASS_TRACK1                                                  = TAG_PAYPASS_TRACK1_DATA,                                  /**< Tag 56 */

    EWL_PAYPASS_OFFLINE_ACCUMULATOR_BALANCE                             = TAG_PAYPASS_OFFLINE_ACCUMULATOR_BALANCE,                  /**< Tag 9F50 [3] \ref EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT */
    EWL_PAYPASS_DRDOL                                                   = TAG_PAYPASS_DRDOL,                                        /**< Tag 9F51 */

    EWL_PAYPASS_DS_ODS_CARD                                             = TAG_PAYPASS_DS_ODS_CARD,                                  /**< Tag 9F54 */

    EWL_PAYPASS_DSDOL                                                   = TAG_PAYPASS_DSDOL,                                        /**< Tag 9F5B [2]*/
    EWL_PAYPASS_DS_REQUESTED_OPERATOR_ID                                = TAG_PAYPASS_DS_REQUESTED_OPERATOR_ID,                     /**< Tag 9F5C */
    EWL_PAYPASS_APPLICATION_CAPABILITIES_INFORMATION                    = TAG_PAYPASS_APPLICATION_CAPABILITIES_INFORMATION,         /**< Tag 9F5D [2] */
    EWL_PAYPASS_DS_ID                                                   = TAG_PAYPASS_DS_ID,                                        /**< Tag 9F5E [2] */
    EWL_PAYPASS_DS_SLOT_AVAILABILITY                                    = TAG_PAYPASS_DS_SLOT_AVAILABILITY,                         /**< Tag 9F5F [2] */
    EWL_PAYPASS_CVC3_TRACK1                                             = TAG_PAYPASS_CVC3_TRACK1,                                  /**< Tag 9F60 */
    EWL_PAYPASS_CVC3_TRACK2                                             = TAG_PAYPASS_CVC3_TRACK2,                                  /**< Tag 9F61 */
    EWL_PAYPASS_PCVC3_TRACK1                                            = TAG_PAYPASS_PCVC3_TRACK1,                                 /**< Tag 9F62 */
    EWL_PAYPASS_PUNATC_TRACK1                                           = TAG_PAYPASS_PUNATC_TRACK1,                                /**< Tag 9F63 */
    EWL_PAYPASS_NATC_TRACK1                                             = TAG_PAYPASS_NATC_TRACK1,                                  /**< Tag 9F64 */
    EWL_PAYPASS_PCVC3_TRACK2                                            = TAG_PAYPASS_PCVC3_TRACK2,                                 /**< Tag 9F65 */
    EWL_PAYPASS_PUNATC_TRACK2                                           = TAG_PAYPASS_PUNATC_TRACK2,                                /**< Tag 9F66 [2] [12]*/
    EWL_PAYPASS_NATC_TRACK2                                             = TAG_PAYPASS_NATC_TRACK2,                                  /**< Tag 9F67 */

    EWL_PAYPASS_UDOL                                                    = TAG_PAYPASS_UDOL,                                         /**< Tag 9F69 [2] */
    EWL_PAYPASS_MSTRIPE_UNPREDICTABLE_NUMBER                            = TAG_PAYPASS_MSTRIPE_UNPREDICTABLE_NUMBER,                 /**< Tag 9F6A */
    EWL_PAYPASS_TRACK2                                                  = TAG_PAYPASS_TRACK2_DATA,                                  /**< Tag 9F6B [3]  \ref EWL_TAG_TRACK2_DATA */
    EWL_PAYPASS_MSTRIPE_APPLI_VERSION_NUMBER_ICC                        = TAG_PAYPASS_MSTRIPE_APPLI_VERSION_NUMBER_ICC,             /**< Tag 9F6C [2] */
    EWL_PAYPASS_MSTRIPE_APPLI_VERSION_NUMBER_TERM                       = TAG_PAYPASS_MSTRIPE_APPLI_VERSION_NUMBER_TERM,            /**< Tag 9F6D [2] */
    EWL_PAYPASS_THIRD_PARTY_DATA                                        = TAG_PAYPASS_THIRD_PARTY_DATA,                             /**< Tag 9F6E [2] */
    EWL_PAYPASS_DS_SLOT_MANAGEMENT_CONTROL                              = TAG_PAYPASS_DS_SLOT_MANAGEMENT_CONTROL,                   /**< Tag 9F6F */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_1                               = TAG_PAYPASS_PROTECTED_DATA_ENVELOPE_1,                    /**< Tag 9F70 [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_2                               = TAG_PAYPASS_PROTECTED_DATA_ENVELOPE_2,                    /**< Tag 9F71 [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_3                               = TAG_PAYPASS_PROTECTED_DATA_ENVELOPE_3,                    /**< Tag 9F72 [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_4                               = TAG_PAYPASS_PROTECTED_DATA_ENVELOPE_4,                    /**< Tag 9F73 [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_5                               = TAG_PAYPASS_PROTECTED_DATA_ENVELOPE_5,                    /**< Tag 9F74 [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_1                             = TAG_PAYPASS_UNPROTECTED_DATA_ENVELOPE_1,                  /**< Tag 9F75 [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_2                             = TAG_PAYPASS_UNPROTECTED_DATA_ENVELOPE_2,                  /**< Tag 9F76 [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_3                             = TAG_PAYPASS_UNPROTECTED_DATA_ENVELOPE_3,                  /**< Tag 9F77 [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_4                             = TAG_PAYPASS_UNPROTECTED_DATA_ENVELOPE_4,                  /**< Tag 9F78 [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_5                             = TAG_PAYPASS_UNPROTECTED_DATA_ENVELOPE_5,                  /**< Tag 9F79 [2] */

    EWL_PAYPASS_MERCHANT_CUSTOM_DATA                                    = TAG_PAYPASS_MERCHANT_CUSTOM_DATA,                         /**< Tag 9F7C [2] */
    EWL_PAYPASS_DS_SUMMARY_1                                            = TAG_PAYPASS_DS_SUMMARY_1,                                 /**< Tag 9F7D [2] */
    EWL_PAYPASS_MOBILE_SUPPORT_INDICATOR                                = TAG_PAYPASS_MOBILE_SUPPORT_INDICATOR,                     /**< Tag 9F7E [2] */
    EWL_PAYPASS_DS_UNPREDICTABLE_NUMBER                                 = TAG_PAYPASS_DS_UNPREDICTABLE_NUMBER,                      /**< Tag 9F7F [2] */

    EWL_PAYPASS_POS_CARDHOLDER_INTERACTION_INFO                         = TAG_PAYPASS_POS_CARDHOLDER_INTERACTION_INFO,              /**< Tag DF4B [2]*/

    EWL_PAYPASS_DS_INPUT_CARD                                           = TAG_PAYPASS_DS_INPUT_CARD,                                /**< Tag DF60 */
    EWL_PAYPASS_DS_DIGEST_H                                             = TAG_PAYPASS_DS_DIGEST_H,                                  /**< Tag DF61 */
    EWL_PAYPASS_DS_ODS_INFO                                             = TAG_PAYPASS_DS_ODS_INFO,                                  /**< Tag DF62 */
    EWL_PAYPASS_DS_ODS_TERM                                             = TAG_PAYPASS_DS_ODS_TERM,                                  /**< Tag DF63 */

    EWL_PAYPASS_DS_SUMMARY_2                                            = 0xDF8101,
    EWL_PAYPASS_DS_SUMMARY_3                                            = 0xDF8102,
    EWL_PAYPASS_BALANCE_READ_BEFORE_GEN_AC                              = 0xDF8104,
    EWL_PAYPASS_BALANCE_READ_AFTER_GEN_AC                               = 0xDF8105,
    EWL_PAYPASS_DATA_NEEDED                                             = 0xDF8106,

    EWL_PAYPASS_DS_AC_TYPE                                              = 0xDF8108,
    EWL_PAYPASS_DS_INPUT_TERM                                           = 0xDF8109,
    EWL_PAYPASS_DS_ODS_INFO_FOR_READER                                  = 0xDF810A,
    EWL_PAYPASS_DS_SUMMARY_STATUS                                       = 0xDF810B,

    EWL_PAYPASS_DSVN_TERM                                               = 0xDF810D,
    EWL_PAYPASS_POST_GEN_AC_PUT_DATA_STATUS                             = 0xDF810E,
    EWL_PAYPASS_PRE_GEN_AC_PUT_DATA_STATUS                              = 0xDF810F,
    EWL_PAYPASS_PROCEED_TO_FIRST_WRITE_FLAG                             = 0xDF8110,
    EWL_PAYPASS_TAGS_TO_READ                                            = 0xDF8112,
    EWL_PAYPASS_DRDOL_RELATED_DATA                                      = 0xDF8113,
    EWL_PAYPASS_REFERENCE_CONTROL_PARAMETER                             = 0xDF8114,
    EWL_PAYPASS_ERROR_INDICATION                                        = 0xDF8115,
    EWL_PAYPASS_USER_INTERFACE_REQUEST_DATA                             = 0xDF8116,
    EWL_PAYPASS_CARD_DATA_INPUT_CAPABILITY                              = 0xDF8117,
    EWL_PAYPASS_CVM_CAPABILITY_CVM_REQUIRED                             = 0xDF8118,
    EWL_PAYPASS_CVM_CAPABILITY_NO_CVM_REQUIRED                          = 0xDF8119,
    EWL_PAYPASS_DEFAULT_UDOL                                            = 0xDF811A,
    EWL_PAYPASS_KERNEL_CONFIGURATION                                    = 0xDF811B,
    EWL_PAYPASS_MAX_LIFETIME_OF_TORN_TRANSACTION_LOG_RECORD             = 0xDF811C,
    EWL_PAYPASS_MAX_NUMBER_OF_TORN_TRANSACTION_LOG_RECORDS              = 0xDF811D,
    EWL_PAYPASS_MAG_STRIPE_CVM_CAPABILITY_CVM_REQUIRED                  = 0xDF811E,
    EWL_PAYPASS_SECURITY_CAPABILITY                                     = 0xDF811F,

    EWL_PAYPASS_READER_CONTACTLESS_TRANSACTION_LIMIT_NO_ON_DEVICE_CVM   = 0xDF8124,
    EWL_PAYPASS_READER_CONTACTLESS_TRANSACTION_LIMIT_ON_DEVICE_CVM      = 0xDF8125,
    EWL_PAYPASS_TIME_OUT_VALUE                                          = 0xDF8127,
    EWL_PAYPASS_IDS_STATUS                                              = 0xDF8128,
    EWL_PAYPASS_OUTCOME_PARAMETER_SET                                   = 0xDF8129,
    EWL_PAYPASS_DD_CARD_TRACK1                                          = 0xDF812A,
    EWL_PAYPASS_DD_CARD_TRACK2                                          = 0xDF812B,
    EWL_PAYPASS_MAG_STRIPE_CVM_CAPABILITY_NO_CVM_REQUIRED               = 0xDF812C,
    EWL_PAYPASS_MESSAGE_HOLD_TIME                                       = 0xDF812D,

    EWL_PAYPASS_HOLD_TIME_VALUE                                         = 0xDF8130,

    EWL_PAYPASS_TORN_RECORD                                             = 0xFF8101,
    EWL_PAYPASS_TAGS_TO_WRITE_BEFORE_GEN_AC                             = 0xFF8102,
    EWL_PAYPASS_TAGS_TO_WRITE_AFTER_GEN_AC                              = 0xFF8103,
    EWL_PAYPASS_DATA_TO_SEND                                            = 0xFF8104,
    EWL_PAYPASS_DATA_RECORD                                             = 0xFF8105,
    EWL_PAYPASS_DISCRETIONARY_DATA                                      = 0xFF8106,
};

enum ewlPayPassTagsLen_t {
    EWL_PAYPASS_TRACK1_MAX_LEN                                              = 76,                                               /**< Tag 56 Len [1] */

    EWL_PAYPASS_OFFLINE_ACCUMULATOR_BALANCE_LEN                             = 6,                                                /**< Tag 9F50 Len [2] */
    EWL_PAYPASS_DRDOL_MAX_LEN                                               = 256,                                              /**< Tag 9F51 Len [2] */

    EWL_PAYPASS_DS_ODS_CARD_MAX_LEN                                         = 160,                                              /**< Tag 9F54 Len */

    //EWL_PAYPASS_DSDOL_MAX_LEN                                             = undefined,                                        /**< Tag 9F5B Len [2] [4] */
    EWL_PAYPASS_DS_REQUESTED_OPERATOR_ID_LEN                                = 8,                                                /**< Tag 9F5C Len */

    EWL_PAYPASS_APPLICATION_CAPABILITIES_INFORMATION_LEN                    = 3,                                                /**< Tag 9F5D Len [2] */
    EWL_PAYPASS_DS_ID_MAX_LEN                                               = 11,                                               /**< Tag 9F5E Len [2] */
    EWL_PAYPASS_DS_SLOT_AVAILABILITY_LEN                                    = 1,                                                /**< Tag 9F5F Len [2] */
    EWL_PAYPASS_CVC3_TRACK1_LEN                                             = 2,                                                /**< Tag 9F60 Len */
    EWL_PAYPASS_CVC3_TRACK2_LEN                                             = 2,                                                /**< Tag 9F61 Len */
    EWL_PAYPASS_PCVC3_TRACK1_LEN                                            = 6,                                                /**< Tag 9F62 Len */
    EWL_PAYPASS_PUNATC_TRACK1_LEN                                           = 6,                                                /**< Tag 9F63 Len */
    EWL_PAYPASS_NATC_TRACK1_LEN                                             = 1,                                                /**< Tag 9F64 Len */
    EWL_PAYPASS_PCVC3_TRACK2_LEN                                            = 2,                                                /**< Tag 9F65 Len */
    EWL_PAYPASS_PUNATC_TRACK2_LEN                                           = 2,                                                /**< Tag 9F66 Len [2] */

    EWL_PAYPASS_NATC_TRACK2_LEN                                             = 1,                                                /**< Tag 9F67 Len */
    //EWL_PAYPASS_UDOL_MAX_LEN                                              = undefined,                                        /**< Tag 9F69 Len [2] [4] */
    EWL_PAYPASS_MSTRIPE_UNPREDICTABLE_NUMBER_LEN                            = 8,                                                /**< Tag 9F6A Len*/
    EWL_PAYPASS_TRACK2_MAX_LEN                                              = 19,                                               /**< Tag 9F6B Len [2] */
    EWL_PAYPASS_MSTRIPE_APPLI_VERSION_NUMBER_ICC_LEN                        = 2,                                                /**< Tag 9F6C Len [2] */
    EWL_PAYPASS_MSTRIPE_APPLI_VERSION_NUMBER_TERM_LEN                       = 2,                                                /**< Tag 9F6D Len [2] */
    EWL_PAYPASS_THIRD_PARTY_DATA_MAX_LEN                                    = 32,                                               /**< Tag 9F6E Len [2] */
    EWL_PAYPASS_DS_SLOT_MANAGEMENT_CONTROL_LEN                              = 1,                                                /**< Tag 9F6F Len */

    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_X_MAX_LEN                           = 192,
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_1_MAX_LEN                           = EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_X_MAX_LEN,    /**< Tag 9F70 Len [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_2_MAX_LEN                           = EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_X_MAX_LEN,    /**< Tag 9F71 Len [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_3_MAX_LEN                           = EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_X_MAX_LEN,    /**< Tag 9F72 Len [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_4_MAX_LEN                           = EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_X_MAX_LEN,    /**< Tag 9F73 Len [2] */
    EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_5_MAX_LEN                           = EWL_PAYPASS_PROTECTED_DATA_ENVELOPE_X_MAX_LEN,    /**< Tag 9F74 Len [2] */

    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_X_MAX_LEN                         = 192,
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_1_MAX_LEN                         = EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_X_MAX_LEN,  /**< Tag 9F75 Len [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_2_MAX_LEN                         = EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_X_MAX_LEN,  /**< Tag 9F76 Len [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_3_MAX_LEN                         = EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_X_MAX_LEN,  /**< Tag 9F77 Len [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_4_MAX_LEN                         = EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_X_MAX_LEN,  /**< Tag 9F78 Len [2] */
    EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_5_MAX_LEN                         = EWL_PAYPASS_UNPROTECTED_DATA_ENVELOPE_X_MAX_LEN,  /**< Tag 9F79 Len */

    EWL_PAYPASS_MERCHANT_CUSTOM_DATA_LEN                                    = 20,                                               /**< Tag 9F7C Len [1] [2] */
    EWL_PAYPASS_DS_SUMMARY_1_LEN                                            = 8,                                                /**< Tag 9F7D Len [2] */
    EWL_PAYPASS_MOBILE_SUPPORT_INDICATOR_LEN                                = 1,                                                /**< Tag 9F7E Len [2] */
    EWL_PAYPASS_DS_UNPREDICTABLE_NUMBER_LEN                                 = 4,                                                /**< Tag 9F7F Len [2] */

    EWL_PAYPASS_POS_CARDHOLDER_INTERACTION_INFO_LEN                         = 4,                                                /**< Tag DF4B Len */

    EWL_PAYPASS_DS_INPUT_CARD_LEN                                           = 8,                                                /**< Tag DF60 Len */
    EWL_PAYPASS_DS_DIGEST_H_LEN                                             = 8,                                                /**< Tag DF61 Len */
    EWL_PAYPASS_DS_ODS_INFO_LEN                                             = 1,                                                /**< Tag DF62 Len */
    EWL_PAYPASS_DS_ODS_TERM_MAX_LEN                                         = 160,                                              /**< Tag DF63 Len */


    EWL_PAYPASS_DS_SUMMARY_2_LEN                                            = 8,                                                /**< Tag DF8101  Len */
    EWL_PAYPASS_DS_SUMMARY_3_LEN                                            = 8,                                                /**< Tag DF8102  Len */
    EWL_PAYPASS_BALANCE_READ_BEFORE_GEN_AC_LEN                              = 6,                                                /**< Tag DF8104  Len */
    EWL_PAYPASS_BALANCE_READ_AFTER_GEN_AC_LEN                               = 6,                                                /**< Tag DF8105  Len */
    //EWL_PAYPASS_DATA_NEEDED_MAX_LEN                                       = undefined,                                        /**< Tag DF8106  Len */
    EWL_PAYPASS_DS_AC_TYPE_LEN                                              = 1,                                                /**< Tag DF8108  Len */
    EWL_PAYPASS_DS_INPUT_TERM_LEN                                           = 8,                                                /**< Tag DF8109  Len */
    EWL_PAYPASS_DS_ODS_INFO_FOR_READER_LEN                                  = 1,                                                /**< Tag DF810A  Len */
    EWL_PAYPASS_DS_SUMMARY_STATUS_LEN                                       = 1,                                                /**< Tag DF810B  Len */

    //EWL_PAYPASS_DSVN_TERM_MAX_LEN                                         = undefined,                                        /**< Tag DF810D  Len */
    EWL_PAYPASS_POST_GEN_AC_PUT_DATA_STATUS_LEN                             = 1,                                                /**< Tag DF810E  Len */
    EWL_PAYPASS_PRE_GEN_AC_PUT_DATA_STATUS_LEN                              = 1,                                                /**< Tag DF810F  Len */
    EWL_PAYPASS_PROCEED_TO_FIRST_WRITE_FLAG_LEN                             = 1,                                                /**< Tag DF8110  Len */

    //EWL_PAYPASS_TAGS_TO_READ_MAX_LEN                                      = undefined,                                        /**< Tag DF8112  Len */
    EWL_PAYPASS_DRDOL_RELATED_DATA_LEN                                      = 256,                                              /**< Tag DF8113  Len */
    EWL_PAYPASS_REFERENCE_CONTROL_PARAMETER_MAX_LEN                         = 1,                                                /**< Tag DF8114  Len */
    EWL_PAYPASS_ERROR_INDICATION_LEN                                        = 6,                                                /**< Tag DF8115  Len */
    EWL_PAYPASS_USER_INTERFACE_REQUEST_DATA_LEN                             = 22,                                               /**< Tag DF8116  Len */
    EWL_PAYPASS_CARD_DATA_INPUT_CAPABILITY_LEN                              = 1,                                                /**< Tag DF8117  Len */
    EWL_PAYPASS_CVM_CAPABILITY_CVM_REQUIRED_LEN                             = 1,                                                /**< Tag DF8118  Len */
    EWL_PAYPASS_CVM_CAPABILITY_NO_CVM_REQUIRED_LEN                          = 1,                                                /**< Tag DF8119  Len */
    EWL_PAYPASS_DEFAULT_UDOL_LEN                                            = 3,                                                /**< Tag DF811A  Len */
    EWL_PAYPASS_KERNEL_CONFIGURATION_LEN                                    = 1,                                                /**< Tag DF811B  Len */
    EWL_PAYPASS_MAX_LIFETIME_OF_TORN_TRANSACTION_LOG_RECORD_LEN             = 2,                                                /**< Tag DF811C  Len */
    EWL_PAYPASS_MAX_NUMBER_OF_TORN_TRANSACTION_LOG_RECORDS_LEN              = 1,                                                /**< Tag DF811D  Len */
    EWL_PAYPASS_MAG_STRIPE_CVM_CAPABILITY_CVM_REQUIRED_LEN                  = 1,                                                /**< Tag DF811E  Len */
    EWL_PAYPASS_SECURITY_CAPABILITY_LEN                                     = 1,                                                /**< Tag DF811F  Len */

    EWL_PAYPASS_READER_CONTACTLESS_TRANSACTION_LIMIT_NO_ON_DEVICE_CVM_LEN   = 6,                                                /**< Tag DF8124  Len */
    EWL_PAYPASS_READER_CONTACTLESS_TRANSACTION_LIMIT_ON_DEVICE_CVM_LEN      = 6,                                                /**< Tag DF8125  Len */

    EWL_PAYPASS_TIME_OUT_VALUE_LEN                                          = 2,                                                /**< Tag DF8127  Len */
    EWL_PAYPASS_IDS_STATUS_LEN                                              = 1,                                                /**< Tag DF8128  Len */
    EWL_PAYPASS_OUTCOME_PARAMETER_SET_LEN                                   = 8,                                                /**< Tag DF8129  Len */
    EWL_PAYPASS_DD_CARD_TRACK1_MAX_LEN                                      = 56,                                               /**< Tag DF812A  Len */
    EWL_PAYPASS_DD_CARD_TRACK2_MAX_LEN                                      = 8,                                                /**< Tag DF812B  Len */
    EWL_PAYPASS_MAG_STRIPE_CVM_CAPABILITY_NO_CVM_REQUIRED_LEN               = 1,                                                /**< Tag DF812C  Len */
    EWL_PAYPASS_MESSAGE_HOLD_TIME_LEN                                       = 3,                                                /**< Tag DF812D  Len */

    EWL_PAYPASS_HOLD_TIME_VALUE_LEN                                         = 1,                                                /**< Tag DF8130  Len */

    EWL_PAYPASS_TORN_RECORD_MAX_LEN                                         = 2048,                                             /**< Tag FF8101  Len */
    //EWL_PAYPASS_TAGS_TO_WRITE_BEFORE_GEN_AC_MAX_LEN                       = undefined,                                        /**< Tag FF8102  Len */
    //EWL_PAYPASS_TAGS_TO_WRITE_AFTER_GEN_AC_MAX_LEN                        = undefined,                                        /**< Tag FF8103  Len */
    //EWL_PAYPASS_DATA_TO_SEND_MAX_LEN                                      = undefined,                                        /**< Tag FF8104  Len */
    EWL_PAYPASS_DATA_RECORD_MAX_LEN                                         = 512,                                              /**< Tag FF8105  Len */
    //EWL_PAYPASS_DISCRETIONARY_DATA_MAX_LEN                                = undefined,                                        /**< Tag FF8106  Len */
};
#endif


#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
/** Library ExpressPay tags */
enum ewExpressPayTags_t  {
    EWL_EXPRESSPAY_TERMINAL_CAPABILITIES                = TAG_EXPRESSPAY_TERMINAL_CAPABILITIES,                     /**< Tag 9F6D [2] */
    EWL_EXPRESSPAY_TRANSACTION_CAPABILITIES             = TAG_EXPRESSPAY_TERMINAL_TRANSACTION_CAPABILITIES,         /**< Tag 9F6E [2] */

    EWL_EXPRESSPAY_CARD_INTERFACE_CAPABILITIES          = TAG_EXPRESSPAY_CARD_INTERFACE_CAPABILITIES,               /**< Tag 9F70 [2] */
    EWL_EXPRESSPAY_MOBILE_CVM_RESULTS                   = TAG_EXPRESSPAY_MOBILE_CVM_RESULTS,                        /**< Tag 9F71 [2] */
};

enum ewlExpressPayTagsLen_t {
    EWL_EXPRESSPAY_TERMINAL_CAPABILITIES_LEN                = 1,                                                    /**< Tag 9F6D Len [2] */
    EWL_EXPRESSPAY_TRANSACTION_CAPABILITIES_LEN             = 4,                                                    /**< Tag 9F6E Len [2] */

    EWL_EXPRESSPAY_CARD_INTERFACE_CAPABILITIES_LEN          = 2,                                                    /**< Tag 9F70 Len [2] */
    EWL_EXPRESSPAY_MOBILE_CVM_RESULTS_LEN                   = 3,                                                    /**< Tag 9F71 Len [2] */
};
#endif


#ifdef EWL_ENABLE_KERNEL_PURE

/** Library PURE tags */
enum ewPureTags_t  {
    EWL_PURE_MEMORY_SLOT_SETTING                        = TAG_PURE_MEMORY_SLOT_SETTING,                             /**< Tag 85 */

    EWL_PURE_OFFLINE_ACCUMULATOR_BALANCE                = TAG_PURE_OFFLINE_ACCUMULATOR_BALANCE,                     /**< Tag 9F50 [3] \ref EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT */
    EWL_PURE_DRDOL                                      = TAG_PURE_DRDOL,                                           /**< Tag 9F51 */

    EWL_PURE_GDDOL                                      = TAG_PURE_GDDOL,                                           /**< Tag 9F70 [2] */

    EWL_PURE_DS_ID                                      = TAG_PURE_DS_ID,                                           /**< Tag 9F5E [2] */
    EWL_PURE_DS_SLOT_AVAILABILITY                       = TAG_PURE_DS_SLOT_AVAILABILITY,                            /**< Tag 9F5F [2] */

    EWL_PURE_GDDOL_RESULT                               = TAG_PURE_GDDOL_RESULT,                                    /**< Tag 9F71 [2] */
    EWL_PURE_MEMORY_SLOT_IDENTIFIER                     = TAG_PURE_MEMORY_SLOT_IDENTIFIER,                          /**< Tag 9F72 [2] */
    EWL_PURE_ISSUER_SCRIPT_RESULTS                      = TAG_PURE_ISSUER_SCRIPT_RESULTS,                           /**< Tag 9F73 [3] \ref EWL_TAG_ISSUER_SCRIPT_RESULT */
    EWL_PURE_UPDATE_RESULT                              = TAG_PURE_UPDATE_RESULT,                                   /**< Tag 9F74 [2] */
    EWL_PURE_ECHO_CARD_IDENTIFIER                       = TAG_PURE_ECHO_CARD_IDENTIFIER,                            /**< Tag 9F75 [2] */
    EWL_PURE_TERMINAL_TRANSACTION_DATA                  = TAG_PURE_TERMINAL_TRANSACTION_DATA,                       /**< Tag 9F76 [2] */
    EWL_PURE_TERMINAL_DEDICATED_DATA                    = TAG_PURE_TERMINAL_DEDICATED_DATA,                         /**< Tag 9F77 [2] */

    EWL_PURE_MERCHANT_CUSTOM_DATA                       = TAG_PURE_MERCHANT_CUSTOM_DATA,                            /**< Tag 9F7C [2] */
    EWL_PURE_DS_SUMMARY_1                               = TAG_PURE_DS_SUMMARY_1,                                    /**< Tag 9F7D [2] */
    EWL_PURE_MOBILE_SUPPORT_INDICATOR                   = TAG_PURE_MOBILE_SUPPORT_INDICATOR,                        /**< Tag 9F7E [2] */
    EWL_PURE_DS_UNPREDICTABLE_NUMBER                    = TAG_PURE_DS_UNPREDICTABLE_NUMBER,                         /**< Tag 9F7F [2] */

	EWL_PURE_ATOL										= TAG_PURE_ATOL,   											/**< Tag 9F918D01 [14] */
	EWL_PURE_MTOL										= TAG_PURE_MTOL,   											/**< Tag 9F918D02 [14] */
	EWL_PURE_KERNEL_CAPABILITIES						= TAG_PURE_KERNEL_CAPABILITIES,								/**< Tag 9F918D04 [14] */

    EWL_PURE_MEMORY_SLOT_UPDATE_1                       = TAG_PURE_MEMORY_SLOT_UPDATE_1,                            /**< Tag A2 up date tag before GPO */
    EWL_PURE_MEMORY_SLOT_UPDATE_2                       = TAG_PURE_MEMORY_SLOT_UPDATE_2,                            /**< Tag A3 up date tag after GPO*/
    EWL_PURE_MEMORY_SLOT_UPDATE_3                       = TAG_PURE_MEMORY_SLOT_UPDATE_3,                            /**< Tag A4 up date tag after first generate*/

    EWL_PURE_MEMORY_SLOT_UPDATE_TEMPLATE                = 0xBF70,                                                   /**< Tag BF70 has the EWL_PURE_MEMORY_SLOT_UPDATE_X*/
    EWL_PURE_MEMORY_SLOT_READ_TEMPLATE                  = 0xBF71,                                                   /**< Tag BF71 has the GDDOL*/

    EWL_PURE_CCID                                       = TAG_PURE_CCID,                                            /**< Tag C5 */

    EWL_PURE_TTPI                                       = TAG_PURE_TTPI,                                            /**< Tag C7 */

    EWL_PURE_CRM_CURRENCY_CODE                          = TAG_PURE_CRM_CURRENCY_CODE,                               /**< Tag CD */

    EWL_PURE_DATA_ENVELOPE_1                            = TAG_PURE_DATA_ENVELOPE_1,                                 /**< Tag DF01 */
    EWL_PURE_DATA_ENVELOPE_2                            = TAG_PURE_DATA_ENVELOPE_2,                                 /**< Tag DF02 */
    EWL_PURE_DATA_ENVELOPE_3                            = TAG_PURE_DATA_ENVELOPE_3,                                 /**< Tag DF03 */
    EWL_PURE_DATA_ENVELOPE_4                            = TAG_PURE_DATA_ENVELOPE_4,                                 /**< Tag DF04 */
    EWL_PURE_DATA_ENVELOPE_5                            = TAG_PURE_DATA_ENVELOPE_5,                                 /**< Tag DF05 */
    EWL_PURE_DATA_ENVELOPE_6                            = TAG_PURE_DATA_ENVELOPE_6,                                 /**< Tag DF06 */
    EWL_PURE_DATA_ENVELOPE_7                            = TAG_PURE_DATA_ENVELOPE_7,                                 /**< Tag DF07 */
    EWL_PURE_DATA_ENVELOPE_8                            = TAG_PURE_DATA_ENVELOPE_8,                                 /**< Tag DF08 */
    EWL_PURE_DATA_ENVELOPE_9                            = TAG_PURE_DATA_ENVELOPE_9,                                 /**< Tag DF09 */
    EWL_PURE_DATA_ENVELOPE_A                            = TAG_PURE_DATA_ENVELOPE_A,                                 /**< Tag DF0A */

    EWL_PURE_POS_CARDHOLDER_INTERACTION_INFO            = TAG_PURE_POS_CARDHOLDER_INTERACTION_INFO,                 /**< Tag DF4B [2] */
};

enum ewlPureTagsLen_t {
    EWL_PURE_MEMORY_SLOT_SETTING_LEN                        = 1,                                                    /**< Tag 85 Len */

    EWL_PURE_OFFLINE_ACCUMULATOR_BALANCE_LEN                = 6,                                                    /**< Tag 9F50 Len [2] */
    EWL_PURE_DRDOL_MAX_LEN                                  = 256,                                                  /**< Tag 9F51 Len [2] */

    EWL_PURE_DS_ID_MAX_LEN                                  = 11,                                                   /**< Tag 9F5E Len [2] */
    EWL_PURE_DS_SLOT_AVAILABILITY_LEN                       = 1,                                                    /**< Tag 9F5F Len [2] */

    EWL_PURE_GDDOL_MAX_LEN                                  = 50,                                                   /**< Tag 9F70 Len [2] */
    EWL_PURE_GDDOL_RESULT_MAX_LEN                           = 512,                                                  /**< Tag 9F71 Len [2] */
    EWL_PURE_MEMORY_SLOT_IDENTIFIER_LEN                     = 16,                                                   /**< Tag 9F72 Len [2] */
    EWL_PURE_ISSUER_SCRIPT_RESULTS_MAX_LEN                  = 50,                                                   /**< Tag 9F73 Len [2] */
    EWL_PURE_UPDATE_RESULT_LEN                              = 2,                                                    /**< Tag 9F74 Len [2] */
    EWL_PURE_ECHO_CARD_IDENTIFIER_MAX_LEN                   = 20,                                                   /**< Tag 9F75 Len [2] */
    EWL_PURE_TERMINAL_TRANSACTION_DATA_MAX_LEN              = 250,                                                  /**< Tag 9F76 Len [2] */
    EWL_PURE_TERMINAL_DEDICATED_DATA_MAX_LEN                = 250,                                                  /**< Tag 9F77 Len [2] */

    EWL_PURE_MERCHANT_CUSTOM_DATA_MAX_LEN                   = 20,                                                   /**< Tag 9F7C Len [1] [2] */
    EWL_PURE_DS_SUMMARY_1_LEN                               = 8,                                                    /**< Tag 9F7D Len [2] */
    EWL_PURE_MOBILE_SUPPORT_INDICATOR_LEN                   = 1,                                                    /**< Tag 9F7E Len [2] */
    EWL_PURE_DS_UNPREDICTABLE_NUMBER_LEN                    = 4,                                                    /**< Tag 9F7F Len [2] */

	EWL_PURE_ATOL_MAX_LEN									= 30,   												/**< Tag 9F918D01 Len [14] */
	EWL_PURE_MTOL_MAX_LEN									= 50,   												/**< Tag 9F918D02 Len [14] */
	EWL_PURE_KERNEL_CAPABILITIES_LEN					    = 5,													/**< Tag 9F918D04 Len [14] */

    EWL_PURE_MEMORY_SLOT_UPDATE_X_MAX_LEN                   = 260,                                                  /**< All EWL_PURE_MEMORY_SLOT_UPDATE has the same length */
    EWL_PURE_MEMORY_SLOT_UPDATE_1_MAX_LEN                   = EWL_PURE_MEMORY_SLOT_UPDATE_X_MAX_LEN,                /**< Tag A2 Len */
    EWL_PURE_MEMORY_SLOT_UPDATE_2_MAX_LEN                   = EWL_PURE_MEMORY_SLOT_UPDATE_X_MAX_LEN,                /**< Tag A3 Len */
    EWL_PURE_MEMORY_SLOT_UPDATE_3_MAX_LEN                   = EWL_PURE_MEMORY_SLOT_UPDATE_X_MAX_LEN,                /**< Tag A4 Len */


    EWL_PURE_MEMORY_SLOT_UPDATE_TEMPLATE_MAX_LEN            = 800,                                                  /**< Tag BF70 Len*/
    EWL_PURE_MEMORY_SLOT_READ_TEMPLATE_MAX_LEN              = 80,                                                   /**< Tag BF71 Len*/

    EWL_PURE_CCID_LEN                                       = 1,                                                    /**< Tag C5 Len */

    EWL_PURE_TTPI_LEN                                       = 5,                                                    /**< Tag C7 Len */

    EWL_PURE_CRM_CURRENCY_CODE_LEN                          = 2,                                                    /**< Tag CD Len */

    EWL_PURE_DATA_ENVELOPE_X_MAX_LEN                        = 250,                                                  /**< All EWL_PURE_DATA_ENVELOPE has the same length */
    EWL_PURE_DATA_ENVELOPE_1_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF01 Len */
    EWL_PURE_DATA_ENVELOPE_2_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF02 Len */
    EWL_PURE_DATA_ENVELOPE_3_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF03 Len */
    EWL_PURE_DATA_ENVELOPE_4_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF04 Len */
    EWL_PURE_DATA_ENVELOPE_5_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF05 Len */
    EWL_PURE_DATA_ENVELOPE_6_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF06 Len */
    EWL_PURE_DATA_ENVELOPE_7_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF07 Len */
    EWL_PURE_DATA_ENVELOPE_8_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF08 Len */
    EWL_PURE_DATA_ENVELOPE_9_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF09 Len */
    EWL_PURE_DATA_ENVELOPE_A_MAX_LEN                        = EWL_PURE_DATA_ENVELOPE_X_MAX_LEN,                     /**< Tag DF0A Len */

    EWL_PURE_POS_CARDHOLDER_INTERACTION_INFO_LEN            = 3,                                                    /**< Tag DF4B Len */

};
#endif


#ifdef EWL_ENABLE_KERNEL_DISCOVER_PAS
/** Library PayWave tags */
enum ewlDiscoverPASTags_t  {
	EWL_DISCOVERPAS_TRACK1_DATA                             = TAG_DISCOVER_DPAS_TRACK1_DATA,                        /**< Tag 56 [1][2] */
    EWL_DISCOVERPAS_CARD_OFFLINE_BALANCE                    = TAG_DISCOVER_DPAS_CARD_OFFLINE_BALANCE,               /**< Tag D1 */
	EWL_DISCOVERPAS_CONTACTLESS_CARD_VERIFICATION_RESULT	= TAG_DISCOVER_DPAS_CARD_VERIFICATION_RESULTS, 			/**< Tag 9F57 [2] */
	EWL_DISCOVERPAS_TTQ                                     = TAG_DISCOVER_DPAS_TERMINAL_TRANSACTION_QUALIFIERS,    /**< Tag 9F66 [2] */
    EWL_DISCOVERPAS_CARD_PROCESSING_REQUIREMENTS            = TAG_DISCOVER_DPAS_CARD_PROCESSING_REQUIREMENTS,       /**< Tag 9F71 [2] */
};

enum ewlDiscoverPASTagsLen_t {
    EWL_DISCOVERPAS_TRACK1_DATA_MAX_LEN                     	= 76,                                                   /**< Tag 56 Len [1][2] */
    EWL_DISCOVERPAS_CARD_OFFLINE_BALANCE_LEN                	= EWL_BCD_VALUE_LEN,                                    /**< Tag D1 Len*/
	EWL_DISCOVERPAS_CONTACTLESS_CARD_VERIFICATION_RESULT_LEN	= 8,													/**< Tag 9F57 Len [2] */
    EWL_DISCOVERPAS_TTQ_LEN                                 	= 4,                                                    /**< Tag 9F66 Len [2] */
    EWL_DISCOVERPAS_CARD_PROCESSING_REQUIREMENTS_LEN        	= 2,                                                    /**< Tag 9F71 Len [2] */

};
#endif

#ifdef EWL_ENABLE_KERNEL_DISCOVER_ZIP
/** Library PayWave tags */
enum ewlDiscoverZIPTags_t  {
    EWL_DISCOVERZIP_TRACK1_DATA                             = TAG_DISCOVER_TRACK1_DATA,                             /**< Tag 56 [1][2] */
    EWL_DISCOVERZIP_APPLI_VERSION_NUMBER_ICC                = TAG_DISCOVER_APPLI_VERSION_NUMBER_ICC,                /**< Tag 9F7D [2] */
    EWL_DISCOVERZIP_DCVV                                    = TAG_DISCOVER_DCVV,                                    /**< Tag 9F7E [2] */

};

enum ewlDiscoverZIPTagsLen_t {
    EWL_DISCOVERZIP_TRACK1_DATA_MAX_LEN                     = TAG_DISCOVER_TRACK1_DATA,                             /**< Tag 56 [1][2] */
    EWL_DISCOVERZIP_APPLI_VERSION_NUMBER_ICC_LEN            = 2,                                                    /**< Tag 9F7D [2] */
    EWL_DISCOVERZIP_DCVV_LEN                                = 3,                                                    /**< Tag 9F7E [2][11] */
};
#endif

#ifdef EWL_ENABLE_KERNEL_QUICKPASS
/** Library PayWave tags */
enum ewlQuickPassTags_t  {
    EWL_QUICKPASS_FDDA_VERSION                             	= TAG_QUICKPASS_FDDA_VERSION ,                          /**< Tag 9F928507 [14]*/
};

enum ewlQuickPassLen_t {
	EWL_QUICKPASS_FDDA_VERSION_LEN                     		= 2,                             						/**< Tag 9F928507 [14]*/
};
#endif





// [1] ASCII        - This tag value was defined as ASCII value, so some times is necessary consider a \x00 value at the end, The TAG_LEN associate do not consider the \x00 at the end.
// [2] Collision    - This tag was defined in other kernel.
// [3] Kernel proprietary internal collision - This tag was define as Ingenico proprietary kernel tag.
// [4] Undefined    - This tag has variable length and do not have a maximum length defined.
// [5] Use example:
//
// int ChangeCallback (){
//     int ret;
//     void *temp = callbackFunction;
//
//     ret = ewlSetParameter(data->emvHandle,EWL_TAG_CALLBACK_???, callbackFunction,EWL_TAG_CALLBACK_???_LEN);
//     if(ret != EWL_OK) return ret;
//
//     return ret;
// }
// [6] Valid only when we use ewlModule/ewlModuleLib instead of EWL library
// [7] ASCII value length up to 8 bytes in telium 3 Terminals
// [8] Use this flag with caution, once ExpressPay 1 do not follow EMV book B ( mandatory 2PAY.PSY.DDF01 ) so, if you active this flag the transaction can become a little slower (other than Expresspay), try obtain a waver if possible before use this solution
// [9] See ewltransactionType_t
// [10] Valid only to transaction service environment
// [11] Value empirically found, under revision
// [12] Set this this only make sense before selection phase.
// [13] Internal USE, normally this tag is not necessary to normal transactions
// [14] Ingenico Propietary tag but named by specification


#endif





