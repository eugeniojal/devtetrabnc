#ifndef EWLFUNCTIONS_H
#define EWLFUNCTIONS_H

/**
 * Create an EWL instance.
 *
 * @param callbacks List of callback functions
 * @param callbacks List to kernel function
 *
 * Use the struct ewlKernelFunctions present in the file ewlCBKernel and
 *
 * @return NULL if could not create a new EWL instance, a new EWL instance otherwise.
 *
 * @note Each EWL \e transaction must be executed in a different instance.
 *  Re-using the same EWL instance for multiple transactions is not supported and
 *  has undefined results.
 */
ewlObject_t *ewlCreate(const ewlCBFunctions_t *callbacks, const ewlCBKernel_t *functions );

/**
 * Add an AID to the candidate list.
 *
 * @param handle          EWL Handle
 * @param pAID            AID to be added
 *
 * @note Even though EWL itself does not impose any hard-coded limits on the size
 *  of the AID candidate list, a specific kernel version might.  This case will
 *  only be detected during \ref ewlGetCard().
 *
 * @return See \ref ewlStatus_t
 */
ewlStatus_t ewlAddAID(ewlObject_t *handle, const ewlAIDSetStruct_t *pAID);

/**
 * Initialize EMV transaction and select application.
 *
 * Execute the following steps:
 * -# Recover the common list of AIDs from terminal and card;
 * -# Execute the final selection process;
 * -# Execute the GPO process;
 * -# Read card records.
 *
 * @param handle    EWL Handle
 * @param interface Terminal device interface to be used for this transaction
 *
 * @return See \ref ewlStatus_t.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlGetCard(ewlObject_t *handle, ewlInterfaceType_t interface);

/**
 * Execute card processing until first generate AC.
 *
 * Execute the following steps:
 * -# Data authentication.
 * -# Terminal risk management.
 * -# Processing restrictions
 * -# Cardholder Verification.
 * -# Terminal action analysis.
 * -# Card action analysis
 *
 * The results of all those steps will be stored as tags directly in \p handle.
 * Use \ref ewlGetParameter() to read their values.
 *
 * @param handle EWL Handle
 * @return See \ref ewlStatus_t.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlGoOnChip(ewlObject_t *handle);

/**
 * Finish Chip
 *
 * Execute the following steps:
 * -# Issuer Authentication;
 * -# Script Processing (0x71);
 * -# Completion;
 * -# Script Processing (0x72)
 *
 * @param handle EWL Handle
 * @return See \ref ewlStatus_t.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlFinishChip(ewlObject_t *handle);

/**
 * Destroy an EWL object.
 *
 * @param handle EWL Hand�le to be destroyed
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
void ewlDestroy(ewlObject_t *handle);


/**
 * Check if the transaction can be done using defined interface.
 *
 * @param handle          Handle of object in use.
 * @param interface       Interface to be tested.

 * @return see \ref ewlStatus_t.
 *
 * \note This function can be used only after application set terminal AID list and transaction amount.
 *
 * \note this function Cannot be used if the handle was created using ewlDirectCreate
 *
 */
ewlStatus_t ewlTransactionAllowed(ewlObject_t *handle, ewlInterfaceType_t interface);


/**
 * Add an element to the list of revoked keys.
 *
 * @param handle EWL Handle
 * @param data
 * @return
 */
ewlStatus_t ewlSetRevoked(ewlObject_t *handle, const ewlSetRevoked_t *data );


/**
 * Check if is necessary a second tap in contacless transaction.
 *
 * @param handle          Handle of object in use.;;
 *
 * \note this function must be call before after all parameters provided by the host are sent to the library.
 *
 */
ewlStatus_t ewlSecondTapCheck(ewlObject_t *handle );


/**
 * Cancel an EWL instance.
 *
 * Interrupt a EWL function during their execution.
 *
 * void ewlCancel(ewlObject_t *handle)
 *
 *
 * @param handle    EWL Handle
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 *            This function is not necessary in normal flow, use only in task situations.
 *
 */

void ewlCancel(ewlObject_t *handle);


/**
 * Reset an EWL instance.
 *
 * Reset a EWL flow without lost the setted parametes.
 *
 * ewlReset (ewlObject_t *handle, bool keepAID)
 *
 *
 * @param handle    EWL Handle
 * @param keepAID   If true keep parameter and aid list, if false keep only parameters
 *
 */

void ewlReset (ewlObject_t *handle, bool keepAID);


/**
 *
 * Test components
 *
 * Test if all EWL requested component is loaded and configured in the terminal
 *
 *
 * @componets - components to be test use enum EWL_TEST_COMPONENTS
 * @out TRUE - All component is loaded and correctly configured
 *      FALSE - There are one or more components missing or misconfigured
 *
 */

bool ewlTestComponents (ewlTestKernelPresence_t components);


/**
 *
 * ewlAIDCandidateExtraData
 *
 * Recover additional data from a contact candidate.
 *
 * @param handle        EWL handle
 * @param data          pointer where the additional data will be stored
 * @param candidate     candidate id
 * @return
 */
int ewlAIDCandidateExtraData ( ewlObject_t *handle, const ewlAIDCandidateStruct_t *data
                             , uint16_t candidate);

/**
 * Initialize Cless Detection
 *
 * @param handle EWL Handle
 *
 * @return See \ref ewlStatus_t.
 *
 * \note If occur a card detection a event CLESS will occur.
 */
int ewlClessStartDetection (ewlObject_t *handle);


/**
 * Stop Cless Detection
 *
 * @param handle EWL Handle
 *
 * @return See \ref ewlStatus_t.
 */
int ewlClessStopDetection (ewlObject_t *handle);

/**
 * Initialize Contact Detection
 *
 * @param handle EWL Handle
 *
 * @return See \ref ewlStatus_t.
 *
 */
int ewlContactStartDetection (ewlObject_t *handle);

/**
 * Stop Cless Detection
 *
 * @param handle EWL Handle
 *
 * @return See \ref ewlStatus_t.
 *
 * \note If occur a card detection a event CAM0 will occur.
 */
int ewlContactStopDetection (ewlObject_t *handle);


int ewlTestContactPresence (ewlObject_t *handle);


#endif
