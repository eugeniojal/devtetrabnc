#ifndef EWLSTRUCT_H
#define EWLSTRUCT_H


/** Certificate request structure */
typedef struct {
    byte RID[EWL_TAG_RID_LEN];     /**< Card application RID. */
    uint16_t index;                     /**< Certification authority public key index. */
} ewlGetCertificate_t;

/** Certificate revoked request structure */
typedef struct {
    byte RID[EWL_TAG_RID_LEN];     /**< Card application RID */
    byte index;                /**< Certification authority public key index */
    byte certificateSN[EMV_CERTIFICATE_SERIAL_NUMBER_LEN]; /**< Revoked certificate serial number */
} ewlSetRevoked_t;

#if defined(EWL_ENABLE_KERNEL_PAYWAVE) || defined (EWL_IS_DLL)
/** qVSDC specific AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    byte        ttq[EWL_TTQ_LEN];           /**< Terminal transaction qualifier [2] [3]*/
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessqVSDCSetStruct_t;

/** MDS specific AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    byte        ttq[EWL_TTQ_LEN];           /**< Terminal transaction qualifier [2] [3]*/
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessMSDSetStruct_t;
#endif

#if defined(EWL_ENABLE_KERNEL_PAYPASS) || defined (EWL_IS_DLL)
/** PayPass specific Magnetic AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit No CDCVM Value */
    uint64_t    transactionLimitCDCVMValue; /**< ContactLess Transaction Limit CDCVM Value (if not defined set with the same value of transactionLimitValue)*/
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessPAYPASSMAGSetStruct_t;

/** PayPass specific MChip AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit No CDCVM Value */
    uint64_t    transactionLimitCDCVMValue; /**< ContactLess Transaction Limit CDCVM Value (if not defined set with the same value of transactionLimitValue)*/
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessMCHIPSetStruct_t;
#endif

#if defined(EWL_ENABLE_KERNEL_EXPRESSPAY ) || defined (EWL_IS_DLL)
/** ExpressPay specific Magnetic AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessExpressPayMAGSetStruct_t;

/** ExpressPay specific EMV AID data */
typedef struct {
    uint64_t    transactionLimitValue;  /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;       /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;  /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;      /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessExpressPayEMVSetStruct_t;
#endif

#if defined(EWL_ENABLE_KERNEL_PURE) || defined (EWL_IS_DLL)
/** PURE specific EMV AID data */
typedef struct {
    uint64_t    transactionLimitValue;  /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;       /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;  /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;      /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessPURESetStruct_t;
#endif

#if defined(EWL_ENABLE_KERNEL_DISCOVER_PAS) || defined (EWL_IS_DLL)
/** D-PAS (Discover Payment Application Specification) specific EMV AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    byte        ttq[EWL_TTQ_LEN];                /**< Terminal transaction qualifier */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessDiscoverPAS_t;
#endif

#if defined(EWL_ENABLE_KERNEL_DISCOVER_ZIP) || defined (EWL_IS_DLL)
/** D-PAS (Discover Payment Application Specification) specific EMV AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessDiscoverZIP_t;
#endif

#if defined(EWL_ENABLE_KERNEL_JSPEEDY) || defined (EWL_IS_DLL)
/** D-PAS (Discover Payment Application Specification) specific EMV AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessJSpeedy_t;
#endif

#if defined(EWL_ENABLE_KERNEL_QUICKPASS) || defined (EWL_IS_DLL)
/** D-PAS (Discover Payment Application Specification) specific EMV AID data */
typedef struct {
    uint64_t    transactionLimitValue;      /**< ContactLess Transaction Limit Value */
    uint64_t    cvmRequiredValue;           /**< ContactLess Transaction CVM required Value */
    uint64_t    contactLessFloorLimit;      /**< ContactLess Transaction floor limit Value */
    byte        ttq[EWL_TTQ_LEN];                /**< Terminal transaction qualifier */
    bool        zeroAmountAllowed;          /**< Indicate if zero amount is allowed in ContactLess transaction */
} ewlAIDContactlessQuickPass_t;
#endif


/** Specific EMV contact AID data
 * @note Currently EMV does not need any specific parameters. */
typedef struct {
    /* EMV contact operation do not need additional parameters */
} ewlAIDContactStruct_t;


/** Additional data associated with an AID */
typedef union {

    #if defined(EWL_ENABLE_KERNEL_PAYWAVE) || defined (EWL_IS_DLL)
    ewlAIDContactlessqVSDCSetStruct_t           qVSDC;          /**< qVDSC */
    ewlAIDContactlessMSDSetStruct_t             MSD;            /**< MSD */
    #endif

    #if defined(EWL_ENABLE_KERNEL_PAYPASS) || defined (EWL_IS_DLL)
    ewlAIDContactlessPAYPASSMAGSetStruct_t      PayPassMag;     /**< PayPass Magnetic */
    ewlAIDContactlessMCHIPSetStruct_t           PayPassMChip;   /**< PayPass MChip */
    #endif

    #if defined(EWL_ENABLE_KERNEL_EXPRESSPAY) || defined (EWL_IS_DLL)
    ewlAIDContactlessExpressPayMAGSetStruct_t   ExpressPayMag;  /**< ExpressPay Magnetic */
    ewlAIDContactlessExpressPayEMVSetStruct_t   ExpressPayEMV;  /**< ExpressPay EMV */
    #endif

    #if defined(EWL_ENABLE_KERNEL_PURE) || defined (EWL_IS_DLL)
    ewlAIDContactlessPURESetStruct_t            PURE;           /**< PURE */
    #endif

    #if defined(EWL_ENABLE_KERNEL_DISCOVER_PAS) || defined (EWL_IS_DLL)
    // To  EWL_TECHNOLOGY_DISCOVER_MAG see [1]
    ewlAIDContactlessDiscoverPAS_t              DiscoverPAS;   /**< Discover PAS (D-PAS) */
    #endif

    #if defined(EWL_ENABLE_KERNEL_DISCOVER_ZIP) || defined (EWL_IS_DLL)
    ewlAIDContactlessDiscoverZIP_t              DiscoverZIP;   /**< Discover ZIP (D-ZIP) */
    #endif

	#if defined(EWL_ENABLE_KERNEL_JSPEEDY) || defined (EWL_IS_DLL)
    ewlAIDContactlessJSpeedy_t              	JSpeedy;   /**< JSpeedy */
	#endif

	#if defined(EWL_ENABLE_KERNEL_QUICKPASS) || defined (EWL_IS_DLL)
	ewlAIDContactlessQuickPass_t              	QuickPass;   /**< QuickPass */
	#endif

    ewlAIDContactStruct_t                       contact;        /**< EMV contact */

} ewlAIDAdditionalParameter_t;

/** Information about an AID.  Used as parameter for \ref ewlAddAID().
 * The \c data field is used by EWL during callbacks to indicate which entry on the AID
 * candidate list is being referenced, but it is not forwarded to the kernel or chip.
 */
typedef struct{
    uint16_t                    aidLen;                         /**< AID length in bytes */
    byte                        aid[EWL_EMV_AID_CARD_MAX_LEN];  /**< AID */
    unsigned long               userData;                       /**< Extra information associated to AID (for example table index) */
    ewlTechnology_t             technology;                     /**< Technology associated to AID [1]*/
    ewlAIDAdditionalParameter_t aditionalParameter;             /**< Technology-specific additional data */
} ewlAIDSetStruct_t;

typedef struct{
    uint16_t                    aidLen;                                                         /**< AID length in bytes */
    byte                        aid[EWL_EMV_AID_CARD_MAX_LEN];                                  /**< AID */
    unsigned long               userData;                                                       /**< Extra information associated to AID (for example table index) */
    char                        displayLabel[EWL_TAG_LABEL_MAX_LEN + 1];                        /**< Label used in selection */
    char                        label[EWL_EMV_APPLICATION_LABEL_MAX_LEN  + 1];                  /**< Label recovered from card  (50) */
    char                        preferenceName[EWL_EMV_APPLI_PREFERRED_NAME_MAX_LEN + 1];       /**< Preference name recovered from card  (9F12) */
    byte                        priority;                                                       /**< Priority Indicator (0xFF if not exist) */
    bool                        selectMandatory;                                                /**< if true selection is mandatory */
    byte                        issuerCodeTableIndex;                                           /**< Issuer Code Table Index (0xFF if not exist) (9F18) */
} ewlAIDCandidateStruct_t;



/** Handle to an instance of EWL.
 * Multiple EWL handles may be open at one time, but they must not point to the same device.
 */
typedef struct ewlObject_t ewlObject_t;

/** Type used for a tag ID */
typedef uint32_t ewlTag_t;

/* [1] This field can not be set as EWL_TECHNOLOGY_DISCOVER_MAG, the kernel will
       detect itself, in mag case we need start as EWL_TECHNOLOGY_DISCOVER_PAS*/


/* [2] If equal 0x00,0x00,0x00,0x00 (recommendation) the EWL will use a TTQ base in:
 *     technology
 *     EWL_TAG_EXCLUSIVE_CLESS_EMV
 *     EWL_TAG_SUPPORT_CLESS_ISSUER_AUTENTICATION
 *     EWL_EMV_TERMINAL_CAPABILITIES
 *     EWL_EMV_TERMINAL_TYPE
 *     EWL_TAG_CLESS_MOBILE_SUPPORT
 *     or
 *     base in EWL_PAYWAVE_TTQ or EWL_DISCOVERPAS_TTQ
 */


#endif
