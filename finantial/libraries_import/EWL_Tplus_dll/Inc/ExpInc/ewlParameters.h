#ifndef EWLPARAMETERS_H
#define EWLPARAMETERS_H

/**
 * Set the value of a parameter on an EWL instance.
 *
 * Each EWL instance has, associated with it, a collection of \e parameters
 * associated with all steps of a transaction.  Some of those parameters
 * are required by EMV, others are of internal use.
 *
 * Repeated calls with the same \p tag value overwrite the previous
 * values.
 *
 * This call does not check the contents of \p data, only store it on
 * the data dictionary associated with \p handle.
 *
 * @param handle    EWL Handle
 * @param tag       Tag ID of parameter to be set
 * @param data      Value of parameter
 * @param dataLen   Size of \p data in bytes
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetParameter(ewlObject_t *handle, ewlTag_t tag, const void *data, int dataLen);


/**
 * Set a \p dateTime_t parameter on \p handle
 *
 * Convert \p time to EMV BCD format (3-byte HHMMSS) and call \p ewlSetParameter() with \p tag.
 *
 * @param handle          EWL Handle
 * @param tag             Tag that will be set.
 * @param time            Pointer to time structure to store
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetParameterTime(ewlObject_t *handle, ewlTag_t tag, const dateTime_t *time);

/**
 * Set a \p dateDate_t parameter on \p handle
 *
 * Convert \p date to EMV BCD format (3-byte YYMMDD) and call \p ewlSetParameter() with \p tag.
 *
 * @param handle          EWL Handle
 * @param tag             Tag that will be set.
 * @param date            Pointer to date structure to store
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetParameterDate(ewlObject_t *handle, ewlTag_t tag, const dateDate_t *date);

/**
 * Set an amount parameter in EWL.
 *
 * The actual behavior depends on \p tag.
 *
 * For the <em>transaction amount</em> (authorized or other) tags (\c AG_EMV_AMOUNT_AUTH_BIN,
 * \c TAG_EMV_AMOUNT_AUTH_NUM, \c TAG_EMV_AMOUNT_OTHER_BIN or
 * \c TAG_EMV_AMOUNT_OTHER_NUM), what will be actually stored depends on the
 * absolute value of \p value: according to EMV rules, if <tt>value \> 0xFFFFFFFF</tt> then
 * stores the \c _NUM tag with value \c 0xFFFFFFFF and the actual value as BCD in the \c _BIN
 * tag.  Otherwise store \c value as given into both \c _NUM and \c _BIN tags.
 *
 * Certain tags (\c EWL_TAG_CLESS_TRANSACTION_LIMIT, \c EWL_TAG_CLESS_FLOOR_LIMIT, \c EWL_TAG_CLESS_CVM_LIMIT,
 * \c TAG_EMV_TERMINAL_FLOOR_LIMIT) are forcibly 32-bit, and if \p value cannot be represented
 * in 32-bits, an error is returned.
 *
 * For all other tags, store \p value in either 32 or 64-bits, the smallest size that fits.
 *
 * @param handle          Handle of object in use.
 * @param tag             Tag that will be set.
 * @param value           value that will be storage in EWL object.
 *
 * @return See \ref ewlStatus_t
 *
 * \note this function Cannot be used if the handle was created using ewlDirectCreate
 */
ewlStatus_t ewlSetParameterAmount(ewlObject_t *handle, ewlTag_t tag, uint64_t value);


/**
 * Set the value of a non EMV parameter on an EWL instance.
 *
 * Each EWL instance has, associated with it, a collection of \e arbitrary data
 * associated with all steps of a transaction. Those arbitrary parameters are
 * reserved for the application.
 *
 * Repeated calls with the same \p tag can not be done
 *
 * This call does not allowed set the same parameter several times for each context
 *
 * @param handle    EWL Handle
 * @param tag       Tag ID of parameter to be set
 * @param data      Value of parameter
 * @param dataLen   Size of \p data in bytes
 *
 * @return See \ref ewlStatus_t
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
ewlStatus_t ewlSetNonEMVParameter(ewlObject_t *handle, ewlTag_t tag, const void *data, int dataLen);


/**
 * Read a parameter value from \p handle dictionary
 *
 * @param handle        EWL Handle
 * @param[out] data     Where to store the recovered parameter value
 * @param maxLen        Max number of bytes that can be written to \p data
 * @param tag           Tag ID of parameter to be read
 *
 * @return If >= 0, the actual number of bytes written to \p data
 * @return If < 0, error (see \ref ewlStatus_t)
 *
 * @note If you only need to retrieve the \e size of stored \p tag,
 *  call with <tt>maxLen = 0</tt> and <tt>data = NULL</tt>.
 *
 * @attention Cannot be used if \p handle was created using \ref ewlDirectCreate!
 */
int ewlGetParameter(ewlObject_t *handle, void *data, uint16_t maxLen, ewlTag_t tag);

#endif
