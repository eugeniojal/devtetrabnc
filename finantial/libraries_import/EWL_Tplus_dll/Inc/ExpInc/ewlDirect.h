#ifndef EWLDIRECT_H
#define EWLDIRECT_H

/** @addtogroup DirectChip Direct Chip API
 * @{
 *
 * The Direct Chip functions allow applications to execute direct APDU commands to
 * a Contact or Contact-less smart-card.
 *
 * An \ref ewlObject_t handle created with \ref ewlDirectCreate() cannot be used for
 * regular EMV processing, neither the opposite.  Even though the underlying data
 * structure handle is the same, this set of functions is mutually exclusive.
 */

/**
 * Create an EWL object where we can use APDU direct command.
 *
 * @param interface       Interface which will be used in direct commands.
 *
 * @return NULL           Cannot create a new  EWL environment.
 * @return Other          environment successfully created.
 */
ewlObject_t *ewlDirectCreate(ewlInterfaceType_t interface);

/**
 * Execute a smart-card <em>Power-On</em> command.
 *
 * Note: In tetra terminal this functions must be call after cless detection.
 *
 * @param handle          EWL Handle
 * @param out             Pointer to output buffer.
 * @param maxlen          Max length of output buffer.
 *
 * @return >= 0  length of ATR (contact) or UID (contact-less).
 * @return < 0   see \ref ewlStatus_t.
 *
 */
int ewlDirectPowerOn(ewlObject_t *handle, byte *out, uint16_t maxlen);

/**
 * Execute a smartcard power off.
 *
 * @param handle          Handle of object in use.
 *
 * \note this function Cannot be used if the handle was created using ewlCreate
 */
void ewlDirectPowerOff(ewlObject_t *handle);

/**
 * Send an \e APDU command to smart-card.
 *
 * @param handle          Handle of object in use.
 * @param out             Smart-card answer.
 * @param maxOutLen       Max length expected as card answer.
 * @param in              Command to be send to smart-card.
 * @param inLen           Command length in bytes.
 *
 * @return >= 0  length of card answer.
 * @return < 0   see \ref ewlStatus_t.
 *
 * \note this function Cannot be used if the handle was created using ewlCreate
 */
int ewlDirectCommand(ewlObject_t *handle, byte *out, uint16_t maxOutLen,
                     const byte *in, uint16_t inLen );

/**
 * Close an EWL created using ewlDirectCreate.
 *
 * @param handle          Handle of object to be close.
 *
 * \note this function Cannot be used if the handle was created using ewlCreate
 */
void ewlDirectDestroy(ewlObject_t *handle);

/* @} */


#endif
