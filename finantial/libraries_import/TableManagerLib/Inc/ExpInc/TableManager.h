/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			TableManager library
 * Header file:		TableManager.h
 * Description:		Functions to manage tables
 * Author:			jbotero, mamata
 * Version:			0200
 * SDK				9.28.0.01
 * --------------------------------------------------------------------------*/
#ifndef _H_TABLEMANAGER
#define _H_TABLEMANAGER

#include "File_utils.h"

/** Library version that this header file refers to */
#define TBLMGR_VERSION  "01.03"

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#define DISK_FMG					"/TBLS"

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

#define PACKED __attribute__((aligned (1), packed))

typedef struct tag_TABLE_HEADER
{
	long	NextID;
	long	NumberRecords;
	long	RecordLength;
	bool	DeletedRecords;
}PACKED TABLE_HEADER;

#define	L_TABLE_HEADER					sizeof(TABLE_HEADER)

typedef struct tag_RECROD_HEADER
{
	long	RecordID;
	long	FileIndex;
	long	RFU;
	bool	Deleted;
}PACKED RECORD_HEADER;

#define L_RECORD_HEADER 				sizeof(RECORD_HEADER)

#define TM_RET_OK						0
#define TM_RET_NOFMG					-1
#define TM_RET_NOVOLUME 				-2
#define TM_RET_FILE_EXISISTS			-3
#define TM_RET_FILE_CREATION_ERROR		-4
#define TM_RET_CANT_DELETE_FILE			-5
#define TM_RET_CANT_ADDRECORD			-6
#define TM_RET_CANT_DELETERECORD		-7
#define TM_RET_NO_MORE_RECORDS			-8
#define TM_RET_CANT_READ_RECORD			-9
#define TM_RET_CANT_MODIFY_RECORD		-10
#define TM_RET_CANT_OPEN_TABLE_HEADER	-11
#define TM_RET_CANT_SUPPORT_API			-12

#define L_FULL_PATH						100
#define LZ_FULL_PATH					L_FULL_PATH + 1


/* --------------------------------------------------------------------------
 * Function Name:	TM_InitFMG
 * Description:		Init FMG module and mount table disc
 * Parameters:
 *  - unsigned short ApplicationType
 * Return:
 *  - TM_RET_OK	:Everything is OK
 *  - TM_RET_NOFMG : Can't load FMG module
 *  - TM_RET_NOVOLUME : Can't mount table disk
 * Notes:
 */
int TM_InitFMG(unsigned short ApplicationType);

/* --------------------------------------------------------------------------
 * Function Name:	TM_DeleteDisk
 * Description:		Delete disk of files
 * Parameters:
 *  nothing
 * Return:
 *  - See FS_dskkill Errors
 * Notes:
 */
int TM_DeleteDisk(void);

/* --------------------------------------------------------------------------
 * Function Name:	TM_CreateTable
 * Description:		Create table in FMG module
 * Parameters:
 *  - char *Name - Name for table and file, up to 11 characters
 *  - unsigned int RecordLen - length of record
 * Return:
 *  - FMG_SUCCESS : file table created OK
 *  - TM_RET_FILE_CREATION_ERROR: can't create table file
 *  - TM_RET_FILE_EXISISTS : can't create table file / a file name already exists
 * Notes:
 */
extern int TM_CreateTable(char *Name, unsigned int RecordLen);


/* --------------------------------------------------------------------------
 * Function Name:	TM_DeleteTable
 * Description:		Delete table file
 * Parameters:
 *  - char *Name - name of table
 * Return:
 *  - TM_RET_OK : file deleted
 *  - TM_RET_CANT_DELETE_FILE : can't delete table file
 * Notes:
 */
extern int TM_DeleteTable(char *Name);


/* --------------------------------------------------------------------------
 * Function Name:	TM_AddRecord
 * Description:		Add  record at the end of a table
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to record data
 * Return:
 *  - TM_RET_OK - Record added OK
 *  - TM_RET_CANT_ADDRECORD - Can't add record
 * Notes:
 */
extern int TM_AddRecord(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TM_ModifyRecord
 * Description:		Modify/Update record
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer of record to be modify
 * Return:
 *  - TM_RET_OK - record modified OK
 *  - TM_RET_CANT_MODIFY_RECORD - cannot modify record
 * Notes:
 */
extern int TM_ModifyRecord(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TM_DeleteRecord
 * Description:		Delete record form table
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to data record
 * Return:
 *  - TM_RET_OK - Record deleted OK
 *  - TM_RET_CANT_DELETERECORD - cannot delete record
 * Notes:
 */
int TM_DeleteRecord(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TM_FindFirst
 * Description:		Find first record of table
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 * Return:
 *  - TM_RET_OK - record found
 * Notes:
 */
extern int TM_FindFirst(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TM_FindNext
 * Description:		Find next record in a table
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 * Return:
 *  - TM_RET_OK - record found
 *  - TM_RET_NO_MORE_RECORDS - no more records
 * Notes:
 */
extern int TM_FindNext(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TM_FindRecord
 * Description:		Find record by table RecordID
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 *  - long RecordID - Id of record to be find
 * Return:
 *  - TM_RET_OK - record found and place in Data
 *  - TM_RET_NO_MORE_RECORDS - record didn't find
 * Notes:
 */
int TM_FindRecord(char *Name, void *Data, long RecordID);


/* --------------------------------------------------------------------------
 * Function Name:	TM_GetDirectRecord
 * Description:		Get direct record by File Index
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 *  - long FileIndex- file index of record
 * Return:
 *  - TM_RET_OK - record found
 *  - TM_RET_NO_MORE_RECORDS - record didn't find
 *  - TM_RET_CANT_OPEN_TABLE_HEADER
 * Notes:
 */
int TM_GetDirectRecord(char *Name, void *Data, long FileIndex);


/* --------------------------------------------------------------------------
 * Function Name:	TM_GetLastRecord
 * Description:		Get last record
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 * Return:
 *  - TM_RET_OK - record found
 *  - TM_RET_NO_MORE_RECORDS - record didn't find
 *  - TM_RET_CANT_OPEN_TABLE_HEADER
 * Notes:
 */
int TM_GetLastRecord(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TM_GetTableHeader
 * Description:		Get table header
 * Parameters:
 *  - char *Name - name of table
 *  - TABLE_HEADER *TableHeader - pointer to put data
 * Return:
 *  - TM_RET_OK - table header opened OK
 *  - TM_RET_CANT_OPEN_TABLE_HEADER - cannot open table header
 * Notes:
 */
int TM_GetTableHeader(char *Name, TABLE_HEADER *TableHeader);

/* --------------------------------------------------------------------------
 * Function Name:	TM_WOC_OpenFile
 * Description:		open file
 * Parameters:
 *  - char *Name - name of table
 * Return:
 *  - T_HFILE - Pointer to file
 * Notes:
 */
//T_HFILE TM_WOC_OpenFile(char *Name);
T_HFILE TM_WOC_OpenFile(char *Name);

/* --------------------------------------------------------------------------
 * Function Name:	TM_WOC_OpenFile
 * Description:		open file
 * Parameters:
 *  - char *Name - name of table
 * Return:
 *  - T_HFILE - Pointer to file
 * Notes:
 */
//bool TM_WOC_CloseFile(T_HFILE hFile);
bool TM_WOC_CloseFile(char * pUcPath, T_HFILE hFile);

/* --------------------------------------------------------------------------
 * Function Name:	TM_WOC_Select
 * Description:		Select or read record
 * Parameters:
 *  - char *Name - name of table
 * Return:
 *  - T_HFILE - Pointer to file
 * Notes:
 */
//int TM_WOC_Select(T_HFILE hFile, void *Data, unsigned int index);
int TM_WOC_Select(T_HFILE hFile, void *Data, unsigned int index);

/* --------------------------------------------------------------------------
 * Function Name:	TM_WOC_Select_Ln
 * Description:		Select or read record, passing length of record by parameter
 * Parameters:
 *  - char *Name - name of table
 * Return:
 *  - T_HFILE - Pointer to file
 * Notes:
 */
//int TM_WOC_Select_Ln(T_HFILE hFile, void *Data, long length, unsigned int index);
int TM_WOC_Select_Ln(T_HFILE hFile, void *Data, long length, unsigned int index);


/* --------------------------------------------------------------------------
 * Function Name:	TMOld_InitFMG
 * Description:		Init FMG module and mount table disc
 * Parameters:
 *  - unsigned short ApplicationType
 * Return:
 *  - TM_RET_OK	:Everything is OK
 *  - TM_RET_NOFMG : Can't load FMG module
 *  - TM_RET_NOVOLUME : Can't mount table disk
 * Notes:
 */
int TMOld_InitFMG(unsigned short ApplicationType);


/* --------------------------------------------------------------------------
 * Function Name:	TMOld_DeleteTable
 * Description:		Delete table file
 * Parameters:
 *  - char *Name - name of table
 * Return:
 *  - TM_RET_OK : file deleted
 *  - TM_RET_CANT_DELETE_FILE : can't delete table file
 * Notes:
 */
int TMOld_DeleteTable(char *Name);

/* --------------------------------------------------------------------------
 * Function Name:	TMOld_FindFirst
 * Description:		Find first record of table
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 * Return:
 *  - TM_RET_OK - record found
 * Notes:
 */
int TMOld_FindFirst(char *Name, void *Data);


/* --------------------------------------------------------------------------
 * Function Name:	TMOld_FindNext
 * Description:		Find next record in a table
 * Parameters:
 *  - char *Name - name of table
 *  - void *Data - pointer to put record data
 * Return:
 *  - TM_RET_OK - record found
 *  - TM_RET_NO_MORE_RECORDS - no more records
 * Notes:
 */
int TMOld_FindNext(char *Name, void *Data);

#endif //_H_TABLEMANAGER
