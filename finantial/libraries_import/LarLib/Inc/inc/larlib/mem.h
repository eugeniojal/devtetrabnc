#ifndef LARLIB_MEM_H
#define LARLIB_MEM_H

/**
 * @file mem.h
 * @brief Memory Management
 *
 * @addtogroup larlib_mem Memory Management
 * @{
 *
 * \section Rationale
 *
 * Memory management is important, specially in an embedded
 * environment. Besides not all platforms correctly supporting ANSI C,
 * having our own memory-allocation functions allow for easier
 * debugging of applications.
 *
 * \section Introduction
 *
 * This modules includes portable memory-management functions. All
 * modules of \c larlib use this functions. They may allow improved
 * debugging / tracing support than any given platform's built-in
 * implementation.
 */

#include <larlib/base.h>

/**
 * Allocate a block of memory from the heap.
 *
 * The memory allocated by this function should only be accessed by
 * the calling application. The pointer is not guaranteed to be valid
 * for other applications.
 *
 * @param nb Number of bytes to allocate.
 *
 * @return \c NULL if memory could not be allocated.
 * @return A pointer to the newly allocated, uninitialized, block.
 */
void *memAlloc(int nb);

/**
 * Allocate and zero a block of memory from the heap.
 *
 * This is equivalent to calling \ref memAlloc() followed by \c
 * memset().
 *
 * @param nb Number of bytes to allocate.
 *
 * @return \c NULL if memory could not be allocated.
 * @return A pointer to the newly allocated, zero-initialized, block.
 */
void *memAllocZero(int nb);

/**
 * Resize a block of memory, or allocate a new one.
 *
 * The data pointed by \p oldp is shallow-copied (using \c memcpy())
 * to the new pointer on success. This is equivalent to the ANSI C
 * function \c realloc().
 *
 * @param oldp Address of the memory block to resize.
 * @param nb Number of bytes to resize \p oldp to.
 *
 * @return \c NULL if \p oldp could not be resized and new memory
 *   could not be allocated.
 * @return A pointer to the newly allocated memory, its prefix is the
 *   same as \p oldp.
 */
void *memRealloc(void *oldp, int nb);

/**
 * Release a previously allocated block.
 *
 * @param p A pointer returned by \ref memAlloc()
 */
void memFree(void *p);

/* @} */

#endif
