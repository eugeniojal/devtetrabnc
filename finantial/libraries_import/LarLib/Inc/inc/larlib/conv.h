#ifndef LARLIB_CONV_H
#define LARLIB_CONV_H

/**
 * @file conv.h
 * @brief Conversion routines.
 *
 * @addtogroup larlib_conv Conversion routines
 * @{
 *
 * \section Rationale
 *
 * There are many existing conversion libraries, but all of them have
 * their own definition of which format is what, how to name them, and
 * API look-and-feel. This module is a unification of all existing
 * libraries with clearer names and error handling.
 *
 * \section Introduction
 *
 * This package implements routines to convert between a few widely
 * used formats for generic data (specially numerical data):
 *
 * - <i>Text (Txt)</i>: a string representation of a number. It only
 *   makes sense with a base. For example, the string "1001" is either
 *   4097 in hexadecimal, 9 in binary or 1001 in decimal. Only bases
 *   in the range [2, 36] are supported.
 * - <i>Integer (Int)</i>: internal numerical representation of
 *   integers for the current machine. The actual representation as
 *   bits in memory is not defined (or, more precisely, is platform
 *   dependent). In general only non-negative integers are accepted.
 * - <i>Binary-Coded Decimals (Bcd)</i>: each digit of a decimal
 *   number is represented by a 4-bit value, for example the decimal
 *   number 1234 is encoded as the number 0x1234. If the number of
 *   digits is odd, the number may either start with 0x0 (as in 0x012345)
 *   or use 0xF as a terminator (0x12345F).
 * - <i>Buffer (Buf)</i>: a binary representation of numbers in binary
 *   format. The decimal number 3405692655 is <tt>"CAFEBEEF"</tt> in
 *   base 16, and the buffer <tt>{ 0xCA, 0xFE, 0xBE, 0xEF }</tt>.
 * - <i>Hexadecimal (Hex)</i>: a sub-class of Text (using base 16). It is
 *   differentiated since some functions only provide conversion to or
 *   from Hex.
 * - <i>Base-64 (Base64)</i>: the common Base-64 encoding of binary
 *   data.
 *
 * All routines have names with the format <tt>convXxxToYyy()</tt>
 * where <tt>Xxx</tt> and <tt>Yyy</tt> are either \c Bcd, \c Txt,
 * \c Int, \c Buf or \c Hex, and <tt>Xxx</tt> defines the \e source
 * format and <tt>Yyy</tt> the \e destination format.
 *
 * @note For enhanced domain, all functions support Integer values
 *   represented by an \c uint64_t. This may cause a slight performance
 *   penalty in lieu of a much larger Integer range. This also means
 *   that most functions are limited to numbers up to 2<sup>64</sup>-1.
 *
 * Not all possible conversion routines are implemented, some
 * conversions are used much more often than others, and this module
 * tries to select the most used ones. Also it should provide a "path"
 * from one format to another, so it may be possible to define a
 * function that convert from any given format to another by calling
 * pre-existing functions (i.e. the conversion graph should be fully
 * connected).
 *
 * Except otherwise noted, all functions are O(n) on the number
 * of digits of the input number in whatever base and format it is
 * to be processed.
 */

#include <larlib/base.h>

/**
 * Error codes specific to this module.
 */
enum convErrors_t {
    CONV_ERR_INVALID_BASE = -101  /**< Invalid base for Text parameter */
};

/**
 * Convert Text to Integer.
 *
 * Up to to \p cnt digits of \p txt are converted to integer, using \p
 * base. The result is stored in \p n.
 *
 * @param txt String to convert.
 * @param cnt Up to \p cnt digits will be read from \p txt. Conversion
 *   also stops at any digit out of range. Use <tt>cnt == -1</tt> to
 *   use \c strlen(txt) instead.
 * @param base Base of the number represented in \p txt.
 * @param[out] n Where the converted value should be stored.
 *
 * @return The number of digits converted from \p txt.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt, \p cnt, or \p n are invalid.
 * @return BASE_ERR_OVERFLOW if the resulting integer does not fit on a \ref uint64_t
 */
int convTxtToInt(const char *txt, int cnt, int base, uint64_t *n);

/**
 * Convert Text to an \ref uint8_t.
 *
 * Behaves exactly as \ref convTxtToInt(), but stores the result on
 * an 8-bit unsigned integer instead.
 *
 * @param txt String to convert.
 * @param cnt Up to \p cnt digits will be read from \p txt. Conversion
 *   also stops at any digit out of range. Use <tt>cnt == -1</tt> to
 *   use \c strlen(txt) instead.
 * @param base Base of the number represented in \p txt.
 * @param[out] n Where the converted value should be stored.
 *
 * @return The number of digits converted from \p txt.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt, \p cnt, or \p n are invalid.
 * @return BASE_ERR_OVERFLOW if the resulting integer does not fit on a \ref uint8_t
 */
int convTxtToInt8(const char *txt, int cnt, int base, uint8_t *n);

/**
 * Convert Text to an \ref uint16_t.
 *
 * Behaves exactly as \ref convTxtToInt(), but stores the result on
 * a 16-bit unsigned integer instead.
 *
 * @param txt String to convert.
 * @param cnt Up to \p cnt digits will be read from \p txt. Conversion
 *   also stops at any digit out of range. Use <tt>cnt == -1</tt> to
 *   use \c strlen(txt) instead.
 * @param base Base of the number represented in \p txt.
 * @param[out] n Where the converted value should be stored.
 *
 * @return The number of digits converted from \p txt.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt, \p cnt, or \p n are invalid.
 * @return BASE_ERR_OVERFLOW if the resulting integer does not fit on a \ref uint16_t
 */
int convTxtToInt16(const char *txt, int cnt, int base, uint16_t *n);

/**
 * Convert Text to an \ref uint32_t.
 *
 * Behaves exactly as \ref convTxtToInt(), but stores the result on
 * a 32-bit unsigned integer instead.
 *
 * @param txt String to convert.
 * @param cnt Up to \p cnt digits will be read from \p txt. Conversion
 *   also stops at any digit out of range. Use <tt>cnt == -1</tt> to
 *   use \c strlen(txt) instead.
 * @param base Base of the number represented in \p txt.
 * @param[out] n Where the converted value should be stored.
 *
 * @return The number of digits converted from \p txt.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt, \p cnt, or \p n are invalid.
 * @return BASE_ERR_OVERFLOW if the resulting integer does not fit on a \ref uint32_t
 */
int convTxtToInt32(const char *txt, int cnt, int base, uint32_t *n);

/**
 * This function is a synonym to \ref convTxtToInt().
 *
 * It is added only for completeness, you may use it to make clear the intent
 * of using a \ref uint64_t as the conversion target.
 *
 * @param txt String to convert.
 * @param cnt Up to \p cnt digits will be read from \p txt. Conversion
 *   also stops at any digit out of range. Use <tt>cnt == -1</tt> to
 *   use \c strlen(txt) instead.
 * @param base Base of the number represented in \p txt.
 * @param[out] n Where the converted value should be stored.
 *
 * @return The number of digits converted from \p txt.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt, \p cnt, or \p n are invalid.
 * @return BASE_ERR_OVERFLOW if the resulting integer does not fit on a \ref uint64_t
 */
int convTxtToInt64(const char *txt, int cnt, int base, uint64_t *n);

/**
 * Convert Integer to Text.
 *
 * If \p maxcnt is not enough to store all converted digits plus the
 * terminating zero, conversion will \b not be done, but the number of
 * digits required for conversion will be returned. Calling this
 * function with <tt>maxcnt == 0</tt> and <tt>txt == NULL</tt> is a
 * valid option to calculate the number of digits necessary to
 * represent a number on a certain base.
 *
 * @param n Number to be converted.
 * @param[out] txt Where the converted digits will be stored.
 * @param maxcnt Maximum number of digits that may be written to txt
 *   (\e including the terminating zero!)
 * @param base Base to use for \p txt.
 *
 * @return The number of digits that would be necessary to encode \p n
 *   totally (including the terminating zero). If the return value is
 *   <tt>>= maxcnt</tt> the conversion did not happen and \p txt was
 *   never written to.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt is invalid.
 */
int convIntToTxt(uint64_t n, char *txt, int maxcnt, int base);

/**
 * Convert Integer to Text padding left with zeroes to fill \p cnt chars.
 *
 * For example, converting the number 13 with <tt>cnt = 4</tt> and
 * <tt>base = 10</tt> results in the string <tt>txt = "0013"</tt>. Note that
 * zero is used whichever \p base is used.
 *
 * @note The output string \p txt is \b not zero-terminated!
 *
 * @param n Number to be converted.
 * @param[out] txt Where the converted digits will be stored.
 * @param cnt Number of digits that should be written to \p txt.
 * @param base Base to use for \p txt.
 *
 * @return The number of significant digits of \p n in \p base (that is,
 *   discounting the zeroes to the left inserted for padding). If the return
 *   is larger than \p cnt then \p txt is not large enough and no conversion
 *   was done.
 * @return CONV_ERR_INVALID_BASE if \p base is out of range.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt is invalid.
 */
int convIntToTxtPad(uint64_t n, char *txt, int cnt, int base);

/**
 * Convert buffer to hexadecimal string.
 *
 * @param buf Buffer to convert.
 * @param nbuf Number of bytes to convert from buffer.
 * @param[out] hex Where to store the converted data.
 * @param maxcnt Max number of digits to write to \p hex (\e including the terminating zero!)
 *
 * @note \p maxcnt should be at least twice \p nbuf as each bytes
 *   converts to two hexadecimal digits.
 *
 * @return The number of digits written to \p hex.
 * @return BASE_ERR_INVALID_PARAMETER if \p buf or \p hex are invalid.
 */
int convBufToHex(const uint8_t *buf, int nbuf, char *hex, int maxcnt);

/**
 * Convert hexadecimal string to buffer.
 *
 * @param hex Hexadecimal string to convert.
 * @param cnt Max number of digits to convert from \p hex. Use <tt>cnt
 *   == -1</tt> to use \c strlen(hex) instead.
 * @param[out] buf Output buffer with converted bytes.
 * @param maxbuf Max number of bytes to write to \p buf.
 *
 * @note \p maxbuf should be at least half <tt>min(strlen(hex),
 *   cnt)</tt>, as each pair of hexadecimal digits convert to one
 *   byte.
 *
 * @note If the number of hexadecimal digits is odd, the last byte
 *   will be padded with 0 to the right. For example <tt>"ABCDE"</tt>
 *   will be encoded as <tt>{ 0xAB, 0xCD, 0xE0 }</tt>.
 *
 * If a non-hexadecimal digit is found in \p hex, conversion will stop
 * and the number of \e complete bytes written to \p buf is returned.
 * If the number of converted digits is odd, the last (partial) byte is
 * not counted in the return, but will be partially filed as if the
 * number of input digits were odd.  (E.g. <tt>"ABCDEX"</tt> returns 2
 * and writes <tt>{0xAB, 0xCD, 0xE0}</tt> to \p buf).
 *
 * @return The number of bytes written to \p buf.
 * @return BASE_ERR_INVALID_PARAMETER if \p hex or \p buf are invalid.
 */
int convHexToBuf(const char *hex, int cnt, uint8_t *buf, int maxbuf);

/**
 * Convert Integer to BCD.
 *
 * @param n The integer to convert.
 * @param[out] bcd Where to store the BCD-coded \p n. Note that \p bcd
 *   will \e not be zero-terminated!
 * @param maxcnt Max number of bytes to write to \p bcd.
 * @param padRight If non-zero, the resulting BCD is padded left with a
 *   terminating \c 0xF. If zero odd-sized BCD numbers start with a
 *   \c 0x0 as padding. Note that this flag only applies to BCD numbers
 *   where the resulting number of digits is odd (thus requiring an
 *   "extra" nibble).
 *
 * @return The number of bytes necessary to fully convert \p n. If
 *   <tt>> maxcnt</tt>, then the conversion was not done, and \p bcd
 *   was not modified.
 * @return BASE_ERR_INVALID_PARAMETER if \p bcd is invalid.
 */
int convIntToBcd(uint64_t n, uint8_t *bcd, int maxcnt, int padRight);

/**
 * Convert Integer to BCD with left padding.
 *
 * This function is similar to \ref convIntToBcd(), but instead of using
 * the least necessary number of bytes to express the BCD encoded number,
 * it fills all the nibbles requested in the \p nibbles parameter.
 *
 * If the value of \p nibbles is odd, the last half of the last byte
 * would be unused by the encoding function, and is filled with \c 0xF
 * instead.
 *
 * If the value of \p nibbles is even, all the bytes are wholly used,
 * and there is no need to fill with \c 0xF the last byte.
 *
 * In both cases, the \p bcd output buffer is prefixed with enough
 * \c 0x00 bytes so that the encoding of \p n uses <tt>(nibbles + 1) / 2</tt>
 * bytes.
 *
 * @param n The integer to convert.
 * @param[out] bcd Where to store the BCD-coded \p n.  Note that \p bcd
 *   will \e not be zero terminated.  If is assumed that \p bcd has enough
 *   space for <tt>(nibble + 1) / 2</tt> bytes.
 * @param nibbles The number of \e nibbles to pad the converted BCD number
 *   to.  See the notes above for more information.
 *
 * @return If \p nibbles is too small, return the number of \e bytes that
 *   would be required to fully encode \p n
 * @return On success return the number of \e bytes actually written to \p bcd
 *   (that should always be equal to <tt>(nibbles + 1) / 2</tt>
 * @return BASE_ERR_INVALID_PARAMETER if \p bcd or \p nibbles is invalid.
 */
int convIntToBcdPad(uint64_t n, uint8_t *bcd, int nibbles);

/**
 * Convert BCD to Integer.
 *
 * @param bcd The BCD number to convert.
 * @param cnt The max number of bytes to convert. Conversion also
 *   stops when a non-decimal nibble is found (usually \c 0xF).
 * @param[out] n Where to store the converted number.
 *
 * @return The number of <em>decimal digits</em> of the number stored
 *   in \p n.
 * @return BASE_ERR_INVALID_PARAMETER if \p bcd or \p n are invalid.
 */
int convBcdToInt(const uint8_t *bcd, int cnt, uint64_t *n);

/**
 * Convert a string of decimal digits (or ISO PAN) to BCD.
 *
 * This function was designed to be primarily used with PAN number conversion
 * for ISO messages. If the string is odd-sized, it is padded left with a
 * terminating \c 0xF.
 *
 * If the sequence includes the char '=', it is changed to \c 0xD in the
 * BCD output. Conversion ends at any char that is not a decimal digit or '='.
 *
 * @param txt The source string. May be zero-terminated or not, see \p maxcnt.
 * @param maxtxt Max numbers of chars to convert from \p txt. If (-1) use \c strlen(txt).
 * @param[out] bcd Target buffer for the BCD-coded value.
 * @param maxbcd Max number of \e bytes that can be written to \p bcd.
 *
 * @return The number of \e bytes necessary to fully convert \p txt. If the
 *   return value is larger than \p maxbcd, then conversion was not done.
 * @return BASE_ERR_INVALID_PARAMETER if \p txt is \c NULL or if \p bcd is \c NULL and
 *   <tt>maxbcd \> 0</tt>
 */
int convTxtToBcd(const char *txt, int maxtxt, uint8_t *bcd, int maxbcd);

/**
 * Convert a sequence of digits in BCD format to a decimal string.
 *
 * This function was designed to be primarily used with PAN number conversion
 * for ISO messages. It assumes that, if the sequence has an odd number of
 * digits, it will be terminated with \c 0xF.
 *
 * If \p bcd includes the code \c 0xD, it will be changed to the char '='
 * on \p txt. Any other digit code in \p bcd terminates execution.
 *
 * @note The output string \p txt is \e NOT zero-terminated!
 *
 * @param bcd Data in BCD format.
 * @param maxbcd Max number of bytes to decode from \p bcd.
 * @param[out] txt Output string, will \e not be zero-terminated!
 * @param maxtxt Max number of chars to write to \p txt
 *
 * @return The number of valid digits in \p bcd. If this is larger than
 *   \p maxtxt then no conversion was done.
 * @return BASE_ERR_INVALID_PARAMETER if \p bcd is \c NULL or \p txt is \c NULL
 *   and <tt>maxtxt \> 0</tt>.
 */
int convBcdToTxt(const uint8_t *bcd, int maxbcd, char *txt, int maxtxt);

/**
 * Convert an integer to a sequence of bytes in *big-endian* ordering.
 *
 * Example:
 * @code
 * uint8_t buf[sizeof(uint64_t)];
 * int i = convIntToBuf(0x12345678, buf, sizeof(buf));
 * // i == 4 && buf == { 0x12, 0x34, 0x56, 0x78 }
 * @endcode
 *
 * @param n The integer to convert
 * @param[out] buf Where to store the bytes of \p n
 * @param maxbuf Max number of *bytes* that may be written to \p buf
 *
 * @return The number of bytes necessary to fully encode \p n. If <tt>\> maxbuf</tt>
 *      then conversion was not performed and nothing is written to \p buf
 * @return BASE_ERR_INVALID_PARAMETER if \p buf is NULL or <tt>maxbuf \< 0</tt>
 */
int convIntToBuf(uint64_t n, uint8_t *buf, int maxbuf);

/**
 * Convert an integer to a sequence of bytes in *big-endian* ordering and with padding.
 *
 * This call is equivalent to \ref convIntToBuf() but the output buffer \p buf will be
 * zero-padded to the left so that always \p maxbuf bytes are filled.
 *
 * Example:
 * @code
 * uint8_t buf[sizeof(uint64_t)];
 * int i = convIntToBuf(0x12345678, buf, sizeof(buf));
 * // i == 8 && buf == { 0x00, 0x00, 0x00, 0x00, 0x12, 0x34, 0x56, 0x78 }
 * @endcode
 *
 * @param n The integer to convert
 * @param[out] buf Where to store the bytes of \p n
 * @param maxbuf Number of *bytes* that will be written to \p buf
 *
 * @return The number of bytes written to \p buf
 * @return In case of overflow, return how many bytes would be necessary to fully encode \p n
 * @return BASE_ERR_INVALID_PARAMETER if \p buf is NULL or <tt>maxbuf \< 0</tt>
 */
int convIntToBufPad(uint64_t n, uint8_t *buf, int maxbuf);

/**
 * Convert a sequence of bytes in *big-endian* ordering to an integer.
 * The opposite of \ref convIntToBuf().
 *
 * @param buf Sequence of bytes to be converted
 * @param nbuf How many bytes of \p buf to convert
 *
 * @return The converted value of `buf[0 .. nbuf - 1]`
 * @return Zero in any error
 */
uint64_t convBufToInt(const uint8_t *buf, int nbuf);

/**
 * Decode a base-64 encoded string into a sequence of bytes.
 * Use the Base-64 index table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".
 *
 * Example:
 * @code
 * unsigned char buf[24];
 * int n = convBase64ToBuf("SW5nZW5pY28gZG8gQnJhc2lsIQ==", -1, buf, sizeof(buf));
 * // n == 19 && buf == "Ingenico do Brasil!"
 * @endcode
 *
 * @param b64 Input base-64 text, a sequence of chars from the Base-64 index table above
 * @param cnt Number of chars to decode, or (-1) to use `strlen()`
 * @param[out] buf Where to store the decoded bytes
 * @param maxbuf Max number of bytes to write to `maxbuf`
 *
 * @return The number of bytes that `b64` decode to.  If the returned value is larger than
 *      `maxbuf` then an overflow was detected, and `buf` was not written to.
 * @return BASE_ERR_INVALID_PARAMETER if `b64` or `buf` are `NULL`
 *
 * @see convBufToBase64
 * @see https://en.wikipedia.org/wiki/Base64
 */
int convBase64ToBuf(const char *b64, int cnt, uint8_t *buf, int maxbuf);

/**
 * Encode a sequence of bytes into a Base-64 string.
 * Use the Base-64 index table "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".
 *
 * @param buf Input sequence of bytes
 * @param nbuf Number of bytes to `buf` to encode
 * @param[out] b64 Where to store the encoded Base-64 chars
 * @param maxb64 Max number of chars to write to `b64`, including terminating zero!
 *
 * @return The number of bytes necessary to fully encode `buf`.  If the returned value is larger than
 *      `maxb64` then an overflow was detected, and `b64` was not written to.
 * @return BASE_ERR_INVALID_PARAMETER if `buf` or `b64` are `NULL`
 *
 * @see convBase64ToBuf
 * @see https://en.wikipedia.org/wiki/Base64
 */
int convBufToBase64(const uint8_t *buf, int nbuf, char *b64, int maxb64);

/* @} */

#endif

