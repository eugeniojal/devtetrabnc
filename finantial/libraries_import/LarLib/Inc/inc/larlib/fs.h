#ifndef LARLIB_FS_H
#define LARLIB_FS_H

/**
 * @file fs.h
 * @brief File System abstraction
 *
 * @addtogroup larlib_fs File System
 * @{
 *
 * \section Rationale
 *
 * File system API differs heavily between platforms. But more than
 * 90% of all uses require the same simple basic functions. By
 * defining them in a portable manner we can handle most porting
 * issues without needing complicated implementations on simpler
 * platforms.
 *
 * \section fs_intro Introduction
 *
 * This module defines a basic abstraction over the common operations
 * of all file systems. Functionality is added only if easily
 * implementable on \e all supported file systems, so it may not be
 * enough/adequate for certain operations, specially in case of access
 * to some platform-dependent meta-data.
 *
 * The sort of operations covered here are:
 * - Opening files;
 * - Basic I/O (block/string oriented I/O);
 *
 * Note that some directory-related operations are not included
 * (change current directory, create/remove directory). In place of
 * this, it is used the concept of <i>file name prefix</i> (see \ref
 * fsSetFileNamePrefix()). Each operation on files names will prepend
 * this prefix to the file name before operating. This allows for
 * quick adaptation between architectures that have different concepts
 * for directory/volume/working-unit.
 *
 * The file name prefixes are also stored as a stack, allowing entries
 * to be "pushed" and "poped" (see \ref fsPushFileNamePrefix() and
 * \ref fsPopFileNamePrefix()), this way a code that temporarily needs
 * to set the file prefix do not need to store the current one on
 * a temporary variable to restore later.
 *
 * \subsection fs_telium Telium
 *
 * On \e Telium architecture, use the file name prefix to define which volume to use, but note that:
 * - The prefix string should include the full volume name,
 *   terminating with '/': <tt>"/VOLUME/"</tt>;
 * - The volume is \e not automatically created, this must be done
 *   outside this library
 *
 * \subsection fs_unicapt Unicapt
 *
 * Global files are currently not supported on Unicapt32.
 *
 * \todo Directory iteration
 */

#include <larlib/base.h>

/**
 * Opaque type of a file.
 */
typedef struct fsFile_t fsFile_t;

/**
 * Flags for \ref fsOpen().
 *
 * The \c FS_OPEN_READ and \c FS_OPEN_WRITE options are not strict,
 * i.e., a file open for reading may have write allowed and a file
 * open for writing may have read allowed. The strictest permission
 * possible for a given architecture will be used.
 */
enum fsOpenFlags_t {
    FS_OPEN_READ = 1,       /**< Open file for reading */
    FS_OPEN_WRITE = 2,      /**< Open file for writing */
    FS_OPEN_CREATE = 4,     /**< Create file if not found */
    FS_OPEN_RESET = 8,      /**< Reset file if present */
    FS_OPEN_APPEND = 16     /**< Move position to FS_WHENCE_END after open, forces FS_OPEN_WRITE */
};

/**
 * Posible seek positions, see \ref fsSeek().
 */
enum fsSeekWhence_t {
    FS_WHENCE_SET,          /**< Seek from the start of file */
    FS_WHENCE_END,          /**< Seek from the end of file */
    FS_WHENCE_CUR           /**< Seek from the current position */
};

/**
 * Set the common file name prefix.
 * Each operation on files by name prepend this prefix (without any
 * processing!) before calling the OS.
 *
 * @param prefix File name prefix to use.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if \p prefix is not valid on current architecture.
 * @return BASE_ERR_OVERFLOW if \p prefix is too large.
 *
 * @see fsGetFileNamePrefix fsOpen
 */
int fsSetFileNamePrefix(const char *prefix);

/**
 * Push a new item on the file name prefix stack.
 *
 * This call will change the value that \ref fsGetFileNamePrefix() returns
 * but keep the previous values untouched.  A call to \ref fsPopFileNamePrefix()
 * may be used to restore previous values.
 *
 * @param prefix File name prefix to use.
 *
 * @return BASE_ERR_OK on success
 * @return BASE_ERR_INVALID_PARAMETER if \p prefix is not valid on current architecture.
 * @return BASE_ERR_OVERFLOW if \p prefix is too large, or if file name prefix stack is too long
 */
int fsPushFileNamePrefix(const char *prefix);

/**
 * Remove the top item on the file name prefix stack.
 *
 * If the file name prefix stack has at least two elements, this call will
 * remove the "top" element of the stack.  If the list has a single element,
 * this call has no effect (and the current value is kept).
 *
 * @return BASE_ERR_OK
 */
int fsPopFileNamePrefix(void);

/**
 * Return the current prefix being used.
 *
 * @return Pointer to the prefix in use. The returned string \e must
 *   not be changed, use \ref fsSetFileNamePrefix() to do this
 *   instead.
 *
 * @see fsSetFileNamePrefix
 */
const char *fsGetFileNamePrefix(void);

/**
 * Open the selected file to input and/or output.
 *
 * Note the double indirection on the \p file parameter, to use this
 * function:
 * @code
 * fsFile_t *file;
 * int error = fsOpen(name, FS_OPEN_READ | FS_OPEN_WRITE | FS_OPEN_CREATE, &file);
 * // use file...
 * fsClose(file);
 * @endcode
 *
 * @param name File name. The actual name sent to the OS has the value
 *   defined by \ref fsSetFileNamePrefix() prepended.
 * @param flags File opening flags OR'ed together, see \ref
 *   fsOpenFlags_t. At least one of \ref FS_OPEN_READ or \ref
 *   FS_OPEN_WRITE must be set.
 * @param[out] file On successful return, a handle to the open file.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_DATA_NOT_FOUND if file was not found.
 * @return BASE_ERR_RESOURCE_PB if could not allocate a new handle.
 * @return BASE_ERR_ACCESS do not have permission to access file.
 * @return BASE_ERR_OVERFLOW if \p name (or the concatenation of \p
 *   name and prefix) is too large.
 * @return BASE_ERR_INVALID_PARAMETER if \p name, \p flags or \p file
 *   are invalid.
 *
 * @see fsClose
 */
int fsOpen(const char *name, int flags, fsFile_t **file);

/**
 * Close a file handle.
 *
 * @param f Handle to open file.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid or \c NULL.
 *
 * @see fsOpen
 */
int fsClose(fsFile_t *f);

/**
 * Try to read a number of bytes from a file.
 *
 * Reads up to \p n bytes from the file, depending on
 * availability. Never reads more than \p n bytes.
 *
 * @param f Handle to open file.
 * @param n Number of bytes to read.
 * @param[out] p Where to store the bytes read. Must have place for upto \p n bytes.
 *
 * @return Number of bytes actually read (zero if no more bytes to
 *   read -- end of file).
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if do not have permission to read from \p f.
 * @return BASE_ERR_INVALID_PARAMETER if \p p is \c NULL.
 *
 * @see fsReadByte fsReadUntil
 */
int fsRead(fsFile_t *f, int n, void *p);

/**
 * Try to read the next byte from the file.
 *
 * @param f Handle to open file.
 *
 * @return Non-negative value of byte read, a return less than zero
 *   signals error.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if do not have permission to read from \p f.
 * @return BASE_ERR_DATA_NOT_FOUND if reached end of file.
 *
 * @see fsRead fsReadUntil
 */
int fsReadByte(fsFile_t *f);

/**
 * Read bytes from the input file until either one of \p delims bytes
 * is found, or \p n bytes have been read. The delimiter character is
 * included on the output buffer.
 *
 * Note that \p p is not zero-terminated, the caller must use the
 * return from this function to determine the number of bytes written
 * to \p p and place the terminating zero if necessary.
 *
 * Example:
 * @code
 * char buf[N];
 * int nread;
 * nread = fsReadUntil(f, "\n", -1, N - 1, buf);
 * if (nread >= 0) buf[nread] = '\0'; // force zero-termination
 * @endcode
 *
 * @param f Handle to open file.
 * @param delims Bytes to use as delimiters.
 * @param ndelim Number of bytes in \p delims. May be negative to use
 *   <tt>strlen(delims)</tt>.
 * @param n Read upto \p n bytes from input.
 * @param[out] p Where to store the data read, must have space for
 *   upto \p n bytes.
 *
 * @return Non-negative number of bytes read (zero if end of file).
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if do not have permission to read from \p f.
 * @return BASE_ERR_INVALID_PARAMETER if \p delims or \p p are \c NULL.
 *
 * @see fsRead fsReadByte
 */
int fsReadUntil(fsFile_t *f, const uint8_t *delims, int ndelim, int n, void *p);

/**
 * Try to write a number of bytes to a file.
 *
 * Writes upto to \p n bytes to the file. Depending on available space
 * may not be able to write all the requested bytes. Writing on the
 * end of the file to append is allowed.
 *
 * @param f Handle to open file.
 * @param n Number of bytes to write.
 * @param p Buffer with data to write.
 *
 * @return Number of bytes written on success.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if do not have permission to write to \p f.
 *
 * @see fsWriteByte
 */
int fsWrite(fsFile_t *f, int n, const void *p);

/**
 * Try to write a byte to a file.
 *
 * @param f Handle to open file.
 * @param b Byte value to write. Only the least significant 8-bits are
 *   used, truncating \p b to the range [0, 255].
 *
 * @return BASE_ERR_OK if byte was written.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if do not have permission to write to \p f.
 * @return BASE_ERR_RESOURCE_PB if could not write byte.
 *
 * @see fsWrite
 */
int fsWriteByte(fsFile_t *f, int b);

/**
 * Return the current read/write position of a file.
 *
 * @param f Handle to open file.
 *
 * @return Non-negative file position on success.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if this operation is not allowed on this
 *   type of file.
 *
 * @see fsSeek
 */
int fsTell(fsFile_t *f);

/**
 * Set the read/write position for a file.
 *
 * Trying to move before the start of the file or after one past
 * the end of the file has undefined behavior, it may fail or
 * move the position to one of the edges of the file, depending
 * on the platform. Do \e not depend on either behavior.
 *
 * @param f Handle to open file.
 * @param whence Where to seek from, see \ref fsSeekWhence_t.
 * @param offset Distance to seek, positive to seek forward, negative
 *   to seek backward.
 *
 * @return Non-negative new position inside the file.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if this operation is not allowed on this
 *   type of file.
 * @return BASE_ERR_INVALID_PARAMETER if \p whence is not \ref
 *   FS_WHENCE_SET, \ref FS_WHENCE_END or \ref FS_WHENCE_CUR.
 *
 * @see fsTell
 */
int fsSeek(fsFile_t *f, int whence, int offset);

/**
 * Return the length of an open file in bytes. The current file pointer is not changed.
 *
 * This is equivalent to:
 * @code
 * int fileSize(fsFile_t *f) {
 *   // FIXME: no error handling!
 *   int orig = fsTell(f);
 *   int siz = fsSeek(f, FS_WHENCE_END, 0);
 *   fsSeek(f, FS_WHENCE_SET, orig);
 *   return siz;
 * }
 * @endcode
 *
 * @param f Handle to open file.
 *
 * @return Non-negative size of file \p f.
 * @return BASE_ERR_INVALID_HANDLE if \p f is invalid.
 * @return BASE_ERR_ACCESS if this operation is not allowed on this
 *   type of file.
 */
int fsLength(fsFile_t *f);

/**
 * Delete file \p name from the file system.
 *
 * The behavior is undefined if the file is currently open.
 *
 * @param name Name of file to delete.
 *
 * @return BASE_ERR_OK if file was deleted.
 * @return BASE_ERR_DATA_NOT_FOUND if \p name does not exist.
 * @return BASE_ERR_ACCESS if could not access file (in use?)
 * @return BASE_ERR_RESOURCE_PB on file system access error
 */
int fsRemove(const char *name);

/**
 * Rename the file \p oname to \p newname.
 *
 * Both \p oldname and \p newname should not be open. The behavior is
 * undefined if \p oldname and \p newname refer to diferent
 * directories or volumes (most implementations will fail).
 *
 * @param oldname Original (current) name of file.
 * @param newname New name of file.
 *
 * @return BASE_ERR_OK if file was deleted.
 * @return BASE_ERR_DATA_NOT_FOUND if \p oldname does not exist.
 * @return BASE_ERR_ACCESS if could not access file (in use?)
 * @return BASE_ERR_RESOURCE_PB on file system access error
 */
int fsRename(const char *oldname, const char *newname);

/**
 * Check if file \p name exists.
 *
 * @param name Name of file to check.
 *
 * @return Non-zero if file exists
 * @return Zero otherwise
 *
 * @attention The fact that the file exists does \e not imply that
 *  it can be opened by the caller application!
 */
int fsExist(const char *name);

/* @} */

#endif
