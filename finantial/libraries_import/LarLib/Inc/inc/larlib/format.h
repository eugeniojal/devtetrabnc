#ifndef LARLIB_FORMAT_H
#define LARLIB_FORMAT_H

/**
 * @file format.h
 * @brief Buffer formatting
 *
 * @addtogroup larlib_format Buffer formatting
 * @{
 *
 * \section Rationale
 *
 * One of the basic operations necessary to perform during communication and
 * storage is generating a formatted buffer coalesced from many data items,
 * perhaps with necessary conversions, for example ISO8583 message packing.
 * This module is an extension of existing ISO8583 message-packing libraries,
 * with additions so it can be used for other packing formats, and can be
 * highly customized by the user.
 *
 * \section Introduction
 *
 * \e Formatting (or \e packing) a buffer is the generation of a buffer from
 * a collection of (many) data elements possibly spread in many data structures,
 * with added conversion and formatting. The inverse process is called
 * \e unpacking.
 *
 * The design of this module is such that the user needs to create and maintain
 * a single table with the list of fields, and how to format each, and this
 * same structure can be used both for packing and unpacking. Some pre-defined
 * functions for this are included with the library (see \ref larlib_format_fn),
 * but the user is free to create new ones.
 *
 * See the documentation of \ref formatField_t and \ref format_t for information
 * on how to fill those structures. See below for more information about how
 * each field is used during packing and unpacking.
 *
 * \section Packing
 *
 * On a call to \ref formatPack() the following algorithm is followed for each
 * field:
 * -# If \c filter != \c NULL and \c filter(fmt, field) == 0, ignore this field
 * -# Set \c bodySize = \c 0;
 * -# If \c body != \c NULL, call it to write the field data;
 *    - Set \c bodySize to the return value of \c body;
 * -# If \c prefix != \c NULL, call it;
 *    - Note that \c prefix can access \c bodySize to know the number of bytes
 *      actually written by \c body, and \c size for the user-provided size information;
 * -# If \c suffix != \c NULL, call it;
 *    - Note that \c suffix can access \c bodySize to know the number of bytes
 *      actually written by \c body, and \c size for the user-provided size information;
 * -# Swap \c body and \c prefix so \c prefix is before \c body on the output
 *    buffer.
 *
 * - The formatting of the size and padding is independent of the formatting
 *   of the actual field data;
 * - \c bodySize rarely needs to be filled by the user, it is filled on runtime
 *   with the value returned by \c body;
 * - Fields that require padding to specific sizes can break this into a
 *   \c body that write the actual data and a \c prefix and/or \c suffix that
 *   deals with the padding;
 * - On the call to \c prefix or \c suffix the \c size field indicates the
 *   number given by the caller when filling the \ref formatField_t structure,
 *   while \c bodySize is the actual number of bytes written by \c body;
 * - Padding is usually done by a \c suffix that writes <tt>size - bodySize</tt>
 *   bytes;
 * - \c prefix can be used to write a size field with the actual number of bytes
 *   written, as stored in \c bodySize;
 * - A separator can be generated using the \c suffix function.
 *
 * \section Unpacking
 *
 * On a call to \ref formatUnpack() the following algorithm is followed for each
 * field:
 * -# If \c filter != \c NULL and \c filter(fmt, field) == 0, ignore this field
 * -# Set \c bodySize = \c size;
 * -# If \c prefix != \c NULL, call it;
 *    - If \c prefix deals with the actual field length, it \e must fill
 *      \c bodySize with the number of bytes that need to be read!
 * -# If \c body != \c NULL, call it to read the field data;
 *    - \c body should \e always read \c bodySize bytes, \e not \c size bytes;
 * -# If \c suffix != \c NULL, call it;
 *    - Note that \c suffix can access \c bodySize to know the actual size
 *      of \c body in bytes;
 *
 * - Remember that \c prefix should set the \c bodySize field, and that \c
 *   body must use \c bodySize instead of \c size to know how many bytes to
 *   read (if \c prefix is \c NULL then \c bodySize = \c size);
 * - If \c suffix is used for padding or separator, then it may only skip the
 *   necessary number of bytes;
 *
 * \section Examples
 *
 * Here we list a sample of common field types and a simple ISO8583-like field list
 * with a few bits.
 *
 * \subsection fix Fixed-size Fields
 *
 * Fixed-size fields are fields where the number of bytes written by \c body is
 * always equal to \c size as given by the user on the field table. This
 * usually means that those fields include no \c prefix or \c suffix, but they may
 * require a \c sufix function to write a fixed separator.
 *
 * Example: a fixed-size field with 16 bytes.
 * \code
 * field.index = fieldIndex;
 * field.prefix = NULL;
 * field.body = formatBodyMemcpy;   // copy exactly field.size bytes
 * field.suffix = NULL;
 * field.size = 16;                 // size must be provided
 * field.data = dataPtr;
 * field.bodySize = 0;
 * \endcode
 *
 * \subsection var Variable-size Fields
 *
 * Variable-size fields require a size prefix to indicate the actual size of
 * the body. Usually they are string data (as card-holder name) or numerical
 * data without padding.
 *
 * Example: a variable-size field of a string.
 * \code
 * field.index = fieldIndex;
 * field.prefix = formatSizeLL;     // size as 2-digit decimal number
 * field.body = formatBodyStrcpy;   // copy until '\0'
 * field.suffix = NULL;
 * field.size = 0;                  // size is not relevant
 * field.data = stringPtr;
 * field.bodySize = 0;
 * \endcode
 *
 * \subsection pad Padded Fields
 *
 * Padded fields are a combination of fixed and variable-sized fields. They
 * have a size prefix but are padded to fit exactly \c size bytes.
 *
 * Example: a string field padded with spaces to 20 bytes.
 * \code
 * formatField_t field;
 * field.index = fieldIndex;
 * field.prefix = formatSizeLL;     // size as 2-digit decimal number
 * field.body = formatBodyStrcpy;   // copy until '\0'
 * field.suffix = formatPadSpaces;  // pad with spaces
 * field.size = 20;                 // desired total size of field
 * field.data = stringPtr;
 * field.bodySize = 0;
 * \endcode
 *
 * \subsection filter Filtering
 *
 * By filling the \c filter field of the \ref format_t structure, the caller
 * may allow conditional processing of fields. The most common use is during
 * ISO8583 processing, but the system is flexible enough for other uses.
 * During execution the \c filter function is called for each field, passing
 * a pointer to both the \ref format_t and the specific \ref formatField_t.
 *
 * A \c filter implementation may use the \c fieldMap pointer to store the
 * set of allowed fields. For example, for ISO8583, the \c fieldMap could be
 * a pointer to the actual ISO8583 bitmap as in the packed buffer.
 *
 * A common framework is to use a \c body writer function that both pack/
 * unpack the bitmap and set the \c fieldMap field, for example:
 * \code
 * int filterISO8583(const format_t *fmt, const formatField_t *field)
 * {
 *   return (fmt->fieldMap == NULL)
 *     || (field->index < 0)
 *     || bitsGet(fmt->fieldMap, field->index) == 1;
 * }
 * \endcode
 * Is a function that checks the bit \c field->index of \c fmt->fieldMap to
 * choose if a field should or not be part of the packing/unpacking process.
 *
 * In order for this to work, the user must also define, inside the field list,
 * where the bitmap is stored, and properly associate the value of the \c fieldMap
 * field. The following function does both:
 * \code
 * int formatBodyISO8583(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field)
 * {
 *   int ret;
 *   if (direction == FORMAT_PACK) {
 *     memcpy(buffer, field->data, field->size);
 *     ret = field->size;
 *   }
 *   else {
 *     memcpy(field->data, buffer, field->bodySize);
 *     ret = field->bodySize;
 *   }
 *   fmt->fieldMap = field->data;
 *   return ret;
 * }
 * \endcode
 * And can be used as:
 * \code
 * bitsBuffer_t iso_bitmap[NBITMAP];
 * formatField_t field = { -1, NULL, formatBodyISO8583, NULL, sizeof(iso_bitmap), iso_bitmap, 0 };
 * \endcode
 *
 * \subsection body A simple body function
 *
 * This is a simple function that could be used as the \c body of a field. It
 * copies variable-sized zero-terminated strings:
 * \code
 * int formatBodyStrcpy(uint8_t *buffer,
 *                      int limit,
 *                      formatDirection_t direction,
 *                      format_t *fmt,
 *                      formatField_t *field)
 * {
 *   if (direction == FORMAT_PACK) {
 *     int n = strlen(field->data);
 *     memcpy(buffer, field->data, n);
 *     return n;
 *   }
 *   else {
 *     memcpy(field->data, buffer, field->bodySize);
 *     ((char *)field->data)[field->bodySize] = '\0';
 *     return field->bodySize;
 *   }
 * }
 * \endcode
 * Note how it return \c strlen() on packing, and both uses and return
 * \c bodySize on unpack.
 *
 * \subsection prefix A simple prefix function
 *
 * Following the example above, this is a \c prefix function that write the
 * value of \c bodySize as a two-digit decimal number:
 * \code
 * int formatSizeAsc2(uint8_t *buffer,
 *                    int limit,
 *                    formatDirection_t direction,
 *                    format_t *fmt,
 *                    formatField_t *field)
 * {
 *   if (direction == FORMAT_PACK) {
 *     if (limit < 2)
 *       return BASE_ERR_OVERFLOW;
 *     if (field->bodySize < 0 || field->bodySize > 99)
 *       return BASE_ERR_INVALID_PARAMETER;
 *     convIntToTxtPad(field->bodySize, (char *) buffer, 2, 10);
 *     return 2;
 *   }
 *   else {
 *     uint64_t value;
 *     convTxtToInt((const char *) buffer, 2, 10, &value);
 *     field->bodySize = (int) value;
 *     return 2;
 *   }
 * }
 * \endcode
 * Note how it return the number of bytes read or written on both pack and
 * unpack, and how it uses \c bodySize on packing and stores to it on
 * unpacking. (This version uses the functions from \ref larlib_conv).
 *
 * \subsection suffix A simple suffix function
 *
 * Now a function that does padding with spaces: it writes enough spaces to
 * compensate by the different between \c size and \c bodySize, if any.
 * \code
 * int formatPadSpaces(uint8_t *buffer,
 *                     int limit,
 *                     formatDirection_t direction,
 *                     format_t *fmt,
 *                     formatField_t *field)
 * {
 *   if (direction == FORMAT_PACK) {
 *      memset(buffer, ' ', field->size - field->bodySize);
 *   }
 *   return field->size - field->bodySize;
 * }
 * \endcode
 * Note how, on unpacking, the function ignores the actual buffer contents and
 * just return how many bytes need to be skipped over.
 *
 * \subsection complete A (small) complete example
 *
 * Putting together the pieces above we can create a simple formatter table
 * for a buffer with only variable-sized string fields.
 *
 * First, define the list of fields and their format:
 * \code
 * char string1[NSTRING1];
 * char string2[NSTRING2];
 * char iso_bitmap[NBITMAP];
 * formatField_t fields[] = {
 *   // position of the bitmap on the formatted buffer
 *   { -1, NULL, formatBodyISO8583, NULL, sizeof(iso_bitmap), iso_bitmap, 0 },
 *   // no padding, with size prefix
 *   {  0, formatSizeLL, formatBodyStrcpy, NULL, 0, string1, 0 },
 *   // padding with spaces
 *   {  1, formatSizeLL, formatBodyStrcpy, formatPadSpaces, NSTRING2, string2, 0 },
 * };
 * \endcode
 *
 * Create the \ref format_t instance with the fields defined above:
 * \code
 * format_t fmt = {
 *   // list of fields
 *   fields,
 *   // number of fields
 *   sizeof(fields) / sizeof(fields[0]),
 *   // filter function
 *   &formatFilterISO8583,
 *   // the other fields are initialized to zero
 * };
 * \endcode
 *
 * Set the bits on \c iso_bitmap to identify which fields are part of the buffer.
 * (When unpacking, the buffer is read from the input buffer).
 * \code
 * // destination buffer
 * uint8_t output[NOUTPUT];
 * // clear list of bits
 * bitsSetRange(iso_bitmap, 0, 8 * NBITMAP, 0);
 * // enable field indexes 0 and 1
 * bitsSet(iso_bitmap, 0, 1);
 * bitsSet(iso_bitmap, 1, 1);
 * // call formatting
 * formatPack(&fmt, output, sizeof(output));
 * \endcode
 */

#include <larlib/base.h>

/*
 * Typedefs
 */
typedef struct format_t format_t;
typedef struct formatField_t formatField_t;

/**
 * Used as parameter to the formatting functions to inform if they
 * should handle packing or unpacking.
 */
enum formatDirection_t {
    FORMAT_PACK,            /**< Write to buffer */
    FORMAT_UNPACK           /**< Read from buffer */
};

typedef enum formatDirection_t formatDirection_t;

/**
 * Overall parameters of a format session.
 */
struct format_t {
    /** Pointer to array of fields to be processed */
    formatField_t *fields;

    /** Number of entries in \p fields */
    int nfields;

    /**
     * This function is called before each field is processed, receiving a
     * pointer to the field being processed and the overall format_t. It
     * should return \e non-zero if the field should be written or read and
     * \e zero if not.
     *
     * @param fmt Descriptor of format instance.
     * @param field Descriptor of field to be formatted.
     *
     * @return Non-zero if \p field should be read/written.
     * @return Zero if \p field should be ignored.
     */
    int (*filter)(const format_t *fmt, const formatField_t *field);

    /**
     * Should point to an array with the information that is read by \p filter
     * to decide if a field is or not at the buffer.
     */
    void *fieldMap;

    /** Last error code during pack / unpack */
    int errorCode;

    /**
     * Pointer to \ref formatField_t where error was detected.
     * NULL if no error was found during processing, or error was outside
     * a field.
     */
    formatField_t *errorField;
};

/**
 * Information about each field to be read or written.
 */
struct formatField_t {

    /**
     * Index of this field (usually on \p fieldMap in the parent \ref format_t).
     * If not used should be set to -1.
     */
    int index;

    /**
     * Pack/unpack prefix of field.
     */
    int (*prefix)(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

    /**
     * Pack/unpack actual field contents.
     */
    int (*body)(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

    /**
     * Pack/unpack suffix of field.
     */
    int (*suffix)(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

    /**
     * Default size of the field. Should be filled by user, zero if not relevant.
     */
    int size;

    /**
     * Pointer to actual field data. Should be filled by the \c body function.
     */
    void *data;

    /**
     * Actual size of the field, as returned by \c body.
     * Default to the same value of \c size.
     */
    int bodySize;
};

/**
 * Write to \p output the packed values as described in \p fmt.
 *
 * @param fmt \ref format_t with packing parameters.
 * @param output Where to store the packed message.
 * @param maxOutput Size of \p output, in bytes.
 *
 * @return Number of bytes writen on success.
 * @return Negative on error.
 */
int formatPack(format_t *fmt, uint8_t *output, int maxOutput);

/**
 * Read from \p input and unpack the fields to \p fmt.
 *
 * @param fmt \ref format_t with packing parameters.
 * @param input Where to read the packed message.
 * @param inputSize Size of \p input, in bytes.
 *
 * @return Number of bytes actually read from \p input, on success.
 * @return Negative on error.
 */
int formatUnpack(format_t *fmt, uint8_t *input, int inputSize);

/**
 * @addtogroup larlib_format_fn Field Formatting Functions
 * @{
 * Set of pre-defined formatting functions.
 *
 * @todo Review the list of format functions and add new ones.
 */

/**
 * Use this function as the \c filter function for a ISO8583 buffer.
 *
 * This function uses \c field->index as the index of a bit in \c fmt->fieldMap
 * to check if this field should be read or written.
 *
 * Always return non-zero (\e allowed) if \c fieldMap is \c NULL or
 * \c field->index is negative.
 *
 * @param fmt Formatting instance.
 * @param field Pointer to field data.
 *
 * @return Non-zero if \p field should be read or written.
 * @return Zero if not.
 */
int formatFilterISO8583(const format_t *fmt, const formatField_t *field);

/**
 * Read/write the bitmap of a ISO8583 message.
 *
 * \c field->size \e must include the size of the bitmap, in bytes
 * (8 for 64-bits, 16 for 128-bits).
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the ISO8583 bitmap.
 *
 * @return Positive number of bytes actually read or written.
 * @return Negative number on error.
 */
int formatBodyISO8583(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format a field as a recursive call to \ref formatPack() and \ref formatUnpack().
 *
 * Packs the field by calling <tt>formatPack(field->data, buffer, limit)</tt> and
 * unpacks the field by calling <tt>formatUnpack(field->data, buffer, field->bodySize)</tt>.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about this field, \c field->data should point to a
 *  \ref format_t instance.
 *
 * @return Positive number of bytes actually read or written.
 * @return Negative number on error.
 */
int formatBodyPacked(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Skip \p field->size bytes from message.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Positive number of bytes actually skipped (always \p field->size).
 * @return Negative number on error.
 */
int formatBodySkip (uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Copy \p field->size bytes of \p field->data to/from \p buffer.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Positive number of bytes actually read or written (always \p field->size).
 * @return Negative number on error.
 */
int formatBodyMemcpy(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Formatting for variable-sized zero-terminated strings.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read (always \c bodySize).
 * @return Negative number on error.
 */
int formatBodyStrcpy(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format an <tt>uint64_t</tt> field as a decimal string.
 *
 * \c field->data must point to an <tt>uint64_t</tt>, its value will be
 * read and written as a string in base-10.
 *
 * @note No padding with zeroes is done, the return is the number of decimal
 *  digits actually used. Use \c prefix or \c suffix for padding. Also note
 *  that negative numbers are not supported!
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatBodyUint64Dec(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format an <tt>uint32_t</tt> field as a decimal string.
 *
 * \c field->data must point to an <tt>unsigned int</tt>, its value will be
 * read and written as a string in base-10.
 *
 * @note No padding with zeroes is done, the return is the number of decimal
 *  digits actually used. Use \c prefix or \c suffix for padding. Also note
 *  that negative numbers are not supported!
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatBodyUint32Dec(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format an <tt>uint16_t</tt> field as a decimal string.
 *
 * \c field->data must point to an <tt>unsigned short</tt>, its value will be
 * read and written as a string in base-10.
 *
 * @note No padding with zeroes is done, the return is the number of decimal
 *  digits actually used. Use \c prefix or \c suffix for padding. Also note
 *  that negative numbers are not supported!
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatBodyUint16Dec(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format an <tt>uint8_t</tt> field as a decimal string.
 *
 * \c field->data must point to an <tt>unsigned char</tt>, its value will be
 * read and written as a string in base-10.
 *
 * @note No padding with zeroes is done, the return is the number of decimal
 *  digits actually used. Use \c prefix or \c suffix for padding. Also note
 *  that negative numbers are not supported!
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatBodyUint8Dec(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format an `uint64_t` field as a sequence of bytes.
 *
 * `field->data` must point to an `uint64_t`, its value will be read and written as
 * a sequence of bytes in *big-endian* order.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to `buffer`, or number of bytes read.
 * @return Negative number on error
 */
int formatBodyUint64Buf(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Format an <tt>buffer</tt> field as a hexadecimal string.
 *
 * \c field->data must point to an <tt>char</tt> array, its values will be
 * read and written as a string in base-16.
 *
 * @note No padding is done, the return is the number of hexadecimal
 *  digits actually used. Use \c prefix or \c suffix for padding.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatBodyBufHex(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Read or write the value of \c bodySize as a 2-digit base-10 string.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field. Only \c bodySize is used.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatSizeAsc2(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Read or write the value of \c bodySize as a 3-digit base-10 string.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field. Only \c bodySize is used.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatSizeAsc3(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Read of write the value of \c bodySize as a 2-digit BCD number (1 byte).
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field. Only \c bodySize is used.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatSizeBcd2(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Read of write the value of \c bodySize as a 3-digit BCD number (2 bytes).
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field. Only \c bodySize is used.
 *
 * @return Number of bytes actually written to \p buffer, or number of bytes read.
 * @return Negative number on error.
 */
int formatSizeBcd3(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Write or skip <tt>field->size - field->bodySize</tt> spaces.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return <tt>field->size - field->bodySize</tt>
 * @return Negative number on error.
 */
int formatPadSpaces(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Write or skip <tt>field->size - field->bodySize</tt> ASCII '0' chars.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return <tt>field->size - field->bodySize</tt>
 * @return Negative number on error.
 */
int formatPadZeroes(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Write or skip `field->size - field->bodySize` 0x00 bytes.
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return <tt>field->size - field->bodySize</tt>
 * @return Negative number on error.
 */
int formatPadZeroBytes(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/**
 * Skip exactly <tt>field->size - field->bodySize</tt> bytes.
 *
 * @attention Nothing is written to the output buffer during FORMAT_PACK!
 *
 * @param buffer Output buffer.
 * @param limit Max number of bytes to write to \p buffer.
 * @param direction FORMAT_PACK or FORMAT_UNPACK
 * @param fmt Formatting context
 * @param field Information about the field.
 *
 * @return <tt>field->size - field->bodySize</tt>
 */
int formatPadSkip(uint8_t *buffer, int limit, formatDirection_t direction, format_t *fmt, formatField_t *field);

/* @} */

/* @} */

#endif
