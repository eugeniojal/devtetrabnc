#ifndef LARLIB_DATE_H
#define LARLIB_DATE_H

/**
 * @file date.h
 * @brief Date and time
 *
 * @addtogroup larlib_date Date and time
 * @{
 *
 * \section Rationale
 *
 * Date handling is widely known to be hard. Our day-to-day uses are
 * basically reading and setting the system date, and doing simple
 * date math to skip over weekends and computing closing and opening
 * times.
 *
 * \section date_intro Introduction
 *
 * This module provides basic functionality to work with dates and time.
 * Time-zones or daylight saving time are ignored, all operations are assumed
 * to take place in a common neutral time zone.
 */

#include <larlib/base.h>

/*
 * Typedef for structures below
 */
typedef struct dateDate_t dateDate_t;
typedef struct dateTime_t dateTime_t;

/**
 * Enumeration of months.
 * Notice that \ref DATE_JAN is \c 1, there is no month zero.
 */
enum dateMonths_t {
    DATE_JAN = 1,       /**< January */
    DATE_FEB,           /**< February */
    DATE_MAR,           /**< March */
    DATE_APR,           /**< April */
    DATE_MAY,           /**< May */
    DATE_JUN,           /**< June */
    DATE_JUL,           /**< July */
    DATE_AUG,           /**< August */
    DATE_SEP,           /**< September */
    DATE_OCT,           /**< October */
    DATE_NOV,           /**< November */
    DATE_DEC            /**< December */
};

/**
 * Enumeration of days of a week
 */
enum dateWeekdays_t {
    DATE_SUN = 0,       /**< Sunday */
    DATE_MON,           /**< Monday */
    DATE_TUE,           /**< Tuesday */
    DATE_WED,           /**< Wednesday */
    DATE_THU,           /**< Thursday */
    DATE_FRI,           /**< Friday */
    DATE_SAT            /**< Saturday */
};

/**
 * A gregorian date.
 */
struct dateDate_t {
    uint8_t day;        /**< Day of month, range [1,31] */
    uint8_t mon;        /**< Month (see \ref dateMonths_t) */
    uint16_t year;      /**< Non-negative year */
};

/**
 * A gregorian date with time.
 */
struct dateTime_t {
    dateDate_t date;        /**< Date information */
    uint8_t hour;       /**< Hour, range [0,23] */
    uint8_t min;        /**< Minutes, range [0,59] */
    uint8_t sec;        /**< Seconds, range [0,59] */
};

/**
 * Return the current system date.
 *
 * @param[out] d Current system date.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>d == NULL</tt>.
 */
int dateGet(dateDate_t *d);

/**
 * Return the current system date and time.
 *
 * @param[out] dt Current system date and time.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>dt == NULL</tt>.
 */
int dateTimeGet(dateTime_t *dt);

/**
 * Return an increasing count of milliseconds.
 *
 * Given the architectural differences, this call does not guarantee <em>from when</em>
 * the count starts.  It may start on the device boot or may be the number of
 * milliseconds since a certain past date.
 *
 * Also, there is no guarantee that the resolution is effectively "thousands of seconds",
 * it may be "hundreds of seconds", for example.
 *
 * The only guarantee is that in two sequential calls, the second one will never
 * return a value smaller than the first, and that the returned value increases in
 * a constant frequency.
 *
 * Given those limitations, this call is intended to be used as a chronometer, not
 * to actually mark current time.
 *
 * @return Number of milliseconds since a certain date.
 */
uint64_t dateTimeGetMs(void);

/**
 * Update the system date.
 *
 * @param d Date to set.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>d == NULL</tt> or
 *   <tt>!dateIsValid(d)</tt>.
 * @return BASE_ERR_ACCESS if the application does not have permission
 *   to change the system date.
 */
int dateSet(const dateDate_t *d);

/**
 * Update the system date and time.
 *
 * @param dt Date and time to set.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>dt == NULL</tt> or
 *   <tt>!dateIsValid(dt)</tt>.
 * @return BASE_ERR_ACCESS if the application does not have permission
 *   to change the system date.
 */
int dateTimeSet(const dateTime_t *dt);

/**
 * Return the Julian Day Number of \p d.
 * @see http://en.wikipedia.org/wiki/Julian_day
 * @pre \c dateIsValid(d)
 *
 * @param d Date to convert.
 *
 * @return The non-negative number of days since January 1, 4713 BC,
 *   aka Julian Day Number.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>d == NULL</tt> or
 *   <tt>!dateIsValid(d)</tt>.
 */
int dateToJulianDay(const dateDate_t *d);

/**
 * Convert the Julian Day Number to the Gregorian calendar.
 * @see http://en.wikipedia.org/wiki/Julian_day
 *
 * @param jdn The Julian Day Number to convert.
 * @param[out] d Gregorian date.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>d == NULL</tt> or <tt>jdn
 *   \< 0</tt>.
 */
int dateFromJulianDay(int jdn, dateDate_t *d);

/**
 * Return the weekday of a given date.
 *
 * @pre \c dateIsValid(d)
 *
 * @param d Date to be processed.
 *
 * @return Weekday of the date in \p d. See \ref dateWeekdays_t.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>d == NULL</tt> or
 *   <tt>!dateIsValid(d)</tt>.
 */
int dateWeekday(const dateDate_t *d);

/**
 * Return non-zero if given year number is a leap year.
 *
 * @param year Year to check, with century (for example 1998 or 2011).
 *
 * @return Zero if \p year is \e not a leap year.
 * @return Non-zero if \p year is a leap year.
 */
int dateIsLeap(uint16_t year);

/**
 * Checks if a date is valid.
 * Takes into account leap years.
 *
 * @param d Date to verify.
 *
 * @return Zero if \p d is invalid, this includes <tt>d == NULL</tt>.
 * @return Non-zero if \p d is valid.
 */
int dateIsValid(const dateDate_t *d);

/**
 * Checks if both time and date are valid.
 *
 * @param dt Date/time to verify.
 *
 * @return Zero if \p d is invalid, this includes <tt>d == NULL</tt>.
 * @return Non-zero if \p d is valid.
 */
int dateTimeIsValid(const dateTime_t *dt);

/**
 * Return the difference, in days, between two dates.
 *
 * Given the defined possible return values for this function, it is
 * recommended to check if both \p a and \p b are valid before
 * calling:
 * @code
 * if (!dateIsValid(a) || !dateIsValid(b)) error("invalid dates!");
 * else return dateDiff(a, b);
 * @endcode
 *
 * @pre <tt>dateIsValid(a) && dateIsValid(b)</tt>
 *
 * @param a Minuend.
 * @param b Subtrahend.
 *
 * @return The difference <tt>a - b</tt> in days. This maybe a
 *   positive or negative number.
 * @return Zero if either \p a or \p b are invalid. Note that there is
 *   no way to differentiate this case from <tt>a == b</tt>, the
 *   caller must guarantee that the preconditions apply.
 */
int dateDiff(const dateDate_t *a, const dateDate_t *b);

/**
 * Add a number of days to a given date.
 *
 * @param[in,out] d Date to be modified in place.
 * @param ndays Number of days to add, positive or negative.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>d == NULL</tt> or
 *   <tt>!dateIsValid(d)</tt>.
 */
int dateAddDays(dateDate_t *d, int ndays);

/**
 * Compare two dates.
 *
 * @pre <tt>dateIsValid(a) && dateIsValid(b)</tt>
 * @see dateDiff()
 *
 * @param a Left-hand of comparison.
 * @param b Right-hand of comparison.
 *
 * @return > 0 if <tt>a > b</tt>.
 * @return == 0 if <tt>a == b</tt> or either \p a or \p b are invalid.
 * @return \< 0 if <tt>a \< b</tt>.
 */
int dateCompare(const dateDate_t *a, const dateDate_t *b);

/**
 * Add seconds to a given date and time.
 *
 * @param[in,out] dt Date time to be modified in place.
 * @param nsecs Number of seconds to add, positive or negative. The
 *   date component is also updated if necessary.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>dt == NULL</tt> or
 *   <tt>!timeIsValid(dt)</tt>.
 */
int dateTimeAddSeconds(dateTime_t *dt, int nsecs);

/**
 * Compare two date times.
 *
 * @pre <tt>timeIsValid(a) && timeIsValid(b)</tt>
 *
 * @param a Left-hand of comparison.
 * @param b Right-hand of comparison.
 *
 * @return > 0 if <tt>a > b</tt>.
 * @return == 0 if <tt>a == b</tt> or either \p a or \p b are invalid.
 * @return \< 0 if <tt>a \< b</tt>.
 */
int dateTimeCompare(const dateTime_t *a, const dateTime_t *b);

/**
 * Return the difference, in seconds, between two date times.
 *
 * Given the defined possible return values for this function, it is
 * recommended to check if both \p a and \p b are valid before
 * calling:
 * @code
 * if (!timeIsValid(a) || !timeIsValid(b)) error("invalid!");
 * else return timeDiff(a, b);
 * @endcode
 *
 * @pre <tt>timeIsValid(a) && timeIsValid(b)</tt>
 *
 * @param a Minuend.
 * @param b Subtrahend.
 *
 * @note Given the limited return range, this function may not work
 *   properly if \p a and \p b are too far way (about 68 years in
 *   32-bit machines).
 *
 * @return The difference <tt>a - b</tt> in seconds. This maybe a
 *   positive or negative number.
 * @return Zero if either \p a or \p b are invalid. Note that there is
 *   no way to differentiate this case from <tt>a == b</tt>, the
 *   caller must guarantee that the preconditions apply.
 */
int dateTimeDiff(const dateTime_t *a, const dateTime_t *b);

/* @} */

#endif
