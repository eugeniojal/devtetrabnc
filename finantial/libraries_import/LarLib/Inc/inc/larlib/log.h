#ifndef LARLIB_LOG_H
#define LARLIB_LOG_H

/**
 * @file log.h
 * @brief General Logging System.
 *
 * @addtogroup larlib_log Logging
 * @{
 *
 * \section Rationale
 *
 * Tracing is one of the basic helper tools for debugging. Defining a
 * common tracing API that can be used by all LAR libraries and
 * modules makes easier for them to interact. Nevertheless tracing
 * should be optional and highly customizable.
 *
 * \section Introduction
 *
 * The \c larlib.log module provides a general logging/tracing system
 * with assertions and other helper routines. It is designed to be
 * easily ported to any platform.
 *
 * Its mechanics are based on the concept of <em>tracing
 * channels</em>: 256 channels are available, and each one can be
 * separately configured to be disabled, or enabled with a given
 * configuration. An application can use different channel IDs to
 * different modules so to conditionally enable tracing of specific
 * modules.
 *
 * By default all channels are \e disabled.
 *
 * @note Channel numbers 200 and higher are reserved for LAR's
 *   internal libraries and modules and should \e not be used by
 *   applications.
 *
 * Given that tracing is very platform dependent, this library takes
 * the position that all platform-dependency is moved to the \e
 * configuration step, and after that the actual tracing code should
 * not need to change.
 *
 * \section Setup
 *
 * Each channel has a separated configuration, and each configuration
 * includes, information about how to write, and the tracing level in
 * use.
 *
 * The <em>tracing level</em> of a given channel is a number that
 * define the priority of the trace. On run-time only traces with a
 * priority value <em>larger than or equal to</em> the configured
 * priority of this channel are actually written.
 *
 * For example, to have all tracing disabled, except for the channel
 * number 42, which should only print errors or higher priority:
 * @code
 * // disable all channels (this is the default)
 * logSetChannels(LOG_ALL_CHANNELS, 0, NULL, NULL, NULL);
 * // enable only channel 42
 * logSetChannels(42, 1, LOG_ERROR, myWriteFunction,
 *                myDumpFormatFunction, myContext);
 * @endcode
 *
 * For each channel a \c write function should be given, this function is
 * unique for each platform and device, and a \c format function may also
 * be given.
 *
 * In platforms and devices where the \c write function expects binary
 * data (Telium's \c trace() for instance) the \c format function may
 * not be given, and all binary data written using \ref logDump() will
 * be directly sent to the \c write function.
 *
 * In platforms where the \c write function only accepts ASCII chars
 * (the printer for example) then the \c format function must be given
 * and it should convert the binary data received by \ref logDump() to
 * a printable (ASCII-only) format. The provided function \ref
 * logDumpFormattedAscii() is an example of this formatting function,
 * and will convert the binary data to a two-column hexadecimal/ascii
 * format.
 *
 * \subsection Telium
 *
 * The Telium platform has a built-in system for tracing, using the \c
 * trace() function. Also, each trace may have an associated code
 * (called its \e SAP code).
 *
 * To use the built-in \c trace() function, use \ref logWriteTeliumTrace()
 * as \c writeFunction and \c NULL as \c dumpFormatFunction parameters to
 * \ref logSetChannels().
 * @code
 * logSetChannels(<firstChannel>, <channelCount>, <logLevel>, logWriteTeliumTrace, NULL, (void *) <sapCode>);
 * @endcode
 *
 * \section Tracing
 *
 * After the configuration is done using \ref logSetChannels(), one
 * should use \ref logPrintf(), \ref logPrintvf() or \ref logDump()
 * to output traces:
 * @code
 * // this trace has LOG_ERROR priority
 * logPrintf(42|LOG_ERROR, "Failed at %s with error %d", functionName, errorCode);
 * // But those only print if LOG_DEBUG level is active for channel 42:
 * logPrintf(42, "Result buffer:");
 * logDump(42, buffer, bufferSize);
 * @endcode
 *
 * \ref LOG_ASSERT() uses channel \ref LOG_CH_BASE to display the
 * assertion failure message and resets the device. If channel \ref
 * LOG_CH_BASE is not enabled the message is not displayed, but the
 * device will be reseted anyway. See \ref logReservedChannels_t.
 */

#include <larlib/base.h>
#include <stdarg.h>

/**
 * General tracing levels. Applications may define intermediary values.
 */
enum logLevel_t {
    LOG_DEBUG    = 0x0000,  /**< Lowest-priority: only for debugging */
    LOG_INFO     = 0x2000,  /**< Informative */
    LOG_WARNING  = 0x4000,  /**< Run-time warnings */
    LOG_ERROR    = 0x6000,  /**< Run-time errors  */
    LOG_CRITICAL = 0x8000,  /**< Critical */
    LOG_ALWAYS   = 0xFF00   /**< Always write (if channel is enabled) */
};

/**
 * List of reserved log channels. These channels are reserved by \c
 * larlib and other system libraries.
 */
enum logReservedChannels_t {
    LOG_CH_BASE = 200,  /**< \ref larlib_base and \ref LOG_ASSERT() */
    LOG_CH_CONV,        /**< \ref larlib_conv */
    LOG_CH_DATE,        /**< \ref larlib_date */
    LOG_CH_FS,          /**< \ref larlib_fs */
    LOG_CH_INI,         /**< \ref larlib_ini */
    LOG_CH_MEM,         /**< \ref larlib_mem */
    LOG_CH_TAB,         /**< \ref larlib_tab */
    LOG_CH_STEP,        /**< \ref larlib_step */
    LOG_CH_FORMAT,      /**< \ref larlib_format */
    LOG_CH_TASK         /**< \ref larlib_task */
};

/**
 * This is a short-cut to define the parameters of all channels in one
 * single call to \ref logSetChannels().
 */
#define LOG_ALL_CHANNELS 0, 256

/**
 * Type of the function that does the actual writing on a trace
 * channel.
 *
 * @param context The same \c context variable that is passed as
 *   parameter to \ref logSetChannels()
 * @param channelLevel Channel and level that triggered this trace
 * @param msg Data to write.
 * @param msgLength Length, in bytes, of \p msg.
 *
 * @return Zero on success, non-zero on error.
 */
typedef int (*logWriteFunction_t)(void *context, uint16_t channelLevel,
                                  const char *msg, int msgLength);

/**
 * Type of function called to format the output on \ref logDump().
 * It should call the received \p write function to actually output
 * the formatted data.
 *
 * @param write The function to call to actually generate the trace
 * @param context The same \c context variable that is passed as
 *   parameter to \ref logSetChannels()
 * @param channelLevel Channel and level that triggered this trace
 * @param data Pointer to binary data to write.
 * @param dataLen Length of \p data, in bytes.
 *
 * @return Zero on success, non-zero on error.
 */
typedef int (*logDumpFunction_t)(logWriteFunction_t write,
                                 void *context, uint16_t channelLevel,
                                 const void *data, int dataLen);

/**
 * Assertion using \c larlib.log for output.
 *
 * If \p cond evaluates to zero, write an error message on channel
 * \ref LOG_CH_BASE and either \b halt execution or \b reset the device.
 *
 * @hideinitializer
 */
#ifndef NDEBUG
#define LOG_ASSERT(cond) ((void) ((!!(cond)) || (logAssertImpl(#cond, __LINE__, __FILE__), 0)))
#else
#define LOG_ASSERT(cond)
#endif

/*
 * Implementation detail -- this function prints the assertion message
 * \p condStr plus the line number \p line and the file name \p file
 * and resets the device.
 */
void logAssertImpl(const char *condStr, int line, const char *file);

/**
 * A \ref logWriteFunction_t that can be used as parameter to \ref
 * logSetChannels() and writes traces using Telium's \c trace()
 * function.
 *
 * Traces are written using the SAP code defined by the integer value
 * of \p context. For example, to use SAP code 0xB000, use:
 * @code
 * logWriteTeliumTrace((void*) 0xB000, myMessage, strlen(myMessage))
 * @endcode
 * To configure the channel 42 to use this tracing function with SAP
 * code 0x1234, use:
 * @code
 * logSetChannels(42, 1, LOG_DEBUG, logWriteTeliumTrace, NULL, (void*) 0x1234);
 * @endcode
 *
 * @attention This function is Telium-specific and may not be defined
 *   on other platforms.
 *
 * @param context The \e value of this pointer indicates which SAP
 *   code to use.
 * @param channelLevel Channel and level that triggered this trace
 * @param msg Zero-terminated string to write.
 * @param msgLength Length of \p msg in bytes.
 *
 * @return Always return BASE_ERR_OK.
 *
 * @see logSetChannels
 */
int logWriteTeliumTrace(void *context, uint16_t channelLevel, const char *msg, int msgLength);

/**
 * A \ref logWriteFunction_t that can be used as parameter to \ref
 * logSetChannels() and writes traces to the Remote Debugger console on
 * IngeDev.
 *
 * @attention This function is Telium-specific and may not be defined
 *   on other platforms.
 *
 * @param context Not used
 * @param channelLevel Channel and level that triggered this trace
 * @param msg Zero-terminated string to write.
 * @param msgLength Length of \p msg in bytes.
 *
 * @return Always return BASE_ERR_OK.
 *
 * @see logSetChannels
 */
int logWriteTeliumRemoteDebugger(void *context, uint16_t channelLevel, const char *msg, int msgLength);

/**
 * \c dumpFormatFunction that formats the received buffer as a two-column
 * hexadecimal and ASCII display and calls the received \p write function.
 *
 * This function can be used as the \c dumpFormatFunction parameter of
 * \ref logSetChannels() for channels where \c write is not ready to
 * directly receive binary (non-ASCII) data.
 *
 * @param write Function that will be used to actually write the formatted lines.
 * @param context Ignored and passed as received to \p write.
 * @param channelLevel Channel and level that triggered this trace
 * @param data Binary data to format.
 * @param dataLen Length of \p data in bytes.
 *
 * @return The return of \p write.
 */
int logDumpFormattedAscii(logWriteFunction_t write,
                          void *context, uint16_t channelLevel,
                          const void *data, int dataLen);

/**
 * Configures one or more tracing channels.
 *
 * Configures the first \p numChannels starting at \p firstChannel to
 * use the tracing priority \p level and write output using \p
 * writeFunction and \p dumpFormatFunction.
 *
 * For example, to configure the channels in the range
 * <tt>[5,10]</tt>:
 * @code
 * logSetChannels(5, 5, myLevel, myWriteFunction, NULL, myContext);
 * @endcode
 *
 * To configure all channels, use the macro \c LOG_ALL_CHANNELS:
 * @code
 * logSetChannels(LOG_ALL_CHANNELS, myLevel, myWriteFunction, NULL, myContext);
 * @endcode
 *
 * @param firstChannel Index of the first channel to configure.
 * @param numChannels How many channels starting at \p firstChannel to
 *   configure. If zero no channels are configured!
 * @param level Tracing level enabled for the selected channels. Only
 *   the most significant 8-bits are used, see the values in \ref
 *   logLevel_t.
 * @param writeFunction A function that is called with the string to
 *   write. It is called only with a zero-terminated string of chars
 *   in the ASCII range. Using \c NULL disables this channel.
 * @param dumpFormatFunction A function that is called to pre-process
 *   the binary buffer received by \ref logDump(). If \c NULL then
 *   logDump() will directly call \p writeFunction with the received
 *   data buffer. If not \c NULL then this function should format the
 *   input buffer and call the received \c write function with the
 *   formatted output. See \ref logDumpFormattedAscii().
 * @param context This value is stored and passed unchanged to \p
 *   writeFunction, and can be used to configure its behavior. See
 *   the documentation of each logWrite function for more information.
 *
 * @return BASE_ERR_OK
 * @return BASE_ERR_INVALID_PARAMETER if the range defined by \p
 *   firstChannel and \p numChannels is invalid
 *
 * @see logWriteFunction_t
 * @see logDumpFunction_t
 * @see logPrintf
 * @see logPrintvf
 * @see logDump
 * @see logDumpFormattedAscii
 * @see logWriteTeliumTrace
 */
int logSetChannels(uint8_t firstChannel, int numChannels, uint16_t level,
                   logWriteFunction_t writeFunction,
                   logDumpFunction_t dumpFormatFunction,
                   void *context);

/**
 * Check if a log channel is enabled for a specific priority level.
 *
 * Given a \p channelLevel, formatted exactly as would for \ref logPrintf() or
 * \ref logDump(), check if a write to this configuration would generate
 * a trace.
 *
 * @param channelLevel Defines both the channel and the level of this
 *   trace. The lower 8-bits define the channel to use, and the upper
 *   8-bits define the level.
 *
 * @return Non-zero if a \ref logPrintf() or \ref logDump() to \p channelLevel
 *   would generate a call to the configured \c writeFunction.
 * @return Zero otherwise.
 */
int logChannelIsEnabled(uint16_t channelLevel);

/**
 * \c sprintf() like formatted traces.
 *
 * This call formats and sends the message defined by \p fmt using the
 * parameters associated with the channel and priorities defined in \p
 * channelLevel.
 *
 * The values in \ref logLevel_t are defined such that a simple OR'ing
 * of the values with the channel number work.
 *
 * For example, to write to channel \c 42 with the default priority:
 * @code
 * logPrintf(42, "this is the message %d", 1);
 * @endcode
 * And to write to the same channel using the \ref LOG_CRITICAL priority:
 * @code
 * logPrintf(42|LOG_CRITICAL, "this is the message %d", 2);
 * @endcode
 *
 * @param channelLevel Defines both the channel and the level of this
 *   trace. The lower 8-bits define the channel to use, and the upper
 *   8-bits define the level.
 * @param fmt String format (as \c sprintf()).
 *
 * @return BASE_ERR_OK even if \p channel is disabled or message not written by priority
 * @return An error code returned by the \p writeFunction defined for this channel
 *
 * @see logSetChannels
 */
int logPrintf(uint16_t channelLevel, const char *fmt, ...);

/**
 * A version of logPrintf() that accepts a \c va_list as parameter.
 * This is quite useful for creating other abstractions on top of this
 * lib.
 *
 * @param channelLevel Channel and level of tracing for this trace
 *   (see \ref logPrintf()).
 * @param fmt String format (as \c sprintf()).
 * @param va \c va_list with the list of parameters to be formatted.
 *
 * @return BASE_ERR_OK
 *
 * @see logSetChannels
 */
int logPrintvf(uint16_t channelLevel, const char *fmt, va_list va);

/**
 * Write a block of data as a binary dump.
 *
 * The output format should include both the readable part of the data
 * pointed by \p buffer and the hexadecimal of its bytes.
 *
 * @param channelLevel Channel and level of this trace (see \ref logPrintf())
 * @param buffer Data block to dump.
 * @param size Number of bytes of \p buffer to print.
 *
 * @return BASE_ERR_OK
 *
 * @see logSetChannels
 */
int logDump(uint16_t channelLevel, const void *buffer, int size);

/* @} */

#endif
