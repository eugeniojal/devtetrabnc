#ifndef LARLIB_TASK_H
#define LARLIB_TASK_H

/**
 * @file task.h
 * @brief Multi-threading
 *
 * @addtogroup larlib_task Multi-threading
 * @{
 *
 * \section Rationale
 *
 * Multi-threading capabilities are necessary for cases such as background
 * dialing, controlling of external devices and background processing.
 * Unfortunately OS capabilities and APIs vary widely, so a common ground
 * is necessary to ease porting.
 *
 * \section Introduction
 *
 * This module deals with multi-threading, or the creation of simultaneous
 * threads of execution, and synchronization of those threads. The following
 * objects are given:
 *
 * - \e Threads: an application may create parallel threads of execution;
 * - \e Queues: FIFO (first-in-first-out) sequences of values designed so
 *   one thread can communicate with other in an orderly manner;
 * - \e Events: signals between threads, that carry no other information
 *   other than the event has been triggered;
 * - \e Semaphores: restrict simultaneous access to resources.
 *
 * \section Threads
 *
 * Each thread is a parallel point of execution that has access to the same
 * global environment (global variables, APIs) as the application as a whole,
 * but a local stack (for local variables).
 *
 * Because all threads access the same set of global variables great care
 * must be taken so two threads do not attempt to modify the same memory
 * location at the same time.
 *
 * Each thread has an associated amount of memory for its stack, the application
 * may "suggest" a minimum size for this stack on the call to \ref taskThreadCreate(),
 * but the size of the stack is ultimately platform-dependent.
 *
 * After a thread has finished running (i.e. the user provided function has
 * returned) its \ref taskThread_t handle becomes invalid, it is the user
 * responsibility to guarantee that the handle is not used in this case.
 *
 * This module does not provide any form of thread-local storage, if such is
 * required the application must implement this itself.
 *
 * @attention Each platform may have an unspecified limit on the number of concurrent
 *  tasks that may be created at each time!
 *
 * See http://en.wikipedia.org/wiki/Thread_(computer_science)
 * and http://en.wikipedia.org/wiki/Thread_safety
 *
 * \section Queues
 *
 * A queue is a variable-sized array of elements with first-in-first-out semantics.
 * The two basic operations provided are \ref taskQueuePost() which adds an
 * element to the \e front of the queue, and \ref taskQueuePop() which removes an
 * element from the \e back of the queue.
 *
 * This implementation is limited to queue elements of type <tt>void*</tt>, they
 * can be used to directly encode integer values (small enough to fit on an address)
 * or point themselves to the actual value.
 *
 * Queues are the recommended method of intra-thread communication, but the
 * application must be careful to enforce \e move semantics: i.e. when a value
 * (specially in case of pointers) is sent from one thread to another using a
 * queue, the sender thread is in effect relinquishing ownership of this element
 * and passing to the receiver thread. Carefully applying this methodology
 * has the potential of reducing errors caused by simultaneous access to memory
 * locations (see reference below).
 *
 * Note that, as in the \ref larlib_deque module, the queue does no special handling
 * of the <tt>void*</tt> pointers stored in it, more specifically, it will not
 * call \ref memFree() on any pointer, the application must be careful to
 * release any memory itself (specially when \ref taskQueueDestroy() is called).
 *
 * See http://en.wikipedia.org/wiki/Message_passing
 *
 * \section Events
 *
 * Events signal the occurrence of something. They are a binary marker with the
 * states \e signaled and \e non-signaled. When created, a \ref taskEvent_t
 * is in \e non-signaled state, and only a call to \ref taskEventSignal()
 * will change this.
 *
 * Threads may wait until an event is in \e signaled state by calling
 * \ref taskEventWait() or \ref taskEventWaitAny(). The difference is that the
 * first is a simpler API when it is necessary to wait for only one event handle,
 * while the second allows to wait for multiple event handles on the same
 * call.
 *
 * \subsection device_events Device Events
 *
 * Both supported platforms (Unicapt32, Telium) use a bitmap (encoded in a
 * 32-bit unsigned integer) to mark which events have occurred. This API
 * support interface with those OS-provided event facilities by using
 * \ref taskEventCreateSystem().
 *
 * A \ref taskEvent_t created this way works as any other event when used
 * with \ref taskEventWait() or \ref taskEventWaitAny(), but will not work
 * when used with \ref taskEventSignal(), only OS APIs will work.
 *
 * Note that since \e which bits are used for which peripherals changes
 * from platform to platform, this in itself is not a completely portable
 * solution.
 *
 * @note On Unicapt32 only device events are supported, user events generated
 *   by \c psyEventSend() and such are \e not supported, this is because the
 *   bitmap associated with each event defines the first parameter to
 *   \c psyPeripheralResultWait(), but do not handle the case of
 *   \c PSY_EVENT_RECEIVED.
 *
 * \section Semaphores
 *
 * Semaphores limit the number of threads that can simultaneously enter a given
 * region of code, called a <em>critical-region</em>. The most common case
 * being of allowing only one thread to enter a critical region at a time.
 *
 * This is useful to control access to resources where a queue or an event would
 * not apply, for example access to a file.
 *
 * For example, consider the (very) constrained case where two threads are
 * repeatedly appending information to a same file using a function \c appendToFile()
 * that receives as parameter the name of the file and a string message to
 * append to it:
 * \code
 * taskSemaphore_t *mutex;
 * static void init() {
 *   mutex = taskSemaphoreCreate(1); // only one thread at a time
 * }
 * static void thread1(void *p) {
 *   for (;;) {
 *     taskSemaphoreAcquire(mutex, TASK_INFINITE_TIMEOUT);
 *     appendToFile("global.txt", "from thread 1");
 *     taskSemaphoreRelease(mutex);
 *   }
 * }
 * static void thread2(void *p) {
 *   for (;;) {
 *     taskSemaphoreAcquire(mutex, TASK_INFINITE_TIMEOUT);
 *     appendToFile("global.txt", "from thread 2");
 *     taskSemaphoreRelease(mutex);
 *   }
 * }
 * void test(void) {
 *   init();
 *   taskThreadCreate(&thread1, NULL, 0);
 *   taskThreadCreate(&thread2, NULL, 0);
 *   taskThreadSleep(TASK_INFINITE_TIMEOUT);
 * }
 * \endcode
 * The semaphore \c mutex is used to forbid the two tasks from access the file
 * <tt>"global.txt"</tt> at the same time. (Note that this convoluted example
 * is un-optimal in many respects. If you find yourself writing this sort of
 * architecture on a real application, please reconsider! A more scalable approach
 * would be having an \e append-to-file thread that receives requests of
 * strings to write to the global file from a \ref taskQueue_t).
 *
 * See http://en.wikipedia.org/wiki/Semaphore_(programming)
 */

#include <larlib/base.h>

/** Use this constant to wait forever when a timeout is necessary. */
#define TASK_INFINITE_TIMEOUT (0xFFFFFFFFLU)

/**
 * A handle to a running thread.
 */
typedef struct taskThread_t taskThread_t;

/**
 * Type of a function pointer to be used as entry point of a new thread.
 * @param param The user-provided \c param as given to \ref taskThreadCreate()
 */
typedef void (*taskThreadFunction_t)(void *param);

/**
 * Handle to a multi-threading queue.
 */
typedef struct taskQueue_t taskQueue_t;

/**
 * Handle to a semaphore.
 */
typedef struct taskSemaphore_t taskSemaphore_t;

/**
 * Handle to an event.
 */
typedef struct taskEvent_t taskEvent_t;

/**
 * Create a new execution thread with entry point \p fn.
 *
 * @param fn Entry point for thread execution.
 * @param param Parameter to be passed to \p fn.
 * @param stackSize A hint on the minimum stack size required for this thread.
 *   Depending on the platform, different limitations on minimum and maximum
 *   stack size will apply. Use 0 for a default value that is at least 2KiB in
 *   all supported platforms.
 *
 * @return Handle to newly created thread, or \c NULL on error.
 */
taskThread_t *taskThreadCreate(taskThreadFunction_t fn, void *param, uint32_t stackSize);

/**
 * Sleep the current running thread.
 *
 * This will cause the current thread to stop executing for \p timeout hundreds
 * of second, but other threads will keep running.
 *
 * @param timeout Time to sleep the current thread, in hundreds of seconds.
 */
void taskSleep(uint32_t timeout);

/**
 * Return the handle of the current running thread.
 * @return Handle to current thread.
 */
taskThread_t *taskThreadCurrent(void);

/**
 * Create a new queue with up to \p maxItems allowed.
 * @param maxItems Max number of items that fit on this queue.
 * @return Handle to new queue or \c NULL on error.
 */
taskQueue_t *taskQueueCreate(int maxItems);

/**
 * Destroy a queue and discards all pending elements.
 *
 * @param q Queue to be discarded.
 *
 * @note The queue elements are discarded with no further processing,
 *   which may cause resource and/or memory leaks. If a \ref taskQueuePop()
 *   is waiting the behavior is undefined and platform-dependent.
 */
void taskQueueDestroy(taskQueue_t *q);

/**
 * Insert an element on the back of the queue \p q.
 *
 * Example:
 * \code
 * taskQueue_t *q = taskQueueCreate(NITEMS);
 * taskQueuePost(q, (void *) 0x12345678); // hand-crafted pointer value
 * taskQueuePost(q, (void *) &someVariable); // pointer to existing variable
 * \endcode
 *
 * @param q Handle to queue.
 * @param item Item to be inserted.
 *
 * @return BASE_ERR_OK on success
 * @return BASE_ERR_INVALID_HANDLE if \p q is invalid.
 * @return BASE_ERR_OVERFLOW if \p q already has the max number of elements
 *   allowed.
 */
int taskQueuePost(taskQueue_t *q, void *item);

/**
 * Extract the element from the front of the queue.
 *
 * Example:
 * \code
 * taskQueue_t *q = get_queue_handle();
 * void *value;
 * if (taskQueuePop(q, &value, TASK_INFINITE_TIMEOUT) == BASE_ERR_OK) {
 *   process((myType_t *) value);
 * }
 * \endcode
 *
 * @param q Handle to queue.
 * @param[out] item Where to store the element extracted from \p q
 *   (note the double indirection!).
 * @param timeout Time to wait for element, in hundreds of seconds.
 *
 * @return BASE_ERR_OK on success
 * @return BASE_ERR_INVALID_HANDLE if \p q is invalid
 * @return BASE_ERR_INVALID_PARAMETER if \p item is \c NULL
 * @return BASE_ERR_TIMEOUT if no element was received in \p timeout
 */
int taskQueuePop(taskQueue_t *q, void **item, uint32_t timeout);

/**
 * Create a semaphore with a maximum entry count of \p limit.
 *
 * @param limit Number of threads that may simultaneously acquire the returned
 *   semaphore.
 *
 * @return Handle to semaphore or \c NULL on error.
 */
taskSemaphore_t *taskSemaphoreCreate(int limit);

/**
 * Destroy an existing semaphore.
 *
 * The effects of destroying a semaphore that is being waited on is
 * undefined and platform-dependent.
 *
 * @param s Handle to semaphore.
 */
void taskSemaphoreDestroy(taskSemaphore_t *s);

/**
 * Acquire (also called \e wait or \e down) the semaphore.
 *
 * May block if the \p s access count is full, waiting for a slot to
 * vacant.
 *
 * @param s Handle to semaphore
 * @param timeout Max time to wait for a vacancy, in hundreds of seconds.
 *
 * @return BASE_ERR_OK on success (semaphore was acquired)
 * @return BASE_ERR_TIMEOUT if could no acquire semaphore in \p timeout
 * @return BASE_ERR_INVALID_HANDLE if \p s is invalid
 */
int taskSemaphoreAcquire(taskSemaphore_t *s, uint32_t timeout);

/**
 * Release (also called \e signal or \e up) a previously acquired semaphore.
 *
 * @param s Handle to semaphore.
 *
 * @return BASE_ERR_OK on success
 * @return BASE_ERR_INVALID_HANDLE if \p s is invalid
 */
int taskSemaphoreRelease(taskSemaphore_t *s);

/**
 * Create a new event handler.
 *
 * The event is created in \e non-signaled state. Multiple events can be
 * created and the limit on number of created events is not related to
 * the platform event handling primitives.
 *
 * @return An event handler or \c NULL on error.
 */
taskEvent_t *taskEventCreate(void);

/**
 * Creates an event handler to wait for system peripheral events.
 *
 * Both Unicapt32 and Telium platforms use a bitmap to check for device
 * events (see \c psyPeripheralResultWait() and \c ttestall()). This
 * call provides a bridge between taskEvent_t and system peripheral
 * events.
 *
 * Internally any call to \ref taskEventWait() or \ref taskEventWaitAny()
 * with events created this way will call the OS functionality to wait
 * for peripherals.
 *
 * For platforms where this form of event handling does not exist, this
 * call will return \c NULL.
 *
 * Event handlers created in this way also need to be destroyed by calling
 * \ref taskEventDestroy().
 *
 * @note Signaling an event created this way (by \ref taskEventSignal()) is
 *   \e not supported. Those events can only be triggered by the OS, either
 *   internally or by calling OS APIs.
 *
 * @param bitmask Which system event bits to wait for. The 31st bit (0x80000000)
 *   is reserved for internal use, trying to create an event for it will return
 *   \c NULL instead.
 *
 * @return A valid taskEvent_t handle, or \c NULL if \p bitmask is invalid or
 *   peripheral events are not supported in this platform.
 */
taskEvent_t *taskEventCreateSystem(uint32_t bitmask);

/**
 * Release the resources associated with an event handler.
 * What happens to threads blocked waiting for this event is undefined
 * and platform-dependent.
 * @param e Handle to event.
 */
void taskEventDestroy(taskEvent_t *e);

/**
 * Set an event to the \e signaled state.
 * If the event is already signaled, nothing changes.
 *
 * @attention This does \e not work with events created by
 *   \ref taskEventCreateSystem().
 *
 * @param e Handle to event.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_HANDLE if \p e is invalid or created with
 *   \ref taskEventCreateSystem().
 */
int taskEventSignal(taskEvent_t *e);

/**
 * Check if an event if signaled, without waiting or clearing the signal.
 *
 * @attention This does \e not work with events created by
 *   \ref taskEventCreateSystem().
 *
 * @param e Handle to event
 *
 * @return BASE_ERR_OK if \p e is signaled.
 * @return BASE_ERR_TIMEOUT if \p e was not signaled.
 * @return BASE_ERR_INVALID_HANDLE if \p e is invalid ir was created by
 *   \ref taskEventCreateSystem().
 * @return BASE_ERR_RESOURCE_PB in case of internal error.
 */
int taskEventCheck(taskEvent_t *e);

/**
 * Wait for \p e to change to signaled state.
 * If \p e is already signaled, will return immediately.
 *
 * Non-system events are automatically changed back to \e non-signaled state.
 * For system events this is platform-dependent.
 *
 * @param e Handle to event.
 * @param timeout Max time to wait for \p e to signal, in hundreds of second.
 *
 * @return BASE_ERR_OK if \p e was signaled.
 * @return BASE_ERR_TIMEOUT if \p e was not signaled.
 * @return BASE_ERR_INVALID_HANDLE if \p e is invalid.
 * @return BASE_ERR_RESOURCE_PB in case of internal error
 */
int taskEventWait(taskEvent_t *e, uint32_t timeout);

/**
 * Wait for at least one among a list of events to be signaled.
 *
 * Calling this function with <tt>nevents == 1</tt> is equivalent to a call
 * to \ref taskEventWait().
 *
 * Example:
 * \code
 * // ... events e1 and e2 created somewhere else ...
 * taskEvent_t *events[] = { e1, e2 };
 * int which = taskEventWaitAny(events, 2, TASK_INFINITE_TIMEOUT);
 * if (which == 0) event_e1_happened();
 * else if (which == 1) event_e2_happened();
 * else error_happened();
 * \endcode
 *
 * @param events Array of event handles to wait for.
 * @param nevents Number of elements in \p events.
 * @param timeout Max time to wait for an event to signal, in hundreds of second.
 *
 * @return The (zero-based) index (inside \p events) of the \e first event to be detected.
 * @return BASE_ERR_TIMEOUT if no event was signaled.
 * @return BASE_ERR_INVALID_HANDLE if any element in \p events is invalid.
 * @return BASE_ERR_RESOURCE_PB in case of internal error
 */
int taskEventWaitAny(taskEvent_t *events[], int nevents, uint32_t timeout);

/**
 * Set an event as \e non-signaled.
 *
 * This call may be used to "clean the slate" before other operations,
 * discarding previous signals.
 *
 * @attention This does \e not work with events created by
 *   \ref taskEventCreateSystem().
 *
 * @param e Handle to event.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_HANDLE if \p e is invalid.
 * @return BASE_ERR_RESOURCE_PB in case of internal error
 */
int taskEventClear(taskEvent_t *e);

/* @} */

#endif
