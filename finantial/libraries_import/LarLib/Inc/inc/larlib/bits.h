#ifndef LARLIB_BITS_H
#define LARLIB_BITS_H

/**
 * @file bits.h
 * @brief Bit manipulation.
 *
 * \addtogroup larlib_bits Bit operations
 * @{
 *
 * \section Rationale
 *
 * Many of the protocols, standards and devices we have to deal
 * require manipulation bit by bit of structures. Even though certain
 * algorithms are quite simple (set bit, get bit) others can be
 * complex (rotate, shift).
 *
 * \section Introduction
 *
 * This module manipulates sequences of bits. All functions operate on
 * "naked" sequences of bytes, this was decided in order to keep this
 * module low level. Users may wrap the functions to provide a more
 * secure API, with bounds checking, for example.
 *
 * Note that in all cases bellow, sizes and offsets/indices are given
 * in bits, and all operations consider the left most bit of the left
 * most byte as the bit index zero. As is default in C, all indices
 * are zero-based.
 *
 * A bit buffer of 16 bits, with the bit index 13 set, equals the
 * binary value "0000000000000100" or the buffer <tt>{ 0x00, 0x04
 * }</tt>.
 *
 * Certain operations may be faster if the number of bits to operate
 * is multiple of 8 (whole bytes), but there is no guarantee on this
 * regard.
 */

#include <larlib/base.h>

/**
 * A bit-buffer is a sequence of 8-bits unsigned integers. Note that
 * the actual size <em>in bits</em> of the buffer does not need to be
 * a multiple of 8, but memory allocation is done in bytes.
 */
typedef uint8_t bitsBuffer_t;

/**
 * Possible bit operations for \ref bitsBlt().
 */
enum bitsBltOp_t {
    BITS_BLT_COPY,          /**< Copy source bits into destination. */
    BITS_BLT_NOT,           /**< Copy negated source bits into destination */
    BITS_BLT_AND,           /**< Logical AND of source and destination */
    BITS_BLT_OR,            /**< Logical OR of source and destination */
    BITS_BLT_XOR,           /**< Logical XOR of source and destination */
    BITS_BLT_NOT_AND        /**< Logical AND of negated source and destination */
};

/**
 * Set or clear a bit in \p b.
 *
 * Note that this function does \e no bounds checking, the caller must
 * guarantee that \p index is a valid bit index inside \p b.
 *
 * @param b \ref bitsBuffer_t to modify.
 * @param index Index of bit to set.
 * @param value Zero to clear the bit, non-zero to set it.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>b == NULL</tt> or \p
 *   index is invalid.
 */
int bitsSet(bitsBuffer_t *b, int index, int value);

/**
 * Set a range of bits on a buffer.
 *
 * Set \p count bits of \p b starting at index \p offset to \p value.
 *
 * @param b Bit buffer to modify.
 * @param offset Index of first bit to modify.
 * @param count Number of bits to modify.
 * @param value Value to set (zero to clear, non-zero to set).
 *
 * @return BASE_ERR_OK.
 * @return BASE_ERR_INVALID_PARAMETER.
 */
int bitsSetRange(bitsBuffer_t *b, int offset, int count, int value);

/**
 * Return the value of a bit in \p b.
 *
 * Note that this function does \e no bounds checking, the caller must
 * guarantee that \p index is a valid bit index inside \p b.
 *
 * @param b \ref bitsBuffer_t to modify.
 * @param index Index of bit to return.
 *
 * @return The value of the bits at \p index as 0 (clear) or 1 (set)
 * @return BASE_ERR_INVALID_PARAMETER if <tt>b == NULL</tt> or \p
 *   index is invalid.
 */
int bitsGet(const bitsBuffer_t *b, int index);

/**
 * Extract a sub-sequence of a bit buffer as an integer.
 *
 * Interpret \p count bits of \p b starting at \p offset as an integer
 * (most-significant bit first). Note that this function call can only
 * return unsigned integers smaller than 2<sup>63</sup>-1, since
 * negative numbers are used for error reporting.
 *
 * For example, the integer at offset 2, count 3 in "01101000" is
 * "101" whose value is 5.
 *
 * No bounds checking is done on \p dst or \p src.
 *
 * @param b Bits buffer to read.
 * @param offset Index of first bit to read.
 * @param count Number of bits to read.
 *
 * @return The non-negative value of the bits read.
 * @return BASE_ERR_INVALID_PARAMETER.
 */
int64_t bitsExtractInt(const bitsBuffer_t *b, int offset, int count);

/**
 * Append one bit buffer to another.
 *
 * Append the \p srcCount bits of \p src starting at offset \p
 * srcOffset to a bit buffer \p dst that already have \p dstCount
 * bits.
 *
 * No bounds checking is done on \p dst or \p src.
 *
 * @pre \p dst and \p src must not overlap.
 *
 * @param src Source bit buffer.
 * @param srcOffset Index of first bit to read from \p src.
 * @param srcCount Number of bits to append.
 * @param dst Destination bit buffer.
 * @param dstCount Current size of \p dst in bits.
 *
 * @return The new size of \p dst on success (<tt>dstCount +
 *   srcCount</tt>).
 * @return BASE_ERR_INVALID_PARAMETER.
 */
int bitsAppend(const bitsBuffer_t *src, int srcOffset, int srcCount,
               bitsBuffer_t *dst, int dstCount);

/**
 * Append the bits extracted from an integer to a bit buffer.
 *
 * Append, from left to right, the right most \p nbits bits of \p bits
 * into the bit buffer \p dst that already have \p dstCount bits.
 *
 * For example, given a bit buffer with value "1010", using bits = 0xC
 * and nbits = 4 would result in the bit buffer "10101100", inserting
 * the sequence "1100". Notice that the bits to insert should be the
 * right most ones on the input integer, but they are inserted from
 * left to right (most significant first).
 *
 * No bounds checking is done on \p dst.
 *
 * @param bits Value of the bits to insert.
 * @param nbits Number of bits to insert.
 * @param dst Destination bit buffer.
 * @param dstCount Current size of \p dst in bits.
 *
 * @return The new size of \p dst on success (<tt>dstCount +
 *   nbits</tt>).
 * @return BASE_ERR_INVALID_PARAMETER.
 */
int bitsAppendInt(uint64_t bits, int nbits,
                  bitsBuffer_t *dst, int dstCount);

/**
 * Rotate \p b left by \p nrotate bits.
 *
 * Note that both the buffer size and the rotation size are <em>in
 * bits</em>, it is valid to rotate 3 bits to the left a buffer of 11
 * bits.
 *
 * When rotating, the bits that fall off on the left, are re-inserted
 * on the right. For example, rotating "11110000" 1 bit to the left
 * results in "11100001".
 *
 * @param b \ref bitsBuffer_t to rotate.
 * @param index First index on \p b to be affected.
 * @param nbits Number of bits of \p b to be affected.
 * @param nrotate Number of bits to rotate.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>b == NULL</tt> or \p
 *   nbits or \p nrotate are invalid.
 */
int bitsRotateLeft(bitsBuffer_t *b, int index, int nbits, int nrotate);

/**
 * Rotate \p b right by \p nrotate bits.
 *
 * Note that both the buffer size and the rotation size are <em>in
 * bits</em>, it is valid to rotate 3 bits to the right a buffer of 11
 * bits.
 *
 * When rotating, the bits that fall off on the right, are re-inserted
 * on the left. For example, rotating "00001111" 1 bit to the right
 * results in "10000111".
 *
 * @param b \ref bitsBuffer_t to rotate.
 * @param index First index on \p b to be affected.
 * @param nbits Number of bits of \p b to be affected.
 * @param nrotate Number of bits to rotate.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>b == NULL</tt> or \p
 *   nbits or \p nrotate are invalid.
 */
int bitsRotateRight(bitsBuffer_t *b, int index, int nbits, int nrotate);

/**
 * Shift \p b left by \p nshift bits.
 *
 * Note that both the buffer size and the shift size are <em>in
 * bits</em>, it is valid to shift 3 bits to the left a buffer of 11
 * bits.
 *
 * When shifting, zeroes are inserted on the right as the bits move
 * left.
 *
 * @param b \ref bitsBuffer_t to shift.
 * @param index First index on \p b to be affected.
 * @param nbits Size of \p b <em>in bits</em>.
 * @param nshift Number of \e bits to shift \p b.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>b == NULL</tt> or \p
 *   nbits or \p nshift are invalid.
 */
int bitsShiftLeft(bitsBuffer_t *b, int index, int nbits, int nshift);

/**
 * Shift \p b right by \p nshift bits.
 *
 * Note that both the buffer size and the shift size are <em>in
 * bits</em>, it is valid to shift 3 bits to the right a buffer of 11
 * bits.
 *
 * When shifting, zeroes are inserted on the left as the bits move
 * right.
 *
 * @param b \ref bitsBuffer_t to shift.
 * @param index First index on \p b to be affected.
 * @param nbits Size of \p b <em>in bits</em>.
 * @param nshift Number of \e bits to shift \p b.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER if <tt>b == NULL</tt> or \p
 *   nbits or \p nshift are invalid.
 */
int bitsShiftRight(bitsBuffer_t *b, int index, int nbits, int nshift);

/**
 * Copy a sequence of bits from one buffer to another.
 *
 * Copy \p srcCount bits starting at bit index \p srcOffset from \p
 * src into \p dst at offset \p dstOffset. This is equivalent to
 * calling \ref bitsBlt() with the \ref BITS_BLT_COPY operation.
 *
 * No bounds checking is done on \p dst or \p src.
 *
 * @param src Source bits buffer.
 * @param srcOffset Index of first bit to copy, in bits.
 * @param srcCount Number of bits to copy.
 * @param dst Destination bits buffer.
 * @param dstOffset Index of first bit to overwrite in \p dst.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_PARAMETER.
 */
int bitsCopy(const bitsBuffer_t *src, int srcOffset, int srcCount,
             bitsBuffer_t *dst, int dstOffset);

/**
 * Combine two bit buffers using a bit operation.
 *
 * This is a very general function that allow to combine two bit
 * buffers using one of many bit operations. \p dst is replaced with
 * the result of the operation defined by \p op on both \p src and \p
 * dst (note that certain operations ignore the value of \p dst, for
 * example \ref BITS_BLT_COPY).
 *
 * \p src and \p dst may overlap.
 *
 * @param src Source bit buffer.
 * @param srcOffset First index to read from \p src.
 * @param srcCount Number of bits to read.
 * @param op Which bit operation to perform.
 * @param dst Destination bit buffer.
 * @param dstOffset First index to modify of \p dst.
 *
 * @return BASE_ERR_OK.
 * @return BASE_ERR_INVALID_PARAMETER.
 */
int bitsBlt(const bitsBuffer_t *src, int srcOffset, int srcCount,
            enum bitsBltOp_t op,
            bitsBuffer_t *dst, int dstOffset);

/* @} */

#endif
