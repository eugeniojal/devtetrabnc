#ifndef LARLIB_INIPARSE_H
#define LARLIB_INIPARSE_H

/**
 * @file ini.h
 * @brief Read/parsing of MS-like INI files.
 *
 * @addtogroup larlib_ini INI file parsing
 * @{
 *
 * \section Rationale
 *
 * INI files are a common and simple way to specify application
 * parameters in a user-readable and editable manner. Even if the
 * application does not use INI files internally to store
 * configuration data, it can use it to read external user parameters
 * and convert to an internal format.
 *
 * \section ini_intro Introduction
 *
 * Read/parsing of MS-like INI files.
 *
 * Only a subset of INI file syntax is accepted:
 * - Comments start with ';' and must be the first non-space char on a
 *   line. Also, comments must be on a line on their own;
 * - Key names may include spaces in the middle;
 * - Values may be literals (\e value) or strings (\e "value"),
 *   literals have spaces before and after trimmed, strings are
 *   "as-is";
 * - Values are optional, they are considered \c NULL if not present;
 * - Keys and Groups are \b not case-sensitive;
 * - Groups are marked using <tt>[group name]</tt> and spaces inside
 *   the <tt>[]</tt> <em>are important</em>.
 *
 * For example, the pair <tt>key name = key value</tt> will be parsed
 * as <tt>"key name"</tt> is assigned the value of <tt>"key
 * value"</tt>, whereas <tt>key name = " key value "</tt> will have a
 * value of <tt>" key value "</tt> (including the spaces, but not the
 * quotes).
 *
 * Example:
 * @code
 * {
 *   int err;
 *   iniElement_t elements[] = {
 *     { "general", "name", handle_general_name, NULL }, // handle "name" key inside "[general]" section
 *     { NULL, "phone", handle_phone, NULL }, // handle "phone" key inside any section
 *     { "generic", NULL, handle_generic_all, NULL }, // handle all keys inside "[generic]" section
 *     { NULL, NULL, NULL, NULL } // list terminator
 *   };
 *
 *   err = iniParse(elements, "myfile.ini");
 *   if (err == BASE_ERR_OK) // success
 *   else // fail
 * }
 * @endcode
 *
 * This is a valid INI file that matches the description above:
 * @code
 * ; example of INI file
 * phone = "handled by handle_phone"
 * [general]
 * name = "handled by handle_general_name"
 * [generic]
 * other = "handled by handle_generic_all"
 * [stuff]
 * phone = "also handled by handle_phone"
 * other = "this entry is ignored"
 * @endcode
 *
 * @attention Remember that the scanning is done from top to bottom,
 *   so a very generic entry near the top (i.e. with both \c key and
 *   \c group as \c NULL) will be matched before any more specific
 *   entries below!
 */

#include <larlib/base.h>

/*
 * Typedef for struct iniElement_t
 */
typedef struct iniElement_t iniElement_t;

/**
 * Descriptor of elements allowed in an INI file.
 * Every element present in the INI file but not found here is ignored.
 */
struct iniElement_t {
    /**
     * Is this entry allowed inside which group?
     *
     * Entries with the same \c key, but different \c group are allowed
     * and will be selected according to the \c group. If \c NULL this
     * entry is allowed inside any group.
     */
    char *group;

    /**
     * Name of element. If \c NULL any key that does not match any \e
     * previous definition will match.
     */
    char *key;

    /**
     * Function that must be called to handle this element. If it
     * returns any value different than zero, processing will
     * terminate.
     * @param ini Pointer to which element in the \c iniElements_t
     *   table was matched.
     * @param group Which group we are inside (\c NULL if none).
     * @param key Key that is being set (value on the left of \c =).
     * @param value Value to be set to, will be \c NULL if the line had no \c =
     * @return Zero on success, any other value interrupts INI file
     *   parsing.
     */
    int (*handler)(iniElement_t *ini, const char *group, const char *key,
                   const char *value);

    /**
     * An optional user-defined parameter that is not touched by the
     * \ref iniParse() function.
     */
    void *param;
};

/**
 * Signature of function used by \ref iniParseLines() to read next line from input file.
 * User-defined functions \e must follow the signature and behavior defined here.
 *
 * Each call to a iniLineReader_t should read the next line of input.  The \p p parameter
 * is the same that is given to \ref iniParseLines() and should be used to identify
 * the source of input.
 *
 * @param p Opaque pointer passed as parameter to \ref iniParseLines()
 * @param line Where to store the line read
 * @param maxline Max number of chars to write to maxline
 * @return \p line on success, \p NULL on error.
 */
typedef char* (*iniLineReader_t)(void *p, char *line, int maxline);

/**
 * Process the given INI file and calls handler functions according to
 * the element table given.
 *
 * For each <tt>key = value</tt> pair found in the input file, the
 * list \p elements is scanned sequentially until a match is found,
 * and the associated \p handler() function is called. See \ref
 * iniElement_t for information on how matching is done, specially
 * when \ref iniElement_t.group and \ref iniElement_t.key are \c NULL.
 *
 * @param elements List of allowed elements. An entry with <tt>handler
 *   = NULL</tt> must terminate the list.
 * @param fileName Name of INI file to parse.
 *
 * @return BASE_ERR_OK if file was processed OK.
 * @return BASE_ERR_DATA_NOT_FOUND if the file \p fileName was not
 *   found.
 * @return The error code returned by one of the handler functions.
 */
int iniParse(iniElement_t elements[], const char *fileName);

/**
 * Process a previously INI file loaded in memory.
 *
 * This call behaves as \ref iniParse(), but, instead of reading the input
 * from a file, directly decodes \p contents, that should be a zero-terminated
 * string with a valid INI file.
 *
 * @param elements List of allowed elements.  An entry with <tt>handler = NULL</tt>
 *      must terminate the list.
 * @param contents Zero-terminated string with the contents of the INI file.
 *
 * @return BASE_ERR_OK if file was processed OK.
 * @return BASE_ERR_INVALID_PARAMETER if \p elements or \p contents are \p NULL
 * @return The error code returned by one of the handler functions.
 */
int iniParseString(iniElement_t elements[], const char *contents);

/**
 * Process the lines of an INI file returned from repeated calls to \p readLine.
 *
 * This call behaves as \ref iniParse(), but, instead of reading the input
 * from a file, interprets whatever is returned by repeated calls to \p readLine.
 *
 * See the documentation of \ref iniLineReader_t for more information on
 * how \p readLine should behave.
 *
 * @param elements List of allowed elements.  An entry with <tt>handler = NULL</tt>
 *      must terminate the list.
 * @param readLine Function called to read each line of input.  Parsing terminates
 *      when \p readLine returns \p NULL.
 * @param p Passed as the first parameter to \p readLine.  Not accessed in any other way.
 *
 * @return BASE_ERR_OK if file was processed OK.
 * @return BASE_ERR_INVALID_PARAMETER if \p elements or \p readLine are \p NULL
 * @return The error code returned by one of the handler functions.
 */
int iniParseLines(iniElement_t elements[], iniLineReader_t readLine, void *p);

/* @} */

#endif
