#ifndef LARLIB_TAB_H
#define LARLIB_TAB_H

/**
 * @file tab.h
 * @brief Table management
 *
 * @addtogroup larlib_tab Table Management
 * @{
 *
 * \section Rationale
 *
 * Simple DBF-like tables are at the core of most applications
 * (transactions done, supported AIDs/keys, etc). Providing a common
 * API not only eases on development (no need to reinvent the wheel)
 * but also makes easier to replace by a version that provides custom
 * characteristics (caching, storage access) without needing to revise
 * the caller application.
 *
 * \section Introduction
 *
 * This module implements a sequential storage of fixed-size records,
 * in other words a \e table. This table is indexed by the position of
 * each \e record, or \e entry, inside the table, with zero being the
 * first one.
 *
 * Each entry also includes a flag indicating if it was marked for
 * deletion or not. New entries are always written to the end of the
 * file, even if deleted entries exist. The user must manually call
 * \ref tabPack() to permanently remove all deleted entries from the
 * file. If \ref tabPack() is never called the file will grow
 * indefinitely.
 *
 * The table header has a field called \e stamp, a 32-bit unsigned
 * integer value that can be modified by the user to control table
 * versioning and state (no functions on this module, except for \ref
 * tabGetStamp() and \ref tabSetStamp(), change this value). Upon
 * creation the table receives a stamp value of zero.
 *
 * This module does no control of table integrity (checksum, for
 * example), if an application needs this level of integrity control,
 * it must do so itself.
 *
 * Some meta-data of the table (as number of elements) is kept in a
 * memory cache and are only written with \ref tabFlush() is called.
 * This is done to (greatly) improve performance of table operations,
 * specially during repeated add. Note that \ref tabFlush() is
 * internally called by \ref tabClose(). See \ref Caching below.
 *
 * \section Multi-Threading
 *
 * No synchronization is done on accesses to the same \ref table_t
 * instance. The caller must guarantee that the same instance is not
 * accessed concurrently. Simultaneous accesses to separate instances
 * are ok.
 *
 * \section Caching
 *
 * An implementation is allowed to cache operations in memory before
 * committing to storage. To guarantee that all changes have been
 * written, the user must call \ref tabFlush().
 *
 * The current version caches the contents of the table header. This
 * is done to avoid re-writing the header when adding or deleting
 * records and changing the stamp value, and causes significant
 * performance improvements on Telium platform.
 */

#include <larlib/base.h>

/**
 * Opaque type of a table instance.
 */
typedef struct table_t table_t;

/**
 * Return codes specific to this module.
 */
enum tabErrors_t {
    TAB_ERR_DELETED = 1     /**< Signal that the target entry is marked as deleted. */
};

/**
 * Open an existing table.
 *
 * @param fname File name of the table to open.
 * @param[out] t On success, store the handle to the open table.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_DATA_NOT_FOUND if \p fname was not found.
 * @return BASE_ERR_ACCESS if cannot access the file \p fname.
 * @return BASE_ERR_INVALID_PARAMETER if \p fname or \p t is invalid
 */
int tabOpen(const char *fname, table_t **t);

/**
 * Create a new table or reset an existing one.
 *
 * @param fname File name of the table to open.
 * @param recSize Size of each record on the table, in bytes. Must be
 *   a positive number. There is no other hard-coded restriction other
 *   than available memory and the limitations of the \c int type.
 * @param[out] t On success, store the handle to the open table.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_DATA_NOT_FOUND if \p fname was not found.
 * @return BASE_ERR_ACCESS if cannot access the file \p fname.
 * @return BASE_ERR_INVALID_PARAMETER if \p fname, \p recSize or \p t is invalid
 */
int tabCreate(const char *fname, int recSize, table_t **t);

/**
 * Close a \ref table_t instance.
 *
 * @param t A valid \ref table_t instance.
 */
void tabClose(table_t *t);

/**
 * Force an update of the table file.
 *
 * The implementation is allowed to cache certain operations for
 * faster throughput, this call forces all pending changes to be
 * actually written to storage.
 *
 * Because of this, there is no guarantee that any change to the table
 * is actually reflected on storage before this call.
 *
 * @param t A valid \ref table_t instance.
 *
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_RESOURCE_PB if could not commit changes.
 */
int tabFlush(table_t *t);

/**
 * Return the record size for a table.
 *
 * @param t A valid \ref table_t instance.
 *
 * @return The size of each entry on this table in bytes.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 */
int tabRecSize(table_t *t);

/**
 * Return the stamp value of the table.
 *
 * This operation is very cheap, the stamp value is cached on memory
 * and no file reads are actually performed.
 *
 * @param t A valid \ref table_t instance.
 * @param[out] stamp Value of the stamp field of this table.
 *
 * @return BASE_ERR_OK if stamp was read correctly.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_INVALID_PARAMETER if \p stamp is an invalid pointer.
 */
int tabGetStamp(table_t *t, uint32_t *stamp);

/**
 * Modify the stamp value of the table.
 *
 * This operation does one write to the header of the table.
 *
 * @param t A valid \ref table_t instance.
 * @param stamp Value of the stamp field to set.
 *
 * @return BASE_ERR_OK if stamp was read correctly.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 */
int tabSetStamp(table_t *t, uint32_t stamp);

/**
 * Read an entry from the table.
 *
 * The code to read a record and detect if it is marked as deleted:
 * @code
 * err = tabGet(table, index, &record);
 * if (err == BASE_ERR_OK) {
 *   // record was read and *NOT* marked as deleted
 * }
 * else if (err == TAB_ERR_DELETED) {
 *   // record was read but *IS* marked as deleted
 * }
 * else {
 *   // failed: could not read record
 * }
 * @endcode
 *
 * This operation is O(1), requiring only one read operation.
 *
 * @param t A valid \ref table_t instance.
 * @param recn Index of the entry to read, in the range <tt>[0,
 *   tabSize(t) - 1]</tt>.
 * @param[out] recp Where to store the entry data. Must have space for
 *   at least <tt>tabRecSize(t)</tt> bytes.
 *
 * @return BASE_ERR_OK if record was read correctly.
 * @return TAB_ERR_DELETED if the record was read, but it is marked as
 *   deleted.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_DATA_NOT_FOUND if \p recn is an invalid index.
 * @return BASE_ERR_INVALID_PARAMETER if \p recp is invalid.
 */
int tabGet(table_t *t, int recn, void *recp);

/**
 * Write or update an entry on the table.
 *
 * This operation is O(1), but requires two writes (one for the record
 * itself, other for the table header).
 *
 * Note that this call can, exceptionally, write an entry to one past
 * the last existing entry of the table, which is equivalent to a call
 * to \ref tabAppend().
 *
 * Calling \ref tabPut() for a deleted entry does \e not change the
 * deleted flag for it.
 *
 * @param t A valid \ref table_t instance.
 * @param recn Index of the entry to write, in the range <tt>[0,
 *   tabSize(t)]</tt>.
 * @param recp Data to write to entry.
 *
 * @return BASE_ERR_OK if record was read correctly.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_DATA_NOT_FOUND if \p recn is an invalid index.
 * @return BASE_ERR_INVALID_PARAMETER if \p recp is invalid.
 */
int tabPut(table_t *t, int recn, const void *recp);

/**
 * Append an entry to the table.
 *
 * This call is equivalent to <tt>tabPut(t, tabSize(t), recp)</tt> and
 * has the same performance characteristics.
 *
 * @param t A valid \ref table_t instance.
 * @param recp Data to write to new entry.
 *
 * @return BASE_ERR_OK if record was read correctly.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_INVALID_PARAMETER if \p recp is invalid.
 *
 * @see tabPut()
 */
int tabAppend(table_t *t, const void *recp);

/**
 * Return the total number of entries on the table.
 *
 * This operation is O(1) and very quick, requiring no storage access.
 *
 * @param t A valid \ref table_t instance.
 *
 * @return The total number of entries on the file, including the ones
 *   marked as deleted.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 */
int tabSize(table_t *t);

/**
 * Return the number of non-deleted entries on the table.
 *
 * This operation is O(1) and very quick, requiring no storage access.
 *
 * @param t A valid \ref table_t instance.
 *
 * @return The number non-deleted entries on the file.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 */
int tabCount(table_t *t);

/**
 * Mark an entry as deleted.
 *
 * This does not effectively remove the entry from the file, only mark
 * it as deleted. The entry can be read with \ref tabGet() and
 * un-deleted with \ref tabUnDelete(), but will not be returned as a
 * result of any iteration or search function.
 *
 * This operation is O(1) and requires at most two writes (one to the
 * \e deleted flag of the record, another to the table header).
 *
 * @param t A valid \ref table_t instance.
 * @param recn Index of record to delete, in the range <tt>[0,
 *   tabSize(t) - 1]</tt>.
 *
 * @return BASE_ERR_OK if record was read correctly.
 * @return BASE_ERR_DATA_NOT_FOUND if \p recn is an invalid index.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 *
 * @see tabUnDelete() tabGet()
 */
int tabDelete(table_t *t, int recn);

/**
 * Remove the deleted mark of an entry.
 *
 * This operation is O(1) and requires at most two writes (one to the
 * \e deleted flag of the record, another to the table header).
 *
 * @param t A valid \ref table_t instance.
 * @param recn Index of record to un-delete, in the range <tt>[0,
 *   tabSize(t) - 1]</tt>.
 *
 * @return BASE_ERR_OK if record was read correctly.
 * @return BASE_ERR_DATA_NOT_FOUND if \p recn is an invalid index.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 *
 * @see tabDelete() tabGet()
 */
int tabUnDelete(table_t *t, int recn);

/**
 * Permanently remove from the table all entries marked as deleted.
 *
 * A side-effect of this call is that the record index of all entries
 * may change (all entries after a deleted entry will "move up").
 *
 * This operation is O(n) on the number of records on the file. Each
 * record of the file is read and copied to a new temporary table,
 * requiring one read and two writes per record. The original file is
 * then replaced by the temporary one.
 *
 * @param fname Name of table file. Should not be currently open.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_DATA_NOT_FOUND if \p fname is not found.
 * @return BASE_ERR_RESOURCE_PB on errors creating the temporary
 *   table or manipulating the file system.
 */
int tabPack(const char *fname);

/**
 * Move the current entry marker to the first (zero'th) element on the table.
 *
 * The generic code to iterate through an entire table, start to
 * finish, is:
 * @code
 * tabGoFirst(t);
 * while (tabGetNext(t, recp) == BASE_ERR_OK) {
 *   // use the value on recp
 * }
 * @endcode
 *
 * @param t A valid \ref table_t instance.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_DATA_NOT_FOUND if \p t is empty.
 *
 * @see tabGetNext()
 */
int tabGoFirst(table_t *t);

/**
 * Read the current entry and move to the next.
 *
 * This call skips all entries marked as deleted.
 *
 * @param t A valid \ref table_t instance.
 * @param[out] recp Contents of the read entry. Not modified if no
 *   entry was read.
 *
 * @return The index of the record read (>= 0) on success.
 * @return BASE_ERR_DATA_NOT_FOUND on \e EOF.
 */
int tabGetNext(table_t *t, void *recp);

/**
 * Read the previous element.
 *
 * This call skips all entries marked as deleted.
 *
 * @param t A valid \ref table_t instance.
 * @param[out] recp Contents of the read entry. Not modified if no
 *   entry was read.
 *
 * @return The index of the record read (>= 0) on success.
 * @return BASE_ERR_DATA_NOT_FOUND on \e EOF.
 */
int tabGetPrev(table_t *t, void *recp);

/**
 * Move to the last element on the table.
 *
 * The generic code to iterate an entire table, end to start, is:
 * @code
 * tabGoLast(t);
 * while (tabGetPrev(t, recp) == BASE_ERR_OK) {
 *   // use the value on recp
 * }
 * @endcode
 *
 * @param t A valid \ref table_t instance.
 *
 * @return BASE_ERR_OK on success.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_DATA_NOT_FOUND if \p t is empty.
 *
 * @see tabGetPrev()
 */
int tabGoLast(table_t *t);

/**
 * Search incrementally the table for an entry.
 *
 * Return the index of the next element \p recp, starting at the
 * current table position, for which <tt>cmpFn(recp, keyp)</tt> is
 * \e zero.
 *
 * This is equivalent to iterating through the table using \ref
 * tabGetNext() until an entry that satisfies the \p cmpFn() condition
 * is found.
 *
 * This operation is O(n).
 *
 * @param t A valid \ref table_t instance.
 * @param[out] recp Contents of the entry found. Modified even if
 *   entry was not found.
 * @param keyp Pointer to the key passed as second parameter to \p
 *   cmpFn.
 * @param cmpFn Comparison function, see the description above.
 *
 * @return The index of the record read (>= 0) on success.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_DATA_NOT_FOUND if no matching record was found.
 * @return BASE_ERR_INVALID_PARAMETER if any required parameter is \c NULL.
 */
int tabFindNext(table_t *t, void *recp, const void *keyp,
                int (*cmpFn)(const void *recp, const void *keyp));

/**
 * Search the table for an element.
 *
 * This is equivalent to a call to \ref tabGoFirst() followed by a
 * call to \ref tabFindNext().
 *
 * This operation is O(n).
 *
 * @param t A valid \ref table_t instance.
 * @param[out] recp Contents of the entry found. Modified even if
 *   entry was not found.
 * @param keyp Pointer to the key passed as second parameter to \p
 *   cmpFn.
 * @param cmpFn Comparison function, see \ref tabFindNext().
 *
 * @return The index of the record read (>= 0) on success.
 * @return BASE_ERR_INVALID_HANDLE if \p t is invalid.
 * @return BASE_ERR_DATA_NOT_FOUND if no matching record was found.
 * @return BASE_ERR_INVALID_PARAMETER if any required parameter is \c NULL.
 *
 * @see tabFindNext()
 * @see tabGoFirst()
 */
int tabFind(table_t *t, void *recp, const void *keyp,
            int (*cmpFn)(const void *recp, const void *keyp));

/* @} */

#endif
