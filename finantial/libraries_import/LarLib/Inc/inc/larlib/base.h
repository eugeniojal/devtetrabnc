#ifndef LARLIB_BASE_H
#define LARLIB_BASE_H

/**
 * @file base.h
 * @brief Larlib basic definitions
 *
 * @mainpage
 *
 * \section Introduction
 *
 * The <em>Latin America Region Library</em> -- \c larlib -- is a
 * collection of modules to help the development of applications on
 * embedded terminals. Some restriction on which modules are included
 * in this library apply:
 * - No modules with user-interface: only algorithmic operations, no
 *   functions to display, print or accept user input (An exception is
 *   the \ref larlib_log module that generates traces and provides other
 *   useful tools, as an assert);
 * - Only generally applicable functionality: stuff that is limited to
 *   a single platform probably belongs on a separate library. All the
 *   modules here should be easily ported between platforms;
 * - Decent documentation: all modules include a doxygen-like
 *   documentation. It may not be highly-complete or top-grade, but it
 *   must define the interface and give a good idea of how to use;
 * - API naming convention: all functions and defines should follow
 *   certain naming conventions to reduce the risk of name-clashing;
 * - Testing: all libraries include a set of unit-tests (available
 *   separately) and must pass all tests before release
 *
 * The library is modular, and the application may include only the
 * headers for the modules it is actually using. Optionally it may
 * include the file <tt>larlib/all.h</tt> to include all the modules
 * that come with a given larlib version.
 *
 * See the \e modules page for the list of supported modules and how
 * to use each. The \ref larlib_base module is a smaller, generic,
 * module designed to provide a base abstraction to be used by the
 * other modules (and the user code, if he wants so!).
 *
 * \section Tracing
 *
 * All modules of this library use \ref larlib_log to generate traces.
 * See the enumeration \ref logReservedChannels_t for a list of which
 * logging channels are used by which module.
 *
 * Optionally this library can be compiled to disable \e all logging and
 * all assertions, to do this the macro \c NDEBUG must be defined on
 * compilation time <em>of the library</em>. Remember that defining this
 * macro on your application when you use a pre-compiled version of this
 * library will not change this.
 *
 * \section modules Supported Modules
 *
 * The following modules are part of this release:
 * <table>
 * <tr><th>Module Name</th><th>Header</th><th>Description</th></tr>
 * <tr>
 *  <td> \ref larlib_base </td>
 *  <td> larlib/base.h </td>
 *  <td> Basic definitions </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_bits </td>
 *  <td> larlib/bits.h </td>
 *  <td> Bit operations </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_conv </td>
 *  <td> larlib/conv.h </td>
 *  <td> Numeric conversion routines </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_date </td>
 *  <td> larlib/date.h </td>
 *  <td> Date and time </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_deque </td>
 *  <td> larlib/deque.h </td>
 *  <td> Double-ended queue </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_format </td>
 *  <td> larlib/format.h </td>
 *  <td> Buffer formatting </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_fs </td>
 *  <td> larlib/fs.h </td>
 *  <td> File System </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_ini </td>
 *  <td> larlib/ini.h </td>
 *  <td> INI file parsing </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_log </td>
 *  <td> larlib/log.h </td>
 *  <td> Logging </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_mem </td>
 *  <td> larlib/mem.h </td>
 *  <td> Memory Management </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_step </td>
 *  <td> larlib/step.h </td>
 *  <td> Step-Machine </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_tab </td>
 *  <td> larlib/tab.h </td>
 *  <td> Table Management </td>
 * </tr>
 * <tr>
 *  <td> \ref larlib_task </td>
 *  <td> larlib/task.h </td>
 *  <td> Multi-Threading </td>
 * </tr>
 * </table>
 *
 * @addtogroup larlib_base Basic Definitions
 * @{
 *
 * \section Rationale
 *
 * Each platform has its own definition of types and error codes. We
 * need a platform-independent basis to use for our own libraries. As an
 * extra we try to reuse the types from C99's <tt>\<stdint.h></tt>.
 *
 * \section Introduction
 *
 * This module includes basic definitions, types and functions that
 * should be used as the environment for other modules.
 */

#ifdef __TELIUM3__
#define HAS_STDINT
#endif

/*
 * If not available, redefine some C99's stdint types.
 */
#if !defined(HAVE_STDINT) && !defined(HAS_STDINT)

/** Signed 8-bit integer. */
typedef char int8_t;

/** Signed 16-bit integer. */
typedef short int16_t;

/** Signed 32-bit integer. */
typedef int int32_t;

/** Signed 64-bit integer. */
typedef long long int64_t;

/** Unsigned 8-bit integer. */
typedef unsigned char uint8_t;

/** Unsigned 16-bit integer. */
typedef unsigned short uint16_t;

/** Unsigned 32-bit integer. */
typedef unsigned int uint32_t;

/** Unsigned 64-bit integer. */
typedef unsigned long long uint64_t;

#else
#include <stdint.h>
#endif

/* -------------------------------------------------------------------------- */

/**
 * Common error values.
 */
enum baseErrors_t {
    BASE_ERR_OK = 0, /**< No error */
    BASE_ERR_INVALID_PARAMETER = -1, /**< Function was given an invalid parameter */
    BASE_ERR_DATA_NOT_FOUND = -2, /**< Requested data was not found */
    BASE_ERR_INVALID_HANDLE = -3, /**< Given handle is invalid */
    BASE_ERR_RESOURCE_PB = -4, /**< Resource allocation problem (usually memory) */
    BASE_ERR_OVERFLOW = -5, /**< Either parameter is too large, or result buffer too small */
    BASE_ERR_ACCESS = -6, /**< Access/permission failure */
    BASE_ERR_CANCEL = -7, /**< Canceled by external process (user?) */
    BASE_ERR_TIMEOUT = -8 /**< Time-limited function has not completed in the requested time. */
};

/* -------------------------------------------------------------------------- */

/*
 * Helpful defines.
 */

/** Return the largest of two elements (as defined by the comparison <tt>\<</tt>).
 * @hideinitializer
 */
#define BASE_MAX(a,b) (((a) < (b)) ? (b) : (a))

/** Return the smallest of two elements (as defined by the comparison <tt>\<</tt>).
 * @hideinitializer
 */
#define BASE_MIN(a,b) (((a) < (b)) ? (a) : (b))

/** Return the count of a fixed-size array.
 * This will \b not work with pointers and other arrays, for example:
 * @code
 * myStruct_t v[10];
 * myStruct_t *pv = v;
 * int nv = BASE_LENGTH(v); // works
 * int npv = BASE_LENGTH(pv); // does NOT work
 * @endcode
 * @hideinitializer
 */
#define BASE_LENGTH(v)  (sizeof(v) / sizeof((v)[0]))

/* -------------------------------------------------------------------------- */

/*
 * Library version information
 */

/** Version of this LarLib as a 2-byte BCD number.
 * For example, \c 0x0123 indicates version \e 1.23.
 *
 * This allows both the application code and the (human) reader to know which version
 * of LarLib this header file is associated with.
 */
#define LARLIB_VERSION      (0x0114)

/**
 * Return a string with the version and revision of LarLib library.
 *
 * The returned string always follows the format <tt>"xx.yy $Revision: nnn $"</tt>, where:
 * - <tt>xx.yy</tt> are the major and minor versions (from \ref LARLIB_VERSION);
 * - <tt>nnn</tt> is the SVN revision of this build (the length in digits is variable, do
 *      not always consider to be 3!).
 *
 * For example <tt>"01.23 $Revision: 111 $"</tt>.
 *
 * @note If the version value returned by this function does not match the value of
 *  \ref LARLIB_VERSION, there is a mismatch between header and library.
 *
 * @return The library version and SVN revision of LarLib binary library file.
 */
const char *larlibGetVersion(void);

/* @} */

#endif
