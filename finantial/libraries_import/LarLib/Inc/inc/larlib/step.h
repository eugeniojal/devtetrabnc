#ifndef LARLIB_STEP_H
#define LARLIB_STEP_H

/**
 * @file step.h
 * @brief State-machine for transaction processing.
 *
 * @addtogroup larlib_step State-machine
 * @{
 *
 * @section Rationale
 *
 * Most of the POS financial applications have the same overall structure:
 * each transaction is broken into distinct "steps", each one should be
 * executed in turn, and some steps may be shared between different
 * transactions.
 *
 * @section Introduction
 *
 * This module provides a framework to implement a sub-set of state-machines,
 * here called <b>step-machines</b>. A step-machine is formed by a sequence of
 * states (steps), which are executed in sequence, except explicit change of
 * execution flow (such as a \e goto).
 *
 * Each step is defined by the contents of a \ref stepAction_t structure. Each
 * action has an associated \c label, a numeric identifier that is useful if
 * this action must be target for a goto; The function that executes the actual
 * behavior associated with this action; And a opaque parameter that is meant
 * to be used by the associated function.
 *
 * An action may have a \c NULL function, in this case it is considered that
 * the function has returned \ref BASE_ERR_OK. This may be useful to insert
 * "dummy" actions with only a valid label field, meant as goto targets.
 *
 * Unless a \ref stepGotoLabel() call is made, execution follows the steps
 * in the order that they are given to \ref stepCreate(), this is considered
 * the \e default execution sequence.
 *
 * @section Example
 *
 * This example is a loop that prints all numbers in the range [0..9]. Even though
 * this is an unrealistic use of the step-machine module, it demonstrates all its
 * features.
 *
 * @code
 * enum {
 *   // label for 'print' step
 *   LABEL_PRINT = 1
 * };
 *
 * // print the int pointed by c
 * static int print(step_t *s, stepAction_t *a, void *c)
 * {
 *   printf(a->param, * (int *) c);
 *   return BASE_ERR_OK;
 * }
 *
 * // increment the value of the integer pointed by c
 * static int incr(step_t *s, stepAction_t *a, void *c)
 * {
 *   int *n = (int *) c;
 *   (*n) ++;
 *   return BASE_ERR_OK;
 * }
 *
 * // goto LABEL_PRINT if the integer pointed by 'c' is < 10
 * static int loop(step_t *s, stepAction_t *a, void *c)
 * {
 *   int *n = (int *) c;
 *   if (*n < 10) {
 *     // loop back to 'print' action
 *     return stepGotoLabel(s, LABEL_PRINT);
 *   }
 *   else {
 *     // continue to next step in default sequence
 *     return BASE_ERR_OK;
 *   }
 * }
 *
 * static int stepExample(void)
 * {
 *   stepAction_t actions[] = {
 *     LABEL_PRINT, &print, "The number is %d\n",
 *     0,           &incr,  NULL,
 *     0,           &loop,  NULL,
 *   };
 *
 *   step_t *s = stepCreate(actions, BASE_LENGTH(actions));
 *   if (s) {
 *     int n = 0;
 *     int err = stepRun(s, &n);
 *     stepDestroy(s);
 *     return err;
 *   }
 *   else {
 *     return BASE_ERR_RESOURCE_PB;
 *   }
 * }
 * @endcode
 */

#include <larlib/base.h>

/**
 * Opaque type of a handle to a step-machine.
 */
typedef struct step_t step_t;

/*
 * Forward declaration of a step action
 */
typedef struct stepAction_t stepAction_t;

/**
 * Type signature of the function associated with each action on the
 * step machine.
 *
 * @param s Handle to controlling step machine.
 * @param a Pointer to action associated with this step.
 * @param c Overall context parameter passed as parameter to \ref stepRun().
 *
 * @return BASE_ERR_OK on success. Any other value will stop the execution
 *  of the step machine immediately and return this value to the caller
 *  of \ref stepRun().
 */
typedef int (*stepFunction_t)(step_t *s, stepAction_t *a, void *c);

/**
 * Each action in the step machine.
 */
struct stepAction_t {
    int label;              /**< Label associated with this action */
    stepFunction_t fun;     /**< Function that executes this action */
    void *param;            /**< Place-holder for parameter to \c fun */
};

/**
 * Create a new step machine.
 *
 * The list of actions defined by \p actions will be copied to an
 * internal, dynamically allocated, array.
 *
 * This makes safe to create a step from an array on the stack and
 * returning this value:
 * @code
 * step_t *CreateMyStep(void)
 * {
 *   stepAction_t actions[] = {
 *     1, GetAmount,     "###.###.##0,00",
 *     0, CheckAmount,   NULL,
 *     2, GetCardData,   "Passe ou insira o cartao",
 *     0, CheckCardData, NULL,
 *     3, DoTransaction, "Pedindo autorizacao",
 *     0, CheckResult,   NULL,
 *     4, TransFail,     "Transacao Falhou",
 *     5, TransOk,       "Transacao foi OK"
 *   }
 *   return stepCreate(actions, BASE_LENGTH(actions));
 * }
 * @endcode
 *
 * @param actions List of actions associated with this step machine.
 * @param nactions Number of elements in \c actions.
 *
 * @return A pointer to a new step-machine or \c NULL on error.
 * @see stepRun
 * @see stepDestroy
 */
step_t *stepCreate(stepAction_t *actions, int nactions);

/**
 * Execute the step machine.
 *
 * @param s Handle to step-machine to run.
 * @param c Opaque value that is passed to the action functions.
 *
 * @return BASE_ERR_OK if the machine was executed to the end.
 * @return BASE_ERR_INVALID_HANDLE if \p s is invalid.
 * @return The return from the first step that didn't return BASE_ERR_OK.
 * @see stepGotoLabel
 */
int stepRun(step_t *s, void *c);

/**
 * Change the next action to be executed.
 *
 * Note that this call only marks an indicator of "next action to run", it does
 * not cause the flow to change immediately. Multiple calls will override each
 * other, the last one prevailing.
 *
 * If more than one action of the given step-machine have the same label, the
 * first one (the one with the smallest index) is selected.
 *
 * One common idiom for using this call is associated with the \c return keyword:
 * @code
 * int myActionFunction(step_t *s, stepAction_t *a, void *c)
 * {
 *   // ... processing ...
 *   return stepGotoLabel(s, LABEL_MY_OTHER_ACTION);
 * }
 * @endcode
 * This has the side-effect of aborting the execution if \c LABEL_MY_OTHER_ACTION
 * label is not found in the step-machine \c s.
 *
 * @param s Handle to step-machine.
 * @param label Label of target action, see \ref stepAction_t.label.
 *
 * @return BASE_ERR_OK if next action to execute was changed.
 * @return BASE_ERR_DATA_NOT_FOUND if \p label was not found.
 * @see stepRun
 */
int stepGotoLabel(step_t *s, int label);

/**
 * Release the resources associated with a step-machine.
 * @param s Handle to step-machine.
 */
void stepDestroy(step_t *s);

/* @} */

#endif
