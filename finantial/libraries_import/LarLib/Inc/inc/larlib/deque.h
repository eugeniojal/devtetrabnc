#ifndef LARLIB_DEQUE_H
#define LARLIB_DEQUE_H

/**
 * @file deque.h
 * @brief A double-ended queue of <tt>void*</tt> elements.
 *
 * @addtogroup larlib_deque Double-ended queue
 * @{
 *
 * \section Introduction
 *
 * This module implements a double-ended queue (\e deque) of opaque
 * elements that provides \e amortized O(1) push and pop
 * operations on both ends (but not in the middle) and random item
 * access (\e get and \e set operations).
 *
 * Note that the implementation treats elements as opaque pointers,
 * their memory is not automatically released when removed from the
 * queue, the user must do that.
 */

#include <larlib/base.h>

/**
 * Opaque type for a double-ended queue.
 * All operations on deques receive a parameter of this type.
 */
typedef struct deque_t deque_t;

/**
 * Create a new deque with space reserved for at least \p sizeHint elements.
 *
 * Creating a deque with \p sizeHint close to the maximum number of
 * elements that will ever be inserted gives the best performance
 * characteristics, since no re-allocation will ever be necessary.
 *
 * @param sizeHint A hint on the maximum size of elements that will
 *   ever be inserted on the deque. The implementation may use a value
 *   larger than this on practice.
 *
 * @return A new \ref deque_t instance, or \c NULL on error.
 */
deque_t *dequeCreate(int sizeHint);

/**
 * Destroy a deque created with \ref dequeCreate().
 *
 * @param d deque to be destroyed. \c NULL is accepted, and ignored.
 */
void dequeDestroy(deque_t *d);

/**
 * Add an element to the "back" of a deque.
 *
 * This is equivalent as inserting an element at position <tt>dequeLength(d)</tt>.
 *
 * @note If there is not enough space on the \c deque_t for a new element it
 *  will be resized, which is an O(n) operation. Otherwise, this is an
 *  O(1) operation.
 *
 * @param d A valid deque_t instance.
 * @param p Element to be added.
 *
 * @return BASE_ERR_OK if the element was inserted.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 * @return BASE_ERR_RESOURCE_PB if could not expand deque to hold the new element.
 */
int dequePushBack(deque_t *d, void *p);

/**
 * Remove the last element of a queue.
 *
 * @param d A valid deque_t instance.
 *
 * @return BASE_ERR_OK if the element was removed.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 * @return BASE_ERR_DATA_NOT_FOUND if the deque is empty.
 */
int dequePopBack(deque_t *d);

/**
 * Add an element on the "back" of a deque.
 *
 * This is equivalent as inserting an element at position <tt>0</tt>.
 *
 * @note If there is not enough space on the \c deque_t for a new element it
 *  will be resized, which is an O(n) operation. Otherwise, this is an
 *  O(1) operation.
 *
 * @param d A valid deque_t instance.
 * @param p Element to be added.
 *
 * @return BASE_ERR_OK if the element was inserted.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 * @return BASE_ERR_RESOURCE_PB if could not expand deque to hold the new element.
 */
int dequePushFront(deque_t *d, void *p);

/**
 * Remove the last element of a queue.
 *
 * @param d A valid deque_t instance.
 *
 * @return BASE_ERR_OK if the element was removed.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 * @return BASE_ERR_DATA_NOT_FOUND if the deque is empty.
 */
int dequePopFront(deque_t *d);

/**
 * Return the number of elements inside a deque.
 *
 * Note that this value is not related to the actual memory occupation of a
 * deque. For example, a large \p sizeHint parameter to \ref dequeCreate()
 * will reserve much memory to the deque, but \ref dequeLength() will return
 * zero.
 *
 * @param d A valid deque_t instance.
 *
 * @return The number of elements inside the deque <tt> >= 0 </tt>.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 */
int dequeLength(const deque_t *d);

/**
 * Return the i'th element on a queue.
 *
 * @note An implementation may check boundaries and cause an assertion
 *   error on out of bounds access in debug builds. This check may be
 *   omitted in non-debug builds. In this case accessing an element
 *   out of bounds is undefined behavior.
 *
 * @note Calling with an invalid \p d parameter may cause an assertion
 *   error on debug builds or undefined behavior on non-debug builds.
 *
 * @pre (d != NULL) && (0 <= i < dequeLength(d))
 *
 * @param d A valid deque_t instance.
 * @param i Index of the element to be returned.
 *
 * @return The element at the i'th position on the deque \p d.
 */
void *dequeGet(const deque_t *d, int i);

/**
 * Return the first element on a deque.
 *
 * This call is equivalent to <tt>dequeGet(d, 0)</tt>. The same bound
 * checking notes that apply to \ref dequeGet() are valid here.
 *
 * @note Calling with an invalid \p d parameter may cause an assertion
 *   error on debug builds or undefined behavior on non-debug builds.
 *
 * @param d A valid deque_t instance.
 *
 * @return The element at the first position on the deque \p d.
 */
void *dequeGetFront(const deque_t *d);

/**
 * Return the last element on a deque.
 *
 * This call is equivalent to <tt>dequeGet(d, dequeLength(d) -
 * 1)</tt>. The same bound checking notes that apply to \ref
 * dequeGet() are valid here.
 *
 * @note Calling with an invalid \p d parameter may cause an assertion
 *   error on debug builds or undefined behavior on non-debug builds.
 *
 * @param d A valid deque_t instance.
 *
 * @return The element at the last position on the deque \p d.
 */
void *dequeGetBack(const deque_t *d);

/**
 * Change the value of the i'th element on a deque.
 *
 * @note An implementation may check boundaries and cause an assertion
 *   error on access out of bounds in debug builds. This check may be
 *   omitted in non-debug builds. In this case accessing an element
 *   out of bounds is undefined behavior.
 *
 * @note Calling with an invalid \p d parameter may cause an assertion
 *   error on debug builds or undefined behavior on non-debug builds.
 *
 * @param d A valid deque_t instance.
 * @param i Index of element to set.
 * @param p Value to be written.
 *
 * @return The value of the parameter \p p.
 */
void *dequeSet(deque_t *d, int i, void *p);

/**
 * Insert an element at any position inside the deque.
 *
 * Even without any reallocation this is a O(n) operation, as other elements
 * may need to be moved. Operations on either ends of the deque will behave
 * as if \ref dequePushFront() and \ref dequePushBack() were called.
 *
 * @note If there is not enough space on the \c deque_t for a new
 *   element it will be resized, which is also an O(n)
 *   operation.
 *
 * @param d A valid deque_t instance.
 * @param i Index where to insert element. If this operation succeeds,
 *   <tt>dequeGet(d, i) == p</tt>. <tt>i == dequeLength(d)</tt> is a
 *   valid index.
 * @param p Element to be added.
 *
 * @return BASE_ERR_OK if the element was inserted.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 * @return BASE_ERR_RESOURCE_PB if could not expand deque to hold the new element.
 * @return BASE_ERR_DATA_NOT_FOUND if the index is out of range.
 */
int dequeInsert(deque_t *d, int i, void *p);

/**
 * Remove an element at any position inside the deque.
 *
 * @note A check is made if the element is the first or last on the
 *   deque, and dequePopFront() or dequePopBack() are called on those
 *   cases, operations on any other index are O(n).
 *
 * @param d A valid deque_t instance.
 * @param i Index of element to be removed.
 *
 * @return BASE_ERR_OK if the element was removed.
 * @return BASE_ERR_INVALID_HANDLE if \p d is an invalid queue_t.
 * @return BASE_ERR_DATA_NOT_FOUND if the index is out of range.
 */
int dequeRemove(deque_t *d, int i);

/* @} */

#endif
