/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Batch.h
 * Description:		Implement functions to manage transaction batch
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_BATCH
#define _H_BATCH

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define BATCH_RET_OK						0
#define BATCH_RET_NOK						-1
#define BATCH_RET_CANNOT_FIND_RECORD		-2
#define BATCH_RET_EMPTY_FILE				-3

#define L_PAN				21
#define LZ_PAN				L_PAN+1
#define L_EXP_DATE			4
#define LZ_EXP_DATE			L_EXP_DATE+1
#define L_SERVICE_CODE		3
#define LZ_SERVICE_CODE		L_SERVICE_CODE+1
#define L_RESPONSE_CODE		2
#define LZ_RESPONSE_CODE	L_SERVICE_CODE+1
#define L_AUTH_NUMBER		6
#define LZ_AUTH_NUMBER		L_AUTH_NUMBER+1
#define L_RRN				12
#define	LZ_RRN				L_RRN+1
#define L_EMV_1GEN_TAGS		500
#define LZ_EMV_1GEN_TAGS	L_EMV_1GEN_TAGS+1
//+ CLESS @mamata Dec 16, 2014
#define L_EMV_PRINT_TAGS	200
#define IDNUMBER        	32
#define ISSUER_SCRIPT_LEN   50
#define TRACK2_LEN          37
//d #define L_EMV_PRINT_TAGS	100
//- CLESS
#define LZ_EMV_PRINT_TAGS	L_EMV_PRINT_TAGS+1


typedef struct tagBATCH
{
	RECORD_HEADER Header;
	uint16		TranID;
	uint16		TranStatus;
	uint16		TranName;
	uint16		TranOptions;
	uint32		Invoice;
	uint32		MerchantGroupID;
	uint32		MerchantID;
	uint32 		RangeID;
	uint32		HostID;
	uint8		PAN[LZ_PAN];
	uint8		ExpirationDate[LZ_EXP_DATE];
	uint8		ServiceCode[LZ_SERVICE_CODE];
	uint8		Currency;
	uint8		EntryMode;
	uint8		Fallback;
	amount		BaseAmount;
	amount		TaxAmount;
	amount		TipAmount;
	amount		OriginalTotal;
	uint32		STAN;
	DATE		DateTime;
	uint8		HostResponseCode[LZ_RESPONSE_CODE];
	uint8		HostAuthNumber[LZ_AUTH_NUMBER];
	uint8 		HostRRN[LZ_RRN];
	uint8		EMV_1GEN_TAGS[LZ_EMV_1GEN_TAGS];
	uint16		EMV_1GEN_TAGS_LEN;
	uint8		EMV_PRINT_TAGS[LZ_EMV_PRINT_TAGS];
	uint16		EMV_PRINT_TAGS_LEN;
	uint8		EWL_TAG_ISSUER_SCRIPT[ISSUER_SCRIPT_LEN];
	uint16		EWL_TAG_ISSUER_SCRIPT_LEN;
	//+ EWL01 @jbotero 5/02/2016
	uint8 		PinOnReq;
	uint8 		PinOffReq;
	uint8 		SignReq;
	//- EWL01
	uint32 		Server;
//	uint32 		NBatch;
	char      	Idnumber[IDNUMBER];
	int			VoidOriginalRecord;
	uint8 		AccountType;
	//EUGENIO
	uint8		TRACK2[TRACK2_LEN];
	DATE		DateTimeReverse;
	uint32		TranBank;
	uint8		EntryModeOriginal;
	char		APP_LBL[IDNUMBER];
}PACKED BATCH;
// table length
#define L_BATCH sizeof(BATCH)

//+ BATCH01 @mamata Aug 15, 2014
typedef struct tagBatchInfo
{
	int32	MerchantID;
	char	TID[LZ_MERCH_TID];
	char	MID[LZ_MERCH_LID];
	uint32	BatchNumber;
}PACKED BATCH_INFO;
//- BATCH01

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Batch_InitTables
 * Description:		Init tables for Merchants batch
 * Parameters:
 *  - none
 * Return:
 *  - BATCH_RET_OK - all batch table are OK
 *  - BATCH_RET_NOK - cannot create a batch table
 * Notes:
 */
int Batch_InitTables(void);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Add_Transaction
 * Description:		Add transaction to batch file
 * Parameters:
 *  - Tree - tree of transaction
 * Return:
 *  BATCH_RET_OK - transaction added to batch file
 *  BATCH_RET_NOK - cannot add transaction to batch file
 * Notes:
 */
int Batch_Add_Transaction(TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Modify_Transaction
 * Description:		Modify transaction in batch file
 * Parameters:
 *  - Tree - tree of transaction
 * Return:
 *  BATCH_RET_OK - transaction modified
 *  BATCH_RET_NOK - cannot add modify transaction
 *  BATCH_RET_CANNOT_FIND_RECORD - cannot find transaction
 * Notes:
 */
int Batch_Modify_Transaction(TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Delete_Transaction
 * Description:		Modify transaction in batch file
 * Parameters:
 *  - Tree - tree of transaction
 * Return:
 *  BATCH_RET_OK - transaction deleted
 *  BATCH_RET_NOK - cannot delete transaction
 *  BATCH_RET_CANNOT_FIND_RECORD - cannot find transaction
 * Notes:
 */
int Batch_Delete_Transaction(TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Get_Last_Transaction
 * Description:		Get last transaction of batch
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - uint8 *BatchName - name of batch file
 * Return:
 *  BATCH_RET_OK - last transaction ok
 *  BATCH_RET_NOK - cannot open last transaction
 *  BATCH_RET_EMPTY_FILE - no transactions in batch file
 * Notes:
 */
int Batch_Get_Last_Transaction(TLV_TREE_NODE Tree, uint8 *BatchName);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Get_Transaction_Data
 * Description:		get transaction data from BATCH record and put in Tree
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - BATCH BatchRecord - source of transaction data
 * Return:
 *  - BATCH_RET_OK
 * Notes:
 */
int Batch_Get_Transaction_Data(TLV_TREE_NODE Tree, BATCH *BatchRecord);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Fill_Record
 * Description:		Fill BATCH record from Tree
 * Parameters:
 *  - TLV_TREE_NODE Tree - source of transaction data
 *  - BATCH BatchRecord - where to put transaction data
 * Return:
 *  BATCH_RET_OK
 *  BATCH_RET_NOK - a mandatory field is missing
 * Notes:
 */
int Batch_Fill_Record(TLV_TREE_NODE Tree, BATCH *BatchRecord);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_FindTransaction
 * Description:		find transaction in all batch files
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - uint32 Reference - transaction reference
 * Return:
 *  BATCH_RET_OK
 *  BATCH_RET_CANNOT_FIND_RECORD
 * Notes:
 */
int Batch_FindTransaction(TLV_TREE_NODE Tree, uint32 Reference);

/* --------------------------------------------------------------------------
 * Function Name:	Batch_FindServer
 * Description:		find Server in all batch files
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put Server data
 *  - uint32 Mesero - id Mesero
 * Return:
 *  BATCH_RET_OK
 *  BATCH_RET_CANNOT_FIND_RECORD
 * Notes:
 */
int Batch_FindServer(TLV_TREE_NODE Tree, uint32 Mesero);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_DeleteSettled
 * Description:		Delete settled batch files
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Batch_DeleteSettled(void);


/* --------------------------------------------------------------------------
 * Function Name:	Batch_DeleteAllBatch
 * Description:		Delete all batch files
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Batch_DeleteAllBatch(void);


//+ BATCH01 @mamata Aug 15, 2014
/* --------------------------------------------------------------------------
 * Function Name:	SaveBatchsInformation
 * Description:		Save current batchs information in tlvtree
 * Parameters:
 *  - TLV_TREE_NODE BatchsInfo - where to put data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SaveBatchsInformation(TLV_TREE_NODE BatchsInfo);


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentBatchNumber
 * Description:		get the current batch number of specific merchant
 * Parameters:
 *  - char *TID
 *  - char*MID
 *  - TLV_TREE_NODE BatchsInfo
 * Return:
 * - (0) - cannot fond merchant
 * other - current batch number
 * Notes:
 */
uint32 GetCurrentBatchNumber(char *TID, char*MID, TLV_TREE_NODE BatchsInfo);
//- BATCH01


int Batch_FindTransactionByRecordID(TLV_TREE_NODE Tree, int Record);

#endif //_H_BATCH
