/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		CoreDefines.h
 * Description:		Custom defines for iPOSApp
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_CUSTOM_DEFINES
#define _H_CUSTOM_DEFINES
#define COMERCIO					1
#define RESTAURANT					2
#define RENTACAR					3
#define CLINICAS					4
#define HOTELES 					5

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * TYPES
 * ***************************************************************************/
typedef enum TE_EntryMode
{
	ENTRY_MODE_MANUAL = 0x00,
	ENTRY_MODE_SWIPE = 0x01,
	ENTRY_MODE_CHIP = 0x02,
	ENTRY_MODE_CLESS_SWIPE = 0x03,
	ENTRY_MODE_CLESS_CHIP = 0x04,

	//+ ISOMAIN01 @mamata Aug 14, 2014
	ENTRY_MODE_NONE = 0x05,
	//- ISOMAIN01
}LIST_ENTRY_MODE;

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define EWL_PAR_APPTYPE				0xC65A
#define Technician_Password "142536"
#define VERSION_APP         "INSOFTBNC"
#define LTAG_INT8					1
#define	LTAG_INT16					2
#define LTAG_INT32					4
#define LTAG_INT64					8
#define LTAG_UINT8					1
#define	LTAG_UINT16					2
#define LTAG_UINT32					4
#define LTAG_UINT64					8
#define LTAG_AMOUNT					8
#define LTAG_DATE_TIME				sizeof(DATE)

// iPOSApp Transaction TAGS
#define TAG_TRAN_ID					0x01000001		// Transaction ID (uint16)
#define TAG_TRAN_NAME				0x01000002		// Message ID of transaction name (uint16)
#define TAG_TRAN_OPTIOS				0x01000003		// Transaction option flags	(uint16)
#define TAG_MERCH_GROUP_ID			0x01000004		// Merchant Group ID (uint32)
#define TAG_PAN						0x01000005		// PAN number (string)
#define TAG_TRACK1					0x01000006		// Track 1 (string)
#define TAG_TRACK2					0x01000007		// Track 2 (string)
#define TAG_EXP_DATE				0x01000008		// Expiration date (string) YYMM
#define TAG_SERVICE_CODE			0x01000009		// Card Service Code (string)
#define TAG_CARDHOLDER_NAME			0x01000010		// Card Holder Name (string)
#define TAG_CURRENCY				0x01000011		// Currency code (uint8)
#define TAG_MERCHANT_ID				0x01000012		// Merchant ID (uint32)
#define TAG_RANGE_ID				0x01000013		// Range ID (uint32)
#define TAG_ENTRY_MODE				0x01000014		// Card entry mode (uint8) LIST_ENTRY_MODE
#define TAG_CVV2_TYPE				0x01000015		// CVV2 type (uint8)
#define TAG_CVV2					0x01000016		// CVV2 (string)
#define TAG_FALLBACK				0x01000017		// Fallback(uint8)
#define TAG_FORCE_INSERT			0x01000018		// Force insert in card read (uint8)
#define TAG_BASE_AMOUNT				0x01000019		// Base Amount (amount)
#define TAG_TAX_AMOUNT				0x01000020		// Tax Amount (amount)
#define TAG_TIP_AMOUNT				0x01000021		// Tip Amount (amount)
#define TAG_HOST_ID					0x01000022		// Host ID (uint32)
#define TAG_INVOICE					0x01000023		// Invoice number (uint32)
#define TAG_STAN					0x01000024		// System Trace Audit Number (uint32)
#define TAG_DATE_TIME				0x01000025		// Transaction date time (DATE)
#define TAG_HOST_RESPONSE_CODE		0x01000026		// Host Response Code
#define TAG_HOST_AUTH_NUMBER		0x01000027		// Host authorization number (string)
#define TAG_HOST_RRN				0x01000028		// Host Retrieval Reference Number (string)
#define TAG_TRAN_STATUS				0x01000029		// Transaction status (uint16) bitmap
#define TAG_ORIGINAL_TOTAL			0x01000030		// Original total amount (amount)
#define TAG_PINBLOCK				0x01000031		// Pin Block (uint8 * 8)
#define TAG_DUKPT_KSN				0x01000032		// DUKPT KSN
#define TAG_PREDIAL					0x01000033		// Predial performed (uint8)
#define TAG_BATCH_FILE_INDEX		0x01000034		// Batch file index
#define TAG_BATCH_FILE_NAME			0x01000035		// Batch file name
#define TAG_BATCH_NUMBER			0x01000036		// Batch number
#define TAG_APPROVED				0x01000037		// Transaction approved (uint8)
#define TAG_RESPONSE_CODE_TEXT		0x01000038		// ID of message of response code
#define TAG_ACCOUNT_TYPE            0x01000039      // TypeCard (uint16)
#define	TAG_EMV_APP_LBL				0x01000040		// EMV Display Application label
#define TAG_EMV_1GEN_TAGS			0x01000041		// EMV tags to sento to host
#define TAG_EMV_HOST_TAGS			0x01000042		// EMV tags to sento to host
#define TAG_EMV_PRINT_TAGS			0x01000043		// EMV tags to print
#define TAG_EMV_TRAN_STATUS			0x01000044		// EMV tags Transaction status
//+ ISOMAIN01 @mamata Aug 14, 2014
#define TAG_ORIGINAL_STAN			0x01000045		// Original STAN
#define TAG_ADD_AMOUNT				0x01000046		// Additional amount from host
//- ISOMAIN01
//+ CLESS @mamata Dec 10, 2014
#define TAG_CASH_AMOUNT				0x01000047		// Cash amount (amount)
#define TAG_CLESS_RESULT			0x01000048		// // cless result (uint8)
//- CLESS
//+ CARCAAN_PINPAD @mamata Jan 8, 2015
#define TAG_CURRENCY_CODE			0x01000049		// currency code (uint8)*2
#define TAG_CURRENCY_SYMBOL			0x01000050		// currency symbol (string)
#define TAG_EMV_FLOOR_LIMIT			0x01000051		// floor limit for EMV (amount)
#define TAG_PPASS_TRAN_LIMIT		0x01000052		// transactions limit for Paypass (amount)
#define TAG_PPASS_CVM_LIMIT			0x01000053		// cvm limit for Paypass (amount)
#define TAG_PPASS_FLOOR_LIMIT		0x01000054		// floor limit for Paypass (amount)
#define TAG_PWAVE_TRAN_LIMIT		0x01000055		// transactions limit for PayWave (amount)
#define TAG_PWAVE_CVM_LIMIT			0x01000056		// cvm limit for PayWave (amount)
#define TAG_PWAVE_FLOOR_LIMIT		0x01000057		// floor limit for PayWave (amount)
#define TAG_TRAN_TYPE				0x01000058		// transaction type (uint8)
#define TAG_EMV_2GEN_TAGS			0x01000059		// tags of emv second generate ac
//- CARCAAN_PINPAD

#define TAG_PI_TRANS_ERROR			0x01000060		// Tag with one of list error for POS INTEGRADO
#define TAG_VOUCHER_ECR				0x01000063		// Tag for formated Voucher for send to ECR
#define TAG_VOUCHER_ECR_COPY		0x01000064		// Tag for formated Voucher (Customer copy) for send to ECR
#define TAG_VOUCHER_CUSTOMER		0x01000065		// Tag to validate if voucher is customer receipt
#define TAG_CIPHER_PAN				0x01000066		// Clear PAN number (string)

#define TAG_BANK_TYPE     	        0x01000067      // TypeCard (uint16)
#define TAG_TOKENBLOCK				0x01000068		// TOKEN Block (uint8 * 8)
//+ PP_FLOW01 @jbotero 13/05/2015
#define TAG_CARD_NAME				0x01000080		// Card Name (string)
#define TAG_CARD_SHORT_NAME			0x01000081		// Card Short Name (string)
#define TAG_CAMPO53                 0x01000082      //campo 53
#define TAG_TIME_REVERSE            0x01000083      //FECHA DE LA TRANSACCION ORIGINAL (REVERSO)
#define TAG_ENTRY_ORIGINAL            0x01000084      //ENTRY MODE ORIGINAL PARA LAS TRANSACCIONES (REVERSO)
//- PP_FLOW01

#define TAG_TRACK1_RAW				0x01000006		// Raw Track 1 to process NO ISO cards(string)
#define TAG_TRACK2_RAW				0x01000007		// Raw Track 2 to process NO ISO cards(string)


#define TAG_SALES_COUNT				0x02000001		// ISO8583 settle SALES count (uint16)
#define TAG_SALES_AMOUNT			0x02000002		// ISO8583 settle SALES amount (amount)
#define TAG_REFUND_COUNT			0x02000003		// ISO8583 settle REFUND count (uint16)
#define TAG_REFUND_AMOUNT			0x02000004		// ISO8583 settle REFUND amount (amount)
#define TAG_DEBIT_COUNT				0x02000005		// ISO8583 settle DEBIT count (uint16)
#define TAG_DEBIT_AMOUNT			0x02000006		// ISO8583 settle DEBIT amount (amount)
#define TAG_RDEBIT_COUNT			0x02000007		// ISO8583 settle DEBIT REFUND count (uint16)
#define TAG_RDEBIT_AMOUNT			0x02000008		// ISO8583 settle DEBIT REFUND amount (amount)
#define TAG_BATCH_UPLOAD			0x02000009		// ISO8583 settle - batch upload required
//+ ISOMAIN01 @mamata Aug 14, 2014
#define TAG_BATCH_UPLOAD_LAST		0x02000010		// ISO8283 SETELE last transaction in batch upload
//- ISOMAIN01
//JR 26052021
#define TAG_SALESC2P_COUNT			0x02000011		// ISO8583 settle SALES C2P count (uint16)
#define TAG_SALESC2P_AMOUNT			0x02000012		// ISO8583 settle SALES C2P amount (amount)
#define TAG_REFUNDC2P_COUNT			0x02000013		// ISO8583 settle REFUND C2P count (uint16)
#define TAG_REFUNDC2P_AMOUNT		0x02000014		// ISO8583 settle REFUND C2P amount (amount)


#define TAG_LOCAL_MODE				0x05000010		// Local Mode DHCP or STATIC (int32)
#define TAG_LOCAL_MAC				0x05000011		// Local MAC(String)
#define TAG_LOCAL_IP				0x05000012		// Local IP(String)
#define TAG_LOCAL_MASK				0x05000013		// Local MASK(String)
#define TAG_LOCAL_GATE				0x05000014		// Local GATEWAY(String)
#define TAG_LOCAL_DNS1				0x05000015		// Local DNS1(String)
#define TAG_LOCAL_DNS2				0x05000016		// Local DNS2(String)



//+ EWL01 @jbotero 5/02/2016
#define TAG_TRAN_EMV_SUPPORT		0x09009001		//EWL EMV support				(uint8)
#define TAG_TRAN_CLESS_SUPPORT		0x09009002		//EWL CONTACTLESS support		(uint8)
#define TAG_TRAN_MANUAL_SUPPORT		0x09009003		//EWL MANUAL support			(uint8)
#define TAG_TRAN_SWIPE_SUPPORT		0x09009004		//EWL SWIPE support				(uint8)
#define TAG_TRAN_CARD_TECHNOLOGY	0x09009005		//EWL Card technology			(uint8)
#define TAG_TRAN_REQ_PIN_ONLINE		0x09009006		//Pin online was requested		(uint8)
#define TAG_TRAN_REQ_PIN_OFFLINE	0x09009007		//Pin off-line was requested	(uint8)
#define TAG_TRAN_REQ_PIN_PINPASS	0x09009008		//Pin PINPASS was requested		(uint8)
#define TAG_TRAN_REQ_SIGNATURE		0x09009009		//Signature was requested		(uint8)
#define TAG_TRAN_SPENDING_AMT		0x09009010		//Spending amount				(string)
#define TAG_TRAN_HAS_SPENDING_AMT	0x09009011		//Has spending amount			(uint8)
#define TAG_TRAN_CRYPTOGRAM			0x09009012		//EMV CRYPTOGRAM				(String)
#define TAG_TRAN_TVR				0x09009013		//EMV TVR						(String)
#define TAG_TRAN_ANSWER				0x09000014		//HOST ANSWER					(uint8)
#define TAG_FORCE_OTHER_INTERFACE	0x09000015		//Force to use other interface(Except CLESS)(uint8)

#define TAG_TRAN_IDLE_CARD			0x09000016		//
#define TAG_MANAGER_TRANSACTION		0x09000017		// transaction from Manager(HTML5)
//- EWL01

//+ CUOTAS01 @jbotero 24/04/2015
#define TAG_INSTALLMENT_MODE		0x08000010		// Installment Mode(CON/SIN CUOTAS)(uint32)
#define TAG_INSTALLMENT_VALUE		0x08000011		// Installment Value(VALOR CUOTA)(String)
#define TAG_INSTALLMENT_NUMBER		0x08000006		// Installment Number(NUMERO CUOTAS)(uint8 * 3)
//- CUOTAS01

//+ STORE_B63 @carodriguez 08/09/2015
#define TAG_COUNTABLE_DATE			0x08008013		// Countable date
#define TAG_ACCOUNT_NUMBER			0x08008014		// Account number
//- STORE_B63

#define TAG_IDLE_MENU				0x08000001		// Card Type or Idle Menu selection(uint32)
#define TAG_VUELTO					0x08000002		// Vuelto enable/Disable(int32)
#define TAG_TICKET_NUMBER			0x08000003		// Ticket Number(NUMERO DE BOLETA)(uint8 * 6)

#define TAG_EMPLOYEE_ID				0x09000004		// Employer ID(uint8 * 4)

#define TAG_POS_NOPRINTER_VOUCHER  	0x90000026     //No printer voucher
#define TAG_ID_NUMBER               0x90000027     // cardholder ID number (cedula/rut)
#define TAG_SERVER_NUMBER           0x90000028	   // Server Number
#define TAG_VOID_ORIGINAL_RECORD	0x90000029     // void Original batch record id for reference
#define TAG_ISSUER_SCRIPT_RESULT    0x90000030     // TAG TAG_ISSUER_SCRIPT_RESULT
#endif //_H_CUSTOMS_DEFINES
