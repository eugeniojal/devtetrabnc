//+ PAR_EWL @carodriguez 29/02/2016
#if (DIR_EWL_PAR == 1)
#ifndef PARREADER_H
#define PARREADER_H

#include "iPOSApp.h"

#define DIR_HOST 		"HOST"
#define DIR_IPOS 		"IPOS"
#define DIR_IEMV 		"E_CUST5d"

#define SOURCE_FILE		"EWL.PAR"
#define DEST_FILE		"EWL.CFG"

#define D_EWL_PAR		"/EWL.PAR"
#define D_EWL_CONF		"/EWL.CFG"

#define C_TAG_AID		0x1000				//!< AID node number used when updating parameter TLV tree.
#define C_TAG_CAKEYS	0x1100				//!< CAKEYS node number of parameter TLV tree. The TLV tree is updated at loading of CAKEYS.PAR file.
#define C_TAG_CAREVOK	0x1200				//!< CAREVOK node number of parameter TLV tree. The TLV tree is updated at loading of CAREVOK.PAR file.
#define C_TAG_ICS		0x1300				//!< ICS node number used when updating parameter TLV tree.
#define C_TAG_PAYWAVE	0x1400				//!< Paywave specific tags(TTQ)

aid_t *pAIDsFromFile;
stKey_t *pKeysFromFile;
ewlSetRevoked_t *pRevokedFromFile;

TLV_TREE_NODE extraParTree;
TLV_TREE_NODE PaywaveTree;

int AIDCount;
int KeyCount;
int RevokedCount;

typedef enum TE_DIR_PATH_TYPES
{
	DIR_CONF=0,
	DIR_APP=1,
	//+ ADD0022 @gmunar 10/06/2015
	DIR_EMV=2
	//- ADD0022

} DIR_PATH_TYPES;

TLV_TREE_NODE RecoverTreeFromFile(uchar *Name, DIR_PATH_TYPES Directory, uint SerMode);
bool checkPARTree(TLV_TREE_NODE Tree);
bool checkPARFiles(void);
int loadTreeToStructs(TLV_TREE_NODE Tree, int AIDCount, int KeyCount, int RevokedCount);
void processPARFile (char* file_name);
int loadExtraPar(TLV_TREE_NODE Tree, ewlObject_t *handle);
int loadPaywaveSpecific(TLV_TREE_NODE Tree, stPaywave *ParamsFromFile);
int loadEWLConf(void);
int loadRevokedCAs(ewlObject_t *handle);
bool checkFile(uchar* Name);
int EWL_LoadParameters(void);

#endif // PARREADER_H
#endif // DIR_EWL_PAR
//- PAR_EWL
