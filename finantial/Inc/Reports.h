/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Reports.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_REPORTS
#define _H_REPORTS

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Report_TransactionList
 * Description:		Print Report of transaction list for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - table of merchant
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_TransactionList(TABLE_MERCHANT *Merchant);

/* --------------------------------------------------------------------------
 * Function Name:	Report_TransactionListServer
 * Description:		Print Report of transaction list for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - table of merchant
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_TransactionListServer(TABLE_MERCHANT *Merchant, int32 IdMesero);
/* --------------------------------------------------------------------------
 * Function Name:	Report_MerchantTotals
 * Description:		Print Totals report for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - table of merchant
 *  - bool Header - flag to print or not report header
 *  - bool Settle - flag to enable print settle data
 * Return:
 * Notes:
 * //1 cierre
//2 reporte detallado
//3 reporte total
 */
int Report_MerchantTotals(TABLE_MERCHANT *Merchant, bool Header, int Reporte);


/* --------------------------------------------------------------------------
 * Function Name:	Reports_Totals
 * Description:		Print totals reports
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Reports_Totals(int32 MerchantID, int32 MerchantGroupID);


/* --------------------------------------------------------------------------
 * Function Name:	Reports_Detail
 * Description:		Print detail reports
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Reports_Detail(int32 MerchantID, int32 MerchantGroupID);

/* --------------------------------------------------------------------------
 * Function Name:	Reports_Detail_Server
 * Description:		Print detail reports
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Reports_Detail_Server(int32 MerchantID, int32 MerchantGroupID, int32 Mesero);



/* --------------------------------------------------------------------------
 * Function Name:	Report_ReprintSettle
 * Description:		Reprint Settle
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_ReprintSettle(void);
int Report_PrintBines(void);
#endif //_H_REPORTS
