/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Tables.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_GLOBALS
#define _H_GLOBALS

#include "Tables.h"

/* ***************************************************************************
 * VARIABLES
 * ***************************************************************************/
//+ INGESTATE01 @jbotero 13/09/2015
#define D_HOST						"/HOST"

#define IGST_TELNET			0	//only connection to check configuration
#define IGST_PARAMS			1	//Only parameters
#define IGST_FULL			2	//Parameters + Application. Take value from TMS_ID contract
//- INGESTATE01


#define IDLE_NOTHING				0			//NINGUNA SELECCION
#define IDLE_CREDIT					1			//TARJETA CREDITO
#define IDLE_DEBIT					2			//TARJETA DEBITO o REDCOMPRA
#define IDLE_NO_BANKING				3			//TARJETA NO BANCARIA(RIPLEY, PRESTO...)
#define IDLE_MERCHANT				4			//OPCIONES DE COMERCIO

#define INSTALLMENT_WITHOUT			0			//SIN CUOTAS
#define INSTALLMENT_WITH			1			//CON CUOTAS

#define CURRENCY_LOCAL				1			// Merchant Moneda Local
#define CURRENCY_DOLLAR				2			// Merchant Moneda Dolar

#define EXP_LOCAL					0			// Exponent Moneda Local(Number of decimals)
#define EXP_DOLLAR					2			// Exponent Moneda Dolar(Number of decimals)



//Overload NULL variable, to avoid compilation warnings
#if(TETRA_HARDWARE == 1)
#undef NULL
#define NULL						0x00
#endif //TETRA_HARDWARE

typedef struct
{
	char CreditLabel[LZ_LINE_IDLE];
	char DebitLabel	[LZ_LINE_IDLE];
	char NoBankLabel[LZ_LINE_IDLE];

	char CreditFullLabel[200];
	char DebitFullLabel	[200];
	char NoBankFullLabel[200];

	char CreditListId	[50];
	char DebitListId	[50];
	char NoBankListId	[50];

} IDLE_MENU;

typedef struct
{
	unsigned short ApplicationType;

	//+ POS_FLOW02 @jbotero 10/06/2015
	T_GL_HGRAPHIC_LIB hGoal; //Goal Instance
	//- POS_FLOW02

	uint32 CardType;				//Card Type(CREDIT, DEBIT, NO BANKING)
	uint32 NoBankingId;				//id of no banking Card selected
	uint8  ContTimeoutReverso;		//Timer for launch Automatic Reverse.
	uint8  ContTimeoutEntrenam;		//Timer for turn off Training mode.
	uint8  ContGprsDisconnect;		//Timer for turn off GPRS connection.

	uint8  TrxGiro;					//Flag to enable Giro Transaction
	uint8  TrxOpt;					//Flag to enable VB, VF, VE Transaction

	//+ DISCON01 @carodriguez 04/12/2015
	bool  KeepCnx;					//Flag to enable keep connection(To send many transaction without disconnection)
	bool  KeepSocketCnx;			//Flag to keep socket connection(IP & port)
	bool  IsSocketCnx;				//Flag to know if socket connection is established
	//- DISCON01

	IDLE_MENU stIdleMenu;

	uint8 PIRequest;				//Flag to indicate that flow is a POS INTEGRADO request

	bool  FirstTimeIsState;			//Flag to validate if is first time on is_state function

	char	TID_Local[LZ_MERCH_TID];	//Terminal Local
	char	MID_Local[LZ_MERCH_LID];	//Merchant Local

	//+BIMONEDA @lgarcia 12/12/2016
	uint8	CurrencyType;				//Currency type of POS
	char	TID_Dollar[LZ_MERCH_TID];	//Terminal Dolar
	char	MID_Dollar[LZ_MERCH_LID];	//Merchant Dolar
	uint64	AmountMin_Dollar;			//Amount Min Dolar
	uint64	AmountMax_Dollar;			//Amount Max Dolar
	amount	AmountCless_CVM_Cred;		//CVM credit amount
	amount	AmountCless_CVM_Deb;		//CVM debit amount
	amount	AmountCless_TRX_Cred;		//Transaction limit credit amount
	amount	AmountCless_TRX_Deb;		//Transaction limit debit amount
	//-BIMONEDA

	//+MULTICODIGO @lgarcia 05/02/2017
	char LastInquiryMerchantPres[LZ_PRES_MERCAHNT];
	uint8 FlagLastInqMerPres;
	//-MULTICODIGO

	char FtraxPartnerID_PESO[LZ_FTRAX_PARTNER_ID];
	char FtraxDCCProfileID_PESO[LZ_FTRAX_DCCPROFILE_ID];
	char FtraxDCCMerchantNum_PESO[LZ_FTRAX_DCCMERCHANT_NUM];
	char FtraxPartnerID_DOLAR[LZ_FTRAX_PARTNER_ID];
	char FtraxDCCProfileID_DOLAR[LZ_FTRAX_DCCPROFILE_ID];
	char FtraxDCCMerchantNum_DOLAR[LZ_FTRAX_DCCMERCHANT_NUM];

	char Track1_un[200];
	char Track2_un[200];
	char Track3_un[200];

	uint8 FlagFkeyPressed;			//Control for TETRA, if F key was pressed, control Task process

} GLOBALINFO;

extern GLOBALINFO tGlobalInfo;
// Tables
extern TLV_TREE_NODE Tree_NoPrinter;
extern GENERAL_CONF tConf;
extern LLCOMS_CONFIG LLC_Host;
extern TABLE_TERMINAL tTerminal;
extern TABLE_STATISTICS tStatistics;
extern TLV_TREE_NODE Tree_Tran;
extern char IdleKey;
//+ TABLEM01 @mamata Feb 17, 2015
unsigned short ApplicationType;
//- TABLEM01
#endif //_H_GLOBALS
