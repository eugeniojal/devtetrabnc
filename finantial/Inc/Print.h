/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Print.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_PRINT
#define _H_PRINT

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
extern int UI_PrintLine_Ext(char *Line, UI_FONT tFont, UI_ALIGN Aling );
extern int UI_PrintLineFeed_Ext(int lines);
extern int UI_Print2Fields_Ext(char *F1Title, char *F1Text, UI_FONT F1Font, char *F2Title, char *F2Text, UI_FONT F2Font);
extern int UI_OpenPrinter_Ext(void);
extern void UI_ClosePrinter_Ext(void);
extern int Print_TransactionImage_Ext(void);

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionReceipt
 * Description:		Print recetip for a transaction
 * Parameters:
 *  - TLV_TREE_NODE Tree
 *  - bool Copy
 *  - bool Duplicate
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionReceipt(TLV_TREE_NODE Tree, bool Copy, bool Duplicate);
int Print_Pre_TransactionReceipt(TLV_TREE_NODE Tree);
int Print_Pre_TransactionDenied(TLV_TREE_NODE Tree);
/* --------------------------------------------------------------------------
 * Function Name:	Print_ReportHeader
 * Description:		Print header of a report
 * Parameters:
 *  - TABLE_MERCHANT Merchant
 * Return:
 *  - RET_OK
 * Notes:
 */
//1 cierre
//2 reporte detallado
//3 reporte total
int Print_ReportHeader(TABLE_MERCHANT *Merchant, int Reporte);
//int Print_ReportHeader(TABLE_MERCHANT *Merchant, bool Settle);


/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionListItem
 * Description:
 * Parameters:
 *  - BATCH *BatchRecord
 *  - bool Fist
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionListItem(BATCH *BatchRecord, bool Fist);

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionListItemServer
 * Description:
 * Parameters:
 *  - BATCH *BatchRecord
 *  - bool Fist
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionListItemServer(BATCH *BatchRecord);

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionTotalServer
 * Description:
 * Parameters:
 *  - BATCH *BatchRecord
 *  - bool Fist
 * Return:
 *  - RET_OK
 * Notes:
 */

int Print_TransactionTotalServer(TABLE_MERCHANT *Merchant, uint32 ServerId, bool Detalle);
/* --------------------------------------------------------------------------
 * Function Name:	Print_MerchantTotals
 * Description:		Print totals for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_MerchantTotals(TABLE_MERCHANT *Merchant);


/* --------------------------------------------------------------------------
 * Function Name:	Print_RangeTotals
 * Description:		Print total for specific range
 * Parameters:
 *  - TABLE_MERCHANT *Merchant
 *  - uint32 RangeID
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_RangeTotals(TABLE_MERCHANT *Merchant, uint32 RangeID, bool EntryMode);


/* --------------------------------------------------------------------------
 * Function Name:	Print_SettleInfo
 * Description:		Print last settle info
 * Parameters:
 *  - TABLE_MERCHANT *Merchant merchant to be printed
 * Return:
 *  - RET_OK;
 * Notes:
 */
int Print_SettleInfo(TABLE_MERCHANT *Merchant);
int Imprimir_header_report (void);
int Reporte_Terminal(void);
int Reporte_HOST(void);
int Reporte_COMM(void);
#endif //_H_PRINT
