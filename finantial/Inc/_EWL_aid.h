#ifndef AID_H
#define AID_H

typedef struct {
    byte             onlineValue[EWL_TAC_LEN];
    byte             denialValue[EWL_TAC_LEN];
    byte             defaultValue[EWL_TAC_LEN];
} tac_t;

typedef struct {
    long             FloorLimit;
    long             Threshold;
    byte             Max[EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL_LEN];
    byte             Target[EWL_TAG_TARGET_PERC_RAND_SEL_LEN];
} risk_t;

typedef struct {
    long             TransactionLimit;
    long             CMVLimit;
}cless_t;

typedef struct{
    byte            value[EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN];
}version_t;

typedef struct {
    int             nVersions;
    version_t       version[EWLDEMO_VERSION_NUMBER_MAX];
}versions_t;

typedef struct {
    int              dolLen;
    byte             dol[EWLDEMO_DOL_MAX_LEN];
}dol_t;


typedef struct
{
   ewlTechnology_t  technology;
   int              aidLen;
   byte             aid[EWL_EMV_AID_CARD_MAX_LEN];

   versions_t       version;
   tac_t            tac;
   risk_t           risk;
   cless_t          cless;
   dol_t            tdol;
   dol_t            ddol;
}aid_t;

aid_t *aidGet (int id);

#endif
