/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		_TetraMigration.h
* Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/
#ifndef _H_TMIGRATION
#define _H_TMIGRATION

size_t strlcpy(char * dest, const char * src, size_t maxLength);

size_t strlcat(char * dest, const char * src, size_t maxLength);

#endif//_H_TMIGRATION
