/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		_TLV_admin.h
* Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/
#ifndef _H_TLVADMIN
#define _H_TLVADMIN

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	Tlv_FindTag
 * Description:		Search tag into tree on index 0.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * Return:
 * 	- (RET_OK)  Tag exist
 * 	- (RET_NOK) Tag doesn't exist
 * Notes:
 */

int Tlv_FindTag(TLV_TREE_NODE Tree, unsigned int nTag);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValue
 * Description:		Set tag children value into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in void * value: pointer of data
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValue(TLV_TREE_NODE Tree, unsigned int nTag, void * value, unsigned int length);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_GetTagValue
 * Description:		Get tag children value into tree.
 * 	- If tags exist, return value and length.
 * 	- If tags doesn't exist, return error.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- out void * value: pointer of data
 * 	- out unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_GetTagValue(TLV_TREE_NODE Tree, unsigned int nTag, void * value, unsigned int * length);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_ReleaseTag
 * Description:		Release a tag into tree.
 * 	- If tags exist, release tag.
 * 	- If tags doesn't exist, do anything.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_ReleaseTag(TLV_TREE_NODE Tree, unsigned int nTag);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_EwlGetParameter
 * Description:		Read parameter from EWL, and load value on specific Tree
 * 					and tag
 * 	- If tree tag exist, overwrite information on same level.
 * 	- If tree tag doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in ewlObject_t *handle: Handle of EMV EWL
 * 	- out void *data: Pointer to data, can be NULL if data isn't needed
 * 	- in unsigned int maxLen: Max length of data to read
 * 	- in ewlTag_t tag: EWL Tag to read
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_EwlGetParameter(TLV_TREE_NODE Tree, unsigned int nTag,
		ewlObject_t *handle, void *data, unsigned int maxLen, ewlTag_t tag );
//- EWL01

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueInteger
 * Description:		Set tag children value as an integer into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in int value: number with the value
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValueInteger(TLV_TREE_NODE Tree, unsigned int nTag, int value, unsigned int length);

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueString
 * Description:		Set tag children value string into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in char * value: pointer to the null terminated string to save in the tree
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValueString(TLV_TREE_NODE Tree, unsigned int nTag, char *value);
#endif //_H_TLVADMIN
