/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPP-App
 * Header file:		PINPAD.h
 * Description:		Implement CarCAAn Pinpad service calls
 * Author:			mamata
 * --------------------------------------------------------------------------*/
//+ CARCAAN_PINPAD @mamata Jan 8, 2015
#ifndef _H_CARCAAN_PINPAD
#define _H_CARCAAN_PINPAD


/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
extern TLV_TREE_NODE Tree_PINPAD;

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define iPPApp_AppType		0xc640
#define iPPApp_Services		640
#define PINPAD_Services		630

// PINPAD TAGs from 0x05000001 to 0x05999999
#define PP_TAG_COMMAND				0x05000001			// (uint8) 	pinpad commands, posible options in PP_COMMANDS
#define PP_TAG_CONTROL1				0x05000002			// (uint16) first bitmap for command options
#define PP_TAG_CONTROL2				0x05000003			// (uint16) second bitmap for command options
#define PP_TAG_LINE1				0x05000004			// (string) line 1 for pinpad display
#define PP_TAG_LINE2				0x05000005			// (string) line 2 for pinpad display
#define PP_TAG_LINE3				0x05000006			// (string) line 3 for pinpad display
#define PP_TAG_LINE4				0x05000007			// (string) line 4 for pinpad display
#define PP_TAG_RESULT				0x05000008			// (uint8) 	command result
//+ PPCRYPTO @mamata Jun 30, 2015
#define PP_TAG_CRYPTO_CLEAR_DATA	0x05000009			// (buffer) data to be encrypt
#define PP_TAG_CRYPTO_OUTPUT_DATA 	0x05000010			// (buffer) data encrypted
#define PP_TAG_CRYPTO_ALGORITH		0x05000011			// (uint8) 	CRYPTOLIB_ALGORITHM_TYPE
#define PP_TAG_CRYPTO_WK_DATA		0x05000012			// (buffer)	working key
#define PP_TAG_CRYPTO_MK_INDEX		0x05000013			// (uint8)	master key index
//- PPCRYPTO

// PINPAD Commands
typedef enum td_PP_COMMANDS
{
	PP_COMMAND_DISPLAY = 0x01,
	PP_COMMAND_IDLE = 0x02,
	PP_COMMAND_TRANSACTION = 0x03,
	PP_COMMAND_HOST_COMM = 0x04,
	//+ PPCRYPTO @mamata Jun 30, 2015
	PP_COMMAND_ENCRYPT_DATA = 0x05,
	//- PPCRYPTO
	//+ PPGETCARD @mamata Jul 1, 2015
	PP_COMMAND_CARD = 0x06,
	//- PPGETCARD
}PP_COMMANDS;

//+ PPGETCARD @mamata Jul 6, 2015
#define PP_FLAG_MS_READER				0x0001
#define PP_FLAG_ICC_READER				0x0002
#define PP_FLAG_CLESS_READER			0x0004
#define PP_FLAG_KEY_ENTRY				0x0008
#define PP_FLAG_CLESS_FIRST			0x0010
#define PP_FLAG_VALIDATE_SERVICE_CODE	0x0020
#define PP_FLAG_VALIDATE_EXP_DATE		0x0040
#define PP_FLAG_PROMPT_CVV2_4DBC		0x0080
#define	PP_FLAG_PROMPT_LAST4		0x0100
#define PP_FLAG_PROMPT_PIN				0x0200
#define PP_FLAG_CLEAR_SCREEEN			0x0400
#define PP_FLAG_BEEP_ERROR				0x0800
#define PP_FLAG_BEEP_INFORMATION		0x1000
#define PP_FLAG_IDLE					0x2000
//d // PP_COMMAND_DISPLAY - bitmap for PP_TAG_CONTROL1
//d #define PP_DIPS_CLEAR_SCREEN		0x01
//d #define PP_DIPS_BEEP_ERROR			0x02
//d #define PP_DIPS_BEEP_INFORMATION	0x04
//d #define PP_DIPS_IDLE				0x08
//- PPGETCARD


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	PP_ManagerServices
 * Description:		process service calls from PINPAD
 * Author:			mamata
 * Parameters:
 *  - unsigned int nSize - size of service call data
 *  - byte *pData - pointer to service call data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_ManagerServices(unsigned int nSize, byte *pData);


/* --------------------------------------------------------------------------
 * Function Name:	PP_SendTranToHost
 * Description:		send transaction to host via PINPAD
 * Autor:			mamata
 * Parameters:
 *  - node
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_SendTranToHost(void);


#endif //_H_CARCAAN_PINPAD
//- CARCAAN_PINPAD
