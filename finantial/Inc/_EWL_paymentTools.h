#ifndef PAYMENTTOOLS_H
#define PAYMENTTOOLS_H

bool paymentToolsIsChipMagnetic (ewlTechnology_t technology);
bool paymentToolsIsCless (ewlTechnology_t technology);

#endif
