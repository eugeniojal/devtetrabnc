/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		ISO8583_Main.h
 * Description:		Implement ISO 8583 for XXXXXX Host
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_ISO8583_MAIN
#define _H_ISO8583_MAIN
#define BIT_0 0   /* 00000000 */
#define MSB_BIT (1 << 7)
#define LSB_BIT (1 << 0)
#define NOT_BIT 0   /* 00000000 */

/* Bitmap BYTE 1 */
#define BIT_1 (1 << 7)/* 10000000 */
#define BIT_2 (1 << 6)/* 01000000 */
#define BIT_3 (1 << 5)/* 00100000 */
#define BIT_4 (1 << 4)/* 00010000 */
#define BIT_5 (1 << 3)/* 00001000 */
#define BIT_6 (1 << 2)/* 00000100 */
#define BIT_7 (1 << 1)/* 00000010 */
#define BIT_8 (1 << 0)/* 00000001 */

/* Bitmap BYTE 2 */
#define BIT_9   BIT_1
#define BIT_10  BIT_2
#define BIT_11  BIT_3
#define BIT_12  BIT_4
#define BIT_13  BIT_5
#define BIT_14  BIT_6
#define BIT_15  BIT_7
#define BIT_16  BIT_8

/* Bitmap BYTE 3 */
#define BIT_17  BIT_1
#define BIT_18  BIT_2
#define BIT_19  BIT_3
#define BIT_20  BIT_4
#define BIT_21  BIT_5
#define BIT_22  BIT_6
#define BIT_23  BIT_7
#define BIT_24  BIT_8

/* Bitmap BYTE 4 */
#define BIT_25  BIT_1
#define BIT_26  BIT_2
#define BIT_27  BIT_3
#define BIT_28  BIT_4
#define BIT_29  BIT_5
#define BIT_30  BIT_6
#define BIT_31  BIT_7
#define BIT_32  BIT_8

/* Bitmap BYTE 5 */
#define BIT_33  BIT_1
#define BIT_34  BIT_2
#define BIT_35  BIT_3
#define BIT_36  BIT_4
#define BIT_37  BIT_5
#define BIT_38  BIT_6
#define BIT_39  BIT_7
#define BIT_40  BIT_8

/* Bitmap BYTE 6 */
#define BIT_41  BIT_1
#define BIT_42  BIT_2
#define BIT_43  BIT_3
#define BIT_44  BIT_4
#define BIT_45  BIT_5
#define BIT_46  BIT_6
#define BIT_47  BIT_7
#define BIT_48  BIT_8

/* Bitmap BYTE 7 */
#define BIT_49  BIT_1
#define BIT_50  BIT_2
#define BIT_51  BIT_3
#define BIT_52  BIT_4
#define BIT_53  BIT_5
#define BIT_54  BIT_6
#define BIT_55  BIT_7
#define BIT_56  BIT_8

/* Bitmap BYTE 8 */
#define BIT_57  BIT_1
#define BIT_58  BIT_2
#define BIT_59  BIT_3
#define BIT_60  BIT_4
#define BIT_61  BIT_5
#define BIT_62  BIT_6
#define BIT_63  BIT_7
#define BIT_64  BIT_8


/* Bitmap BYTE 9 */
#define BIT_65  BIT_1
#define BIT_66  BIT_2
#define BIT_67  BIT_3
#define BIT_68  BIT_4
#define BIT_69  BIT_5
#define BIT_70  BIT_6
#define BIT_71  BIT_7
#define BIT_72  BIT_8

/* Bitmap BYTE 10 */
#define BIT_73  BIT_1
#define BIT_74  BIT_2
#define BIT_75  BIT_3
#define BIT_76  BIT_4
#define BIT_77  BIT_5
#define BIT_78  BIT_6
#define BIT_79  BIT_7
#define BIT_80  BIT_8

/* Bitmap BYTE 11 */
#define BIT_81  BIT_1
#define BIT_82  BIT_2
#define BIT_83  BIT_3
#define BIT_84  BIT_4
#define BIT_85  BIT_5
#define BIT_86  BIT_6
#define BIT_87  BIT_7
#define BIT_88  BIT_8

/* Bitmap BYTE 12 */
#define BIT_89  BIT_1
#define BIT_90  BIT_2
#define BIT_91  BIT_3
#define BIT_92  BIT_4
#define BIT_93  BIT_5
#define BIT_94  BIT_6
#define BIT_95  BIT_7
#define BIT_96  BIT_8


/* Bitmap BYTE 13 */
#define BIT_97  BIT_1
#define BIT_98  BIT_2
#define BIT_99  BIT_3
#define BIT_100 BIT_4
#define BIT_101 BIT_5
#define BIT_102 BIT_6
#define BIT_103 BIT_7
#define BIT_104 BIT_8


/* Bitmap BYTE 14 */
#define BIT_105 BIT_1
#define BIT_106 BIT_2
#define BIT_107 BIT_3
#define BIT_108 BIT_4
#define BIT_109 BIT_5
#define BIT_110 BIT_6
#define BIT_111 BIT_7
#define BIT_112 BIT_8

/* Bitmap BYTE 15 */
#define BIT_113 BIT_1
#define BIT_114 BIT_2
#define BIT_115 BIT_3
#define BIT_116 BIT_4
#define BIT_117 BIT_5
#define BIT_118 BIT_6
#define BIT_119 BIT_7
#define BIT_120 BIT_8

/* Bitmap BYTE 16 */
#define BIT_121 BIT_1
#define BIT_122 BIT_2
#define BIT_123 BIT_3
#define BIT_124 BIT_4
#define BIT_125 BIT_5
#define BIT_126 BIT_6
#define BIT_127 BIT_7
#define BIT_128 BIT_8
/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
typedef struct tagISOFIELDS{
	TP_TPDU	stTPDU;							// Estrutura do TPDU
	uint8	vbMsgType[4+1];					// Message Id
	uint8	aBitmap[8];						// Bitmap
	uint16	ui16PANB_Len;					// bit 02 - PAN Len
	uint8	aPAN[19+1];						// bit 02 - PAN
	uint8	vbPCode[6+1];					// bit 03 - Processing Code
	uint8	aValor[12+1];					// bit 04 - Valor
	uint8	aDtTm[10+1];					// bit 07 - Date/Time
	uint8	aNSU[6+1];						// bit 11 - System trace // @DFVC 9/06/2014 Se cambia el tama?o del campo de 8+1 a 6+1 por que es el estandar de ISO para el campo 11
	uint8	aTmLocal[6+1];					// bit 12 - Local Time
	uint8	aDtLocal[4+1];					// bit 13 - Local Date
	uint8	aDtExp[4+1];					// bit 14 - Expire Date
	uint8	aDtSett[4+1];					// bit 15 - Date Settlement
	uint8	aDtEmpm[4+1];					// bit 17 - Date EMISION PAGO MOVIL
	uint8	mcc[4+1];						// bit 18 - MCC
	uint8	aMerchCat[4+1];					// bit 18 - Merchant Category
	uint8	aEntryMode[15+1];				// bit 22 - Entry Mode // @DFVC 9/06/2014 Se cambia el tama?o del campo de 12+1 a 3+1 por que es el estandar de ISO para el campo 22
	uint8	aStan[4+1];						// bit 23 - STAN
	uint8	aNII[3+1];						// bit 24 - NII
	uint8	aPOSCond[2+1];					// bit 25 - POS Condition COde
	uint16	u16Track2_Len;					// bit 35 - Track2 Lenght
	uint8	aTrack2[37+1];					// bit 35 - Track2
	uint16	u16Track3_Len;					// bit 36 - Track3 Lenght
	uint8	aTrack3[76+1];					// bit 36 - Track3
	uint8	aRRN[12+1];						// bit 37 - RRN - Retrieval Reference Number
	uint8	aAuthCode[8+1];					// bit 38 - Authorization Code
	uint8	aRespCode[3+1];					// bit 39 - Response Code
	uint8	aTermId[15+1];					// bit 41 - Terminal Id
	uint8	aMerchId[15+1];					// bit 42 - Merchant Id
	uint8	aAddRespData[10+1];				// bit 44 - Additional Response Data
	uint16	ui16Track1_Len;					// bit 45 - Track1 Len
	uint8	vbTrack1[76+1];					// bit 45 - Track1 Data
	uint16	u16AddData_Len;					// bit 48 - Tamanho do bit 48
	uint8	aAddData[150+1];				// bit 48 - Additional Data Private
	uint8	aCurrCode[3+1];					// bit 49 - Currency Code, Transaction
	uint16	u16PinBlock_Len;				// bit 52 - Tamanho do bit 52
	uint8	PinBLock[8+1];					// bit 52 - Valor
	uint16	u16AddAmt_Len;					// bit 54 - Tamanho do bit 54
	uint8	aAddAmt[100];					// bit 54 - Valor
	uint16	u16Bit55_Len;					// bit 55 - Tamanho do bit 55
	uint8	aBit55[400];					// bit 55 - Reserved ISO - Informacao Adicional
	uint16	u16Bit60_Len;					// bit 60 - Tamanho do bit 60
	uint8	aBit60[8+1];					// bit 60
	uint16	u16Bit61_Len;					// bit 61 - Tamanho do bit 61
	uint8	aBit61[512+1];					// bit 61
	uint16	u16Bit62_Len;					// bit 61 - Tamanho do bit 62
	uint8	aBit62[512+1];					// bit 62
	uint16	u16Bit63_Len;					// bit 63 - Tamanho do bit 63
	uint8	aBit63[512+1];					// bit 63
	uint16	u16Bit64_Len;					// bit 64 - Tamanho do bit 64
	uint8	aBit64[512+1];					// bit 64
	uint16	u16Bit73_Len;					// bit 73 - Tamanho do bit 73
	uint8	aBit73[32+1];					// bit 73
	uint16  Bit90len;
	Byte   	Bit90[42+1];
	uint16  Bit95len;
	Byte   	Bit95[42+1];
	uint16  btField127Responselen;         //Eugenio 127
	Byte   	btField127Response[350 +1];     //Eugenio 127
	uint8   campo53rx[16+1];               //Eugenio 127
	uint8   msgid[4+1];                     //EUGENIO
	uint8   cardSecuence[3+1];              //eugenio
	uint8   aEntryMode22[3+1];
	uint8   nreversos[10+1];              //eugenio
	uint8   nventas[10+1];              //eugenio
	uint8   mreversos[16+1];              //eugenio
	uint8   mventas[16+1];              //eugenio
	uint8   mtotall[16+1];              //eugenio
	uint16	u16token_Len;				// bit 122 - Tamanho do bit 122
	uint8	tokenBLock[8+1];			// bit 122 - Valor
} stIsoFields;

typedef struct tagRESPONSE_CODES{
	uint8 ResponseCode[2];
	bool Approved;
	bool ClearReverse;
	bool SaveBatch;
	uint16 MessageID;
}RESPONSE_CODES;


/* ***************************************************************************
 * TABLE: GENERAL CONFIGURATION
 * ***************************************************************************/
#define HOST_RET_OK 0
#define HOST_RET_BATCH_ERROR -1
#define HOST_RET_TREE_ERROR -2
#define HOST_RET_PACK_ERROR -3
#define HOST_RET_UNPACK_ERROR -4
#define HOST_RET_COMM_ERROR -5
#define HOST_RET_NOT_APPROVED -6
#define HOST_RET_TOTALS_ERROR -7
#define HOST_RET_UPLOAD_ERROR -8
#define HOST_RET_TABLE_ERROR -9
#define HOST_RET_REVERSE_ERROR -10
#define HOST_RET_ADVICE_ERROR -11

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
/** ------------------------------------------------------------------------------
 * Function Name:	ISO8583_Pack
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Generate a ISO8583 message format using the data on tMsgTx to build a message
 * Parameters:
 * - bufferTx    O message to send to host
 * - nSizeBuffer O size of message
 * Return:
 * - RET_OK, successful
 * - RET_NOK, No successful
 * Notes:
 */
int ISO8583_Pack( char* bufferTx, int *nSizeBuffer, TLV_TREE_NODE Tree);


/** ------------------------------------------------------------------------------
 * Function Name:	ISO8583_UnPack
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Unpack a ISO8583 message received from host and fill tMsgRx struct
 * Parameters:
 * - bufferRx    I message receive from host
 * - nSizeBuffer I size of message
 * Return:
 * - RET_OK, successful
 * - RET_NOK, No successful
 * Notes:
 */
int ISO8583_UnPack(char* bufferRx, int nSizeBuffer,TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_Connect
 * Description:		begin host connection
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction tree
 * Return:
 *  - RET_OK - connect to host is begin
 *  - RET_NOK - cannot begin host connection
 * Notes:
 */
int HostMain_Connect(TLV_TREE_NODE Tree);






/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SendReceive
 * Description:		Send request to host and receive response forom host
 * Parameters:
 *  - TLV_TREE_NODE Tree - Transaction data
 * Return:
 *  - RET_OK - process OK
 *  - RET_NOK - configuration error
 *  <0 - link layer error
 * Notes:
 */
int HostMain_SendReceive_Transaction(TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_Process_ResponseCode
 * Description:		Process response code
 * Parameters:
 *  TLV_TREE_NODE Tree - transaction data
 *  uint8 *ResponseCode - pointer to host response code
 *  uint16 TranOptions - transaction options
 *  uint16 TranStatus - transaction status
 * Return:
 *  RET_OK - response code processed OK
 *  RET_NOK - cannot process response code
 * Notes:
 */
int HostMain_Process_ResponseCode(TLV_TREE_NODE Tree, uint8 *ResponseCode, uint16 TranOptions, uint16 TranStatus);


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_PerfomSellte
 * Description:		Perform settle process
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter (99999 for all merchant group)
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int HostMain_PerfomSellte(int32 MerchantID, int32 MerchantGroupID);


//+ CLESS @mamata Dec 11, 2014
/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SetOfflineDeclined1G
 * Description:		Set offline declined in fist generate
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK - transaction is set as offline declined
 *  - RET_NOK - cannot set transaciton as offline declined
 * Notes:
 */
int HostMain_SetOfflineDeclined1G(TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SetOfflineApproved
 * Description:		Set offline approved status to a transaction
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK - transaction is set as offline approved
 *  - RET_NOK - cannot set transaciton as offline approved
 * Notes:
 */
int HostMain_SetOfflineApproved(TLV_TREE_NODE Tree);
//- CLESS

int HostMain_GetResponseCodeInfo(uint8 *HostResponseCode, RESPONSE_CODES *ResponseCode);
//EA
int Borrar_Advice(uint8 *BatchName, bool Settle);
int Borrar_Reverse(uint8 *BatchName);
int HostMain_SendReverse(uint8 *BatchName);
int Reverse_Mtip(TLV_TREE_NODE Tree);

#endif //_H_ISO8583_MAIN
