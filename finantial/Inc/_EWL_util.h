#ifndef EWLUTIL_H
#define EWLUTIL_H

enum{
    EWLDEMO_MENU_TIMEOUT        = 60,       // seconds
    EWLDEMO_WAIT_CARD           = 60,       // seconds
    EWLDEMO_WARNING_TIMEOUT     = 5,        // seconds
    EWLDEMO_INFINITE_TIMEOUT    = 0xFFFF,
    EWLDEMO_VERSION_NUMBER_MAX  = 3,
    EWLDEMO_DOL_MAX_LEN         = 256,

    EWLDEMO_BACK_COLOR          = GL_COLOR_ANTIQUE_WHITE,
    EWLDEMO_FORE_COLOR          = GL_COLOR_BLACK,

};


enum{
    EWLDEMO_OK                      = BASE_ERR_OK,
    EWLDEMO_ERR_INVALID_PARAMETER   = BASE_ERR_INVALID_PARAMETER,
    EWLDEMO_ERR_DATA_NOT_FOUND      = BASE_ERR_DATA_NOT_FOUND,
    EWLDEMO_ERR_INVALID_HANDLE      = BASE_ERR_INVALID_HANDLE,
    EWLDEMO_ERR_RESOURCE_PB         = BASE_ERR_RESOURCE_PB ,
    EWLDEMO_ERR_OVERFLOW            = BASE_ERR_OVERFLOW ,
    EWLDEMO_ERR_ACCESS              = BASE_ERR_ACCESS ,
    EWLDEMO_ERR_CANCEL              = BASE_ERR_CANCEL ,
    EWLDEMO_ERR_TIMEOUT             = BASE_ERR_TIMEOUT,
    EWLDEMO_ERR_BACKSPACE           = (BASE_ERR_TIMEOUT - 1),
    EWLDEMO_ERR_CARD_FORMAT         = (BASE_ERR_TIMEOUT - 2),
    EWLDEMO_ERR_INTERNAL            = -0xFFFF,
};

int  utilOpenDriver (void);
void utilCloseDriver (void);
void utilLedsOff (void);
void utilBeepError (void);
void utilBeepWarning (void);


Telium_File_t *utilTrack1 (void);
Telium_File_t *utilTrack2 (void);
Telium_File_t *utilTrack3 (void);

int utilGetPanASCII ( ewlObject_t *handle, char *pOut, int maxSize);
int utilGetExpirationYYMM ( ewlObject_t *handle, char *pOut, int maxSize);
int utilGetServiceCodeAscii ( ewlObject_t *handle, char *pOut, int maxSize);
int utilOpenTracks(TransactionPaymentData_ST *data);
int utilFormatValue (char *formattedValue, unsigned long long amount);
int utilGetKey (uint8_t *nDigits, int *cont);
int utilGetPinScreenLines(char * msgTitle, char * msgLine1, char * msgLine2);
int utilGetPinOnline(TLV_TREE_NODE Tree);
void utilWaitToRemoveChipCard(void);
void utilShowMsgExternalPP(char * MsgLine1, char * MsgLine2, uint8 ClearAll);
int utilInsertEMVTags(TLV_TREE_NODE ParentNode, TLV_TREE_NODE EmvTags);

#endif //EWLUTIL_H
