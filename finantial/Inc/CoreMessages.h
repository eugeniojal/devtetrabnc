/*************************************************************
 *
 * Messages.h
 * Manage the messages for an application, using the manager mode.
 *
 * @AJAF, 20130905
 *
 *************************************************************/


#include "CoreMessageDefinitions.h"	// Messages' name definitions

/*************** DEFINES *********************/
#define ES	"ES"				/*!< Spanish */

#define MESSAGE_ID	0xFF000000

/************** LOCAL VARS*********************/
/************** EXTERN VARS*********************/
/********** MESSAGES FUNCTIONS ****************/

/**
 * Init the messages management, load the messages.
 */
extern void mess_initMessages(void);

/**
 * get a double pointer (array) with the supported languages codes (ISO 639-1)
 * @param LangNumber, Return the number of languages.
 * @return
 */
extern char** mess_getLoadedLanguages(int *LangNumber);

/**
 * Get the name of the languages from the pointer with language codes.
 * @param ppLanguagesTab. Language codes.
 * @param LangNumber.	Number of languages
 * @return
 */
extern char** mess_getLanguageName(char **ppLanguagesTab, int LangNumber);

/**
 * Get the Current language code
 * @param pCurrLangCode
 */
extern void mess_getCurrentLanguageCode(char *pCurrLangCode);

/**
 * Get the Current language name
 * @param pCurrLangName
 */
extern void mess_getCurrentLanguageName(char *pCurrLangName);

/**
 * Get the indicated message
 * @param MSGnum
 * @param currMessage
 * @return OK or error (-1)
 */
extern int mess_getMessageByParameter(int MSGnum, char *currMessage);

/**
 * Get the indicated message
 * @param MSGnum
 * @return Message
 */
char* mess_getReturnedMessage(int MSGnum);

/**
 * Set the current language
 * @param pCurrLangCode. Language code for language to set
 */
extern void mess_setCurrentLanguage(char *pCurrLangCode);

/**
 * get a the number of loaded languages.
 * @return
 */
extern int mess_getLoadedLanguagesNumber(void);
