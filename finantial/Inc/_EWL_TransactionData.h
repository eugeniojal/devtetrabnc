//+ EWL01 @jbotero 29/01/2016
#ifndef TRANSACTIONDATA_H
#define TRANSACTIONDATA_H

#include "ewl.h"

enum{
	TRANSACTION_SERVICECODE_ASCII_LEN       = 3 + 1,
	TRANSACTION_EXPIRATION_ASCII_LEN        = 4 + 1,
	TRANSACTION_EXPIRATION_LEN              = 2,
	TRANSACTION_PAN_ASCII_LEN               = 19 + 1,
	TRANSACTION_TRACK1_ASCII_LEN            = 76 + 1,
	TRANSACTION_TRACK2_ASCII_LEN            = 37 + 1,
	TRANSACTION_TRACK3_ASCII_LEN            = 104 + 1,
	TRANSACTION_AUTHORIZATION_CODE_LEN      = 6+1,
	TRANSACTION_RESPONSE_CODE_LEN           = 2+1,
	TRANSACTION_PIN_ONLINE                  = 8,
};

typedef enum{
	TRANSACTION_NONE                        = 0,
	TRANSACTION_MAGNETIC                    = 1,
	TRANSACTION_TYPED                       = 2,
	TRANSACTION_CHIP_CONTACT                = 3,
	TRANSACTION_CHIP_CONTACTLESS            = 4,
}transactionInterface_t;




typedef struct{
	byte pinOnline[TRANSACTION_PIN_ONLINE];
}transactionPINData_t;


typedef struct{

	// Transaction control
	FILE          *mag31;
	FILE          *mag2;
	FILE          *mag3;

	ewlObject_t   *emvHandle;
	dateTime_t    timeDate;
	unsigned long long value;

	bool emvSupported;
	bool clessSupported;

	transactionInterface_t interface;

	// Card Data
	char track1[TRANSACTION_TRACK1_ASCII_LEN];
	char track2[TRANSACTION_TRACK2_ASCII_LEN];
	char track3[TRANSACTION_TRACK3_ASCII_LEN];

	char label[EWL_TAG_LABEL_MAX_LEN+1];
	char pan[TRANSACTION_PAN_ASCII_LEN];
	char name[EWL_EMV_CARDHOLDER_NAME_MAX_LEN + 1];
	byte expiration[TRANSACTION_EXPIRATION_LEN];
	ewlTechnology_t technology;

	bool hasSpendingAmount;
	byte spendingAmount[EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT_LEN];

	byte tvr[EWL_EMV_TVR_LEN];
	byte criptogram[EWL_EMV_CRYPTOGRAM_LEN];
	byte pinOn[TRANSACTION_PIN_ONLINE];

	bool signature;
	bool pinOnLine;
	bool pinOffLine;
	bool online;

	ewlTransactionStatus_t status;

	// Host Data
	ewlHostAnswer_t answer;
	char responseCode[TRANSACTION_RESPONSE_CODE_LEN];
	char authorizationCode[TRANSACTION_AUTHORIZATION_CODE_LEN];

	unsigned int issuerAuthenticationLen;
	byte issuerAUthentication[EWL_EMV_ISSUER_AUTHENTICATION_DATA_MAX_LEN];

}TransactionPaymentData_ST;

#endif
//- EWL01
