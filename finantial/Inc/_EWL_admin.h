/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		_EWL_admin.h
* Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/
#ifndef _H_EWLADMIN
#define _H_EWLADMIN

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

//+ EWL01 @jbotero 2/02/2016
/* --------------------------------------------------------------------------
 * Function Name:	EWL_SetEmvHandle
 * Description:		Destroy and Set new EMV handle pointer
 * Author:			@jbotero
 * Parameters:
 * 	- nothing
 * Return:
 * 	- (!= NULL) OK, pointer of ewlObject_t*
 * 	- (== NULL) Error
 * Notes:
 */

ewlObject_t * EWL_SetEmvHandle(void);

/* --------------------------------------------------------------------------
 * Function Name:	EWL_GetEmvHandle
 * Description:		Get EMV handle pointer
 * Author:			@jbotero
 * Parameters:
 * 	- nothing
 * Return:
 * 	- (NULL) Error
 * 	- (Other) OK, pointer ewlObject_t
 * Notes:
 */

ewlObject_t * EWL_GetEmvHandle(void);

/* --------------------------------------------------------------------------
 * Function Name:	EWL_DestroyEmvHandle
 * Description:		Destroy EMV handle pointer
 * Author:			@jbotero
 * Parameters:
 * 	- nothing
 * Return:
 * 	- nothing
 * Notes:
 */

void EWL_DestroyEmvHandle(void);
//- EWL01

#endif //_H_EWLADMIN
