/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Tables.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_TABLES
#define _H_TABLES

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define DISK_TABLES "/APPTBLS"
#define PACKED __attribute__((aligned (1), packed))


/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/
typedef enum
{
	TAB_GENERAL_CONF,

	TAB_END = 0xFFFF,
}TAB_LIST;


/* ***************************************************************************
 * TABLE: GENERAL CONFIGURATION
 * ***************************************************************************/
// length defines
#define L_PABX			16
#define LZ_PABX			L_PABX+1
#define L_APN			50
#define LZ_APN			L_APN+1
#define L_IS_ID 		10
#define LZ_IS_ID 		L_IS_ID+1
#define L_HOST 			100+1
#define LZ_HOST			L_HOST+1
#define L_PORT 			5
#define LZ_PORT 		L_PORT+1
#define L_PHONE 		20
#define LZ_PHONE 		L_PHONE+1
#define L_LOGIN 		20
#define LZ_LOGIN 		L_LOGIN+1
#define L_SSLPROFILE	9
#define LZ_SSLPROFILE	L_SSLPROFILE+1
#define L_LID 			16
#define LZ_LID 			L_LID+1
#define L_NII 			3
#define LZ_NII 			L_NII+1
#define L_TECHID		6
#define LZ_TECHID		L_TECHID+1
#define L_LINE_IDLE		40
#define LZ_LINE_IDLE	L_LINE_IDLE + 1
// table typedef
typedef struct tagTAB_GENERAL_CONF
{
	RECORD_HEADER Header;
	// general fields
	char	Dial_PABX[LZ_PABX];				// dial PABX
	char 	GPRS_APN[LZ_APN];				// GPRS APN
	char 	GPRS_User[LZ_LOGIN];			// GPRS PPP User name
	char 	GPRS_Pass[LZ_LOGIN];			// GPRS PPP password
	uint8 	TimeOut;						// general time out

	// IngEstate
	uint8 	iS_CommType;					// communication type: (1=dial | 2=ip/eth/wifi | 3= gprs)
	char 	iS_ID[LZ_IS_ID];					// campaign id
	char 	iS_Host[LZ_HOST];				// IngEstate IP or URL
	char	iS_Port[LZ_PORT];				// IngEstate port
	char 	iS_Phone[LZ_PHONE];				// IngEstate phone
	char	iS_PPP_User[LZ_LOGIN];			// PPP user name
	char	iS_PPP_Pass[LZ_LOGIN];			// PPP password
	bool	iS_SSL;							// SSL flag
	char	iS_SSL_Profile[LZ_SSLPROFILE];	// name of SSL Profile
	//+ LLCOMMS01 @mamata Jun 15, 2015
	uint8	iS_SSLVersion;					// SSL version - Default = 0 (SSLv3)(see details in LLComms.h)
	long int iS_SSLCipher2;					// SSL cipher mode - Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)(see details in LLComms.h)
	long int iS_SSLExportMask2;				// SSL export and mask - Defualt = 0 (SSL_HIGH | SSL_NOT_EXP)(see details in LLComms.h)
	uint8    FlagSSLProfSigned;				// Flag to define if the profile is signed or not (Not signed leaves it on HOST if not the certificate needs to be put on swap signed)
	//- LLCOMMS01

	// iP-Server
	char	iP_ID[LZ_LID];					// init ID
	uint8	iP_CommType;					// communication type: : (1=dial | 2=ip/eth/wifi | 3= gprs)
	char	iP_NII[LZ_NII];					// NII
	uint8	iP_Retries;						// connection retries
	uint8	iP_ConnectTO;					// time out for connect try
	uint8	iP_RspTO;						// time out for host response
	char	iP_Phone1[LZ_PHONE];			// primary phone
	char	iP_Phone2[LZ_PHONE];			// Secondary phone
	char 	iP_Host[LZ_HOST];				// iP-Server host IP or URL
	char	iP_Port[LZ_PORT];				// iP-Server port
	bool 	iP_SSL;							// Flag to use SSL "0" = No SSL (default) "1" = Use SSL
	char	iP_SSL_Profile[LZ_SSLPROFILE];	// name of SSL Profile
	bool	iP_Report;						// Print Report after init
	//+ LLCOMMS01 @mamata Jun 15, 2015
	uint8	iP_SSLVersion;					// SSL version - Default = 0 (SSLv3)(see details in LLComms.h)
	long int iP_SSLCipher2;					// SSL cipher mode - Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)(see details in LLComms.h)
	long int iP_SSLExportMask2;				// SSL export and mask - Default = 0 (SSL_HIGH | SSL_NOT_EXP)(see details in LLComms.h)
	//- LLCOMMS01

	//+ LLCOMMS01 @mamata Jun 15, 2015
	uint8 	Dial_Tone;						// dial tone (LLC_TONE | LLC_PULSE)
	bool	Dial_DetectTone;				// flag to enable/disable detection of dial tone (LLC_ENABLE | LLC_DISABLE)
	bool	Dial_DetectBusy;				// flag to enable/disable detection if line is busy (LLC_ENABLE | LLC_DISABLE)
	uint8	Dial_ModemMode;					// modem mode (LLC_MODEM_MODE_ASYNC | LLC_MODEM_MODE_FASTCONNECT | LLC_MODEM_MODE_V22_V22BIS)
	uint8	Dial_BlindDialPause;			// in blind dial mode - pause before dial (seconds)
	uint8	Dial_ToneTO;		// pause before tone detection (seconds)
	uint8	Dial_PABXPause;					// pause for PBAX using "," (seconds)
	//- LLCOMMS01

	//+ DUALSIM @mamata Jun 24, 2015
	char 	GPRS2_APN[LZ_APN];				// GPRS APN for second sim
	char 	GPRS2_User[LZ_LOGIN];			// GPRS PPP User name for second sim
	char 	GPRS2_Pass[LZ_LOGIN];			// GPRS PPP password for second sim
	//- DUALSIM

	// user fields

}PACKED GENERAL_CONF;
// table length
#define L_GENERAL_CONF sizeof(GENERAL_CONF)
// table name
#define TAB_GENERAL_CONF "GCONF"


/* ***************************************************************************
 * TABLE: TERMINAL
 * ***************************************************************************/
// length defines
#define L_TERM_LINE						42
#define LZ_TERM_LINE					L_TERM_LINE+1
#define L_TERM_PASSWORD					10
#define LZ_TERM_PASSWORD				L_TERM_PASSWORD+1
#define L_TERM_CURR_SYMBOL				4
#define LZ_TERM_CURR_SYMBOL				L_TERM_CURR_SYMBOL+1
#define L_TERM_LABEL					16
#define LZ_TERM_LABEL					L_TERM_LABEL+1
#define L_TERM_PAN_MASK					4
#define LZ_TERM_PAN_MASK				L_TERM_PAN_MASK+1
#define L_USER_FIELD					100
#define LZ_USER_FIELD					L_USER_FIELD+1
#define L_KEY_NAME						40
#define LZ_KEY_NAME						L_KEY_NAME + 1
#define NRO_COPIAS						2 + 1

#define TERM_FLAG1_MULTIMERCHANT		OPT_01
#define TERM_FLAG1_BLOCK_KEYBOARD		OPT_02
#define TERM_FLAG1_ENABLE_ECR			OPT_03
#define TERM_FLAG1_SERVER				OPT_04
#define TERM_FLAG1_MANUAL_CONF			OPT_05
#define TERM_FLAG1_SETTLE_ALL			OPT_06
#define TERM_FLAG1_PRINT_RECEIPT		OPT_07
#define TERM_FLAG1_PRE_PRINT			OPT_08
#define TERM_FLAG1_PRINT_LOGO			OPT_09
#define TERM_FLAG1_PRINT_CARDHOLDER		OPT_10
#define TERM_FLAG1_PRINT_TICKET			OPT_11
#define TERM_FLAG1_PRINT_DEBITO         OPT_12
#define TERM_FLAG1_PRINT_CREDITO        OPT_13
#define TERM_FLAG1_PRINT_RECHAZADO      OPT_14
#define TERM_FLAG1_SETTLE_OBLIGADO		OPT_15
#define TERM_FLAG1_VERIFICACION			OPT_16


// PCL modes
typedef enum PCL_mode
{
	PCL_MODE_OFF = 0,
	PCL_MODE_ON,
}PCL_mode;

typedef enum PCL_printer_mode
{
	PCL_PRINTER_ON = 0,
	PCL_PRINTER_OFF,

}PCL_print_mode;

// table typedef
typedef struct tagTABLE_TERMINAL
{
	RECORD_HEADER Header;
	// general fields
	char	Header_Line1[LZ_TERM_LINE];
	char	Header_Line2[LZ_TERM_LINE];
	char	Header_Line3[LZ_TERM_LINE];
	char	Header_Line4[LZ_TERM_LINE];
	char	Password_Merchant[LZ_TERM_PASSWORD];
	char	Password_Technician[LZ_TERM_PASSWORD];
	//+ CURRENCY01 @mamata Dec 16, 2014
	char	CurrSymbol_Local[LZ_TERM_CURR_SYMBOL];
	char	CurrSymbol_Dollar[LZ_TERM_CURR_SYMBOL];
	//d char	CurrSymbol_Local[L_TERM_CURR_SYMBOL];
	//d char	CurrSymbol_Dollar[L_TERM_CURR_SYMBOL];
	//- CURRENCY01
	//+ CLESS @mamata Dec 15, 2014
	//+ CURRENCY01 @mamata Dec 16, 2014
	char	CurrCode_Local[LZ_TERM_CURR_SYMBOL];
	char 	CurrCode_DOLAR[LZ_TERM_CURR_SYMBOL];
	//- CURRENCY01
	//d char	CurrSymbol_Euro[L_TERM_CURR_SYMBOL];
	//- CLESS
	char	User_Label1[LZ_TERM_LABEL];
	char	User_Label2[LZ_TERM_LABEL];
	char	User_Label3[LZ_TERM_LABEL];
	char	User_Label4[LZ_TERM_LABEL];
	char	Footer_Line1[LZ_TERM_LINE];
	char	Footer_Line2[LZ_TERM_LINE];
	char	Footer_Line3[LZ_TERM_LINE];
	char	Footer_Line4[LZ_TERM_LINE];
	char	Disclaimer_Line1[LZ_TERM_LINE];
	char	Disclaimer_Line2[LZ_TERM_LINE];
	char	Disclaimer_Line3[LZ_TERM_LINE];
	char	Disclaimer_Line4[LZ_TERM_LINE];
	char	PanMask[LZ_TERM_PAN_MASK];
	uint8	TimeOut;
	uint16	Flags1;
	uint16	Flags2;
	uint16	User_Flags1;
	uint16	User_Flags2;
	char	User_Password1[LZ_TERM_PASSWORD];
	char	User_Password2[LZ_TERM_PASSWORD];
	char	User_Password3[LZ_TERM_PASSWORD];
	char	User_Password4[LZ_TERM_PASSWORD];
	char	User_Field01[LZ_USER_FIELD];
	char	User_Field02[LZ_USER_FIELD];
	char	User_Field03[LZ_USER_FIELD];
	char	User_Field04[LZ_USER_FIELD];
	char	User_Field05[LZ_USER_FIELD];
	char	User_Field06[LZ_USER_FIELD];
	char	User_Field07[LZ_USER_FIELD];
	char	User_Field08[LZ_USER_FIELD];
	char	User_Field09[LZ_USER_FIELD];
	char	User_Field10[LZ_USER_FIELD];
	char	User_Field11[LZ_USER_FIELD];
	char	User_Field12[LZ_USER_FIELD];
	char	User_Field13[LZ_USER_FIELD];
	char	User_Field14[LZ_USER_FIELD];
	char	User_Field15[LZ_USER_FIELD];
	char	User_Field16[LZ_USER_FIELD];
	char	User_Field17[LZ_USER_FIELD];
	char	User_Field18[LZ_USER_FIELD];
	char	User_Field19[LZ_USER_FIELD];
	char	User_Field20[LZ_USER_FIELD];
	char	DataKeyName	[LZ_KEY_NAME];
	char	PinKeyName	[LZ_KEY_NAME];
	char	NameC[LZ_TERM_LABEL];
	PCL_mode modePCL;
	uint8 Comercio;
	uint16 Idebito;
	uint16 Icredito;
	uint16 Irechazado;
	char   NCopias       [NRO_COPIAS];
	char	RifC[LZ_TERM_LABEL];
//	char	Amount_Pin_Debit[12+1];
}PACKED TABLE_TERMINAL;
// table length
#define L_TABLE_TERMINAL sizeof(TABLE_TERMINAL)
// table name
#define TAB_TERMINAL "TERM"



/* ***************************************************************************
 * TABLE: MERCHANT
 * ***************************************************************************/
// length defines
#define L_MERCH_NAME					16
#define LZ_MERCH_NAME					L_MERCH_NAME+1
#define L_MERCH_TID						8
#define LZ_MERCH_TID					L_MERCH_TID+1
#define L_MERCH_LID						15
#define LZ_MERCH_LID					L_MERCH_LID+1
#define L_MERCH_PASSWORD				4
#define LZ_MERCH_PASSWORD				L_MERCH_PASSWORD+1
#define L_MERCH_LINE					42
#define LZ_MERCH_LINE					L_MERCH_LINE+1
#define L_MERCH_BATCH_NAME				20
#define LZ_MERCH_BATCH_NAME				L_MERCH_BATCH_NAME+1
#define L_FTRAX_PARTNER_ID				30
#define LZ_FTRAX_PARTNER_ID				L_FTRAX_PARTNER_ID+1
#define L_FTRAX_DCCPROFILE_ID			40
#define LZ_FTRAX_DCCPROFILE_ID			L_FTRAX_DCCPROFILE_ID+1
#define L_FTRAX_DCCMERCHANT_NUM			40
#define LZ_FTRAX_DCCMERCHANT_NUM		L_FTRAX_DCCMERCHANT_NUM+1
#define L_RIF_NUMBER					32
#define LZ_RIF_NUMBER					L_RIF_NUMBER+1
#define L_HOST_TPDU						10
#define LZ_HOST_TPDU					L_HOST_TPDU+1


#define MERCH_TRAN1_SALE				OPT_01
#define MERCH_TRAN1_VOID				OPT_02
#define MERCH_TRAN1_REFUND				OPT_03
#define MERCH_TRAN1_SALE_OFFLINE		OPT_04
#define MERCH_TRAN1_ADJUST				OPT_05
#define MERCH_TRAN1_BALANCE				OPT_06
#define MERCH_TRAN1_AUTH				OPT_07
#define MERCH_TRAN1_LODGIN				OPT_08
#define MERCH_TRAN1_PAYMENTS			OPT_09
#define MERCH_TRAN1_SALE_CASH			OPT_10
#define MERCH_TRAN1_CASH_ADVANCE		OPT_11
#define	MERCH_TRAN1_DEBIT				OPT_12
//+ CLESS @mamata Dec 11, 2014
#define MERCH_TRAN1_QUICK_SALE			OPT_13
//- CLESS

#define MERCH_FLAG1_TIP					OPT_01
#define MERCH_FLAG1_TIP_GUIDE			OPT_02
#define MERCH_FLAG1_WAITER				OPT_03
#define MERCH_FLAG1_TABLE				OPT_04
#define	MERCH_FLAG1_TAX					OPT_05
#define MERCH_FLAG1_ADJUST_BASE_AMOUNT	OPT_06
#define MERCH_FLAG1_PRINT_DISCLAIMER	OPT_07
#define MERCH_FLAG1_RENTA_CAR			OPT_08
#define MERCH_FLAG1_QUICK_PAYMENT		OPT_09
#define MERCH_FLAG1_TX_NEGADA			OPT_10


/* ***************************************************************************
 * TABLE: PRESTADOR
 * ***************************************************************************/
#define	L_PRES_NAME			21
#define	LZ_PRES_NAME		L_PRES_NAME+1
#define	L_PRES_MERCAHNT		20
#define	LZ_PRES_MERCAHNT	L_PRES_MERCAHNT+1

// table typedef
typedef struct tagTABLE_MERCHANT
{
	RECORD_HEADER Header;
	char	Name[LZ_MERCH_NAME];
	uint16	HostID;
	char	TID[LZ_MERCH_TID];
	char	MID[LZ_MERCH_LID];
	uint8	Currency;
	char	Password[LZ_MERCH_PASSWORD];
	char	Header_Line1[LZ_MERCH_LINE];
	char	Header_Line2[LZ_MERCH_LINE];
	char	Header_Line3[LZ_MERCH_LINE];
	char	Header_Line4[LZ_MERCH_LINE];
	uint8	Amount_MaxDigtis;
	uint8	Amount_DecimalPosition;
	uint8	TIP_type;
	uint8	TIP_Max_Percent;
	uint8	TIP_Guide1;
	uint8	TIP_Guide2;
	uint8	TIP_Guide3;
	uint8	TAX_type;
	uint8	TAX_Max_Percent;
	uint8 	Adjust_Max_Percent;
	uint8	Defualt_Transaction;
	uint32	CurrentBatch;
	uint64	Amount_Max;
	uint64	QuickPayment;
	uint8	Mortages;
	uint16	Transactions1;
	uint16	Transactions2;
	uint16	Flags1;
	uint16	Flags2;
	uint16	User_Flags1;
	uint16	User_Flags2;
	char	User_Password1[LZ_MERCH_PASSWORD];
	char	User_Password2[LZ_MERCH_PASSWORD];
	char	User_Password3[LZ_MERCH_PASSWORD];
	char	User_Password4[LZ_MERCH_PASSWORD];
	char	TPDU[LZ_HOST_TPDU+1];
	char	MIDC[LZ_MERCH_LID];
	char	MIDA[LZ_MERCH_LID];
	char	User_Field01[LZ_USER_FIELD];
	char	User_Field02[LZ_USER_FIELD];
	char	User_Field03[LZ_USER_FIELD];
	char	User_Field04[LZ_USER_FIELD];
	char	User_Field05[LZ_USER_FIELD];
	char	User_Field06[LZ_USER_FIELD];
	char	User_Field07[LZ_USER_FIELD];
	char	User_Field08[LZ_USER_FIELD];
	char	User_Field09[LZ_USER_FIELD];
	char	User_Field10[LZ_USER_FIELD];
	char	User_Field11[LZ_USER_FIELD];
	char	User_Field12[LZ_USER_FIELD];
	char	User_Field13[LZ_USER_FIELD];
	char	User_Field14[LZ_USER_FIELD];
	char	User_Field15[LZ_USER_FIELD];
	char	User_Field16[LZ_USER_FIELD];
	char	User_Field17[LZ_USER_FIELD];
	char	User_Field18[LZ_USER_FIELD];
	char	User_Field19[LZ_USER_FIELD];
	char	User_Field20[LZ_USER_FIELD];
	uint16	MerchantGroupID;
	char	BatchName[LZ_MERCH_BATCH_NAME];
	uint32	BatchNumber;
	bool 	BatchSettle;
	DATE	BatchLastSettle;
	// general fields
	uint16	GroupRange;
	//- INGESTATE01
	char 	RIF[LZ_RIF_NUMBER];
	char 	Name2[LZ_MERCH_NAME];
	uint16	TransPassw;
	uint64 Amount_MaxDigtisLote; //variable para el maximo por lote
}PACKED TABLE_MERCHANT;
// table length
#define L_TABLE_MERCHANT sizeof(TABLE_MERCHANT)
// table name
#define TAB_MERCHANT "MERCH"


/* ***************************************************************************
 * TABLE: HOST
 * ***************************************************************************/
// length defines
#define L_HOST_NAME						16
#define LZ_HOST_NAME					L_HOST_NAME+1
#define L_HOST_NII						3
#define LZ_HOST_NII						L_HOST_NII+1
#define L_HOST_DESKEY					16
#define LZ_HOST_DESKEY					L_HOST_DESKEY+1
#define L_HOST_PHONE 					20
#define LZ_HOST_PHONE 					L_HOST_PHONE+1
#define L_HOST_ACQUIRER_ID 				32
#define LZ_HOST_ACQUIRER_ID				L_HOST_PHONE+1

#define HOST_FLAG1_DETECT_LINE			OPT_01
#define	HOST_FLAG1_NO_UPDATE_HOST_TIME	OPT_02
#define HOST_FLAG1_USE_3DES				OPT_03
#define HOST_FLAG1_USE_DUKPT			OPT_04
#define HOST_FLAG1_ENABLE_COMM_BACKUP	OPT_05
#define	HOST_FLAG1_EMV					OPT_06

// table typedef
typedef struct tagTABLE_HOST
{
	RECORD_HEADER Header;
	char	Name[LZ_HOST_NAME];
	uint8	CommunicationType;
	uint8	MessageFormat;
	uint8 	ConnectionMode;
	uint8 	ConnectionRetries;
	uint8 	ConnectionTimeOut;
	uint8 	ResponseTimeOut;
	char	NII[LZ_HOST_NII];
	char	EncrypedWorkingKey1[LZ_HOST_DESKEY];
	char	EncrypedWorkingKey2[LZ_HOST_DESKEY];
	uint8	Dial_Tone;
	//+ HOST01 @mamata Aug 14, 2014
	char	Dial_Primary_Phone1[L_HOST_PHONE];
	char	Dial_Primary_Phone2[L_HOST_PHONE];
	char	Dial_Backup_Phone1[L_HOST_PHONE];
	char	Dial_Backup_Phone2[L_HOST_PHONE];
	uint16	IP_Primary_Address1;
	uint16	IP_Primary_Address2;
	uint16	IP_Backup_Address1;
	uint16	IP_Backup_Address2;
	//- HOST01
	uint16	Flags1;
	uint16 	User_Flags1;
	uint16	GPRS;
	char	User_Field01[LZ_USER_FIELD];
	char	User_Field02[LZ_USER_FIELD];
	char	User_Field03[LZ_USER_FIELD];
	char	User_Field04[LZ_USER_FIELD];
	char	User_Field05[LZ_USER_FIELD];
	char	User_Field06[LZ_USER_FIELD];
	char	User_Field07[LZ_USER_FIELD];
	char	User_Field08[LZ_USER_FIELD];
	char	User_Field09[LZ_USER_FIELD];
	char	User_Field10[LZ_USER_FIELD];
	char	User_Field11[LZ_USER_FIELD];
	char	User_Field12[LZ_USER_FIELD];
	char	User_Field13[LZ_USER_FIELD];
	char	User_Field14[LZ_USER_FIELD];
	char	User_Field15[LZ_USER_FIELD];
	char	User_Field16[LZ_USER_FIELD];
	uint8	BackupCommunicationType;
	uint8	MasterKeyIndex;
	char	Acquirer_ID[LZ_HOST_ACQUIRER_ID];
	char    RIF[LZ_RIF_NUMBER];
	uint8	Fallback;
}PACKED TABLE_HOST;
// table length
#define L_TABLE_HOST sizeof(TABLE_HOST)
// table name
#define TAB_HOST "HOST"

/* ***************************************************************************
 * TABLE: CURRENCY INFO
 * ***************************************************************************/

typedef struct
{
	char Type;								//CURRENCY_LOCAL or CURRENCY_DOLAR
	char exp;								//Currency exponent(Number of decimals)
	char CodeASC	[3 + 1];				//Currency Code using ISO format(ASC format)
	char CodeBCD	[2 + 1];				//Currency Code using ISO format(BCD format)
	char Symbol		[LZ_TERM_CURR_SYMBOL];	//Currency symbol

} CURRENCY_INFO;

/* ***************************************************************************
 * TABLE: RANGE
 * ***************************************************************************/
// length defines
#define L_RANGE_NAME					16
#define LZ_RANGE_NAME					L_RANGE_NAME+1

#define RANGE_FLAG1_DEBIT				OPT_01
#define RANGE_FLAG1_MANUAL_ENTRY		OPT_02
#define RANGE_FLAG1_VERIFY_EXP_DATE		OPT_03
#define RANGE_FLAG1_VERIFY_CHECK_DIGIT	OPT_04
#define RANGE_FLAG1_VERIFY_CVV2			OPT_05
#define RANGE_FLAG1_VERIFY_4DBC			OPT_06
#define RANGE_FLAG1_LAST_4_PAN			OPT_07
#define RANGE_FLAG1_PROMPT_PIN			OPT_08
#define RANGE_FLAG1_PROMPT_ACCOUNT_TYPE	OPT_09

// table typedef
typedef struct tagTABLE_RANGE
{
	RECORD_HEADER Header;
	uint16	HostID;
	char	Name[LZ_RANGE_NAME];
	uint64	Bin_Min;
	uint64	Bin_Max;
	uint8	Max_PAN_Length;
	uint8	DefaultAccount;
	uint16	Flags1;
	uint16	User_Flags1;
	char	ShortDescriptor[LZ_USER_FIELD];
	char	User_Field01[LZ_USER_FIELD];
	char	User_Field02[LZ_USER_FIELD];
	char	User_Field03[LZ_USER_FIELD];
	char	User_Field04[LZ_USER_FIELD];
	char	User_Field05[LZ_USER_FIELD];

	//+ INGESTATE01 @jbotero 13/09/2015
	uint16	Enabled;
	//- INGESTATE01
	uint8	Fallback;
	uint8	Swipe;
}PACKED TABLE_RANGE;
// table length
#define L_TABLE_RANGE sizeof(TABLE_RANGE)
// table name
#define TAB_RANGE "RANGE"


// length defines
#define L_BANK_NAME					16
#define LZ_BANK_NAME					L_BANK_NAME+1

// table typedef
typedef struct tagTABLE_BANK
{
	RECORD_HEADER Header;
	uint16	HostID;
	char	Name[LZ_BANK_NAME];
}PACKED TABLE_BANK;
// table length
#define L_TABLE_BANK sizeof(TABLE_BANK)
// table name
#define TAB_BANK "BANK"

/* ***************************************************************************
 * TABLE: IP
 * ***************************************************************************/
// length defines
#define L_IP_HOST						200
#define LZ_IP_HOST						L_IP_HOST+1
#define L_IP_SSL_PROFILE				9
#define LZ_IP_SSL_PROFILE				L_IP_SSL_PROFILE+1

#define IP_FLAG1_ADD_HEX_LENGTH			OPT_01
#define IP_FLAG1_ADD_TPDU				OPT_02
#define IP_FLAG1_ENABLE_SSL				OPT_03
#define IP_FLAG1_SSL_ATUH_CLIENT		OPT_04


#define MAX_LEN_SSL_CERTIFICATE_NAME 15
#define FILE_CERT_ROOT_TLS		"CACERT"
#define ARCHIVO_CERT_MUTUA		"/SYSTEM/CLCERT.PEM"
#define ARCHIVO_PRIV_KEY_MUTUA	"/SYSTEM/CLKEY.PEM"
#define TXT_ERR_VALIDATING_CERTIFICATE "ERROR Validando\nCertificado"
#define TXT_CERTIFICATE_NOT_LOADED     "%s.PEM\nNO CARGADO"

// table typedef
typedef struct tagTABLE_IP
{
	RECORD_HEADER Header;
	char	Host[LZ_HOST];
	char 	Port[LZ_PORT];
	uint16	Flags1;
	uint16	User_Flags1;
	char	IP_GPRS[LZ_HOST];
	char	PORT_GPRS[LZ_PORT];
	char	User_Field03[LZ_USER_FIELD];
	char	User_Field04[LZ_USER_FIELD];
	char	User_Field05[LZ_USER_FIELD];
	char	Server_SSL_Profile[LZ_IP_SSL_PROFILE];
	char	Client_SSL_Profile[LZ_IP_SSL_PROFILE];
}PACKED TABLE_IP;
// table length
#define L_TABLE_IP sizeof(TABLE_IP)
// table name
#define TAB_IP "IPTAB"



/* ***************************************************************************
 * TABLE: MERCH_RANGE
 * ***************************************************************************/
// length defines

// table typedef
typedef struct tagTABLE_MERCH_RANGE
{
	RECORD_HEADER Header;
	uint16	MerchantID;
	uint16	RangeID;
}PACKED TABLE_MERCH_RANGE;
// table length
#define L_TABLE_MERCH_RANGE sizeof(TABLE_MERCH_RANGE)
// table name
#define TAB_MERCH_RANGE "MERCH_RANGE"
/* ***************************************************************************
 * TABLE: OPCIONES DE COMERCIO
 * ***************************************************************************/
typedef struct tagOPCIONES_MENU
{
	RECORD_HEADER Header;
	bool	OP_PROPINA;
	bool	OP_PORCENTAJE_PREAUTORIZA;
	bool	OP_DIAS_PREAUTORIZA;
}PACKED OPCIONES_MENU;
// table length
#define L_OPCIONES_MENU sizeof(OPCIONES_MENU)
// table name
#define TAB_OPCIONES_MENU "OPCIONES DE MENU"
/* ***************************************************************************
 * TABLE: MERCH_GROUP
 * ***************************************************************************/
// length defines
#define L_MERCH_GROUP_NAME				16
#define LZ_MERCH_GROUP_NAME				L_MERCH_GROUP_NAME+1

// table typedef
typedef struct tagTABLE_MERCH_GROUP
{
	RECORD_HEADER Header;
	char	Name[LZ_MERCH_GROUP_NAME];
}PACKED TABLE_MERCH_GROUP;
// table length
#define L_TABLE_MERCH_GROUP sizeof(TABLE_MERCH_GROUP)
// table name
#define TAB_MERCH_GROUP "MERCH_GROUP"


/* ***************************************************************************
 * TABLE: COUNTERS
 * ***************************************************************************/
// table typedef
typedef struct tagTABLE_COUNTERS
{
	RECORD_HEADER Header;
	uint32 Invoice;
	uint32 STAN;
}PACKED TABLE_COUNTERS;
// table length
#define L_TABLE_COUNTERS sizeof(TABLE_COUNTERS)
// table name
#define TAB_COUNTERS "COUNTERS"

/* ***************************************************************************
 * TABLE: KEY_INFO
 * ***************************************************************************/

typedef enum
{
	KEY_TYPE_PIN = 1,
	KEY_TYPE_DATA,
	KEY_TYPE_MAC,

} e_KeyType;

// table typedef
typedef struct
{
	RECORD_HEADER Header;
	char 	Name[LZ_KEY_NAME];
	int8	AlgoType;
	int 	SecretArea;
	uint16	MKNumber;
	uint16 	WKNumber;
	uint32 	BankId;

}PACKED TABLE_KEY_INFO;

// table length
#define L_TABLE_KEY_INFO sizeof(TABLE_KEY_INFO)
// table name
#define TAB_PIN_KEY_INFO "PINKEY"
// table name
#define TAB_DATA_KEY_INFO "DATAKEY"

/* ***************************************************************************
 * TABLE: STATISTICS
 * ***************************************************************************/
// length defines

// table typedef
typedef struct tagTABLE_STATISTICS
{
	RECORD_HEADER Header;
	int			MsgSend;  // Incrementa por cada mensaje enviado por el POS. Incluye todos los mensajes: control, transacciones y cargas
	int			MsgRecive; // Incrementa por cada mensaje recibido por el POS. Incluye todos los mensajes: control, transacciones y cargas
	int			Transac; // Incrementa por cada transacci�n financiera aprobada y procesada por el terminal
	int			Redials; // ** Pendiente sumarlo por que la libreria ** Incrementa por cada marcado; excluyendo la primera marcada
	int			ErrorCom; // Incrementa por cada transacci�n con problemas de comunicaci�n. Ej.: Tel�fono o IP incorrecta, l�nea ocupada, no hay tono para marcar
	int			TimeoutTransac; // Incrementa por cada transacci�n que no recibe respuesta, incluye timeout de reversas
	int			TimeoutRever; // Incremente por cada reversos autom�tico que no recibe respuesta
	int			Timeout8Seg; // Incrementa transacciones con respuestas menores a 8 segundos
	int			Timeout16Seg; // Incrementa transacciones con respuestas menores a 16 segundos
	int			Timeout45Seg; // Incrementa transacciones con respuestas menores a 45 segundos
	int			Timeout90Seg; // Incrementa transacciones con respuestas menores a 90 segundos
	int			TransPrimaryIp; // ** Pendiente sumarlo por que la libreria ** Incrementa por cada transacci�n financiera procesada usando el n�mero de tel�fono o ip primarios
	int			TransSecundaryIp; // ** Pendiente sumarlo por que la libreria ** Incrementa por cada transacci�n financiera procesada usando el n�mero de tel�fono o ip secundario
	int			TransPrimaryIp2; // ** Pendiente sumarlo por que la libreria ** Incrementa por cada marcaci�n al tel�fono xprimario; en el segundo intento
	int			TransSecundaryIp2; // ** Pendiente sumarlo por que la libreria ** Incrementa por cada marcaci�n al tel�fono secundaria; en el segundo intento
	int			MagCardReaderError; // ** Falta saber que tipo de errores ** Incrementa por cada lectura de error de una tarjeta de banda magn�tica
	int			MagCardReader; // Incrementa por cada lectura de tarjetas; incluye exitosas y no exitosas
	int			ActiveBatchNum; // Numero de cierres realizados
}PACKED TABLE_STATISTICS;
// table length
#define L_TABLE_STATISTICS sizeof(TABLE_STATISTICS)
// table name
#define TAB_MERCH_STATISTICS "STATISTICS"

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

//+ INGESTATE01 @jbotero 13/09/2015
/* ***************************************************************************
 * TABLE: GROUP_RANGE
 * ***************************************************************************/

// table typedef
typedef struct tagTABLE_GROUP_RANGE
{
	RECORD_HEADER Header;
	uint16	IdGroupRange;
	char	NameGroupRange[LZ_MERCH_GROUP_NAME];
	uint16	IdRange;

}PACKED TABLE_GROUP_RANGE;

// table length
#define L_TABLE_GROUP_RANGE sizeof(TABLE_GROUP_RANGE)

// table name
#define TAB_GROUP_RANGE "GROUP_RANGE"
//- INGESTATE01

// table typedef
typedef struct tagTABLE_MESERO
{
	RECORD_HEADER Header;
	uint32	IdMesero;

}PACKED TABLE_MESERO;
// table name
#define L_TABLE_MESERO sizeof(TABLE_MESERO)

#define TAB_MESERO "MESERO"
// table length


/* --------------------------------------------------------------------------
 * Function Name:	Tables_Init
 * Description:		Init all tables
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int Tables_Init(void);


/* --------------------------------------------------------------------------
 * Function Name:	BuildRangeCandidateList
 * Description:		Build candidate list of ranges using TAG_PAN
 * Parameters:
 *  - TLV_TREE_NODE Tree_RangeCandidateList - where to place candidate list
 * Return:
 *  - RET_OK - creates candidate list of ranges
 *  - RET_NOK - empty candidate list
 *  -2 - no PAN in Tran_Tree
 *  -3 - tlvtree error
 * Notes:
 */
int BuildRangeCandidateList(TLV_TREE_NODE Tree_RangeCandidateList);


/* --------------------------------------------------------------------------
 * Function Name:	BuildMerchantCandidateList
 * Description:		Build candidate list of Merchants using candidte list of Range
 * Parameters:
 *  - TLV_TREE_NODE Tree_MerchantCL - where to place candide list
 *  - TLV_TREE_NODE Tree_RangeCL - candidate list of ranges
 *  - uint32 MerchantGroupID - Merchant group filter
 *  - bool *MultiCurrency - OUT - flag to inform exists muti currency in candidate list of Merchants
 * Return:
 * Notes:
 */
int BuildMerchantCandidateList(TLV_TREE_NODE Tree_MerchantCL, TLV_TREE_NODE Tree_RangeCL, uint32 MerchantGroupID, bool *MultiCurrency);


/* --------------------------------------------------------------------------
 * Function Name:	Counters_Get_Invoice
 * Description:		Get a invoice number and increment for nest time
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get invoice number
 *  >0 - invoice number
 * Notes:
 */
uint32 Counters_Get_Invoice(void);


/* --------------------------------------------------------------------------
 * Function Name:	Counters_Increment_Invoice
 * Description:		Increment invoice number
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get invoice number
 *  >0 - invoice number
 * Notes:
 */
uint32 Counters_Increment_Invoice(void);


/* --------------------------------------------------------------------------
 * Function Name:	Counters_Get_STAN
 * Description:		Get a STAN and increment for nest time
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get STAN
 *  >0 - STAN
 * Notes:
 */
uint32 Counters_Get_STAN(void);

/* --------------------------------------------------------------------------
 * Function Name:	Counters_Get_Current_STAN
 * Description:		Get the current STAN(don't increases in memory)
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get STAN
 *  >0 - STAN
 * Notes:
 */
uint32 Counters_Get_Current_STAN(void);

/* --------------------------------------------------------------------------
 * Function Name:	IsTransactionEnable
 * Description:		Verify if a transaction in enable in almost one merchant
 * Parameters:
 * 	- uint16 TranID - transaction ID
 * Return:
 *  TRUE
 *  FALSE
 * Notes:
 */
bool IsTransactionEnable(uint16 TranID);


/* --------------------------------------------------------------------------
 * Function Name:	Merchant_IsTransactionEnable
 * Description:		Verify if a transaction is enable in a specific merchant
 * Parameters:
 * 	- TABLE_MERCHANT Merchant - Merchant to verify
 * 	- TRANS_LIST TranID - transaction to verify
 * Return:
 *  - true - transaction is enable in Merchant
 *  - false - transaction isn't enable in Merchant
 * Notes:
 */
bool Merchant_IsTransactionEnable(uint16 TranFlags1, uint16 TranFlags2, uint16 TranID);

/* --------------------------------------------------------------------------
 * Function Name:	TableTerminal_GetDataKeyInfo
 * Description:		Get key info from TMS tables assigned to terminal
 * Author:			@jbotero
 * Parameters:		TABLE_KEY_INFO * stKeyInfo: Pointer to structure Key info
 * Return:
 * Notes:
 */

int TableTerminal_GetKeyInfo(e_KeyType KeyType, TABLE_KEY_INFO * stKeyInfo);

/* --------------------------------------------------------------------------
 * Function Name:	Table_GetCurrencyInfo
 * Description:		Get currency info of selected merchant
 * Author:			@jbotero
 * Parameters:
 * - TLV_TREE_NODE Tree: Transaction tree
 * - CURRENCY_INFO * pStCurrencyInfo: Output structure with currency information
 *
 * Return:
 * - RET_OK: Processed OK
 * - RET_NOK: Error!!
 * Notes:
 */
int Table_GetCurrencyInfo(TLV_TREE_NODE Tree, CURRENCY_INFO * pStCurrencyInfo);

int ADD_TRANSACCION_MESERO(TLV_TREE_NODE tree);
//grupo de copmercio
int Init_Table_Bank(void);
#endif //_H_TABLES
