/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		CoreDefines.h
 * Description:		Global defines for iPOSApp
 * Author:			mamata
 * Notes:			this is a iPOSApp core file, don't modify this file please
 * 					Este es un archivo core de iPOSApp, no modificar por favor
 * --------------------------------------------------------------------------*/
#ifndef _H_CORE_DEFINES
#define _H_CORE_DEFINES


/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/

/* ***************************************************************************
 * TYPEDEFS
 * ***************************************************************************/
// signed integers
typedef char 				int8;
typedef short 				int16;
typedef long 				int32;
typedef long long			int64;

// unsigned integers
typedef unsigned char		uint8;
typedef unsigned short 		uint16;
typedef unsigned long 		uint32;
typedef unsigned long long	uint64;

// amount
typedef unsigned long long	amount;

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define CHECK(CND,LBL) {if(!(CND)){goto LBL;}}
#ifndef _IPA280_DEF_H_
#define RET_OK	0
#endif //_IPA280_DEF_H_
#define RET_NOK	-1
#define RET_CANCEL	-2
#define RET_PRIM_TX	1
#define RET_BANDA_CHIP	5

#define	OPT_01		0x0001
#define	OPT_02		0x0002
#define	OPT_03		0x0004
#define	OPT_04		0x0008
#define	OPT_05		0x0010
#define	OPT_06		0x0020
#define	OPT_07		0x0040
#define	OPT_08		0x0080
#define	OPT_09		0x0100
#define	OPT_10		0x0200
#define	OPT_11		0x0400
#define	OPT_12		0x0800
#define	OPT_13		0x1000
#define	OPT_14		0x2000
#define	OPT_15		0x4000
#define	OPT_16		0x8000

#endif //_H_CORE_DEFINES
