/*****************************************************************************
 Property of INGENICO
 *****************************************************************************/
/*
 * PROJECT  : CarCAAn Base Application
 * MODULE   : LL Communications
 * FILEMANE : LL_Comms.c
 * PURPOSE  : Contains functions to perform communications using Dial, Eht
 *            and Gprs connections
 *
 * Version	: 0203
 * SDK:		: 9.28.0.01
 * HISTORY
 *
 * date 		author 		modifications
 * 2012-02-22	mdm			Creation
 * 2016-03-23	mamata		Fix to support error -1015 after call LL_GPRS_Connect
 * 							Error detected: the terminal try to connect to GPRS network, it can't
 * 							and when the terminal return to idle the GSM/GPRS signal disappears from idle screen
 * 							and the terminal can't connec to to GPRS after force a restart
 * 2016-04-25	mamata		Update to SDK 9.28.1.02
 * 2016-06-30	mamata		Set timeout fot with GSM signal from 20 seconds to 60 seconds
 * 							Add support to count modem connections in 1200 and 2400 in statistics module
 *
 *****************************************************************************/

#if !defined( LLCOMMS_H )
#define LLCOMMS_H

#include "CoreDefines.h"

/*---******* DEFINES *****************************************************---*/
// LLC Connection Type
#define LLC_DISABLE			0
#define LLC_DIAL			1
#define	LLC_IP				2
#define	LLC_GPRS			3
#define LLC_WIFI			4

//+ PCL_PROXY	angelv	Jun 21, 2017
#define LLC_PCL				5
//- PCL_PROXY

// LLC
#define LLC_CNX_PRIM		1
#define LLC_CNX_SEC			2

// LLC Returns
#define	LLC_RET_OK					0
#define LLC_RET_CONNECTED			1
#define LLC_RET_HANGUP				2

// LLC Errors
#define	LLC_ERR_NOIPS				1	// No IPs to configure
#define	LLC_ERR_CNXCONFIG			2	// Error to perform the configuration of LL Connection
#define LLC_ERR_NETWORK				3	// Network error
#define LLC_ERR_NOTCONNECTED		4
#define LLC_ERR_SEND				5
#define LLC_ERR_RECEIVE				6

// LLC_STATUS
#define LLC_STATUS_CONNECTING		1
#define LLC_STATUS_CONNECTED		2
#define LLC_STATUS_ERROR			3
#define LLC_STATUS_HANGUP			4
#define LLC_STATUS_NOINIT			5
#define LLC_STATUS_CONNECTING_NET	6


#define LLC_ENABLE					1
#define LLC_DISABLE					0

#define LLC_MODEM_MODE_ASYNC		1
#define LLC_MODEM_MODE_FASTCONNECT 2
#define LLC_MODEM_MODE_V22_V22BIS	3

#define LLC_TONE					1
#define LLC_PULSE					2

#define LLC_V22_1200				1
#define LLC_V22BIS_2400				2
#define LLC_V32_9600				3

//+ CTRLVERSION @aambrosio 09/08/2016, TLVTREE LIBRARY VERSION CONTROL
#ifndef TAG_LIB_INFO
///Tags definition
 #define	TAG_LIB_INFO			0x0100	//TLVTree MAIN NODE
 #define	TAG_LIB_NAME			0x0101	//Length: 6.	5 Name + 1 '_'
 #define	TAG_LIB_VERSION			0x0102	//Length: 6.
 #define	TAG_LIB_DATE			0x0103	//Length: 8.	AAAAMMDD
 #define	TAG_LIB_SDK				0x0104	//Length: 8.
 #define	TAG_LIB_UNATTENDED		0x0105	//Length: 8.
 #define	TAG_LIB_TECHNOLOGY		0x0106	//Length: 2.	T2/T3
 #define	TAG_LIB_RELEASEMODE		0x0107	//Length: 1.	TRUE if release mode.
 #define	TAG_LIB_EP2CLESS		0x0108	//Length: 8.
 #define	TAG_LIB_EP2EMV			0x0109	//Length: 8.

///Error codes definition
 #define	ERR_LIBVERSION_OK					0
 #define	ERR_LIBVERSION_TLVNOTINITILIZED		-1
 #define	ERR_LIBVERSION_ERRADDINGCHILD		-2
#endif
//- CTRLVERSION

typedef struct TABLE_HOST_CONNECTION
{
	// general settings
	uint8		CommType;		// communication type (LLC_DISABLE | LLC_DIAL |  LLC_IP | LLC_GPRS | LLC_WIFI)
	uint8		Retries;		// retries number to connect to host
	uint8		TimeoutConn;	// timeout to connect to host
	uint8		TimeoutAns;		// timeout to receive response from host

	// dial settings
	char		Phone1[25];		// phone number 1
	char		Phone2[25];		// phone number 2

	// IP1 settings
	char		Address1[200];	// Address 1 (IP or URL)
	char		Port1[6];		// 	 port number
	uint8		bSSL1;			// 	 flag to enable/disable SSL (0 = disable, 1 = enable)
	char		SSLProf1[15];	// 	 SSL profile name
	uint8		SSLVersion1;	// 	 SSL version (SSLv2, SSLv3, TLSv1, SSLv23, TLSv1_1, TLSv1_2)
								//     Default = 0 (SSLv3)
	long int	SSLCipher1;		//	 Bitmap for Mask of supported cipher
								//     (SSL_RSA, SSL_DSS, SSL_DES, SSL_3DES, SSL_RC4, SSL_RC2, SSL_AES, SSL_MD5, SSL_SHA1, SSL_SHA256, SSL_SHA384)
								//     Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)
	long int 	SSLExportMask1;	//   Bitmap for mask between export mode and the cipher strength
								//     Export information (SSL_NOT_EXP, SSL_EXPORT)
								//     Cipher strength information (SSL_STRONG_NONE, SSL_EXP40, SSL_MICRO, SSL_EXP56, SSL_MINI, SSL_LOW, SSL_MEDIUM, SSL_HIGH)
								//     Default = 0 (SSL_HIGH | SSL_NOT_EXP)

	// IP2 settings
	char		Address2[200];	// Address 2 (IP or URL)
	char		Port2[6];		// 	 port number
	uint8		bSSL2;			// 	 flag to enable/disable SSL (0 = disable, 1 = enable)
	char		SSLProf2[15];	// 	 SSL profile name
	uint8		SSLVersion2;	// 	 SSL version (SSLv2, SSLv3, TLSv1, SSLv23, TLSv1_1, TLSv1_2)
								//     Default = 0 (SSLv3)
	long int	SSLCipher2;		//	 Bitmap for Mask of supported cipher
								//     (SSL_RSA, SSL_DSS, SSL_DES, SSL_3DES, SSL_RC4, SSL_RC2, SSL_AES, SSL_MD5, SSL_SHA1, SSL_SHA256, SSL_SHA384)
								//     Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)
	long int 	SSLExportMask2;	//   Bitmap for mask between export mode and the cipher strength
								//     Export information (SSL_NOT_EXP, SSL_EXPORT)
								//     Cipher strength information (SSL_STRONG_NONE, SSL_EXP40, SSL_MICRO, SSL_EXP56, SSL_MINI, SSL_LOW, SSL_MEDIUM, SSL_HIGH)
								//     Default = 0 (SSL_HIGH | SSL_NOT_EXP)

	// general IP settings
	uint8		bAddHexLength;	// flag to add hex length

	//+ TETRA_MIGRATION	Jan 30, 2017	angelv
	uint8		bPackSegment;	// flag to package segmentation, for GPRS the maxim length is 536, for ETH 2048, with the flag enable you could receive more than that.
	//- TETRA_MIGRATION

	//+ HEXLEN4B @mamata Apr 17, 2019
	uint8		Use4BytesHexLen;	// type of hex length
									// 0 = 2 bytes (default)
									// 1 = 4 bytes
	//- HEXLEN4B
}HOST_CONNECT;

typedef struct TABLE_CONFIG
{
	// host connections
	HOST_CONNECT First_Connection;		// configuration of fist connection
	HOST_CONNECT Second_Connection;		// configuration of second connection

	// dial settings
	uint8	Dial_Tone;					// Dial tone [1=Tone, 2=Pulse]
	uint8	Dial_bDetectTone;			// Detect dial tone [0=Disable - 1=Enable] (0=Blind Dial)
	uint8	Dial_bDetectBussy;			// Detect if line is busy [0=Disable - 1=Enable]
	uint8	Dial_ModemMode;				// Use FastConnect [LLC_MODEM_MODE_ASYNC, LLC_MODEM_MODE_FASTCONNECT, LLC_MODEM_MODE_V22_V22BIS]
	uint8	Dial_BlindDialPause;		// Pause before dial when blind dial is enable
	uint8	Dial_DialToneTO;			// Pause before dial tone detection
	char	Dial_PABX[10];				// PABX Code
	uint8	Dial_PABX_Pause;			// PABX pause in seconds
	uint8	Dial_Modulation;			// Line modulation [ LLC_V22_1200 | LLC_V22BIS_2400 | LLC_V32_9600]

	// GPRS/3G settings
	char 	GPRS_APN[100];				// GPRS/3G apn
	char 	GPRS_Usr[25];				// user for GPRS/3G PPP
	char	GPRS_Pass[25];				// password for GPRS/3G PPP

	// Custom messages
	char	TXT_Dial1[25];				// message to show when is dialing first number
	char	TXT_Dial2[25];				// message to show when is dialing second number
	char	TXT_IP1[25];				// message to show when is connecting first ip via ETH
	char	TXT_IP2[25];				// message to show when is connecting second ip via ETH
	char	TXT_GPRS1[25];				// message to show when is connecting first ip via GPRS/3G
	char	TXT_GPRS2[25];				// message to show when is connecting second ip via GPRS/3G
	char	TXT_Send[25];				// message to show when is sending data to host
	char	TXT_Receive[25];			// message to show when is receiving data from host
	char	TXT_DisconnectGPRS[25];		// message to show when is disconnecting from GPRS network
} LLCOMS_CONFIG;

/*---******* PUBLIC FUNCTIONS  *******************************************---*/
/*****************************************************************************
 * Func Name:		LLC_StartConnection
 * Description: 	Initialize LLC Thead
 * Parameters:
 *   - LLCOMS_CONFIG *Confing pointer to configuration struct
 * Returns:
 *   - RET_OK		The operation has been successfully processed
 *   - RET_NOK		Error in operation, for detail see nLastError
 **************************************************************************--*/
int16 LLC_StartConnection(LLCOMS_CONFIG *Confing);


/*****************************************************************************
 * Func Name:		LLC_WaitConnection
 * Description: 	With for Connection, TIME OUT or ERROR
 * Parameters:
 *   - bool - flag to enable show status messages
 * Returns:
 *   - LLC_STATUS_CONNECTED		Connected to host
 *   - LLC_STATUS_ERROR			Error in connection, , for detail see nLastError
 *   - LLC_STATUS_HANGUP		HangUP is setted
 **************************************************************************--*/
int16 LLC_WaitConnection(bool ShowMessages);


/*****************************************************************************
 * Func Name:		LLC_EndConnection
 * Description: 	HangUp connection
 * Parameters:
 *   - none
 * Returns:
 *   - LLC_RET_OK	HangUp Ok
 **************************************************************************--*/
int16 LLC_EndConnection( void );

/*****************************************************************************
 * Func Name:		LLC_Send
 * Description: 	Send message to host
 * Parameters:
 *   - TXBuffer		Buffer to be send
 *   - TXSize		Size of TXBuffer
 * Returns:
 *   - LLC_RET_OK	Send Ok
 *   - LLC_ERR_SEND Error in send buffer, for detail see nLastError
 **************************************************************************--*/
int16 LLC_Send( uint8 * TXBuffer,  int TXSize );


/*****************************************************************************
 * Func Name:		LLC_Send
 * Description: 	Send message to host
 * Parameters:
 *   - RXBuffer		Buffer to put receive data
 *   - TXSize		Size of data
 * Returns:
 *   - LLC_RET_OK		Send OK
 *   - LLC_ERR_RECEIVE 	Error in receive buffer, for detail see nLastError
 **************************************************************************--*/
int16 LLC_Receive( uint8 * RXBuffer,  int *RXSize );


/*****************************************************************************
 * Func Name:		LLC_GetLastError
 * Description: 	Get last error
 * Parameters:
 * Returns:
 *   - LinkLayer Error
 **************************************************************************--*/
int16 LLC_GetLastError( int16 *LastError );


/*****************************************************************************
 * Func Name:	LLC_SetSSLCA
 * Description: Configure SSL profile
 * Parameters:
 *   - SSLCert				Profile to be configured
 *   - int SSLVersion		(SSLv2, SSLv3, TLSv1, SSLv23, TLSv1_1, TLSv1_2)
 *   						Default = 0 (SSLv3)
 *
 *   - long int Cipher 		(SSL_RSA, SSL_DSS, SSL_DES, SSL_3DES, SSL_RC4, SSL_RC2, SSL_AES, SSL_MD5, SSL_SHA1, SSL_SHA256, SSL_SHA384)
 *   					 	Default = 0 (SSL_RSA | SSL_3DES | SSL_SHA1 | SSL_AES)
 *
 *   - long int ExportMask	Export information (SSL_NOT_EXP, SSL_EXPORT)
 *   						Cipher strength information (SSL_STRONG_NONE, SSL_EXP40, SSL_MICRO, SSL_EXP56, SSL_MINI, SSL_LOW, SSL_MEDIUM, SSL_HIGH)
 *   						Default = 0 (SSL_HIGH | SSL_NOT_EXP)
 *
 * Returns:
 *   - RET_OK	The operation has been successfully processed
 *   - RET_NOK	Error in configuration, for detail see nLastError
 **************************************************************************--*/
int LLC_SetSSLCA(char *SSLCert, int SSLVersion, long int Cipher, long int ExportMask);


/*****************************************************************************
 * Func Name:		LLC_StopGPRS
 * Description: 	stop gprs connection
 * Parameters:
 *   - none
 * Returns:
 *   - none
 **************************************************************************--*/
void LLC_StopGPRS(void);

//+ CTRLVERSION @aambrosio 02/12/2016
/* --------------------------------------------------------------------------
 * Function Name:	CryptoLib_getLibInfo
 * Description:	Gets the library information.
 * Parameters:
 *  @param tlvLibVersion	output TLVTree info
 *
 * Return:
 * 	ERR_LIBVERSION	codes
 * Notes:
 * - tlvLibVersion must be initialized before calling this function
 * - tlvLibVersion must be released after used
 *
 * 02/12/2016
 * @aambrosio
 *
 #define	TAG_LIB_NAME			"LLCOM_"
 #define	TAG_LIB_VERSION			"020501"
 #define	TAG_LIB_DATE			"20171122"
 #define	TAG_LIB_SDK				"11161PB"
 #define	TAG_LIB_TECHNOLOGY		"T2"
 #define 	ISELF_COMPILATION		"06040001"

*/
int LLComms_getLibInfo(TLV_TREE_NODE tlvLibVersion);
//- CTRLVERSION

#endif //LLCOMMS_H

