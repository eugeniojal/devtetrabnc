/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Configuration.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_CONFIGURATION
#define _H_CONFIGURATION

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
//+ LLCOMMS01 @mamata Jun 17, 2015
typedef enum ConfigurationEStepList
{
	STEP_PABX,
	STEP_GPRS_APN,
	STEP_GPRS_USER,
	STEP_GRPS_PASS,
	STEP_TIME_OUT,
	STEP_IP_TERMINAL,
	STEP_IP_COMM_TYPE,
	STEP_IP_NII,
	STEP_IP_RETRIES,
	STEP_IP_CONNECT_TO,
	STEP_IP_RESPONSE_TO,
	STEP_IP_PHONE1,
	STEP_IP_PHONE2,
	STEP_IP_HOST,
	STEP_IP_PORT,
	STEP_IP_SSL,
	STEP_IP_REPORT,
	STEP_GCONF_REPEAT,
	STEP_SAVE_REPEAT_INGELOAD,
	STEP_DIAL_DETECT_TONE,
	STEP_DIAL_DETECT_BUSY,
	STEP_DIAL_MODEM_MODE,
	STEP_DIAL_BLIND_PAUSE,
	STEP_DIAL_TONE_TO,
	STEP_DIAL_PABX_PAUSE,
	//+ DUALSIM @mamata Jun 24, 2015
	STEP_SELECT_SIM,
	STEP_GPRS2_APN,
	STEP_GPRS2_USER,
	STEP_GRPS2_PASS,
	//- DUALSIM

	//+ INGESTATE01 @jbotero 13/09/2015
	STEP_IS_COMM_TYPE,
	STEP_IS_HOST,
	STEP_IS_PORT,
	STEP_IS_TERMINAL,
	STEP_IS_PHONE,
	STEP_IS_PPP_USER,
	STEP_IS_PPP_PASS,
	STEP_IS_SSL,
	STEP_IS_SSL_PROFILE,
	STEP_IS_SSL_VERSION,
	STEP_SAVE_REPEAT_INGESTATE,
	//- INGESTATE01

	STEP_CONFIG_COMM,

	STEP_LOCAL_MODE,
	STEP_LOCAL_IP,
	STEP_LOCAL_MASK,
	STEP_LOCAL_GATEWAY,
	STEP_LOCAL_DNS1,
	STEP_LOCAL_DNS2,

	STEP_ETH_LOAD_CONFIG,
	STEP_ETH_APPLY_CONFIG,
	STEP_PCL_MODE,
	STEP_PCL_APPLY_CONFIG,
	STEP_NAME_TERMINAL,
	STEP_TYPE_APLICATION,
	STEP_IMPRIME,
	STEP_IMPRIME_DEBITO,
	STEP_IMPRIME_CREDITO,
	STEP_PRE_IMPRIME,
	STEP_IMPRIME_RECHAZADO,
	STEP_IMPRIME_LOGO,
	STEP_NUMERO_COPIAS,
	STEP_IS_COMM_TYPE_HOST,
	STEP_LOCAL_IP_HOST,
	STEP_IS_PORT_HOST,
	STEP_IS_SSL_HOST,
	STEP_SAVE_COMUNICACION,
	STEP_MODIFY_TERMINAL,
	STEP_SELECT_NUMBER_MERCHANT,
	STEP_MODIFY_AFILIADO,
	STEP_MODIFY_TPDU,
	STEP_MODIFY_NOMBREC,
	STEP_MODIFY_RIF,
	STEP_TONE_PULSE,
	STEP_MODIFY_AFILIADO_CREDITO,
	STEP_MODIFY_AFILIADO_AMEX,
	STEP_MODIFY_PASS_TECNICO,
	STEP_PASS_TECNICO,
	STEP_MODFY_TECNICO,
	STEP_MODIFY_MERCHANT,
	STEP_SAVE_PASSWORD,
	STEP_LOCAL_IP_HOST_GPRS,
	STEP_IS_PORT_HOST_GPRS,
//	#if(TETRA_HARDWARE == 1)
//	#else
	STEP_WIFI_SSID,
	STEP_WIFI_SECURITY,
	STEP_WIFI_KEY_PASS,
	STEP_WIFI_ADDPROFILE,
	STEP_WIFI_LOAD_CONFIG,
	STEP_WIFI_APPLY_CONFIG,
//	#endif//TETRA_HARDWARE
	STEP_CAMBIO_MONEDA,

}CONFIGURATION_ESTEP_LIST;
//- LLCOMMS01

extern SMT_STEP CONF_STEP_LIST[];
/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/


/* ***************************************************************************
 * TABLE: GENERAL CONFIGURATION
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* --------------------------------------------------------------------------
 * Function Name:	Init_TAB_GENERAL_CONF
 * Description:		Init table Init_TAB_GENERAL_CONF
 * 					if table is not created init tConf with default parameters
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file or from defualt parameters
 *  - RET_NOK - cannot init TAB_GENERAL_CONF
 * Notes:
 */
int Init_TAB_GENERAL_CONF(void);


/* --------------------------------------------------------------------------
 * Function Name:	Menu_GeneralConfiguration
 * Description:		Run menu of general configuration
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int Menu_GeneralConfiguration(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_PABX
 * Description:		Pre function of STEP_PABX
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_PABX(void);


/* --------------------------------------------------------------------------
 * Function Name:	STEP_GPRS_APN
 * Description:		Pre function of STEP_GPRS_APN
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GPRS_APN(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_GPRS_USER
 * Description:		Pre function of STEP_GPRS_USER
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GPRS_USER(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_GRPS_PASS
 * Description:		Pre function of STEP_GRPS_PASS
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GRPS_PASS(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TIME_OUT
 * Description:		Pre function of STEP_TIME_OUT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_TIME_OUT(void);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TIME_OUT
 * Description:		Post function of STEP_TIME_OUT
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_Commit_UINT8(char *Result);

/* --------------------------------------------------------------------------
 * Function Name: GenerateAndSave
 * Description: Defines a EM configuration and saves it on TMS Manager
 * Parameters:
 * -none
 * Return:
 * 	- RET_OK
 * 	- RET_NOK - Error on the saving process
 * Notes:
 */

int GenerateAndSave();




/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_TERMINAL
 * Description:		Pre function of STEP_IP_TERMINAL
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_IP_TERMINAL(void);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IP_COMM_TYPE
 * Description:		Post function of STEP_IP_COMM_TYPE
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IP_COMM_TYPE(char *Result);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_NII
 * Description:		Pre function of STEP_IP_NII
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_NII(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_RETRIES
 * Description:		Pre function of STEP_IP_RETRIES
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_RETRIES(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_CONNECT_TO
 * Description:		Pre function of STEP_IP_CONNECT_TO
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_CONNECT_TO(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_RESPONSE_TO
 * Description:		Pre function of STEP_IP_RESPONSE_TO
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_RESPONSE_TO(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_PHONE1
 * Description:		Pre function of STEP_IP_PHONE1
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_PHONE1(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_PHONE2
 * Description:		Pre function of STEP_IP_PHONE2
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_PHONE2(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_INPUT_IP
 * Description:		Pre function of INPUT_IP
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_INPUT_IP(void);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_PORT
 * Description:		Pre function of STEP_IP_PORT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_PORT(void);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IP_SSL
 * Description:		Post function of STEP_IP_SSL
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IP_SSL(char *Result);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IP_REPORT
 * Description:		Post function of IP_REPORT
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IP_REPORT(char *Result);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SAVE_GENERAL_CONF
 * Description:		Post function of STEP_SAVE_IPTABLE
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_SAVE_GENERAL_CONF(char *Result);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SAVE_INGELOAD
 * Description:		Post function of STEP_SAVE_REPEAT_INGELOAG
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_SAVE_INGELOAD(char *Result);


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TECHNICIAN_PASSWORD
 * Description:		Post function of STEP_SAVE_REPEAT_INGELOAG
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_TECHNICIAN_PASSWORD(char *Result);


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TECHNICIAN_PASSWORD
 * Description:		Pos funciton for STEP_TECHNICIAN_PASSWORD
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_TECHNICIAN_PASSWORD(void);


//+ DUALSIM @mamata Jun 24, 2015
/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentGPRSAPN
 * Description:		Get APN for current sim
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  char* - pointer to current APN
 * Notes:
 */
char *GetCurrentGprsAPN(void);


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentGprsUser
 * Description:		Get ppp user for current sim
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  char* - pointer to current ppp user
 * Notes:
 */
char *GetCurrentGprsUser(void);


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentGprsPassword
 * Description:		Get ppp password for current sim
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  char* - pointer to current ppp password
 * Notes:
 */
char *GetCurrentGprsPassword(void);


/* --------------------------------------------------------------------------
 * Function Name:	GprsSwitchSIM
 * Description:		function for switch sim card
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  RET_OK;
 *  RET_NOK;
 * Notes:
 */
int GprsSwitchSIM(void);

/* --------------------------------------------------------------------------
 * Function Name:	GprsInitSIMs
 * Description:		Init SIM cards
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  none
 * Notes:
 */
void GprsInitSIMs(void);
//- DUALSIM

#endif //_H_CONFIGURATION


int MenuTerminal(void);
int MenuComunicacion(void);
