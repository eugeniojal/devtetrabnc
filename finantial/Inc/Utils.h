/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Utils.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_UTILS
#define _H_UTILS

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
typedef enum EConverType
{
	_FORMAT_TRIM,
	_FORMAT_RIGHT,
	_FORMAT_LEFT
}CONVERT_TYPE;

/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/


/* ***************************************************************************
 * TABLE: GENERAL CONFIGURATION
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	Ascii_to_Hex
 * Description:		Convert Ascii number to Hex number
 * 					Example: to convert "54732" to 0x05 0x47 0x32
 * Parameters:
 *  - uint8 *HexBuffer - pointer to hex buffer (out)
 *  - uint8* AsciiBuffer - pointer to ascii buffer (in)
 *  - uint32 AsciiLength - length of ascii buffer (in) Don't include 0
 * Return:
 *  RET_OK if conversion is OK
 *  other value if error occurred
 * Notes:
 */
int Ascii_to_Hex(uint8 *HexBuffer, uint8* AsciiBuffer, uint32 AsciiLength);


/* --------------------------------------------------------------------------
 * Function Name:	Uint64_to_Ascii
 * Description:		Convert uint64 to string, trimmed or justified with fill char
 * Parameters:
 *  - uint64 pSurce	- number to be converter
 *  - char *pDest - where to place the number converted
 *  - CONVERT_TYPE pFormat - _UTIL_TRIM, _UTIL_RIGHT _UTIL_LEFT
 *  - uint16 pLength - length to be clear and fill with pFill
 *  - uint8 pFill - char, character to fill
 * Return:
 * Notes:
 */
int Uint64_to_Ascii(uint64 pSurce, char *pDest, CONVERT_TYPE pFormat, uint16 pLength, uint8 pFill);

/** ------------------------------------------------------------------------------
 * Function Name:	Format_String_Lenght
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Create a string whith lenght format
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - (-1) pLength = 0
 *  - (-2) pFormat unknown
 *  - (-3) pLength < lenght of pSurce
 * Notes:
 */
int Format_String_Lenght(char *pSurce, char *pDest, CONVERT_TYPE pFormat, uint16 pLength, uint8 pFill);


/* --------------------------------------------------------------------------
 * Function Name:	GetTrack2
 * Description:		Get track2 from swipe2 file, and extract track fields
 * Parameters:
 *  - Telium_File_t *hSwipe2 - pointer to swipe2 file
 * Return:
 *  - RET_OK - the track is correct and put in Tree_Tran
 *  - RET_NOK - cannot read track
 * Notes:
 */
int GetTrack2(uint8 *tcTmp);

/* --------------------------------------------------------------------------
 * Function Name:	CheckTrack2
 * Description:		Check and extract field from Track 2
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - Track 2 is correct
 *  - RET_NOK - Track 2 is not correct
 * Notes:
 */
int CheckTrack2( void );

/* --------------------------------------------------------------------------
 * Function Name:	GetTrack1
 * Description:		Get track1 from swipe31 file, and extract track fields
 * Parameters:
 *  - Telium_File_t *hSwipe2 - pointer to swipe2 file
 * Return:
 *  - RET_OK - the track is correct and put in Tree_Tran
 *  - RET_NOK - cannot read track
 * Notes:
 */
int GetTrack1(uint8 *tcTmp);

/* --------------------------------------------------------------------------
 * Function Name:	CheckTrack1
 * Description:		Check and extract field from Track 1
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - Track 2 is correct
 *  - RET_NOK - Track 2 is not correct
 * Notes:
 */
int CheckTrack1(void);

/* --------------------------------------------------------------------------
 * Function Name:	Get_RangeData
 * Description:		get the RangeDate of a specific transaction using TAG_RANGE_ID
 * Parameters:
 *  - TABLE_RANGE *Range - (OUT) where to put data, NULL if record didn't find
 *  - TLV_TREE_NODE Tree - tree where to find TAG_RANGE_ID
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - cannot get range data
 * Notes:
 */
int Get_TransactionRangeData(TABLE_RANGE *Range, TLV_TREE_NODE Tree);
int Get_TransactionBankData(TABLE_BANK *Bank, TLV_TREE_NODE Tree);

/* --------------------------------------------------------------------------
 * Function Name:	Get_TransactionMerchantData
 * Description:		get the Merchant Data of a specific transaction using TAG_MERCHANT_ID
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - (OUT) where to put data, NULL if record didn't find
 *  - TLV_TREE_NODE Tree - tree where to find TAG_MERCHANT_ID
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - cannot get merchat data
 * Notes:
 */
int Get_TransactionMerchantData(TABLE_MERCHANT *Merchant, TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	Get_TransactionHostData
 * Description:		get the Host Data of a specific transaction using TAG_HOST_ID
 * Parameters:
 *  - TABLE_HOST *Host - (OUT) where to put data, NULL if record didn't find
 *  - TLV_TREE_NODE Tree - tree where to find TAG_HOST_ID
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - cannot get host data
 * Notes:
 */
int Get_TransactionHostData(TABLE_HOST *Host, TLV_TREE_NODE Tree);


/* --------------------------------------------------------------------------
 * Function Name:	ClearCardData
 * Description:		save initial tags, clear Tree_Tran and restore initial tags
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - error occurred
 * Notes:
 */
int ClearCardData(void);



int Format_Amount(amount Source, char *Dest, char *Currency, bool Negative);


/* --------------------------------------------------------------------------
 * Function Name:	GetSampleTransactionData
 * Description:		Open transaction data stored in /HOST/TRAN.XML and
 * 					perform a tlvtree unserialize in Tree
 * Parameters:
 *  - TLV_TREE_NODE *Tree - Pointer to tlvtree
 * Return:
 *  - RET_OK - transaction data is restored OK
 *  - RET_NOK - cannot restore transaction data
 * Notes:
 */
int GetSampleTransactionData(TLV_TREE_NODE *Tree);

/* --------------------------------------------------------------------------
 * Function Name:	GetEMVTrack2
 * Description:		Get EMV Tags fon EMV transaction of EMVCUSTOM
 * Parameters:
 *  - TLV_TREE_NODE *EMVTranInfo - Emv data node
 * Return:
 *  - RET_OK - EMV Data OK
 *  - RET_NOK - EMV Data NOK
 * Notes:
 */
int GetEMVTrack2(TLV_TREE_NODE EMVTranInfo);

/* --------------------------------------------------------------------------
 * Function Name:	GetTotalAmount
 * Description:		Get total amount of Tree_Tran
 * Parameters:
 * Return:
 *  amount total amount
 * Notes:
 */
amount GetTotalAmount();


/* --------------------------------------------------------------------------
 * Function Name:	FormatPAN
 * Description:		Format PAN with mask configured in Terminal table
 * Parameters:
 *  - uint8 *PAN - clear PAN
 *  - uint8 *FormatPAN - where to put format PAN
 * Return:
 *  - RET_OK
 * Notes:
 */
int FormatPAN(uint8 *PAN, uint8 *FormatPAN);


//+ EMV08 @mamata Jun 10, 2015
/* --------------------------------------------------------------------------
 * Function Name:	SetIdleHeaderFooter
 * Description:		Turn on idle header and footer
 * Parameters:
 *  none
 * Return:
 *  none
 * Notes:
 */
void SetIdleHeaderFooter(void);
//- EMV08

//+ INGESTATE01 @jbotero 13/09/2015
/* --------------------------------------------------------------------------
 * Function Name:	XmlFileTlv_To_Tree
 * Description:		Read a file with TLV Tree format and unserialize on Tree.
 * Author:			@dvelazco, @jbotero
 * Parameters:		TLV_TREE_NODE *Tree: Tree where data will be loaded.
 * 					char *Disk: Disk path where XML file is located. Ex: "/HOST"
 * 					char *FullFileName: XML TLV File name. Ex: "/FILE.XML"
 * Return:
 * Notes:
 */

int XmlFileTlv_To_Tree(TLV_TREE_NODE *Tree, char *Disk, char *FullFileName);

/* --------------------------------------------------------------------------
 * Function Name:	Tree_To_XmlFileTlv
 * Description:		Read a Tree and serialize on file using TLV Tree format.
 * Author:			@dvelazco, @jbotero
 * Parameters:		TLV_TREE_NODE *Tree: Tree where data will be loaded.
 * 					char *Disk: Disk path where XML file is located. Ex: "/HOST"
 * 					char *FullFileName: XML TLV File name. Ex: "/FILE.XML"
 * Return:
 * Notes:
 */

int Tree_To_XmlFileTlv(TLV_TREE_NODE Tree, char *Disk, char *FullFileName);

/* --------------------------------------------------------------------------
 * Function Name:	RebootTerminal
 * Description:		Reboot (ICTXXX) or shutdown(IWLXXX) terminal
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:			nothing
 * Notes:
 */

void RebootTerminal(void);

//- INGESTATE01

//+ EWL01 @jbotero 2/03/2016
/* --------------------------------------------------------------------------
 * Function Name: 	Buzzer
 * Description:		Configurable Beep
 * Author:			@jbotero
 *
 * Parameters:		int nFrequency:
 * 					unsigned char Volume: Volume of beep
 * 					int nDelay_Tick: Duration of beep
 * Return:
 * Notes:
 */
void Buzzer(int nFrequency, unsigned char Volume,int nDelay_Tick);

/* --------------------------------------------------------------------------
 * Function Name:	beepOK
 * Description:		Beep when operation is OK
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */
void beepOK( void );

/* --------------------------------------------------------------------------
 * Function Name:	beepError
 * Description:		Beep when operation is Error
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */
void beepError( void );
//- EWL01

/* --------------------------------------------------------------------------
 * Function Name:	Wait_Key
 * Description:		Wait for a key and return it, or Timeout.
 * Author:			@jbotero
 * Parameters:		IN : Only_CancelOK_Key - Flag to indicate that only wait
 * 					for RED and GREEN Button
 *
 *					IN : timeout - Time to wait in seconds
 *
 * Return:			(0)Timeout Expired
 * 					Other case, key pressed(See T_XXX defines on oem_public_def.h)
 *
 * Notes:			If timeout parameter is '0', wait for key infinitely.
 */

int Wait_Key( uint8 Only_CancelOK_Key, uint32 timeout );

/* --------------------------------------------------------------------------
 * Function Name:	ConvIp_AsciToByteArr
 * Description:		Convert Ip from Ascii to Byte Array(4 bytes)
 * 					example->	10.8.18.22 -> {0x0A, 0x08, 0x12, 0x16 }
 *
 * Author:			@jbotero
 *
 * Parameters:		IN : ipAscii - Ascii Ip, format XXX.XXX.XXX.XXX
 *					OUT: ip_bArray[4] - Ip array 4 bytes.
 * Return:			Nothing
 * Notes:			Nothing
 */

void ConvIp_AsciToByteArr( char * ipAscii, char ip_bArray[4]);

/* --------------------------------------------------------------------------
 * Function Name:	CheckBatteryCharge
 * Description:		Check if terminal have battery, and control levels
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:			nothing
 * Notes:
 */

void CheckBatteryCharge( void );

//+ FIX0002 @pmata	May 11, 2016
/* --------------------------------------------------------------------------
 * Function Name:	GetEMVAmount
 * Description:		Get total amount of Tree_Tran
 * Parameters:
 * Return:
 *  amount total amount
 * Notes:
 */
amount GetEMVAmount();

/* --------------------------------------------------------------------------
 * Function Name:	GetEMVOtherAmount  Cash
 * Description:		Get total amount of Tree_Tran
 * Parameters:
 * Return:
 *  amount total amount
 * Notes:
 */
amount GetEMVOtherAmount();
//- FIX0002

//+ FIX0006 @pmata	May 11, 2016
char GetEMVTransactionType();
//- FIX0006

/** ------------------------------------------------------------------------------
 * Function Name:	_pow
 * Author:			jbotero
 * Date:		 	20/01/2015
 * Description:
 * Parameters:
 *  - fAmount 		Pow implementation
 * Return:
 *  - rounded amount
 * Notes:
 */
float _pow( float val1, int exp );

//+ MERCHSELECT @mamata 5/05/2016
/* --------------------------------------------------------------------------
 * Function Name:	TlvTree_FindChild
 * Description:		Find child in specific node (only search on first tree level)
 * Author:			mamata
 * Parameters:
 *  - const TLV_TREE_NODE hNode - node parent
 *  - unsigned int nTag - tag number
 * Return:
 *  - NULL - nTag do not find
 *  (other) - pointer to node
 * Notes:
 */
TLV_TREE_NODE TlvTree_FindChild(const TLV_TREE_NODE hNode, unsigned int nTag);
//- MERCHSELECT

/* --------------------------------------------------------------------------
 * Function Name:	CipherData
 * Description:		Cipher any data, with padding using spaces = 0x20
 * Parameters:
 *  -
 * Return:
 *  - RET_OK - Data cipher OK
 *  - RET_NOK - Data cipher ERROR
 * Notes:
 */
int CipherData( char * buffer, int lnBuffer, unsigned char EWK[], int EWKLen,
		unsigned char OutputBuff[], int * OutputBuffLen,
		int iOpMode, unsigned char *pStrIV);

/* --------------------------------------------------------------------------
 * Function Name:	CipherData_TLV
 * Description:		Cipher any data from Tag Tlv, with automatic padding
 * Parameters:
 *  -
 * Return:
 *  - RET_OK - Data cipher OK
 *  - RET_NOK - Data cipher ERROR
 * Notes:
 */
int CipherData_TLV( TLV_TREE_NODE Tree, unsigned int TrackTag, unsigned char EWK[],
		int EWKLen, unsigned char OutputBuff[], int * OutputBuffLen,
		int iOpMode, unsigned char *pStrIV);

/* --------------------------------------------------------------------------
 * Function Name:	_Test_EWL_Debug
 * Description:		Module to enable EWL debug.
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */
void _Test_EWL_Debug( void );

/* --------------------------------------------------------------------------
 * Function Name:	IsPCLDoTransaction
 * Description:		Check if POS is using mode PCL do transaction
 * Parameters:
 * Return:
 * Notes:
 */
bool IsPCLDoTransaction(void);

#endif //_H_CONFIGURATION

