/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		Communication.h
* Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/
#ifndef _H_COMMUNICATION
#define _H_COMMUNICATION

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/

/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * TABLE LIST
 * ***************************************************************************/

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Comm_BaseMakeValidation
 * Description:		Check if is necessary make the validation of external base
 * Author:			@jbotero
 * Parameters:
 * Return:
 * 	- true(Make validation)
 * 	- false(Don't make validation)
 * Notes:
 */

bool Comm_BaseMakeValidation( void );

/* --------------------------------------------------------------------------
 * Function Name:	Comm_CradleIsConnected
 * Description:		Check if base is connected
 * Author:			@jbotero
 * Parameters:
 *  - IsShowMsg: Flag to show message if base isn't connected.
 * Return:
 * 	- true(connected)
 * 	- false(Error or not connected)
 * Notes:
 */

bool Comm_CradleIsConnected( bool IsShowMsg );

/* --------------------------------------------------------------------------
 * Function Name:	Comm_BaseConfigure
 * Description:		Associate the current base under terminal
 * Author:			@jbotero
 * Parameters:
 *  - IsShowMsg: Flag to show message if base isn't connected.
 * Return:
 * 	- true(OK)
 * 	- false(Error)
 * Notes:
 */

bool Comm_CradleConfigure( bool IsShowMsg );

/* --------------------------------------------------------------------------
 * Function Name:	Comm_CheckGPRSNetworkInfo
 * Description:		Check GPRS Network Information
 * Author:			@jbotero
 * Parameters:
 * 	- OUT 	T_EGPRS_GET_INFORMATION * GPRSInfo: GPRS status information
 * 	- IN 	timeout: Time to wait connection
 * Return:
 * 	- TRUE:	Get status operation OK.
 * 	- FALSE: Problem to get status or SIM no inserted.
 * Notes:			Nothing
 */

uint8 Comm_CheckGPRSNetworkInfo( T_EGPRS_GET_INFORMATION * GPRSInfo, int timeout );

/* --------------------------------------------------------------------------
 * Function Name:	Comm_Initialize
 * Description:		Initialize the communication Module
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */

uint8 Comm_Initialize( void );

#endif //_H_COMMUNICATION
