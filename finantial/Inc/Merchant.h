/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Merchant.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_MERCHANT
#define _H_MERCHANT

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Menu_GeneralConfiguration
 * Description:		Run menu of general configuration
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Menu_Merchant(void);
int Menu_Transacciones(void);
int Reportes(void);
int ReportesGeneral(void);
int cierres(void);
int ReporteMesero(void);


#endif //_H_MERCHANT
