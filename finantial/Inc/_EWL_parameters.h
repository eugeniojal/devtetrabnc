#ifndef PARAMETER_H
#define PARAMETER_H


typedef struct
{
   char     TTQ[EWL_PAYWAVE_TTQ + 1];

}stPaywave;

int parameterSetTransaction (ewlObject_t *handle, TLV_TREE_NODE Tree);
int parameterSetAids(ewlObject_t *handle);
int ViewAids(void);
int parameterApplicationSpecific (ewlObject_t *ewl, int id );
int parameterSetHost (TLV_TREE_NODE Tree);


#endif
