/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		Totals.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_TOTALS
#define _H_TOTALS

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
typedef struct tagTOTALS
{
	// sale transactions
	uint16 Count;
	amount Amount;

}PACKED RecTOTALS;

typedef RecTOTALS TOTALS[11];

#define Index_SALES		0
#define Index_REFUNDS	1
#define Index_DEBTIS	2
#define Index_RDEBTIS	3
#define Index_C2P		4
#define Index_RC2P		5
#define Index_CUSTOM3	6
#define Index_CUSTOM4	7
#define Index_TAX		8
#define Index_TIP		9

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* --------------------------------------------------------------------------
 * Function Name:	Totals_MerchantCalculate
 * Description:		Calculate totals for a specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - merchant data
 *  - TOTALS *Totals - where to put totals
 *  - uint32 RangeID - filter by range id (0 to disable filter)
 * Return:
 * Notes:
 */
int Totals_MerchantCalculate(TABLE_MERCHANT *Merchant, TOTALS *Totals, uint32 RangeID, bool reporte, bool EntryMode);

int BuscarPalabra(char * Cadena, char * Palabra);
#endif //_H_TOTALS
