/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		IngeLoadOffline.h
 * Description:		functions to initialize offline usign XML file.
 * Author:			dvelazco, jbotero
 * --------------------------------------------------------------------------*/
#ifndef _H_INGELOFF
#define _H_INGELOFF


//+ INGLOFF01 @jbotero 29/07/2015
/** ------------------------------------------------------------------------------
* Function Name: GetXMLInitializationData
* Author: @DFVC
* Date: 06/03/2015
* Description: Lee el archivo INGELOAD.XML de la carpeta host y lo serializa en un arbol
* Parameters:
* - *Tree, puntero del arbol donde se va a guardar el XML serializado
* Return:
* - RET_OK
* - RET_NOK
* Notes:
*/
int GetXMLInitializationData(TLV_TREE_NODE *Tree);

/** ------------------------------------------------------------------------------
* Function Name: RunXMLInitializationData
* Author: @DFVC
* Date: 06/03/2015
* Description: armar un buffer simulando al entregado por Ingeload con la informacion cargada desde el archivo INGELOAD.XML
* Parameters:
* - None
* Return:
* - RET_OK
* - RET_NOK
* Notes:
* - El archivo INGELOAD.XML debe ser de tipo tlvtree
* - Para crear una tabla se debe crear un nodo en el arbol con el identificador 0x01000000 (#define TagTable 0x01000000) y como valor el identificador de la tabla en Ingeload
* - Dentro de cada tabla se debe de tener nodos donde el identificador debe ser el mismo identificador usado por las tablas de Ingeload y dentro el valor que se desea
* - Para conocer los identificadores de Ingeload dirgase al archivo IngeLoad_Unpack.c
*/
int RunXMLInitializationData(void);
//- INGLOFF01


#endif //_H_INGELOFF
