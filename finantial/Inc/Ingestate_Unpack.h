//+ INGESTATE01 @jbotero 13/09/2015
/*
 * PROJECT  : Ingestate POS
 * MODULE   : Initialization
 * FILEMANE : Ingestate_unpack.c
 * PURPOSE  : Contains functions to POS initialization
 *
 * HISTORY
 *
 * date 		author 		modifications
 * 2015-05-21 	@jbotero	creation
*
* --------------------------------------------------------------------------
*/
#ifndef INGESTATE_UNPACK_H
#define INGESTATE_UNPACK_H

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_ProcessPAR
 * Description:		Execute process to unpack file parameters from INGESTATE
 * 					The .PAR file is linked with Application Type
 *
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:			RET_OK(Unpack OK)
 * 					RET_NOK(Unpack fail, check length of fields)
 * 					Others(Customized by developer)
 * Notes:
 */
int Ingestate_ProcessPAR(void);

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_ProcessXML_Offline
 * Description:		Load INGESTATE tables using off-line XML file into HOST folder
 * 					The .PAR file is linked with Application Type
 *
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:
 * Notes:
 */

void Ingestate_ProcessXML_Offline(void);

#endif //INGESTATE_UNPACK_H
//- INGESTATE01


