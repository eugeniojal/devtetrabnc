#ifndef _H_CORE_ISO8583
#define _H_CORE_ISO8583

/******************************************************************************/
/*                                                                            */
/*    Projeto     : Desenvolvimento de Libs Unicapt32                         */
/*    M�dulo      : ISO8583.C                                                 */
/*    Data        : 21/08/02                                                  */
/*    Autor       : EH                                                        */
/*    Coment�rios : M�dulo que cont�m os prot�tipos do m�dulo ISO8583.C       */
/*                                                                            */
/******************************************************************************/

/**
 * @file IdleISO8583.h
 * Functions to packing/unpacking ISO8583 frames.
 *
 * @defgroup ISO8583 ISO8583 packing/unpacking routines.
 * @{
 *    \c \@include "IdleISO8583.h"
 * 
 *    Functions to pack/unpack ISO8583 frames.
 */

/**
 * Type of data for an ISO8583 field
 */
enum DATA_TYPE {
   NPR = 1,    /**< Numeric Packed with Right alignment */
   NPL,        /**< Numeric Packed with Left Alignment */
   BINR,       /**< BINary packed with Right alignment */
   BINL,       /**< BINary packed with Left alignment */
   ANR,        /**< AlphaNumeric packed with Right alignment */
   ANL,        /**< AlphaNumeric packed with Left alignment */
   ND          /**< Not Defined */
};

/**
 * Length format of an ISO8583 field.
 */
enum LEN_TYPE {
   FL = 1,     /**< Fixed Length */
   LL,         /**< LL variable */
   LLL,        /**< LLL variable */
   AA,         /**< AA variable */
   AAA,        /**< AAA variable */
   AAAAAA,
   LLLN
};


/**
 * Special bit types to be used on the bit-map.
 */
enum BitTypes_t {
   B_TPDU       = 999,     /**< Insert the TPDU field */
   B_MSGID      = 998,     /**< Insert the MSGID field */
   B_BMP        = 997      /**< Insert the field's bitmap */
};

/**
 * Constants to inform field alignment.
 */
enum FieldAlignment_t {
   ISO_RIGHT = 1,          /**< Align at right */
   ISO_LEFT = 0            /**< Align at left */
};

/**
 * The TPDU field of a packet.
 */
typedef struct {
   uint8 vbTPDU_ID[2];     /**< TPDU id */
   uint8 vbDestino[4];     /**< Destination address */
   uint8 vbOrigem[4+1];    /**< Origin address */
} TP_TPDU;

/**
 * Each entry in an ISO8583 definition table.
 */
typedef struct {
   int16 i16BitNum;        /**< Bit number */
   uint8 bDataType;        /**< Data type, see @ref DATA_TYPE. */
   uint8 bLenType;         /**< Format of length, see @ref LEN_TYPE. */
   int16 i16Len;           /**< Maximum length of this field */
   uint8 bFill;            /**< Filling character */
   uint8 *vbTxData;        /**< Where to read data to pack */
   uint8 *vbRxData;        /**< Where to store unpacked information */
} TP_TABISO8583;

/**
 * Convert an ASC character to HEX.
 * @param[in] bCharASC Character to be converted.
 * @return HEX character.
 */
char ISO_char_asc_to_hex (char bCharASC);

/**
 * Convert an ASC buffer in HEX.
 * The following rules are followed:
 * \li All alpha chars are uppercased
 * \li Chars in the range ['A','F'] subtract 0x37
 * \li All other chars, do a AND with 0x0F.
 *
 * @param[in]  vbBufASC ASC buffer to be converted.
 * @param[out] vbBufHEX Where to store the HEX values.
 * @param[in]  bCharFill Char to be used as filling, if necessary.
 * @param[in]  ui8Right \c TRUE if aligned right, \c FALSE otherwise.
 */
void ISO_asc_to_hex (char *vbBufASC, char *vbBufHEX, char bCharFill, uint8 ui8Right);

/**
 * Convert an HEX input buffer into ASC.
 * @param[in] vbBufHEX HEX buffer to be converted.
 * @param[out] vbBufASC Where to store the converted data.
 * @param[in]  i16TamBufHEX Number of bytes to be converted from \p vbBufHEX.
 */
void ISO_hex_to_asc (char *vbBufHEX, char *vbBufASC, int16 i16TamBufHEX);

/**
 * Check if a bit on the ISO8583 bitmap is set or not.
 *
 * @param[in] i16Bit    Which bit to check.
 * @param[in] pvbBitmap The ISO8583 bitmap.
 *
 * @return \c TRUE if bit set, \c FALSE otherwise.
 */
uchar ISO_CheckBitMap (int16 i16Bit, uint8 *pvbBitmap);

/**
 * Set a bit on a ISO8583 bitmap.
 *
 * @param[in,out] pvbBitmap   Bitmap to set. Only the selected bit will be changed.
 * @param[in]     i16Bit      Which bit to set.
 */
void ISO_SetBitMap (uint8 *pvbBitmap, int16 i16Bit);

/**
 * Build a buffer with the packaged data from the parameters set on the
 * array of \ref TP_TABISO8583 entries inside \p ptrTabIso.
 *
 * @param[in]  ptrTabISO Array of \ref TP_TABISO8583 elements, terminated with a element
 *                       with zero for \c i16BitNum and \c ND for \c bDataType.
 * @param[out] vbBuf Where to store the built packet.
 * @return Number of bytes written to \p vbBuf.
 * @return Zero in case of error.
 */
int16 i16PackISO8583 (void *ptrTabISO, uint8 *vbBuf);

/**
 * Decode an ISO8583 packet.
 *
 * @param[in,out] ptrTabISO Where to read the expected ISO8583 formatting and
 *                         the pointers to where to stored the extracted data.
 * @param[in]     vbBuf Input buffer with the ISO8583 frame.
 *
 * @return Size of decoded buffer (number of bytes read from \p vbBuf).
 * @return Zero in case of error.
 */
int16 i16UnPackISO8583 (void *ptrTabISO, uint8 *vbBuf);

/* @} */

#endif //_H_CORE_ISO8583
