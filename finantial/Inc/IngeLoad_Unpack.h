/*
* PROJECT  : Ingeload Host
* MODULE   : Ingeload
* FILEMANE : Ingeload_Comms.h
* PURPOSE  : functions to configure Ingeload Server and request
*            parameters file
* 
* HISTORY 
* date			author			modifications
* 2011-10-15  	Mario de Mata   creation
* 
* -------------------------------------------------------------------------- 
*/

#if !defined( INGELOAD_UNPACK_H )
#define INGELOAD_UNPACK_H

/*+++******* DEFINES ****************************************************---*/
/*@DPP 20140522 Add label defines to be used in printed report.*/
#define REPORT_TERMINAL		"\n---- TERMINAL ---- "
#define REPORT_MERCHANT		"\n---- MERCHANT ---- "
#define REPORT_HOST			"---- HOST ---- "
#define REPORT_RANGE		"\n---- RANGE ---- "
#define REPORT_IP			"---- IP ---- "
#define REPORT_MRANGE		"---- MERCHANT - RANGE ---- "

#define L_LINES_PRINT		41
#define	LZ_LINES_PRINT      L_LINES_PRINT + 1

/*+++******* VARIABLES ***************************************************---*/
//char LoadBuffer[10000];
uint64 BufferIndex;

/*+++******* PROTOTYPES **************************************************---*/
uint16 UnpackBuffer( char *DataBuffer,uint32 BufferLength );

/*@DPP 20140522 Add function to print IngeLoad's user report. */
void IngeLoad_PrintReport(void);

#ifdef MAKE_CLAVE
void  fvLoadWK(void);
#endif //MAKE_CLAVE

#endif //INGELOAD_UNPACK_H

