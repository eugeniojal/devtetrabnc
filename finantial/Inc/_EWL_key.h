#ifndef KEY_H
#define KEY_H

typedef struct
{
   byte     keyId;
   byte     rid[EWL_TAG_RID_LEN];
   int      keyExpLen;
   byte     keyExp[EWL_TAG_PK_EXP_MAX_LEN];
   int      keyModulusLen;
   byte     keyModulus[EWL_TAG_PK_MODULUS_MAX_LEN];
}stKey_t;

int keyNumber (void);
stKey_t *keyGet (int id);


#endif
