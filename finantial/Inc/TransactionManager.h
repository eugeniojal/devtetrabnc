/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * Header file:		TransactionManager.h
* Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_TRAN_MANAGER
#define _H_TRAN_MANAGER

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/


/* ***************************************************************************
 * TYPEDEFS
 * ***************************************************************************/
typedef enum ETranList
{
	TRAN_SALE = 0,
	TRAN_REFUND,
	TRAN_VOID,
	TRAN_ADJUST,
	TRAN_REVERSE,
	TRAN_TEST,
	TRAN_SETTLE,
	TRAN_UPLOAD,
	TRAN_OFFLINE,

	//+ CLESS @mamata Dec 11, 2014
	TRAN_QUICK_SALE,
	//- CLESS

	//+ CARCAAN_PINPAD @mamata Jan 14, 2015
	TRAN_PINPAD,
	//- CARCAAN_PINPAD

	//+ PPGETCARD @mamata Jul 1, 2015
	TRAN_PINPAD_CARD,
	//- PPGETCARD
	TRAN_LASTSALE,
	TRAN_WORKINGKEYS,
	TRAN_LOGON,
	LAST_ANSWER,
	DUPLICATE,
	TRAN_C2P,
	TRAN_TEST_AUTO,
	TRAN_END_LIST = 0xFFFF
}TRANS_LIST;

typedef enum EAccountTypeList
{
	ACCOUNTTYPE_DEFAULT = 0,
	ACCOUNTTYPE_SAVINGS,
	ACCOUNTTYPE_CHECKING,
	ACCOUNTTYPE_CREDIT,
	ACCOUNTTYPE_END_LIST = 0xFFFF
}ACCOUNTTYPE_LIST;


/* ***************************************************************************
 * DEFINES
 * ***************************************************************************/
#define FTRAN_OFFLINE			OPT_01
#define FTRAN_REVERSE			OPT_02
#define FTRAN_BATCH				OPT_03
#define FTRAN_VOID				OPT_04
#define FTRAN_ADJUST			OPT_05
#define FTRAN_UPLOAD			OPT_06
#define FTRAN_SWIPE				OPT_07
#define FTRAN_CHIP				OPT_08
#define FTRAN_MANUAL			OPT_09
#define FTRAN_CLESS				OPT_10

#define FSTATUS_NEED_REVERSE	OPT_01
#define FSTATUS_NEED_VOID		OPT_02
#define FSTATUS_NEED_ADJUST		OPT_03
#define FSTATUS_VOIDED			OPT_04
#define FSTATUS_ADJUSTED		OPT_05
#define FSTATUS_OFFLINE			OPT_06

#define FTOTAL_SALE				OPT_01
#define FTOTAL_REFUND			OPT_02
#define FTOTAL_DEBIT			OPT_03
#define FTOTAL_RDEBIT			OPT_04
#define FTOTAL_C2P				OPT_05
#define FTOTAL_RC2P				OPT_06
#define FTOTAL_CUSTOM3			OPT_07
#define FTOTAL_CUSTOM4			OPT_08

typedef struct tagTRAN_INFO
{
	uint16 TRAN_ID;
	uint16 TRAN_NAME;
	uint16 TRAN_FLAG;
	uint16 TRAN_TOTALS;
	uint16 *TRAN_STEPS;
	bool Enable;
}PACKED TRAN_INFO;


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	RunTransaction
 * Description:		Execute transaction listed in TRANS_LIST
 * Parameters:
 *  - TRANS_LIST TranID - Id of transaction to be execute
 *  - S_TRANSIN * param_in
 *  - TLV_TREE_NODE EMPparam_in
 *  - bool Fallback
 *  - bool ClearData
 * Return:
 *  - RET_OK - transaction executed OK
 *  - RET_NOK - transaction canceled or time out ocurred
 *  -1 - cannot run transaction because an tlvtree error ocurred
 *  -2 - transaction not found
 * Notes:
 */
int RunTransaction(TRANS_LIST TranID, S_TRANSIN * param_in, TLV_TREE_NODE EMPparam_in, bool Fallback);


/* --------------------------------------------------------------------------
 * Function Name:	GetTransactionInfo
 * Description:		find and get transaction info from TRANSACTIONS
 * Parameters:
 *  - TRANS_LIST TranID - Transaction ID
 *  - TRAN_INFO *TranInfo - where to put data if found
 * Return:
 *  - RET_OK - transaction found
 *  - RET_NOK - transaction not found
 * Notes:
 */
int GetTransactionInfo(TRANS_LIST TranID, TRAN_INFO *TranInfo);


/* --------------------------------------------------------------------------
 * Function Name:	EnableTransactions
 * Description:		review configuration to enable transactions
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int EnableTransactions(void);

/* --------------------------------------------------------------------------
 * Function Name:	MenuTransaction
 * Description:		Show menu transaction and execute selection
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int MenuTransaction(void);

#endif //_H_TRAN_MANAGER

