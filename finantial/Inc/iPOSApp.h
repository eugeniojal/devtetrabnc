/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * Header file:		iPOSApp.h
 * Description:		Gobal includes for iPOS-App
 * Author:			mamata
 * --------------------------------------------------------------------------*/
#ifndef _H_HEADERS
#define _H_HEADERS
/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "sdk_tplus.h"
#include "TlvTree.h"
#include "LinkLayer.h"
#include "ExtraGPRS.h"
#include "UILib.h"
#include "TableManager.h"
#include "StepManager.h"
#include "SSL_.h"
#include "debug.h"
#include "UILib_debug.h"


#if(TETRA_HARDWARE == 1)
#include "SPMCI_.h"
#else
#include "ipa280.h"
#endif//TETRA_HARDWARE-
#include "EstateManager.h"
#include "LLComms.h"
#include "CoreDefines.h"
#include "CoreMessages.h"
#include "Globals.h"
#include "Defines.h"
#include "Tables.h"
#include "Steps.h"
#include "Configuration.h"
#include "_EWL_debug.h"
#include "Utils.h"
#include "IngeLoadComms.h"
#include "IngeLoad_Unpack.h"
#include "TransactionManager.h"
#include "CoreISO8583.h"
#include "ISO8583_Main.h"
#include "Batch.h"
#include "EmvLib_Tags.h"
#include "Print.h"
#include "Totals.h"
#include "Reports.h"
#include "Merchant.h"
//#include "debug.h"

#if(TETRA_HARDWARE == 0)
#include "dll_wifi.h"
#endif

#if(TETRA_HARDWARE == 1)
#include "_TetraMigration.h"
#endif//DIR_TETRA_MIGRATION

//#include "EMVCustom.h"
//+ CLESS @mamata Dec 11, 2014
//#include "CLESS.h"
//- CLESS
//+ CARCAAN_PINPAD @mamata Jan 8, 2015
#include "PINPAD.h"
//- CARCAAN_PINPAD

//+ CRYPTOLIB @mamata Feb 21, 2015
#include "CryptoLib.h"
//- CRYPTOLIB

//+ LLCOMMS01 @mamata Jun 15, 2015
#include "SSL_.h"
//- LLCOMMS01

//+ INGLOFF01 @jbotero 29/07/2015
#include "IngeLoadOffline.h"
//- INGLOFF01

//+ INGESTATE01 @jbotero 13/09/2015
#include "TeliumParams.h"
#include "Ingestate_Unpack.h"
//- INGLOFF01

#include "GTL_Convert.h"

#include "Communication.h"

//+ EWL01 @jbotero 29/01/2016
#include "ewldll.h"

#include "ewlTags.h"
#include "larlib/all.h"


#include "SEC_interface.h"
#include "oem_cless.h"
#include "oem_clemv.h"

#include "_TLV_admin.h"
#include "_EWL_TransactionData.h"
#include "_EWL_util.h"
#include "_EWL_aid.h"
#include "_EWL_key.h"
#include "_EWL_parameters.h"
#include "_EWL_paymentTools.h"
#include "_EWL_paymentError.h"
#include "_EWL_callbacks.h"
#include "_EWL_data.h"
#include "_EWL_admin.h"
//- EWL01

#include "IPCONFIG_.h"
#include "IPTOOLS_.h"
#include "BSD_TPlus.h"

#include "File_utils.h"

//+ DOTRAN_PCL @jbotero
#include "GTL_BerTlv.h"
#include "GTL_BerTlvDecode.h"
#include "GTL_BerTlvEncode.h"
#include "PclUtils.h"
//- DOTRAN_PCL

#if (DIR_EWL_PAR == 1)
#include "_EWL_parReader.h"
#endif // DIR_EWL_PAR

#endif //_H_HEADERS
