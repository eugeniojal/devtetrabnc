#ifndef CALLBACKS_H
#define CALLBACKS_H

ewlStatus_t callbackGetParameters(ewlObject_t   *handle, const unsigned long *dataList,
		uint16_t  nDataListElement);
void callbackLeds (ewlObject_t *handle, uint16_t step);
void callbackRemoveCard (ewlObject_t *handle,  int status);
void callbackBDisplay(ewlObject_t *handle, ewlDisplay_t msgCode);
ewlStatus_t callbackGetMenu (ewlObject_t *handle, uint16_t *selected,
                             const char **aidList, uint16_t nAIDs);
ewlStatus_t callbackGetPinOffLine (ewlObject_t *handle, unsigned char *numDigits);
ewlStatus_t callbackGetPinOnLine (ewlObject_t *handle);
ewlStatus_t callbackFinalCVM (ewlObject_t *handle);
ewlStatus_t callbackGetPublicKey (ewlObject_t *handle, ewlGetCertificate_t *certificate);



#endif
