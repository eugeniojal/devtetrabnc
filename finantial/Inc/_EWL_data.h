#ifndef DATA_H
#define DATA_H

int dataRecoverMagneticCard (TLV_TREE_NODE Tree);
int dataCheckExpDateEmvApp(TLV_TREE_NODE Tree);
int dataRecoverGetData ( TLV_TREE_NODE Tree);
int dataRecoverGoOnChip ( ewlObject_t *ewl, TLV_TREE_NODE Tree);
int dataRecoverAproveOffLine ( TLV_TREE_NODE Tree );
int dataRecoverFinishChip ( TLV_TREE_NODE Tree );
int dataGetChipCard( TLV_TREE_NODE Tree );
int dataGetMagCard( TLV_TREE_NODE Tree );
int dataProcessMagCard( TLV_TREE_NODE Tree );


#endif
