#include "iPOSApp.h"

typedef enum
{
    FLOW_STOP,
    FLOW_SWITCH_INTERFACE,
    FLOW_TRY_AGAIN,

}Flow_t;

typedef struct
{
    int         		status;
    int           		msgContact;
    int 				msgContactLess;
    Flow_t              flow;
    T_GL_DIALOG_ICON    icon;

} ErrList_t;

static ErrList_t EwlErrorList[] =
{
	{ EWL_ERR_CARD_REMOVED,                             MSG_ERROR_REMOVED_CARD,                 MSG_ERROR_REMOVED_CARD,                 FLOW_STOP,              GL_ICON_WARNING },
	{ EWL_ERR_USER_TIMEOUT,                             MSG_ERROR_CANCEL,                 		MSG_ERROR_CANCEL,                 		FLOW_STOP,              GL_ICON_WARNING },
    { EWL_ERR_USER_TIMEOUT,                             MSG_ERROR_CANCEL,                 		MSG_ERROR_CANCEL,                 		FLOW_STOP,              GL_ICON_WARNING },
    { EWL_ERR_USER_CANCEL,                              MSG_ERROR_CANCEL,                 		MSG_ERROR_CANCEL,                 		FLOW_STOP,              GL_ICON_WARNING },

    { EWL_ERR_USER_BY_PASS,                             MSG_ERROR_CANCEL,                 		MSG_ERROR_CANCEL,                 		FLOW_STOP,              GL_ICON_WARNING },
    { EWL_ERR_RESELECT,                                 MSG_ERROR_NO_SUPPORTED_CARD,            MSG_ERROR_NO_SUPPORTED_CARD,			FLOW_TRY_AGAIN,         GL_ICON_NONE    },
    { EWL_ERR_IS_EASY_ENTRY,                            MSG_ERROR_NO_SUPPORTED_CARD,            MSG_ERROR_NO_SUPPORTED_CARD,            FLOW_TRY_AGAIN,         GL_ICON_NONE    },
    { EWL_ERR_NOT_ALLOWED,                              MSG_ERROR_NO_SUPPORTED_CARD,            MSG_ERROR_NO_SUPPORTED_CARD,            FLOW_TRY_AGAIN,         GL_ICON_NONE    },
    { EWL_ERR_ALL_APP_NON_EMV,                          MSG_ERROR_NO_SUPPORTED_CARD,            MSG_ERROR_NO_SUPPORTED_CARD,            FLOW_TRY_AGAIN,         GL_ICON_NONE    },

    { EWL_ERR_CARD_MUTE,                        		MSG_ERROR_READ_CHIP,                  	MSG_ERROR_READ_CHIP,                  	FLOW_STOP,  			GL_ICON_ERROR   },
    { EWL_ERR_NO_COMPATIBLE_APP,                        MSG_ERROR_READ_CHIP,                  	MSG_ERROR_READ_CHIP,                  	FLOW_STOP,  			GL_ICON_ERROR   },
    { EWL_ERR_SWITCH_INTERFACE,                         MSG_ERROR_READ_CHIP,                  	MSG_ERROR_READ_CHIP,                  	FLOW_SWITCH_INTERFACE,  GL_ICON_NONE    },
    { EWL_ERR_CLESS_TYPE_NOT_SUPPORTED,                 MSG_ERROR_READ_CHIP,                  	MSG_ERROR_READ_CHIP,                  	FLOW_SWITCH_INTERFACE,  GL_ICON_NONE    },

    { EWL_ERR_ALL_APP_BLOCKED,                          MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_ALL_APP_INVALIDATED,                      MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_ALL_APP_ERROR,                            MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },

    //{ EWL_ERR_MISSING_CONTACTLESS_CARD_MANDATORY_DATA,  MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INVALID_CARD_DATA,                        MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_REDUNDANT_CARD_DATA,                      MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_CARD_INVALID_ANSWER,                      MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INVALID_CARD,                             MSG_ERROR_CARD_PROBLEMS,				MSG_ERROR_CARD_PROBLEMS,				FLOW_STOP,              GL_ICON_ERROR   },

    { EWL_ERR_CLESS_COLLISION,                          MSG_ERROR_COLLISION_CARD,               MSG_ERROR_COLLISION_CARD,               FLOW_TRY_AGAIN,         GL_ICON_NONE    },

    { EWL_ERR_RF_PROBLEM,                               MSG_ERROR_RETRY,    		           	MSG_ERROR_RETRY,            			FLOW_TRY_AGAIN,         GL_ICON_NONE    },
    { EWL_ERR_CLESS_DETECTION_PROBLEM,                  MSG_ERROR_RETRY,             	  		MSG_ERROR_RETRY,            			FLOW_TRY_AGAIN,         GL_ICON_NONE    },
    { EWL_ERR_INSUFFICIENT_OUT_BUFFER,                  MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_FLOW,                            MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_INVALID_FORMAT,                  MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_VALUE_OVERFLOW,                  MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_TAG_NOT_FOUND,                            MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_NO_RESOURCE,                              MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_MISSING_MANDATORY_PARAMETER,              MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INVALID_PARAMETER,                        MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INVALID_DOL,                              MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_NOT_IMPLEMENTED,                          MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INVALID_CALL,                             MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_MISSING_COMPONENT,                        MSG_ERROR_COMPONENT,                    MSG_ERROR_COMPONENT,                    FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_KERNEL_LINK,                              MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_CLESS_DRIVER_NOT_OPEN,                    MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_PARAMETER,                       MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_TLVTREE,                         MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_MISSING_DATA,                    MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL_INSUFFICIENT_BUFFER,             MSG_ERROR_INTERNAL,						MSG_ERROR_INTERNAL,   					FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INVALID_HANDLE,                           MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,  					FLOW_STOP,              GL_ICON_ERROR   },

    { EWL_ERR_MOBILE_ON_DEVICE_CVM,                     MSG_ERROR_INTERNAL,                     MSG_CHECK_PHONE_INSTRUCTION,            FLOW_TRY_AGAIN,         GL_ICON_NONE    },

    { EWL_ERR_CARD_BLOCKED,                             MSG_ERROR_BLOCKED_CARD,                 MSG_ERROR_BLOCKED_CARD,                 FLOW_STOP,              GL_ICON_ERROR   },

    { EWL_ERR_KERNEL,                                   MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },
    { EWL_ERR_INTERNAL,                                 MSG_ERROR_INTERNAL,                     MSG_ERROR_INTERNAL,                     FLOW_STOP,              GL_ICON_ERROR   },

    { EWLDEMO_ERR_TIMEOUT,                              MSG_ERROR_CANCEL,                 		MSG_ERROR_CANCEL,                 		FLOW_STOP,              GL_ICON_WARNING },
    { EWLDEMO_ERR_CANCEL,                               MSG_ERROR_CANCEL,                 		MSG_ERROR_CANCEL,                 		FLOW_STOP,              GL_ICON_WARNING },
};

//------------------------------------------------------------------------------
static void swithcInterfaceMessage (uint8 FlagEmv, uint8 FlagCless)
{
    utilBeepWarning();

    if(FlagEmv)
    {
        UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_INVALID_MODE_CONTACT),
                      "", GL_ICON_NONE, GL_BUTTON_ALL, 2);
        return;
    }

    if(FlagCless)
    {
        UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_INVALID_MODE_CLESS),
                      "", GL_ICON_NONE, GL_BUTTON_ALL, 2);
        return;
    }

    UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_INVALID_MODE_NONE),
                  "", GL_ICON_NONE, GL_BUTTON_ALL, 2);

    return;
}

//------------------------------------------------------------------------------
int paymentError ( TLV_TREE_NODE Tree,  int status, int * NextStep)
{
    unsigned int ii;
    int msg;
    int RetFun;
    uint8 EntryMode;
    uint8 fallback = 0;
    char LineAux[100];

	uint8 FlagEMVSupport = FALSE;
	uint8 FlagCLSSupport = FALSE;

	* NextStep = STM_RET_ERROR;

	sprintf(LineAux, "%d", status);

    for(ii = 0; ii < BASE_LENGTH(EwlErrorList);ii++)
    {
        if(EwlErrorList[ii].status == status)
        	break;
    }

    if(ii == BASE_LENGTH(EwlErrorList))
    {
        UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_INTERNAL),
        		LineAux, GL_ICON_ERROR, GL_BUTTON_ALL, 2);
        return RET_OK;
    }

    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    msg = ((EntryMode == ENTRY_MODE_CHIP) ? EwlErrorList[ii].msgContact : EwlErrorList[ii].msgContactLess);

    //get EMV support
	RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_EMV_SUPPORT, &FlagEMVSupport, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//get CLESS support
	RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_CLESS_SUPPORT, &FlagCLSSupport, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

    if(EwlErrorList[ii].flow == FLOW_SWITCH_INTERFACE)
    {
        switch(EntryMode)
        {
            case ENTRY_MODE_CHIP:
            	FlagEMVSupport = FALSE;
            	RetFun = Tlv_SetTagValue(Tree, TAG_TRAN_EMV_SUPPORT, &FlagEMVSupport, sizeof(FlagEMVSupport));
            	CHECK(RetFun == RET_OK, LBL_ERROR);
            	break;

            case ENTRY_MODE_CLESS_CHIP:
            	FlagCLSSupport = FALSE;
            	RetFun = Tlv_SetTagValue(Tree, TAG_TRAN_CLESS_SUPPORT, &FlagCLSSupport, sizeof(FlagCLSSupport));
            	CHECK(RetFun == RET_OK, LBL_ERROR);
            	break;

            default:
            	break;
        }

        swithcInterfaceMessage(FlagEMVSupport, FlagCLSSupport);
    }
    else
    {
    	if(msg == MSG_ERROR_INTERNAL)
    	{
            UI_ShowMessage(NULL, mess_getReturnedMessage(msg),
            		LineAux, GL_ICON_ERROR, GL_BUTTON_ALL, 2);
    	}
    	else if(msg == MSG_ERROR_CANCEL)
    	{
    		;//Nothing to do
    	}
    	else
    	{
    		UI_ShowMessage(NULL, mess_getReturnedMessage(msg),
    				"", GL_ICON_ERROR, GL_BUTTON_ALL, 2);
    	}
    }

    switch(EwlErrorList[ii].flow)
    {
        case FLOW_SWITCH_INTERFACE:
        case FLOW_TRY_AGAIN:
        	*NextStep = STEP_EWL_START;
            break;

        case FLOW_STOP:
        	if(EwlErrorList[ii].status == EWL_ERR_CARD_MUTE ||
        		EwlErrorList[ii].status == EWL_ERR_NO_COMPATIBLE_APP)
        	{
    			fallback = TRUE;
    			Tlv_SetTagValue(Tree, TAG_FALLBACK, &fallback, sizeof(fallback));
    			*NextStep = STEP_EWL_START;
        	}
        	else
        		*NextStep = STM_RET_ERROR;
            break;
    }

    return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

