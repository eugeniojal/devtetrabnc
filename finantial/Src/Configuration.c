/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			Configuration.c
 * Header file:		Configuration.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"
extern uint16 TRAN_STEPS_TEST[];
#include "dll_wifi.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
static TLV_TREE_NODE Tree_Conf;
#define TMD_ID_APPTYPE			0xC659
#define NOSSL                   3
#define SISSL                   7

typedef enum eSecurityType
{
	SEC_HOME = 0,
	SEC_ENTERPRISE,

}SECURITY_TYPE;

typedef struct
{
	dll_wifi_scan_result_t 	info;
	int SecurityType;
	int CipherType;
	int AuthType;
	unsigned char Key[50];
	int priority;

} WIFI_DATA;

static WIFI_DATA stProfileWifi;


#ifndef MAC_STR
	#define MAC_STR "%02X:%02X:%02X:%02X:%02X:%02X"
#endif

#ifndef MAC_2_STR
	#define MAC_2_STR(A) (A)[0],(A)[1],(A)[2],(A)[3],(A)[4],(A)[5]
#endif


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
TABLE_HOST axHost;
TABLE_IP IpTable;
TABLE_MERCHANT auxMerch;
int Menu_IngeLoad(void);
int Menu_Comunicacion(void);
int CONFIG_COMERCIO (void);
int CONFIG_SEGURIDAD (void);
//+ INGESTATE01 @jbotero 13/09/2015
int Menu_Ingestate(void);
//- INGESTATE01

//+ BATCH01 @mamata Aug 15, 2014
int DeleteBatch(void);
//- BATCH01

//int DeleteAllAdvice(void);
int DeleteAllReverse(void);

//+ LLCOMMS01 @mamata Jun 17, 2015
int Post_STEP_DIAL_DETECT_TONE(char *Result);
int Post_STEP_DIAL_DETECT_BUSY(char *Result);
int Post_STEP_DIAL_MODEM_MODE(char *Result);
bool Pre_STEP_DIAL_BLIND_PAUSE(void);
int Post_STEP_DIAL_BLIND_PAUSE(char *Result);
bool Pre_STEP_DIAL_TONE_TO(void);
int Post_STEP_DIAL_TONE_TO(char *Result);
bool Pre_STEP_DIAL_PABX_PAUSE(void);
int Post_STEP_DIAL_PABX_PAUSE(char *Result);
//- LLCOMMS01

//+ DUALSIM @mamata Jun 24, 2015
bool Pre_STEP_GPRS2_APN(void);
bool Pre_STEP_GPRS2_USER(void);
bool Pre_STEP_GRPS2_PASS(void);
//- DUALSIM

//+ INGESTATE01 @jbotero 13/09/2015
int Post_STEP_IS_COMM_TYPE(char *Result);
bool Pre_STEP_IS_TERMINAL(void);
bool Pre_INPUT_IS_IP(void);
bool Pre_STEP_IS_PORT(void);
bool Pre_STEP_IS_PHONE(void);
bool Pre_STEP_IS_PPPS_USER(void);
bool Pre_STEP_IS_PPPS_PASS(void);
int Post_STEP_IS_SSL(char *Result);
bool Pre_STEP_IS_SSL_PROFILE(void);
int Post_STEP_IS_SSL_VERSION(char *Result);
int Post_STEP_SAVE_INGESTATE(char *Result);
//- INGESTATE01


int Post_STEP_CONFIG_COMM(char *Result);

bool Pre_STEP_LOCAL_MODE(void);
int Post_STEP_LOCAL_MODE(char *Result);
bool Pre_STEP_LOCAL_IP(void);
bool Pre_STEP_LOCAL_IP_HOST(void);
int Post_STEP_LOCAL_IP(char *Result);
int Post_STEP_LOCAL_IP_HOST(char *Result);

int Post_STEP_LOCAL_IP_HOST_GPRS(char *Result);
bool Pre_STEP_LOCAL_IP_HOST_GPRS(void);

bool Pre_STEP_IS_PORT_HOST_GPRS(void);
int Post_STEP_IS_PORT_HOST_GPRS(char *Result);

bool Pre_STEP_LOCAL_MASK(void);
int Post_STEP_LOCAL_MASK(char *Result);
bool Pre_STEP_LOCAL_GATEWAY(void);
int Post_STEP_LOCAL_GATEWAY(char *Result);
bool Pre_STEP_LOCAL_DNS1(void);
int Post_STEP_LOCAL_DNS1(char *Result);
bool Pre_STEP_LOCAL_DNS2(void);
int Post_STEP_LOCAL_DNS2(char *Result);

bool Pre_STEP_ETH_LOAD_CONFIG(void);
int Post_STEP_ETH_LOAD_CONFIG(char *Result);
bool Pre_STEP_ETH_APPLY_CONFIG(void);
int Post_STEP_ETH_APPLY_CONFIG(char *Result);

bool Pre_STEP_WIFI_SSID(void);
int Post_STEP_WIFI_SSID(char *Result);
bool Pre_STEP_WIFI_SECURITY(void);
int Post_STEP_WIFI_SECURITY(char *Result);
bool Pre_STEP_WIFI_KEY_PASS(void);
int Post_STEP_WIFI_KEY_PASS(char *Result);
bool Pre_STEP_WIFI_ADDPROFILE(void);
int Post_STEP_WIFI_ADDPROFILE(char *Result);
bool Pre_STEP_WIFI_LOAD_CONFIG(void);
int Post_STEP_WIFI_LOAD_CONFIG(char *Result);
bool Pre_STEP_WIFI_APPLY_CONFIG(void);
int Post_STEP_WIFI_APPLY_CONFIG(char *Result);
//- WIFI_TBK01

//- PCL
bool Pre_STEP_PCL_MODE(void);
int Post_STEP_PCL_MODE(char *Result);
int Post_STEP_PCL_APPLY_CONFIG(char *Result);
//- PCL
bool Pre_STEP_NAME_TERMINAL(void);
int PRE_STEP_TYPE_APLICATION(char *Result);
bool Pre_step_imprime (void);
bool Pre_step_imprime_debito (void);
bool Pre_step_imprime_credito (void);
bool Pre_step_pre_imprime (void);
bool Pre_step_imprime_rechazado(void);
bool Pre_step_imprime_logo(void);
bool Pre_NUMERO_COPIAS(void);
int Post_NUMERO_COPIAS(char *Result);

int Post_STEP_IS_COMM_TYPE_HOST(char *Result);
bool Pre_STEP_IS_PORT_HOST(void);
int Post_STEP_IS_PORT_HOST(char *Result);
int Post_STEP_SAVE_COMUNICACION(char *Result);
int Post_STEP_IS_SSL_HOST(char *Result);

int Post_STEP_MODIFY_TERMINAL (char *Result);
bool Pre_STEP_MODIFY_TERMINAL (void);
int Post_STEP_MODIFY_AFILIADO (char *Result);
bool Pre_STEP_MODIFY_AFILIADO (void);
int Post_STEP_MODIFY_TPDU (char *Result);
bool Pre_STEP_MODIFY_TPDU (void);
int Post_STEP_NUMBER_MERCHANT (char *Result);
int IMPRIMIR_PARAMETROS (void);
bool  Pre_STEP_MODIFY_NOMBREC(void);
int Post_STEP_MODIFY_NOMBREC(char *Result);
bool  Pre_STEP_MODIFY_RIF(void);
int Post_STEP_MODIFY_RIF(char *Result);
int Post_STEP_DIAL_TONE_PULSO(char *Result);
int Post_STEP_IP_PHONE2(char *Result);
int Post_STEP_IP_PHONE1(char *Result);
int Post_STEP_MODIFY_AFILIADO_CREDITO (char *result);
bool Pre_STEP_MODIFY_AFILIADO_CREDITO (void);
int Post_STEP_MODIFY_AFILIADO_AMEX (char *result);
bool Pre_STEP_MODIFY_AFILIADO_AMEX (void);

bool pre_clave_tecnico(void);
int post_clave_tecnico(char *Result);
bool pre_ModificarClaveTecnico(void);
int post_ModificarClaveTecnico(char *Result);
bool pre_ModificarClaveMerchant(void);
int post_ModificarClaveMerchant(char *Result);
int post_GuardarClave(char *Result);

bool pre_PEDIR_CIERRE(void);
int post_cambio_moneda(char *Result);
/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
unsigned short CONF_MONEDA[] =
{
	STEP_CAMBIO_MONEDA,
	STEP_END_LIST
};
unsigned short CONF_RIF[] =
{
	STEP_MODIFY_RIF,
	STEP_END_LIST
};
unsigned short CONF_NOMBREC[] =
{
	STEP_MODIFY_NOMBREC,
	STEP_END_LIST
};
unsigned short TRN_CONF_MERCHANT[] =
{
	STEP_SELECT_NUMBER_MERCHANT,
	STEP_MODIFY_TERMINAL,
	STEP_MODIFY_AFILIADO,
	STEP_MODIFY_AFILIADO_CREDITO,
	STEP_MODIFY_AFILIADO_AMEX,
	STEP_MODIFY_TPDU,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_NUMERO_COPIAS[] =
{
	STEP_NUMERO_COPIAS,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_IMPRIME_LOGO[] =
{
	STEP_IMPRIME_LOGO,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_IMPRIME_RECHAZADO[] =
{
	STEP_IMPRIME_RECHAZADO,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_PRE_IMPRIMET[] =
{
	STEP_PRE_IMPRIME,
	STEP_END_LIST
};
unsigned short TRN_IMPRIME_CREDITO[] =
{
	STEP_IMPRIME_CREDITO,
	STEP_END_LIST
};
unsigned short TRN_IMPRIME_DEBITO[] =
{
	STEP_IMPRIME_DEBITO,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_TAPLICATION[] =
{
	STEP_TYPE_APLICATION,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_NAMET[] =
{
    STEP_NAME_TERMINAL,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_IMPRIMET[] =
{
	STEP_IMPRIME,
	STEP_END_LIST
};
unsigned short TRN_GENERAL_CONF[] =
{
	//+ DUALSIM @mamata Jun 24, 2015
	//d STEP_PABX,
	//d STEP_GPRS_APN,
	//d STEP_GPRS_USER,
	//d STEP_GRPS_PASS,
	//- DUALSIM
	STEP_TIME_OUT,
	STEP_GCONF_REPEAT,
	STEP_END_LIST
};

unsigned short TRN_IPARAMETERS_CONF[] =
{
	STEP_IP_TERMINAL,
	STEP_IP_COMM_TYPE,
	STEP_IP_NII,
	STEP_IP_RETRIES,
	STEP_IP_CONNECT_TO,
	STEP_IP_RESPONSE_TO,
//	STEP_IP_PHONE1,
//	STEP_IP_PHONE2,
	STEP_IP_HOST,
	STEP_IP_PORT,
	STEP_IP_SSL,
	STEP_IP_REPORT,
	STEP_SAVE_REPEAT_INGELOAD,
	STEP_END_LIST
};

unsigned short TRN_TECNICIAN_LOGON[] =
{
	STEP_TECHNICIAN_PASSWORD,
//	STEP_MENU_CONFIG,
	STEP_END_LIST,
};

//+ BATCH01 @mamata Aug 15, 2014
unsigned short TRN_DELETE_BATCH[] =
{	STEP_TECHNICIAN_PASSWORD,
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP_ALL,
	STEP_SELECT_MERCHANT_ALL,
	STEP_END_LIST,
};
unsigned short TRN_DELETE_ADVICE[] =
{	STEP_TECHNICIAN_PASSWORD,
	STEP_CHECK_TERMINAL,
	STEP_END_LIST,
};
//- BATCH01

//+ LLCOMMS01 @mamata Jun 17, 2015
unsigned short TRN_CONF_DIAL[] =
{
	STEP_PABX,
	STEP_DIAL_PABX_PAUSE,
	STEP_DIAL_DETECT_TONE,
	STEP_DIAL_TONE_TO,
	STEP_DIAL_BLIND_PAUSE,
	STEP_DIAL_DETECT_BUSY,
	STEP_DIAL_MODEM_MODE,
	STEP_GCONF_REPEAT,
	STEP_END_LIST
};
//- LLCOMMS01

//+ DUALSIM @mamata Jun 24, 2015
unsigned short TRN_CONF_GPRS[] =
{
	STEP_GPRS_APN,
	STEP_GPRS_USER,
	STEP_GRPS_PASS,
	STEP_LOCAL_IP_HOST_GPRS,
	STEP_IS_PORT_HOST_GPRS,
//	STEP_GPRS2_APN,
//	STEP_GPRS2_USER,
//	STEP_GRPS2_PASS,
//	STEP_GCONF_REPEAT,
	STEP_SAVE_COMUNICACION,
	STEP_END_LIST
};
//- DUALSIM

//+ INGESTATE01 @jbotero 13/09/2015
unsigned short TRN_INGESTATE_CONF[] =
{
	STEP_IS_TERMINAL,
	STEP_IS_COMM_TYPE,
	STEP_IS_PHONE,
	STEP_IS_HOST,
	STEP_IS_PORT,
	STEP_IS_SSL,
	STEP_IS_SSL_PROFILE,
	STEP_IS_SSL_VERSION,
	STEP_SAVE_REPEAT_INGESTATE,
	STEP_END_LIST
};
//- INGESTATE01

unsigned short TRN_CONFIG_COMM[] =
{
	STEP_CONFIG_COMM,

	STEP_ETH_LOAD_CONFIG,
	STEP_LOCAL_MODE,
	STEP_LOCAL_IP,
	STEP_LOCAL_MASK,
	STEP_LOCAL_GATEWAY,
	STEP_LOCAL_DNS1,
	STEP_LOCAL_DNS2,
	STEP_ETH_APPLY_CONFIG,
//	#if(TETRA_HARDWARE == 1)
//	#else
	STEP_WIFI_SSID,
	STEP_WIFI_SECURITY,
	STEP_WIFI_KEY_PASS,
	STEP_WIFI_ADDPROFILE,
	STEP_WIFI_LOAD_CONFIG,
	STEP_LOCAL_MODE,
	STEP_LOCAL_IP,
	STEP_LOCAL_MASK,
	STEP_LOCAL_GATEWAY,
	STEP_LOCAL_DNS1,
	STEP_LOCAL_DNS2,
	STEP_WIFI_APPLY_CONFIG,
//	#endif//TETRA_HARDWARE
	STEP_PCL_MODE,
	STEP_PCL_APPLY_CONFIG,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_COMM_HOST[] =
{
	STEP_IS_COMM_TYPE_HOST,
	STEP_WIFI_SSID,
	STEP_WIFI_SECURITY,
	STEP_WIFI_KEY_PASS,
	STEP_WIFI_ADDPROFILE,
	STEP_WIFI_LOAD_CONFIG,
	STEP_WIFI_APPLY_CONFIG,

	STEP_PABX,
	STEP_IP_PHONE1,
	STEP_IP_PHONE2,
	//STEP_TONE_PULSE,
	STEP_DIAL_DETECT_TONE,
	//NEW
	STEP_DIAL_DETECT_BUSY,
	STEP_DIAL_MODEM_MODE,
	STEP_DIAL_BLIND_PAUSE,
	STEP_DIAL_TONE_TO,
	STEP_DIAL_PABX_PAUSE,

	STEP_GPRS_APN,
	STEP_GPRS_USER,
	STEP_GRPS_PASS,
	STEP_LOCAL_IP_HOST,
	STEP_IS_PORT_HOST,
	STEP_IS_SSL_HOST,
	STEP_SAVE_COMUNICACION,
	STEP_END_LIST
};

unsigned short TRN_CONFIG_COMM_USER[] =
{
		STEP_IS_COMM_TYPE_HOST,
		STEP_WIFI_SSID,
		STEP_WIFI_SECURITY,
		STEP_WIFI_KEY_PASS,
		STEP_WIFI_ADDPROFILE,
		STEP_WIFI_LOAD_CONFIG,
		STEP_WIFI_APPLY_CONFIG,
		STEP_IS_SSL_HOST,
	STEP_SAVE_COMUNICACION,
	STEP_END_LIST
};
unsigned short TRN_CONFIG_COMM_ETHERNET[] =
{
	STEP_LOCAL_IP_HOST,
	STEP_IS_PORT_HOST,
	STEP_IS_SSL_HOST,
	STEP_SAVE_COMUNICACION,
	STEP_END_LIST
};

unsigned short TRN_CONFIG_COMM_DIAL[] =
{
		STEP_PABX,
		STEP_IP_PHONE1,
		STEP_IP_PHONE2,
		//STEP_TONE_PULSE,
		STEP_DIAL_DETECT_TONE,
		//NEW
		STEP_DIAL_DETECT_BUSY,
		STEP_DIAL_MODEM_MODE,
		STEP_DIAL_BLIND_PAUSE,
		STEP_DIAL_TONE_TO,
		STEP_DIAL_PABX_PAUSE,
		STEP_SAVE_COMUNICACION,
		STEP_END_LIST
};
unsigned short TRN_CAMBIAR_PASSWORD_TECNICO[] =
{
		STEP_MODFY_TECNICO,
		STEP_SAVE_PASSWORD,
	    STEP_END_LIST
};
unsigned short TRN_CAMBIAR_PASSWORD_MERCHANT[] =
{
		STEP_PASS_TECNICO,
		STEP_MODIFY_MERCHANT,
		STEP_SAVE_PASSWORD,
	    STEP_END_LIST
};


SMT_STEP CONF_STEP_LIST[] =
{
		//{	/* step id */			STEP_CIERRE,
				/* input type */	//	INPUT_NONE,
				/* input min  */	//	0,
				/* input max  */	//	0,
				/* input timeout */	//	30,
				/* result pointer *///	NULL,
				/* pre function */		//pre_PEDIR_CIERRE,
				/* post function*/	//	NULL
		//	},
			{	/* step id */			STEP_CAMBIO_MONEDA,
					/* input type */		INPUT_NONE,
					/* input min  */		0,
					/* input max  */		0,
					/* input timeout */		30,
					/* result pointer */	&auxMerch.Currency,
					/* pre function */		pre_PEDIR_CIERRE,
					/* post function*/		post_cambio_moneda
				},

{	/* step id */			STEP_PASS_TECNICO,
		/* input type */		INPUT_PASSWORD,
		/* input min  */		0,
		/* input max  */		LZ_TERM_PASSWORD,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		pre_clave_tecnico,
		/* post function*/		post_clave_tecnico
	},
	{	/* step id */			STEP_MODFY_TECNICO,
			/* input type */		INPUT_PASSWORD,
			/* input min  */		0,
			/* input max  */		LZ_TERM_PASSWORD,
			/* input timeout */		30,
			/* result pointer */	NULL,
			/* pre function */		pre_ModificarClaveTecnico,
			/* post function*/		post_ModificarClaveTecnico
		},
	{	/* step id */			STEP_MODIFY_MERCHANT,
			/* input type */		INPUT_PASSWORD,
			/* input min  */		0,
			/* input max  */		LZ_TERM_PASSWORD,
			/* input timeout */		30,
			/* result pointer */	NULL,
			/* pre function */		pre_ModificarClaveMerchant,
			/* post function*/		post_ModificarClaveMerchant
		},
		{	/* step id */			STEP_SAVE_PASSWORD,
				/* input type */		INPUT_NONE,
				/* input min  */		0,
				/* input max  */		0,
				/* input timeout */		30,
				/* result pointer */	NULL,
				/* pre function */		NULL,
				/* post function*/		post_GuardarClave
			},
	{	/* step id */			STEP_PABX,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		LZ_PABX,
		/* input timeout */		30,
		/* result pointer */	tConf.Dial_PABX,
		/* pre function */		Pre_STEP_PABX,
		/* post function*/		NULL
	},

	{	/* step id */			STEP_GPRS_APN,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		L_APN,
		/* input timeout */		30,
		/* result pointer */	tConf.GPRS_APN,
		/* pre function */		Pre_STEP_GPRS_APN,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_GPRS_USER,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		LG_LOGIN,
		/* input timeout */		30,
		/* result pointer */	tConf.GPRS_User,
		/* pre function */		Pre_STEP_GPRS_USER,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_GRPS_PASS,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		LG_LOGIN,
		/* input timeout */		30,
		/* result pointer */	tConf.GPRS_Pass,
		/* pre function */		Pre_STEP_GRPS_PASS,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_TIME_OUT,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	&tConf.TimeOut,
		/* pre function */		Pre_STEP_TIME_OUT,
		/* post function*/		Post_Commit_UINT8
	},

	{
		/* step id */			STEP_IP_TERMINAL,
		/* input type */		INPUT_TEXT,
		/* input min  */		1,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	tConf.iP_ID,
		/* pre function */		Pre_STEP_IP_TERMINAL,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IP_COMM_TYPE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tConf.iP_CommType,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IP_COMM_TYPE
	},

	{
		/* step id */			STEP_IP_NII,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		L_NII-1,
		/* input max  */		L_NII,
		/* input timeout */		30,
		/* result pointer */	tConf.iP_NII,
		/* pre function */		Pre_STEP_IP_NII,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IP_RETRIES,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	&tConf.iP_Retries,
		/* pre function */		Pre_STEP_IP_RETRIES,
		/* post function*/		Post_Commit_UINT8
	},

	{
		/* step id */			STEP_IP_CONNECT_TO,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	&tConf.iP_ConnectTO,
		/* pre function */		Pre_STEP_IP_CONNECT_TO,
		/* post function*/		Post_Commit_UINT8
	},

	{
		/* step id */			STEP_IP_RESPONSE_TO,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	&tConf.iP_RspTO,
		/* pre function */		Pre_STEP_IP_RESPONSE_TO,
		/* post function*/		Post_Commit_UINT8
	},

	{
		/* step id */			STEP_IP_PHONE1,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		LZ_PHONE,
		/* input timeout */		30,
		/* result pointer */	&axHost.Dial_Primary_Phone1,
		/* pre function */		Pre_STEP_IP_PHONE1,
		/* post function*/		Post_STEP_IP_PHONE1
	},

	{
		/* step id */			STEP_IP_PHONE2,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		LZ_PHONE,
		/* input timeout */		30,
		/* result pointer */	&axHost.Dial_Primary_Phone2,
		/* pre function */		Pre_STEP_IP_PHONE2,
		/* post function*/		Post_STEP_IP_PHONE2
	},

	{
		/* step id */			STEP_IP_HOST,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	tConf.iP_Host,
		/* pre function */		Pre_INPUT_IP,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IP_PORT,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		L_PORT,
		/* input timeout */		30,
		/* result pointer */	tConf.iP_Port,
		/* pre function */		Pre_STEP_IP_PORT,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IP_SSL,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tConf.iP_SSL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IP_SSL
	},

	{
		/* step id */			STEP_IP_REPORT,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tConf.iP_Report,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IP_REPORT
	},

	{
		/* step id */			STEP_GCONF_REPEAT,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_SAVE_GENERAL_CONF
	},

	{
		/* step id */			STEP_SAVE_REPEAT_INGELOAD,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_SAVE_INGELOAD
	},

	//+ LLCOMMS01 @mamata Jun 17, 2015
	{
		/* step id */			STEP_DIAL_DETECT_TONE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_DIAL_DETECT_TONE
	},

	{
		/* step id */			STEP_DIAL_DETECT_BUSY,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_DIAL_DETECT_BUSY
	},

	{
		/* step id */			STEP_DIAL_MODEM_MODE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_DIAL_MODEM_MODE
	},

	{
		/* step id */			STEP_DIAL_BLIND_PAUSE,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_DIAL_BLIND_PAUSE,
		/* post function*/		Post_STEP_DIAL_BLIND_PAUSE
	},

	{
		/* step id */			STEP_DIAL_TONE_TO,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_DIAL_TONE_TO,
		/* post function*/		Post_STEP_DIAL_TONE_TO
	},

	{
		/* step id */			STEP_DIAL_PABX_PAUSE,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_DIAL_PABX_PAUSE,
		/* post function*/		Post_STEP_DIAL_PABX_PAUSE
	},
	//- LLCOMMS01

	//+ DUALSIM @mamata Jun 24, 2015
	{
		/* step id */			STEP_SELECT_SIM,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		NULL,
	},

	{	/* step id */			STEP_GPRS2_APN,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		L_APN,
		/* input timeout */		30,
		/* result pointer */	tConf.GPRS2_APN,
		/* pre function */		Pre_STEP_GPRS2_APN,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_GPRS2_USER,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		LG_LOGIN,
		/* input timeout */		30,
		/* result pointer */	tConf.GPRS2_User,
		/* pre function */		Pre_STEP_GPRS2_USER,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_GRPS2_PASS,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		LG_LOGIN,
		/* input timeout */		30,
		/* result pointer */	tConf.GPRS2_Pass,
		/* pre function */		Pre_STEP_GRPS2_PASS,
		/* post function*/		NULL
	},

	//- DUALSIM

	//+ INGESTATE01 @jbotero 13/09/2015
	{
		/* step id */			STEP_IS_COMM_TYPE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tConf.iS_CommType,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IS_COMM_TYPE
	},

	{
		/* step id */			STEP_IS_HOST,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	tConf.iS_Host,
		/* pre function */		Pre_INPUT_IS_IP,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IS_PORT,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		L_PORT,
		/* input timeout */		30,
		/* result pointer */	tConf.iS_Port,
		/* pre function */		Pre_STEP_IS_PORT,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IS_TERMINAL,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	tConf.iS_ID,
		/* pre function */		Pre_STEP_IS_TERMINAL,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IS_PHONE,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		L_PHONE,
		/* input timeout */		30,
		/* result pointer */	tConf.iS_Phone,
		/* pre function */		Pre_STEP_IS_PHONE,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IS_PPP_USER,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		L_PHONE,
		/* input timeout */		30,
		/* result pointer */	tConf.iS_PPP_User,
		/* pre function */		Pre_STEP_IS_PPPS_USER,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IS_PPP_PASS,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		L_PHONE,
		/* input timeout */		30,
		/* result pointer */	tConf.iS_PPP_Pass,
		/* pre function */		Pre_STEP_IS_PPPS_PASS,
		/* post function*/		NULL
	},

	{
		/* step id */			STEP_IS_SSL,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tConf.iS_SSL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IS_SSL
	},
	{
		/* step id */			STEP_IS_SSL_PROFILE,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		LZ_SSLPROFILE,
		/* input timeout */		30,
		/* result pointer */	&tConf.iS_SSL_Profile,
		/* pre function */		Pre_STEP_IS_SSL_PROFILE,
		/* post function*/		NULL
	},
	{
		/* step id */			STEP_IS_SSL_VERSION,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tConf.iS_SSLVersion,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IS_SSL_VERSION
	},
	{
		/* step id */			STEP_SAVE_REPEAT_INGESTATE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_SAVE_INGESTATE
	},
	//- INGESTATE01

	{
		/* step id */			STEP_CONFIG_COMM,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_CONFIG_COMM
	},

	{
		/* step id */			STEP_LOCAL_MODE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_LOCAL_MODE,
		/* post function*/		Post_STEP_LOCAL_MODE
	},

	{
		/* step id */			STEP_LOCAL_IP,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_LOCAL_IP,
		/* post function*/		Post_STEP_LOCAL_IP
	},

	{
		/* step id */			STEP_LOCAL_MASK,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_LOCAL_MASK,
		/* post function*/		Post_STEP_LOCAL_MASK
	},

	{
		/* step id */			STEP_LOCAL_GATEWAY,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_LOCAL_GATEWAY,
		/* post function*/		Post_STEP_LOCAL_GATEWAY
	},

	{
		/* step id */			STEP_LOCAL_DNS1,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_LOCAL_DNS1,
		/* post function*/		Post_STEP_LOCAL_DNS1
	},

	{
		/* step id */			STEP_LOCAL_DNS2,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_LOCAL_DNS2,
		/* post function*/		Post_STEP_LOCAL_DNS2
	},

	{
		/* step id */			STEP_ETH_LOAD_CONFIG,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_ETH_LOAD_CONFIG,
		/* post function*/		Post_STEP_ETH_LOAD_CONFIG
	},
	{
		/* step id */			STEP_ETH_APPLY_CONFIG,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_ETH_APPLY_CONFIG,
		/* post function*/		Post_STEP_ETH_APPLY_CONFIG
	},
	{
		/* step id */			STEP_PCL_MODE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_PCL_MODE,
		/* post function*/		Post_STEP_PCL_MODE
	},
	{
		/* step id */			STEP_PCL_APPLY_CONFIG,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_PCL_APPLY_CONFIG
	},
	//Jonathan
	{	/* step id      */	    STEP_NAME_TERMINAL,
	    /* input type   */		INPUT_TEXT,
		/* input min    */		0,
		/* input max    */		L_APN,
		/* input timeout*/		50,
		/* result pointer*/	    tTerminal.Header_Line1,
		/* pre function */		Pre_STEP_NAME_TERMINAL,
		/* post function*/		NULL
	},
	//jonathan
	{
		/* step id */			STEP_TYPE_APLICATION,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&tTerminal.Comercio,
		/* pre function */		pre_PEDIR_CIERRE,
		/* post function*/		PRE_STEP_TYPE_APLICATION
	},
		//Jonathan
	{	/* step id      */	    STEP_IMPRIME,
		/* input type   */		INPUT_NONE,
		/* input min    */		0,
		/* input max    */		0,
		/* input timeout*/		30,
		/* result pointer*/	    NULL,
		/* pre function */		Pre_step_imprime,
		/* post function*/		NULL
	},
	{	/* step id      */	    STEP_IMPRIME_DEBITO,
		/* input type   */		INPUT_NONE,
		/* input min    */		0,
		/* input max    */		0,
		/* input timeout*/		30,
		/* result pointer*/	    NULL,
		/* pre function */		Pre_step_imprime_debito,
		/* post function*/		NULL
	},
	{	/* step id      */	    STEP_IMPRIME_CREDITO,
		/* input type   */		INPUT_NONE,
		/* input min    */		0,
		/* input max    */		0,
		/* input timeout*/		30,
		/* result pointer*/	    NULL,
		/* pre function */		Pre_step_imprime_credito,
		/* post function*/		NULL
	},
	{	/* step id      */	    STEP_PRE_IMPRIME,
		/* input type   */		INPUT_NONE,
		/* input min    */		0,
		/* input max    */		0,
		/* input timeout*/		30,
		/* result pointer*/	    NULL,
		/* pre function */		Pre_step_pre_imprime,
		/* post function*/		NULL
	},
	{	/* step id      */	STEP_IMPRIME_RECHAZADO,
	/* input type   */		INPUT_NONE,
	/* input min    */		0,
	/* input max    */		0,
	/* input timeout*/		30,
	/* result pointer*/	    NULL,
	/* pre function */		Pre_step_imprime_rechazado,
	/* post function*/		NULL
	},
	{	/* step id      */	STEP_IMPRIME_LOGO,
	/* input type   */		INPUT_NONE,
	/* input min    */		0,
	/* input max    */		0,
	/* input timeout*/		30,
	/* result pointer*/	    NULL,
	/* pre function */		Pre_step_imprime_logo,
	/* post function*/		NULL
	},
	{
		/* step id */			STEP_NUMERO_COPIAS,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		2,
		/* input timeout */		30,
		/* result pointer */	tTerminal.NCopias,
		/* pre function */		Pre_NUMERO_COPIAS,
		/* post function*/		Post_NUMERO_COPIAS
	},

	{
		/* step id */			STEP_IS_COMM_TYPE_HOST,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&axHost.CommunicationType,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IS_COMM_TYPE_HOST
	},
	{
		/* step id */			STEP_LOCAL_IP_HOST,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	IpTable.Host,
		/* pre function */		Pre_STEP_LOCAL_IP_HOST,
		/* post function*/		Post_STEP_LOCAL_IP_HOST
	},
	{
		/* step id */			STEP_LOCAL_IP_HOST_GPRS,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	IpTable.IP_GPRS,
		/* pre function */		Pre_STEP_LOCAL_IP_HOST_GPRS,
		/* post function*/		Post_STEP_LOCAL_IP_HOST_GPRS
	},
	{
		/* step id */			STEP_IS_PORT_HOST,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		L_PORT,
		/* input timeout */		30,
		/* result pointer */	IpTable.Port,
		/* pre function */		Pre_STEP_IS_PORT_HOST,
		/* post function*/		Post_STEP_IS_PORT_HOST
	},
	{
		/* step id */			STEP_IS_PORT_HOST_GPRS,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		L_PORT,
		/* input timeout */		30,
		/* result pointer */	IpTable.PORT_GPRS,
		/* pre function */		Pre_STEP_IS_PORT_HOST_GPRS,
		/* post function*/		Post_STEP_IS_PORT_HOST_GPRS
	},
	{
		/* step id */			STEP_IS_SSL_HOST,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	&IpTable.Flags1,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_IS_SSL_HOST
	},
	{
		/* step id */			STEP_SAVE_COMUNICACION,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_SAVE_COMUNICACION
	},
	{
		/* step id */			STEP_MODIFY_TERMINAL,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	&auxMerch.TID,
		/* pre function */		Pre_STEP_MODIFY_TERMINAL,
		/* post function*/		Post_STEP_MODIFY_TERMINAL
	},
	{
		/* step id */			STEP_SELECT_NUMBER_MERCHANT,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_NUMBER_MERCHANT
	},
	{
		/* step id */			STEP_MODIFY_AFILIADO,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	&auxMerch.MID,
		/* pre function */		Pre_STEP_MODIFY_AFILIADO,
		/* post function*/		Post_STEP_MODIFY_AFILIADO
	},
	{
		/* step id */			STEP_MODIFY_AFILIADO_CREDITO,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	&auxMerch.MIDC,
		/* pre function */		Pre_STEP_MODIFY_AFILIADO_CREDITO,
		/* post function*/		Post_STEP_MODIFY_AFILIADO_CREDITO
	},
	{
		/* step id */			STEP_MODIFY_AFILIADO_AMEX,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		L_LID,
		/* input timeout */		30,
		/* result pointer */	&auxMerch.MIDA,
		/* pre function */		Pre_STEP_MODIFY_AFILIADO_AMEX,
		/* post function*/		Post_STEP_MODIFY_AFILIADO_AMEX
	},
	{
		/* step id */			STEP_MODIFY_TPDU,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		10,
		/* input timeout */		30,
		/* result pointer */	&auxMerch.TPDU,
		/* pre function */		Pre_STEP_MODIFY_TPDU,
		/* post function*/		Post_STEP_MODIFY_TPDU
	},
	{
		/* step id */			STEP_MODIFY_NOMBREC,
		/* input type */		INPUT_TEXT,
		/* input min  */		0,
		/* input max  */		16,
		/* input timeout */		30,
		/* result pointer */	&tTerminal.Header_Line1,
		/* pre function */		Pre_STEP_MODIFY_NOMBREC,
		/* post function*/		Post_STEP_MODIFY_NOMBREC
	},
	{
		/* step id */			STEP_MODIFY_RIF,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		0,
		/* input max  */		9,
		/* input timeout */		30,
		/* result pointer */	&tTerminal.RifC,
		/* pre function */		Pre_STEP_MODIFY_RIF,
		/* post function*/		Post_STEP_MODIFY_RIF
	},
	{
		/* step id */			STEP_TONE_PULSE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		9,
		/* input timeout */		30,
		/* result pointer */	&tConf.Dial_Tone,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_DIAL_TONE_PULSO
	},
	{
		/* step id */			STEP_WIFI_SSID,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_WIFI_SSID,
		/* post function*/		Post_STEP_WIFI_SSID
	},

	{
		/* step id */			STEP_WIFI_SECURITY,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_WIFI_SECURITY,
		/* post function*/		Post_STEP_WIFI_SECURITY
	},

	{
		/* step id */			STEP_WIFI_KEY_PASS,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_WIFI_KEY_PASS,
		/* post function*/		Post_STEP_WIFI_KEY_PASS
	},

	{
		/* step id */			STEP_WIFI_ADDPROFILE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_WIFI_ADDPROFILE,
		/* post function*/		Post_STEP_WIFI_ADDPROFILE
	},

	{
		/* step id */			STEP_WIFI_LOAD_CONFIG,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_WIFI_LOAD_CONFIG,
		/* post function*/		Post_STEP_WIFI_LOAD_CONFIG
	},

	{
		/* step id */			STEP_WIFI_APPLY_CONFIG,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		0,
		/* result pointer */	NULL,
		/* pre function */		Pre_STEP_WIFI_APPLY_CONFIG,
		/* post function*/		Post_STEP_WIFI_APPLY_CONFIG
	},



};


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Init_TAB_GENERAL_CONF
 * Description:		Init table Init_TAB_GENERAL_CONF
 * 					if table is not created init tConf with default parameters
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file or from defualt parameters
 *  - RET_NOK - cannot init TAB_GENERAL_CONF
 * Notes:
 */
int Init_TAB_GENERAL_CONF(void)
{
	int RetFun;

	// Init TAB_GENERAL_CONF
	RetFun = TM_CreateTable(TAB_GENERAL_CONF, L_GENERAL_CONF);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	// Fins fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_GENERAL_CONF, &tConf);
	if(RetFun == TM_RET_NO_MORE_RECORDS)
	{
		// table doesn't have records, apply default recrods
		tConf.TimeOut = 30;
		tConf.iS_CommType = 1;
		tConf.iS_SSL = false;
		tConf.iP_CommType = 1;
		tConf.iP_Retries = 1;
		tConf.iP_ConnectTO = 30;
		tConf.iP_RspTO = 30;
		tConf.iP_SSL = false;
		tConf.iP_Report = false;

		//+ LLCOMMS01 @mamata Jun 17, 2015
		tConf.Dial_Tone = LLC_TONE;
		tConf.Dial_DetectTone = LLC_ENABLE;
		tConf.Dial_DetectBusy = LLC_ENABLE;
		tConf.Dial_ModemMode = LLC_MODEM_MODE_FASTCONNECT;
		tConf.Dial_BlindDialPause = 1;
		tConf.Dial_ToneTO = 0;
		tConf.Dial_PABXPause = 2;
		//- LLCOMMS01

		// add record to file
		RetFun = TM_AddRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// read record
		RetFun = TM_FindFirst(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Menu_GeneralConfiguration
 * Description:		Run menu of general configuration
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int Menu_GeneralConfiguration(void)
{
//+ DUALSIM @mamata Jun 24, 2015
//  NOTE: replace all content of function

	int RetFun;
	int MenuRet;
	ulong QuestionResult ;
	ulong QuestionResultMerchant;

#ifndef _DEBUG_
	RetFun = STM_RunTransaction(TRN_TECNICIAN_LOGON, STEP_LIST);
	CHECK(RetFun == RET_OK, LBL_MENU_END);
#endif

	do
	{
		UI_MenuReset();

		// build menu
//		UI_MenuAddItem(mess_getReturnedMessage(MSG_IPARAM), 2);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_TERMINAL), 1);

	//	if(IsRadioGPRS() || IsRadio3G() )
	//		UI_MenuAddItem(mess_getReturnedMessage(MSG_CONF_GPRS), 7);

//		UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIGCOMM), 6);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIGCOMM), 7);
		//+ LLCOMMS01 @mamata Jun 17, 2015
	//	UI_MenuAddItem(mess_getReturnedMessage(MSG_GENERAL_CONF), 1);
		//- LLCOMMS01
		//+ BATCH01 @mamata Aug 15, 2014
		UI_MenuAddItem(mess_getReturnedMessage(MSG_DELETE_BATCH), 5);
	//	UI_MenuAddItem(mess_getReturnedMessage(MSG_DELETE_ADVICE), 2);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_DELETE_REVERSE), 3);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_CONF_MERCHANT), 8);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_CONF_COMERCIO), 9);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_REPORT_PARA), 4);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIG_SEGUR), 10);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_EMPARAM), 11);
		UI_MenuAddItem("REPORTE AID", 12);
		UI_MenuAddItem("REPORTE DE BINES", 13);
		MenuRet = UI_MenuRun(mess_getReturnedMessage(MSG_GENERAL_CONF), 30, 1);
		CHECK(MenuRet > 0, LBL_MENU_END);

		switch(MenuRet)
		{
			case 1:
			RetFun = MenuTerminal();
				if(RetFun == STM_RET_CANCEL)
				{
					QuestionResultMerchant = 1;
				}
				else
				{
					QuestionResultMerchant = 0;
				}
				break;
			case 2:
		//		DeleteAllAdvice();
				QuestionResultMerchant = 1;
				break;

			//+ INGESTATE01 @jbotero 13/09/2015
			case 3:
				DeleteAllReverse();
//				QuestionResultMerchant = 1;
				break;
			//- INGESTATE01

			case 4:

				IMPRIMIR_PARAMETROS();
//				QuestionResultMerchant = 1;
				break;

			//+ BATCH01 @mamata Aug 15, 2014
			case 5:
				DeleteBatch();
//				QuestionResultMerchant = 1;
				break;
			//- BATCH01

			//+ LLCOMMS01 @mamata Jun 17, 2015
			case 6:
				RetFun = STM_RunTransaction(TRN_CONF_GPRS, CONF_STEP_LIST);
				if(RetFun == STM_RET_CANCEL)
				{
					QuestionResult = UI_ShowMessage( mess_getReturnedMessage(MSG_GENERAL_CONF),
													mess_getReturnedMessage(MSG_SAVE_CHANGES),
													mess_getReturnedMessage(MSG_YESNO),
													GL_ICON_QUESTION,
													GL_BUTTON_VALID_CANCEL, 30);
				}
				else
				{
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
								mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
								mess_getReturnedMessage(MSG_EXITO),
								GL_ICON_INFORMATION,
								GL_BUTTON_ALL,
								tConf.TimeOut);
					QuestionResult = 0;
				}
				break;
			//- LLCOMMS01

			case 7:
				Menu_Comunicacion();
				return RET_OK;
//				QuestionResultMerchant = 1;
            break;
			case 8:
				UI_ClearAllLines();
				if( Tree_Conf != NULL )
				{
					TlvTree_Release(Tree_Conf);
					Tree_Conf = NULL;
				}
				Tree_Conf = TlvTree_New(0);
				RetFun = STM_RunTransaction(TRN_CONF_MERCHANT, CONF_STEP_LIST);
				if(RetFun == STM_RET_CANCEL)
				{
					QuestionResultMerchant = UI_ShowMessage( mess_getReturnedMessage(MSG_CONF_MERCHANT),
													mess_getReturnedMessage(MSG_SAVE_CHANGES),
													mess_getReturnedMessage(MSG_YESNO),
													GL_ICON_QUESTION,
													GL_BUTTON_VALID_CANCEL, 30);
				}
				else
				{
					QuestionResultMerchant = 0;
				}
				if( Tree_Conf != NULL )
				{
					TlvTree_Release(Tree_Conf);
					Tree_Conf = NULL;
				}
				break;
			case 9:
				CONFIG_COMERCIO();
//				QuestionResultMerchant = 1;
				break;
			case 10:
				RetFun = CONFIG_SEGURIDAD();

				if(RetFun ==  PANEL_CONTROL)
					return PANEL_CONTROL;

				break;
			case 11:
				Menu_Ingestate();
				return RET_OK;
				break;
			case 12:
				ViewAids();
				return RET_OK;
				break;
			case 13:
				UI_ClearAllLines();
				Report_PrintBines();
				return RET_OK;
				break;
			default:
				return RET_NOK;
		}

		if(QuestionResult == GL_KEY_VALID)
		{
			RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
			CHECK(RetFun == TM_RET_OK, LBL_SAVE_ERROR);
		}
		if(QuestionResultMerchant == GL_KEY_VALID || QuestionResultMerchant == 0)
		{
			RetFun = TM_ModifyRecord(TAB_MERCHANT, &auxMerch);
			CHECK(RetFun == TM_RET_OK, LBL_SAVE_ERROR);

			UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
						mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
						mess_getReturnedMessage(MSG_EXITO),
						GL_ICON_INFORMATION,
						GL_BUTTON_ALL,
						tConf.TimeOut);
		}

	}while(MenuRet > 0);

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;

LBL_SAVE_ERROR:
	return RET_NOK;
//- DUALSIM
}

/* --------------------------------------------------------------------------
 * Function Name:	Menu_de_comunicacion_nuevo
 * Description:		Run IngeLoad menu
 * Parameters:
 *  - none
 * Return:
 * 	- RET_OK
 * 	- RET_NOK - user cancel
 * Notes:
 */
int Menu_Comunicacion(void)
{
	int RetFun;
	ulong QuestionResult = 0;

	// build menu
	UI_MenuReset();
	UI_MenuAddItem("Ethernet/Wifi", 1);
	UI_MenuAddItem("GPRS", 2);
	UI_MenuAddItem("Dial-UP", 3);
	// run menu
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_GENERAL_CONF), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	switch(RetFun)
	{
		case 1:
			RetFun = STM_RunTransaction(TRN_CONFIG_COMM_ETHERNET, CONF_STEP_LIST);

			break;
		case 2:
			RetFun = STM_RunTransaction(TRN_CONF_GPRS, CONF_STEP_LIST);

			break;
		case 3:
			RetFun = STM_RunTransaction(TRN_CONFIG_COMM_DIAL, CONF_STEP_LIST);

			break;
		default:
			return RET_NOK;
	}


	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_MENU_END);
		return STM_RET_NEXT;
	}

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Menu_IngeLoad
 * Description:		Run IngeLoad menu
 * Parameters:
 *  - none
 * Return:
 * 	- RET_OK
 * 	- RET_NOK - user cancel
 * Notes:
 */
int Menu_IngeLoad(void)
{
	int RetFun;
	ulong QuestionResult = 0;

	// build menu
	UI_MenuReset();
	UI_MenuAddItem("INICIALIZAR", 1);
	UI_MenuAddItem("CONFIGURAR INGELOAD", 2);
	// run menu
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_GENERAL_CONF), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	switch(RetFun)
	{
		case 1:
			//+ BATCH01 @mamata Aug 15, 2014
			Batch_DeleteSettled();
			//- BATCH01
			IngeLoad_LoadParameters();
			break;
		case 2:
			//+ LLCOMMS01 @mamata Jun 17, 2015
			RetFun = STM_RunTransaction(TRN_IPARAMETERS_CONF, CONF_STEP_LIST);
			//d RetFun = STM_RunTransaction(TRN_IPARAMETERS_CONF, STEP_LIST);
			//- LLCOMMS01
			if(RetFun == STM_RET_CANCEL)
				QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_IPARAM),
												mess_getReturnedMessage(MSG_SAVE_CHANGES),
												mess_getReturnedMessage(MSG_YESNO),
												GL_ICON_QUESTION,
												GL_BUTTON_VALID_CANCEL,
												30);
			else
				QuestionResult = 0;
			break;
		default:
			return RET_NOK;
	}


	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_MENU_END);
		return STM_RET_NEXT;
	}

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}

//+ INGESTATE01 @jbotero 13/09/2015

/* --------------------------------------------------------------------------
 * Function Name: GenerateAndSave
 * Description: Defines a EM configuration and saves it on TMS Manager
 * Parameters:
 * -none
 * Return:
 * 	- RET_OK
 * 	- RET_NOK - Error on the saving process
 * Notes:
 */

int GenerateAndSave()
{
	EM_CONF TmsConf;
	int RetFun = 0;
	memset( &TmsConf, 0x00, sizeof(TmsConf) );
	strcpy( TmsConf.iP_Host, tConf.iS_Host);
	strcpy( TmsConf.Port, tConf.iS_Port);
	strcpy( TmsConf.Phone, tConf.iS_Phone);
	TmsConf.type_modem = VIP;
	//Map Communication Type
	switch(tConf.iS_CommType)
 	{
		case LLC_DIAL:
			TmsConf.ConnType = TMSIPRTC;
			strcpy( TmsConf.login,  tConf.iS_PPP_User );		// ISP Login.
			strcpy( TmsConf.password, tConf.iS_PPP_Pass );	// ISP Password.
			break;
		case LLC_PCL:
			SPM_start_service();
			TmsConf.ConnType = TMSIP;
			break;
		case LLC_IP:
		case LLC_WIFI:
			TmsConf.ConnType = TMSIP;
			break;
		case LLC_GPRS:
			TmsConf.ConnType = TMSGPRS;
			strcpy( TmsConf.apn,  	 tConf.GPRS_APN );			// GPRS APN.
			strcpy( TmsConf.login,	 tConf.GPRS_User );			// GPRS Login.
			strcpy( TmsConf.password, tConf.GPRS_Pass );		// GPRS Password.
			break;
		default:
			TmsConf.ConnType = RTC;
			break;
	}
	if(tConf.iS_SSL == TRUE){
		strcpy(TmsConf.SSLProfile, tConf.iS_SSL_Profile);
		TmsConf.iS_SSLVersion=tConf.iS_SSLVersion;
		TmsConf.iS_SSLCipher=tConf.iS_SSLCipher2;
		TmsConf.iS_SSLExportMask=tConf.iS_SSLExportMask2;
		#ifndef _DEBUG_
			tConf.FlagSSLProfSigned=TRUE;
			TmsConf.FlagSSLProfSigned=tConf.FlagSSLProfSigned;
		#else
			tConf.FlagSSLProfSigned=FALSE;
			TmsConf.FlagSSLProfSigned=tConf.FlagSSLProfSigned;
		#endif
	}
	else
		memset(TmsConf.SSLProfile, 0x00, sizeof(TmsConf.SSLProfile));


	RetFun=TMS_Save_Parameters(&TmsConf);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	return RetFun;

	LBL_ERROR:
		return RET_NOK;
}

/* ------------------------------------------------------------------------------
 * Function Name:	CallEstateManager
 * Date:		 	7/07/2014
 * Description: Function to do the remote download of parameters
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
int CallEstateManager(bool autoRestart)
{
	int RetFun = 0;
	int StatusNetwork = 0;

	DisplayHeader(_OFF_);

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, "ACTUALIZACION CRITICA", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, "El equipo se", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE3, "actualizara favor", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, "NO apagar...", UI_ALIGN_CENTER);
	UI_ShowInfoScreen(NULL);

	//Delay 2 seconds
	Telium_Ttestall(0,300);

	//Map Communication Type
	switch(tConf.iS_CommType)
 	{
		case LLC_PCL:
			break;
		case LLC_IP:
		case LLC_WIFI:
			SPM_stop_service();
			if(tConf.iS_CommType == LLC_IP)
			{
				//Status
				RetFun = LL_Network_GetStatus(LL_PHYSICAL_V_ETHERNET, &StatusNetwork);
				CHECK(RetFun == RET_OK && StatusNetwork == LL_STATUS_ETHERNET_AVAILABLE, LBL_COMM_ERROR);
			}
			else
			{
				//Status
				RetFun = LL_Network_GetStatus(LL_PHYSICAL_V_WIFI, &StatusNetwork);
				CHECK(RetFun == RET_OK && StatusNetwork == LL_STATUS_WIFI_CONNECTED, LBL_COMM_ERROR);
			}

			break;
		default:
			break;
	}
	//Destroy last Goal window
	UI_Destroy_GOAL_Instance();

	DisplayHeader(_ON_);
	RetFun = TMS_Call(AutoStart_OFF);
	DisplayHeader(_ON_);

	switch(RetFun)
	{
		case REMOTE_CALL_SUCCESSFULL:
					RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
					CHECK(RetFun == RET_OK, LBL_ERROR);
					RetFun = Ingestate_ProcessPAR();
					break;
		case REMOTE_CALL_SUCCESSFUL_NO_INSTALL:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_NO_INST),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		case REMOTE_CALL_BAD_ACTIVITY:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_BAD_ACT),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		case REMOTE_CALL_ARG_MISSING:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_ARG_MISS),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		case REMOTE_CALL_ARG_INVALID:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_ARG_INV),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		case REMOTE_CALL_PPP_CONFIG_FAIL:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_PPP_CONFIG_FAIL),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		case REMOTE_CALL_CONNECTION_FAIL:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_CON),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		case REMOTE_CALL_CONNECTION_LOST:
					UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
										"", mess_getReturnedMessage(MSG_ERROR_EM_CON_LOST),
										GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					break;
		default:
			UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
								"", mess_getReturnedMessage(MSG_ERROR_INGESTATE),
								GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			break;

	}

	if (autoRestart && RetFun == RET_OK)
		//RebootTerminal();
	return RetFun;

LBL_ERROR:
	UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
			mess_getReturnedMessage(MSG_ERROR_EM_COMM),NULL,
			GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_NOK;


LBL_COMM_ERROR:

	UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
			mess_getReturnedMessage(MSG_ERROR_EM_LINK_L1),
			mess_getReturnedMessage(MSG_ERROR_EM_LINK_L2),
			GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

	UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
			mess_getReturnedMessage(MSG_ERROR_EM_COMM),NULL,
			GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Menu_Ingestate
 * Description:		Run Ingestate menu
 * Parameters:
 *  - none
 * Return:
 * 	- RET_OK
 * 	- RET_NOK - user cancel
 * Notes:
 */
int Menu_Ingestate(void)
{
	int RetFun;
	ulong QuestionResult = 0;

	// build menu
	UI_MenuReset();
	UI_MenuAddItem("INICIALIZAR", 1);
	UI_MenuAddItem("CONFIGURAR", 2);
	// run menu
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_GENERAL_CONF), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	switch(RetFun)
	{
		case 1:
			Batch_DeleteSettled();
			CallEstateManager(TRUE);
			break;
		case 2:
			RetFun = STM_RunTransaction(TRN_INGESTATE_CONF, CONF_STEP_LIST);

			if(RetFun == STM_RET_CANCEL)
				QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
												mess_getReturnedMessage(MSG_SAVE_CHANGES),
												mess_getReturnedMessage(MSG_YESNO),
												GL_ICON_QUESTION,
												GL_BUTTON_VALID_CANCEL,
												30);
			else
				QuestionResult = 0;
			break;
		default:
			return RET_NOK;
	}


	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_MENU_END);
		RetFun=GenerateAndSave();
		CHECK(RetFun == TM_RET_OK, LBL_ERROR_GS);
		return STM_RET_NEXT;

	}

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
LBL_ERROR_GS:
	UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
				   mess_getReturnedMessage(MSG_SSL_ERROR_CONF),NULL,
				   GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	WriteHostEstateManagerConfigFile
 * Description:		Overwrite HOST file with DDLL & POS serial number
 * 					Useful to register on EM full serial number, and not serial logic
 * Parameters:
 *  - nothing
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */

int WriteHostFileForEstateManager(void)
{
	char FullSerialNumberRaw[50];
	char FullSerialNumber[50];
	char TempBuff[50] = {0x00, };

	memset(FullSerialNumberRaw, 0x00, sizeof(FullSerialNumberRaw));
	memset(FullSerialNumber, 0x00, sizeof(FullSerialNumber));

	PSQ_Give_Full_Serial_Number((uint8*)FullSerialNumberRaw);
	Format_String_Lenght(FullSerialNumberRaw, FullSerialNumber, _FORMAT_TRIM, strlen(FullSerialNumberRaw), ' ' );

	snprintf(TempBuff, sizeof(TempBuff), "DDLL=%s", tConf.iS_ID);
	snprintf(TempBuff + strlen(TempBuff), sizeof(TempBuff) - strlen(TempBuff), "\r\nSERIAL=%s", FullSerialNumber);

	//SERIAL=%s
	//File used by INGESTATE TBK, to check DDLL for parameters download
	FL_WriteBuffer((uint8*)"/HOST/DDLL.CFG", TempBuff, strlen(TempBuff), false);

	return RET_OK;
}


//- INGESTATE01

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_PABX
 * Description:		Pre function of STEP_PABX
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_PABX(void)
{

//	if(axHost.CommunicationType != LLC_DIAL)
//		return FALSE;
	int RetFun = 0;

	RetFun = TM_FindFirst(TAB_HOST, &axHost);
		if (RetFun != TM_RET_OK)
			return FALSE;

	RetFun = TM_FindFirst(TAB_IP, &IpTable);
		if (RetFun != TM_RET_OK)
			return FALSE;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_GENERAL_CONF), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_PABX), UI_ALIGN_CENTER);

	// set initial value if is present
	if(tConf.Dial_PABX[0] != 0)
		memcpy(Step_Buffer, tConf.Dial_PABX, LZ_PABX);

	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	STEP_GPRS_APN
 * Description:		Pre function of STEP_GPRS_APN
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GPRS_APN(void)
{
	int RetFun = 0;

	RetFun = TM_FindFirst(TAB_HOST, &axHost);
		if (RetFun != TM_RET_OK)
			return FALSE;

	RetFun = TM_FindFirst(TAB_IP, &IpTable);
		if (RetFun != TM_RET_OK)
			return FALSE;

	if(!IsGPRS())
		return FALSE;



//	if(axHost.CommunicationType != LLC_GPRS){
//		return FALSE;
//	}
	UI_ClearAllLines();
	//+ DUALSIM @mamata Jun 24, 2015
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_GPRS), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE3, "SIM 1", UI_ALIGN_CENTER);
	//- DUALSIM
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_GPRS_APN), UI_ALIGN_CENTER);

	// set initial value if is present
	if(tConf.GPRS_APN[0] != 0)
		memcpy(Step_Buffer, tConf.GPRS_APN, LZ_APN);

	return TRUE;
}
//eugenio
bool Pre_STEP_NAME_TERMINAL(void)
{
	//+ DUALSIM @mamata Jun 24, 2015
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_NOM_BANC), UI_ALIGN_CENTER);
	//- DUALSIM

	// set initial value if is present
	if(tTerminal.Header_Line1[0] != 0)
		memcpy(Step_Buffer, tTerminal.Header_Line1, LZ_APN);

	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_GPRS_USER
 * Description:		Pre function of STEP_GPRS_USER
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GPRS_USER(void)
{
	if(!IsGPRS())
		return FALSE;

//	if(axHost.CommunicationType != LLC_GPRS){
//		return FALSE;
//	}
	// set initial value if is present
	if(tConf.GPRS_User[0] != 0)
		memcpy(Step_Buffer, tConf.GPRS_User, LZ_LOGIN);

	//+ DUALSIM @mamata Jun 24, 2015
	UI_SetLine(UI_LINE3, "SIM 1", UI_ALIGN_CENTER);
	//- DUALSIM
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_GPRS_USR), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_GRPS_PASS
 * Description:		Pre function of STEP_GRPS_PASS
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GRPS_PASS(void)
{
	if(!IsGPRS())
		return FALSE;

//	if(axHost.CommunicationType != LLC_GPRS){
//		return FALSE;
//	}

	// set initial value if is present
	if(tConf.GPRS_Pass[0] != 0)
		memcpy(Step_Buffer, tConf.GPRS_Pass, LZ_LOGIN);

	//+ DUALSIM @mamata Jun 24, 2015
	UI_SetLine(UI_LINE3, "SIM 1", UI_ALIGN_CENTER);
	//- DUALSIM
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_GPRS_PASS), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TIME_OUT
 * Description:		Pre function of STEP_TIME_OUT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_TIME_OUT(void)
{
	Binasc((uint8*)Step_Buffer, tConf.TimeOut, 2);
	//+ MESS01 @mamata Jun 10, 2015
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TIMEOUT), UI_ALIGN_CENTER);
	//d UI_SetLine(UI_LINE4, mess_getReturnedMessage(MGS_TIMEOUT), UI_ALIGN_CENTER);
	//- MESS01
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TIME_OUT
 * Description:		Post function of STEP_TIME_OUT
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_Commit_UINT8(char *Result)
{
	*Result = (uint8)atoi(Step_Buffer);
	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_TERMINAL
 * Description:		Pre function of STEP_IP_TERMINAL
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_IP_TERMINAL(void)
{
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_IPARAM), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TERMINALID), UI_ALIGN_CENTER);

	memcpy(Step_Buffer, tConf.iP_ID, LZ_LID);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IP_COMM_TYPE
 * Description:		Post function of STEP_IP_COMM_TYPE
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IP_COMM_TYPE(char *Result)
{
	int RetFun = 0;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DIAL), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ETH), 2);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_GPRS), 3);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI), 4);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_COMM_TYPE), 30, tConf.iP_CommType);
	CHECK(RetFun > 0, LBL_CANCEL);

	*Result = (uint8)RetFun;

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_NII
 * Description:		Pre function of STEP_IP_NII
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_NII(void)
{
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_NII), UI_ALIGN_CENTER);
	memcpy(Step_Buffer, tConf.iP_NII, LZ_NII);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_RETRIES
 * Description:		Pre function of STEP_IP_RETRIES
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_RETRIES(void)
{
	Binasc((uint8*)Step_Buffer, tConf.iP_Retries, 2);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_RETRIES), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_CONNECT_TO
 * Description:		Pre function of STEP_IP_CONNECT_TO
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_CONNECT_TO(void)
{
	Binasc((uint8*)Step_Buffer, tConf.iP_ConnectTO, 2);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONNECT_TO), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_RESPONSE_TO
 * Description:		Pre function of STEP_IP_RESPONSE_TO
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_RESPONSE_TO(void)
{
	Binasc((uint8*)Step_Buffer, tConf.iP_RspTO, 2);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_RESPONSE_TO), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_PHONE1
 * Description:		Pre function of STEP_IP_PHONE1
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_PHONE1(void)
{

//	if(axHost.CommunicationType != LLC_DIAL)
//		return FALSE;
	UI_ClearAllLines();
	memcpy(Step_Buffer,axHost.Dial_Primary_Phone1, LZ_PHONE);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_PRIMARY_PHONE), UI_ALIGN_CENTER);

	return TRUE;
}
int Post_STEP_IP_PHONE1(char *Result){

	memcpy(axHost.Dial_Primary_Phone1,Step_Buffer, LZ_PHONE);

	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_PHONE2
 * Description:		Pre function of STEP_IP_PHONE2
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_PHONE2(void)
{
//	if(axHost.CommunicationType != LLC_DIAL)
//		return FALSE;

	memcpy(Step_Buffer,axHost.Dial_Primary_Phone2, LZ_PHONE);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_SECUNDARY_PHONE), UI_ALIGN_CENTER);
	return TRUE;
}
int Post_STEP_IP_PHONE2(char *Result){

	memcpy(axHost.Dial_Primary_Phone2,Step_Buffer, LZ_PHONE);

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_INPUT_IP
 * Description:		Pre function of INPUT_IP
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_INPUT_IP(void)
{
//	if(tConf.iP_CommType == 1)
//		return FALSE;

	memcpy(Step_Buffer, tConf.iP_Host, LZ_HOST);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_IP), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IP_PORT
 * Description:		Pre function of STEP_IP_PORT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IP_PORT(void)
{
//	if(tConf.iP_CommType == 1)
//		return FALSE;

	memcpy(Step_Buffer, tConf.iP_Port, LZ_PORT);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_PORT), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IP_SSL
 * Description:		Post function of STEP_IP_SSL
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IP_SSL(char *Result)
{
	int RetFun = 0;
	int SelectedValue;

//	if(tConf.iP_CommType == 1)
//		return FALSE;

	//	if(axHost.CommunicationType != LLC_IP)
	//		return FALSE;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ENABLE), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DISABLE), 2);

	if(tConf.iP_SSL)
		SelectedValue = 1;
	else
		SelectedValue = 2;

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_SSL), 30, SelectedValue);
	CHECK(RetFun > 0, LBL_CANCEL);

	if(RetFun == 1)
		*Result = (bool)TRUE;
	else
		*Result = (bool)FALSE;

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IP_REPORT
 * Description:		Post function of IP_REPORT
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IP_REPORT(char *Result)
{
	int RetFun = 0;
	int SelectedValue;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ENABLE), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DISABLE), 2);

	if(tConf.iP_Report)
		SelectedValue = 1;
	else
		SelectedValue = 2;

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_REPORT), 30, SelectedValue);
	CHECK(RetFun > 0, LBL_CANCEL);

	if(RetFun == 1)
		*Result = (bool)TRUE;
	else
		*Result = (bool)FALSE;

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SAVE_GENERAL_CONF
 * Description:		Post function of STEP_SAVE_IPTABLE
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_SAVE_GENERAL_CONF(char *Result)
{
	int RetFun = 0;
	ulong QuestionResult;

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		return STM_RET_NEXT;
	}

	return STEP_PABX;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SAVE_INGELOAD
 * Description:		Post function of STEP_SAVE_REPEAT_INGELOAG
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_SAVE_INGELOAD(char *Result)
{
	int RetFun = 0;
	ulong QuestionResult;

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_IPARAM), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		return STM_RET_NEXT;
	}

	return STEP_IP_TERMINAL;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TECHNICIAN_PASSWORD
 * Description:		Pos funciton for STEP_TECHNICIAN_PASSWORD
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_TECHNICIAN_PASSWORD(void)
{
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_GENERAL_CONF), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TECH_PASSWORD), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TECHNICIAN_PASSWORD
 * Description:		Post function of STEP_TECHNICIAN_PASSWORD
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_TECHNICIAN_PASSWORD(char *Result)
{
	if(tTerminal.Password_Technician[0] == 0)
	{
		if(strcmp(Step_Buffer, Technician_Password) == 0 )
			return STM_RET_NEXT;
	}
	else
	{
		if(strcmp(Step_Buffer, tTerminal.Password_Technician) == 0 )
			return STM_RET_NEXT;
	}
	UI_ShowMessage(	mess_getReturnedMessage(MSG_GENERAL_CONF),
					mess_getReturnedMessage(MSG_ERROR_BAD_PASSWORD),
					mess_getReturnedMessage(MSG_TRY_AGAIN),
					GL_ICON_ERROR,
					GL_BUTTON_ALL,
					30);
	return STM_RET_AGAIN;
}


//+ BATCH01 @mamata Aug 15, 2014
/* --------------------------------------------------------------------------
 * Function Name:	DeleteBatch
 * Description:		Delete batch
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int DeleteBatch(void)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint32 MerchantID;
	TABLE_MERCHANT Merchant;

	// release Tree_Tran memory
	if(Tree_Tran != NULL)
	{
		TlvTree_Release(Tree_Tran);
		Tree_Tran = NULL;
	}

	// create memory for Tree_Trann
	Tree_Tran = TlvTree_New(0);
	CHECK(Tree_Tran != NULL, LBL_ERROR)

	// run merchatn selection
	RetFun = STM_RunTransaction(TRN_DELETE_BATCH, STEP_LIST);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// node merchant selected
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(uint32*)TlvTree_GetData(node);


	RetFun = TM_DeleteTable(TAB_MESERO);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	RetFun = TM_CreateTable(TAB_MESERO, L_TABLE_MESERO);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DELETE_BATCH), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_DELETE_BATCH), UI_ALIGN_CENTER);
	RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CONFIRMATION), tConf.TimeOut);
	CHECK(RetFun == RET_OK, LBL_CANCEL);

	if(MerchantID == 99999)
	{
		// delete all batchs
		RetFun = Batch_DeleteAllBatch();
	}
	else
	{
		// delete specific merchant
		RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
		CHECK(RetFun == RET_OK, LBL_ERROR);
		RetFun = TM_DeleteTable(Merchant.BatchName);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	}

	CHECK(RetFun == RET_OK, LBL_ERROR)
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_BATCH_DELETED), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);


	CHECK(tTerminal.Flags1 & TERM_FLAG1_SETTLE_OBLIGADO, LBL_NO_CIERRE);
	 TM_FindFirst(TAB_TERMINAL, &tTerminal);
			tTerminal.Flags1 &= ~TERM_FLAG1_SETTLE_OBLIGADO;
			TM_ModifyRecord(TAB_TERMINAL, &tTerminal);

	LBL_NO_CIERRE:
	// init batch files
	Batch_InitTables();
	return RET_OK;

LBL_ERROR:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_DELETE_BATCH), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

LBL_CANCEL:
	return RET_NOK;
}
//- BATCH01

//+ LLCOMMS01 @mamata Jun 17, 2015
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_DIAL_DETECT_TONE
 * Description:		Post funciont for STEP_DIAL_DETECT_TONE
 * Author:			mamata
 * Parameters:
 *  char *Result
 * Return:
 *  STM_RET_NEXT
 *  STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_DIAL_DETECT_TONE(char *Result)
{
//	if(axHost.CommunicationType != LLC_DIAL){
//		return STM_RET_NEXT;
//	}

	int RetFun;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ENABLE), LLC_ENABLE);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DISABLE), LLC_DISABLE);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_DETECT_TONE), tConf.TimeOut, tConf.Dial_DetectTone);
	CHECK(RetFun >= 0, LBL_CANCEL);
	tConf.Dial_DetectTone = RetFun;
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_DIAL_DETECT_TONE
 * Description:		Post funciont for STEP_DIAL_DETECT_BUSY
 * Author:			mamata
 * Parameters:
 *  char *Result
 * Return:
 *  STM_RET_NEXT
 *  STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_DIAL_DETECT_BUSY(char *Result)
{
//	if (axHost.CommunicationType != LLC_DIAL)
//	return STM_RET_NEXT;

	int RetFun;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ENABLE), LLC_ENABLE);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DISABLE), LLC_DISABLE);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_DETECT_BUSY), tConf.TimeOut, tConf.Dial_DetectBusy);
	CHECK(RetFun >= 0, LBL_CANCEL);
	tConf.Dial_DetectBusy = RetFun;
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}


int Post_STEP_DIAL_TONE_PULSO(char *Result)
{
//	if(axHost.CommunicationType != LLC_DIAL)
//		return STM_RET_NEXT;
		//return FALSE;

	int RetFun;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TONO), LLC_TONE);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_PULSO), LLC_PULSE);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_MODEM_MODE), tConf.TimeOut, tConf.Dial_Tone);
	CHECK(RetFun >= 0, LBL_CANCEL);
	tConf.Dial_Tone = RetFun;
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_DIAL_MODEM_MODE
 * Description:		Post funciont for STEP_DIAL_MODEM_MODE
 * Author:			mamata
 * Parameters:
 *  char *Result
 * Return:
 *  STM_RET_NEXT
 *  STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_DIAL_MODEM_MODE(char *Result)
{

//	if (axHost.CommunicationType != LLC_DIAL)
//		return STM_RET_NEXT;

	int RetFun;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_MODE_FASTCONNECT), LLC_MODEM_MODE_FASTCONNECT);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_MODE_V22V22BIS), LLC_MODEM_MODE_V22_V22BIS);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_MODE_ASYN), LLC_MODEM_MODE_ASYNC);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_MODEM_MODE), tConf.TimeOut, tConf.Dial_ModemMode);
	CHECK(RetFun >= 0, LBL_CANCEL);
	tConf.Dial_ModemMode = RetFun;
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_DIAL_BLIND_PAUSE
 * Description:		Pre function for STEP_DIAL_BLIND_PAUSE
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  TRUE
 * Notes:
 */
bool Pre_STEP_DIAL_BLIND_PAUSE(void)
{
//	if (axHost.CommunicationType != LLC_DIAL)
//		return FALSE;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_DIAL),UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_BLIND_DAIL_PAUSE), UI_ALIGN_CENTER);

	if(tConf.Dial_BlindDialPause != 0)
	{
		Uint64_to_Ascii(tConf.Dial_BlindDialPause, Step_Buffer, _FORMAT_TRIM, 0, 0);
	}
	else
	{
		Step_Buffer[0] = '0';
	}
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_DIAL_BLIND_PAUSE
 * Description:		Post function for Post_STEP_DIAL_BLIND_PAUSE
 * Author:			mamata
 * Parameters:
 *  char *Result
 * Return:
 *  STM_RET_NEXT
 * Notes:
 */
int Post_STEP_DIAL_BLIND_PAUSE(char *Result)
{
	if(Step_Buffer[0] == 0)
	{
		tConf.Dial_BlindDialPause = 0;
	}
	else
	{
		tConf.Dial_BlindDialPause = atoi(Step_Buffer);
	}
	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_DIAL_TONE_TO
 * Description:		Pre function for STEP_DIAL_TONE_TO
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  TRUE
 * Notes:
 */
bool Pre_STEP_DIAL_TONE_TO(void)
{
//	if (axHost.CommunicationType != LLC_DIAL)
//		return FALSE;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_DIAL),UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TONE_TO), UI_ALIGN_CENTER);

	if(tConf.Dial_ToneTO != 0)
	{
		Uint64_to_Ascii(tConf.Dial_ToneTO, Step_Buffer, _FORMAT_TRIM, 0, 0);
	}
	else
	{
		Step_Buffer[0] = '0';
	}
	return TRUE;
}


int Post_STEP_DIAL_TONE_TO(char *Result)
{
	if(Step_Buffer[0] == 0)
	{
		tConf.Dial_ToneTO = 0;
	}
	else
	{
		tConf.Dial_ToneTO = atoi(Step_Buffer);
	}
	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_DIAL_PABX_PAUSE
 * Description:		Pre function for STEP_DIAL_PABX_PAUSE
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  TRUE
 * Notes:
 */
bool Pre_STEP_DIAL_PABX_PAUSE(void)
{
//	if (axHost.CommunicationType != LLC_DIAL)
//	return FALSE;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_DIAL),UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_PABX_PAUSE), UI_ALIGN_CENTER);

	if(tConf.Dial_PABXPause != 0)
	{
		Uint64_to_Ascii(tConf.Dial_PABXPause, Step_Buffer, _FORMAT_TRIM, 0, 0);
	}
	else
	{
		Step_Buffer[0] = '0';
	}
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_DIAL_PABX_PAUSE
 * Description:		Post function for STEP_DIAL_PABX_PAUSE
 * Author:			mamata
 * Parameters:
 *  char *Result
 * Return:
 *  STM_RET_NEXT
 * Notes:
 */
int Post_STEP_DIAL_PABX_PAUSE(char *Result)
{
	if(Step_Buffer[0] == 0)
	{
		tConf.Dial_PABXPause = 0;
	}
	else
	{
		tConf.Dial_PABXPause = atoi(Step_Buffer);
	}
	return STM_RET_NEXT;
}
//- LLCOMMS01


//+ DUALSIM @mamata Jun 24, 2015
/* --------------------------------------------------------------------------
 * Function Name:	STEP_GPRS_APN
 * Description:		Pre function of STEP_GPRS_APN
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GPRS2_APN(void)
{
	if(gprs_is_dual_sim())
		return FALSE;

	UI_SetLine(UI_LINE3, "SIM 2", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_GPRS_APN), UI_ALIGN_CENTER);

	// set initial value if is present
	if(tConf.GPRS2_APN[0] != 0)
		memcpy(Step_Buffer, tConf.GPRS2_APN, LZ_APN);

	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_GPRS_USER
 * Description:		Pre function of STEP_GPRS_USER
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GPRS2_USER(void)
{
	if(gprs_is_dual_sim())
		return FALSE;

	// set initial value if is present
	if(tConf.GPRS2_User[0] != 0)
		memcpy(Step_Buffer, tConf.GPRS2_User, LZ_LOGIN);

	UI_SetLine(UI_LINE3, "SIM 2", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_GPRS_USR), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_GRPS_PASS
 * Description:		Pre function of STEP_GRPS_PASS
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Is GPRS terminal
 *  - FALSE - No is a GPRS terminal
 * Notes:
 */
bool Pre_STEP_GRPS2_PASS(void)
{
	if(gprs_is_dual_sim())
		return FALSE;

	// set initial value if is present
	if(tConf.GPRS2_Pass[0] != 0)
		memcpy(Step_Buffer, tConf.GPRS2_Pass, LZ_LOGIN);

	UI_SetLine(UI_LINE3, "SIM 2", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_GPRS_PASS), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentGPRSAPN
 * Description:		Get APN for current sim
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  char* - pointer to current APN
 * Notes:
 */
char *GetCurrentGprsAPN(void)
{
	int CurrentSim = 0;
	gprs_get_sim_slot(&CurrentSim);
	if(CurrentSim != 0)
		return tConf.GPRS2_APN;
	return tConf.GPRS_APN;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentGprsUser
 * Description:		Get ppp user for current sim
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  char* - pointer to current ppp user
 * Notes:
 */
char *GetCurrentGprsUser(void)
{
	int CurrentSim = 0;
	gprs_get_sim_slot(&CurrentSim);
	if(CurrentSim != 0)
		return tConf.GPRS2_User;
	return tConf.GPRS_User;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentGprsPassword
 * Description:		Get ppp password for current sim
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  char* - pointer to current ppp password
 * Notes:
 */
char *GetCurrentGprsPassword(void)
{
	int CurrentSim = 0;
	gprs_get_sim_slot(&CurrentSim);
	if(CurrentSim != 0)
		return tConf.GPRS2_Pass;
	return tConf.GPRS_Pass;
}


//#include "ExtraGPRS.h"
/* --------------------------------------------------------------------------
 * Function Name:	GprsSwitchSIM
 * Description:		function for switch sim card
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  RET_OK;
 *  RET_NOK;
 * Notes:
 */
int GprsSwitchSIM(void)
{
	int RetFun;
	int CurrentSim = 100;

	// verify is is a dual sim terminal
	CHECK(gprs_is_dual_sim() == GPRS_OK, LBL_NO_DUAL_SIM);

	// show confirmation screen to switch sim
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_GPRS), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, "CAMBIO DE SIM", UI_ALIGN_CENTER);
	RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CONFIRMATION), tConf.TimeOut);
	CHECK(RetFun == 0, LBL_CANCEL);

	DisplayHeader(_ON_);
	UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PLEASE_WAIT));

	// switch sim
	gprs_get_sim_slot(&CurrentSim);
	gprs_stop();
	if(CurrentSim == 0)
		gprs_select_sim_slot(1);
	else
		gprs_select_sim_slot(0);

	LL_GPRS_Start(NULL, NULL);

	// resete terminal to apply change
	#if(TETRA_HARDWARE == 0)
	exit(0);
	#else
	Telium_Exit(0);
	#endif//TETRA_HARDWARE

	return RET_OK;

LBL_NO_DUAL_SIM:
	UI_ShowMessage(NULL, "NO ES TERMINAL DUAL SIM", NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_NOK;

LBL_CANCEL:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	GprsInitSIMs
 * Description:		Init SIM cards
 * Author:			mamata
 * Parameters:
 *  none
 * Return:
 *  none
 * Notes:
 */
void GprsInitSIMs(void)
{
/*
	int RetFun;
	Telium_File_t *hGPRS;
	T_DGPRS_GET_INFORMATION SimInfo;
	LL_GPRS_Start(NULL, "broadban.tigo.gt");
	hGPRS = Telium_Stdperif("DGPRS", NULL);
	do
	{
		Telium_Ttestall(0, 500);
		RetFun = Telium_Fioctl(DGPRS_FIOCTL_GET_INFORMATION, &SimInfo, hGPRS);
	}while(SimInfo.network_connection != DGPRS_GSM_NETWORK_NORMAL_CONNECT);

	GprsSwitchSIM();

	do
	{
		Telium_Ttestall(0, 500);
		RetFun = Telium_Fioctl(DGPRS_FIOCTL_GET_INFORMATION, &SimInfo, hGPRS);
	}while(SimInfo.network_connection != DGPRS_GSM_NETWORK_NORMAL_CONNECT);
*/
}
//- DUALSIM

//+ INGESTATE01 @jbotero 13/09/2015
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IS_COMM_TYPE
 * Description:		Post function of STEP_IS_COMM_TYPE
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IS_COMM_TYPE(char *Result)
{
	int RetFun = 0;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DIAL), LLC_DIAL);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ETH), LLC_IP);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_GPRS),LLC_GPRS);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI), LLC_WIFI);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_PCL), LLC_PCL);
    RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_COMM_TYPE), 30, tConf.iS_CommType);
	CHECK(RetFun > 0, LBL_CANCEL);

	*Result = (uint8)RetFun;

	if(RetFun == LLC_PCL)
	{
			#if(TETRA_HARDWARE == 1)
			#else
			socks_param_s stSocks;
			memset(&stSocks, 0x00, sizeof(stSocks));
			stSocks.proxy_type = 5;
			stSocks.proxy_port = 1080;
			stSocks.proxy_address = Telium_inet_addr("1.1.1.1");
			HWCNF_SetProxyParameter(&stSocks);
			#endif //TETRA_HARDWARE
	}
	else{
			#if(TETRA_HARDWARE == 1)
			#else
			socks_param_s stSocks;
			memset(&stSocks, 0x00, sizeof(stSocks));
			stSocks.proxy_type = 0;
			HWCNF_SetProxyParameter(&stSocks);
			#endif //TETRA_HARDWARE
	}
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IS_TERMINAL
 * Description:		Pre function of STEP_IS_TERMINAL
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_IS_TERMINAL(void)
{
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_EMPARAM), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TERMINALID), UI_ALIGN_CENTER);

	memcpy(Step_Buffer, tConf.iS_ID, LZ_LID);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_INPUT_IS_IP
 * Description:		Pre function of INPUT_IS_IP
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_INPUT_IS_IP(void)
{
	if(tConf.iS_CommType == LLC_DIAL)
		return FALSE;

	memcpy(Step_Buffer, tConf.iS_Host, LZ_HOST);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_IP), UI_ALIGN_CENTER);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IS_PORT
 * Description:		Pre function of STEP_IS_PORT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IS_PORT(void)
{
	if(tConf.iS_CommType == LLC_DIAL)
		return FALSE;

	memcpy(Step_Buffer, tConf.iS_Port, LZ_PORT);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_PORT), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IS_PHONE
 * Description:		Pre function of STEP_IS_PHONE
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IS_PHONE(void)
{
	if(tConf.iS_CommType != LLC_DIAL)
		return FALSE;

	memcpy(Step_Buffer, tConf.iS_Phone, LZ_PHONE);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_PRIMARY_PHONE), UI_ALIGN_CENTER);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IS_PPPS_USER
 * Description:		Pre function of STEP_IS_PPPS_USER
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IS_PPPS_USER(void)
{
	if(tConf.iS_CommType != LLC_DIAL)
		return FALSE;

	// set initial value if is present
	if(tConf.iS_PPP_User[0] != 0)
	{
		memcpy(Step_Buffer, tConf.iS_PPP_User, LZ_LOGIN);
	}

	UI_ClearLine(UI_LINE4);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_PPP_USR), UI_ALIGN_CENTER);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IS_PPPS_PASS
 * Description:		Pre function of STEP_IS_PPPS_PASS
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */

bool Pre_STEP_IS_PPPS_PASS(void)
{
	if(tConf.iS_CommType != LLC_DIAL)
		return FALSE;

	// set initial value if is present
	if(tConf.iS_PPP_Pass[0] != 0)
	{
		memcpy(Step_Buffer, tConf.iS_PPP_Pass, LZ_LOGIN);
	}

	UI_ClearLine(UI_LINE4);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_PPP_PASS), UI_ALIGN_CENTER);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IS_SSL
 * Description:		Post function of STEP_IS_SSL
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IS_SSL(char *Result)
{
	int RetFun = 0;
	int SelectedValue;

	if(tConf.iS_CommType == LLC_DIAL)
		return STM_RET_NEXT;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ENABLE), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DISABLE), 2);

	if(tConf.iS_SSL)
		SelectedValue = 1;
	else
		SelectedValue = 2;

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_SSL), 30, SelectedValue);
	CHECK(RetFun > 0, LBL_CANCEL);

	if(RetFun == 1)
		*Result = (bool)TRUE;
	else
		*Result = (bool)FALSE;

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_IS_SSL_PROFILE
 * Description:		Pre function of STEP_IS_SSL_PROFILE
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_IS_SSL_PROFILE(void)
{
	if(tConf.iS_CommType == LLC_DIAL || !tConf.iS_SSL)
		return FALSE;

	// set initial value if is present
	if(tConf.iS_SSL_Profile[0] != 0)
	{
		memcpy(Step_Buffer, tConf.iS_SSL_Profile, LZ_SSLPROFILE);
	}
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);
	UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_SSL_PROFILE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_SSL_PROFILE_L2), UI_ALIGN_CENTER);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_IS_SSL_VERSION
 * Description:		Post function of STEP_IS_SSL_VERSION
* Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_IS_SSL_VERSION(char *Result)
{
	if(tConf.iS_CommType == LLC_DIAL || !tConf.iS_SSL)
			return STM_RET_NEXT;
	int RetFun = 0;
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SSL_TLSv1_2), TLSv1_2);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SSL_TLSv1_1), TLSv1_1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SSL_TLSv1), TLSv1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SSL_SSLv3), SSLv3);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SSL_SSLv2), SSLv2);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SSL_SSLv23), SSLv23);
    RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_COMM_TYPE), 30, tConf.iS_SSLVersion);
	CHECK(RetFun > 0, LBL_CANCEL);

	*Result = (uint8)RetFun;

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SAVE_INGESTATE
 * Description:		Post function of STEP_SAVE_INGESTATE
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_SAVE_INGESTATE(char *Result)
{
	int RetFun = 0;
	ulong QuestionResult;

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		RetFun=GenerateAndSave();
		CHECK(RetFun == TM_RET_OK, LBL_ERROR_GS);
		return STM_RET_NEXT;
	}

	return STEP_IS_TERMINAL;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_ERROR_GS:
			UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
					mess_getReturnedMessage(MSG_SSL_ERROR_CONF),NULL,
					GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			return RET_NOK;
}

//- INGESTATE01

/* ------------------------------------------------------------------------------
 * Function Name:	Post_STEP_CONFIG_COMM
 * Description:		Post function for STEP_CONFIG_COMM
 * Parameters:
 *  - char *Result - where to place step result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_CONFIG_COMM(char *Result)
{
	int RetFun = 0;
	object_descriptor_t objDescriptor;
	int PCLAppType = 0xB3BA;

	UI_MenuReset();

	if(IsRadioETHERNET())
		UI_MenuAddItem(mess_getReturnedMessage(MSG_ETH), LLC_IP);

	if(IsRadioWifi())
		UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI), LLC_WIFI);
	memset(&objDescriptor, 0x00, sizeof(objDescriptor));
	RetFun = ObjectGetDescriptor(OBJECT_TYPE_APPLI, PCLAppType, &objDescriptor);
	if(RetFun == 0 && tTerminal.modePCL > 0)
		UI_MenuAddItem(mess_getReturnedMessage(MSG_PCL), LLC_PCL);
    RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_COMM_TYPE), 30,0);
	CHECK(RetFun >= 0, LBL_CANCEL);

	switch(RetFun)
	{
		case LLC_IP:
#if(TETRA_HARDWARE==1)
		case LLC_WIFI:
#endif
			return STEP_ETH_LOAD_CONFIG;
		case LLC_PCL:
			return STEP_PCL_MODE;
		#if(TETRA_HARDWARE == 1)
		#else
		case LLC_WIFI:
			return STEP_WIFI_SSID;
		#endif//TETRA_HARDWARE
	}

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LOCAL_MODE
 * Description:		Pre function for STEP_LOCAL_MODE
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module
 *  - FALSE - Terminal hasn't
 * Notes:
 */
bool Pre_STEP_LOCAL_MODE(void)
{
	if(!IsRadioETHERNET())
		return FALSE;

	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LOCAL_MODE
 * Description:		Post function of STEP_LOCAL_MODE
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_LOCAL_MODE(char *Result)
{
	int RetFun = 0;
	uint32 EthMode = 1;

	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &EthMode, NULL);
	if(RetFun == RET_OK)
	{
		if(EthMode == __DFL_ETH_BOOT_PROTOCOL_NONE)
			EthMode = 2;
		else
			EthMode = 1;
	}

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ETH_MODE_DHCP), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ETH_MODE_MANUAL), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_ETH_MODE_TL), 30, EthMode);
	CHECK(RetFun > 0, LBL_CANCEL);

	RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_MODE, &RetFun, sizeof(RetFun));
	CHECK(RetFun == RET_OK, LBL_ERROR);

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;

LBL_ERROR:
	return STM_RET_ERROR;

}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LOCAL_IP
 * Description:		Pre function for STEP_LOCAL_IP
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module, or Manual IP
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_LOCAL_IP(void)
{
	uint32 mode = 0;
	int RetFun;

//	if(!IsRadioETHERNET())
//		return FALSE;

	// verify if terminal IP is Manual
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &mode, NULL);
	if(RetFun == RET_OK)
	{
		//is DHCP ?
		if( mode == 1 )
			return FALSE;
	}

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_LOCAL_IP), UI_ALIGN_CENTER);

	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_IP, Step_Buffer, NULL);

	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LOCAL_IP
 * Description:		Post function of STEP_LOCAL_IP
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_LOCAL_IP(char *Result)
{
	Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_IP, Step_Buffer, strlen(Step_Buffer));

	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LOCAL_MASK
 * Description:		Pre function for STEP_LOCAL_MASK
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module, or Manual IP
 *  - FALSE - Terminal doesn't have Ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_LOCAL_MASK(void)
{
	uint32 mode;
	int RetFun;

	// verify if terminal has ethernet
	if(!IsRadioETHERNET())
		return FALSE;

	// verify if terminal IP is Manual
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &mode, NULL);

	if(RetFun == RET_OK)
	{
		//is DHCP ?
		if( mode == 1 )
			return FALSE;
	}

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_LOCAL_MASK), UI_ALIGN_CENTER);

	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );

	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MASK, Step_Buffer, NULL);

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LOCAL_MASK
 * Description:		Post function of STEP_LOCAL_MASK
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_LOCAL_MASK(char *Result)
{
	Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_MASK, Step_Buffer, strlen(Step_Buffer));

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LOCAL_GATEWAY
 * Description:		Pre function for STEP_LOCAL_GATEWAY
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module, or Manual IP
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_LOCAL_GATEWAY(void)
{
	uint32 mode;
	int RetFun;

	// verify if terminal has ethernet
	if(!IsRadioETHERNET())
		return FALSE;

	// verify if terminal IP is Manual
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &mode, NULL);

	if(RetFun == RET_OK)
	{
		//is DHCP ?
		if( mode == 1 )
			return FALSE;
	}

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_LOCAL_GATE), UI_ALIGN_CENTER);

	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );

	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_GATE, Step_Buffer, NULL);

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LOCAL_GATEWAY
 * Description:		Post function of STEP_LOCAL_GATEWAY
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_LOCAL_GATEWAY(char *Result)
{
	Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_GATE, Step_Buffer, strlen(Step_Buffer));

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LOCAL_DNS1
 * Description:		Pre function for STEP_LOCAL_DNS1
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module, or Manual IP
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_LOCAL_DNS1(void)
{
	uint32 mode;
	int RetFun;

	// verify if terminal has ethernet
	if(!IsRadioETHERNET())
		return FALSE;

	// verify if terminal IP is Manual
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &mode, NULL);

	if(RetFun == RET_OK)
	{
		//is DHCP ?
		if( mode == 1 )
			return FALSE;
	}

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_LOCAL_DNS1), UI_ALIGN_CENTER);

	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );

	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_DNS1, Step_Buffer, NULL);

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LOCAL_DNS1
 * Description:		Post function of STEP_LOCAL_DNS1
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_LOCAL_DNS1(char *Result)
{
	Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_DNS1, Step_Buffer, strlen(Step_Buffer));

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LOCAL_DNS2
 * Description:		Pre function for STEP_LOCAL_DNS2
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module, or Manual IP
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_LOCAL_DNS2(void)
{
	uint32 mode;
	int RetFun;

	// verify if terminal has ethernet
	if(!IsRadioETHERNET())
		return FALSE;

	// verify if terminal IP is Manual
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &mode, NULL);

	if(RetFun == RET_OK)
	{
		//is DHCP ?
		if( mode == 1 )
			return FALSE;
	}

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_LOCAL_DNS2), UI_ALIGN_CENTER);

	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );

	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_DNS2, Step_Buffer, NULL);

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LOCAL_DNS2
 * Description:		Post function of STEP_LOCAL_DNS2
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_LOCAL_DNS2(char *Result)
{
	Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_DNS2, Step_Buffer, strlen(Step_Buffer));

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_ETH_LOAD_CONFIG
 * Description:		Pre function for STEP_ETH_LOAD_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Wifi module
 *  - FALSE - Terminal hasn't Wifi or IP is DHCP
 * Notes:
 */
bool Pre_STEP_ETH_LOAD_CONFIG(void)
{
	// verify if terminal has ethernet
	if(!IsRadioETHERNET())
		return FALSE;

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_ETH_LOAD_CONFIG
 * Description:		Post function of STEP_ETH_LOAD_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_ETH_LOAD_CONFIG(char *Result)
{
	int nError = __DFL_OK;
	int RetFun;

	DFLCONF_HANDLE hConf;
	uint32 uiLocalAddr = 0;
	struct Telium_in_addr inAddr;

	int8 strLocalMAC[7];
	int8 strLocalIp[LZ_IP_HOST];
	int8 strLocalMask[LZ_IP_HOST];
	int8 strLocalGate[LZ_IP_HOST];
	int8 strLocalDNS1[LZ_IP_HOST];
	int8 strLocalDNS2[LZ_IP_HOST];

	hConf = __LoadDefaultOptions( &nError );

	if( ( hConf != NULL ) && ( nError == __DFL_OK ) )
	{
		// DHCP Option
		__GetDefaultOption( hConf, __DFL_ETH_BOOT_PROTOCOL, &uiLocalAddr );

		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_MODE, &uiLocalAddr, LTAG_UINT32);

		//Local MAC Address
		__GetDefaultOption(hConf, __DFL_ETH_MAC_ADDRESS, strLocalMAC );
		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_MAC, strLocalMAC, 6);

		//Local IP
		EthernetGetOption(ETH_IFO_ADDR, &uiLocalAddr);
		inAddr.s_addr = uiLocalAddr;
		strcpy( strLocalIp, Telium_inet_ntoa(inAddr) );
		if(strcmp(strLocalIp, "0.0.0.0") == 0 )
			memset( strLocalIp, 0x00, sizeof(strLocalIp) );

		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_IP, strLocalIp, strlen(strLocalIp));

		//Local Mask
		EthernetGetOption(ETH_IFO_NETMASK, &uiLocalAddr);
		inAddr.s_addr = uiLocalAddr;
		strcpy( strLocalMask, Telium_inet_ntoa(inAddr) );
		if(strcmp(strLocalMask, "0.0.0.0") == 0 )
			memset( strLocalMask, 0x00, sizeof(strLocalMask) );

		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_MASK, strLocalMask, strlen(strLocalMask));

		//Local Gateway
		EthernetGetOption(ETH_IFO_GATEWAY, &uiLocalAddr);
		inAddr.s_addr = uiLocalAddr;
		strcpy( strLocalGate, Telium_inet_ntoa(inAddr) );
		if(strcmp(strLocalGate, "0.0.0.0") == 0 )
			memset( strLocalGate, 0x00, sizeof(strLocalGate) );

		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_GATE, strLocalGate, strlen(strLocalGate));

		//Local DNS1
		EthernetGetOption(ETH_IFO_DNS1, &uiLocalAddr);
		inAddr.s_addr = uiLocalAddr;
		strcpy( strLocalDNS1, Telium_inet_ntoa(inAddr) );
		if(strcmp(strLocalDNS1, "0.0.0.0") == 0 )
			memset( strLocalDNS1, 0x00, sizeof(strLocalGate) );

		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_DNS1, strLocalDNS1, strlen(strLocalDNS1));

		//Local DNS2
		EthernetGetOption(ETH_IFO_DNS2, &uiLocalAddr);
		inAddr.s_addr = uiLocalAddr;
		strcpy( strLocalDNS2, Telium_inet_ntoa(inAddr) );
		if(strcmp(strLocalDNS2, "0.0.0.0") == 0 )
			memset( strLocalDNS2, 0x00, sizeof(strLocalDNS2) );

		RetFun = Tlv_SetTagValue(Tree_Conf, TAG_LOCAL_DNS2, strLocalDNS2, strlen(strLocalDNS2));
	}

	return STM_RET_NEXT;
}
/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_ETH_APPLY_CONFIG
 * Description:		Pre function for STEP_ETH_APPLY_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_ETH_APPLY_CONFIG(void)
{
	// verify if terminal has ethernet
	if(!IsRadioETHERNET())
		return FALSE;

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_ETH_APPLY_CONFIG
 * Description:		Post function of STEP_ETH_APPLY_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_ETH_APPLY_CONFIG(char *Result)
{
	DFLCONF_HANDLE hConf;
	uint32 uiLocalAddr = 0;
	int nError = __DFL_OK;
	int EthMode;
	uint32 mode;
	int RetFun;

	int8 strLocalIp[LZ_IP_HOST] 	= {0x00, };
	int8 strLocalMask[LZ_IP_HOST]	= {0x00, };
	int8 strLocalGate[LZ_IP_HOST]	= {0x00, };
	int8 strLocalDNS1[LZ_IP_HOST]	= {0x00, };
	int8 strLocalDNS2[LZ_IP_HOST]	= {0x00, };

	// verify if terminal IP is Manual or DHCP
	RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MODE, &mode, NULL);

	if(RetFun == RET_OK)
	{
		//is DHCP ?
		if( mode == 1 )
			EthMode = __DFL_ETH_BOOT_PROTOCOL_DHCP;
		else
			EthMode = __DFL_ETH_BOOT_PROTOCOL_NONE;
	}

	// Configure Local Ethernet
	hConf = __LoadDefaultOptions( &nError );

	if( ( hConf != NULL ) && ( nError == __DFL_OK ) )
	{
		// DHCP Option
		__SetDefaultOption( hConf, __DFL_ETH_BOOT_PROTOCOL, &EthMode );

		if( EthMode == __DFL_ETH_BOOT_PROTOCOL_NONE )
		{
			//Local IP
			RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_IP, strLocalIp, NULL);
			if( RetFun == RET_OK)
			{
				uiLocalAddr = Telium_inet_addr(strLocalIp);
				__SetDefaultOption(hConf, __DFL_ETH_ADDR, &uiLocalAddr);
			}

			//Local Mask
			RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_MASK, strLocalMask, NULL);
			if( RetFun == RET_OK)
			{
				uiLocalAddr = Telium_inet_addr(strLocalMask);
				__SetDefaultOption(hConf, __DFL_ETH_NETMASK, &uiLocalAddr);
			}

			//Local Gateway
			RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_GATE, strLocalGate, NULL);
			if( RetFun == RET_OK)
			{
				uiLocalAddr = Telium_inet_addr(strLocalGate);
				__SetDefaultOption(hConf, __DFL_ETH_GATEWAY, &uiLocalAddr);
			}

			//DNS1
			RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_DNS1, strLocalDNS1, NULL);
			if( RetFun == RET_OK)
			{
				uiLocalAddr = Telium_inet_addr(strLocalDNS1);
				__SetDefaultOption(hConf, __DFL_ETH_DNS1, &uiLocalAddr);
			}

			//DNS2
			RetFun = Tlv_GetTagValue(Tree_Conf, TAG_LOCAL_DNS2, strLocalDNS2, NULL);
			if( RetFun == RET_OK)
			{
				uiLocalAddr = Telium_inet_addr(strLocalDNS2);
				__SetDefaultOption(hConf, __DFL_ETH_DNS2, &uiLocalAddr);
			}

		}

		UI_ClearAllLines();
		UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_ETH_APPLY_CONF_L1), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_ETH_APPLY_CONF_L2), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE5, mess_getReturnedMessage(MSG_ETH_APPLY_CONF_L3), UI_ALIGN_CENTER);
		UI_ShowInfoScreen(NULL);

		//Save the configuration. The terminal reboots automatically.
		__SaveDefaultOptions( hConf );

	}

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_PCL_MODE
 * Description:		Pre function for STEP_PCL_MODE
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has PCL enabled
 *  - FALSE - Terminal hasn't PCL enabled
 * Notes:
 */
bool Pre_STEP_PCL_MODE(void)
{
	// verify if terminal has PCL enabled
	if(!IsPCLDoTransaction())
		return FALSE;
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PCL_MODE
 * Description:		Post function of STEP_PCL_MODE
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_PCL_MODE(char *Result)
{
	int RetFun;
	unsigned char Interface = 0;

	UI_MenuReset();

	UI_MenuAddItem("USB", INTERFACE_USB);

	if(IsCOM0() && !IsPortable())
		UI_MenuAddItem("COM0", INTERFACE_COM0);

	if(IsBT())
		UI_MenuAddItem("BLUETOOTH", INTERFACE_BLUETOOTH);

	if(IsRadioETHERNET())
		UI_MenuAddItem("ETHERNET", INTERFACE_TCP);

	PCL_get_interface(&Interface);

	UI_SetMenuShowOneOption(FALSE);
	RetFun = UI_MenuRun("INTERFAZ PCL", tConf.TimeOut, Interface);
	UI_SetMenuShowOneOption(TRUE);

	CHECK(RetFun >= 0, LBL_ERROR);

	Interface = RetFun;
	PCL_set_interface(Interface, TRUE);

	switch(RetFun)
	{
		case INTERFACE_BLUETOOTH:
			#if(TETRA_HARDWARE == 1)
			#else
			HWCNF_Shortcut_ConfigureBluetoothSettings();
			#endif //TETRA_HARDWARE
			break;
		default:
			break;
	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PCL_APPLY_CONFIG
 * Description:		Post function of STEP_PCL_APPLY_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_PCL_APPLY_CONFIG(char *Result)
{
	#if(TETRA_HARDWARE == 1)
	#else
		socks_param_s stSocks;
		memset(&stSocks, 0x00, sizeof(stSocks));
		stSocks.proxy_type = 5;
		stSocks.proxy_port = 1080;
		stSocks.proxy_address = Telium_inet_addr("1.1.1.1");
		HWCNF_SetProxyParameter(&stSocks);
	#endif //TETRA_HARDWARE
	return STM_RET_NEXT;
}


//+ WIFI_TBK01 @jbotero 22/06/2015

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_WIFI_SSID
 * Description:		Pre function for STEP_WIFI_SSID
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Wifi Module
 *  - FALSE - Terminal hasn't
 * Notes:
 */
bool Pre_STEP_WIFI_SSID(void)
{
	if (axHost.CommunicationType != LLC_WIFI) {
		return FALSE;
	}

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_WIFI_SCANNING_TL), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_WIFI_SCANNING_L1), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_WIFI_SCANNING_L2), UI_ALIGN_CENTER);
	UI_ShowInfoScreen(NULL);

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_WIFI_SSID
 * Description:		Post function of STEP_WIFI_SSID
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_WIFI_SSID(char *Result)
{
	dll_wifi_access_point_list_t CurrProfiles;
	int32 RetFun;

	dll_wifi_scan_result_t list[DLL_WIFI_MAX_ACCESS_POINT];
	char bssid_str[DLL_WIFI_MAX_ACCESS_POINT][18];
	int i, count,x;


	if( !Wifi_IsPowerOn() )
		Wifi_PowerOn();

	Wifi_ProfileGetList( &CurrProfiles );

	//Remove all current profiles
	for( i = 0; i < CurrProfiles.access_point_count; i++ )
		Wifi_ProfileRemoveBySsid( CurrProfiles.access_point_info[i].essid );

	memset( &stProfileWifi, 0x00, sizeof( stProfileWifi ) );

	x = Wifi_AccessPointScan();

	if( 0 == x )
	{
		if(Wait_Key(true, 15) == T_ANN)
			return STM_RET_CANCEL;

		//Get Data for each Network
		count = Wifi_GetScanResults( list, DLL_WIFI_MAX_ACCESS_POINT );

		UI_MenuReset();

		for( i = 0; i< count; i++ )
		{
			// non-string => hidden ESSID, display BSSID instead
			if(0 == list[i].ssid[0])
			{
				sprintf(bssid_str[i], MAC_STR, MAC_2_STR(list[i].bssid));
				UI_MenuAddItem(bssid_str[i], i);
			}
			else
				UI_MenuAddItem((char*)list[i].ssid, i);
		}

		RetFun = UI_MenuRun( mess_getReturnedMessage(MSG_WIFI_SCANNING_TL), 1000, 0);

		if( RetFun < 0 )
			return STM_RET_CANCEL;
		else
			memcpy( &stProfileWifi.info, list + RetFun, sizeof( stProfileWifi.info ) );

	}else{
		Wifi_ProfileSetActiveBySsid(NULL);
	}

	//HOME SECURITY
	stProfileWifi.SecurityType = SEC_HOME;

	//MAIN PRIORITY
	stProfileWifi.priority = 1;

	return STM_RET_NEXT;

}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_WIFI_SECURITY
 * Description:		Pre function for STEP_WIFI_SECURITY
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Wifi Module
 *  - FALSE - Terminal hasn't
 * Notes:
 */
bool Pre_STEP_WIFI_SECURITY(void)
{

	if (axHost.CommunicationType != LLC_WIFI) {
	return FALSE;
}
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_WIFI_SECURITY
 * Description:		Post function of STEP_WIFI_SECURITY
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_WIFI_SECURITY(char *Result)
{
	int32 RetFun;

	UI_MenuReset();

	UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI_SEC_NONE), 0);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI_SEC_WEP_1), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI_SEC_WEP_2), 2);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI_SEC_WPA), 3);

	RetFun = UI_MenuRun( mess_getReturnedMessage(MSG_WIFI_TL_SEC_TYPE), 1000, 0);

	if( RetFun < 0 )
		return STM_RET_CANCEL;

	stProfileWifi.CipherType = RetFun;

	return STM_RET_NEXT;

}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_WIFI_KEY_PASS
 * Description:		Pre function for STEP_WIFI_KEY_PASS
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Wifi Module
 *  - FALSE - Terminal hasn't
 * Notes:
 */
bool Pre_STEP_WIFI_KEY_PASS(void)
{

	if (axHost.CommunicationType != LLC_WIFI) {
		return FALSE;
	}
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_WIFI_KEY_PASS
 * Description:		Post function of STEP_WIFI_KEY_PASS
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_WIFI_KEY_PASS(char *Result)
{
	int iRet;
	char key[ 50 ] = { 0x00, };

	Telium_File_t *hKeyboard = NULL;
	bool IsCancel 	= false;
	bool IsAgain	= false;

	T_GL_HGRAPHIC_LIB hGoal = GL_GraphicLib_Create();

	hKeyboard = Telium_Fopen("KEYBOARD", "r*");

	stProfileWifi.AuthType = DLL_WIFI_AUTH_MODE_SHARED;

	switch ( stProfileWifi.CipherType )
	{
		case DLL_WIFI_ALGO_NONE:
			stProfileWifi.AuthType = DLL_WIFI_AUTH_MODE_OPEN;
			break;

		case DLL_WIFI_ALGO_WEP64:

			iRet = GL_Dialog_VirtualKeyboard(hGoal,
					mess_getReturnedMessage(MSG_WIFI_KEY_WEP64),
					NULL,
					"/x/x/x/x/x/x/x/x/x/x",
					key, sizeof(key), GL_TIME_MINUTE );

			if( ( iRet != GL_KEY_CANCEL) && ( iRet != GL_RESULT_INACTIVITY) )
			{
				if( strlen( key ) < 10 )
					IsAgain = true;

				Ascii_to_Hex(stProfileWifi.Key, (uint8*) key, 10);
			}
			else
				IsCancel = true;

			break;

		case DLL_WIFI_ALGO_WEP128:

			iRet = GL_Dialog_VirtualKeyboard(hGoal,
					mess_getReturnedMessage(MSG_WIFI_KEY_WEP128),
					NULL,
					"/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x/x",
					key, sizeof(key), GL_TIME_MINUTE );

			if( ( iRet != GL_KEY_CANCEL) && ( iRet != GL_RESULT_INACTIVITY) )
			{
				if( strlen( key ) < 26 )
					IsAgain = true;

				Ascii_to_Hex(stProfileWifi.Key, (uint8*)key, 26);
			}
			else
				IsCancel = true;

			break;

		case DLL_WIFI_ALGO_WPA_PSK:
		case DLL_WIFI_ALGO_WPA2_PSK:
			iRet = GL_Dialog_VirtualKeyboard(hGoal,
					mess_getReturnedMessage(MSG_WIFI_KEY_WPA),
					NULL,
					NULL, key, sizeof(key), GL_TIME_MINUTE );

			if( ( iRet == GL_KEY_CANCEL) || ( iRet == GL_RESULT_INACTIVITY) )
				IsCancel = true;
			else
			{
				if( strlen( key ) < 8 )
					IsAgain = true;
				else
					strcpy( (char*)stProfileWifi.Key, key );
			}

			break;

		default:
			break;
	}

	GL_GraphicLib_Destroy(hGoal);

	Telium_Fclose(hKeyboard);

	if( IsAgain)
		return STM_RET_AGAIN;

	if( IsCancel )
		return STEP_WIFI_SECURITY;

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_WIFI_ADDPROFILE
 * Description:		Pre function for STEP_WIFI_ADDPROFILE
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Wifi Module
 *  - FALSE - Terminal hasn't
 * Notes:
 */
bool Pre_STEP_WIFI_ADDPROFILE(void)
{	if (axHost.CommunicationType != LLC_WIFI) {
	return FALSE;
}
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_WIFI_ADDPROFILE
 * Description:		Post function of STEP_WIFI_ADDPROFILE
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_WIFI_ADDPROFILE(char *Result)
{
	if( stProfileWifi.SecurityType == SEC_HOME )
	{
		//Add a new profile
		Wifi_ProfileAddHome(
				(char*)stProfileWifi.info.ssid,
				stProfileWifi.info.bssid,
				stProfileWifi.CipherType,
				stProfileWifi.Key,
				stProfileWifi.info.ssid[0],
				stProfileWifi.priority);
	}

	Wifi_ProfileSetActiveBySsid( stProfileWifi.info.ssid );

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_WIFI_LOAD_CONFIG
 * Description:		Pre function for STEP_WIFI_LOAD_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_WIFI_LOAD_CONFIG(void)
{	if (axHost.CommunicationType != LLC_WIFI) {
	return FALSE;
}
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_WIFI_LOAD_CONFIG
 * Description:		Post function of STEP_WIFI_LOAD_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_WIFI_LOAD_CONFIG(char *Result)
{
	TLV_TREE_NODE node;
	uint32 uiLocalAddr = 0;
	int32 EthMode = 0;
	unsigned char Ipv4[5];

	int8 strLocalIp[LZ_IP_HOST];
	int8 strLocalMask[LZ_IP_HOST];
	int8 strLocalGate[LZ_IP_HOST];

	// DHCP Option
	uiLocalAddr = Wifi_GetBootproto() == DLL_WIFI_BOOT_PROTO_DHCP ?
						__DFL_ETH_BOOT_PROTOCOL_DHCP :__DFL_ETH_BOOT_PROTOCOL_NONE;

	node = TlvTree_Find(Tree_Conf, TAG_LOCAL_MODE, 0);

	if(__DFL_ETH_BOOT_PROTOCOL_NONE == uiLocalAddr)
	{
		EthMode = 1;
		if(NULL==node)
			TlvTree_AddChild(Tree_Conf, TAG_LOCAL_MODE, &EthMode, LTAG_INT32);
		else
			TlvTree_SetData(node, &EthMode, LTAG_INT32);
	}
	else
		TlvTree_Release(node);


	//Local IP
	Wifi_GetIpAddress( Ipv4 );
	sprintf( strLocalIp, "%d.%d.%d.%d",
			Ipv4[0] == 0 ? 1:Ipv4[0], Ipv4[1], Ipv4[2], Ipv4[3] );

	node = TlvTree_Find(Tree_Conf, TAG_LOCAL_IP, 0);
	if(NULL==node)
		TlvTree_AddChild(Tree_Conf, TAG_LOCAL_IP, strLocalIp, strlen(strLocalIp));
	else
		TlvTree_SetData(node, strLocalIp, strlen(strLocalIp));

	//Local Mask
	Wifi_GetNetmask( Ipv4 );
	sprintf( strLocalMask, "%d.%d.%d.%d",
			Ipv4[0] == 0 ? 1:Ipv4[0], Ipv4[1], Ipv4[2], Ipv4[3] );

	node = TlvTree_Find(Tree_Conf, TAG_LOCAL_MASK, 0);
	if(NULL==node)
		TlvTree_AddChild(Tree_Conf, TAG_LOCAL_MASK, strLocalMask, strlen(strLocalMask));
	else
		TlvTree_SetData(node, strLocalMask, strlen(strLocalMask));

	//Local Gateway
	Wifi_GetGateway( Ipv4 );

	//Solve problem when Gateway is 0.0.0.0
	if( Ipv4[0] == 0 &&
		Ipv4[1] == 0 &&
		Ipv4[2] == 0 &&
		Ipv4[3] == 0 )
	{
		Wifi_GetIpAddress( Ipv4 );

		sprintf( strLocalGate, "%d.%d.%d.%d",
				Ipv4[0] == 0 ? 1:Ipv4[0], Ipv4[1], Ipv4[2], 1 );
	}
	else
	{
		sprintf( strLocalGate, "%d.%d.%d.%d",
				Ipv4[0] == 0 ? 1:Ipv4[0], Ipv4[1], Ipv4[2], Ipv4[3] );
	}

	node = TlvTree_Find(Tree_Conf, TAG_LOCAL_GATE, 0);
	if(NULL==node)
		TlvTree_AddChild(Tree_Conf, TAG_LOCAL_GATE, strLocalGate, strlen(strLocalGate));
	else
		TlvTree_SetData(node, strLocalGate, strlen(strLocalGate));

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_WIFI_APPLY_CONFIG
 * Description:		Pre function for STEP_WIFI_APPLY_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - Terminal has Ethernet module
 *  - FALSE - Terminal hasn't ethernet or IP is DHCP
 * Notes:
 */
bool Pre_STEP_WIFI_APPLY_CONFIG(void)
{
	if (axHost.CommunicationType != LLC_WIFI)
	return FALSE;

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_WIFI_APPLY_CONFIG
 * Description:		Post function of STEP_WIFI_APPLY_CONFIG
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT - step Ok
 *  - STM_RET_AGAIN - repeat step
 *  - STM_RET_CANCEL - cancel or timeout
 * Notes:
 */
int Post_STEP_WIFI_APPLY_CONFIG(char *Result)
{
	TLV_TREE_NODE node;
	char ip_bArray[4];
	int EthMode;
	int8 EthApplyMode = 0;
	uint32 mode;

	int8 strLocalIp[LZ_IP_HOST] 	= {0x00, };
	int8 strLocalMask[LZ_IP_HOST]	= {0x00, };
	int8 strLocalGate[LZ_IP_HOST]	= {0x00, };

	// verify if terminal IP is Manual or DHCP
	node = TlvTree_Find(Tree_Conf, TAG_LOCAL_MODE, 0);
	if(NULL != node)
	{
		memcpy( &mode, TlvTree_GetData(node), LTAG_UINT32 );

		//is DHCP ?
		if( mode == 1 )
			EthMode = DLL_WIFI_BOOT_PROTO_DHCP;
		else
			EthMode = DLL_WIFI_BOOT_PROTO_STATIC;

		// DHCP Option
		Wifi_SetBootproto( EthMode );

		if( EthMode == DLL_WIFI_BOOT_PROTO_STATIC )
		{
			//Local IP
			node = TlvTree_Find(Tree_Conf, TAG_LOCAL_IP, 0);
			memcpy( strLocalIp, TlvTree_GetData(node), TlvTree_GetLength(node));
			ConvIp_AsciToByteArr( strLocalIp, ip_bArray );
			Wifi_SetIpAddress((unsigned char*)ip_bArray);

			//Local Mask
			node = TlvTree_Find(Tree_Conf, TAG_LOCAL_MASK, 0);
			memcpy( strLocalMask, TlvTree_GetData(node), TlvTree_GetLength(node));
			ConvIp_AsciToByteArr( strLocalMask, ip_bArray );
			Wifi_SetNetmask((unsigned char*)ip_bArray);

			//Local Gateway
			node = TlvTree_Find(Tree_Conf, TAG_LOCAL_GATE, 0);
			memcpy( strLocalGate, TlvTree_GetData(node), TlvTree_GetLength(node));
			ConvIp_AsciToByteArr( strLocalGate, ip_bArray );
			Wifi_SetGateway((unsigned char*)ip_bArray);
		}

		UI_ClearAllLines();
		UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_WIFI_CONF_TITLE), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_ETH_APPLY_CONF_L1), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_ETH_APPLY_CONF_L2), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_APPLY_CONF_L3), UI_ALIGN_CENTER);
		UI_ShowInfoScreen(NULL);

		EthApplyMode = 1;
	}

	return STM_RET_NEXT;
}
//jonathan
int MenuTerminal()
{
	int RetFun;
	int MenuRet;
	ulong QuestionResult = 0;
	ulong QuestionResultMerchant = 0;

	do
		{
			UI_MenuReset();

			// build menu
			UI_MenuAddItem(mess_getReturnedMessage(MSG_NOM_BANC), 1);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_MODO), 2);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_MK), 3);
		//	UI_MenuAddItem(mess_getReturnedMessage(MSG_CARGA_KEY), 4);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_PINPAD), 5);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_TIPO_PINPAD), 6);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_IMPRIMIR), 7);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_IMP_DEBITO), 8);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_IMP_CREDITO), 9);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_IMP_D), 10);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_PRE_IMP), 11);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_IMP_COPIA), 12);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_IMP_RECHAZADO), 13);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_IMP_LOGO), 14);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_NRO_COPIA), 15);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_TRANS_PASSW), 16);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_ACEPTA), 17);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_DIAS_PRE), 18);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_PORCEN_AUTO), 19);
		//	UI_MenuAddItem(mess_getReturnedMessage(MSG_COD_MONEDA), 20);
		//	UI_MenuAddItem(mess_getReturnedMessage(MSG_LIT_MONEDA), 21);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_CAMBIO_MONEDA), 3);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_BORRA_REVERSO), 23);
			//UI_MenuAddItem(mess_getReturnedMessage(MSG_CFG_FECHA), 24);
		//	UI_MenuAddItem(mess_getReturnedMessage(MSG_BIN_PRIVADO), 25);
		//	UI_MenuAddItem(mess_getReturnedMessage(MSG_DEBUG_RAFAGA), 26);

			// run menu
			MenuRet = UI_MenuRun(mess_getReturnedMessage(MSG_GENERAL_CONF), 30, 1);
			CHECK(MenuRet > 0, LBL_MENU_END);

			switch(MenuRet)
			{
				case 1:
					RetFun = STM_RunTransaction(TRN_CONFIG_NAMET, CONF_STEP_LIST);
					if(RetFun == STM_RET_CANCEL)
					{
						QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
						QuestionResult = 0;
					}
					break;
				case 2:
					RetFun = STM_RunTransaction(TRN_CONFIG_TAPLICATION, CONF_STEP_LIST);
					if(RetFun == STM_RET_CANCEL)
					{
						QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
						QuestionResult = 0;
					}
					break;
				case 3:
					RetFun = STM_RunTransaction(CONF_MONEDA, CONF_STEP_LIST);
					if(RetFun == STM_RET_CANCEL)
					{
						QuestionResultMerchant = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
						QuestionResultMerchant = 0;
					}
					break;
				case 4:
					break;
				case 5:
					break;
				case 6:
					break;
				case 7:
					//IMPIMIR TICKET O RECIBO
					RetFun = STM_RunTransaction(TRN_CONFIG_IMPRIMET, CONF_STEP_LIST);
					if(RetFun == STM_RET_CANCEL)
					{
						QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
						QuestionResult = 0;
					}
					break;
				case 8:
					RetFun = STM_RunTransaction(TRN_IMPRIME_DEBITO, CONF_STEP_LIST);
					if(RetFun == STM_RET_CANCEL)
					{
					QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
					QuestionResult = 0;
					}
					break;
				case 9:
					RetFun = STM_RunTransaction(TRN_IMPRIME_CREDITO, CONF_STEP_LIST);
					if(RetFun == STM_RET_CANCEL)
					{
					QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
					QuestionResult = 0;
					}
					break;
				case 10:
					break;
				case 11:
					//
					RetFun = STM_RunTransaction(TRN_CONFIG_PRE_IMPRIMET, CONF_STEP_LIST);
					if(RetFun == TRUE)
					{
					QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}
					else
					{
					QuestionResult = 0;
					}
					break;
				case 12:
					break;
				case 13:
					//
					RetFun = STM_RunTransaction(TRN_CONFIG_IMPRIME_RECHAZADO, CONF_STEP_LIST);
					if(RetFun == TRUE){
						QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}else{
						QuestionResult = 0;
					}
					break;
				case 14:
					RetFun = STM_RunTransaction(TRN_CONFIG_IMPRIME_LOGO, CONF_STEP_LIST);
					if(RetFun == TRUE){
						QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}else{
						QuestionResult = 0;
					}
					break;
				case 15:
					RetFun = STM_RunTransaction(TRN_CONFIG_NUMERO_COPIAS, CONF_STEP_LIST);
					if(RetFun == TRUE){
						QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_CONFIG_COPIAS), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
					}else{
						QuestionResult = 0;
					}
					break;
				case 16:
					break;
				case 17:
					break;
				case 18:
					break;
				case 19:
					break;
				case 20:
					break;
				case 21:
					break;
				case 22:
					break;
				case 23:
					break;
				case 24:
					break;
				case 25:
					break;
				case 26:
					break;
				default:
					return RET_NOK;
			}


		}while(MenuRet > 0);

	if(QuestionResult == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
		CHECK(RetFun == TM_RET_OK, LBL_MENU_END);
	}

	if(QuestionResultMerchant == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_MERCHANT, &auxMerch);
		CHECK(RetFun == TM_RET_OK, LBL_MENU_END);
	}
		return RET_OK;

	LBL_MENU_END:
	return STM_RET_CANCEL;
	//	return RET_NOK;


	//- DUALSIM
}
//JONATHAN
int PRE_STEP_TYPE_APLICATION(char *Result){
	int RetFun = 0;
	int Merchant=0;
	ulong QuestionResult;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_COMERCIO), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_RESTAURANTE), 2);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_RENT_CARD), 3);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_CLINICA), 4);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_HOTELES), 5);

	Merchant = UI_MenuRun(mess_getReturnedMessage(MSG_MODO), 30, tTerminal.Comercio);
		CHECK(Merchant > 0, LBL_CANCEL);

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
	if(QuestionResult == GL_KEY_VALID)
	{


		RetFun = TM_FindFirst(TAB_TERMINAL, &tTerminal);
		if (RetFun != TM_RET_OK)
		return FALSE;

		 tTerminal.Comercio = (uint8)Merchant;

		RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
		if (RetFun != TM_RET_OK)
		return FALSE;

		UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
		"MODIFICACION",
		mess_getReturnedMessage(MSG_EXITO),
		GL_ICON_INFORMATION,
		GL_BUTTON_ALL,
		tConf.TimeOut);

		return STM_RET_NEXT;
	}
	LBL_CANCEL:
		return STM_RET_CANCEL;

}
bool Pre_step_imprime (void){
	int iRet;
	int RetFun = 0;
	int x=1;
	ulong QuestionResult;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECEIPT))
	   x=2;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SI), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_IMPRIMIR), 30, x);
		CHECK(RetFun > 0, LBL_CANCEL);

		QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
		if(QuestionResult == GL_KEY_VALID)
		{


			switch (RetFun) {
			case 1:

				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
					return FALSE;

				tTerminal.Flags1 |= TERM_FLAG1_PRINT_RECEIPT;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
					return FALSE;

				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
					"MODIFICACION",
					mess_getReturnedMessage(MSG_EXITO),
					GL_ICON_INFORMATION,
					GL_BUTTON_ALL,
					tConf.TimeOut);
				return TRUE;
				break;
			case 2:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 &= ~(TERM_FLAG1_PRINT_RECEIPT);;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
				"MODIFICACION",
				mess_getReturnedMessage(MSG_EXITO),
				GL_ICON_INFORMATION,
				GL_BUTTON_ALL,
				tConf.TimeOut);

				return TRUE;
				break;
			default:
				return FALSE;
				break;
			}
		}
	LBL_CANCEL:
		return FALSE;

}
bool Pre_step_imprime_debito (void){
	int iRet;
	int RetFun = 0;
	int x= 1;
	ulong QuestionResult;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_DEBITO))
	   x=2;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SI), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_IMPRIMIR), 30, x);
		CHECK(RetFun > 0, LBL_CANCEL);

		QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
				if(QuestionResult == GL_KEY_VALID)
				{


		switch (RetFun) {
			case 1:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 |= TERM_FLAG1_PRINT_DEBITO;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);

			return TRUE;
				break;
			case 2:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 &= ~(TERM_FLAG1_PRINT_DEBITO);;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);
			return TRUE;
			break;
			default:
				return FALSE;
				break;
		}
	}
	LBL_CANCEL:
		return FALSE;

}
bool Pre_step_imprime_credito(void){
	int iRet;
	int RetFun = 0;
	int x=1;
	ulong QuestionResult;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_CREDITO))
	   x=2;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SI), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_IMPRIMIR), 30, x);
		CHECK(RetFun > 0, LBL_CANCEL);

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
	if(QuestionResult == GL_KEY_VALID)
	{


		switch (RetFun) {
			case 1:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 |= TERM_FLAG1_PRINT_CREDITO;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
								"MODIFICACION",
								mess_getReturnedMessage(MSG_EXITO),
								GL_ICON_INFORMATION,
								GL_BUTTON_ALL,
								tConf.TimeOut);

			return TRUE;
				break;
			case 2:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 &= ~(TERM_FLAG1_PRINT_CREDITO);;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
								"MODIFICACION",
								mess_getReturnedMessage(MSG_EXITO),
								GL_ICON_INFORMATION,
								GL_BUTTON_ALL,
								tConf.TimeOut);

			return TRUE;
			break;
			default:
				return FALSE;
				break;
		}
	}
	LBL_CANCEL:
		return FALSE;

}
bool Pre_step_pre_imprime (void){
	int iRet;
	int RetFun = 0;
	int x=1;
	ulong QuestionResult;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRE_PRINT))
	   x=2;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SI), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_IMPRIMIR), 30, x);
		CHECK(RetFun > 0, LBL_CANCEL);

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
	if(QuestionResult == GL_KEY_VALID)
	{


		switch (RetFun) {
			case 1:

				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
					return FALSE;

				tTerminal.Flags1 |= TERM_FLAG1_PRE_PRINT;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
					return FALSE;

				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);

				return STM_RET_NEXT;
				break;
			case 2:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 &= ~(TERM_FLAG1_PRE_PRINT);;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);

				return STM_RET_NEXT;
				break;
			default:
				return FALSE;
				break;
		}
	}
	LBL_CANCEL:
		return FALSE;

}
bool Pre_step_imprime_rechazado(void){
	int iRet;
	int RetFun = 0;
	int x=1;
	ulong QuestionResult;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECHAZADO))
	   x=2;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SI), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_IMPRIMIR), 30, x);
		CHECK(RetFun > 0, LBL_CANCEL);
		QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
			if(QuestionResult == GL_KEY_VALID)
			{


		switch (RetFun) {
			case 1:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 |= TERM_FLAG1_PRINT_RECHAZADO;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);

			return TRUE;
				break;
			case 2:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 &= ~(TERM_FLAG1_PRINT_RECHAZADO);;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);
			return TRUE;
			break;
			default:
				return FALSE;
				break;
		}
			}
	LBL_CANCEL:
		return FALSE;


}
bool Pre_step_imprime_logo(void){
	int iRet;
	int RetFun = 0;
	int x=1;
	ulong QuestionResult;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_LOGO))
	   x=2;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SI), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_IMPRIMIR), 30, x);
		CHECK(RetFun > 0, LBL_CANCEL);

		QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);
			if(QuestionResult == GL_KEY_VALID)
			{


		switch (RetFun) {
			case 1:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 |= TERM_FLAG1_PRINT_LOGO;

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);

			return TRUE;
				break;
			case 2:
				iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;

				tTerminal.Flags1 &= ~(TERM_FLAG1_PRINT_LOGO);

				iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
				if (iRet != TM_RET_OK)
				return FALSE;
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"MODIFICACION",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);
			return TRUE;
			break;
			default:
				return FALSE;
				break;
		}
			}
	LBL_CANCEL:
		return FALSE;


}

bool Pre_NUMERO_COPIAS(void)
{
	if(tTerminal.NCopias == 0)
		return FALSE;

	memcpy(Step_Buffer, tTerminal.NCopias, 2);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONFIG_COPIAS), UI_ALIGN_CENTER);
	return TRUE;
}

int Post_NUMERO_COPIAS(char *Result)
{
	int iRet;

	iRet = TM_FindFirst(TAB_TERMINAL, &tTerminal);
	if (iRet != TM_RET_OK)
	return FALSE;

	memcpy(tTerminal.NCopias,Step_Buffer, 2);

	iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
	if (iRet != TM_RET_OK)
	return FALSE;

	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONFIG_COPIAS), UI_ALIGN_CENTER);

	return STM_RET_NEXT;
}


int Post_STEP_IS_COMM_TYPE_HOST(char *Result)
{
	int RetFun = 0;

	RetFun = TM_FindFirst(TAB_HOST, &axHost);
		if (RetFun != TM_RET_OK)
			return FALSE;

	RetFun = TM_FindFirst(TAB_IP, &IpTable);
		if (RetFun != TM_RET_OK)
			return FALSE;
//menu de tipo de comunicacion

	UI_MenuReset();

							if (IsMODEM())
							UI_MenuAddItem(mess_getReturnedMessage(MSG_DIAL), LLC_DIAL);
							if(IsRadioETHERNET())
							UI_MenuAddItem(mess_getReturnedMessage(MSG_ETH), LLC_IP);

							if (IsRadioGPRS())
							UI_MenuAddItem(mess_getReturnedMessage(MSG_GPRS),LLC_GPRS);
							if (IsRadioWifi())
							UI_MenuAddItem(mess_getReturnedMessage(MSG_WIFI), LLC_WIFI);

						    RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_COMM_TYPE), 30, axHost.CommunicationType);
							CHECK(RetFun > 0, LBL_CANCEL);

	axHost.CommunicationType = RetFun;

	if (RetFun != LLC_WIFI) {
		if (IsRadioWifi())
				Wifi_PowerOff();
		}else{
			if( !Wifi_IsPowerOn() )
				Wifi_PowerOn();
		}

	return STM_RET_NEXT;

	LBL_CANCEL:
		return STM_RET_CANCEL;

}
bool Pre_STEP_LOCAL_IP_HOST(void)
{
	TABLE_HOST tHost;
	TABLE_IP IpTable;
	int Ret, RetFun = 0;

	Ret = TM_FindFirst(TAB_HOST, &tHost);
	RetFun = TM_FindRecord(TAB_IP, &IpTable, tHost.IP_Primary_Address1);

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_HOST_IP), UI_ALIGN_CENTER);

//	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&IpTable.Host, LZ_HOST);
	return TRUE;
}
int Post_STEP_LOCAL_IP_HOST(char *Result)
{
//	memset(&IpTable.Host, 0x00, sizeof(Step_Buffer) );
	memcpy(&IpTable.Host,Step_Buffer,LZ_HOST);
	return STM_RET_NEXT;
}
bool Pre_STEP_LOCAL_IP_HOST_GPRS(void)
{
	TABLE_HOST tHost;
	TABLE_IP IpTable;
	int Ret, RetFun = 0;

	Ret = TM_FindFirst(TAB_HOST, &tHost);
	RetFun = TM_FindRecord(TAB_IP, &IpTable, tHost.IP_Primary_Address1);

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ETH_HOST_IP), UI_ALIGN_CENTER);

//	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&IpTable.IP_GPRS, LZ_HOST);
	return TRUE;
}
int Post_STEP_LOCAL_IP_HOST_GPRS(char *Result)
{
//	memset(&IpTable.Host, 0x00, sizeof(Step_Buffer) );
	memcpy(&IpTable.IP_GPRS,Step_Buffer,LZ_HOST);
	return STM_RET_NEXT;
}
bool Pre_STEP_IS_PORT_HOST_GPRS(void)
{
	TABLE_HOST tHost;
	TABLE_IP IpTable;
	int Ret, RetFun = 0;

	Ret = TM_FindFirst(TAB_HOST, &tHost);
	RetFun = TM_FindRecord(TAB_IP, &IpTable, tHost.IP_Primary_Address1);

	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_PORT), UI_ALIGN_CENTER);

	memcpy(Step_Buffer,&IpTable.PORT_GPRS, LZ_PORT);

	return TRUE;
}
int Post_STEP_IS_PORT_HOST_GPRS(char *Result)
{



	memcpy(&IpTable.PORT_GPRS,Step_Buffer,strlen(Step_Buffer));



	return STM_RET_NEXT;
}

bool Pre_STEP_IS_PORT_HOST(void)
{
	TABLE_HOST tHost;
	TABLE_IP IpTable;

		int Ret, RetFun = 0;
		Ret = TM_FindFirst(TAB_HOST, &tHost);
		RetFun = TM_FindRecord(TAB_IP, &IpTable, tHost.IP_Primary_Address1);


	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_ETH_CONF_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_PORT), UI_ALIGN_CENTER);

	memcpy(Step_Buffer,&IpTable.Port, LZ_PORT);

	return TRUE;
}
int Post_STEP_IS_PORT_HOST(char *Result)
{



	memcpy(&IpTable.Port,Step_Buffer,strlen(Step_Buffer));



	return STM_RET_NEXT;
}


int Post_STEP_IS_SSL_HOST(char *Result)
{
	int RetFun = 0;
	int SelectedValue;

	if(axHost.CommunicationType == LLC_DIAL)
		return STM_RET_NEXT;

	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_ENABLE), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_DISABLE), 2);

	if(IpTable.Flags1 & IP_FLAG1_ENABLE_SSL)
		SelectedValue = 1;
	else
		SelectedValue = 2;

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_SSL), 30, SelectedValue);
	CHECK(RetFun > 0, LBL_CANCEL);

	if(RetFun == 1){
//		*Result = (bool)TRUE;
		IpTable.Flags1 |= IP_FLAG1_ENABLE_SSL;
		IpTable.Flags1 = SISSL;

	}else{
//		*Result = (bool)FALSE;
		IpTable.Flags1 &= ~(IP_FLAG1_ENABLE_SSL);
		IpTable.Flags1 = NOSSL;

	}
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;
}

int Post_STEP_SAVE_COMUNICACION(char *Result)
{
	int RetFun = 0;
	ulong QuestionResult;

	QuestionResult = UI_ShowMessage("COMUNICACION", mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

	if(QuestionResult == GL_KEY_VALID)
	{

		RetFun = TM_ModifyRecord(TAB_IP, &IpTable);
//		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		RetFun = TM_ModifyRecord(TAB_HOST, &axHost);
//		CHECK(RetFun == TM_RET_OK, LBL_ERROR_GS);

		RetFun = TM_ModifyRecord(TAB_GENERAL_CONF, &tConf);
//		CHECK(RetFun == TM_RET_OK, LBL_ERROR_GS);

		return STM_RET_NEXT;
	}

	return STM_RET_NEXT;

//LBL_ERROR:
	//return STM_RET_ERROR;

//LBL_ERROR_GS:
//			UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
//					mess_getReturnedMessage(MSG_SSL_ERROR_CONF),NULL,
//					GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
//	return STM_RET_ERROR;
}
//+ BATCH01 @mamata Aug 15, 2014
/* --------------------------------------------------------------------------
 * Function Name:	DeleteAllAdvice
 * Description:		Delete Advice
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:Creado por EA para borrar los advices sin necesidad de borrar el bacth
 */
/*int DeleteAllAdvice(void)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint32 MerchantID;
	TABLE_MERCHANT Merchant;
	int32 SelectedMerchGroup=0;


	// release Tree_Tran memory
	if(Tree_Tran != NULL)
	{
		TlvTree_Release(Tree_Tran);
		Tree_Tran = NULL;
	}

	// create memory for Tree_Trann
	Tree_Tran = TlvTree_New(0);
	CHECK(Tree_Tran != NULL, LBL_ERROR)

	// run merchatn selection
	RetFun = STM_RunTransaction(TRN_DELETE_ADVICE, STEP_LIST);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	SelectedMerchGroup = 99999;
	node = TlvTree_AddChild(Tree_Tran, TAG_MERCH_GROUP_ID, &SelectedMerchGroup, LTAG_UINT32);


	node = TlvTree_AddChild(Tree_Tran, TAG_MERCHANT_ID, &SelectedMerchGroup, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	// node merchant selected
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);

	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(uint32*)TlvTree_GetData(node);


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DELETE_ADVICE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_DELETE_ADVICE), UI_ALIGN_CENTER);
	RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CONFIRMATION), tConf.TimeOut);
	CHECK(RetFun == RET_OK, LBL_CANCEL);

		// delete all
		RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR);
				do{
				//	Borrar_Advice((uint8 *)Merchant.BatchName, TRUE);
							// get next merchant
					RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);

				}while(RetFun == TM_RET_OK);


	CHECK(RetFun == RET_OK || RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR)
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_DELETE_ADVICE_SUCCESS), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);

	return RET_OK;

LBL_ERROR:
	UI_ShowMessage(NULL,"ERROR", mess_getReturnedMessage(MSG_DELETE_ADVICE_SUCCESS), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

LBL_CANCEL:
	return RET_NOK;
}*/
//+ BATCH01 @mamata Aug 15, 2014
/* --------------------------------------------------------------------------
 * Function Name:	DeleteAllReverse
 * Description:		Delete Reverse
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:Creado por EA para borrar los reversos sin necesidad de borrar el bacth
 */
int DeleteAllReverse(void)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint32 MerchantID;
	TABLE_MERCHANT Merchant;
	int32 SelectedMerchGroup=0;


	// release Tree_Tran memory
	if(Tree_Tran != NULL)
	{
		TlvTree_Release(Tree_Tran);
		Tree_Tran = NULL;
	}

	// create memory for Tree_Trann
	Tree_Tran = TlvTree_New(0);
	CHECK(Tree_Tran != NULL, LBL_ERROR)

	// run merchatn selection
	RetFun = STM_RunTransaction(TRN_DELETE_ADVICE, STEP_LIST);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	SelectedMerchGroup = 99999;
	node = TlvTree_AddChild(Tree_Tran, TAG_MERCH_GROUP_ID, &SelectedMerchGroup, LTAG_UINT32);


	node = TlvTree_AddChild(Tree_Tran, TAG_MERCHANT_ID, &SelectedMerchGroup, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	// node merchant selected
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);

	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(uint32*)TlvTree_GetData(node);


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DELETE_REVERSE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_DELETE_REVERSE), UI_ALIGN_CENTER);
	RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CONFIRMATION), tConf.TimeOut);
	CHECK(RetFun == RET_OK, LBL_CANCEL);

		// delete all
		RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR);
				do{
				Borrar_Reverse((uint8 *)Merchant.BatchName);
							// get next merchant
					RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);

				}while(RetFun == TM_RET_OK);


	CHECK(RetFun == RET_OK || RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR)
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_DELETE_REVERSE_SUCCESS), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);

	return RET_OK;

LBL_ERROR:
	UI_ShowMessage(NULL,"ERROR", mess_getReturnedMessage(MSG_DELETE_REVERSE_SUCCESS), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

LBL_CANCEL:
	return RET_NOK;
}

int Post_STEP_MODIFY_TPDU (char *result){

	memcpy(auxMerch.TPDU,Step_Buffer, LZ_HOST_TPDU);

	return STM_RET_NEXT;
}

bool Pre_STEP_MODIFY_TPDU (void){


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_MERCHANT), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TPDU), UI_ALIGN_CENTER);

	memcpy(Step_Buffer,&auxMerch.TPDU, LZ_HOST_TPDU);
	// Source address
return TRUE;

}

int Post_STEP_MODIFY_AFILIADO (char *result){

	memcpy(&auxMerch.MID,Step_Buffer, LZ_LID);

	return STM_RET_NEXT;
}

bool Pre_STEP_MODIFY_AFILIADO (void){


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_MERCHANT), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_NUM_AFILIADO), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&auxMerch.MID, LZ_LID);

return TRUE;

}


bool Pre_STEP_MODIFY_AFILIADO_CREDITO (void){


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_MERCHANT), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_AFILIADO_CRE), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&auxMerch.MIDC, LZ_LID);

return TRUE;

}
int Post_STEP_MODIFY_AFILIADO_CREDITO (char *result){

	memcpy(&auxMerch.MIDC,Step_Buffer, LZ_LID);

	return STM_RET_NEXT;
}


bool Pre_STEP_MODIFY_AFILIADO_AMEX (void){


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_MERCHANT), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_AFILIADO_AME), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&auxMerch.MIDA, LZ_LID);

return TRUE;

}
int Post_STEP_MODIFY_AFILIADO_AMEX (char *result){

	memcpy(&auxMerch.MIDA,Step_Buffer, LZ_LID);

	return STM_RET_NEXT;
}

int Post_STEP_MODIFY_TERMINAL (char *result){

	memcpy(&auxMerch.TID,Step_Buffer, LZ_LID);

	return STM_RET_NEXT;
}

bool Pre_STEP_MODIFY_TERMINAL (void){

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_MERCHANT), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TERMINALID), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&auxMerch.TID, LZ_LID);

return TRUE;

}

int Post_STEP_NUMBER_MERCHANT (char *Result){

	int FunResult;
	TLV_TREE_NODE node;
	int NumItems=0;
	int32 SelectedMerchant=0;
	int32 FirstItem = 0;

	UI_MenuReset();

	FunResult = TM_FindFirst(TAB_MERCHANT, &auxMerch);
		CHECK(FunResult == TM_RET_OK, LBL_NO_MERCHGROUP);

		do
		{

				// Transaction is enable, add Merchant Group to menu
				UI_MenuAddItem(auxMerch.Name, auxMerch.Header.RecordID);
				NumItems++;

				if(NumItems == 1)
					FirstItem = auxMerch.Header.RecordID;


			// try to find next record
			FunResult = TM_FindNext(TAB_MERCHANT, &auxMerch);
		//+ NORECORDS @mamata 6/05/2016
		//d }while(FunResult != TM_RET_NO_MORE_RECORDS);
		}while(FunResult == TM_RET_OK);
		CHECK(FunResult == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
		//- NORECORDS

		// check if no item found, then cancel transaction
		CHECK(NumItems > 0, LBL_NO_ITEMS);

		// execute menu
		if(NumItems == 1)
			SelectedMerchant = FirstItem;
		else
			SelectedMerchant = UI_MenuRun(mess_getReturnedMessage(MSG_CONF_MERCHANT), tConf.TimeOut, 0);

		// if is a valid result, goto next step
		if(SelectedMerchant > 0)
		{
			node = TlvTree_AddChild(Tree_Conf, TAG_MERCHANT_ID, &SelectedMerchant, LTAG_UINT32);
			CHECK(node != NULL, LBL_NO_MERCHGROUP)

			TM_FindRecord(TAB_MERCHANT, &auxMerch, SelectedMerchant);

			return STM_RET_NEXT;
		}

LBL_NO_MERCHGROUP: // no merchant 	groups configured
	return STM_RET_CANCEL;

LBL_NO_ITEMS:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION), mess_getReturnedMessage(MSG_NO_ALLOWED), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

//+ NORECORDS @mamata 6/05/2016
LBL_ERROR:
	return STM_RET_ERROR;

}

int IMPRIMIR_PARAMETROS (void){

	int ret =0;
//	int MenuRet;

	ret = Imprimir_header_report();
	ret = Reporte_Terminal();
	ret = Reporte_HOST();
	ret = Reporte_COMM();
	CHECK(ret == RET_OK, LBL_ERROR);
	UI_ShowMessage(	mess_getReturnedMessage(MSG_REPORT_HOST),mess_getReturnedMessage(MSG_REPORT_PARA),
				mess_getReturnedMessage(MSG_EXITO),
				GL_ICON_INFORMATION,
				GL_BUTTON_ALL,
				tConf.TimeOut);

//	CHECK(ret== RET_OK,LBL_MENU_END);
//
//
//	do
//		{
//			UI_MenuReset();
//			// build menu
//			UI_MenuAddItem(mess_getReturnedMessage(MSG_REPORT_TERM), 1);
//			UI_MenuAddItem(mess_getReturnedMessage(MSG_REPORT_HOST), 2);
//			UI_MenuAddItem(mess_getReturnedMessage(MSG_REPORT_RANG), 3);
//			UI_MenuAddItem(mess_getReturnedMessage(MSG_REPORT_COMM), 3);
//			UI_MenuAddItem(mess_getReturnedMessage(MSG_ALL), 4);
//			MenuRet = UI_MenuRun(mess_getReturnedMessage(MSG_REPORT_PARA), 30, 1);
//			CHECK(MenuRet > 0, LBL_MENU_END);
//
//			switch(MenuRet)
//			{
//				case 1:
//
//					ret = Reporte_Terminal();
//					CHECK(ret == RET_OK, LBL_ERROR);
//
//					UI_ShowMessage(	mess_getReturnedMessage(MSG_REPORT_TERM),mess_getReturnedMessage(MSG_REPORT_TERM),
//								mess_getReturnedMessage(MSG_EXITO),
//								GL_ICON_INFORMATION,
//								GL_BUTTON_ALL,
//								tConf.TimeOut);
//
//					break;
//				case 2:
//					ret = Reporte_HOST();
//					CHECK(ret == RET_OK, LBL_ERROR);
//					UI_ShowMessage(	mess_getReturnedMessage(MSG_REPORT_HOST),mess_getReturnedMessage(MSG_REPORT_HOST),
//								mess_getReturnedMessage(MSG_EXITO),
//								GL_ICON_INFORMATION,
//								GL_BUTTON_ALL,
//								tConf.TimeOut);
//					break;
//
//				case 3:
//					ret = Reporte_COMM();
//					CHECK(ret == RET_OK, LBL_ERROR);
//					UI_ShowMessage(	mess_getReturnedMessage(MSG_REPORT_HOST),mess_getReturnedMessage(MSG_REPORT_HOST),
//								mess_getReturnedMessage(MSG_EXITO),
//								GL_ICON_INFORMATION,
//								GL_BUTTON_ALL,
//								tConf.TimeOut);
//
//					break;
//				case 4:
//					ret = Reporte_Terminal();
//					ret = Reporte_HOST();
//					ret = Reporte_COMM();
//					CHECK(ret == RET_OK, LBL_ERROR);
//					UI_ShowMessage(	mess_getReturnedMessage(MSG_REPORT_HOST),mess_getReturnedMessage(MSG_REPORT_PARA),
//								mess_getReturnedMessage(MSG_EXITO),
//								GL_ICON_INFORMATION,
//								GL_BUTTON_ALL,
//								tConf.TimeOut);
//
//					break;
//
//
//				default:
//					return RET_NOK;
//			}
//
//		}while(MenuRet > 0);

	return RET_OK;

//	LBL_MENU_END:
//		return RET_OK;

	LBL_ERROR:
	UI_ShowMessage(	mess_getReturnedMessage(MSG_REPORT_TERM),NULL,
				"ERROR REPORTE",
				GL_ICON_ERROR,
				GL_BUTTON_ALL,
				tConf.TimeOut);
	return RET_NOK;



}
int CONFIG_COMERCIO (void){

	int RetFun =0;
	int MenuRet;
	ulong QuestionResult;


	do
		{
			UI_MenuReset();
			// build menu
			UI_MenuAddItem(mess_getReturnedMessage(MSG_CONF_NOMBREC), 1);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_CONF_RIFC), 2);
			MenuRet = UI_MenuRun(mess_getReturnedMessage(MSG_CONF_COMERCIO), 30, 1);
			CHECK(MenuRet > 0, LBL_MENU_END);

			switch(MenuRet)
			{
				case 1:
					RetFun = STM_RunTransaction(CONF_NOMBREC, CONF_STEP_LIST);
					QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

					if(QuestionResult == GL_KEY_VALID)
					{

						RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
						CHECK(RetFun == TM_RET_OK, LBL_SAVE_ERROR);

						UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);
					}

					break;
				case 2:
					RetFun = STM_RunTransaction(CONF_RIF, CONF_STEP_LIST);
					QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_GENERAL_CONF), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

					if(QuestionResult == GL_KEY_VALID)
					{

						RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
						CHECK(RetFun == TM_RET_OK, LBL_SAVE_ERROR);

						UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);
					}

					break;

				default:
					return RET_NOK;
			}

		}while(MenuRet > 0);


	return STM_RET_NEXT;

	LBL_MENU_END:
		return RET_NOK;
	LBL_SAVE_ERROR:
	UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
				"ERROR GUARDANDO",
				NULL,
				GL_ICON_ERROR,
				GL_BUTTON_ALL,
				tConf.TimeOut);
	return RET_NOK;

}


bool Pre_STEP_MODIFY_NOMBREC (void){


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_COMERCIO), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONF_NOMBREC), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&tTerminal.NameC,LZ_LID );


	return TRUE;
}

int Post_STEP_MODIFY_NOMBREC(char *Result){


	memcpy(&tTerminal.NameC, Step_Buffer, LZ_LID);

	return STM_RET_NEXT;

}
bool Pre_STEP_MODIFY_RIF (void){


	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONF_COMERCIO), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONF_RIFC), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, sizeof(Step_Buffer) );
	memcpy(Step_Buffer,&tTerminal.RifC,LZ_LID );


	return TRUE;
}

int Post_STEP_MODIFY_RIF(char *Result){


	memcpy(&tTerminal.RifC,Step_Buffer, LZ_LID);

	return STM_RET_NEXT;

}

int CONFIG_SEGURIDAD (void){

	int RetFun =0;
	int MenuRet;

	int MenuRet1;
	ulong QuestionResult1;


	do
		{
			UI_MenuReset();
			// build menu
			UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIG_PASSW), 1);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIG_RESET), 2);
			UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIG_MENGE), 3);
			MenuRet = UI_MenuRun(mess_getReturnedMessage(MSG_CONF_COMERCIO), 30, 1);
			CHECK(MenuRet > 0, LBL_MENU_END);

			switch(MenuRet)
			{


				case 1:

					do{
						UI_MenuReset();
									// build menu
						UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIG_PTECN), 1);
						UI_MenuAddItem(mess_getReturnedMessage(MSG_CONFIG_PMERC), 2);
						MenuRet1 = UI_MenuRun(mess_getReturnedMessage(MSG_CONFIG_PASSW), 30, 1);
						CHECK(MenuRet1 > 0, LBL_MENU_END);

						switch(MenuRet1){
						case 1:
							RetFun = STM_RunTransaction(TRN_CAMBIAR_PASSWORD_TECNICO, CONF_STEP_LIST);
							if(RetFun == STM_RET_CANCEL)
							{
								QuestionResult1 = UI_ShowMessage( mess_getReturnedMessage(MSG_GENERAL_CONF),
											mess_getReturnedMessage(MSG_SAVE_CHANGES),
											mess_getReturnedMessage(MSG_YESNO),
											GL_ICON_QUESTION,
											GL_BUTTON_VALID_CANCEL, 30);
							}

							break;
						case 2:
							RetFun = STM_RunTransaction(TRN_CAMBIAR_PASSWORD_MERCHANT, CONF_STEP_LIST);
							if(RetFun == STM_RET_CANCEL)
							{
								QuestionResult1 = UI_ShowMessage( mess_getReturnedMessage(MSG_GENERAL_CONF),
											mess_getReturnedMessage(MSG_SAVE_CHANGES),
											mess_getReturnedMessage(MSG_YESNO),
											GL_ICON_QUESTION,
											GL_BUTTON_VALID_CANCEL, 30);
							}

							break;

						}

						}while(MenuRet > 0);

					break;
				case 2:
					Ingestate_ProcessXML_Offline();

					break;
				case 3:

					return PANEL_CONTROL;

					break;

				default:
					return RET_NOK;
					break;

			}

		}while(MenuRet > 0);

	if( QuestionResult1 == GL_KEY_VALID)
	{
		RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
		CHECK(RetFun == TM_RET_OK, LBL_SAVE_ERROR);
		UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
					mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
					mess_getReturnedMessage(MSG_EXITO),
					GL_ICON_INFORMATION,
					GL_BUTTON_ALL,
					tConf.TimeOut);
	}

	return STM_RET_NEXT;

	LBL_MENU_END:
		return RET_NOK;
	LBL_SAVE_ERROR:
	UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
				"ERROR GUARDANDO",
				NULL,
				GL_ICON_ERROR,
				GL_BUTTON_ALL,
				tConf.TimeOut);
	return RET_NOK;
}

bool pre_clave_tecnico(void){

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONFIG_PASSW), UI_ALIGN_CENTER);
    UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TECH_PASSWORD), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, LZ_TERM_PASSWORD);
	return TRUE;
}
int post_clave_tecnico(char *Result){

	if(tTerminal.Password_Technician[0] == 0)
	{
		if(strcmp(Step_Buffer, Technician_Password) == 0 )
			return STM_RET_NEXT;
	}
	else
	{
		if(strcmp(Step_Buffer, tTerminal.Password_Technician) == 0 )
			return STM_RET_NEXT;
	}

	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BAD_PASSWORD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

	return STM_RET_AGAIN;

}

bool pre_ModificarClaveTecnico(void){

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONFIG_PASSW), UI_ALIGN_CENTER);
    UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONFIG_PTECN), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, LZ_TERM_PASSWORD);
	return TRUE;
}

int post_ModificarClaveTecnico(char *Result){

	if(strcmp(Step_Buffer, tTerminal.Password_Technician) == 0){
		UI_ShowMessage(NULL,"REPETIDA", NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_AGAIN;
	}

	memcpy(&tTerminal.Password_Technician,Step_Buffer,LZ_TERM_PASSWORD);

	return STM_RET_NEXT;

}
bool pre_ModificarClaveMerchant(void){

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_CONFIG_PASSW), UI_ALIGN_CENTER);
    UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CONFIG_PMERC), UI_ALIGN_CENTER);


	memset( Step_Buffer, 0x00, LZ_TERM_PASSWORD);

	return TRUE;

}

int post_ModificarClaveMerchant(char *Result){

	if(strcmp(Step_Buffer, tTerminal.Password_Merchant) == 0){
		UI_ShowMessage(NULL,"REPETIDA", NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_AGAIN;
	}

	memcpy(&tTerminal.Password_Merchant,Step_Buffer,LZ_TERM_PASSWORD);

	return STM_RET_NEXT;

}

int post_GuardarClave(char *Result){

	int RetFun = 0;
	ulong QuestionResult;

	QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

	if(QuestionResult == GL_KEY_VALID)
	{

		RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR_GS);
		UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
							mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
							mess_getReturnedMessage(MSG_EXITO),
							GL_ICON_INFORMATION,
							GL_BUTTON_ALL,
							tConf.TimeOut);
		return STM_RET_NEXT;
	}

	return STM_RET_NEXT;
LBL_ERROR_GS:
			UI_ShowMessage(mess_getReturnedMessage(MSG_EMPARAM),
					mess_getReturnedMessage(MSG_SSL_ERROR_CONF),NULL,
					GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_ERROR;
}

bool pre_PEDIR_CIERRE(void){
	int RetFun;
	TABLE_HEADER TableHeader;
	int32 MerchantID;
	MerchantID=1;


	TM_FindRecord(TAB_MERCHANT, &auxMerch, MerchantID);
	//CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	// get batch header
	RetFun = TM_GetTableHeader(auxMerch.BatchName, &TableHeader);

	// validate if batch if not empty
	CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);

	CHECK(!auxMerch.BatchSettle, LBL_BATCH_EMPTY);

	UI_ShowMessage(mess_getReturnedMessage(MSG_CAMBIO),"DEBE REALIZAR CIERRE",NULL,
									GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return FALSE;

	LBL_BATCH_EMPTY:
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, "CAMBIO", UI_ALIGN_CENTER);

	return TRUE;


}

int post_cambio_moneda(char *Result){

	int Moneda = 0;
	int RetFun = 0;
	ulong QuestionResult;

		UI_MenuReset();
		UI_MenuAddItem(mess_getReturnedMessage(MSG_BOLIVAR), 1);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_DOLAR), 2);


		Moneda = UI_MenuRun(mess_getReturnedMessage(MSG_COMERCIO), 30, auxMerch.Currency);
		CHECK(Moneda > 0, LBL_CANCEL);

		auxMerch.Currency = (uint8)Moneda;


		QuestionResult = UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO), mess_getReturnedMessage(MSG_SAVE_CHANGES), mess_getReturnedMessage(MSG_YESNO), GL_ICON_QUESTION, GL_BUTTON_VALID_CANCEL, 30);

			if(QuestionResult == GL_KEY_VALID)
			{

				RetFun = TM_ModifyRecord(TAB_MERCHANT, &auxMerch);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR_GS);
				UI_ShowMessage(	mess_getReturnedMessage(MSG_CONF_MERCHANT),
									"CAMBIO DE MONEDA",
									mess_getReturnedMessage(MSG_EXITO),
									GL_ICON_INFORMATION,
									GL_BUTTON_ALL,
									tConf.TimeOut);
				return STM_RET_NEXT;
			}


		LBL_CANCEL:
		return STM_RET_CANCEL;

		LBL_ERROR_GS:
						UI_ShowMessage(mess_getReturnedMessage(MSG_COMERCIO),
								mess_getReturnedMessage(MSG_SSL_ERROR_CONF),NULL,
								GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_ERROR;
}
