/*************************************************************
 *
 * Messages.c
 * Manage the messages for an application, using the manager mode.
 *
 * @AJAF, 20130904
 *
 *************************************************************/

#include <sdk_tplus.h>
#include "MSGlib.h"
#include "CoreMessages.h"	// Messages' name definitions

/*************** DEFINES *********************/
#define MESSAGES_DIR	"/SYSTEM/MESSAGES.SGN"

/************** LOCAL VARS*********************/
static char *LocalMessages;// Address of the messages copied in the terminal
static bool isLoadedMessages;

/************** EXTERN VARS*********************/
extern const unsigned char CoreMessagesDef[]; // Default messages in spanish

/********** MESSAGES FUNCTIONS ****************/

/**
 * Init the messages management, load the messages.
 */
void mess_initMessages(void)
{
	char *NewMessages; 	// Address of the loaded message file

	LocalMessages=(char *)CoreMessagesDef;

	// Load the message file in memory
	NewMessages = LoadMSG(MESSAGES_DIR);
	if( NewMessages == NULL ) {
		// Local messages are used
		DefCurrentMSG(LocalMessages);
		isLoadedMessages = FALSE;
		// Define the messages used by default (if messages are not found in the current MSG file)
		DefDefaultMSG(LocalMessages);
		// Define current and default languages (English)
		DefDefaultLang("ES");
		DefCurrentLang("ES");
	} else {
		// Messages (from the loaded MSG file) are used
		DefCurrentMSG(NewMessages);
		isLoadedMessages = TRUE;
		// Define current and default languages (English)
//		DefCurrentLang(GetCurrentLang());
	}
}

/**
 * get a double pointer (array) with the supported languages codes (ISO 639-1)
 * @param LangNumber, Return the number of languages.
 * @return
 */
char** mess_getLoadedLanguages(int *LangNumber)
{
	char **ppLanguagesTab;
	char LangAux[3];
	int i = 0;

	if(isLoadedMessages) {
//		char *pLanguages;
		char pLanguages[16];

		// Get the languages supported by the application
		*LangNumber = GetSupportedLanguages(pLanguages,GetCurrentMSG());

		ppLanguagesTab = umalloc(sizeof(char*)*(*LangNumber+1));//se agrega uno para el nulo
		for(i=0 ; i < *LangNumber; i++) {
			ppLanguagesTab[i] = umalloc(sizeof(char)*3);//se agrega uno para el nulo
			// Copy the ISO639-1 language code
			strncpy(LangAux,pLanguages+(i*2),2);
			LangAux[2]='\x00';
			strcpy(ppLanguagesTab[i], LangAux);
		}
	} else {
		ppLanguagesTab = umalloc(sizeof(char*)*2);//se agrega uno para el nulo
		ppLanguagesTab[i] = umalloc(sizeof(char)*3);//se agrega uno para el nulo
		strncpy(LangAux, EN, 2);
		LangAux[2]='\x00';
		*LangNumber = 1;
		strcpy(ppLanguagesTab[i++], LangAux);
	}
	ppLanguagesTab[i] = NULL;
	return ppLanguagesTab;
}

/**
 * Get the name of the languages from the pointer with language codes.
 * @param ppLanguagesTab. Language codes.
 * @param LangNumber.	Number of languages
 * @return
 */
char** mess_getLanguageName(char **ppLanguagesTab, int LangNumber)
{
	char **ppLanguagesName;
	char LangAux[3];
	char strLangNameAux[20];
	int i;

	memset(LangAux, 0x00, sizeof(LangAux));
	ppLanguagesName = umalloc(sizeof(char*)*(LangNumber+1));//se agrega uno para el nulo
	for(i=0 ; i < LangNumber; i++) {
		int iLen = 0;

		// Copy the ISO639-1 language code
		strncpy(LangAux, ppLanguagesTab[i], 2);
		memset(strLangNameAux, 0x00, sizeof(strLangNameAux));
		if((strncmp(LangAux, EN, 2)) == 0)
			iLen = sprintf(strLangNameAux, "ENGLISH");
		else if((strncmp(LangAux, FR, 2)) == 0)
			iLen = sprintf(strLangNameAux, "FRENCH");
		else if((strncmp(LangAux, ES, 2)) == 0)
			iLen = sprintf(strLangNameAux, "SPANISH");
		ppLanguagesName[i] = umalloc(sizeof(char)*(iLen + 1));//se agrega uno para el nulo
		strcpy(ppLanguagesName[i], strLangNameAux);
	}
	ppLanguagesName[i] = NULL;
	return ppLanguagesName;
}

/**
 * Get the Current language code
 * @param pCurrLangCode
 */
void mess_getCurrentLanguageCode(char *pCurrLangCode)
{
	char *LCode = NULL;
	LCode = GetCurrentLang();
	sprintf(pCurrLangCode, "%s", LCode);
}

/**
 * Get the Current language name
 * @param pCurrLangName
 */
void mess_getCurrentLanguageName(char *pCurrLangName)
{
	char **pCurrLangCode;
	char **pLangNames;

	pCurrLangCode = umalloc(sizeof(char*)*2);//se agrega uno para el nulo
	pCurrLangCode[0] = GetCurrentLang();
	pLangNames = mess_getLanguageName(pCurrLangCode, 1);
	sprintf(pCurrLangName, "%s", pLangNames[0]);
}

/**
 * Get the indicated message
 * @param MSGnum
 * @param currMessage
 * @return OK or error (-1)
 */
int mess_getMessageByParameter(int MSGnum, char *currMessage)
{
	MSGinfos currentMSG;
	int iRet;

	iRet = GetMessageInfos(MSGnum,&currentMSG);
	strcpy(currMessage, currentMSG.message);
	return iRet;
}

/**
 * Get the indicated message
 * @param MSGnum
 * @return Message
 */
char* mess_getReturnedMessage(int MSGnum)
{
	MSGinfos currentMSG;
	int iRet;

	iRet = GetMessageInfos(MSGnum,&currentMSG);
	return (currentMSG.message);
}

/**
 * Set the current language
 * @param pCurrLangCode. Language code for language to set
 */
void mess_setCurrentLanguage(char *pCurrLangCode)
{
//	DefCurrentLang(pCurrLangCode);
	// Define the messages used by default (if messages are not found in the current MSG file)
	DefDefaultMSG(GetCurrentMSG());
	// Define current and default languages (English)
	DefDefaultLang(pCurrLangCode);
	DefCurrentLang(pCurrLangCode);
}

/**
 * get a the number of loaded languages.
 * @return
 */
int mess_getLoadedLanguagesNumber(void)
{
	int LangNumber = 1;

	if(isLoadedMessages) {
		char *pLanguages = NULL;

		// Get the languages supported by the application
		LangNumber=GetSupportedLanguages(pLanguages,GetCurrentMSG());
	}
	return LangNumber;
}
