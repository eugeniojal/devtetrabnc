/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Batch.c
 * Header file:		Batch.h
 * Description:		Implement functions to manage transaction batch
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	Batch_InitTables
 * Description:		Init tables for Merchants batch
 * Parameters:
 *  - none
 * Return:
 *  - BATCH_RET_OK - all batch table are OK
 *  - BATCH_RET_NOK - cannot create a batch table
 * Notes:
 */
int Batch_InitTables(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;

	// iterate all merchants
	memset((char*)&Merchant, 0, L_TABLE_MERCHANT);
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	do
	{
		// create batch file or current merchant
		if(NULL != Merchant.BatchName[0])
		{

			RetFun = TM_CreateTable(Merchant.BatchName, L_BATCH);
			CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);
		}

		// read next merchant
		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun != TM_RET_NO_MORE_RECORDS);

	return BATCH_RET_OK;

LBL_ERROR: // cannot create batch file
	return BATCH_RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Add_Transaction
 * Description:		Add transaction to batch file
 * Parameters:
 *  - Tree - tree of transaction
 * Return:
 *  BATCH_RET_OK - transaction added to batch file
 *  BATCH_RET_NOK - cannot add transaction to batch file
 * Notes:
 */
int Batch_Add_Transaction(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int RetFun;
	BATCH BatchRecord;
	uint8 BatchName[LZ_MERCH_BATCH_NAME];
	int Tries = 0;

	// get batch name
	memset(BatchName, 0, LZ_MERCH_BATCH_NAME);
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_NAME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)BatchName, TlvTree_GetData(node), TlvTree_GetLength(node));

	// fill batch record
	memset((char*)&BatchRecord, 0, L_BATCH);
	RetFun = Batch_Fill_Record(Tree_Tran, &BatchRecord);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// add record to Batch File
	do
	{
		Tries++;
		RetFun = TM_AddRecord((char*)BatchName, &BatchRecord);
	}while(RetFun != TM_RET_OK && Tries < 4);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	// add file index to tree
	node = TlvTree_AddChild(Tree, TAG_BATCH_FILE_INDEX, &BatchRecord.Header.FileIndex, LTAG_UINT32);

	return BATCH_RET_OK;

LBL_ERROR:
	return BATCH_RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Batch_Modify_Transaction
 * Description:		Modify transaction in batch file
 * Parameters:
 *  - Tree - tree of transaction
 * Return:
 *  BATCH_RET_OK - transaction modified
 *  BATCH_RET_NOK - cannot add modify transaction
 *  BATCH_RET_CANNOT_FIND_RECORD - cannot find transaction
 * Notes:
 */
int Batch_Modify_Transaction(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int RetFun;
	BATCH BatchRecord;
	uint8 BatchName[LZ_MERCH_BATCH_NAME];
	uint32 FileIndex;
	int Tries = 0;

	// get batch name
	memset(BatchName, 0, LZ_MERCH_BATCH_NAME);
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_NAME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)BatchName, TlvTree_GetData(node), TlvTree_GetLength(node));

	// get invoice
	RetFun = Tlv_GetTagValue(Tree, TAG_BATCH_FILE_INDEX, &FileIndex, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	/*node = TlvTree_Find(Tree, TAG_BATCH_FILE_INDEX, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&FileIndex, TlvTree_GetData(node), LTAG_UINT32);*/

	// Get record data from batch file
	RetFun = TM_GetDirectRecord((char*)BatchName, &BatchRecord, FileIndex);
	CHECK(RetFun == TM_RET_OK, LBL_NO_RECROD);

	// fill batch record
	RetFun = Batch_Fill_Record(Tree, &BatchRecord);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// add record to Batch File
	do
	{
		Tries++;
		RetFun = TM_ModifyRecord((char*)BatchName, &BatchRecord);
	}while(RetFun != TM_RET_OK && Tries < 4);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return BATCH_RET_OK;

LBL_ERROR:
	return BATCH_RET_NOK;

LBL_NO_RECROD:
	return BATCH_RET_CANNOT_FIND_RECORD;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Delete_Transaction
 * Description:		Modify transaction in batch file
 * Parameters:
 *  - Tree - tree of transaction
 * Return:
 *  BATCH_RET_OK - transaction deleted
 *  BATCH_RET_NOK - cannot delete transaction
 *  BATCH_RET_CANNOT_FIND_RECORD - cannot find transaction
 * Notes:
 */
int Batch_Delete_Transaction(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int RetFun;
	BATCH BatchRecord;
	uint8 BatchName[LZ_MERCH_BATCH_NAME];
	uint32 FileIndex;
	int Tries = 0;

	// get batch name
	memset(BatchName, 0, LZ_MERCH_BATCH_NAME);
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_NAME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)BatchName, TlvTree_GetData(node), TlvTree_GetLength(node));

	// get invoice
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_INDEX, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&FileIndex, TlvTree_GetData(node), TlvTree_GetLength(node));

	// Get record data from batch file
	RetFun = TM_GetDirectRecord((char*)BatchName, &BatchRecord, FileIndex);
	CHECK(RetFun == TM_RET_OK, LBL_NO_RECROD);

	// add record to Batch File
	do
	{
		Tries++;
		RetFun = TM_DeleteRecord((char*)BatchName, &BatchRecord);
	}while(RetFun != TM_RET_OK && Tries < 4);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return BATCH_RET_OK;

LBL_ERROR:
	return BATCH_RET_NOK;

LBL_NO_RECROD: // cannot find record
	return BATCH_RET_CANNOT_FIND_RECORD;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Get_Last_Transaction
 * Description:		Get last transaction of batch
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - uint8 *BatchName - name of batch file
 * Return:
 *  BATCH_RET_OK - last transaction ok
 *  BATCH_RET_NOK - cannot open last transaction
 *  BATCH_RET_EMPTY_FILE - no transactions in batch file
 * Notes:
 */
int Batch_Get_Last_Transaction(TLV_TREE_NODE Tree, uint8 *BatchName)
{
	BATCH RecordID;
	int RetFun;
	uint16 TranStatus;
	TLV_TREE_NODE node;
	uint16 TranID;

	// try to get last transaction
	memset((char*)&RetFun, 0, L_BATCH);
	RetFun = TM_GetLastRecord((char*)BatchName, &RecordID);
	CHECK(RetFun == TM_RET_OK, LBL_TAB_ERROR);

	// verify if record is file header
	CHECK(RecordID.Header.FileIndex != 0, LBL_EMPTY_FILE);

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	if(node != NULL)
	{
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);
		if(TranID == LAST_ANSWER)
		{
			RetFun = Batch_FindTransaction(Tree_Tran, (uint32)RecordID.STAN);
			CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN)

			// get transaction status
			node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
			CHECK(node != NULL, LBL_ERROR);
			TranStatus = *(uint16*)TlvTree_GetData(node);

			CHECK(!(TranStatus & FSTATUS_NEED_REVERSE), LBL_NO_REVERSE);
		}
	}

	// get transaction data
	RetFun = Batch_Get_Transaction_Data(Tree, &RecordID);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	if(TranID == LAST_ANSWER)
	{

		Tlv_SetTagValueInteger(Tree_Tran, TAG_TRAN_STATUS, TranStatus, LTAG_INT16);
	}

	return BATCH_RET_OK;

LBL_ERROR:
	return BATCH_RET_NOK;

LBL_TAB_ERROR:
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		return BATCH_RET_EMPTY_FILE;
	else
		return BATCH_RET_NOK;

LBL_EMPTY_FILE:
	return BATCH_RET_EMPTY_FILE;

LBL_NO_REVERSE:
	UI_ShowMessage(NULL, "REVERSO TRANS.", NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_CANCEL;

LBL_NO_TRAN:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_TRAN_BATCH), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Batch_Get_Transaction_Data
 * Description:		get transaction data from BATCH record and put in Tree
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - BATCH BatchRecord - source of transaction data
 * Return:
 *  - BATCH_RET_OK
 * Notes:
 */
int Batch_Get_Transaction_Data(TLV_TREE_NODE Tree, BATCH *BatchRecord)
{
	TLV_TREE_NODE node;
	int RetFun;

	// add TAG_INVOICE
	node = TlvTree_Find(Tree, TAG_INVOICE, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_INVOICE, &BatchRecord->Invoice, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->Invoice, LTAG_UINT32);

	// add TAG_BATCH_FILE_INDEX
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_INDEX, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_BATCH_FILE_INDEX, &BatchRecord->Header.FileIndex, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->Header.FileIndex, LTAG_UINT32);

	// add TAG_TRAN_ID
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_TRAN_ID, &BatchRecord->TranID, LTAG_UINT16);
	else
		TlvTree_SetData(node, &BatchRecord->TranID, LTAG_UINT16);


	// add TAG_TRAN_STATUS
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_TRAN_STATUS, &BatchRecord->TranStatus, LTAG_UINT16);
	else
		TlvTree_SetData(node, &BatchRecord->TranStatus, LTAG_UINT16);

	// add TAG_TRAN_NAME
	node = TlvTree_Find(Tree, TAG_TRAN_NAME, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_TRAN_NAME, &BatchRecord->TranName, LTAG_UINT16);
	else
		TlvTree_SetData(node, &BatchRecord->TranName, LTAG_UINT16);

//	node = TlvTree_Find(Tree, TAG_EMV_APP_LBL, 0);
//	if(node == NULL)
//		TlvTree_AddChild(Tree, TAG_EMV_APP_LBL, &BatchRecord->APP_LBL, LTAG_UINT16);
//	else
//		TlvTree_SetData(node, &BatchRecord->APP_LBL, LTAG_UINT16);

	// add TAG_TRAN_OPTIOS
	node = TlvTree_Find(Tree, TAG_TRAN_OPTIOS, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_TRAN_OPTIOS, &BatchRecord->TranOptions, LTAG_UINT16);
	else
		TlvTree_SetData(node, &BatchRecord->TranOptions, LTAG_UINT16);

	// add TAG_MERCH_GROUP_ID
	node = TlvTree_Find(Tree, TAG_MERCH_GROUP_ID, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_MERCH_GROUP_ID, &BatchRecord->MerchantGroupID, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->MerchantGroupID, LTAG_UINT32);

	// add TAG_MERCHANT_ID
	node = TlvTree_Find(Tree, TAG_MERCHANT_ID, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_MERCHANT_ID, &BatchRecord->MerchantID, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->MerchantID, LTAG_UINT32);

	// add TAG_RANGE_ID
	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_RANGE_ID, &BatchRecord->RangeID, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->RangeID, LTAG_UINT32);

	// add TAG_HOST_ID
	node = TlvTree_Find(Tree, TAG_HOST_ID, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_HOST_ID, &BatchRecord->HostID, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->HostID, LTAG_UINT32);

	// add TAG_PAN
	node = TlvTree_Find(Tree, TAG_PAN, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_PAN, BatchRecord->PAN, strlen((char*)BatchRecord->PAN));
	else
		TlvTree_SetData(node, BatchRecord->PAN, strlen((char*)BatchRecord->PAN));

	// add TAG_EXP_DATE
	node = TlvTree_Find(Tree, TAG_EXP_DATE, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_EXP_DATE, &BatchRecord->ExpirationDate, L_EXP_DATE);
	else
		TlvTree_SetData(node, &BatchRecord->ExpirationDate, L_EXP_DATE);

	// add TAG_SERVICE_CODE
	node = TlvTree_Find(Tree, TAG_SERVICE_CODE, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_SERVICE_CODE, &BatchRecord->ServiceCode, L_SERVICE_CODE);
	else
		TlvTree_SetData(node, &BatchRecord->ServiceCode, L_SERVICE_CODE);

	// add TAG_CURRENCY
	node = TlvTree_Find(Tree, TAG_CURRENCY, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_CURRENCY, &BatchRecord->Currency, LTAG_UINT8);
	else
		TlvTree_SetData(node, &BatchRecord->Currency, LTAG_UINT8);

	// add TAG_ENTRY_MODE
	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_ENTRY_MODE, &BatchRecord->EntryMode, LTAG_UINT8);
	else
		TlvTree_SetData(node, &BatchRecord->EntryMode, LTAG_UINT8);

	// add TAG_ENTRY_MODE_ORIGINAL
	node = TlvTree_Find(Tree, TAG_ENTRY_ORIGINAL, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_ENTRY_ORIGINAL, &BatchRecord->EntryModeOriginal, LTAG_UINT8);
	else
		TlvTree_SetData(node, &BatchRecord->EntryModeOriginal, LTAG_UINT8);

	// add TAG_FALLBACK
	node = TlvTree_Find(Tree, TAG_FALLBACK, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_FALLBACK, &BatchRecord->Fallback, LTAG_UINT8);
	else
		TlvTree_SetData(node, &BatchRecord->Fallback, LTAG_UINT8);

	// add TAG_BASE_AMOUNT
	node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_BASE_AMOUNT, &BatchRecord->BaseAmount, LTAG_UINT64);
	else
		TlvTree_SetData(node, &BatchRecord->BaseAmount, LTAG_UINT64);

	// add TAG_TAX_AMOUNT
	node = TlvTree_Find(Tree, TAG_TAX_AMOUNT, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_TAX_AMOUNT, &BatchRecord->TaxAmount, LTAG_UINT64);
	else
		TlvTree_SetData(node, &BatchRecord->TaxAmount, LTAG_UINT64);

	// add TAG_TIP_AMOUNT
	node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_TIP_AMOUNT, &BatchRecord->TipAmount, LTAG_UINT64);
	else
		TlvTree_SetData(node, &BatchRecord->TipAmount, LTAG_UINT64);

	// add TAG_ORIGINAL_TOTAL
	node = TlvTree_Find(Tree, TAG_ORIGINAL_TOTAL, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_ORIGINAL_TOTAL, &BatchRecord->OriginalTotal, LTAG_UINT64);
	else
		TlvTree_SetData(node, &BatchRecord->OriginalTotal, LTAG_UINT64);

	// add TAG_STAN
	node = TlvTree_Find(Tree, TAG_STAN, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_STAN, &BatchRecord->STAN, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->STAN, LTAG_UINT32);

	// TAG_DATE_TIME
	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_DATE_TIME, &BatchRecord->DateTime, sizeof(DATE));
	else
		TlvTree_SetData(node, &BatchRecord->DateTime, sizeof(DATE));


	// TAG_TIME_REVERSE(EUGENIO)
		node = TlvTree_Find(Tree, TAG_TIME_REVERSE, 0);
		if(node == NULL)
			TlvTree_AddChild(Tree, TAG_TIME_REVERSE, &BatchRecord->DateTimeReverse, sizeof(DATE));
		else
			TlvTree_SetData(node, &BatchRecord->DateTimeReverse, sizeof(DATE));


	// TAG_HOST_RESPONSE_CODE
	if(BatchRecord->HostResponseCode[0] != 0)
	{
		node = TlvTree_Find(Tree, TAG_HOST_RESPONSE_CODE, 0);
		if(node == NULL)
			TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, &BatchRecord->HostResponseCode, LZ_RESPONSE_CODE);
		else
			TlvTree_SetData(node, &BatchRecord->HostResponseCode, LZ_RESPONSE_CODE);
	}

	// TAG_HOST_AUTH_NUMBER
	if(BatchRecord->HostAuthNumber[0] != 0)
	{
		node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
		if(node == NULL)
			TlvTree_AddChild(Tree, TAG_HOST_AUTH_NUMBER, &BatchRecord->HostAuthNumber, LZ_AUTH_NUMBER);
		else
			TlvTree_SetData(node, &BatchRecord->HostAuthNumber, LZ_AUTH_NUMBER);
	}

	// TAG_HOST_RRN
	if(BatchRecord->HostRRN[0] != 0)
	{
		node = TlvTree_Find(Tree, TAG_HOST_RRN, 0);
		if(node == NULL)
			TlvTree_AddChild(Tree, TAG_HOST_RRN, &BatchRecord->HostRRN, LZ_RRN);
		else
			TlvTree_SetData(node, &BatchRecord->HostRRN, LZ_RRN);
	}


	//tag id number
	if(BatchRecord->Idnumber[0] != 0)
	{
		RetFun = Tlv_SetTagValueString(Tree,TAG_ID_NUMBER,BatchRecord->Idnumber);

		if(RetFun != RET_OK)
			TlvTree_SetData(node,BatchRecord->Idnumber, TlvTree_GetLength(node));

	}

	if(BatchRecord->APP_LBL[0] != 0)
	{
		RetFun = Tlv_SetTagValueString(Tree,TAG_EMV_APP_LBL,BatchRecord->APP_LBL);

		if(RetFun != RET_OK)
			TlvTree_SetData(node,BatchRecord->APP_LBL, TlvTree_GetLength(node));

	}
	//TAG
	node = TlvTree_Find(Tree, TAG_ISSUER_SCRIPT_RESULT, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree, TAG_ISSUER_SCRIPT_RESULT, &BatchRecord->EWL_TAG_ISSUER_SCRIPT, EWL_TAG_ISSUER_SCRIPT_RESULT_LEN);
	else
		TlvTree_SetData(node, &BatchRecord->EWL_TAG_ISSUER_SCRIPT, TlvTree_GetLength(node));

	node = TlvTree_Find(Tree, TAG_TRACK2, 0);
	if(node == NULL)
	    TlvTree_AddChild(Tree,TAG_TRACK2, &BatchRecord->TRACK2,TRACK2_LEN);
	else
		TlvTree_SetData(node, &BatchRecord->TRACK2, TRACK2_LEN);

	// add Numero de batch
	/*node = TlvTree_Find(Tree, TAG_BATCH_NUMBER, 0);
	if(node == NULL)
		TlvTree_AddChild(Tree,TAG_BATCH_NUMBER, &BatchRecord->NBatch, LTAG_UINT32);
	else
		TlvTree_SetData(node, &BatchRecord->NBatch, LTAG_UINT32);*/

	if((BatchRecord->RangeID != 8) && (BatchRecord->Fallback!= 1))
	{
	// TAG_EMV_1GEN_TAGS
	if(BatchRecord->EMV_1GEN_TAGS_LEN > 0)
	{
		TLV_TREE_NODE tmpnode;
		uint8 TData = 1;
		int RetFun;
		node = TlvTree_Find(Tree, TAG_EMV_1GEN_TAGS, 0);
		if(node != NULL)
			TlvTree_Release(node);

		node = TlvTree_AddChild(Tree, TAG_EMV_1GEN_TAGS, &TData, 1);
		CHECK(node != NULL, LBL_ERROR);

		tmpnode = TlvTree_New(0);
		CHECK(tmpnode != NULL, LBL_ERROR);

		RetFun = TlvTree_Unserialize(&tmpnode, TLV_TREE_SERIALIZER_DEFAULT, BatchRecord->EMV_1GEN_TAGS, BatchRecord->EMV_1GEN_TAGS_LEN);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

		RetFun = utilInsertEMVTags(node, tmpnode);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		//+ TREEMEM @mamata 5/05/2016
		TlvTree_Release(tmpnode);
		tmpnode = NULL;
		//- TREEMEM

	}

	// TAG_EMV_PRINT_TAGS
	if(BatchRecord->EMV_PRINT_TAGS_LEN > 0)
	{
		uint8 TData = 1;
		int RetFun;
		node = TlvTree_Find(Tree, TAG_EMV_PRINT_TAGS, 0);
		if(node != NULL)
			TlvTree_Release(node);

		node = TlvTree_AddChild(Tree, TAG_EMV_PRINT_TAGS, &TData, 1);
		CHECK(node != NULL, LBL_ERROR);

		//+ EMV03 @mamata Dec 16, 2014
		TLV_TREE_NODE tmpnode;

		//+ TREEMEM @mamata 5/05/2016
		tmpnode = TlvTree_New(0);
		CHECK(tmpnode != NULL, LBL_ERROR);
		//- TREEMEM

		RetFun = TlvTree_Unserialize(&tmpnode, TLV_TREE_SERIALIZER_DEFAULT, BatchRecord->EMV_PRINT_TAGS, BatchRecord->EMV_PRINT_TAGS_LEN);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

		RetFun = utilInsertEMVTags(node, tmpnode);
		CHECK(RetFun == RET_OK, LBL_ERROR);
		//d RetFun = TlvTree_Unserialize(&node, TLV_TREE_SERIALIZER_DEFAULT, BatchRecord->EMV_PRINT_TAGS, BatchRecord->EMV_PRINT_TAGS_LEN);
		//d CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
		//- EMV03

		//+ TREEMEM @mamata 5/05/2016
		TlvTree_Release(tmpnode);
		tmpnode = NULL;
		//- TREEMEM
	}

}
	// pin online was requested
	RetFun = Tlv_SetTagValue(Tree, TAG_TRAN_REQ_PIN_ONLINE,
			&BatchRecord->PinOnReq, sizeof(BatchRecord->PinOnReq));

	// pin off-line was requested
	RetFun = Tlv_SetTagValue(Tree, TAG_TRAN_REQ_PIN_OFFLINE,
			&BatchRecord->PinOffReq, sizeof(BatchRecord->PinOffReq));

	// Signature was requested
	RetFun = Tlv_SetTagValue(Tree, TAG_TRAN_REQ_SIGNATURE,
			&BatchRecord->SignReq, sizeof(BatchRecord->SignReq));

	RetFun = Tlv_SetTagValueInteger(Tree, TAG_SERVER_NUMBER, BatchRecord->Server, LTAG_UINT32);

	RetFun = Tlv_SetTagValueInteger(Tree, TAG_ACCOUNT_TYPE, BatchRecord->AccountType, LTAG_UINT8);

	if(BatchRecord->TranID == TRAN_C2P)
	{
		RetFun = Tlv_SetTagValueInteger(Tree, TAG_BANK_TYPE, BatchRecord->TranBank, LTAG_UINT32);
	}
	return BATCH_RET_OK;

LBL_ERROR:
	return BATCH_RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_Fill_Record
 * Description:		Fill BATCH record from Tree
 * Parameters:
 *  - TLV_TREE_NODE Tree - source of transaction data
 *  - BATCH BatchRecord - where to put transaction data
 * Return:
 *  BATCH_RET_OK
 *  BATCH_RET_NOK - a mandatory field is missing
 * Notes:
 */
int Batch_Fill_Record(TLV_TREE_NODE Tree, BATCH *BatchRecord)
{
	TLV_TREE_NODE node;

	// add invoice as record id
	node = TlvTree_Find(Tree, TAG_INVOICE, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->Invoice, TlvTree_GetData(node), LTAG_UINT32);

	// add transaction id
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->TranID, TlvTree_GetData(node), LTAG_UINT16);

	// add transaction status
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->TranStatus, TlvTree_GetData(node), LTAG_UINT16);

	// add transaction name
	node = TlvTree_Find(Tree, TAG_TRAN_NAME, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->TranName, TlvTree_GetData(node), LTAG_UINT16);

//	APP_Lbl EUGENIO
//	node = TlvTree_Find(Tree, TAG_EMV_APP_LBL , 0);
//	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
//	memcpy((char*)&BatchRecord->APP_LBL, TlvTree_GetData(node), LTAG_UINT16);

	// add transaction options
	node = TlvTree_Find(Tree, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->TranOptions, TlvTree_GetData(node), LTAG_UINT16);

	// add merchant group id
	node = TlvTree_Find(Tree, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->MerchantGroupID, TlvTree_GetData(node), LTAG_UINT32);

	// add merchant id
	node = TlvTree_Find(Tree, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->MerchantID, TlvTree_GetData(node), LTAG_UINT32);

	// add range id
	//node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	//CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	if(node != NULL)
	memcpy((char*)&BatchRecord->RangeID, TlvTree_GetData(node), LTAG_UINT32);

	// add host id
	node = TlvTree_Find(Tree, TAG_HOST_ID, 0);
	CHECK(node != NULL, LBL_ERROR_MANDATORY_TAG);
	memcpy((char*)&BatchRecord->HostID, TlvTree_GetData(node), LTAG_UINT32);

	// add PAN
	node = TlvTree_Find(Tree, TAG_PAN, 0);
	if(node != NULL)
		memcpy(BatchRecord->PAN, TlvTree_GetData(node), TlvTree_GetLength(node));

	// add expiration date
	node = TlvTree_Find(Tree, TAG_EXP_DATE, 0);
	if(node != NULL)
		memcpy(BatchRecord->ExpirationDate, TlvTree_GetData(node), TlvTree_GetLength(node));

	// add service code
	node = TlvTree_Find(Tree, TAG_SERVICE_CODE, 0);
	if(node != NULL)
		memcpy(BatchRecord->ServiceCode, TlvTree_GetData(node), TlvTree_GetLength(node));

	// add currency
	node = TlvTree_Find(Tree, TAG_CURRENCY, 0);
	if(node != NULL)
		memcpy(&BatchRecord->Currency, TlvTree_GetData(node), LTAG_UINT8);

	// entry mode
	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	if(node != NULL)
		memcpy(&BatchRecord->EntryMode, TlvTree_GetData(node), LTAG_UINT8);

	// entry mode
	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	if(node != NULL)
		memcpy(&BatchRecord->EntryModeOriginal, TlvTree_GetData(node), LTAG_UINT8);

	// add fallback
	node = TlvTree_Find(Tree, TAG_FALLBACK, 0);
	if(node != NULL)
		memcpy(&BatchRecord->Fallback, TlvTree_GetData(node), LTAG_UINT8);

	// add base amount
	node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->BaseAmount, TlvTree_GetData(node), LTAG_UINT64);

	// add tax base amount
	node = TlvTree_Find(Tree, TAG_TAX_AMOUNT, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->TaxAmount, TlvTree_GetData(node), LTAG_UINT64);

	// add tip base amount
	node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->TipAmount, TlvTree_GetData(node), LTAG_UINT64);

	// add original total
	node = TlvTree_Find(Tree, TAG_ORIGINAL_TOTAL, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->OriginalTotal, TlvTree_GetData(node), LTAG_UINT64);

	// add STAN
	node = TlvTree_Find(Tree, TAG_STAN, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->STAN, TlvTree_GetData(node), LTAG_UINT32);

	// add date time
	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->DateTime, TlvTree_GetData(node), TlvTree_GetLength(node));

	// add date time reverse
	node = TlvTree_Find(Tree, TAG_TIME_REVERSE, 0);
	if(node != NULL)
		memcpy((char*)&BatchRecord->DateTimeReverse, TlvTree_GetData(node), TlvTree_GetLength(node));


	// add host response code
	node = TlvTree_Find(Tree, TAG_HOST_RESPONSE_CODE, 0);
	if(node != NULL)
		memcpy(BatchRecord->HostResponseCode, TlvTree_GetData(node), L_RESPONSE_CODE);

	// add host auth number
	node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
	if(node != NULL)
		memcpy(BatchRecord->HostAuthNumber, TlvTree_GetData(node), L_AUTH_NUMBER);

	// add host rrn
	node = TlvTree_Find(Tree, TAG_HOST_RRN, 0);
	if(node != NULL)
		memcpy(BatchRecord->HostRRN, TlvTree_GetData(node), L_RRN);

	//agregar cedula al batch
	node = TlvTree_Find(Tree, TAG_ID_NUMBER, 0);
	if(node != NULL)
		memcpy((char*)BatchRecord->Idnumber, TlvTree_GetData(node),IDNUMBER);

	node = TlvTree_Find(Tree, TAG_EMV_APP_LBL, 0);
	if(node != NULL)
		memcpy((char*)BatchRecord->APP_LBL, TlvTree_GetData(node),TlvTree_GetLength(node));

	//TAG
	node = TlvTree_Find(Tree, TAG_ISSUER_SCRIPT_RESULT, 0);
	if(node != NULL)
		memcpy(BatchRecord->EWL_TAG_ISSUER_SCRIPT, TlvTree_GetData(node),ISSUER_SCRIPT_LEN);

	//agregar numero de batch
//	node = TlvTree_Find(Tree, TAG_BATCH_NUMBER, 0);
//	if(node != NULL)
//		memcpy((char *)BatchRecord->NBatch, TlvTree_GetData(node),LTAG_UINT32);
	node = TlvTree_Find(Tree, TAG_TRACK2, 0);
	if(node != NULL)
			memcpy((char*)&BatchRecord->TRACK2, TlvTree_GetData(node),TlvTree_GetLength(node));

	// save emv tags form fisr generate
	node = TlvTree_Find(Tree, TAG_EMV_1GEN_TAGS, 0);
	if(node != NULL)
	{
		BatchRecord->EMV_1GEN_TAGS_LEN = TlvTree_Serialize(node, TLV_TREE_SERIALIZER_DEFAULT, BatchRecord->EMV_1GEN_TAGS, L_EMV_1GEN_TAGS);
		CHECK(BatchRecord->EMV_1GEN_TAGS_LEN > 0, LBL_ERROR);
	}

	// save emv tags for print
	node = TlvTree_Find(Tree, TAG_EMV_PRINT_TAGS, 0);
	if(node != NULL)
	{
		BatchRecord->EMV_PRINT_TAGS_LEN = TlvTree_Serialize(node, TLV_TREE_SERIALIZER_DEFAULT, BatchRecord->EMV_PRINT_TAGS, LZ_EMV_PRINT_TAGS);
		CHECK(BatchRecord->EMV_PRINT_TAGS_LEN > 0, LBL_ERROR);
	}


	// pin online was requested
	Tlv_GetTagValue(Tree, TAG_TRAN_REQ_PIN_ONLINE, &BatchRecord->PinOnReq, NULL);

	// pin offline was requested
	Tlv_GetTagValue(Tree, TAG_TRAN_REQ_PIN_OFFLINE, &BatchRecord->PinOffReq, NULL);

	// Signature was requested
	Tlv_GetTagValue(Tree, TAG_TRAN_REQ_SIGNATURE, &BatchRecord->SignReq, NULL);

	// add Server number
	Tlv_GetTagValue(Tree, TAG_SERVER_NUMBER, &BatchRecord->Server, NULL);

	Tlv_GetTagValue(Tree, TAG_VOID_ORIGINAL_RECORD, &BatchRecord->VoidOriginalRecord, NULL);

	Tlv_GetTagValue(Tree, TAG_ACCOUNT_TYPE, &BatchRecord->AccountType, NULL);

	if(BatchRecord->TranID == TRAN_C2P)
	{
		Tlv_GetTagValue(Tree, TAG_BANK_TYPE, &BatchRecord->TranBank, NULL);
	}

	return BATCH_RET_OK;

LBL_ERROR_MANDATORY_TAG:
	return BATCH_RET_NOK;

LBL_ERROR:
	return BATCH_RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_FindTransaction
 * Description:		find transaction in all batch files
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - uint32 Invoice - transaction invoice
 * Return:
 *  BATCH_RET_OK
 *  BATCH_RET_CANNOT_FIND_RECORD
 * Notes:
 */
int Batch_FindTransaction(TLV_TREE_NODE Tree, uint32 Reference)
{
	BATCH BatchRecord;
	TABLE_MERCHANT Merchant;
	int RetFun;

	// Merchant table loop
	// find first Merchant
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	do
	{
		// Batch file loop
		// find first transaction in batch file
		RetFun = TM_FindFirst(Merchant.BatchName, &BatchRecord);
		if(RetFun == TM_RET_OK)
		{
			do
			{
				// verify if is correct invoice
				if(BatchRecord.STAN == Reference)
				{
					// yes, fill tree and exit
					RetFun = Batch_Get_Transaction_Data(Tree, &BatchRecord);
					return BATCH_RET_OK;
				}
				// no, find next transaction
				RetFun = TM_FindNext(Merchant.BatchName, &BatchRecord);
			}while(RetFun == TM_RET_OK);
			//+ NORECORDS @mamata 6/05/2016
			CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
			//- NORECORDS
		}
		// find next Merchant
		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun == TM_RET_OK);

LBL_ERROR:
	return BATCH_RET_CANNOT_FIND_RECORD;
}

/* --------------------------------------------------------------------------
 * Function Name:	Batch_FindServer
 * Description:		find Server in all batch files
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put Server data
 *  - uint32 Invoice - transaction IdMesero
 * Return:
 *  BATCH_RET_OK
 *  BATCH_RET_CANNOT_FIND_RECORD
 * Notes:
 */
int Batch_FindServer(TLV_TREE_NODE Tree, uint32 Mesero)
{
	BATCH BatchRecord;
	TABLE_MERCHANT Merchant;
	int RetFun;

	// Merchant table loop
	// find first Merchant
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	do
	{
		// Batch file loop
		// find first transaction in batch file
		RetFun = TM_FindFirst(Merchant.BatchName, &BatchRecord);
		if(RetFun == TM_RET_OK)
		{
			do
			{
				// verify if is correct invoice
				if(BatchRecord.Server == Mesero)
				{
					// yes, fill tree and exit
					//RetFun = Batch_Get_Transaction_Data(Tree, &BatchRecord);
					return BATCH_RET_OK;
				}
				// no, find next transaction
				RetFun = TM_FindNext(Merchant.BatchName, &BatchRecord);
			}while(RetFun == TM_RET_OK);
			//+ NORECORDS @mamata 6/05/2016
			CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
			//- NORECORDS
		}
		// find next Merchant
		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun == TM_RET_OK);

LBL_ERROR:
	return BATCH_RET_CANNOT_FIND_RECORD;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_DeleteSettled
 * Description:		Delete settled batch files
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Batch_DeleteSettled(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;

	// find first merchant
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	do
	{
		// if merchant was settle?
		if(Merchant.BatchSettle)
		{
			// yes
			// delete batch
			RetFun = TM_DeleteTable(Merchant.BatchName);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);


			// crete new batch
			RetFun = TM_CreateTable(Merchant.BatchName, L_BATCH);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);

			RetFun = TM_DeleteTable(TAB_MESERO);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);

			RetFun = TM_CreateTable(TAB_MESERO, L_TABLE_MESERO);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);


			// update merchant info
			Merchant.BatchSettle = FALSE;
					if(Merchant.BatchNumber == 9999)
						Merchant.BatchNumber = 1;
					else
						Merchant.BatchNumber++;
			RetFun = TM_ModifyRecord(TAB_MERCHANT, &Merchant);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		}

		// find next merchant
		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun == TM_RET_OK);
	//+ NORECORDS @mamata 6/05/2016
	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Batch_DeleteAllBatch
 * Description:		Delete all batch files
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Batch_DeleteAllBatch(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;

	// find first merchant
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	do
	{
		// delete batch
		RetFun = TM_DeleteTable(Merchant.BatchName);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// crete new batch
		RetFun = TM_CreateTable(Merchant.BatchName, L_BATCH);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// update merchant info
		Merchant.BatchSettle = FALSE;
				if(Merchant.BatchNumber == 9999)
					Merchant.BatchNumber = 1;
				else
					Merchant.BatchNumber++;
		RetFun = TM_ModifyRecord(TAB_MERCHANT, &Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// find next merchant
		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun == TM_RET_OK);
	//+ NORECORDS @mamata 6/05/2016
	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


//+ BATCH01 @mamata Aug 15, 2014
/* --------------------------------------------------------------------------
 * Function Name:	SaveBatchsInformation
 * Description:		Save current batchs information in tlvtree
 * Parameters:
 *  - TLV_TREE_NODE BatchsInfo - where to put data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SaveBatchsInformation(TLV_TREE_NODE BatchsInfo)
{
	TABLE_MERCHANT Merchant;
	TLV_TREE_NODE node;
	BATCH_INFO CurrentBatch;

	int RetFun;

	// verify if tree is no null
	CHECK(BatchsInfo != NULL, LBL_ERROR);

	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	do
	{
		memset(&CurrentBatch, 0, sizeof(BATCH_INFO));

		CurrentBatch.MerchantID = Merchant.Header.RecordID;
		memcpy(CurrentBatch.TID, Merchant.TID, L_MERCH_TID);
		memcpy(CurrentBatch.MID, Merchant.MID, L_MERCH_LID);
		CurrentBatch.BatchNumber = Merchant.BatchNumber;

		node = TlvTree_AddChild(BatchsInfo, 1, &CurrentBatch, sizeof(BATCH_INFO));
		CHECK(node != NULL, LBL_ERROR);


		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun == TM_RET_OK);

	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetCurrentBatchNumber
 * Description:		get the current batch number of specific merchant
 * Parameters:
 *  - char *TID
 *  - char*MID
 *  - TLV_TREE_NODE BatchsInfo
 * Return:
 * - (0) - cannot fond merchant
 * other - current batch number
 * Notes:
 */
uint32 GetCurrentBatchNumber(char *TID, char*MID, TLV_TREE_NODE BatchsInfo)
{
	TLV_TREE_NODE node;
	int Index = 0;
	uint32 RetValue = 0;
	BATCH_INFO *CurrentBatch;

	 node = TlvTree_Find(BatchsInfo, 1, Index);
	 CHECK(node != NULL, LBL_FINISH);
	 do
	 {
		 CurrentBatch = TlvTree_GetData(node);

		 if(	memcmp(TID, CurrentBatch->TID, LZ_MERCH_TID) == 0
			&&	memcmp(MID, CurrentBatch->MID, LZ_MERCH_LID) == 0
		   )
		 {
			 RetValue = CurrentBatch->BatchNumber;
			 return RetValue;
		 }

		 Index++;
		 node = TlvTree_Find(BatchsInfo, 1, Index);
	 }while(node != NULL);

LBL_FINISH:
	return RetValue;
}
//- BATCH01

//+ REPRINT_TBK01 @jbotero 30/04/2015
/* --------------------------------------------------------------------------
 * Function Name:	Batch_Get_Last_Approved_Transaction
 * Description:		Get last transaction approved of batch
 * Parameters:
 *  - TLV_TREE_NODE Tree - where to put transaction data
 *  - uint8 *BatchName - name of batch file
 * Return:
 *  BATCH_RET_OK - last transaction ok
 *  BATCH_RET_NOK - cannot open last transaction
 *  BATCH_RET_EMPTY_FILE - no transactions in batch file
 * Notes:
 */
int Batch_Get_Last_Approved_Transaction(TLV_TREE_NODE Tree, uint8 *BatchName)
{
	BATCH BatchRecord;
	int RetFun;
	int i = 0;

	// try to get last transaction
	memset((char*)&BatchRecord, 0, L_BATCH);
	RetFun = TM_GetLastRecord((char*)BatchName, &BatchRecord);
	CHECK(RetFun == TM_RET_OK, LBL_TAB_ERROR);

	// verify if record is file header
	CHECK(BatchRecord.Header.FileIndex != 0, LBL_EMPTY_FILE);

	i = BatchRecord.Header.FileIndex;

	do
	{
		RetFun = TM_GetDirectRecord((char*)BatchName, &BatchRecord, i);

		//Check that records isn't a Reverese or Deleted
		if(!(BatchRecord.TranStatus & FSTATUS_NEED_REVERSE) &&
				BatchRecord.Header.Deleted == FALSE)
		{
			// get transaction data
			RetFun = Batch_Get_Transaction_Data(Tree, &BatchRecord);
			CHECK(RetFun == RET_OK, LBL_TAB_ERROR);
			break;
		}

		i--;

	} while( i > 0 );

	// verify if record is file header
	CHECK(i != 0, LBL_EMPTY_FILE);

	return BATCH_RET_OK;

LBL_TAB_ERROR:
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		return BATCH_RET_EMPTY_FILE;
	else
		return BATCH_RET_NOK;

LBL_EMPTY_FILE:
	return BATCH_RET_EMPTY_FILE;
}
//- REPRINT_TBK01

int Batch_FindTransactionByRecordID(TLV_TREE_NODE Tree, int Record)
{
	BATCH BatchRecord;
	TABLE_MERCHANT Merchant;
	int RetFun;

	// Merchant table loop
	// find first Merchant
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	RetFun = TM_FindRecord(Merchant.BatchName, &BatchRecord, Record);
	if(RetFun == TM_RET_OK)
	{
		// yes, fill tree and exit
		RetFun = Batch_Get_Transaction_Data(Tree, &BatchRecord);
		Tlv_SetTagValueInteger(Tree, TAG_VOID_ORIGINAL_RECORD, BatchRecord.Header.RecordID, sizeof(int));
		return BATCH_RET_OK;
	}

LBL_ERROR:
	return BATCH_RET_CANNOT_FIND_RECORD;
}

