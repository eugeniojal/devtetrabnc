//+ INGESTATE01 @jbotero 13/09/2015
/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			IngestateComms.c
 * Header file:		IngestateComms.h
 * Description:
 * Author:			@dvelazco, @jbotero
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/

#define INGELOAD_SSL_PROFILE "INGESTATE"

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
static const char g_pcExtendedStructTag[3]     = { 0x01, 0x02, 0x03 };

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	_FUN_GetIpAddress
 * Description:		Parse the address to ip and port
 * Author:			@dvelazco
 * Parameters:
 * Return:			1 if OK; 0 on error
 * Notes:
 */

static int GetIpAddress(const char *addr, unsigned int *o_pnAddress, unsigned int *o_pipPort)
{
    unsigned int ipAddr=0;
    unsigned int i=0, j=0;
    unsigned char c;
    char tmp[4];

    while (((c = addr[i++]) != 0) && (c != ':'))
    {
        if (c == '.')
        {
            ipAddr = (ipAddr << 8) + atoi(tmp);
            j = 0;
        }
        else
        {
            tmp[j++] = c;
            tmp[j] = '\0';
        }
    }

    if (c == ':')
    {
        *o_pnAddress = (ipAddr << 8) + atoi(tmp);
        *o_pipPort = atoi(&addr[i]);

        return 1;
    }

    return 0;
}

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_PerformRemote
 * Description:		Execute API for Ingestate Cnx
 * Author:			@dvelazco modified @jbotero
 * Parameters:		bool autoRestart: TRUE(Autorestart) or FALSE(No restart)
 * Return:
 * Notes:
 */

int Ingestate_PerformRemote(bool autoRestart)
{
	int RetFun = 0;
	unsigned int ipAddr;
	unsigned int ipPort;

	S_PARAM_TLCHGT Params;

	char szAddress[256];

	int L_szAddress = 0;

	memset(szAddress, 0, sizeof(szAddress));

	memset( &Params, 0, sizeof( S_PARAM_TLCHGT ));

	// Create t_noappel Data xxx.xxx.xxx.xxx:yyyy
	L_szAddress = strlen(tConf.iS_Host);
	memcpy(szAddress, tConf.iS_Host, L_szAddress); // add xxx.xxx.xxx.xxx
	memcpy(szAddress+L_szAddress, ":", 1);  // add :
	L_szAddress += 1;
	memcpy(szAddress+L_szAddress, tConf.iS_Port, strlen(tConf.iS_Port)); // add yyyy

	CHECK(GetIpAddress(szAddress, &ipAddr, &ipPort) == 1, LBL_ERROR);

	//	Params.ip = Telium_inet_addr(tConf.iS_Host); // TMS IP address // con esta funcion no funciona la conexion Dial, se cambia por la funcion _FUN_GetIpAddress
	Params.type_modem = VIP;
	Params.ip = ipAddr; // TMS IP address
	Params.port = ipPort; // TMS IP
	memcpy(Params.Adress, szAddress, sizeof(szAddress));

	// Contract number. Pad with null.
	memset( Params.t_nocontrat, 0, sizeof( Params.t_nocontrat ));
	memcpy( Params.t_nocontrat, tConf.iS_ID, strlen(tConf.iS_ID)); // Set ID

	// Tag for extended structure.
	memcpy( Params.t_nologiciel, g_pcExtendedStructTag, 3 );

	//Map Communication Type
	{
		if(tConf.iS_CommType == LLC_DIAL)
			Params.reseau = TMSIPRTC;

		else if (tConf.iS_CommType == LLC_IP || tConf.iS_CommType == LLC_WIFI)
			Params.reseau = TMSIP;

		else if (tConf.iS_CommType == LLC_GPRS )
			Params.reseau = TMSGPRS;
		else
			Params.reseau = RTC;
	}

	// Set the other parameters according to the remote type.
	switch( Params.reseau  )
	{
		case TMSGPRS:
			strcpy( Params.t_noappel, szAddress );

			strcpy( (char*)Params.apn,   tConf.GPRS_APN );			// GPRS APN.
			strcpy( (char*)Params.login, tConf.GPRS_User );			// GPRS Login.
			strcpy( (char*)Params.password, tConf.GPRS_Pass );		// GPRS Password.
			break;

		case TMSIP:
			strcpy( Params.t_noappel, szAddress );
			break;

		case TMSIPRTC:
			L_szAddress = 1;
			memset(szAddress, 0, sizeof(szAddress));
			memcpy(szAddress, "T", L_szAddress);
			memcpy(szAddress+L_szAddress, tConf.iS_Phone, strlen(tConf.iS_Phone));

			strcpy( Params.t_noappel, szAddress ); // ISP PhoneNumber.
			strcpy( (char*)Params.login,  tConf.iS_PPP_User );		// ISP Login.
			strcpy( (char*)Params.password, tConf.iS_PPP_Pass );	// ISP Password.
			break;

		case RTC:
			sprintf( Params.t_noappel, "T%s", tConf.iS_Phone );
			break;
	}

	RetFun = remote_download ( &Params );
	DisplayHeader(_ON_);

	if (RetFun == REMOTE_CALL_SUCCESSFULL)
		RetFun = Ingestate_ProcessPAR();
	else
	{
		UI_ShowMessage(	mess_getReturnedMessage(MSG_INGESTATE),
		"",
		mess_getReturnedMessage(MSG_ERROR_INGESTATE),
		GL_ICON_ERROR,
		GL_BUTTON_ALL,
		tConf.TimeOut);
	}

	if (autoRestart && RetFun == RET_OK)
		RebootTerminal();

	return RetFun;

LBL_ERROR:
	return RET_NOK;
}
//- INGESTATE01
