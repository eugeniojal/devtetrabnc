/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Tran_VOID.c
 * Header file:		Tran_VOID.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
uint16 TRAN_STEPS_VOID[] =
{
		STEP_CHECK_TERMINAL,
		STEP_SELECT_MERCH_GROUP,
		STEP_SELECT_MERCHANT_BY_TRAN,

		STEP_INVOICE,

		STEP_START_PAYMENT, 		//EWL, set entry supported entry modes
		STEP_EWL_START,				//EWL, Initialize handle, set parameters EMV/CLESS
		STEP_WAIT_CARD,				//EWL, wait card and read initial data
		STEP_MANUAL_CARD_ENTRY,
		STEP_MANUAL_EXP_DATE,
		STEP_CVV2,

		STEP_COMPARE_READ_PAN,
		STEP_CLESS_VALIDATE_CARD,

		STEP_CONFIRM_VOID,
		STEP_ENTER_ID_NUMBER,
		STEP_PREDIAL,
		STEP_MERCHANT_PASSWORD,

		STEP_SEND_RECEIVE,
		STEP_END_COMMUNICATION,
		STEP_PROCESS_RESPONSE,
		STEP_PRINT_RECEIPT,
		STEP_END_LIST
};

