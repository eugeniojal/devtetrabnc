//#include "ewlDemo.h"
//#include "data.h"
//#include "util.h"
//#include "paymentTools.h"
#include "iPOSApp.h"

#define REMOVE_END(x) (x - 1)

/* --------------------------------------------------------------------------
 * Function Name:	dataRecoverMagneticCard
 * Description:		Recover data from Magnetic Card
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- RET_OK: OK
 * 	- RET_NOK: Error
 * Notes:
 */

int dataRecoverMagneticCard (TLV_TREE_NODE Tree)
{

	uint8 TrackBuffer[200];
	uint8 PanLen;
	int RetFun;

	//--- Get Track 1
	memset(TrackBuffer, 0, sizeof(TrackBuffer));
	PanLen = 0;
	RetFun = Telium_Is_iso1(utilTrack1(), &PanLen, TrackBuffer);

	if( RetFun == ISO_OK || RetFun == DEF_LUH )
		RetFun = Tlv_SetTagValue(Tree, TAG_TRACK1, TrackBuffer, strlen((char*)TrackBuffer) );

	//--- Get Track 2
	memset(TrackBuffer, 0, sizeof(TrackBuffer));
	PanLen = 0;
	RetFun = Telium_Is_iso2(utilTrack2(), &PanLen, TrackBuffer);

	if( RetFun == ISO_OK || RetFun == DEF_LUH )
		RetFun = Tlv_SetTagValue(Tree, TAG_TRACK2, TrackBuffer, strlen((char*)TrackBuffer) );

	//read track Fail
	if(RetFun != RET_OK)
	{
		UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_CARD_READ), "", GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	    return RET_NOK;
	}

    return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name: dataCheckExpDateEmvApp
 * Description: Validate expiration date from an EMV card
 * Author:		@gmunar
 * Date:        18/02/2016
 * Parameters: 	Expiration date (in 5F24 tag format)
 * Return:
 *  - RET_OK	Application not expired
 *  - RET_NOK   Application expired, or date  can be recovered from Manager
 * Notes:
 */

int dataCheckExpDateEmvApp(TLV_TREE_NODE Tree)
{

	DATE_EXT FechaHoy;
	int RetFun;
	uint8 FechaExp[3 + 1];
	uint Valor;

	memset(FechaExp, 0x00, sizeof(FechaExp));
	RetFun = Tlv_GetTagValue(Tree, TAG_EXP_DATE, FechaExp, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	RetFun = Telium_Getdate(&FechaHoy);
	CHECK(RetFun == 0, LBL_ERROR);

	//Validate Year
	Valor = 2000 + FechaExp[0];
	CHECK(Valor >= FechaHoy.year, LBL_TARJETA_VENCIDA)

	if (Valor == FechaHoy.year)
	{
		Valor = FechaExp[1];
		CHECK(Valor >= FechaHoy.month, LBL_TARJETA_VENCIDA)

		if (Valor == FechaHoy.month)
		{
			Valor = FechaExp[2];
			CHECK(Valor >= FechaHoy.day, LBL_TARJETA_VENCIDA)
		}
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

LBL_TARJETA_VENCIDA:
	UI_ShowMessage("CHIP", " ", mess_getReturnedMessage(MSG_ERROR_EXPIRED_CARD), GL_ICON_INFORMATION,
			GL_BUTTON_ALL, tConf.TimeOut);

	return RET_NOK;

}

/* --------------------------------------------------------------------------
 * Function Name:	dataRecoverGetData
 * Description:		Recover data from CONTACT/CONTACLESS cards
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- RET_OK: OK
 * 	- RET_NOK: Error
 * Notes:
 */
int dataRecoverGetData (TLV_TREE_NODE Tree)
{
	ewlObject_t *ewl;
	int RetFun;

	char pan[TRANSACTION_PAN_ASCII_LEN + 1];
	char CardHolderName[EWL_EMV_CARDHOLDER_NAME_MAX_LEN + 1];
	char track1[TRANSACTION_TRACK1_ASCII_LEN + 1];
	char track2[TRANSACTION_TRACK2_ASCII_LEN + 1];
	char expiration[TRANSACTION_EXPIRATION_ASCII_LEN + 1];
	bool hasSpendingAmount = FALSE;
	ewlTechnology_t technology;

	char ServiceCode[TRANSACTION_SERVICECODE_ASCII_LEN + 1];
	char dircretionary[EWL_EMV_TRACK_1_DISCRET_DATA_MAX_LEN + 1];

	ewl = EWL_GetEmvHandle();
	CHECK(ewl != 0, LBL_ERROR);

	//--- technology
	RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_CARD_TECHNOLOGY,
			ewl, &technology, EWL_TAG_TECHNOLOGY_LEN,EWL_TAG_TECHNOLOGY);
	CHECK(RetFun >= 0, LBL_ERROR);

	//--- PAN
	memset(pan, 0x00, sizeof(pan));
	RetFun = utilGetPanASCII(ewl, pan, TRANSACTION_PAN_ASCII_LEN);
	CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);
	RetFun = Tlv_SetTagValue(Tree, TAG_PAN, pan, strlen(pan));
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Expiration date
	memset(expiration, 0x00, sizeof(expiration));
	RetFun = utilGetExpirationYYMM(ewl, expiration,TRANSACTION_EXPIRATION_ASCII_LEN);
	if((RetFun < 0) && (RetFun != EWL_ERR_TAG_NOT_FOUND)) return RetFun;
	RetFun = Tlv_SetTagValue(Tree, TAG_EXP_DATE, expiration, strlen((char*)expiration));
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Card Holder name
	memset(CardHolderName, 0x00, sizeof(CardHolderName));
	RetFun = Tlv_EwlGetParameter(Tree, TAG_CARDHOLDER_NAME,
			ewl, CardHolderName, EWL_EMV_CARDHOLDER_NAME_MAX_LEN,EWL_EMV_CARDHOLDER_NAME);
	CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

	memset(track1, 0x00, sizeof(track1));
	RetFun = Tlv_EwlGetParameter(Tree, TAG_TRACK1,
			ewl, track1,REMOVE_END(TRANSACTION_TRACK1_ASCII_LEN),EWL_TAG_TRACK1_DATA);
	CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

	memset(ServiceCode, 0x00, sizeof(ServiceCode));
	RetFun = utilGetServiceCodeAscii(ewl, ServiceCode, TRANSACTION_SERVICECODE_ASCII_LEN);
	if((RetFun < 0) && (RetFun != EWL_ERR_TAG_NOT_FOUND)) return RetFun;

	memset(dircretionary, 0x00, sizeof(dircretionary));
	RetFun = ewlGetParameter(ewl,dircretionary,EWL_EMV_TRACK_1_DISCRET_DATA_MAX_LEN, EWL_EMV_TRACK_1_DISCRET_DATA);
	if((RetFun < 0) && (RetFun != EWL_ERR_TAG_NOT_FOUND)) return RetFun;

	memset(track2, 0x00, sizeof(track2));
	RetFun = Tlv_EwlGetParameter(Tree, TAG_TRACK2,
			ewl, track2,REMOVE_END(TRANSACTION_TRACK2_ASCII_LEN),EWL_TAG_TRACK2_DATA);
	CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

	if( strlen( pan ) == 0 && strlen(track2) > 0 )
	{
		char * p = NULL;
		int cont = 0;
		//---Process Track 2

		//Get PAN
		p = strtok(track2 + cont, "=");
		strcpy(pan, p);
		cont += strlen(p) + 1;
		RetFun = Tlv_SetTagValue(Tree, TAG_PAN, pan, strlen(pan));
		CHECK(RetFun == RET_OK, LBL_ERROR);

		//Get Expiration Date
		if(strlen((char*)expiration) == 0)
		{
			memset(expiration, 0x00, sizeof(expiration));
			//Ascii_to_Hex((uint8*)expiration, (uint8*) track2 + cont, 4);
			memcpy(expiration, (uint8*) track2 + cont, 4);
			cont += 4;

			RetFun = Tlv_SetTagValue(Tree, TAG_EXP_DATE, expiration, strlen((char*)expiration));
			CHECK(RetFun == RET_OK, LBL_ERROR);
		}

		//Get Service Code
		if(strlen((char*)ServiceCode) == 0)
		{
			memset(ServiceCode, 0x00, sizeof(ServiceCode));
			memcpy(ServiceCode, (uint8*) track2 + cont, 3);
			cont += 3;

			RetFun = Tlv_SetTagValue(Tree, TAG_SERVICE_CODE, ServiceCode, strlen((char*)ServiceCode));
			CHECK(RetFun == RET_OK, LBL_ERROR);
		}
	}

	//<!--(Optional) Build Track 1 on Chip Cards. Comment lines if is not needed.
	if( strlen(track1) <= 0 )
	{
		if(expiration[0] == NULL)
			memset(expiration, ' ', sizeof(expiration) - 1);

		if(ServiceCode[0] == NULL)
			memset(ServiceCode, ' ', sizeof(ServiceCode) - 1);

		if(CardHolderName[0] == NULL)
			memset(CardHolderName, ' ', sizeof(CardHolderName) - 1);

		sprintf(track1,"B%.19s^%.26s^%.4s%.3s",pan, CardHolderName, expiration, ServiceCode);

		if((strlen(track1) + strlen(dircretionary)) > (sizeof(track1) - 1))
			return RET_NOK;

		strcat(track1, dircretionary);

		RetFun = Tlv_SetTagValue(Tree, TAG_TRACK1, track1, strlen((char*)track1));
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}
	//(Optional)-->

	RetFun = Tlv_EwlGetParameter(Tree, TAG_EMV_APP_LBL,
			ewl, NULL,EWL_TAG_LABEL_MAX_LEN,EWL_TAG_LABEL);
	CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

	RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_SPENDING_AMT,
			ewl, NULL,EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT_LEN,EWL_TAG_AVAILABLE_OFFLINE_SPENDING_AMOUNT);
	CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

	if(RetFun > 0)
	{
		hasSpendingAmount = TRUE;
		RetFun = Tlv_SetTagValue(Tree, TAG_TRAN_HAS_SPENDING_AMT, &hasSpendingAmount, sizeof(hasSpendingAmount));
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}

	return RET_OK;

	LBL_ERROR:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	dataInsertFisrtGenTags
 * Description:		Insert tags of fisrt generate to any Parent node
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE ParentNode: ParentNode to load data
 * Return:
 * 	- RET_OK: OK
 * 	- RET_NOK: Error
 * Notes:
 */
int dataInsertFisrtGenTags(TLV_TREE_NODE ParentNode)
{
	ewlObject_t *ewl = EWL_GetEmvHandle();

	int i = 0;
	int RetFun = 0;
	char * p = NULL;

	typedef struct
	{
		unsigned int TagId;
		unsigned int MaxLength;

	} EMV_TAGS;

	EMV_TAGS stEmvTag[] =
	{
	{	EWL_EMV_AIP, 							EWL_EMV_AIP_LEN							},
	{	EWL_EMV_DF_NAME, 						EWL_EMV_DF_NAME_MAX_LEN					},
	{	EWL_EMV_TVR, 							EWL_EMV_TVR_LEN							},
	{	EWL_EMV_TRANSACTION_DATE, 				EWL_EMV_TRANSACTION_DATE_LEN			},
	{	EWL_EMV_TRANSACTION_TYPE, 				EWL_EMV_TRANSACTION_TYPE_LEN			},
	{	EWL_EMV_TRANSACTION_CURRENCY_CODE,      EWL_EMV_TRANSACTION_CURRENCY_CODE_LEN	},
	{	EWL_EMV_APPLI_PAN_SEQUENCE_NUMBER,      EWL_EMV_APPLI_PAN_SEQUENCE_NUMBER_LEN	},
	{	EWL_EMV_AMOUNT_AUTH_NUM,                EWL_EMV_AMOUNT_AUTH_NUM_LEN				},
	{	EWL_EMV_AMOUNT_OTHER_NUM,               EWL_EMV_AMOUNT_OTHER_NUM_LEN			},
	{	EWL_EMV_APPLI_VERSION_NUMBER_TERM,      EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN,	},
	{	EWL_EMV_ISSUER_APPLI_DATA,              EWL_EMV_ISSUER_APPLI_DATA_MAX_LEN,      },
	{	EWL_EMV_TERMINAL_COUNTRY_CODE,          EWL_EMV_TERMINAL_COUNTRY_CODE_LEN,      },
	{	EWL_EMV_IFD_SERIAL_NUMBER,              EWL_EMV_IFD_SERIAL_NUMBER_LEN,          },
	{	EWL_EMV_CRYPTOGRAM,         			EWL_EMV_CRYPTOGRAM_LEN,					},
	{	EWL_EMV_CID,           					EWL_EMV_CID_LEN,						},
	{	EWL_EMV_TERMINAL_CAPABILITIES,          EWL_EMV_TERMINAL_CAPABILITIES_LEN,      },
	{	EWL_EMV_CVM_RESULTS,                    EWL_EMV_CVM_RESULTS_LEN,                },
	{	EWL_EMV_TERMINAL_TYPE,                  EWL_EMV_TERMINAL_TYPE_LEN,              },
	{	EWL_EMV_ATC,                            EWL_EMV_ATC_LEN,                        },
	{	EWL_EMV_UNPREDICTABLE_NUMBER,           EWL_EMV_UNPREDICTABLE_NUMBER_LEN,       },
	{	EWL_EMV_TRANSACTION_SEQUENCE_COUNTER,   EWL_EMV_TRANSACTION_SEQUENCE_COUNTER_MAX_LEN},
	{	EWL_EMV_APPLI_CURRENCY_CODE,            EWL_EMV_APPLI_CURRENCY_CODE_LEN,        },
	{	EWL_EMV_TRANSACTION_CATEGORY_CODE,		EWL_EMV_TRANSACTION_CATEGORY_CODE_LEN	},

	//<!--Additional Paywave tags
	{	EWL_PAYWAVE_ISSUER_SCRIPT_RESULT,		EWL_PAYWAVE_ISSUER_SCRIPT_RESULT_MAX_LEN},
	{	EWL_PAYWAVE_FORM_FACTOR_INDICATOR,		EWL_PAYWAVE_FORM_FACTOR_INDICATOR_LEN	},

	//<!--Additional Paypass tags
	{	EWL_PAYPASS_DSDOL,						EWL_PAYPASS_DS_ODS_INFO_LEN				},
	{	EWL_PAYPASS_THIRD_PARTY_DATA,			EWL_PAYPASS_THIRD_PARTY_DATA_MAX_LEN	},

	//<!--Additional Paypass tags
	{	EWL_EXPRESSPAY_TRANSACTION_CAPABILITIES,EWL_EXPRESSPAY_TRANSACTION_CAPABILITIES_LEN},

	//<!--Before that first Generate
	{	EWL_TAG_LABEL,							EWL_TAG_LABEL_MAX_LEN					},
	{	EWL_EMV_APPLICATION_LABEL,				EWL_EMV_APPLICATION_LABEL_MAX_LEN		},
	{	EWL_EMV_AID_CARD,						EWL_EMV_AID_CARD_MAX_LEN				},
	{	EWL_EMV_CARDHOLDER_NAME,				EWL_EMV_CARDHOLDER_NAME_MAX_LEN			},
	{	EWL_EMV_ISSUER_COUNTRY_CODE,            EWL_EMV_ISSUER_COUNTRY_CODE_LEN,        },
	{	EWL_EMV_EXPIRATION_DATE,            	EWL_EMV_EXPIRATION_DATE_LEN,        	},
	//EUGENIO TAGS AGREGADOS
	{	EWL_EMV_TSI,          	                EWL_EMV_TSI_LEN,                     	},
	//Add or remove your tags for application
	{ 0, 0},
	};

	while( stEmvTag[i].TagId !=  0 )
	{
		p = umalloc(sizeof(char) * stEmvTag[i].MaxLength);
		CHECK(p!= NULL, LBL_ERROR);

		RetFun = Tlv_EwlGetParameter( ParentNode, stEmvTag[i].TagId,
				ewl, p, stEmvTag[i].MaxLength, stEmvTag[i].TagId);

		if(p != NULL)
			ufree(p);

		if(RetFun != EWL_ERR_TAG_NOT_FOUND)
		{
			CHECK(RetFun >= 0, LBL_ERROR);
		}

		i++;
	}

	return RET_OK;

LBL_ERROR:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	dataRecoverGoOnChip
 * Description:		Recover data after EWL callback
 * Author:			@jbotero
 * Parameters:
 * 	- ewlObject_t *ewl: Handle of EWL
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- RET_OK: OK
 * 	- RET_NOK: Error
 * Notes:
 */
int dataRecoverGoOnChip ( ewlObject_t *ewl, TLV_TREE_NODE Tree)
{

    int RetFun;
    TLV_TREE_NODE node;
	uint8 ValueNode = 1;

    RetFun = Tlv_EwlGetParameter( Tree, TAG_TRAN_REQ_PIN_ONLINE,
    		ewl, NULL, EWL_TAG_REQUESTED_PIN_ONLINE_LEN,EWL_TAG_REQUESTED_PIN_ONLINE);
    CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_REQ_PIN_OFFLINE,
    		ewl, NULL, EWL_TAG_REQUESTED_PIN_OFFLINE_LEN,EWL_TAG_REQUESTED_PIN_OFFLINE);
    CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_REQ_SIGNATURE,
    		ewl, NULL, EWL_TAG_REQUESTED_SIGNATURE_LEN,EWL_TAG_REQUESTED_SIGNATURE);
    CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_CRYPTOGRAM,
    		ewl, NULL, EWL_EMV_CRYPTOGRAM_LEN,EWL_EMV_CRYPTOGRAM);
    CHECK(RetFun >= 0, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_TVR,
    		ewl, NULL, EWL_EMV_TVR_LEN,EWL_EMV_TVR);
    CHECK(RetFun >= 0, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_EMV_TRAN_STATUS,
    		ewl, NULL, EWL_TAG_TRANSACTION_STATUS_LEN,EWL_TAG_TRANSACTION_STATUS);
    CHECK(RetFun >= 0, LBL_ERROR);

    //--- Add all tags of First Generate to TAG_EMV_1GEN_TAGS

    //Parent 	-> TAG_EMV_1GEN_TAGS
    node = TlvTree_Find(Tree, TAG_EMV_1GEN_TAGS, 0);
    if(node != NULL)
    	TlvTree_Release(node);
    node = TlvTree_AddChild(Tree, TAG_EMV_1GEN_TAGS, &ValueNode, LTAG_UINT8 );
    CHECK(node != NULL, LBL_ERROR);

    //Children	-> All tags after first generate
    dataInsertFisrtGenTags(node);

    return RET_OK;

LBL_ERROR:

	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	dataRecoverAproveOffLine
 * Description:		Recover data after approved off-line Transaction
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- RET_OK: OK
 * 	- RET_NOK: Error
 * Notes:
 */
int dataRecoverAproveOffLine ( TLV_TREE_NODE Tree )
{
    ewlObject_t *ewl = EWL_GetEmvHandle();
    int RetFun;
    char authorizationCode[TRANSACTION_AUTHORIZATION_CODE_LEN];

    RetFun = Tlv_EwlGetParameter(Tree, TAG_HOST_RESPONSE_CODE,
    		ewl, NULL,EWL_EMV_AUTHORISATION_RESPONSE_CODE_LEN,EWL_EMV_AUTHORISATION_RESPONSE_CODE);
    CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

    sprintf(authorizationCode,"######");
    Tlv_SetTagValue(Tree, TAG_HOST_AUTH_NUMBER, authorizationCode, strlen(authorizationCode));

    return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	dataRecoverFinishChip
 * Description:		Recover data after second generate
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- RET_OK: OK
 * 	- RET_NOK: Error
 * Notes:
 */
int dataRecoverFinishChip ( TLV_TREE_NODE Tree )
{
    ewlObject_t *ewl = EWL_GetEmvHandle();
    int RetFun;

    RetFun = Tlv_EwlGetParameter( Tree, TAG_TRAN_CRYPTOGRAM,
    		ewl, NULL,EWL_EMV_CRYPTOGRAM_LEN,EWL_EMV_CRYPTOGRAM);
    CHECK(RetFun >= 0, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_TRAN_TVR,
    		ewl, NULL,EWL_EMV_TVR_LEN,EWL_EMV_TVR);
    CHECK(RetFun >= 0, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_EMV_TRAN_STATUS,
    		ewl, NULL,EWL_TAG_TRANSACTION_STATUS_LEN,EWL_TAG_TRANSACTION_STATUS);
    CHECK(RetFun >= 0, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_HOST_RESPONSE_CODE,
    		ewl, NULL,EWL_EMV_AUTHORISATION_RESPONSE_CODE_LEN,EWL_EMV_AUTHORISATION_RESPONSE_CODE);
    CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

    RetFun = Tlv_EwlGetParameter(Tree, TAG_ISSUER_SCRIPT_RESULT,
    		ewl, NULL,EWL_TAG_ISSUER_SCRIPT_RESULT_LEN,EWL_TAG_ISSUER_SCRIPT_RESULT);
    CHECK(RetFun >= 0 || RetFun == EWL_ERR_TAG_NOT_FOUND, LBL_ERROR);

    return RET_OK;

LBL_ERROR:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	dataGetChipCard
 * Description:		Read application of Chip card and get initial EMV tags
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- STM_RET_NEXT: OK
 * 	- STM_RET_ERROR: Error
 * Notes:
 */
int dataGetChipCard( TLV_TREE_NODE Tree )
{
    ewlInterfaceType_t interface;
    ewlTechnology_t Technology;
    uint8 EntryMode;
    ewlObject_t *emvHandle;
    int NextStep;

    int RetFun = RET_NOK;
    int RetFunStep = STM_RET_NEXT;

    uint8 FlagOtherInterf = TRUE;

    //Get Handle
    emvHandle = EWL_GetEmvHandle();
    CHECK(emvHandle != NULL, LBL_ERROR);

	//Get Entry mode
    RetFun = Tlv_GetTagValue(Tree, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

	switch (EntryMode)
	{
		case ENTRY_MODE_CHIP:
			interface = EWL_CONTACT;
			break;

		case ENTRY_MODE_CLESS_CHIP:
			interface = EWL_CONTACTLESS;
			break;

		default:
			return STM_RET_NEXT;
	}

	#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
	bool FlagExpressPay1 = 1;
	RetFun = ewlSetParameter(emvHandle,
			EWL_TAG_EXPRESPAY_1_COMPATIBILITY,
			&FlagExpressPay1,
			EWL_TAG_EXPRESPAY_1_COMPATIBILITY_LEN);
	#endif

	RetFun = ewlGetCard(emvHandle, interface);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    RetFun = dataRecoverGetData(Tree);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

//    RetFun = dataCheckExpDateEmvApp(Tree);
//    CHECK(RetFun == EWL_OK, LBL_ERROR_EXP_DATE);

    //+Check if card is Chip/Magnetic
    RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_CARD_TECHNOLOGY, &Technology, NULL);
    CHECK(RetFun == EWL_OK, LBL_ERROR_TLV);

    if(paymentToolsIsChipMagnetic(Technology))
    {
    	RetFunStep = dataProcessMagCard(Tree);
    }
    else
        RetFunStep = STM_RET_NEXT;
    //-

    return RetFunStep;

LBL_ERROR:
	utilLedsOff();

	if(EntryMode == ENTRY_MODE_CLESS_CHIP)
	{
		if(ewlTransactionAllowed(emvHandle,EWL_CONTACTLESS) != EWL_OK)
		{
			FlagOtherInterf = TRUE;
			RetFun = Tlv_SetTagValue(Tree, TAG_FORCE_OTHER_INTERFACE, &FlagOtherInterf, sizeof(FlagOtherInterf));
			CHECK(RetFun == RET_OK, LBL_ERROR);

			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_CLESS_LIMIT_L1),
					mess_getReturnedMessage(MSG_ERROR_CLESS_LIMIT_L2), GL_ICON_ERROR, GL_BUTTON_ALL, 3);

			return STEP_EWL_START;
		}
	}

	RetFun = paymentError(Tree, RetFun, &NextStep);
	if(RetFun != RET_OK)
		return STM_RET_ERROR;

	return NextStep;

LBL_ERROR_TLV:
//LBL_ERROR_EXP_DATE:
    return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:	dataGetMagCard
 * Description:		Get Tracks buffers
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- STM_RET_NEXT: OK
 * 	- STM_RET_ERROR: Error
 * Notes:
 */
int dataGetMagCard( TLV_TREE_NODE Tree )
{
	int RetFun;

    utilLedsOff();

    //Data recovery
	RetFun = dataRecoverMagneticCard(Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	RetFun = dataProcessMagCard(Tree);
	return RetFun;

LBL_ERROR:
	return STM_RET_ERROR;

}

/* --------------------------------------------------------------------------
 * Function Name:	dataProcessMagCard
 * Description:		Read information of magnetic card
 * Author:			@jbotero
 * Parameters:
 * 	- TLV_TREE_NODE Tree: Tree of transaction
 * Return:
 * 	- STM_RET_NEXT: OK
 * 	- STM_RET_ERROR: Error
 * Notes:
 */
int dataProcessMagCard( TLV_TREE_NODE Tree )
{
	int RetFun;
	uint8 EntryMode;
	unsigned int TrackLen;
	uint8 CardTechnology;
	char TrackBuffer[200];

	//Get Entry mode
    RetFun = Tlv_GetTagValue(Tree, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    //Check Entry mode
    CHECK(EntryMode != ENTRY_MODE_MANUAL, LBL_NEXT_STEP);

    //Get card technology -> Process cards MAGSTRIPE
    RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_CARD_TECHNOLOGY, &CardTechnology, NULL);
    if(RetFun == RET_OK)
    {
		uint8 EntryMode;

		//Check chip Magnetic technology
		RetFun = paymentToolsIsChipMagnetic(CardTechnology);
		CHECK(RetFun == TRUE, LBL_NEXT_STEP);

		//Update entry mode for CLESS MAGSTRIPE
		EntryMode = ENTRY_MODE_CLESS_SWIPE;
		Tlv_SetTagValue(Tree, TAG_ENTRY_MODE, &EntryMode, sizeof(EntryMode));

		//---Unpack track 1
		memset(TrackBuffer, 0x00, sizeof(TrackBuffer));
		RetFun = Tlv_GetTagValue(Tree, TAG_TRACK1, TrackBuffer, &TrackLen);
		if(RetFun == RET_OK)
		{
			if(CheckTrack1() != RET_OK)
				Tlv_SetTagValue(Tree_Tran, TAG_TRACK1_RAW, TrackBuffer, TrackLen);
		}

		//---Unpack track 2
		memset(TrackBuffer, 0x00, sizeof(TrackBuffer));
		RetFun = Tlv_GetTagValue(Tree, TAG_TRACK2, TrackBuffer, &TrackLen);
		if(RetFun == RET_OK)
		{
			if( CheckTrack2() != RET_OK)
				Tlv_SetTagValue(Tree_Tran, TAG_TRACK2_RAW, TrackBuffer, TrackLen);
		}
    }
    else
    {
		//---Unpack track 1
		memset(TrackBuffer, 0x00, sizeof(TrackBuffer));
		RetFun = Tlv_GetTagValue(Tree, TAG_TRACK1, TrackBuffer, &TrackLen);
		if(RetFun == RET_OK)
		{
			if(GetTrack1((uint8*)TrackBuffer) != RET_OK)
				Tlv_SetTagValue(Tree_Tran, TAG_TRACK1_RAW, TrackBuffer, TrackLen);
		}

		//---Unpack track 2
		memset(TrackBuffer, 0x00, sizeof(TrackBuffer));
		RetFun = Tlv_GetTagValue(Tree, TAG_TRACK2, TrackBuffer, &TrackLen);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		if( GetTrack2((uint8*)TrackBuffer) != RET_OK)
			Tlv_SetTagValue(Tree_Tran, TAG_TRACK2_RAW, TrackBuffer, TrackLen);
    }

LBL_NEXT_STEP:
	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}




