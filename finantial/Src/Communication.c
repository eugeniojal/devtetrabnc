/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * File:			Communication.c
 * Header file:		Communication.h
 * Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* --------------------------------------------------------------------------
 * Function Name:	Comm_BaseMakeValidation
 * Description:		Check if is necessary make the validation of external base
 * Author:			@jbotero
 * Parameters:
 * Return:
 * 	- true(Make validation)
 * 	- false(Don't make validation)
 * Notes:
 */

bool Comm_BaseMakeValidation( void )
{
	//Add on validation the terminals that support external base
	if( IsIWL2XX() )
		return true;

	return false;
}

/* --------------------------------------------------------------------------
 * Function Name:	Comm_CradleIsConnected
 * Description:		Check if base is connected
 * Author:			@jbotero
 * Parameters:
 *  - IsShowMsg: Flag to show message if base isn't connected.
 * Return:
 * 	- true(connected)
 * 	- false(Error or not connected)
 * Notes:
 */

bool Comm_CradleIsConnected( bool IsShowMsg )
{
	#if(TETRA_HARDWARE == 1)
	{
		return true;
	}
	#else
	{
		int result = 0;

		Telium_File_t * bt = Telium_Stdperif( "INTER_UC_BT", NULL );

		Telium_Fioctl( DBLUETOOTH_FIOCTL_ASSOCIATION, &result, bt );

		if( result != 0 )
		{
			if( IsShowMsg )
				UI_ShowMessage("ERROR", "*Base no detectada", "Conecte a Base!", GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut );

			return false;
		}

		return true;
	}
	#endif //TETRA_HARDWARE

}

/* --------------------------------------------------------------------------
 * Function Name:	Comm_BaseConfigure
 * Description:		Associate the current base under terminal
 * Author:			@jbotero
 * Parameters:
 *  - IsShowMsg: Flag to show message if base isn't connected.
 * Return:
 * 	- true(OK)
 * 	- false(Error)
 * Notes:
 */

bool Comm_CradleConfigure( bool IsShowMsg )
{
	#if(TETRA_HARDWARE == 1)
	{
		return true;
	}
	#else
	{
		int iRet, result;
		Telium_File_t * bt = NULL;
		dbluetooth_bases_infos_ext_s bases_infos;

		if( IsIWL2XX() )
		{
			bt = Telium_Stdperif( "INTER_UC_BT", NULL );

			//----------------------- GET ALL BASES

			iRet = Telium_Fioctl( DBLUETOOTH_FIOCTL_BASES_INFOS_EXT, &bases_infos, bt );
			CHECK(iRet == 0, lblErr);

			int i=0;

			//----------------------- REMOVE ALL BASES

			for( i=0; i< bases_infos.actual_number_of_bases; i++ )
			{
				iRet = Telium_Fioctl( DBLUETOOTH_FIOCTL_REMOVE_BASE, &bases_infos.bases[i].serial_number, bt );
				CHECK(iRet == 0, lblErr);
			}

			//----------------------- ASSOCIATE CURRENT BASE

			iRet = Telium_Fioctl( DBLUETOOTH_FIOCTL_ASSOCIATION, &result, bt );
			CHECK(iRet == 0, lblErr);

			if( result != 0 && !IsWifi() )
			{
				if( IsShowMsg )
					UI_ShowMessage("ERROR", "*Base no detectada", "", GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut );

				return FALSE;
			}

			//----------------------- GET CURRENT BASE

			iRet = Telium_Fioctl( DBLUETOOTH_FIOCTL_BASES_INFOS_EXT, &bases_infos, bt );
			CHECK(iRet == 0, lblErr);

			//----------------------- SELECT BASE

			iRet = Telium_Fioctl( DBLUETOOTH_FIOCTL_SELECT_BASE, &bases_infos.bases[0].serial_number, bt );
			CHECK(iRet == 0, lblErr);

		}
		return true;

		lblErr:
		return false;

	}
	#endif //TETRA_HARDWARE
}

/* --------------------------------------------------------------------------
 * Function Name:	Comm_CheckGPRSNetworkInfo
 * Description:		Check GPRS Network Information
 * Author:			@jbotero
 * Parameters:
 * 	- OUT 	T_EGPRS_GET_INFORMATION * GPRSInfo: GPRS status information
 * 	- IN 	timeout: Time to wait connection(1 msec). Timeout = 1000 -> 1 second.
 * Return:
 * 	- TRUE:	Get status operation OK.
 * 	- FALSE: Problem to get status or SIM no inserted.
 * Notes:			Nothing
 */

uint8 Comm_CheckGPRSNetworkInfo( T_EGPRS_GET_INFORMATION * GPRSInfo, int timeout )
{
	Telium_File_t * hGprs = NULL;
	unsigned int nWaitTotal = 0;
	int RetFun = 0;
	T_EGPRS_GET_INFORMATION tGPRSInfo;

	memset(&tGPRSInfo, 0x00, sizeof(T_EGPRS_GET_INFORMATION));

	if(!IsRadioGPRS() && !IsRadio3G())
		return FALSE;

	#if(TETRA_HARDWARE == 0)
	hGprs = Telium_Stdperif("DGPRS", NULL);
	CHECK(hGprs != NULL, LBL_ERROR);
	#endif

	do
	{
		RetFun = gprs_GetInformation(hGprs, GPRSInfo, sizeof(T_EGPRS_GET_INFORMATION));

		if( GPRSInfo->sim_status == EGPRS_SIM_NOT_INSERTED )
			break;

		if(GPRSInfo->status_gprs != EGPRS_GPRS_PDP_CONNECTED && timeout > 1)
		{
			UI_ClearAllLines();
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_PLEASE_WAIT), UI_ALIGN_CENTER);
			UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_CONFIGCOMM), UI_ALIGN_CENTER);
			UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_GPRS), UI_ALIGN_CENTER);
			UI_ShowInfoScreen(NULL);
		}

		if( GPRSInfo->sim_status == EGPRS_SIM_NOT_RECEIVED_YET)
		{
			#if(TETRA_HARDWARE == 0)
			RetFun = LL_GSM_Start(NULL);
			CHECK(RetFun == LL_ERROR_OK, LBL_ERROR);
			#else
			RetFun = LL_GPRS_Start(NULL, tConf.GPRS_APN);
			#endif

		}

		if( GPRSInfo->sim_status == EGPRS_SIM_OK)
		{
			RetFun = LL_GPRS_Start(NULL, tConf.GPRS_APN);
			CHECK(RetFun == LL_ERROR_OK, LBL_ERROR);
		}

		if( EGPRS_SIM_OK == GPRSInfo->sim_status && GPRSInfo->plmn > 0)
			break;
		else
		{
			Telium_Ttestall( 0, 100 );
			nWaitTotal += 100;
		}

	}while( nWaitTotal < timeout );

	CHECK(EGPRS_SIM_NOT_INSERTED != GPRSInfo->sim_status, LBL_NO_SIM)

	memcpy(&tGPRSInfo, GPRSInfo, sizeof(tGPRSInfo));

	return TRUE;

LBL_ERROR:
	return FALSE;

LBL_NO_SIM:
	return FALSE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Comm_Initialize
 * Description:		Initialize the communication Module
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */

uint8 Comm_Initialize( void )
{
	Telium_File_t *hPrinter;
	T_EGPRS_GET_INFORMATION GPRSInfo;
	int RetFun;

	//--- Configure external Base
	Comm_CradleConfigure(FALSE);

	//--- WIFI CONFIGURATION
	#if(TETRA_HARDWARE == 1)
	#else
	if( IsWifi() )
	{
		//--- Open WIFI DLL
		if( 0 == wifilib_open() )
		{
			HWCNF_SetWifiPower( HWCNF_WIFI_RADIO_ON );

			//--- Configure Standby Delay & Back-light delay
			HWCNF_SetEnergySaveMode(ENERGYSAVE_MODE_BACKLIGHT);
			HWCNF_SetBacklightDelayEx(BACKLIGHT_DEVICE_ALL, BACKLIGHT_TARGET_ALL, 120 );
			HWCNF_SetStandbyDelay(600);
		}
	}
	#endif

	//--- GPRS CONFIGURATION
	if( IsRadioGPRS() || IsRadio3G() )
	{
		RetFun = Comm_CheckGPRSNetworkInfo( &GPRSInfo, 60 * 100 );

		if(GPRSInfo.sim_status == EGPRS_SIM_NOT_INSERTED)
		{
			UI_ShowMessage(mess_getReturnedMessage(MSG_GPRS),
					mess_getReturnedMessage(MSG_NO_SIMCARD),
					NULL,GL_ICON_WARNING,GL_BUTTON_ALL,tTerminal.TimeOut);
		}

		//--- Configure Standby Delay & Back-light delay
	#ifndef TETRA_HARDWARE
		HWCNF_SetEnergySaveMode(ENERGYSAVE_MODE_BACKLIGHT);
	#endif
		HWCNF_SetBacklightDelayEx(BACKLIGHT_DEVICE_ALL, BACKLIGHT_TARGET_ALL, 120 );
		HWCNF_SetStandbyDelay(600);
	}

#ifndef TETRA_HARDWARE
	//--- DESKTOP CONFIGURATION
	if(IsICT220() || IsICT250())
	{
		//--- Configure Standby Delay & Back-light delay
		HWCNF_SetEnergySaveMode(ENERGYSAVE_MODE_NONE);
	}
#endif

	//Configure contrast of printer
    hPrinter = Telium_Fopen("PRINTER", "w-*");                 // Open printer driver

    if(IsPortable())
    	RetFun = TM_Printer_AdjustIntensity(hPrinter, PRINTER_INTENSITY_NORMAL);
    else
    	RetFun = TM_Printer_AdjustIntensity(hPrinter, PRINTER_INTENSITY_NORMAL);

	Telium_Fclose(hPrinter);

	return RET_OK;

}

