/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			Steps.c
 * Header file:		Steps.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/
bool MerchGroup_IsTransactionEnable(int32 MerchGroupID, TRANS_LIST TranID);


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int Post_STEP_CHECK_TERMINAL(char *Result);
bool Pre_STEP_SELECT_MERCH_GROUP(void);
int Post_STEP_SELECT_MERCH_GROUP(char *Result);
bool Pre_STEP_GET_CARD(void);
int Post_STEP_GET_CARD(char *Result);
bool Pre_STEP_MANUAL_CARD_ENTRY(void);
int Post_STEP_MANUAL_CARD_ENTRY(char *Result);
bool Pre_STEP_MANUAL_TELF_ENTRY(void);
int Post_STEP_MANUAL_TELF_ENTRY(char *Result);
int Post_STEP_SELECT_MERCHANT(char *Result);
int Post_STEP_CHECK_EXP_DATE(char *Result);
bool Pre_STEP_MANUAL_EXP_DATE(void);
int Post_STEP_MANUAL_EXP_DATE(char *Result);
bool Pre_STEP_CVV2(void);
int Post_STEP_CVV2(char *Result);
bool Pre_STEP_LAST_4_PAN(void);
int Post_STEP_LAST_4_PAN(char *Result);
int Post_STEP_CHECK_SERVICE_CODE(char *Result);
bool Pre_STEP_BASE_AMOUNT(void);
int Post_STEP_BASE_AMOUNT(char *Result);
bool Pre_STEP_TAX_AMOUNT(void);
int Post_STEP_TAX_AMOUNT(char *Result);
bool Pre_STEP_TIP_AMOUNT(void);
int Post_STEP_TIP_AMOUNT(char *Result);
int Post_STEP_TIP_AMOUNT_RESTAURANT(char *Result);
int Post_STEP_CONFIRM_TOTAL(char *Result);
int Post_STEP_PREDIAL(char *Result);
int Post_STEP_END_COMMUNICATION(char *Result);
int Post_STEP_SEND_RECEIVE(char *Result);
int Post_STEP_PROCESS_RESPONSE(char *Result);
int Post_STEP_PRINT_RECEIPT(char *Result);
bool Pre_STEP_AUTH_NUMBER(void);
int Post_STEP_AUTH_NUMBER(char *Result);
bool Pre_STEP_INVOICE(void);
int Post_STEP_INVOICE(char *Result);
int Post_STEP_INVOICE_ADJUST(char *Result);
bool Pre_STEP_SERVER(void);
int Post_STEP_SERVER(char *Result);
bool Pre_STEP_MERCHANT_PASSWORD(void);
int Post_STEP_MERCHANT_PASSWORD(char *Result);
int Post_STEP_CONFIRM_VOID(char *Result);
bool Pre_STEP_ADJUST_BASE_AMOUNT(void);
int Post_STEP_ADJUST_BASE_AMOUNT(char *Result);
bool Pre_STEP_ADJUST_TIP_AMOUNT(void);
bool Pre_STEP_ADJUST_TAX_AMOUNT(void);
int Post_STEP_ADJUST_CONFIRM(char *Result);
int Post_STEP_ADJUST_VALIDATE(char *Result);
int Post_STEP_SELECT_MERCH_GROUP_ALL(char *Result);
int Post_STEP_SELECT_MERCHANT_ALL(char *Result);
int Post_STEP_PRINT_TOTALS_REPORT(char *Result);
int Post_STEP_PRINT_DETAILS_REPORT(char *Result);
int Post_STEP_PRINT_DETAILS_SERVER(char *Result);
int Post_STEP_RUN_SETTLE(char *Result);
bool Pre_STEP_TERMINAL_PASSWORD(void);
int Post_STEP_TERMINAL_PASSWORD(char *Result);
//+ CRYPTOLIB @mamata Feb 21, 2015
//d bool Pre_STEP_PIN(void);
//- CRYPTOLIB
int Post_STEP_PIN(char *Result);

//+ ECHOTEST @mamata Aug 15, 2014
int Post_STEP_SELECT_MERCHANT_BY_TRAN(char *Result);
int Post_STEP_SHOW_RESULT(char *Result);
//- ECHOTEST

//+ CLESS @mamata Dec 11, 2014
int Post_STEP_CLESS_VALIDATE_CARD(char *Result);
bool Pre_STEP_QUICK_SALE_AMOUNT(void);
int Post_STEP_QUICK_SALE_AMOUNT(char *Result);
//- CLESS

//+ CARCAAN_PINPAD @mamata Jan 14, 2015
int Post_STEP_PINPAD_GET_CARD(char *Result);
int Post_STEP_PINPAD_HOST_AUTH(char *Result);
int Post_STEP_PINPAD_EMV_1GEN(char *Result);
//- CARCAAN_PINPAD

//+ EMV09 @mamata Jun 22, 2015
int Post_STEP_QUICK_SALE_CHECK_SERVICE_CODE(char *Result);
//- EMV09

//+ EWL01 @jbotero 29/01/2016
int Post_STEP_START_PAYMENT(char *Result);
int Post_STEP_EWL_START(char *Result);
int Post_STEP_WAIT_CARD(char *Result);
int Post_STEP_EWL_GO_ONCHIP(char *Result);
int Post_STEP_APPROVED_OFF(char *Result);
int Post_STEP_DENIAL_OFF(char *Result);
int Post_STEP_HOST_ANALYSE(char *Result);
int Post_STEP_FINISH(char *Result);
int Post_STEP_SECOND_TAP(char *Result);
//- EWL01

bool Pre_STEP_ENTER_ID_NUMBER(void);
int Post_STEP_ENTER_ID_NUMBER(char *Result);

bool Pre_STEP_SELECT_ACCOUNT_TYPE(void);
int Post_STEP_SELECT_ACCOUNT_TYPE(char *Result);

int Post_STEP_COMPARE_READ_PAN(char *Result);

bool Pre_STEP_TRAN_SERVER(void);
int Post_STEP_TRAN_SERVER(char *Result);
//EUGENIO
int Post_STEP_PRE_PRINT_RECEIPT(char *Result);
int post_MENU_CONFIG (char *Result);
int Post_STEP_LAST_ANSWER(char *Result);

//jr26052021
bool Pre_STEP_PIN_NUMBER(void);
int Post_STEP_PIN_NUMBER(char *Result);

int Post_STEP_SELECT_BANK(char *Result);
int Post_STEP_Validate_Mount(char *Result);
int Post_STEP_CHECK_FALLBACK(char *Result);
#define maxDigitos	12
/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
uint8 MEntryBuffer[25];

SMT_STEP STEP_LIST[] =
{
	//Step ID					Input Type	   		Min 		Max 			TO  			Result				PreFunc							PostFunc
	{ STEP_CHECK_TERMINAL, 		INPUT_NONE, 		0,			0,				0,				NULL, 				NULL,							Post_STEP_CHECK_TERMINAL },
	{ STEP_TECHNICIAN_PASSWORD,	INPUT_PASSWORD,		1,			10,				30,				NULL,				Pre_STEP_TECHNICIAN_PASSWORD,	Post_STEP_TECHNICIAN_PASSWORD },
	{ STEP_SELECT_MERCH_GROUP,	INPUT_NONE,			0,			0,				0, 				NULL,				Pre_STEP_SELECT_MERCH_GROUP,	Post_STEP_SELECT_MERCH_GROUP },
	{ STEP_MANUAL_CARD_ENTRY,	INPUT_NUMERIC_TEXT,	1,			19,				30, 			NULL,				Pre_STEP_MANUAL_CARD_ENTRY,		Post_STEP_MANUAL_CARD_ENTRY },
	{ STEP_MANUAL_TELF_ENTRY,	INPUT_NUMERIC_TEXT,	1,			10,				30, 			NULL,				Pre_STEP_MANUAL_TELF_ENTRY,		Post_STEP_MANUAL_TELF_ENTRY },
	{ STEP_MANUAL_EXP_DATE,		INPUT_EXPDATE,		4,			4,				30,				NULL,				Pre_STEP_MANUAL_EXP_DATE,		Post_STEP_MANUAL_EXP_DATE },
	{ STEP_SELECT_MERCHANT,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_SELECT_MERCHANT },
	{ STEP_CHECK_EXP_DATE, 		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_CHECK_EXP_DATE },
	{ STEP_CVV2,				INPUT_NUMERIC_TEXT, 3,			4,				30,				NULL, 				Pre_STEP_CVV2,					Post_STEP_CVV2 },
	{ STEP_LAST_4_PAN,			INPUT_NUMERIC_TEXT,	4,			4,				30,				NULL,				Pre_STEP_LAST_4_PAN,			Post_STEP_LAST_4_PAN },
	{ STEP_CHECK_SERVICE_CODE, 	INPUT_NONE, 		0,			0,				0,				NULL,				NULL,							Post_STEP_CHECK_SERVICE_CODE },
	{ STEP_BASE_AMOUNT,			INPUT_AMOUNT,		1,			maxDigitos,				30,				NULL, 				Pre_STEP_BASE_AMOUNT,			Post_STEP_BASE_AMOUNT },
	{ STEP_TAX_AMOUNT,			INPUT_AMOUNT,		0,			9,				30,				NULL, 				Pre_STEP_TAX_AMOUNT,			Post_STEP_TAX_AMOUNT },
	{ STEP_TIP_AMOUNT,			INPUT_AMOUNT,		0,			9,				30,				NULL, 				Pre_STEP_TIP_AMOUNT,			Post_STEP_TIP_AMOUNT },
	{ STEP_TIP_AMOUNT_RESTAURANT,INPUT_NONE,		0,			9,				30,				NULL, 				NULL,							Post_STEP_TIP_AMOUNT_RESTAURANT },
	{ STEP_CONFIRM_TOTAL,		INPUT_NONE,			0,			0,				0,				NULL, 				NULL,							Post_STEP_CONFIRM_TOTAL },
	{ STEP_PREDIAL,				INPUT_NONE, 		0,			0,				0,				NULL,				NULL,							Post_STEP_PREDIAL },
	{ STEP_END_COMMUNICATION, 	INPUT_NONE,			0,			0,				0,				NULL, 				NULL, 							Post_STEP_END_COMMUNICATION },
	{ STEP_SEND_RECEIVE,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_SEND_RECEIVE },
	{ STEP_PROCESS_RESPONSE,	INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_PROCESS_RESPONSE },
	{ STEP_PRINT_RECEIPT,		INPUT_NONE, 		0,			0,				0,				NULL,				NULL,							Post_STEP_PRINT_RECEIPT },
	{ STEP_AUTH_NUMBER,			INPUT_TEXT,			1,			6,				30,				NULL,				Pre_STEP_AUTH_NUMBER, 			Post_STEP_AUTH_NUMBER },
	{ STEP_INVOICE,				INPUT_NUMERIC_TEXT,	1,			6,				30,				NULL,				Pre_STEP_INVOICE,				Post_STEP_INVOICE },
	{ STEP_INVOICE_ADJUST,		INPUT_NUMERIC_TEXT,	1,			6,				30,				NULL,				Pre_STEP_INVOICE,				Post_STEP_INVOICE_ADJUST },
	{ STEP_MERCHANT_PASSWORD, 	INPUT_PASSWORD,		4,			4,				30,				NULL,				Pre_STEP_MERCHANT_PASSWORD,		Post_STEP_MERCHANT_PASSWORD },
	{ STEP_CONFIRM_VOID,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_CONFIRM_VOID },
	{ STEP_ADJUST_BASE_AMOUNT,	INPUT_AMOUNT,		0,			11,				30,				NULL, 				Pre_STEP_ADJUST_BASE_AMOUNT,	Post_STEP_ADJUST_BASE_AMOUNT },
	{ STEP_ADJUST_TIP_AMOUNT,	INPUT_AMOUNT,		0,			9,				30,				NULL,				Pre_STEP_ADJUST_TIP_AMOUNT,		Post_STEP_TIP_AMOUNT },
	{ STEP_ADJUST_TAX_AMOUNT,	INPUT_AMOUNT,		0,			9,				30,				NULL,				Pre_STEP_ADJUST_TAX_AMOUNT,		Post_STEP_TAX_AMOUNT },
	{ STEP_ADJUST_CONFIRM,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_ADJUST_CONFIRM },
	{ STEP_ADJUST_VALIDATE,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_ADJUST_VALIDATE },
	{ STEP_SELECT_MERCH_GROUP_ALL, INPUT_NONE,		0,			0,				0,				NULL,				NULL,							Post_STEP_SELECT_MERCH_GROUP_ALL },
	{ STEP_SELECT_MERCHANT_ALL,	INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_SELECT_MERCHANT_ALL },
	{ STEP_PRINT_TOTALS_REPORT, INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_PRINT_TOTALS_REPORT },
	{ STEP_PRINT_DETAILS_REPORT,INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_PRINT_DETAILS_REPORT },
	{ STEP_PRINT_DETAILS_SERVER,INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_PRINT_DETAILS_SERVER },
	{ STEP_RUN_SETTLE,			INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_RUN_SETTLE },
	{ STEP_TERMINAL_PASSWORD, 	INPUT_PASSWORD,		4,			4,				30,				NULL,				Pre_STEP_TERMINAL_PASSWORD,		Post_STEP_TERMINAL_PASSWORD },
	//+ CRYPTOLIB @mamata Feb 21, 2015
	{ STEP_PIN,					INPUT_NONE,		    4,			6,				30,				NULL,				NULL,							Post_STEP_PIN },
//	{ STEP_PIN,					INPUT_PASSWORD,		4,			6,				30,				NULL,				Pre_STEP_PIN,					Post_STEP_PIN },
	//- CRYPTOLIB

	//+ ECHOTEST @mamata Aug 15, 2014
	{ STEP_SELECT_MERCHANT_BY_TRAN,INPUT_NONE,		0,			0,				0,				NULL,				NULL,							Post_STEP_SELECT_MERCHANT_BY_TRAN },
	{ STEP_SHOW_RESULT,		     INPUT_NONE,		0,			0,				0,				NULL,				NULL,							Post_STEP_SHOW_RESULT },
	//- ECHOTEST

	//+ CLESS @mamata Dec 11, 2014
	{ STEP_CLESS_VALIDATE_CARD, INPUT_NONE, 		0,			0,				0,				NULL,				NULL,							Post_STEP_CLESS_VALIDATE_CARD },
	{ STEP_QUICK_SALE_AMOUNT,	INPUT_NUM_DEC,		1,			9,				30,				NULL, 				Pre_STEP_QUICK_SALE_AMOUNT,		Post_STEP_QUICK_SALE_AMOUNT },
	//- CLESS

	//+ CARCAAN_PINPAD @mamata Jan 14, 2015
	{ STEP_PINPAD_GET_CARD,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_PINPAD_GET_CARD },
	{ STEP_PINPAD_HOST_AUTH,	INPUT_NONE, 		0,			0,				0,				NULL,				NULL,							Post_STEP_PINPAD_HOST_AUTH },
	{ STEP_PINPAD_EMV_1GEN,		INPUT_NONE,			0,			0,				0,				NULL,				NULL,							Post_STEP_PINPAD_EMV_1GEN },
	//- CARCAAN_PINPAD

	//+ EMV09 @mamata Jun 22, 2015
	{ STEP_QUICK_SALE_CHECK_SERVICE_CODE, INPUT_NONE, 0,		0,				0,				NULL,				NULL,							Post_STEP_QUICK_SALE_CHECK_SERVICE_CODE },
	//- EMV09

	//+ EWL01 @jbotero 29/01/2016
	{ STEP_START_PAYMENT,			INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_START_PAYMENT},
	{ STEP_EWL_START,				INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_EWL_START},
	{ STEP_WAIT_CARD,				INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_WAIT_CARD},
	{ STEP_EWL_GO_ONCHIP,			INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_EWL_GO_ONCHIP},
	{ STEP_APPROVED_OFF,			INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_APPROVED_OFF},
	{ STEP_DENIAL_OFF,				INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_DENIAL_OFF},
	{ STEP_HOST_ANALYSE,			INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_HOST_ANALYSE},
	{ STEP_SECOND_TAP,				INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_SECOND_TAP},
	{ STEP_FINISH,					INPUT_NONE,			0,          0,              0,            	NULL, 			NULL,							Post_STEP_FINISH},
	//- EWL01
	{  STEP_LAST_ANSWER,			INPUT_NONE, 		0,			0,				0,				NULL,			NULL,							Post_STEP_LAST_ANSWER },
	{ STEP_ENTER_ID_NUMBER, 		INPUT_NUMERIC_TEXT,        1,         10,             30,              NULL,           Pre_STEP_ENTER_ID_NUMBER,       Post_STEP_ENTER_ID_NUMBER},
	{ STEP_SELECT_ACCOUNT_TYPE,		INPUT_NONE,			0,  		0,   			0,      		NULL, 			Pre_STEP_SELECT_ACCOUNT_TYPE,  	Post_STEP_SELECT_ACCOUNT_TYPE},
	{ STEP_COMPARE_READ_PAN,        INPUT_NONE,		0,  		0,   			0,      		NULL,           NULL,							Post_STEP_COMPARE_READ_PAN},
	{ STEP_TRAN_SERVER,				INPUT_NUMERIC_TEXT,	        1,          4, 			   30,	            NULL,	        Pre_STEP_TRAN_SERVER,			Post_STEP_TRAN_SERVER},
	{ STEP_PRE_PRINT,				INPUT_NONE, 		0,			0,				0,				NULL,			NULL,						    Post_STEP_PRE_PRINT_RECEIPT },
	{ STEP_MENU_CONFIG,				INPUT_NONE, 		0,			0,				0,				NULL,			NULL,							post_MENU_CONFIG },
	{ STEP_SERVER,					INPUT_NUMERIC_TEXT,	1,			3,				30,				NULL,			Pre_STEP_SERVER,				Post_STEP_SERVER },
	{ STEP_PIN_NUMBER,  			INPUT_NUMERIC_TEXT, 1,   	    8,              30,             NULL,           Pre_STEP_PIN_NUMBER,       		Post_STEP_PIN_NUMBER},
	{ STEP_SELECT_BANK,				INPUT_NONE,			4,  		4,   			0,      		NULL, 			NULL,						  	Post_STEP_SELECT_BANK},
	{ STEP_VALIDATE_MOUNT,			INPUT_NONE,			0,  		0,   			0,      		NULL, 			NULL,						  	Post_STEP_Validate_Mount},
	{ STEP_CHECK_FALLBACK,			INPUT_NONE,			0,  		0,   			0,      		NULL, 			NULL,						  	Post_STEP_CHECK_FALLBACK}
};

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_CHECK_TERMINAL
 * Description:		Verify if terminal is initiliazed and ready to do transactions
 * Parameters:
 *  - none
 * Return:
 *  - true - the terminal is ready
 *  - false - is needed to configure and initialize the terminal
 * Notes:
 */
int Post_STEP_CHECK_TERMINAL(char *Result)
{
	if(tTerminal.Header.RecordID != 0)
		return STM_RET_NEXT;

	UI_ClearAllLines();

	UI_ShowMessage(	mess_getReturnedMessage(MSG_APPNAME),
					mess_getReturnedMessage(MSG_ERROR_TERMINAL_NO_CONF),
					mess_getReturnedMessage(MSG_PLEASE_INIT),
					GL_ICON_ERROR,
					GL_BUTTON_ALL,
					30);
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_SELECT_MERCH_GROUP
 * Description:		Pre function to STEP_SELECT_MERCH_GROUP
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_SELECT_MERCH_GROUP(void)
{
	TLV_TREE_NODE node;
	int16 MessageID;
	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if(node != NULL)
		memcpy((char*)&MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;

	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MessageID), UI_ALIGN_CENTER);
	return true;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SELECT_MERCH_GROUP
 * Description:		select a Merchant Group depend transaction
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  -
 * Notes:
 */
int Post_STEP_SELECT_MERCH_GROUP(char *Result)
{
	int FunResult;
	TABLE_MERCH_GROUP tmpMerchGroup;
	int16 TranID;
	TLV_TREE_NODE node;
	int NumItems=0;
	int32 SelectedMerchGroup=0;

	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_NO_MERCHGROUP);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	UI_MenuReset();

	// try to find fist record of Merchant Gruop table
	FunResult = TM_FindFirst(TAB_MERCH_GROUP, &tmpMerchGroup);
	CHECK(FunResult == TM_RET_OK, LBL_NO_MERCHGROUP);

	do
	{
		// Verify if transaction is enable
		if( MerchGroup_IsTransactionEnable(tmpMerchGroup.Header.RecordID, TranID))
		{
			// Transaction is enable, add Merchant Group to menu
			UI_MenuAddItem(tmpMerchGroup.Name, tmpMerchGroup.Header.RecordID);
			NumItems++;
		}
		// try to find next record
		FunResult = TM_FindNext(TAB_MERCH_GROUP, &tmpMerchGroup);
	//+ NORECORDS @mamata 6/05/2016
	//d }while(FunResult != TM_RET_NO_MORE_RECORDS);
	}while(FunResult == TM_RET_OK);
	CHECK(FunResult == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	// check if no item found, then cancel transaction
	CHECK(NumItems > 0, LBL_NO_ITEMS);

	// execute menu
	SelectedMerchGroup = UI_MenuRun(mess_getReturnedMessage(MSG_MERCH_GROUP), tConf.TimeOut, 0);

	// if is a valid result, goto next step
	if(SelectedMerchGroup > 0)
	{
		node = TlvTree_AddChild(Tree_Tran, TAG_MERCH_GROUP_ID, &SelectedMerchGroup, LTAG_UINT32);
		CHECK(node != NULL, LBL_NO_MERCHGROUP)
		return STM_RET_NEXT;
	}


LBL_NO_MERCHGROUP: // no merchant groups configured
	return STM_RET_CANCEL;

LBL_NO_ITEMS:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION), mess_getReturnedMessage(MSG_NO_ALLOWED), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

//+ NORECORDS @mamata 6/05/2016
LBL_ERROR:
	return STM_RET_ERROR;
//- NORECORDS
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_CARD_MANUAL_ENTRY
 * Description:		Pre function for STEP_CARD_MANUAL_ENTRY
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - PAN is not present, its necessary perform manual entry
 *  - FALSE - PAN is already present, goto to next step
 * Notes:
 */
bool Pre_STEP_MANUAL_CARD_ENTRY(void)
{
	TLV_TREE_NODE node;
	int16 EntryMode;
	uint16 TranID;
	uint32 RangID;

    node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	if(TranID == TRAN_VOID)
   {
		node = TlvTree_Find(Tree_Tran, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

		if (RangID == 8)
		return false;

   }

	//---Get Transaction Status

	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	EntryMode = *(uint8*)TlvTree_GetData(node);

	if(EntryMode != ENTRY_MODE_MANUAL)
		return FALSE;

	// verify if transaction allows manual entry
	//node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	//if(node == NULL)
	//{
		UI_ClearLine(UI_LINE2);
		UI_ClearLine(UI_LINE3);
		UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_MANUAL_ENTRY), UI_ALIGN_CENTER);
		//memcpy(Step_Buffer, MEntryBuffer, 25);
		return TRUE;
	//}

	//return FALSE;
		LBL_TAG_TRAN_ID_NOTFOUND:
			return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CARD_MANUAL_ENTRY
 * Description:		Post funciton STEP_CARD_MANUAL_ENTRY
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT - card number OK
 *  - STM_RET_CANCEL - cancel or tiemout
 * Notes:
 */
int Post_STEP_MANUAL_CARD_ENTRY(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;


		// Add Auth Code to TAG
		node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
		if(node != NULL)
		{
			RetFun=TlvTree_SetData( node, Step_Buffer, strlen(Step_Buffer) );
			CHECK(RetFun == TLV_TREE_OK, LBL_TREE_ERROR);
		}
		else
		{
			node = TlvTree_AddChild(Tree_Tran, TAG_PAN, Step_Buffer, strlen(Step_Buffer));
			CHECK(node!= NULL, LBL_TREE_ERROR);
		}


	char cpCurrentPAN[64] = { 0 };
	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	CHECK(node != NULL, LBL_TREE_ERROR);
	memcpy(cpCurrentPAN, TlvTree_GetData(node), TlvTree_GetLength(node));

	uint8 EntryMode = ENTRY_MODE_MANUAL;
	node = TlvTree_AddChild(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, LTAG_UINT8);
	CHECK(node!= NULL, LBL_TREE_ERROR);
	//TlvTree_Release(Tree_Tran);
	if(node != NULL)
		return STM_RET_NEXT;
	else
	{
		UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_TRAN), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_CANCEL;
	}

LBL_TREE_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TELF_MANUAL_ENTRY
 * Description:		Post funciton STEP_TELF_MANUAL_ENTRY
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT - TELF number OK
 *  - STM_RET_CANCEL - cancel or tiemout
 * Notes:
 */
int Post_STEP_MANUAL_TELF_ENTRY(char *Result)
{
	TLV_TREE_NODE node;

	node = TlvTree_AddChild(Tree_Tran, TAG_PAN, Step_Buffer, strlen(Step_Buffer));
	CHECK(node!= NULL, LBL_TREE_ERROR);

	uint8 EntryMode = ENTRY_MODE_MANUAL;
	node = TlvTree_AddChild(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, LTAG_UINT8);
	CHECK(node!= NULL, LBL_TREE_ERROR);

	if(node != NULL)
		return STM_RET_NEXT;
	else
	{
		UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_TRAN), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_CANCEL;
	}

LBL_TREE_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TELF_MANUAL_ENTRY
 * Description:		Pre function for STEP_TELF_MANUAL_ENTRY
 * Parameters:
 *  - none
 * Return:
 *  - TRUE - PAN is not present, its necessary perform manual entry
 *  - FALSE - PAN is already present, goto to next step
 * Notes:
 */
bool Pre_STEP_MANUAL_TELF_ENTRY(void)
{
	TLV_TREE_NODE node;

	// verify if transaction allows manual entry
	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	if(node == NULL)
	{
		UI_ClearLine(UI_LINE2);
		UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_TELF_ENTRY), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_EJ_TELF), UI_ALIGN_CENTER);
		memcpy(Step_Buffer, MEntryBuffer, 25);
		return TRUE;
	}

	return FALSE;
}



/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
bool Pre_STEP_MANUAL_EXP_DATE(void)
{
	TLV_TREE_NODE node;
	uint8 EntryMode;
	uint16 TranID;
	uint32 RangID;

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	if(TranID == TRAN_VOID)
   {
		node = TlvTree_Find(Tree_Tran, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

		if (RangID == 8)
		return FALSE;

   }

	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	EntryMode = *(uint8*)TlvTree_GetData(node);

	if(EntryMode != ENTRY_MODE_MANUAL)
		return FALSE;
	UI_ClearLine(UI_LINE2);
	UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_EXPIRATION_DATE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_MMYY), UI_ALIGN_CENTER);
	return TRUE;

	LBL_TAG_TRAN_ID_NOTFOUND:
			return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_MANUAL_EXP_DATE(char *Result)
{
	TLV_TREE_NODE node;
	uint8 tmpExpDate[4];

	memset(tmpExpDate, 0, 4);

	memcpy(tmpExpDate, &Step_Buffer[3], 2);
	memcpy(&tmpExpDate[2], Step_Buffer, 2);

	node = TlvTree_AddChild( Tree_Tran, TAG_EXP_DATE, tmpExpDate, 4);
	CHECK(node != NULL, LBL_TREE_ERROR);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);

	return STM_RET_NEXT;

LBL_TREE_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_SELECT_MERCHANT(char *Result)
{
	//+ TREEMEM @mamata 5/05/2016
	//d TLV_TREE_NODE Range_CandidateList;
	//d TLV_TREE_NODE Merchant_CandidateList;
	TLV_TREE_NODE Range_CandidateList = NULL;
	TLV_TREE_NODE Merchant_CandidateList = NULL;
	//- TREEMEM
	TLV_TREE_NODE node;
	TLV_TREE_NODE node2;
	bool MultyCurrency;
	uint32 MerchantGropuID;
	int RetFun;
	uint8 Currency = 0;
	uint16 TranID;
	uint8 EntryMode;
	TABLE_RANGE Range;

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);



	//verifica si es necesario realizar el cierre
	CHECK(!(tTerminal.Flags1 & TERM_FLAG1_SETTLE_OBLIGADO), LBL_ERROR_CIERRE)

	//UI_ClearAllLines();

	// get Merchant group id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_TREE_ERROR);
	memcpy((char*)&MerchantGropuID, TlvTree_GetData(node), LTAG_UINT64);

	Range_CandidateList = TlvTree_New(0);
	Merchant_CandidateList = TlvTree_New(0);

	RetFun = BuildRangeCandidateList(Range_CandidateList);
	CHECK(RetFun == RET_OK, LBL_ERROR_RANGECL);

	RetFun = BuildMerchantCandidateList(Merchant_CandidateList, Range_CandidateList, MerchantGropuID, &MultyCurrency);
	CHECK(RetFun == RET_OK, LBL_ERROR_RANGECL);


	if(MultyCurrency)
	{
		UI_MenuReset();
		UI_MenuAddItem(mess_getReturnedMessage(MSG_LOCAL), 1);
		UI_MenuAddItem(mess_getReturnedMessage(MSG_DOLAR), 2);
		Currency = UI_MenuRun(mess_getReturnedMessage(MSG_CURRENCY), tConf.TimeOut, 0);
		CHECK(Currency >0, LBL_CANCEL);
	}

	// Select Merchant
	UI_MenuReset();
	node = TlvTree_GetFirstChild(Merchant_CandidateList);
	do
	{
		uint8 MerchName[30];
		int MerchID;
		uint8 MerchCurrency;

		node2 = TlvTree_Find(node, TAG_CURRENCY, 0);
		memcpy(&MerchCurrency, TlvTree_GetData(node2), LTAG_UINT8);

		if(Currency == 0)
		{
			// no multi currency, just add merchant to menu
			memset(MerchName, 0, 30);
			memcpy(MerchName, TlvTree_GetData(node), TlvTree_GetLength(node));
			MerchID = TlvTree_GetTag(node);
			UI_MenuAddItem((char*)MerchName, MerchID);
		}
		else
		{
			// Multi currency, validate merchant currency
			if(MerchCurrency == Currency)
			{
				memset(MerchName, 0, 30);
				memcpy(MerchName, TlvTree_GetData(node), TlvTree_GetLength(node));
				MerchID = TlvTree_GetTag(node);
				UI_MenuAddItem((char*)MerchName, MerchID);
			}
		}
		node = TlvTree_GetNext(node);
	}while(node != NULL);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_MERCHANT), tConf.TimeOut, 0);
	CHECK(RetFun > 0, LBL_CANCEL);

	node = TlvTree_AddChild(Tree_Tran, TAG_MERCHANT_ID, &RetFun, LTAG_UINT32);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// Select range
	//+ MERCHSELECT @mamata 5/05/2016
	//d UI_MenuReset();
	//d node = TlvTree_GetFirstChild(Range_CandidateList);
	//d do
	//d {
	//d     uint8 RangeName[30];
	//d     int RangeID;
	//d     memset(RangeName, 0, 30);
	//d     memcpy(RangeName, TlvTree_GetData(node), TlvTree_GetLength(node));
	//d     RangeID = TlvTree_GetTag(node);
	//d     UI_MenuAddItem((char*)RangeName, RangeID);
	//d     node = TlvTree_GetNext(node);
	//d }while(node != NULL);
	//d RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_CARD), tConf.TimeOut, 0);
	node2 = TlvTree_FindChild(Merchant_CandidateList, RetFun);
	CHECK(node2 != NULL, LBL_TREE_ERROR);
	node = TlvTree_GetFirstChild(node2);
	CHECK(node != NULL, LBL_TREE_ERROR);
	UI_MenuReset();
	do
	{
		uint8 RangeName[30];
		int RangeID;

		if(TlvTree_GetTag(node) != TAG_CURRENCY)
		{
			memset(RangeName, 0, 30);
			memcpy(RangeName, TlvTree_GetData(node), TlvTree_GetLength(node));
			RangeID = TlvTree_GetTag(node);


		if(TranID == TRAN_C2P && RangeID==8)
			UI_MenuAddItem((char*)RangeName, RangeID);
		if(TranID != TRAN_C2P && RangeID!=8)
			UI_MenuAddItem((char*)RangeName, RangeID);


		}
		node = TlvTree_GetNext(node);
	}while(node != NULL);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_CARD), tConf.TimeOut, 0);
	//- MERCHSELECT


		node = TlvTree_AddChild(Tree_Tran, TAG_RANGE_ID, &RetFun, LTAG_UINT32);
		CHECK(node != NULL, LBL_TREE_ERROR);

		TABLE_MERCHANT Merchant;
		uint32 HostID;
		RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
		//UI_SetLine_2Texts(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), Merchant.Name);

		// add host id to tree
		HostID = Merchant.HostID;
		node = TlvTree_AddChild(Tree_Tran, TAG_HOST_ID, &HostID, LTAG_UINT32);
		CHECK(node != NULL, LBL_TREE_ERROR);

		// add batch file name to tree
		node = TlvTree_AddChild(Tree_Tran, TAG_BATCH_FILE_NAME, Merchant.BatchName, strlen(Merchant.BatchName));
		CHECK(node != NULL, LBL_TREE_ERROR);

		// add currency
		node = TlvTree_AddChild(Tree_Tran, TAG_CURRENCY, &Merchant.Currency, LTAG_UINT8);
		CHECK(node != NULL, LBL_TREE_ERROR);

		//add batch number eugenio
		RetFun = Tlv_SetTagValueInteger(Tree_Tran, TAG_BATCH_NUMBER, Merchant.BatchNumber, LTAG_INT32);
		CHECK(RetFun == RET_OK, LBL_TREE_ERROR);


		RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
		node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
		EntryMode = *(uint8*)TlvTree_GetData(node);

		if (EntryMode== ENTRY_MODE_MANUAL) {
			CHECK(!(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_NO_MATCH);
		}
		//CHECK(EntryMode != ENTRY_MODE_MANUAL && !(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_NO_MATCH);


	TlvTree_Release(Range_CandidateList);
	TlvTree_Release(Merchant_CandidateList);
	return STM_RET_NEXT;

LBL_TREE_ERROR:
	//+ TREEMEM @mamata 5/05/2016
	if(Range_CandidateList != NULL)
	{
		TlvTree_Release(Range_CandidateList);
	}
	if(Merchant_CandidateList != NULL)
	{
		TlvTree_Release(Merchant_CandidateList);
	}
	//- TREEMEM
	return STM_RET_ERROR;

LBL_ERROR_RANGECL:
	TlvTree_Release(Range_CandidateList);
	TlvTree_Release(Merchant_CandidateList);
	switch(RetFun)
	{

		case RET_NOK:
			if(TranID == TRAN_C2P)
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_SUPPORTED_TELF), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			else
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_SUPPORTED_CARD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			break;
		default:
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_TRAN), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			break;
	}

	return STM_RET_CANCEL;

LBL_CANCEL:
//+ TREEMEM @mamata 5/05/2016
	if(Range_CandidateList != NULL)
	{
		TlvTree_Release(Range_CandidateList);
	}
	if(Merchant_CandidateList != NULL)
	{
		TlvTree_Release(Merchant_CandidateList);
	}
	//- TREEMEM
	return STM_RET_CANCEL;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;

LBL_ERROR_CIERRE:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_AMOUNT_LOTE), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

	LBL_NO_MATCH:
		UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_INVALID_CARD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, 30);
		return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CHECK_EXP_DATE
 * Description:		Post function for STEP_CHECK_EXP_DATE
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 * Notes:
 */
int Post_STEP_CHECK_EXP_DATE(char *Result)
{
	TLV_TREE_NODE node;
	TABLE_RANGE Range;
	int RetFun;
	uint8 *CardDate;
	int CardYear;
	int CardMont;
	char tmpbuff[3];

	DATE stCurrentDate;
	char year[2+1] 	= { 0x00, };
	char month[2+1]	= { 0x00, };
	char day[2+1]	= { 0x00, };

	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// verify if is necessary to validate expiration date
	if(!(Range.Flags1 & RANGE_FLAG1_VERIFY_EXP_DATE))
		return STM_RET_NEXT;

	Telium_Read_date(&stCurrentDate);

	memcpy(year, stCurrentDate.year, 2 );
	memcpy(month, stCurrentDate.month, 2 );
	memcpy(day, stCurrentDate.day, 2 );

	// get card expiration date
	node = TlvTree_Find(Tree_Tran, TAG_EXP_DATE, 0);
	CHECK(node !=NULL, LBL_ERROR);
	CardDate = TlvTree_GetData(node);
	// convert card date to int
	memset(tmpbuff, 0, 3);
	memcpy((char*)tmpbuff, CardDate, 2);
	CardYear = atoi(tmpbuff);// + 2000;
	memcpy(tmpbuff, &CardDate[2], 2);
	CardMont = atoi(tmpbuff);

	//validate year
	CHECK(CardYear >= atoi(year), LBL_EXPIRED_CARD);
	// validate month if necessary
	if(CardYear == atoi(year))
	{
		CHECK(CardMont >= atoi(month), LBL_EXPIRED_CARD);
	}

	return STM_RET_NEXT;


LBL_ERROR:
	return STM_RET_ERROR;

LBL_EXPIRED_CARD:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_EXPIRED_CARD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_CVV2
 * Description:		Pre function for STEP_CVV2, verify if CVV2 is required
 * 					and show Menu "CVV2 type"
 * Parameters:
 * Return:
 * Notes:
 */
bool Pre_STEP_CVV2(void)
{
	TLV_TREE_NODE node;
	TABLE_RANGE Range;
	int RetFun;
	uint8 EntryMode;
	uint8 CVV2_Type;
	uint16 TranID;
	uint32 RangID;

 	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	if(TranID == TRAN_VOID)
   {
		node = TlvTree_Find(Tree_Tran, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

		if (RangID == 8)
		return FALSE;

   }

	// verify is a manual entry
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
	CHECK( (EntryMode == ENTRY_MODE_MANUAL) || (EntryMode == ENTRY_MODE_SWIPE), LBL_NEXT_STEP);

	// get range data
	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);

	// verify if cvv2 is required
	//CHECK((Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_NEXT_STEP);


	// cvvs is required, show CVV2 menu
//	UI_MenuReset();
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO_PRESENT), 0);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_PRESENT), 1);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_UNREADABLE), 2);
//	UI_MenuAddItem(mess_getReturnedMessage(MSG_NO_PROVIDED), 3);
//	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_CVV2), tConf.TimeOut, 1);
//	CHECK(RetFun >= 0, LBL_CANCEL);
//
	CVV2_Type = 1;
	node = TlvTree_AddChild(Tree_Tran, TAG_CVV2_TYPE, &CVV2_Type, LTAG_UINT8);
	CHECK(node != NULL, LBL_ERROR);
//
	// if use select that cvv2 is present, run step
	if(CVV2_Type == 1)
	{
		UI_ClearLine(UI_LINE2);
		UI_ClearLine(UI_LINE3);
		UI_ClearLine(UI_LINE4);
		UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_CVV2), UI_ALIGN_CENTER);
		return TRUE;
	}

	// other wise, goto next step
	return FALSE;


LBL_NEXT_STEP:
	return FALSE;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_TAG_TRAN_ID_NOTFOUND:
	return STM_RET_ERROR;
//LBL_CANCEL:
//	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CVV2
 * Description:		Pos funciton for STEP_CVV2
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 * - STM_RET_NEXT
 * - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_CVV2(char *Result)
{
	uint8 tmpCVV2[4];
	uint8 tmpCVV2Len;
	TLV_TREE_NODE node;

	// calculate cvv2 length
	tmpCVV2Len = strlen(Step_Buffer);
	if(tmpCVV2Len > 4)
		tmpCVV2Len = 4;

	// copy cvv2
	memset(tmpCVV2, 0x20, 4);
	//memcpy(&tmpCVV2[4-tmpCVV2Len], Step_Buffer, tmpCVV2Len);
	memcpy(tmpCVV2, Step_Buffer, tmpCVV2Len);

	// add TAG_CVV2
	node = TlvTree_AddChild(Tree_Tran, TAG_CVV2, &tmpCVV2, 4);
	CHECK(node != NULL, LBL_ERROR);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_LAST_4_PAN
 * Description:		Pre function for STEP_LAST_4_PAN
 * 					verify if entry mode is swipe and if step is enable
 * 					in range table
 * Parameters:
 * Return:
 * Notes:
 */
bool Pre_STEP_LAST_4_PAN(void)
{
	TLV_TREE_NODE node;
//	TABLE_RANGE Range;
	uint8 EntryMode;
//	int RetFun;

	// verify entry mode is swipe
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
	CHECK(EntryMode == ENTRY_MODE_SWIPE, LBL_NEXT_STEP);

	// verify if is enable prompt of last 4 of pan
	// get range data
//	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
//	CHECK(Range.Flags1 & RANGE_FLAG1_LAST_4_PAN, LBL_NEXT_STEP);

	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_LAST_4), UI_ALIGN_CENTER);
	return TRUE;

LBL_NEXT_STEP:
	return FALSE;

LBL_ERROR:
	return STM_RET_ERROR;

}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LAST_4_PAN
 * Description:		Post function for STEP_LAST_4_PAN
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 * - STM_RET_NEXT
 * - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_LAST_4_PAN(char *Result)
{
	TLV_TREE_NODE node;
	uint8 tmpPAN[25];

	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	memset(tmpPAN, 0, 25);
	memcpy(tmpPAN, TlvTree_GetData(node), TlvTree_GetLength(node));

	if(memcmp(Step_Buffer, (char*)&tmpPAN[strlen((char*)tmpPAN) - 4], 4) == 0)
		return STM_RET_NEXT;

	memset(Step_Buffer, 0, L_buffer);
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_LAST_4), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_AGAIN;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CHECK_SERVICE_CODE
 * Description:		Post function for STEP_CHECK_SERVICE_CODE
 * Parameters:
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 * - STM_RET_NEXT
 * - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_CHECK_SERVICE_CODE(char *Result)
{
	TABLE_MERCHANT Merchant;
	TABLE_HOST Host;
	int RetFun;
	uint8 EntryMode;
	TLV_TREE_NODE node;
	uint8 ServiceCode[3];

	//+ SCODE @mamata Dec 16, 2014
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
	CHECK(EntryMode == ENTRY_MODE_SWIPE, LBL_NEXT_STEP);
	//+ EMV09 @Mario Jun 22, 2015
	//d CHECK(EntryMode == ENTRY_MODE_MANUAL, LBL_NEXT_STEP);
	//- EMV09
	//- SCODE

	// get service code
	node = TlvTree_Find(Tree_Tran, TAG_SERVICE_CODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(ServiceCode, TlvTree_GetData(node), 3);

	//+ SCODE @mamata Dec 16, 2014
	//d node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	//d CHECK(node != NULL, LBL_ERROR);
	//d memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
	//d CHECK(EntryMode == ENTRY_MODE_SWIPE, LBL_NEXT_STEP);
	//- SCODE

	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	// get Host data
	RetFun = TM_FindRecord(TAB_HOST, &Host, Merchant.HostID);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// verify is a EMV host
	CHECK(Host.Flags1 & HOST_FLAG1_EMV, LBL_NEXT_STEP);

	// if the transaction isn't a fallback, then verify service code
	node = TlvTree_Find(Tree_Tran, TAG_FALLBACK, 0);
	CHECK(node == NULL, LBL_FALLBACK);

	// If is a chip card, return to STEP_GET_CARD
	if(		ServiceCode[0] == '2'
		||	ServiceCode[0] == '6'
	  )
	{
		uint8 ForceIncert = 1;
//		RetFun = ClearCardData();
		CHECK(RetFun == RET_OK, LBL_ERROR);

		node = TlvTree_AddChild(Tree_Tran, TAG_FORCE_INSERT, &ForceIncert, LTAG_UINT8);
		CHECK(node != NULL, LBL_ERROR);
		buzzer(25);

		return STEP_START_PAYMENT;
	}

LBL_NEXT_STEP:
	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_FALLBACK:
	if(		ServiceCode[0] != '2'
		&&	ServiceCode[0] != '6'
	  )
		TlvTree_Release(node);
	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_BASE_AMOUNT
 * Description:		Pre function for STEP_BASE_AMOUNT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_BASE_AMOUNT(void)
{
	int RetFun;
	TLV_TREE_NODE node;
	uint8 Currency = 1;
	//char AuxLine[30];
	int16 MessageID;

	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);
	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if(node != NULL)
		memcpy((char*)&MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MessageID), UI_ALIGN_CENTER);


	//+ DOTRAN_PCL @jbotero
	CURRENCY_INFO stCurrencyInfo;
	amount lBase = 0;

	Table_GetCurrencyInfo(Tree_Tran, &stCurrencyInfo);

	if(IsPCLDoTransaction())
	{
		RetFun = Tlv_GetTagValue(stPclTransFields, PCL_BASE_AMOUNT, &lBase, NULL);
		if(RetFun == RET_OK)
		{
			if(stCurrencyInfo.Type == CURRENCY_LOCAL)
				lBase = lBase/100;

			Uint64_to_Ascii(lBase, Step_Buffer, _FORMAT_TRIM, NULL, '0');

			//Change step configuration, to Force Post_step to execute amount validations
			STM_CustomInputFields(0, 0, 0, INPUT_NONE);

			return TRUE;
		}
	}
	//- DOTRAN_PCL

	//UI_ClearLine(UI_LINE2);

	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_AMOUNT), UI_ALIGN_CENTER);

	node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
	if(node != NULL)
		memcpy(&Currency,TlvTree_GetData(node), LTAG_UINT8);

	if(Currency == 1)
		STM_SetCurrency(tTerminal.CurrSymbol_Local);
	else
		STM_SetCurrency(tTerminal.CurrSymbol_Dollar);

	// Display application Label
	/*node = TlvTree_Find(Tree_Tran, TAG_EMV_APP_LBL, 0);
	if (node != NULL)
	{
		memset(AuxLine, 0x00, 30);
		memcpy(AuxLine, TlvTree_GetData(node), TlvTree_GetLength(node));
		UI_SetLine(UI_LINE2, AuxLine, UI_ALIGN_CENTER);
	}*/

	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_BASE_AMOUNT
 * Description:		Post function for STEP_BASE_AMOUNT
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_AGAIN
 * Notes:
 */
int Post_STEP_BASE_AMOUNT(char *Result)
{
//	TABLE_MERCHANT Merchant;
	amount Amount;
	int RetFun;
	TLV_TREE_NODE node;
//	BATCH BatchRecord;
	amount Total;
	amount TotalLote;
	TRAN_INFO TranInfo;
//	TOTALS Totals;
	uint16 TranID;
//	bool reporte=FALSE;

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	// convert amount
	Amount=0;
	Amount = atof(Step_Buffer);
	TotalLote=0;
	Total=0;

	RetFun = GetTransactionInfo(TranID, &TranInfo);
	CHECK(RetFun == RET_OK, LBL_ERROR);


	// verify max amount
//	if(Merchant.Amount_Max != 0)
//		CHECK(Amount <= Merchant.Amount_Max, LBL_BIGER_AMOUNT);
//
//	// verify max amount for LOTE -- JR07092020 obliga realizar cierre
//	if(Merchant.Amount_MaxDigtisLote != 0)
//	CHECK(TotalLote <= Merchant.Amount_MaxDigtisLote, LBL_BIGER_AMOUNT_LOTE);

	// add base amount to tree
	node = TlvTree_AddChild(Tree_Tran, TAG_BASE_AMOUNT, &Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_ERROR);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

//LBL_BIGER_AMOUNT:
//	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_AMOUNT), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
//	memset(Step_Buffer, 0, L_buffer);
//	return STM_RET_AGAIN;
//
//LBL_BIGER_AMOUNT_LOTE:
//	    TM_FindFirst(TAB_TERMINAL, &tTerminal);
//		tTerminal.Flags1 |= TERM_FLAG1_SETTLE_OBLIGADO;
//		TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
//
//	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_AMOUNT_LOTE), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
//	return STM_RET_ERROR;

LBL_TAG_TRAN_ID_NOTFOUND:
	return RET_NOK;
//
//LBL_ERROR_NOPERMITIDA:
//	UI_ShowMessage(NULL,
//		mess_getReturnedMessage(MSG_RC_TRANSACTION_NOT_ALLOWED),"",GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
//		utilWaitToRemoveChipCard();
//		SetIdleHeaderFooter();
//
//		return -3;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TAX_AMOUNT
 * Description:		Pre function for STEP_TAX_AMOUNT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 *  - FALSE
 * Notes:
 */
bool Pre_STEP_TAX_AMOUNT(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
	bool ReturnValue=TRUE;
	amount TaxAmount;
	amount BaseAmount;
	TLV_TREE_NODE node;

	//+ DOTRAN_PCL @jbotero
	amount lTax = 0;
	CURRENCY_INFO stCurrencyInfo;
	Table_GetCurrencyInfo(Tree_Tran, &stCurrencyInfo);

	if(IsPCLDoTransaction())
	{

		RetFun = Tlv_GetTagValue(stPclTransFields, PCL_TAX1_AMOUNT, &lTax, NULL);
		if(RetFun == RET_OK)
		{
			if(stCurrencyInfo.Type == CURRENCY_LOCAL)
				lTax = lTax/100;

			Uint64_to_Ascii(lTax, Step_Buffer, _FORMAT_TRIM, NULL, '0');

			//Change step configuration, to Force Post_step to execute amount validations
			STM_CustomInputFields(0, 0, 0, INPUT_NONE);

			return TRUE;
		}
	}
	//- DOTRAN_PCL
	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_FASLE);

	CHECK(Merchant.Flags1 & MERCH_FLAG1_TAX, LBL_FASLE);

	switch(Merchant.TAX_type)
	{
		case 0: // nomral tax
			UI_ClearLine(UI_LINE2);
			UI_ClearLine(UI_LINE3);
			UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_TAX), UI_ALIGN_CENTER);
			break;
		case 1: // automatic tax
			// get base amount
			node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
			CHECK(node != NULL, LBL_FASLE);
			memcpy((char*)&BaseAmount, TlvTree_GetData(node), LTAG_AMOUNT);
			// calculate tax
			TaxAmount = (BaseAmount * Merchant.TAX_Max_Percent) / 100;
			// add tax to tree
			node = TlvTree_AddChild(Tree_Tran, TAG_TAX_AMOUNT, &TaxAmount, LTAG_AMOUNT);
			ReturnValue = FALSE;
			break;
	}
	return ReturnValue;

LBL_FASLE:
	return FALSE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TAX_AMOUNT
 * Description:		Post funciton for STEP_TAX_AMOUNT
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_AGAIN
 *  Notes:
 */
int Post_STEP_TAX_AMOUNT(char *Result)
{
	TABLE_MERCHANT Merchant;
	amount TaxAmount;
	amount BaseAmount;
	amount TaxPercent;
	int RetFun;
	TLV_TREE_NODE node;

	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	if(Merchant.TAX_type == 0)
	{
		// convert tax amount
		TaxAmount = atof(Step_Buffer);

		// get base amount
		node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
		CHECK(node != NULL, LBL_ERROR);
		memcpy((char*)&BaseAmount, TlvTree_GetData(node), LTAG_AMOUNT);

		// calculate tax percent
		TaxPercent = (TaxAmount * 10000) / BaseAmount;

		if(TaxPercent > (Merchant.TAX_Max_Percent*100))
		{
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_TAX), NULL, GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
			memset(Step_Buffer, 0, L_buffer);
			return STM_RET_AGAIN;
		}

		node = TlvTree_Find(Tree_Tran, TAG_TAX_AMOUNT, 0);
		if(node == NULL)
		{
			node = TlvTree_AddChild(Tree_Tran, TAG_TAX_AMOUNT, &TaxAmount, LTAG_AMOUNT);
			CHECK(node != NULL, LBL_ERROR);
		}
		else
		{
			RetFun = TlvTree_SetData(node, &TaxAmount, LTAG_AMOUNT);
			CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
		}
	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TIP_AMOUNT
 * Description:		Pre function for STEP_TIP_AMOUNT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 *  - FALSE
 * Notes:
 */
bool Pre_STEP_TIP_AMOUNT(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
	bool ReturnValue=TRUE;
	amount TipAmount;
	amount BaseAmount;
	TLV_TREE_NODE node;

	//+ DOTRAN_PCL @jbotero
	amount lTip = 0;
	CURRENCY_INFO stCurrencyInfo;
	Table_GetCurrencyInfo(Tree_Tran, &stCurrencyInfo);
	TABLE_RANGE Range;
		RetFun = Get_TransactionRangeData(&Range, Tree_Tran);

	CHECK(tTerminal.Comercio & RESTAURANT && !(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_FASLE);


	if(IsPCLDoTransaction())
	{

		RetFun = Tlv_GetTagValue(stPclTransFields, PCL_TIP_AMOUNT, &lTip, NULL);
		if(RetFun == RET_OK)
		{
			if(stCurrencyInfo.Type == CURRENCY_LOCAL)
				lTip = lTip/100;

			Uint64_to_Ascii(lTip, Step_Buffer, _FORMAT_TRIM, NULL, '0');

			//Change step configuration, to Force Post_step to execute amount validations
			STM_CustomInputFields(0, 0, 0, INPUT_NONE);

			return TRUE;
		}
	}
	//- DOTRAN_PCL
	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_FASLE);


//	CHECK(tTerminal.Flags1 & TERM_FLAG1_SERVER, LBL_FASLE);
//RESTAURANT
	switch(Merchant.TIP_type)
	{
		case 0: // nomral tip
			UI_ClearLine(UI_LINE2);
			UI_ClearLine(UI_LINE3);
			UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_TIP), UI_ALIGN_CENTER);
			break;
		case 1: // automatic tip
			// get base amount
			node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
			CHECK(node != NULL, LBL_FASLE);
			memcpy((char*)&BaseAmount, TlvTree_GetData(node), LTAG_AMOUNT);
			// calculate tip
			TipAmount = (BaseAmount * Merchant.TIP_Max_Percent) / 100;
			// add tip to tree
			node = TlvTree_AddChild(Tree_Tran, TAG_TIP_AMOUNT, &TipAmount, LTAG_AMOUNT);
			ReturnValue = FALSE;
			break;
	}
	return ReturnValue;

LBL_FASLE:
	return FALSE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TIP_AMOUNT
 * Description:		Post function for STEP_TIP_AMOUNT
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_AGAIN
 *  Notes:
 */
int Post_STEP_TIP_AMOUNT(char *Result)
{
	TABLE_MERCHANT Merchant;
	amount TipAmount;
	amount BaseAmount;
	amount TipPercent;
	int RetFun;
	TLV_TREE_NODE node;



	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

//	CHECK(tTerminal.Comercio & RESTAURANT, LBL_FASLE);

	if(Merchant.TIP_type == 0)
	{
		// convert tip amount
		TipAmount = atof(Step_Buffer);

		// get base amount
		node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
		CHECK(node != NULL, LBL_ERROR);
		memcpy((char*)&BaseAmount, TlvTree_GetData(node), LTAG_AMOUNT);

		// calculate tip percent
		if(BaseAmount>0)
			TipPercent = (TipAmount * 10000) / BaseAmount;
		else
			TipPercent = (TipAmount * 10000);

		if(TipPercent > (Merchant.TIP_Max_Percent*100))
		{
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_TIP), NULL, GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
			memset(Step_Buffer, 0, L_buffer);
			return STM_RET_AGAIN;
		}

		node = TlvTree_Find(Tree_Tran, TAG_TIP_AMOUNT, 0);
		if(node == NULL)
		{
			node = TlvTree_AddChild(Tree_Tran, TAG_TIP_AMOUNT, &TipAmount, LTAG_AMOUNT);
			CHECK(node != NULL, LBL_ERROR);
		}
		else
		{
			RetFun = TlvTree_SetData(node, &TipAmount, LTAG_AMOUNT);
			CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
		}

	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

//	LBL_FASLE:
//		return STM_RET_NEXT;


}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TIP_AMOUNT_RESTAURANT
 * Description:
 * Parameters:
 *  - none
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_TIP_AMOUNT_RESTAURANT(char *Result)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
//	bool ReturnValue=TRUE;
	amount TipAmount;
	amount BaseAmount;
	TLV_TREE_NODE node;

	//+ DOTRAN_PCL @jbotero
	amount lTip = 0;
	CURRENCY_INFO stCurrencyInfo;
	Table_GetCurrencyInfo(Tree_Tran, &stCurrencyInfo);
	TABLE_RANGE Range;
		RetFun = Get_TransactionRangeData(&Range, Tree_Tran);

	CHECK(tTerminal.Comercio & RESTAURANT && !(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_FALSE);


	if(IsPCLDoTransaction())
	{

		RetFun = Tlv_GetTagValue(stPclTransFields, PCL_TIP_AMOUNT, &lTip, NULL);
		if(RetFun == RET_OK)
		{
			if(stCurrencyInfo.Type == CURRENCY_LOCAL)
				lTip = lTip/100;

			Uint64_to_Ascii(lTip, Step_Buffer, _FORMAT_TRIM, NULL, '0');

			//Change step configuration, to Force Post_step to execute amount validations
			STM_CustomInputFields(0, 0, 0, INPUT_NONE);

			return STM_RET_NEXT;
		}
	}
	//- DOTRAN_PCL
	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);


//	CHECK(tTerminal.Flags1 & TERM_FLAG1_SERVER, LBL_FASLE);
//RESTAURANT
	//switch(Merchant.TIP_type)
//	{
	//	case 1: // automatic tip
			// get base amount
			node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
			CHECK(node != NULL, LBL_ERROR);
			memcpy((char*)&BaseAmount, TlvTree_GetData(node), LTAG_AMOUNT);
			// calculate tip
			TipAmount = (BaseAmount * Merchant.TIP_Max_Percent) / 100;
			// add tip to tree
			node = TlvTree_AddChild(Tree_Tran, TAG_TIP_AMOUNT, &TipAmount, LTAG_AMOUNT);
		//	break;
//	}
	//return STM_RET_NEXT;


	LBL_FALSE:
		return STM_RET_NEXT;

	LBL_ERROR:
		return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CONFIRM_TOTAL
 * Description:		Post function for STEP_CONFIRM_TOTAL
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - LBL_CANCEL
 *  - LBL_ERROR
 * Notes:
 */
int Post_STEP_CONFIRM_TOTAL(char *Result)
{
	TLV_TREE_NODE node;
	amount Amount;
	amount Total = 0;
	int RetFun;
	char tmpbuff[30];
	uint8 Line;
	bool AddTotalLIne = FALSE;
	bool NegativeAmount = FALSE;
	uint16 TranID;
	TABLE_MERCHANT Merchant;
	char CurrencySymbol[5];

	TABLE_RANGE Range;

	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
	CHECK(tTerminal.Comercio & RESTAURANT && !(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_FASLE);

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);

	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);


	memset(CurrencySymbol, 0, 5);
	if( Merchant.Currency == 1 )
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
	else
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);


	// get transaciton id
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	if(node != NULL)
	{
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		if(TranID == TRAN_REFUND)
			NegativeAmount = TRUE;
	}

	// Set Base Amount line
	// get TAG_BASE_AMOUNT from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
	Total+=Amount;
	// formant amount
	Line = UI_LINE1;
	memset(tmpbuff, 0, 30);
	Format_Amount(Amount, tmpbuff, CurrencySymbol, NegativeAmount);
	// add line
	UI_SetLine_2Texts(Line, mess_getReturnedMessage(MSG_AMOUNT), tmpbuff);

	// Set Tax Amount line
	if(Merchant.Flags1 & MERCH_FLAG1_TAX)
	{
		node = TlvTree_Find(Tree_Tran, TAG_TAX_AMOUNT, 0);
		if(node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
			Line++;
			memset(tmpbuff, 0, 30);
			Format_Amount(Amount, tmpbuff, CurrencySymbol, NegativeAmount);
			UI_SetLine_2Texts(Line, mess_getReturnedMessage(MSG_TAX), tmpbuff);
			AddTotalLIne = TRUE;
		}
	}

	// Set Tax Amount line
//	if((Merchant.Flags1 & MERCH_FLAG1_TIP)||(tTerminal.Flags1 & TERM_FLAG1_SERVER))
	if((Merchant.Flags1 & MERCH_FLAG1_TIP)||(tTerminal.Comercio & RESTAURANT))
	{
		node = TlvTree_Find(Tree_Tran, TAG_TIP_AMOUNT, 0);
		if(node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
			Line++;
			memset(tmpbuff, 0, 30);
			Format_Amount(Amount, tmpbuff, CurrencySymbol, NegativeAmount);
			UI_SetLine_2Texts(Line, mess_getReturnedMessage(MSG_TIP), tmpbuff);
			AddTotalLIne = TRUE;
		}
	}

	// Add total line if necessary
	if(AddTotalLIne)
	{
		Line++;
		memset(tmpbuff, 0, 30);
		Format_Amount(Total, tmpbuff, CurrencySymbol, NegativeAmount);
		UI_SetLine_2Texts(Line, mess_getReturnedMessage(MSG_TOTAL), tmpbuff);
	}

	RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CONFIRMATION), tConf.TimeOut);
	CHECK(RetFun == RET_OK, LBL_CANCEL);

LBL_FASLE:
	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;

LBL_ERROR:
	return STM_RET_ERROR;



}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PREDIAL
 * Description:		Perform connection to host
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - LBL_ERROR
 * Notes:
 */
int Post_STEP_PREDIAL(char *Result)
{
	TLV_TREE_NODE node;
	ewlObject_t *ewl_handle = NULL;
	int RetFun;
	uint16 TranOptions;
	uint8 EntryMode;

	//Stop the CLESS detection when is opened
	ewl_handle = EWL_GetEmvHandle();
	if (ewl_handle != NULL)
	{
		ewlClessStopDetection(ewl_handle);
	}


	// Get transaction options
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranOptions = *(uint16*)TlvTree_GetData(node);

	// verify if is a OFFLINE transaction
	CHECK(!(TranOptions & FTRAN_OFFLINE), LBL_NEXT)

	// Get entry mode
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	EntryMode = *(uint8*)TlvTree_GetData(node);

	// verify if not a EMV transactions
	CHECK(EntryMode != ENTRY_MODE_CHIP, LBL_NEXT);

	RetFun = HostMain_Connect(Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// add TAG_PREDIAL to tree
	RetFun = 1;
	node = TlvTree_AddChild(Tree_Tran, TAG_PREDIAL, &RetFun, LTAG_UINT8);

LBL_NEXT:
	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_END_COMMUNICATION
 * Description:		End communication
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 * Notes:
 */
int Post_STEP_END_COMMUNICATION(char *Result)
{

	LLC_EndConnection();
	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SEND_RECEIVE
 * Description:		Post function for STEP_SEND_RECEIVE
 * Parameters:
 *  - char *Result - where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - LBL_ERROR
 * Notes:
 */
int Post_STEP_SEND_RECEIVE(char *Result)
{
	int RetFun;

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_PLEASE_WAIT), UI_ALIGN_CENTER);

	RetFun = HostMain_SendReceive_Transaction(Tree_Tran);

	//+ REVERSE01 @mamata Dec 16, 2014
	//d //+ ISOMAIN01 @mamata Aug 14, 2014
	//d if(RetFun == HOST_RET_REVERSE_ERROR)
	//d {
	//d     UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_RC_TIME_OUT), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tTerminal.TimeOut);
	//d     return STM_RET_CANCEL;
	//d }
	//d //- ISOMAIN01
	//- REVERSE01

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PROCESS_RESPONSE
 * Description:		Post function for STEP_PROCESS_RESPONSE
 * Parameters:
 *  - char *Result - where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - LBL_ERROR
 * Notes:
 */
int Post_STEP_PROCESS_RESPONSE(char *Result)
{
	TLV_TREE_NODE node;
	uint16 TranOptions;
	uint16 TranStatus;
	uint16 RC_MessageID = 0;
	char Message[30];
	amount BaseAmount= 0;
//	amount TotalAmount = 0;
	int RetFun;
	char CAmount[20];
	char valor[20];
	char CurrencySymbol[LZ_TERM_CURR_SYMBOL];
	uint8 Currency;
	char CStan[4+1];
	uint32 stan;
	uint16 TranID;
	char Dref[11];
	TABLE_MERCHANT tMerchAux;
	uint8 AutoCode[6];
	uint8 CodeResp[2];


	memset(CAmount, 0x00, sizeof(CAmount));
	memset(valor, 0x00, sizeof(valor));
	memset(CStan, 0x00, sizeof(CStan));
	memset(Dref, 0x00, sizeof(Dref));


	// Get transaction options
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranOptions = *(uint16*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);



	// process response code
	node = TlvTree_Find(Tree_Tran, TAG_HOST_RESPONSE_CODE, 0);
	memcpy(CodeResp, TlvTree_GetData(node), 2);
	if(node != NULL)
		RetFun = HostMain_Process_ResponseCode(Tree_Tran, TlvTree_GetData(node), TranOptions, TranStatus);
	else
		RetFun = HostMain_Process_ResponseCode(Tree_Tran, (uint8*)"E1", TranOptions, TranStatus);
	CHECK(RetFun == RET_OK, LBL_ERROR);



	//code autorizacion
	node = TlvTree_Find(Tree_Tran, TAG_HOST_AUTH_NUMBER, 0);
	    if(node != NULL)
	    {
	    	memcpy(AutoCode, TlvTree_GetData(node), 6);
	    	if((memcmp(CodeResp, "00", 2) == 0 ) && ((memcmp(AutoCode, "      ", 6) == 0 ) || (memcmp(AutoCode, "000000", 6) == 0 )))
	    			{
						RetFun = Get_TransactionMerchantData(&tMerchAux, Tree_Tran);
						CHECK(RetFun == RET_OK, LBL_ERROR);
						tMerchAux.Flags1 |= MERCH_FLAG1_TX_NEGADA;
						TM_ModifyRecord(TAB_MERCHANT, &tMerchAux);

						memset(Message, 0, 30);
						memcpy(Message, "Negada", 6);
						UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_TRANSACTION), Message, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

						return STM_RET_CANCEL;
	    			}
	    }


	node = TlvTree_Find(Tree_Tran, TAG_RESPONSE_CODE_TEXT, 0);
	if(node != NULL)
		memcpy((char*)&RC_MessageID, TlvTree_GetData(node), LTAG_UINT16);

	// clear all lines
	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);

	node = TlvTree_Find(Tree_Tran, TAG_APPROVED, 0);
	if(node != NULL)
	{
		UI_SetLine(UI_LINE2, mess_getReturnedMessage(RC_MessageID), UI_ALIGN_CENTER);

		node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
		if(node != NULL)
		{
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		if (TranID != TRAN_TEST && TranID != TRAN_LOGON) {
		//EUGENIO JOSE
		node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
		CHECK(node != NULL, LBL_ERROR);
			memcpy(&Currency,TlvTree_GetData(node), LTAG_UINT8);
			if(Currency == 1)
				memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
			else
				memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);


			node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
			CHECK(node != NULL, LBL_ERROR);
			BaseAmount = *(amount*)TlvTree_GetData(node);
			Format_Amount(BaseAmount, CAmount, CurrencySymbol, FALSE);


		/*	TotalAmount = GetTotalAmount();
			if(TotalAmount > 0)
			Format_Amount(TotalAmount, CAmount, CurrencySymbol, FALSE);*/


			Telium_Sprintf(valor,"%s",CAmount);

		UI_SetLine(UI_LINE3,valor, UI_ALIGN_CENTER);

		// set authorization number
		node = TlvTree_Find(Tree_Tran, TAG_STAN, 0);
		CHECK(node != NULL, LBL_ERROR);
		stan = *(uint32*)TlvTree_GetData(node);

		Binasc((uint8*)CStan, stan, 4);
		Telium_Sprintf(Dref,"REF: %s",CStan);

		UI_SetLine(UI_LINE4,(char*)Dref, UI_ALIGN_CENTER);
		}}
		// show message
		UI_ShowInfoScreen(NULL);
		buzzer(30);
		Wait_Key(false,1);
//		beepOK;

		   //FLAG NEGADA
			RetFun = Get_TransactionMerchantData(&tMerchAux, Tree_Tran);
			CHECK(RetFun == RET_OK, LBL_ERROR);
			tMerchAux.Flags1 &= ~MERCH_FLAG1_TX_NEGADA;
			TM_ModifyRecord(TAB_MERCHANT, &tMerchAux);


	}
	else
	{
		//FLAG NEGADA TX
		RetFun = Get_TransactionMerchantData(&tMerchAux, Tree_Tran);
		CHECK(RetFun == RET_OK, LBL_ERROR);
		tMerchAux.Flags1 |= MERCH_FLAG1_TX_NEGADA;
		TM_ModifyRecord(TAB_MERCHANT, &tMerchAux);

		if(RC_MessageID)
		{

			// set host response code text
			UI_ShowMessage(NULL, mess_getReturnedMessage(RC_MessageID), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, 2);

			if(RC_MessageID == MSG_RC_COMMUNICATION_ERROR)
				return STM_RET_CANCEL;

			if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECHAZADO))
				return STM_RET_CANCEL;

			return STM_RET_NEXT;//CAMBIO EUGENIO


		}
		else
		{
			node = TlvTree_Find(Tree_Tran, TAG_HOST_RESPONSE_CODE, 0);
			if(node != NULL)
			{
				memset(Message, 0, 30);
				memcpy(Message, "RC: ", 4);
				memcpy(&Message[4], TlvTree_GetData(node), 2);
			}

			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_TRANSACTION), Message, GL_ICON_ERROR, GL_BUTTON_ALL, 2);

			return STM_RET_CANCEL;
		}

	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PRE_PRINT_RECEIPT
 * Description:		Post function for STEP_PRE_PRINT_RECEIPT
 * Parameters:
 *  - char *Result - where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - LBL_ERROR
 * Notes:
 */

int Post_STEP_PRE_PRINT_RECEIPT(char *Result){

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECEIPT))
			return STM_RET_NEXT;

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRE_PRINT))
			return STM_RET_NEXT;

	Print_Pre_TransactionReceipt(Tree_Tran);

		return STM_RET_NEXT;
}
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PRINT_RECEIPT
 * Description:		Post function for STEP_PRINT_RECEIPT
 * Parameters:
 *  - char *Result - where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - LBL_ERROR
 * Notes:
 */
int Post_STEP_PRINT_RECEIPT(char *Result)
{
	TABLE_RANGE Range;
	TLV_TREE_NODE node;
	int RetFun;
	int CurrencyCode,i=0;
	uint16 TranID;



// get transaciton id
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN);

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECEIPT))
		return STM_RET_NEXT;

	if(TranID != TRAN_C2P)
	{
		memset((char*)&Range, 0, L_TABLE_RANGE);
		RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
		CHECK(RetFun == RET_OK, LBL_FINISH);


		if(Range.Flags1 & RANGE_FLAG1_PROMPT_PIN){
			if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_DEBITO))
			return STM_RET_NEXT;
		}
		if(!(Range.Flags1 & RANGE_FLAG1_PROMPT_PIN)){
			if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_CREDITO))
				return STM_RET_NEXT;
		}
	}

	if(TranID != DUPLICATE && TranID != LAST_ANSWER)
	{
		node = TlvTree_Find(Tree_Tran, TAG_APPROVED, 0);
		if(node != NULL)
		{
			Print_TransactionReceipt(Tree_Tran, FALSE, FALSE);
			CurrencyCode=atol(tTerminal.NCopias);
			while(i<CurrencyCode){

				RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CLIENT_RECEIPT), tConf.TimeOut);
				CHECK(RetFun == RET_OK, LBL_FINISH);
				Print_TransactionReceipt(Tree_Tran, TRUE, TRUE);
				i++;;
			}


		}else{
			Print_Pre_TransactionDenied(Tree_Tran);

		}
	}
	else if(TranID == DUPLICATE)
	{
		Print_TransactionReceipt(Tree_Tran, FALSE, TRUE);
	}
	else if(TranID == LAST_ANSWER)
	{
		Print_TransactionReceipt(Tree_Tran, FALSE, FALSE);
	}

LBL_FINISH:
	return STM_RET_NEXT;

	LBL_NO_TRAN:
		UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_TRAN_BATCH), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_AUTH_NUMBER
 * Description:		Pre function to STEP_AUTH_NUMBER
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_AUTH_NUMBER(void)
{
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_AUTHCODE), UI_ALIGN_CENTER);

	return true;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_AUTH_NUMBER
 * Description:		Post function to STEP_AUTH_NUMBER
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
int Post_STEP_AUTH_NUMBER(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;

	// Add Auth Code to TAG
	node = TlvTree_Find(Tree_Tran, TAG_HOST_AUTH_NUMBER, 0);
	if(node != NULL)
	{
		RetFun=TlvTree_SetData( node, Step_Buffer, strlen(Step_Buffer) );
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
	}
	else
	{
		node=TlvTree_AddChild( Tree_Tran, TAG_HOST_AUTH_NUMBER, Step_Buffer, strlen(Step_Buffer) );
		CHECK(node != NULL, LBL_ERROR);
	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_INVOICE
 * Description:		Pre function to STEP_INVOICE
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_INVOICE(void)
{
	TLV_TREE_NODE node;
	int16 MessageID;
	int RetFun;

	if (IsPCLDoTransaction())
	{
		char TempBuffer[30];
		unsigned int MaxLn = sizeof(TempBuffer) - 1;

		memset(TempBuffer, 0x00, sizeof(TempBuffer));

		RetFun = Tlv_GetTagValue(stPclTransFields, PCL_INVOICE, TempBuffer,
				&MaxLn);
		if (RetFun == RET_OK && MaxLn > 0)
			strcpy(Step_Buffer, TempBuffer);

		//Change step configuration, to Force Post_step
		if (Step_Buffer[0] != NULL)
		{
			STM_CustomInputFields(0, 0, 0, INPUT_NONE);
			return TRUE;
		}
	}
	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if (node != NULL)
		memcpy((char*) &MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;

	// set title and prompt line
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MessageID), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_NUM_REFERENCE), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_INVOICE
 * Description:		Post function to STEP_INVOICE
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_INVOICE(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint32 Reference;
	uint16 TranStatus;
	uint16 TranOptions;
	TABLE_MERCHANT Merchant;
	uint16 TranID;
	uint16 IdMerchant;
	TABLE_HEADER TableHeader;

	// get transaciton id
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN);

	UI_ShowProcessScreen(mess_getReturnedMessage(MSG_SEARCHING_TRAN));

	// convert invoice to int
	Reference = atol(Step_Buffer);

	//id merchant con id tx
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_NO_TRAN);
	memcpy((char*)&IdMerchant, TlvTree_GetData(node), LTAG_UINT16);


	// find transaction in batch files
	RetFun = Batch_FindTransaction(Tree_Tran, Reference);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN)


	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	// validate if batch is not already settled
	CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);
	//compare tx con id ref mismo merchant
	CHECK (memcmp(&IdMerchant,&Merchant.Header.RecordID,1)==0,LBL_NO_TRAN);


	// get batch header
	RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	// validate if batch if not empty
	CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);

	if(TranID != DUPLICATE)
	{
	// get transaction options
		node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranOptions = *(uint16*)TlvTree_GetData(node);
		CHECK(TranOptions & FTRAN_VOID, LBL_CANNOT_VOID);

		// get transaction status
		node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranStatus = *(uint16*)TlvTree_GetData(node);
		CHECK(!(TranStatus & FSTATUS_VOIDED), LBL_VOIDED);
	}

	if(TranID == DUPLICATE)
	{
		node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranStatus = *(uint16*)TlvTree_GetData(node);
		CHECK(!(TranStatus & FSTATUS_NEED_REVERSE), LBL_NO_TRAN);
	}


	node = TlvTree_Find(Tree_Tran, TAG_BATCH_FILE_NAME, 0);
	if(node == NULL)
		node = TlvTree_AddChild(Tree_Tran, TAG_BATCH_FILE_NAME, &Merchant.BatchName, LZ_MERCH_BATCH_NAME);

	Tlv_SetTagValueInteger(Tree_Tran, TAG_TRAN_ID, TranID, LTAG_INT16);


	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_CANNOT_VOID:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_VOID), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_VOIDED:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_VOIDED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_NO_TRAN:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_TRAN_BATCH), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_DETAIL_REPORT), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_OK;

}



/* --------------------------------------------------------------------------
 * Function Name:	STEP_INVOICE_ADJUST
 * Description:		Post function to STEP_INVOICE
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_INVOICE_ADJUST(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint32 Reference;
	uint16 TranStatus;
	uint16 TranOptions;
	TABLE_MERCHANT Merchant;
	uint16 TranID;
//	uint16 IdMerchant;
	TABLE_HEADER TableHeader;

	// get transaciton id
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN);

	UI_ShowProcessScreen(mess_getReturnedMessage(MSG_SEARCHING_TRAN));

	// convert invoice to int
	Reference = atol(Step_Buffer);

	//id merchant con id tx
	/*node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_NO_TRAN);
	memcpy((char*)&IdMerchant, TlvTree_GetData(node), LTAG_UINT16);
	 */

	// find transaction in batch files
	RetFun = Batch_FindTransaction(Tree_Tran, Reference);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN)


	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	// validate if batch is not already settled
	CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);
	//compare tx con id ref mismo merchant
	//CHECK (memcmp(&IdMerchant,&Merchant.Header.RecordID,1)==0,LBL_NO_TRAN);


	// get batch header
	RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	// validate if batch if not empty
	CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);


	// get transaction options
		node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranOptions = *(uint16*)TlvTree_GetData(node);
		CHECK(TranOptions & FTRAN_VOID, LBL_CANNOT_VOID);

		// get transaction status
	/*	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranStatus = *(uint16*)TlvTree_GetData(node);
		CHECK(!(TranStatus & FSTATUS_NEED_ADJUST), LBL_ADJUSTED);*/


		// get transaction status
		node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranStatus = *(uint16*)TlvTree_GetData(node);
		CHECK(!(TranStatus & FSTATUS_VOIDED), LBL_VOIDED);


	node = TlvTree_Find(Tree_Tran, TAG_BATCH_FILE_NAME, 0);
	if(node == NULL)
		node = TlvTree_AddChild(Tree_Tran, TAG_BATCH_FILE_NAME, &Merchant.BatchName, LZ_MERCH_BATCH_NAME);

	Tlv_SetTagValueInteger(Tree_Tran, TAG_TRAN_ID, TranID, LTAG_INT16);


	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_CANNOT_VOID:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_VOID), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

/*LBL_ADJUSTED:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION_ADJUSTED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;*/
	LBL_VOIDED:
		UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION_ANULED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_CANCEL;

LBL_NO_TRAN:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_TRAN_BATCH), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_DETAIL_REPORT), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_OK;

}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_MERCHANT_PASSWORD
 * Description:		Pre function to STEP_MERCHANT_PASSWORD
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_MERCHANT_PASSWORD(void)
{
	TLV_TREE_NODE node;
	int32 MerchantID;

	// if tag no present don't execute step
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_FALSE);

	// get merchant id
	MerchantID = *(int32*)TlvTree_GetData(node);

	// verify ins't all merchant operation
	CHECK(MerchantID != 99999, LBL_FALSE);

	UI_ClearAllLines();
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_MERCHANT_PASSWORD), UI_ALIGN_CENTER);
	return TRUE;

LBL_FALSE:
	return FALSE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_MERCHANT_PASSWORD
 * Description:		Post function to STEP_MERCHANT_PASSWORD
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_AGAIN
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_MERCHANT_PASSWORD(char *Result)
{
	TABLE_MERCHANT Merchant;
	int RetFun;

	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	if(strcmp(Step_Buffer, Merchant.Password) == 0)
		return STM_RET_NEXT;

	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BAD_PASSWORD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_AGAIN;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_MERCHANT_PASSWORD
 * Description:		Post function to STEP_MERCHANT_PASSWORD
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_AGAIN
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_CONFIRM_VOID(char *Result)
{
	TLV_TREE_NODE node;
	amount Total=0;
	int RetFun;
	char tmpbuff[30];
	char CurrencySymbol[6];
	bool NegativeAmount = FALSE;
	uint16 TranID;
	uint16 TranStatus;
	char *PAN;
	uint8 Currency;
	//amount Amount;
	uint32 RangID;

	node = TlvTree_Find(Tree_Tran, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

	// get transaciton id
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	if(node != NULL)
	{
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		if(TranID == TRAN_REFUND)
			NegativeAmount = TRUE;
	}

	// get currency symbol
	memset(CurrencySymbol, 0, 6);
	node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
	Currency = *(uint8*)TlvTree_GetData(node);
	if( Currency == 1 )
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
	else
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);

	UI_ClearAllLines();

	// add title line
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if(node != NULL)
		TranID = *(uint16*)TlvTree_GetData(node);
	else
		TranID = 0;
	UI_SetLine_2Texts(UI_TITLE, mess_getReturnedMessage(MSG_TRAN_VOID), mess_getReturnedMessage(TranID));

	// get last 4 of pan
	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	if(node != NULL)
	{
		memset(tmpbuff, 0, 30);
		memcpy(tmpbuff, "**", 2);
		PAN = TlvTree_GetData(node) + TlvTree_GetLength(node) - 4;
		memcpy(&tmpbuff[2], PAN, 4);
	}

	if(RangID == 8)
   		UI_SetLine_2Texts(UI_LINE1, mess_getReturnedMessage(MSG_TELF), tmpbuff);
	else
		UI_SetLine_2Texts(UI_LINE1, mess_getReturnedMessage(MSG_CARD), tmpbuff);



	// add amount line
	memset(tmpbuff, 0, 30);

/*	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	if(TranStatus && FSTATUS_ADJUSTED){
		node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
		if (node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
		}*/
		//Total = GetTotalAmount();
	//}
	//else
		Total = GetTotalAmount();


	Format_Amount(Total, tmpbuff, CurrencySymbol, NegativeAmount);
	UI_SetLine_2Texts(UI_LINE2, mess_getReturnedMessage(MSG_AMOUNT), tmpbuff);

	RetFun = UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_VOID_COFIRMATION), tConf.TimeOut);
	CHECK(RetFun == RET_OK, LBL_CANCEL);

	// get tran status
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	// set void flag
	TranStatus |= FTRAN_REVERSE;
	RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);



	TranID = TRAN_REFUND;
    TlvTree_AddChild(Tree_Tran, TAG_TRAN_ID, &TranID, LTAG_UINT16);

	return STM_RET_NEXT;

LBL_CANCEL:
	return STM_RET_CANCEL;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_TAG_TRAN_ID_NOTFOUND:
		return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_ADJUST_BASE_AMOUNT
 * Description:		Pre function for STEP_ADJUST_BASE_AMOUNT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_ADJUST_BASE_AMOUNT(void)
{
	TLV_TREE_NODE node;
	amount BaseAmount = 0;
	uint8 Currency = 1;
	TABLE_MERCHANT Merchant;
	int RetFun;

	// verify is is allowed adjust base amount
	memset((char*)&Merchant, 0, L_TABLE_MERCHANT);
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	if(!(Merchant.Flags1 & MERCH_FLAG1_ADJUST_BASE_AMOUNT))
		return FALSE;

	// clear buffer
	memset(Step_Buffer, 0, L_buffer);

	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_NEW_AMOUNT), UI_ALIGN_CENTER);

	node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
	if(node != NULL)
		memcpy(&Currency,TlvTree_GetData(node), LTAG_UINT8);

	if(Currency == 1)
		STM_SetCurrency(tTerminal.CurrSymbol_Local);
	else
		STM_SetCurrency(tTerminal.CurrSymbol_Dollar);

	node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
	if(node != NULL)
	{
		BaseAmount = *(amount*)TlvTree_GetData(node);
		Uint64_to_Ascii(BaseAmount, Step_Buffer, _FORMAT_TRIM, 0, 0);
	}

	return TRUE;

}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_ADJUST_BASE_AMOUNT
 * Description:		Post function for STEP_ADJUST_BASE_AMOUNT
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_AGAIN
 * Notes:
 */
int Post_STEP_ADJUST_BASE_AMOUNT(char *Result)
{
	TABLE_MERCHANT Merchant;
	amount Amount;
	amount ActualBaseAmount;
	amount DiferencePercent;
	int RetFun;
	TLV_TREE_NODE node;

	// convert amount
	Amount = atof(Step_Buffer);

	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// verify max amount
	CHECK(Amount <= Merchant.Amount_Max, LBL_BIGER_AMOUNT);

	// get actual base amount
	node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	ActualBaseAmount = *(amount*)TlvTree_GetData(node);

	DiferencePercent = ((Amount - ActualBaseAmount) * 10000) / ActualBaseAmount;
	CHECK(DiferencePercent <=  Merchant.Adjust_Max_Percent*100, LBL_BIGER_ADJUST);

	// set new base amount
	RetFun = TlvTree_SetData(node, &Amount, LTAG_AMOUNT);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_BIGER_AMOUNT:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_AMOUNT), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
	memset(Step_Buffer, 0, L_buffer);
	return STM_RET_AGAIN;

LBL_BIGER_ADJUST:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_ADJUST_GREATER), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
	memset(Step_Buffer, 0, L_buffer);
	return STM_RET_AGAIN;

}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_ADJUST_TIP_AMOUNT
 * Description:		Pre function for STEP_ADJUST_TIP_AMOUNT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 *  - FALSE
 * Notes:
 */
bool Pre_STEP_ADJUST_TIP_AMOUNT(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
	bool ReturnValue=TRUE;
	amount TipAmount;
	TLV_TREE_NODE node;

	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_FASLE);

	//CHECK(Merchant.Flags1 & MERCH_FLAG1_TIP, LBL_FASLE);
	CHECK(Merchant.TIP_type == 0, LBL_FASLE);

	node = TlvTree_Find(Tree_Tran, TAG_TIP_AMOUNT, 0);
	if(node == NULL)
	{
		UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_TIP), UI_ALIGN_CENTER);
	}
	else
	{
		TipAmount = *(amount*)TlvTree_GetData(node);
		if(TipAmount == 0)
			UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_TIP), UI_ALIGN_CENTER);
		else
		{
			UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_NEW_TIP), UI_ALIGN_CENTER);
			Uint64_to_Ascii(TipAmount, Step_Buffer, _FORMAT_TRIM, 0, 0);
		}
	}

	return ReturnValue;

LBL_FASLE:
	return FALSE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_ADJUST_TAX_AMOUNT
 * Description:		Pre function for STEP_ADJUST_TAX_AMOUNT
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 *  - FALSE
 * Notes:
 */
bool Pre_STEP_ADJUST_TAX_AMOUNT(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
	bool ReturnValue=TRUE;
	amount TaxAmount;
	TLV_TREE_NODE node;

	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_FASLE);

	CHECK(Merchant.Flags1 & MERCH_FLAG1_TAX, LBL_FASLE);
	CHECK(Merchant.TAX_type == 0, LBL_FASLE);

	node = TlvTree_Find(Tree_Tran, TAG_TAX_AMOUNT, 0);
	if(node == NULL)
	{
		UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_TAX), UI_ALIGN_CENTER);
	}
	else
	{
		TaxAmount = *(amount*)TlvTree_GetData(node);
		if(TaxAmount == 0)
			UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_TAX), UI_ALIGN_CENTER);
		else
		{
			UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_NEW_TAX), UI_ALIGN_CENTER);
			Uint64_to_Ascii(TaxAmount, Step_Buffer, _FORMAT_TRIM, 0, 0);
		}
	}

	return ReturnValue;

LBL_FASLE:
	return FALSE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_ADJUST_CONFIRM
 * Description:		Post function for STEP_ADJUST_CONFIRM
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_AGAIN
 * Notes:
 */
int Post_STEP_ADJUST_CONFIRM(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint16 TranStatus;

	// Confirm total
	RetFun = Post_STEP_CONFIRM_TOTAL(NULL);
	CHECK(RetFun == STM_RET_NEXT, LBL_CONFIRM_ERROR);

	// get transaction status
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	// turn on adjust status
	//TranStatus |= FSTATUS_NEED_ADJUST;
	TranStatus &= ~ FSTATUS_ADJUSTED;
	TranStatus |= FSTATUS_NEED_ADJUST;

	// update Tree_Tran with new tran status
	RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

	// update transaction in batch file
	RetFun = Batch_Modify_Transaction(Tree_Tran);
	CHECK(RetFun == BATCH_RET_OK, LBL_ERROR);

	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION_ADJUSTED), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);

	return STM_RET_NEXT;

LBL_CONFIRM_ERROR:
	return RetFun;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_ADJUST_VALIDATE
 * Description:		Post function for Post_STEP_ADJUST_VALIDATE
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_ADJUST_VALIDATE(char *Result)
{
	TLV_TREE_NODE node;
	TABLE_MERCHANT Merchant;
	int RetFun;
	uint16 TranOptios;
	TABLE_RANGE Range;
	uint16 TranID=0;

	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);


	// get merchant data
	memset((char*)&Merchant, 0, L_TABLE_MERCHANT);
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	CHECK(tTerminal.Comercio & RESTAURANT && !(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_ADJUST_NOT_ALLOWED);

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);
	CHECK((TranID & TRAN_VOID), LBL_ANULED);

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranOptios = *(uint16*)TlvTree_GetData(node);
	CHECK((TranOptios & FSTATUS_ADJUSTED), LBL_ADJUSTED);
	// check if is allowed adjust the current transaction

	// get transaction options
	/*node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranOptios = *(uint16*)TlvTree_GetData(node);*/

	//CHECK(TranOptios & FSTATUS_NEED_ADJUST, LBL_ADJUSTED);



	//+ ADJUST01 @mamata Dec 16, 2014
	//CHECK(Merchant.Transactions1 & MERCH_TRAN1_ADJUST, LBL_ADJUST_NOT_ALLOWED);
	//- ADJUST01

	// validate if Merchant is configured with TAX or TIP
	/*if(		Merchant.Flags1 & MERCH_FLAG1_TAX
		|| 	Merchant.Flags1 & MERCH_FLAG1_TIP
		||	Merchant.Flags1 & MERCH_FLAG1_ADJUST_BASE_AMOUNT
	  )*/

		return STM_RET_NEXT;

LBL_ADJUST_NOT_ALLOWED:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_ADJUST_NOT_ALLOWED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_ADJUSTED:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION_ADJUSTED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_ANULED:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION_ANULED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SELECT_MERCH_GROUP_ALL
 * Description:		select a Merchant Group depend transaction
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_CANCEL
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_SELECT_MERCH_GROUP_ALL(char *Result)
{
	int FunResult;
	TABLE_MERCH_GROUP tmpMerchGroup;
	TLV_TREE_NODE node;
	int NumItems=0;
	int32 SelectedMerchGroup=0;
	int32 FirstItem = 0;
	uint16 TranID=0;

	// verify if is a settle
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	if(node != NULL)
	{
		TranID = *(uint16*)TlvTree_GetData(node);
		if(TranID == TRAN_SETTLE && tTerminal.Flags1 & TERM_FLAG1_SETTLE_ALL)
		{
			SelectedMerchGroup = 99999;
			node = TlvTree_AddChild(Tree_Tran, TAG_MERCH_GROUP_ID, &SelectedMerchGroup, LTAG_UINT32);
			CHECK(node != NULL, LBL_NO_MERCHGROUP)
			return STM_RET_NEXT;
		}

	}

	UI_MenuReset();

	// try to find fist record of Merchant Gruop table
	FunResult = TM_FindFirst(TAB_MERCH_GROUP, &tmpMerchGroup);
	CHECK(FunResult == TM_RET_OK, LBL_NO_MERCHGROUP);

	UI_MenuAddItem(mess_getReturnedMessage(MSG_ALL), 99999);

	do
	{
		// Transaction is enable, add Merchant Group to menu
		UI_MenuAddItem(tmpMerchGroup.Name, tmpMerchGroup.Header.RecordID);
		NumItems++;

		if(NumItems == 1)
			FirstItem = tmpMerchGroup.Header.RecordID;

		// try to find next record
		FunResult = TM_FindNext(TAB_MERCH_GROUP, &tmpMerchGroup);
	//+ NORECORDS @mamata 6/05/2016
	//d }while(FunResult != TM_RET_NO_MORE_RECORDS);
	}while(FunResult == TM_RET_OK);
	CHECK(FunResult == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	// check if no item found, then cancel transaction
	CHECK(NumItems > 0, LBL_NO_ITEMS);

	// execute menu
	if(NumItems == 1)
		SelectedMerchGroup = FirstItem;
	else
		SelectedMerchGroup = UI_MenuRun(mess_getReturnedMessage(MSG_MERCH_GROUP), tConf.TimeOut, 0);

	// if is a valid result, goto next step
	if(SelectedMerchGroup > 0)
	{
		node = TlvTree_AddChild(Tree_Tran, TAG_MERCH_GROUP_ID, &SelectedMerchGroup, LTAG_UINT32);
		CHECK(node != NULL, LBL_NO_MERCHGROUP)
		return STM_RET_NEXT;
	}

LBL_NO_MERCHGROUP: // no merchant groups configured
	return STM_RET_CANCEL;

LBL_NO_ITEMS:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION), mess_getReturnedMessage(MSG_NO_ALLOWED), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

//+ NORECORDS @mamata 6/05/2016
LBL_ERROR:
	return STM_RET_ERROR;
//- NORECORDS
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SELECT_MERCHANT_ALL
 * Description:		Post function STEP_SELECT_MERCHANT_ALL
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_CANCEL
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_SELECT_MERCHANT_ALL(char *Result)
{
	int FunResult;
	TABLE_MERCHANT Merchant;
	TLV_TREE_NODE node;
	int NumItems=0;
	int32 SelectedMerchant=0;
	int32 FirstItem = 0;
	int32 MerchantGroupID;

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	// if selected all merchant groups, select all merchants
	if(MerchantGroupID == 99999)
	{
		node = TlvTree_AddChild(Tree_Tran, TAG_MERCHANT_ID, &MerchantGroupID, LTAG_UINT32);
		CHECK(node != NULL, LBL_ERROR);
		return STM_RET_NEXT;
	}

	UI_MenuReset();

	// try to find fist record of Merchant Gruop table
	FunResult = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(FunResult == TM_RET_OK, LBL_NO_MERCHGROUP);

	UI_MenuAddItem(mess_getReturnedMessage(MSG_ALL), 99999);

	do
	{
		if(Merchant.MerchantGroupID == MerchantGroupID)
		{
			// Transaction is enable, add Merchant Group to menu
			UI_MenuAddItem(Merchant.Name, Merchant.Header.RecordID);
			NumItems++;

			if(NumItems == 1)
				FirstItem = Merchant.Header.RecordID;
		}

		// try to find next record
		FunResult = TM_FindNext(TAB_MERCHANT, &Merchant);
	//+ NORECORDS @mamata 6/05/2016
	//d }while(FunResult != TM_RET_NO_MORE_RECORDS);
	}while(FunResult == TM_RET_OK);
	CHECK(FunResult == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	// check if no item found, then cancel transaction
	CHECK(NumItems > 0, LBL_NO_ITEMS);

	// execute menu
	if(NumItems == 1)
		SelectedMerchant = FirstItem;
	else
		SelectedMerchant = UI_MenuRun(mess_getReturnedMessage(MSG_MERCHANT), tConf.TimeOut, 0);

	// if is a valid result, goto next step
	if(SelectedMerchant > 0)
	{
		node = TlvTree_AddChild(Tree_Tran, TAG_MERCHANT_ID, &SelectedMerchant, LTAG_UINT32);
		CHECK(node != NULL, LBL_NO_MERCHGROUP)
		return STM_RET_NEXT;
	}

LBL_NO_MERCHGROUP: // no merchant groups configured
	return STM_RET_CANCEL;

LBL_NO_ITEMS:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION), mess_getReturnedMessage(MSG_NO_ALLOWED), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PRINT_TOTALS_REPORT
 * Description:		Post function STEP_PRINT_TOTALS_REPORT
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_PRINT_TOTALS_REPORT(char *Result)
{
	TLV_TREE_NODE node;
	int32 MerchantID;
	int32 MerchantGroupID;

	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(int32*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	Reports_Totals(MerchantID, MerchantGroupID);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PRINT_DETAILS_REPORT
 * Description:		Post function STEP_PRINT_DETAILS_REPORT
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_PRINT_DETAILS_REPORT(char *Result)
{
	TLV_TREE_NODE node;
	int32 MerchantID;
	int32 MerchantGroupID;

	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(int32*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	Reports_Detail(MerchantID, MerchantGroupID);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PRINT_DETAILS_SERVER
 * Description:		Post function STEP_PRINT_DETAILS_SERVER
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_PRINT_DETAILS_SERVER(char *Result)
{
	TLV_TREE_NODE node;
	int32 MerchantID;
	int32 MerchantGroupID;
	int32 Mesero;

	node = TlvTree_Find(Tree_Tran, TAG_SERVER_NUMBER, 0);
	//CHECK(node != NULL, LBL_GENERAL);
if (node != NULL)
	Mesero = *(int32*)TlvTree_GetData(node);
else
	Mesero=0;
	//LBL_GENERAL:
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(int32*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	Reports_Detail_Server(MerchantID, MerchantGroupID, Mesero);

	return STM_RET_NEXT;



LBL_ERROR:
	return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_RUN_SETTLE
 * Description:		Post function STEP_RUN_SETTLE
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_RUN_SETTLE(char *Result)
{
	TLV_TREE_NODE node;
	int32 MerchantID;
	int32 MerchantGroupID;
	int ret;

	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantID = *(int32*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	ret = HostMain_PerfomSellte(MerchantID, MerchantGroupID);

	CHECK(ret==0,LBL_ERROR);

	CHECK(tTerminal.Flags1 & TERM_FLAG1_SETTLE_OBLIGADO, LBL_NO_CIERRE);
	 TM_FindFirst(TAB_TERMINAL, &tTerminal);
			tTerminal.Flags1 &= ~TERM_FLAG1_SETTLE_OBLIGADO;
			TM_ModifyRecord(TAB_TERMINAL, &tTerminal);



LBL_NO_CIERRE:
	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TERMINAL_PASSWORD
 * Description:		pre function STEP_TERMINAL_PASSWORD
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 *  - FALSE
 * Notes:
 */
bool Pre_STEP_TERMINAL_PASSWORD(void)
{
	TLV_TREE_NODE node;
	int32 MerchantGroupID;
	int32 MerchantID;

	// if tag no present don't execute step
	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_FALSE);

	// get merchant group id
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	// if all merchant group operation, execute step
	CHECK(MerchantGroupID != 99999, LBL_TRUE);


	node = TlvTree_Find(Tree_Tran,TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_FALSE);

	// get merchant id
	MerchantID = *(int32*)TlvTree_GetData(node);
	CHECK(MerchantID != 99999, LBL_TRUE);

LBL_FALSE:
	return FALSE;

LBL_TRUE:
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_TERMINAL_PASSWORD), UI_ALIGN_CENTER);
	return TRUE;

}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_TERMINAL_PASSWORD
 * Description:		Post function STEP_TERMINAL_PASSWORD
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_AGAIN
 * Notes:
 */
int Post_STEP_TERMINAL_PASSWORD(char *Result)
{
	if(strcmp(Step_Buffer, tTerminal.Password_Merchant) == 0)
		return STM_RET_NEXT;

	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BAD_PASSWORD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

	return STM_RET_AGAIN;
}


//+ CRYPTOLIB @mamata Feb 21, 2015
//d /* --------------------------------------------------------------------------
//d  * Function Name:	Pre_STEP_PIN
//d  * Description:		pre function STEP_PIN
//d  * Parameters:
//d  *  - none
//d  * Return:
//d  *  - TRUE
//d  *  - FALSE
//d  * Notes:
//d  */

//bool Pre_STEP_PIN(void)
//{
//    TLV_TREE_NODE node;
//    TABLE_RANGE Range;
//    TABLE_HOST Host;
//    int RetFun;
//    amount Total;
//    CRYPTOLIB_PIN_PARAMETERS PINEntryParams;
//    TABLE_KEY_INFO stPinKeyInfo;
//    CRYPTOLIB_PIN_GENERAL_PARAMS GeneralPINEntryParams;
//
//	RetFun = TableTerminal_GetKeyInfo(KEY_TYPE_PIN, &stPinKeyInfo);
//	CHECK(RetFun == RET_OK, LBL_FALSE);
//
//	memset( &PINEntryParams, 0x00, sizeof(PINEntryParams) );
//	memset( &GeneralPINEntryParams, 0x00, sizeof(GeneralPINEntryParams));
//
//	PINEntryParams.PINGeneralParams = &GeneralPINEntryParams;
//
//    memset((char*)&Range, 0, L_TABLE_RANGE);
//    RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
//    CHECK(RetFun == RET_OK, LBL_FALSE);
//
//    memset((char*)&Host, 0, L_TABLE_HOST);
//    RetFun = Get_TransactionHostData(&Host, Tree_Tran);
//    CHECK(RetFun == RET_OK, LBL_FALSE);
//
//    // verify if card requires PIN entry
//    CHECK(Range.Flags1 & RANGE_FLAG1_PROMPT_PIN, LBL_FALSE);
//
//    // yes, configure pin entry parameters
//    // clear PINEntryParams
//    memset((char*)&PINEntryParams, 0, sizeof(PINEntryParams));
//
//    // set PAN
//    node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
//    CHECK(node != NULL, LBL_FALSE);
//    memcpy(GeneralPINEntryParams.PAN, TlvTree_GetData(node), TlvTree_GetLength(node));
//
//
//	PINEntryParams.stKeyInfo.cAlgoType = PINEntryParams.stMasterKeyInfo.cAlgoType = stPinKeyInfo.AlgoType;
//	PINEntryParams.stKeyInfo.iSecretArea = PINEntryParams.stMasterKeyInfo.iSecretArea = stPinKeyInfo.SecretArea;
//	PINEntryParams.stKeyInfo.uiBankId = PINEntryParams.stMasterKeyInfo.uiBankId = stPinKeyInfo.BankId;
//
//    // set Algorithm Type
//    if(Host.Flags1 & HOST_FLAG1_USE_DUKPT)
//    {
//        // DUKPT
//        if(Host.Flags1 & HOST_FLAG1_USE_3DES)
//        	stPinKeyInfo.AlgoType = TLV_TYPE_TDESDUKPT;
//        else
//        	stPinKeyInfo.AlgoType = TLV_TYPE_DESDUKPT;
//    }
//    else
//    {
//        // MASTER SESSION
//        if(Host.Flags1 & HOST_FLAG1_USE_3DES)
//        	stPinKeyInfo.AlgoType = TLV_TYPE_KTDES;
//        else
//        	stPinKeyInfo.AlgoType = TLV_TYPE_KDES;
//    }
//
//    // set WorkinKeyIndex
//    PINEntryParams.WorkinKeyIndex = Host.MasterKeyIndex;
//
//
//	PINEntryParams.stMasterKeyInfo.usNumber = stPinKeyInfo.MKNumber;
//	PINEntryParams.stKeyInfo.usNumber = stPinKeyInfo.WKNumber;
//
//	Ascii_to_Hex(GeneralPINEntryParams.EncryptedWorkingKey, (uint8*)Host.EncrypedWorkingKey1, 16);
//		if(Host.Flags1 & HOST_FLAG1_USE_3DES)
//			Ascii_to_Hex(&GeneralPINEntryParams.EncryptedWorkingKey[8], (uint8*)Host.EncrypedWorkingKey2, 16);
//
//	// set line 1
//    Total = GetTotalAmount();
//    node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
//    CHECK(node != NULL, LBL_FALSE);
//    if(*(uint8*)TlvTree_GetData(node) == 1)
//        Format_Amount(Total, GeneralPINEntryParams.Line1, tTerminal.CurrSymbol_Local, FALSE);
//    else
//        Format_Amount(Total,  GeneralPINEntryParams.Line1, tTerminal.CurrSymbol_Dollar, FALSE);
//
//    // set line 2
//    memcpy(GeneralPINEntryParams.Line2, "DIGITE PIN", 10);
//
//    // EncryptedWorkingKey
//    memcpy(GeneralPINEntryParams.EncryptedWorkingKey, Host.EncrypedWorkingKey1, 16);
//    if(Host.Flags1 & HOST_FLAG1_USE_3DES)
//        memcpy(&GeneralPINEntryParams.EncryptedWorkingKey[16], Host.EncrypedWorkingKey2, 16);
//
//    // MasterKeyIndex
//    PINEntryParams.stMasterKeyInfo.usNumber  = Host.MasterKeyIndex;
//
//    return TRUE;
//
//LBL_FALSE:
//    return FALSE;
//}
//- CRYPTOLIB

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PIN
 * Description:		Post function STEP_PIN
 * Parameters:
 *  - char *Result - Stem manager, where to put data
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_PIN(char *Result)
{
	TABLE_RANGE Range;
	TLV_TREE_NODE node;
	char ServiceCode[10];
//	char AuxLine[30];
	int RetFun;
	bool PinRequested = FALSE;
	amount Amount;

	node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);

	Amount = *(amount*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	if(node != 0)
	{
		if(*(uint8*)TlvTree_GetData(node) == ENTRY_MODE_CLESS_CHIP)
		{
						if (Amount <= atof(tTerminal.User_Label1)) {
							return STM_RET_NEXT;
						}else{
							goto GO_TO_PIN;
						}
		}
	}

	//--- Check if PIN ONLINE/OFFLINE was entered
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_REQ_PIN_OFFLINE, &PinRequested, NULL);
	if(RetFun == RET_OK)
	{
		CHECK(PinRequested == FALSE, LBL_NEXT_STEP);
	}

	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_REQ_PIN_ONLINE, &PinRequested, NULL);
	if(RetFun == RET_OK)
	{
		CHECK(PinRequested == FALSE, LBL_NEXT_STEP);
	}


//	EUGENIO



//	node = TlvTree_Find(Tree_Tran, TAG_EMV_APP_LBL, 0);
//	if(node != NULL)
//	{
//		memset( AuxLine, 0x00, sizeof(AuxLine) );
//		memcpy(AuxLine, TlvTree_GetData(node), TlvTree_GetLength(node));
//		for (int indice = 0; AuxLine[indice] != '\0'; ++indice){
//			AuxLine[indice] = toupper(AuxLine[indice]);
//		}
//
//		RetFun = BuscarPalabra(AuxLine,"DEBIT");
//		amoun = atof(tTerminal.Amount_Pin_Debit);
//
//
//		if (RetFun == 1) {
//			if (Amount <= atof(tTerminal.Amount_Pin_Debit)) {
//				return STM_RET_NEXT;
//			}
//
//		}
//	}




	memset((char*)&Range, 0, L_TABLE_RANGE);
		RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
		CHECK(RetFun == RET_OK, LBL_ERROR);
		// verify if card requires PIN entry
		CHECK(Range.Flags1 & RANGE_FLAG1_PROMPT_PIN, LBL_NEXT_STEP);

		GO_TO_PIN:
		RetFun = utilGetPinOnline(Tree_Tran);
		if(RetFun == RET_NOK)
				return STM_RET_ERROR;
			else if(RetFun == RET_CANCEL)
				return STM_RET_CANCEL;


	node = TlvTree_Find(Tree_Tran, TAG_SERVICE_CODE, 0);
	if(node != NULL)
	{
		memset( ServiceCode, 0x00, sizeof(ServiceCode) );
		memcpy(ServiceCode, TlvTree_GetData(node), TlvTree_GetLength(node));

		if (ServiceCode[2] == '0' || ServiceCode[2] == '6')
			return STM_RET_NEXT;
	}





LBL_NEXT_STEP:

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

}


//+ ECHOTEST @mamata Aug 15, 2014
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SELECT_MERCHANT_BY_TRAN
 * Description:		Post function for STEP_SELECT_MERCHANT_BY_TRAN
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_SELECT_MERCHANT_BY_TRAN(char *Result)
{
	int FunResult;
	TABLE_MERCHANT Merchant;
	TLV_TREE_NODE node;
	int NumItems=0;
	int32 SelectedMerchant=0;
	int32 FirstItem = 0;
	int32 MerchantGroupID;
	uint16 TranID;
	int32 HostID;
	uint8 EntryMode = ENTRY_MODE_NONE;

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGroupID = *(int32*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node),

	UI_MenuReset();

	// try to find fist record of Merchant Gruop table
	FunResult = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(FunResult == TM_RET_OK, LBL_NO_MERCHGROUP);

	do
	{
		if(		Merchant.MerchantGroupID == MerchantGroupID
			&&	Merchant_IsTransactionEnable(Merchant.Transactions1, Merchant.Transactions2, TranID)
		  )
		{
			// Transaction is enable, add Merchant Group to menu
			//UI_MenuAddItem(Merchant.Name, Merchant.Header.RecordID);
				UI_MenuAddItem(Merchant.Name, Merchant.Header.RecordID);
			NumItems++;

			if(NumItems == 1)
				FirstItem = Merchant.Header.RecordID;
		}

		// try to find next record
		FunResult = TM_FindNext(TAB_MERCHANT, &Merchant);
	//+ NORECORDS @mamata 6/05/2016
	//d }while(FunResult != TM_RET_NO_MORE_RECORDS);
	}while(FunResult == TM_RET_OK);
	CHECK(FunResult == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	// check if no item found, then cancel transaction
	CHECK(NumItems > 0, LBL_NO_ITEMS);

	// execute menu
	if(NumItems == 1 || TranID == TRAN_TEST || TranID == TRAN_LOGON ){
		SelectedMerchant = FirstItem;
	}else{
		SelectedMerchant = UI_MenuRun(mess_getReturnedMessage(MSG_MERCHANT), tConf.TimeOut, 0);
	}

	// if is a valid result, goto next step
	if(SelectedMerchant > 0)
	{
		node = TlvTree_AddChild(Tree_Tran, TAG_MERCHANT_ID, &SelectedMerchant, LTAG_UINT32);
		CHECK(node != NULL, LBL_NO_MERCHGROUP)

		FunResult = TM_FindRecord(TAB_MERCHANT, &Merchant, SelectedMerchant);
		CHECK(FunResult == TM_RET_OK, LBL_ERROR);

		HostID = Merchant.HostID;
		node = TlvTree_AddChild(Tree_Tran, TAG_HOST_ID, &HostID, LTAG_UINT32);
		CHECK(node != NULL, LBL_ERROR);

		node = TlvTree_AddChild(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, LTAG_UINT8);
		CHECK(node != NULL, LBL_ERROR);

		node = TlvTree_AddChild(Tree_Tran, TAG_BATCH_FILE_NAME, Merchant.BatchName, strlen(Merchant.BatchName));
		CHECK(node != NULL, LBL_ERROR);

		node = TlvTree_AddChild(Tree_Tran, TAG_CURRENCY, &Merchant.Currency, LTAG_UINT8);
		CHECK(node != NULL, LBL_ERROR);
		//add batch number eugenio
		FunResult = Tlv_SetTagValueInteger(Tree_Tran, TAG_BATCH_NUMBER, Merchant.BatchNumber, LTAG_INT32);
		CHECK(FunResult == RET_OK,LBL_ERROR);
		return STM_RET_NEXT;
	}

LBL_NO_MERCHGROUP: // no merchant groups configured
	return STM_RET_CANCEL;

LBL_NO_ITEMS:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_TRANSACTION), mess_getReturnedMessage(MSG_NO_ALLOWED), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_ERROR:
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SHOW_RESULT
 * Description:		Post function for Post_STEP_SHOW_RESULT
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_SHOW_RESULT(char *Result)
{

	UI_ShowConfirmationScreen(" ", 30);

	return STM_RET_NEXT;
}
//- ECHOTEST

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CLESS_VALIDATE_CARD
 * Description:		Post function for STEP_CLESS_VALIDATE_CARD
 * Autor:			mamata
 * Parameters:
 *  - char *Result
 * Return:
 * Notes:
 */
int Post_STEP_CLESS_VALIDATE_CARD(char *Result)
{
	TLV_TREE_NODE Range_CandidateList = NULL;
	TLV_TREE_NODE Merchant_CandidateList = NULL;
	TLV_TREE_NODE node = NULL;
	int RetFun;
	uint32 SelectedMerchant;
	uint32 MerchantGropuID;
	bool MultyCurrency;
	TABLE_MERCHANT Merchant;
	uint32 HostID;
	uint16 TranID;


	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);


	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	SelectedMerchant = *(amount*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	MerchantGropuID = *(amount*)TlvTree_GetData(node);

	Range_CandidateList = TlvTree_New(0);
	Merchant_CandidateList = TlvTree_New(0);

	RetFun = BuildRangeCandidateList(Range_CandidateList);
	CHECK(RetFun == RET_OK, LBL_ERROR_RANGECL);

	RetFun = BuildMerchantCandidateList(Merchant_CandidateList, Range_CandidateList, MerchantGropuID, &MultyCurrency);
	CHECK(RetFun == RET_OK, LBL_ERROR_RANGECL);

	RetFun = RET_NOK;
	node = TlvTree_Find(Merchant_CandidateList, SelectedMerchant, 0);
	CHECK(node != NULL, LBL_ERROR_RANGECL);

	// Select range
	UI_MenuReset();
	node = TlvTree_GetFirstChild(Range_CandidateList);
	do
	{
		uint8 RangeName[30];
		int RangeID;
		memset(RangeName, 0, 30);
		memcpy(RangeName, TlvTree_GetData(node), TlvTree_GetLength(node));
		RangeID = TlvTree_GetTag(node);
		if(TranID == TRAN_C2P && RangeID==8)
				UI_MenuAddItem((char*)RangeName, RangeID);
		if(TranID != TRAN_C2P && RangeID!=8)
				UI_MenuAddItem((char*)RangeName, RangeID);
		//UI_MenuAddItem((char*)RangeName, RangeID);
		node = TlvTree_GetNext(node);
	}while(node != NULL);
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_CARD), tConf.TimeOut, 0);

	node = TlvTree_AddChild(Tree_Tran, TAG_RANGE_ID, &RetFun, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	UI_SetLine_2Texts(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), Merchant.Name);

	// add host id to tree
	HostID = Merchant.HostID;
	node = TlvTree_AddChild(Tree_Tran, TAG_HOST_ID, &HostID, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	// add batch file name to tree
	node = TlvTree_AddChild(Tree_Tran, TAG_BATCH_FILE_NAME, Merchant.BatchName, strlen(Merchant.BatchName));
	CHECK(node != NULL, LBL_ERROR);

	// add currency
	node = TlvTree_AddChild(Tree_Tran, TAG_CURRENCY, &Merchant.Currency, LTAG_UINT8);
	CHECK(node != NULL, LBL_ERROR);

	TlvTree_Release(Range_CandidateList);
	TlvTree_Release(Merchant_CandidateList);

	return STM_RET_NEXT;

LBL_ERROR:
	if(Range_CandidateList != NULL)
		TlvTree_Release(Range_CandidateList);
	if(Merchant_CandidateList != NULL)
		TlvTree_Release(Merchant_CandidateList);
	return STM_RET_ERROR;

LBL_ERROR_RANGECL:
	TlvTree_Release(Range_CandidateList);
	TlvTree_Release(Merchant_CandidateList);
	switch(RetFun)
	{
		case RET_NOK:
			node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
			if(node != 0)
			{
				if(*(uint8*)TlvTree_GetData(node) == ENTRY_MODE_MANUAL)
				{
					UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_MANUAL_ENTRY), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
					return STM_RET_CANCEL;
				}
			}

			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_SUPPORTED_CARD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			break;
		default:
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_TRAN), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
			break;
	}
	return STM_RET_CANCEL;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SetOfflineApproved
 * Description:		Set offline approved status to a transaction
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK - transaction is set as offline approved
 *  - RET_NOK - cannot set transaciton as offline approved
 * Notes:
 */
int HostMain_SetOfflineApproved(TLV_TREE_NODE Tree)
{
	char RC[2];
	TLV_TREE_NODE node;
	int RetFun;
	memcpy(RC, "AF", 2);
	uint16 TranStatus;
	uint32 Invoice = 0;
	DATE SysDateTime;
	char AuthNumber[7];

	// set response code
	node = TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, RC, 2);
	CHECK(node != NULL, LBL_ERROR);

	// set offline approved flag
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);
	TranStatus |= FSTATUS_OFFLINE;
	RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

	// add invoice to tree
	Invoice = Counters_Get_Invoice();
	CHECK(Invoice > 0, LBL_ERROR);
	node = TlvTree_AddChild(Tree, TAG_INVOICE, &Invoice, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	// add system date and time
	//+ FIXDATETIME @mamata 5/05/2016
	//d RetFun = ReadDateBooster(&SysDateTime);
	RetFun = Telium_Read_date(&SysDateTime);
	//- FIXDATETIME
	CHECK(RetFun == RET_OK, LBL_ERROR);
	node = TlvTree_AddChild(Tree, TAG_DATE_TIME, &SysDateTime, sizeof(DATE));
	CHECK(node != NULL, LBL_ERROR);

	// add auth number
	node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
	if(node == NULL)
	{
		memset(AuthNumber, 0, 7);
		memcpy(AuthNumber, "--FL--", 6);
		node = TlvTree_AddChild(Tree, TAG_HOST_AUTH_NUMBER, AuthNumber, 6);
		CHECK(node != NULL, LBL_ERROR);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_QUICK_SALE_AMOUNT
 * Description:		Pre function for STEP_QUICK_SALE_AMOUNT
 * Autor:			mamata
 * Parameters:
 *  - none
 * Return:
 *  - TRUE
 * Notes:
 */
bool Pre_STEP_QUICK_SALE_AMOUNT(void)
{
	//+ EMV09 @mamata Jun 23, 2015
	TLV_TREE_NODE node;
	int16 MessageID;
	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if(node != NULL)
		memcpy((char*)&MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MessageID), UI_ALIGN_CENTER);
	//- EMV09
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_ENTER_AMOUNT), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_QUICK_SALE_AMOUNT
 * Description:		Post function for STEP_QUICK_SALE_AMOUNT
 * Autor:			mamata
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 * Notes:
 */
int Post_STEP_QUICK_SALE_AMOUNT(char *Result)
{
	TLV_TREE_NODE node;
	amount Amount;

	Amount = atof(Step_Buffer);
	node = TlvTree_AddChild(Tree_Tran, TAG_BASE_AMOUNT, &Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_ERROR);

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}
//- CLESS


//+ CARCAAN_PINPAD @mamata Jan 14, 2015
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PINPAD_GET_CARD
 * Description:		Post function for STEP_PINPAD_GET_CARD
 * Autor:			mamata
 * Parameters:
 *  - char *Result
 * Return:
 * Notes:
 */
int Post_STEP_PINPAD_GET_CARD(char *Result)
{
/*
	TLV_TREE_NODE node;
	TLV_TREE_NODE node2;
	int RetFun;
	uint8 InputType = 1;
	uint32 Readers = 0;
	Telium_File_t *hKey;
	Telium_File_t *hSwipe1;
	Telium_File_t *hSwipe2;
	Telium_File_t *hChip;
	uint32 ReadResult;
	uint8 KeyPressed;
	uint8 TrackBuffer[200];
	uint8 TrackLen;
	uint16 PinpadControl1 = 0;

	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	if(node != NULL)
	{
		// get dato from pinpad
		node = TlvTree_GetFirstChild(Tree_PINPAD);
		CHECK(node != NULL, LBL_ERROR);
		do
		{
			node2 = TlvTree_AddChild(Tree_Tran, TlvTree_GetTag(node), TlvTree_GetData(node), TlvTree_GetLength(node));
			node = TlvTree_GetNext(node);
		}while(node != NULL);
		return STEP_PINPAD_EMV_1GEN;
	}

	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	if(node != NULL)
		TlvTree_Release(node);

	// verify if cless is enable
	node = TlvTree_Find(Tree_PINPAD, PP_TAG_CONTROL1, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(TrackBuffer, TlvTree_GetData(node), TlvTree_GetLength(node));
	PinpadControl1 = TrackBuffer[0]*256 + TrackBuffer[1];
	if(PinpadControl1 & OPT_04)
		// yes, enable cless mode
		InputType = 2;

	switch(InputType)
	{
		case 1:
			// open handles
			hKey = Telium_Fopen("KEYBOARD", "r*");
			hSwipe1 = Telium_Fopen("SWIPE31", "r*");
			hSwipe2 = Telium_Fopen("SWIPE2", "r*");
			hChip = Telium_Fopen("CAM0", "r*");

			// enable readers
			Readers |= KEYBOARD;
			if(PinpadControl1 & OPT_01)
			{
				Readers |= SWIPE2;
				Readers |= SWIPE31;
			}
			if(PinpadControl1 & OPT_02)
			Readers |= CAM0;

			// show input screen
			DisplayFooter(_OFF_);
			DisplayHeader(_OFF_);
			UI_ClearAllLines();
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_APPNAME), UI_ALIGN_CENTER);
			node = TlvTree_Find(Tree_PINPAD, PP_TAG_LINE1, 0);
			if(node != NULL)
				UI_SetLine(UI_LINE2, TlvTree_GetData(node),UI_ALIGN_CENTER);
			node = TlvTree_Find(Tree_PINPAD, PP_TAG_LINE2, 0);
			if(node != NULL)
				UI_SetLine(UI_LINE3, TlvTree_GetData(node),UI_ALIGN_CENTER);
			UI_ShowInfoScreen(NULL);
			buzzer(10);

			// wait for event
			ReadResult = Telium_Ttestall(Readers, tConf.TimeOut * 100);

			if(ReadResult == KEYBOARD)
			{
				KeyPressed = Telium_Getchar();
				switch(KeyPressed)
				{
					case N0:
					case N1:
					case N2:
					case N3:
					case N4:
					case N5:
					case N6:
					case N7:
					case N8:
					case N9:
						memset(MEntryBuffer, 0, 25);
						MEntryBuffer[0] = KeyPressed;
						RetFun = STM_RET_NEXT;
						break;
					default:
						RetFun = Telium_Fclose(hKey);
						RetFun = Telium_Fclose(hSwipe1);
						RetFun = Telium_Fclose(hSwipe2);
						RetFun = Telium_Fclose(hChip);
						return STM_RET_CANCEL;
				}
			}
			else if(		(ReadResult == SWIPE2)
					||		(ReadResult == SWIPE31)
			   	   )
			{
				if(ReadResult & SWIPE2)
				{
					memset(TrackBuffer, 0, 200);
					TrackLen = 0;
					RetFun = Telium_Is_iso2(hSwipe2, &TrackLen, TrackBuffer);
					CHECK( RetFun == ISO_OK || RetFun == DEF_LUH, LBL_ERROR_TRACK2);

					RetFun = GetTrack2(TrackBuffer);
					CHECK(RetFun == RET_OK, LBL_ERROR_TRACK2);
				}


				ReadResult = Telium_Ttestall( SWIPE31, 10 );
				if(ReadResult & SWIPE31)
				{
					memset(TrackBuffer, 0, 200);
					TrackLen = 0;
					RetFun = Telium_Is_iso1(hSwipe1, &TrackLen, TrackBuffer);
					if( RetFun == ISO_OK || RetFun == DEF_LUH )
						RetFun = GetTrack1(TrackBuffer);
				}

				// Add TAG_ENTRY_MODE
				uint8 EntryMode = ENTRY_MODE_SWIPE;
				node = TlvTree_AddChild(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, LTAG_UINT8);
				CHECK(node!= NULL, LBL_ERROR_TRACK2);
			}
			else if(ReadResult == CAM0)
			{
				uint16 TranID;
				// close devices
				RetFun = Telium_Fclose(hKey);
				RetFun = Telium_Fclose(hSwipe1);
				RetFun = Telium_Fclose(hSwipe2);
				RetFun = Telium_Fclose(hChip);

				// init emv tran
				node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
				CHECK(node != NULL, LBL_ERROR);
				TranID = *(uint8*)TlvTree_GetData(node);
				EMVCUST_SetTransaction(TranID);

				FinishImplicit = 0;
				EMVCUST_StartImplicitSelection();
				FinishImplicit = 1;

				// return to idle to initialize emv transaction
				return STM_RET_CANCEL;
			}

			RetFun = Telium_Fclose(hKey);
			RetFun = Telium_Fclose(hSwipe1);
			RetFun = Telium_Fclose(hSwipe2);
			RetFun = Telium_Fclose(hChip);
			break;
		case 2:
			// swipe + keyboard + emv + cless
			RetFun = CLESS_PINPAD_RunTransaction();
			CHECK(RetFun == RET_OK, LBL_ERROR);

			// wait for transaction result
			RetFun = Telium_P(CLESS_Semaphore, 0);

			if(CLESS_Step == CLESS_STEP_SWIPE_TRAN)
			{
				;
			}
			else if(CLESS_Step == CLESS_STEP_CHIP_TRAN)
			{
				uint16 TranID;

				// init emv tran
				node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
				CHECK(node != NULL, LBL_ERROR);
				TranID = *(uint8*)TlvTree_GetData(node);
				EMVCUST_SetTransaction(TranID);

				FinishImplicit = 0;
				EMVCUST_StartImplicitSelection();
				FinishImplicit = 1;

				// return to idle to initialize emv transaction
				return STM_RET_CANCEL;
			}
			else if(CLESS_Step == CLESS_STEP_ONLINE)
			{
				char TranResult;
				// get transaction result
				node = TlvTree_Find(Tree_Tran, TAG_CLESS_RESULT, 0);
				CHECK(node != NULL, LBL_NORMAL);
				TranResult = *(uint8*)TlvTree_GetData(node);
			}
			else if (CLESS_Step == CLESS_STEP_END_TRAN)
			{
				char TranResult;
				char RC[2];
				// get transaction result
				node = TlvTree_Find(Tree_Tran, TAG_CLESS_RESULT, 0);
				CHECK(node != NULL, LBL_NORMAL);
				TranResult = *(uint8*)TlvTree_GetData(node);

				if(TranResult == CLESS_RESULT_OFFLINE_APPROVED)
					memcpy(RC, "AF", 2);
				if(TranResult == CLESS_RESULT_OFFLINE_DECLINED)
					memcpy(RC, "D1", 2);
				node = TlvTree_AddChild(Tree_Tran, TAG_HOST_RESPONSE_CODE, RC, 2);

				return STEP_EMV_END;
			}
			else if(CLESS_Step == CLESS_STEP_MANUAL_TRAN)
			{
				memset(MEntryBuffer, 0, 25);
				MEntryBuffer[0] = IdleKey;
			}
			else
				return STM_RET_CANCEL;

			break;
	}

LBL_NORMAL:
	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_ERROR_TRACK2:
	return STM_RET_ERROR;
*/
	return STM_RET_ERROR;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PINPAD_HOST_AUTH
 * Description:		Request authorization from host
 * Autor:			mamata
 * Parameters:
 *  - char *Result
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Post_STEP_PINPAD_HOST_AUTH(char *Result)
{
	PP_SendTranToHost();
	return STM_RET_NEXT;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PINPAD_EMV_1GEN
 * Description:		On EMV transactions gives finantial data to EMVCUSTOM for fist generate
 *
 * Parameters:
 *  - none
 * Return:
 *  - true - the terminal is ready
 *  - false - is needed to configure and initialize the terminal
 * Notes:
 */
int Post_STEP_PINPAD_EMV_1GEN(char *Result)
{
/*
	TLV_TREE_NODE node;
	uint8 EntryMode;
	amount TotalAmt = 0;
	char data[4];
	char datacurrency[2];
	int RetFun;
	char TranResult;

	// verify is a chip entry
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
	CHECK(EntryMode == ENTRY_MODE_CHIP, LBL_NEXT_STEP);

	// set Total ammount
	TotalAmt = GetTotalAmount();
	data[0] = (unsigned char)((TotalAmt & 0xFF000000) >> 24);
	data[1] = (unsigned char)((TotalAmt & 0x00FF0000) >> 16);
	data[2] = (unsigned char)((TotalAmt & 0x0000FF00) >> 8);
	data[3] = (unsigned char)((TotalAmt & 0x000000FF));

	// set currency code
	node = TlvTree_Find(Tree_PINPAD, TAG_CURRENCY_CODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(datacurrency, TlvTree_GetData(node), 2);

	// return information to emvcustom
	RetFun = EMVCUST_StartTran_ReturnData(EMVCUST_RESULT_OK, TotalAmt, datacurrency, EMVCUST_DEBIT_TRANSACTION);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// Waith for EMV custom make first generate
	RetFun = Telium_P(FinanApp_Semaphore, 0);

	if (EMVTranResult == 99)
		return STEP_EMV_END;

	// Verify if transaction is Approved or delined offline
	node = TlvTree_Find(Tree_Tran, TAG_EMV_TRAN_STATUS, 0);
	if (node != NULL )
	{
		// get Emv transaction status
		memcpy( &TranResult, TlvTree_GetData( node ), TlvTree_GetLength( node ) );

		if (	TranResult == EMVCUST_APR_OFFLINE
			||	TranResult == EMVCUST_DEC_OFFLINE
			)
			return STEP_EMV_END;
	}

	LBL_NEXT_STEP:
		return STM_RET_NEXT;

	LBL_ERROR:
		return STM_RET_CANCEL;
*/
	return STM_RET_ERROR;
}
//- CARCAAN_PINPAD


//+ EMV09 @mamata Jun 22, 2015
/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_QUICK_SALE_CHECK_SERVICE_CODE
 * Description:		Post function for STEP_QUICK_SALE_CHECK_SERVICE_CODE
 * Author:			mamata
 * Parameters:
 *  char *Result
 * Return:
 *  STM_RET_NEXT
 * Notes:
 */
int Post_STEP_QUICK_SALE_CHECK_SERVICE_CODE(char *Result)
{
	TABLE_MERCHANT Merchant;
	TABLE_HOST Host;
	int RetFun;
	uint8 EntryMode;
	TLV_TREE_NODE node;
	uint8 ServiceCode[3];

	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
	CHECK(EntryMode == ENTRY_MODE_SWIPE, LBL_NEXT_STEP);

	// get service code
	node = TlvTree_Find(Tree_Tran, TAG_SERVICE_CODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(ServiceCode, TlvTree_GetData(node), 3);

	// get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	// get Host data
	RetFun = TM_FindRecord(TAB_HOST, &Host, Merchant.HostID);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// verify is a EMV host
	CHECK(Host.Flags1 & HOST_FLAG1_EMV, LBL_NEXT_STEP);

	// if the transaction isn't a fallback, then verify service code
	node = TlvTree_Find(Tree_Tran, TAG_FALLBACK, 0);
	CHECK(node == NULL, LBL_FALLBACK);

	// If is a chip card, return to STEP_GET_CARD
	if(		ServiceCode[0] == '2'
		||	ServiceCode[0] == '6'
	  )
	{
		uint8 ForceIncert = 1;

		// clear card data
		node = TlvTree_Find(Tree_Tran, TAG_TRACK1, 0);
		if(node != NULL)
			TlvTree_Release(node);
		node = TlvTree_Find(Tree_Tran, TAG_TRACK2, 0);
		if(node != NULL)
			TlvTree_Release(node);
		node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
		if(node != NULL)
			TlvTree_Release(node);
		node = TlvTree_Find(Tree_Tran, TAG_EXP_DATE, 0);
		if(node != NULL)
			TlvTree_Release(node);
		node = TlvTree_Find(Tree_Tran, TAG_SERVICE_CODE, 0);
		if(node != NULL)
			TlvTree_Release(node);
		node = TlvTree_Find(Tree_Tran, TAG_CARDHOLDER_NAME, 0);
		if(node != NULL)
			TlvTree_Release(node);
		node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
		if(node != NULL)
			TlvTree_Release(node);

		node = TlvTree_AddChild(Tree_Tran, TAG_FORCE_INSERT, &ForceIncert, LTAG_UINT8);
		CHECK(node != NULL, LBL_ERROR);
		buzzer(25);
		return STEP_QUICK_SALE_GET_CARD;
	}

LBL_NEXT_STEP:
	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_FALLBACK:
	if(		ServiceCode[0] != '2'
		&&	ServiceCode[0] != '6'
	  )
		TlvTree_Release(node);
	return STM_RET_NEXT;
	return STM_RET_NEXT;
}

//- EMV09

//+ EWL01 @jbotero 29/01/2016
/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_START_PAYMENT
 * Description:	  Post Function of STEP_START_PAYMENT
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_START_PAYMENT(char *Result)
{
	TLV_TREE_NODE node 	= NULL;
	uint16 TranOptions 	= 0;
	int RetFun;
	uint32 RangID;
	uint16 TranID;

	uint8 FlagEmvSupport = FALSE;	//EMV
	uint8 FlagClsSupport = FALSE;	//CLESS
	uint8 FlagManSupport = FALSE;	//MANUAL
	uint8 FlagSwpSupport = FALSE;	//SWIPE



	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	if(TranID == TRAN_VOID)
   {
		node = TlvTree_Find(Tree_Tran, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

		if (RangID == 8)
	    return STM_RET_NEXT;

   }


	//--- Read Transaction options
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranOptions = *(uint16*)TlvTree_GetData(node);

	//--- Check MANUAL ENTRY support
	if(TranOptions & FTRAN_MANUAL)
		FlagManSupport = TRUE;

	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_MANUAL_SUPPORT, &FlagManSupport, sizeof(FlagManSupport) );
	CHECK(RetFun == RET_OK, LBL_ERROR);
	//-->

	//--- Check SWIPE support
	if(TranOptions & FTRAN_SWIPE)
		FlagSwpSupport = TRUE;

	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_SWIPE_SUPPORT, &FlagSwpSupport, sizeof(FlagSwpSupport) );
	CHECK(RetFun == RET_OK, LBL_ERROR);
	//-->

	//--- Check EMV support
	if(TranOptions & FTRAN_CHIP)
		FlagEmvSupport = TRUE;

	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_EMV_SUPPORT, &FlagEmvSupport, sizeof(FlagEmvSupport) );
	CHECK(RetFun == RET_OK, LBL_ERROR);
	//-->

	//<!--- Check CONTACTLESS support
	if(TranOptions & FTRAN_CLESS)
		FlagClsSupport = TRUE;

	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_CLESS_SUPPORT, &FlagClsSupport, sizeof(FlagClsSupport) );
	CHECK(RetFun == RET_OK, LBL_ERROR);
	//-->

    return STM_RET_NEXT;

LBL_ERROR:

	return STM_RET_ERROR;

	LBL_TAG_TRAN_ID_NOTFOUND:
			return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_EWL_START
 * Description:	  Post Function of STEP_EWL_START
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_EWL_START(char *Result)
{
    int RetFun;
    ewlObject_t *emvHandle = NULL;

    emvHandle = EWL_SetEmvHandle();
	CHECK(emvHandle != NULL, LBL_ERROR);

    RetFun =  parameterSetTransaction(emvHandle, Tree_Tran);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    RetFun =  parameterSetAids(emvHandle);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    if(ewlTransactionAllowed(emvHandle,EWL_CONTACT) != EWL_OK)
    {
    	uint8 FlagEmvSupport = FALSE;
    	Tlv_SetTagValue(Tree_Tran, TAG_TRAN_EMV_SUPPORT, &FlagEmvSupport, sizeof(FlagEmvSupport));
    }

    return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;


}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_WAIT_CARD
 * Description:	  Post Function of STEP_WAIT_CARD
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_WAIT_CARD(char *Result)
{
	Telium_File_t *hKey 	= NULL;
	Telium_File_t *hChip 	= NULL;
	ewlObject_t *ewl_handle = NULL;

	TABLE_MERCHANT Merchant;
	uint32 Readers = 0;
	uint32 ReadResult;
	uint8 KeyPressed;
    int NextStep;

	int RetFun;
	int RetFunStep = STM_RET_NEXT;

	int16 TranStatus;
	int16 TranID;
	amount TotalAmount = 0;
	char TempBuff[50];
	char StrOperCardL1[60];
	char StrOperCardL2[60];
	char CurrencySymbol[6];
	uint16 TranName;

	uint8 FlagEMVSupport = FALSE;
	uint8 FlagCLSSupport = FALSE;
	uint8 FlagManSupport = FALSE;
	uint8 FlagSwpSupport = FALSE;
	uint8 FlagOtherInt = FALSE;
	uint8 FlagForceEMV = FALSE;
	uint8 FlagFallback = FALSE;

	uint8 EntryMode = NULL;
	uint8 state;
    uint8 ValueNode = 1;
	TLV_TREE_NODE node;
    uint32 RangID;
    TABLE_HOST Host;

    node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
    	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
    	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

    	if(TranID == TRAN_VOID)
       {
    		node = TlvTree_Find(Tree_Tran, TAG_RANGE_ID, 0);
    		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
    		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

    		if (RangID == 8)
    	    return STM_RET_NEXT;

       }


	//Get flags of EMV/CLESS support
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_EMV_SUPPORT, &FlagEMVSupport, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_CLESS_SUPPORT, &FlagCLSSupport, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_SWIPE_SUPPORT, &FlagSwpSupport, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_MANUAL_SUPPORT, &FlagManSupport, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);


	//---Get Transaction Status
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_STATUS, &TranStatus, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Get Transaction ID
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

/*	if( (TranStatus & FSTATUS_NEED_VOID) || TranID == TRAN_VOID)
	{
		node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
		if(node != NULL)
			memcpy((char*)&EntryModeOriginal, TlvTree_GetData(node), LTAG_UINT8);
	}*/

    //Check if card was operated on idle
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_IDLE_CARD, &EntryMode, NULL);
	if(RetFun == RET_OK)
	{
		Tlv_ReleaseTag(Tree_Tran, TAG_TRAN_IDLE_CARD);

		//Set Entry mode on Tree
		Tlv_SetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, sizeof(EntryMode));

		if( EntryMode == ENTRY_MODE_SWIPE )
		{
			if(!FlagEMVSupport)
			{
				UI_ShowMessage("AVISO", "BANDA NO SOPORTADO", "",GL_ICON_WARNING, GL_BUTTON_ALL, 5);
				return STM_RET_CANCEL;
			}
			else
				return dataProcessMagCard(Tree_Tran);
		}
		else
		{
			if(!FlagEMVSupport)
			{
				UI_ShowMessage("AVISO", "CHIP NO SOPORTADO", "",GL_ICON_WARNING, GL_BUTTON_ALL, 5);
				return STM_RET_CANCEL;
			}
			else
				return dataGetChipCard(Tree_Tran);
		}
	}
//	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
//	RetFun = TM_FindRecord(TAB_HOST, &Host,1);
	TM_FindFirst(TAB_HOST, &Host);
	//--- Check Fall-back
	if(Tlv_GetTagValue(Tree_Tran, TAG_FALLBACK, &FlagFallback, NULL) == RET_OK)
	{
		if(FlagFallback)
		{
			if(Host.Fallback == 1){
				FlagEMVSupport = FALSE;
				FlagManSupport = FALSE;
				FlagCLSSupport = FALSE;
				FlagSwpSupport = TRUE;

				utilCloseDriver();
				utilOpenDriver();
			}else {
				goto LBL_TAG_TRAN_ID_NOTFOUND;
			}

		}
	}

	//--- Other interface(Contact-less condition)?
	if(Tlv_GetTagValue(Tree_Tran, TAG_FORCE_OTHER_INTERFACE, &FlagOtherInt, NULL) == RET_OK
		&& FlagFallback == FALSE )
	{
		if(FlagOtherInt)
		{
			FlagCLSSupport = FALSE;

			if(FlagEMVSupport == FALSE && FlagSwpSupport == FALSE)
				return STM_RET_ERROR;
		}
	}

	//--- Force Chip by Service Code?
	if(Tlv_GetTagValue(Tree_Tran, TAG_FORCE_INSERT, &FlagForceEMV, NULL) == RET_OK
		&& FlagEMVSupport == TRUE
		&& FlagFallback == FALSE )
	{
		if(FlagForceEMV)
		{
			FlagManSupport = FALSE;
			FlagCLSSupport = FALSE;
			FlagSwpSupport = FALSE;
			FlagEMVSupport = TRUE;
		}
	}

	//--- Check enabled peripheral
	if( FlagCLSSupport == 0 &&
		FlagEMVSupport == 0 &&
		FlagSwpSupport == 0 )
	{
		//--- Only manual Entry
		if( FlagManSupport == 1)
		{
			//Set Entry mode on Tree
			EntryMode = ENTRY_MODE_MANUAL;
			Tlv_SetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, sizeof(EntryMode));

			return STM_RET_NEXT;
		}
		//--- Nothing
		else
			return STM_RET_ERROR;
	}

	//--- Get merchant information
	memset(CurrencySymbol, 0, sizeof(CurrencySymbol));
	memset(TempBuff, 0x00, sizeof(TempBuff));

	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	if(RetFun == RET_OK)
	{
		//--- Get currency symbol
		if( Merchant.Currency == 1 )
			memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
		else
			memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);

		//--- Get total amount

/*
		if(TranStatus && FSTATUS_ADJUSTED){
		node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
			if (node != NULL)
			{
				memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
				TotalAmount+=Amount;
			}
			TotalAmount = GetTotalAmount();
		}
		else*/
			TotalAmount = GetTotalAmount();


		if(TotalAmount > 0)
			Format_Amount(TotalAmount, TempBuff, CurrencySymbol, FALSE);
	}




	//--- Build message to wait Card
	if( (TranStatus & FSTATUS_NEED_VOID) || TranID == TRAN_VOID)
	{
		TranName = MSG_TRAN_VOID;

		//Set Transaction name to Void transaction
		RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_NAME, &TranName, sizeof(TranName));
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}
	else
	{
		//Get Transaction name
		RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_NAME, &TranName, NULL);
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);

	//--- Check if CLESS is enabled on the terminal
	int ClesCap = PSQ_Get_Cless_Capabilities();

	if(FlagFallback)
	{
		UI_SetLine(UI_LINE1, TempBuff, UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_SWIPE_CARD), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE3, "FALLBACK", UI_ALIGN_CENTER);
	}
	else if(FlagForceEMV)
	{
		UI_SetLine(UI_LINE1, TempBuff, UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_INSERT_CARD), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_CARD), UI_ALIGN_CENTER);
	}
	else
	{
		UI_SetLine(UI_TITLE, mess_getReturnedMessage(TranName), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE1, TempBuff, UI_ALIGN_CENTER);

		memset(StrOperCardL1, 0x00, sizeof(StrOperCardL1));
		memset(StrOperCardL2, 0x00, sizeof(StrOperCardL2));

		//--- Text Line 2
		if(FlagSwpSupport)
			strcat(StrOperCardL1, "DESLICE/");

		if(FlagEMVSupport)
			strcat(StrOperCardL1, "INSERTE/");

		StrOperCardL1[strlen(StrOperCardL1) - 1] = 0x00;

		//--- Text Line 3
		if(FlagCLSSupport && ( ClesCap & CLESS_ACTIVATED ) )
			strcat(StrOperCardL2, "ACERQUE ");

		strcat(StrOperCardL2, "TARJETA");

		UI_SetLine(UI_LINE2, StrOperCardL1, UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE3, StrOperCardL2, UI_ALIGN_CENTER);
	}

	//--- Obtain the handle for starting detection of emv/cless.
	ewl_handle = EWL_GetEmvHandle();

	//--- Open peripheral
	//CLESS
    if(FlagCLSSupport && ( ClesCap & CLESS_ACTIVATED ) )
    {
    	RetFun=ewlClessStartDetection(ewl_handle);
    	CHECK(RetFun == RET_OK, LBL_ERROR_EWL); /* Erro interno */
    	Readers |= CLESS;
    }
	//EMV
	if (FlagEMVSupport)
	{

		RetFun = ewlContactStartDetection(ewl_handle);
		CHECK(RetFun == RET_OK, LBL_ERROR_EWL); /* Error interno */
		Readers |= CAM0;
	}
	//Manual
	Readers |= KEYBOARD;
	hKey = Telium_Fopen("KEYBOARD", "r*");

	//Swipe
	if(FlagSwpSupport)
	{
		Readers |= SWIPE2;
		Readers |= SWIPE31;
	}

	//--- Wait for event
	UI_ShowInfoScreen(NULL);
	buzzer(10);
	ReadResult = Telium_Ttestall(Readers, tConf.TimeOut * 100);

	//--- Check MANUAL Entry
	if ((ReadResult & KEYBOARD) == KEYBOARD)
	{
		KeyPressed = Telium_Getchar();

		//manual entry
		if(KeyPressed >= '0' &&
		   KeyPressed <= '9' &&
		   FlagManSupport)
		{
			memset(MEntryBuffer, 0, 25);
			MEntryBuffer[0] = KeyPressed;
			RetFunStep = STM_RET_NEXT;
		}
		//CANCEL
		else
				RetFunStep = STM_RET_CANCEL;

		//else if(KeyPressed == T_ANN)
		//RetFunStep = STM_RET_CANCEL;
		//else
		//	RetFunStep = STM_RET_AGAIN;

		EntryMode = ENTRY_MODE_MANUAL;
	}
	//--- Check SWIPE track 2
	if ((ReadResult & SWIPE31) == SWIPE31)
		EntryMode = ENTRY_MODE_SWIPE;

	//--- Check SWIPE track 1
	if ((ReadResult & SWIPE2) == SWIPE2)
		EntryMode = ENTRY_MODE_SWIPE;

	//--- Check Chip Card
	if ((ReadResult & CAM0) == CAM0)
	{
		hChip = Telium_Stdperif("CAM0", NULL);
		if (hChip != NULL)
		{
			Telium_Status(hChip, &state);
			if (state & CAM_PRESENT)
				EntryMode = ENTRY_MODE_CHIP;
		}
	}
	//--- Check Contact-less Card
	if ((ReadResult & CLESS) == CLESS)
		EntryMode = ENTRY_MODE_CLESS_CHIP;

	//--- Close peripherals
	if(hKey != NULL)
		RetFun = Telium_Fclose(hKey);
	if(hChip != NULL)
		RetFun = ewlContactStopDetection(ewl_handle);

	//--- Check TIMEOUT expired
	if (ReadResult == TIMEOUT)
		RetFunStep = STM_RET_CANCEL;




	//Set Entry mode on Tree
	Tlv_SetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, sizeof(EntryMode));

	switch(EntryMode)
	{
		case ENTRY_MODE_CLESS_CHIP:
		case ENTRY_MODE_CHIP:

		if(TranName == MSG_TRAN_VOID){
			RetFunStep = dataGetChipCard(Tree_Tran);
			break;
		}
		RetFun = Tlv_SetTagValue(Tree_Tran, TAG_EMV_1GEN_TAGS, &ValueNode, LTAG_UINT8);
		CHECK(RetFun == RET_OK, LBL_ERROR);
			RetFunStep = dataGetChipCard(Tree_Tran);
			break;

		case ENTRY_MODE_SWIPE:
			RetFunStep = dataGetMagCard(Tree_Tran);
			break;
	}





	return RetFunStep;

LBL_ERROR_EWL:
RetFun = paymentError(Tree_Tran, RetFun, &NextStep);
	if(RetFun != RET_OK)
		return STM_RET_ERROR;
	return NextStep;

LBL_ERROR:
	return STM_RET_ERROR;

	LBL_TAG_TRAN_ID_NOTFOUND:
	UI_ShowMessage(NULL,"TRANSACCION NO SOPORTADA", NULL, GL_ICON_ERROR , GL_BUTTON_ALL, tConf.TimeOut);
		return STM_RET_ERROR;

}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_EWL_GO_ONCHIP
 * Description:	  Post Function of STEP_EWL_GO_ONCHIP
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_EWL_GO_ONCHIP(char *Result)
{
    int RetFun = 0;
    int NextStep;
    ewlObject_t *emvHandle = NULL;
    ewlTransactionStatus_t status;
	ewlTechnology_t technology;
    transactionPINData_t pin;
    transactionPINData_t *p = &pin;

    emvHandle = EWL_GetEmvHandle();
    CHECK(emvHandle != NULL, LBL_ERROR);

    //Update parameters of Transactions
    RetFun =  parameterSetTransaction(emvHandle, Tree_Tran);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    RetFun = ewlSetParameter(EWL_GetEmvHandle(),EWL_TAG_USER_DATA_1,&p,sizeof(p));
    CHECK(RetFun == RET_OK, LBL_NEXT);

	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_CARD_TECHNOLOGY, &technology, NULL);
	CHECK(RetFun == RET_OK, LBL_NEXT);

	CHECK(paymentToolsIsChipMagnetic(technology) == FALSE, LBL_NEXT);

    RetFun = ewlGoOnChip(emvHandle);
    CHECK(RetFun == EWL_OK,LBL_ERROR);

    //---Recover data after First generate
    RetFun = dataRecoverGoOnChip(emvHandle, Tree_Tran);
    CHECK(RetFun == EWL_OK,LBL_ERROR);

    //---Get EMV status
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_EMV_TRAN_STATUS, &status, NULL);
    CHECK(RetFun == EWL_OK,LBL_ERROR);
    if(status==EWL_STATUS_APPROVED_OFFLINE || status==EWL_STATUS_DENIAL_OFFLINE || status==EWL_STATUS_GO_ONLINE )
    	return STM_RET_NEXT;

    return STM_RET_ERROR;

LBL_ERROR:

	RetFun = paymentError(Tree_Tran, RetFun, &NextStep);
	if(RetFun != RET_OK)
		return STM_RET_ERROR;

	return STM_RET_ERROR;

LBL_NEXT:

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_APPROVED_OFF
 * Description:	  Post Function of STEP_APPROVED_OFF
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_APPROVED_OFF(char *Result)
{
	uint8 EntryMode;
	int RetFun;
	uint8 FlagApproved = TRUE;
	ewlTransactionStatus_t status;

	//Get Entry mode
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    //Check Entry mode
    CHECK(	EntryMode == ENTRY_MODE_CHIP ||
    		EntryMode == ENTRY_MODE_CLESS_CHIP,
    		LBL_NEXT_STEP);

    //---Get EMV status
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_EMV_TRAN_STATUS, &status, NULL);
    CHECK(RetFun == EWL_OK,LBL_ERROR);
    CHECK(status==EWL_STATUS_APPROVED_OFFLINE,LBL_NEXT_STEP);

    dataRecoverAproveOffLine(Tree_Tran);

    RetFun = Tlv_SetTagValue(Tree_Tran, TAG_APPROVED, &FlagApproved, sizeof(FlagApproved));
    CHECK(RetFun == RET_OK, LBL_ERROR);

    UI_ShowMessage("CHIP", mess_getReturnedMessage(MSG_RC_APPROVED_OFFLINE_L1),
    		mess_getReturnedMessage(MSG_RC_APPROVED_OFFLINE_L2), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);

    return STEP_PRINT_RECEIPT;

LBL_NEXT_STEP:

	return STM_RET_NEXT;

LBL_ERROR:

	return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_DENIAL_OFF
 * Description:	  Post Function of STEP_DENIAL_OFF
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_DENIAL_OFF(char *Result)
{
	uint8 EntryMode;
	int RetFun;
	ewlTransactionStatus_t status;

	//Get Entry mode
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    //Check Entry mode
    CHECK(	EntryMode == ENTRY_MODE_CHIP ||
    		EntryMode == ENTRY_MODE_CLESS_CHIP,
    		LBL_NEXT_STEP);

    //---Get EMV status
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_EMV_TRAN_STATUS, &status, NULL);
    CHECK(RetFun == EWL_OK,LBL_ERROR);
    CHECK(status==EWL_STATUS_DENIAL_OFFLINE,LBL_NEXT_STEP);


	//---messages on External PINPAD
    UI_ShowMessage("CHIP", mess_getReturnedMessage(MSG_RC_DECLINED_BY_CARD_L1),
    		mess_getReturnedMessage(MSG_RC_DECLINED_BY_CARD_L2), GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);


    RetFun= Reverse_Mtip(Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);

    return STM_RET_ERROR;

LBL_NEXT_STEP:

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

LBL_ERROR_REVERSE:
	 UI_ShowMessage("Error", "Error enviando reverso"," ", GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	 return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_HOST_ANALYSE
 * Description:	  Post Function of Post_STEP_HOST_ANALYSE
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_HOST_ANALYSE(char *Result)
{
    int RetFun;
    uint8 EntryMode;
    char ResponseCode[3] = {0x00, };

    ewlTechnology_t  technology = EWL_TECHNOLOGY_UNKNOWN;
    ewlTransactionStatus_t status;
    ewlHostAnswer_t answer;

    //Get Entry mode
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK( RetFun == RET_OK, LBL_ERROR);

    //Get Host response code
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_HOST_RESPONSE_CODE, ResponseCode, NULL);
    CHECK( RetFun == RET_OK, LBL_ERROR);

    //Check response code
	if ( memcmp( ResponseCode, "00", 2) == 0)
	{
		status = EWL_STATUS_APPROVED_ONLINE;
		answer = EWL_HOST_APPROVED;
	}
	else
	{
		status = EWL_STATUS_DENIAL_ONLINE;
		answer = EWL_HOST_DENIAL;
	}

	//Set EMV Tran Status
	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_EMV_TRAN_STATUS, &status, sizeof(status));
    CHECK( RetFun == RET_OK, LBL_ERROR);

    //Set host answer status
	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_ANSWER, &answer, sizeof(answer));
    CHECK( RetFun == RET_OK, LBL_ERROR);

    //Get Card Technology
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_CARD_TECHNOLOGY, &technology, NULL);

	if ((paymentToolsIsChipMagnetic(technology))
		|| (EntryMode == ENTRY_MODE_SWIPE)
		|| (EntryMode == ENTRY_MODE_MANUAL))
		return STM_RET_NEXT;

	RetFun = parameterSetHost(Tree_Tran);
    CHECK( RetFun == RET_OK, LBL_ERROR);

	return STM_RET_NEXT;

LBL_ERROR:

	return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_SECOND_TAP
 * Description:	  Post Function of STEP_SECOND_TAP
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_SECOND_TAP(char *Result)
{
	int RetFun;
	int NextStep = STM_RET_NEXT;
	uint16 TranName;

	uint32 Readers = 0;
	uint32 ReadResult;
	uint8 KeyPressed;
	Telium_File_t *hKey 	= NULL;

    char ResponseCode[3] = {0x00, };

    ewlTechnology_t  technology = EWL_TECHNOLOGY_UNKNOWN;
    ewlObject_t *ewl = EWL_GetEmvHandle();

    //Get Host response code
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_HOST_RESPONSE_CODE, ResponseCode, NULL);
    CHECK( RetFun == RET_OK, LBL_ERROR);
	CHECK(memcmp( ResponseCode, "00", 2) == 0, LBL_NEXT_STEP);

    //Get Card Technology
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_CARD_TECHNOLOGY, &technology, NULL);
    CHECK(RetFun == RET_OK, LBL_NEXT_STEP);

    //Check if CLESS technology
    RetFun = paymentToolsIsCless(technology);
    CHECK(RetFun == TRUE, LBL_NEXT_STEP);

    //Check second TAP
    RetFun = ewlSecondTapCheck(ewl);
    CHECK( RetFun == EWL_OK, LBL_NEXT_STEP);

    //---Wait second Tap
    RetFun = ClessEmv_DetectCardsStart(1,CL_TYPE_AB);
    CHECK( RetFun == 0, LBL_ERROR);

    //RetFun = WaitSecondTapCardCore();

	//Get Transaction name
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_NAME, &TranName, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	UI_SetLine(UI_TITLE, mess_getReturnedMessage(TranName), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, "ACERQUE NUEVAMENTE", UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE3, "TARJETA", UI_ALIGN_CENTER);

	Readers |= CLESS;
	Readers |= KEYBOARD;
	hKey = Telium_Fopen("KEYBOARD", "r*");

	if(IsColorDisplay())
		SetLedBlinking(SOFTWARE_LED_01, 25,25);
	else
		SetLedBlinking(HARDWARE_LED_01, 25,25);

	//--- Wait for event
	UI_ShowInfoScreen(NULL);
	buzzer(10);
	ReadResult = Telium_Ttestall(Readers, tConf.TimeOut * 100);

	//--- Check MANUAL Entry
	if ((ReadResult & KEYBOARD) == KEYBOARD)
	{
		KeyPressed = Telium_Getchar();

		if(KeyPressed == T_ANN)
			NextStep = STM_RET_CANCEL;
		else
			NextStep = STM_RET_AGAIN;
	}

	//--- Close peripherals
	if(hKey != NULL)
		Telium_Fclose(hKey);

	//--- Check TIMEOUT expired
	if (ReadResult == TIMEOUT)
		NextStep = STM_RET_CANCEL;

	if(NextStep != STM_RET_NEXT)
		utilLedsOff();

	return NextStep;

LBL_NEXT_STEP:

	return STM_RET_NEXT;

LBL_ERROR:

	RetFun = paymentError(Tree_Tran, RetFun, &NextStep);
	if(RetFun != RET_OK)
		return STM_RET_ERROR;

	return NextStep;

}

/* --------------------------------------------------------------------------
 * Function Name: Post_STEP_FINISH
 * Description:	  Post Function of STEP_FINISH
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_FINISH(char *Result)
{
	uint8 EntryMode;
	int RetFun;
	int NextStep;
	ewlObject_t * ewl = EWL_GetEmvHandle();
	ewlTransactionStatus_t status;
	TLV_TREE_NODE node;
	uint16 TranStatus;

	//Get Entry mode
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_ENTRY_MODE, &EntryMode, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    //Check Entry mode
    CHECK(	EntryMode == ENTRY_MODE_CHIP ||
    		EntryMode == ENTRY_MODE_CLESS_CHIP,
    		LBL_NEXT_STEP);

    RetFun = ewlFinishChip(ewl);
    CHECK( RetFun == EWL_OK, LBL_ERROR );

    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_EMV_TRAN_STATUS, &status, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    RetFun = dataRecoverFinishChip(Tree_Tran);
    CHECK( RetFun == EWL_OK, LBL_ERROR );

	//Get EMV Status
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_EMV_TRAN_STATUS, &status, NULL);
    CHECK(RetFun == RET_OK, LBL_ERROR);

    switch(status)
    {
        case EWL_STATUS_APPROVED_OFFLINE:
            return STEP_APPROVED_OFF;

        case EWL_STATUS_DENIAL_OFFLINE:
        	return STEP_DENIAL_OFF;

        case EWL_STATUS_APPROVED_ONLINE:
        	return STEP_PROCESS_RESPONSE;

        case EWL_STATUS_DENIAL_ONLINE:
        	return STEP_PROCESS_RESPONSE;

        default:
        	return STM_RET_ERROR;
    }

LBL_NEXT_STEP:

	return STM_RET_NEXT;

LBL_ERROR:

	RetFun = paymentError(Tree_Tran, RetFun, &NextStep);

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR1);
	TranStatus = *(uint16*)TlvTree_GetData(node);
	// turn on need reverse flag
	TranStatus|= FSTATUS_NEED_REVERSE;

	// update tree
	RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR1);
	// add record to batch
	if(TranStatus & FSTATUS_NEED_ADJUST)
	TranStatus &= ~FSTATUS_NEED_ADJUST;

	Tlv_SetTagValueInteger(Tree_Tran, TAG_TRAN_STATUS, TranStatus, LTAG_UINT16);
	RetFun =  Batch_Modify_Transaction(Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR1);



	LBL_ERROR1:
	if(RetFun != RET_OK)
		return STM_RET_ERROR;

	return STM_RET_ERROR;
}

//- EWL01

bool Pre_STEP_ENTER_ID_NUMBER(void)
{
	//+ EMV09 @mamata Jun 23, 2015
	TLV_TREE_NODE node;
	int16 MessageID;

	UI_ClearAllLines();
	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if(node != NULL)
		memcpy((char*)&MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MessageID), UI_ALIGN_CENTER);
	//- EMV09
	UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_ENTER_ID_NUMBER), UI_ALIGN_CENTER);
	return TRUE;
}

int Post_STEP_ENTER_ID_NUMBER(char *Result)
{
	int RetFun;

	RetFun = Tlv_SetTagValueString(Tree_Tran, TAG_ID_NUMBER, Step_Buffer);
	CHECK( RetFun == RET_OK, LBL_ERROR );

	UI_ClearLine(UI_LINE3);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}

bool Pre_STEP_SELECT_ACCOUNT_TYPE(void)
{
	TLV_TREE_NODE node;
	int RetFun;
	int TAccount;
	TABLE_RANGE Range;


	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);

	// verify CC_ID

	CHECK(Range.Flags1 & RANGE_FLAG1_DEBIT, LBL_FALSE);

	return TRUE;

LBL_FALSE:
	TAccount=3;
    node=TlvTree_AddChild(Tree_Tran, TAG_ACCOUNT_TYPE, &TAccount, LTAG_INT8);
    CHECK(node != NULL, LBL_ERROR);
	return FALSE;

LBL_ERROR:
	return STM_RET_ERROR;
}

/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_SELECT_ACCOUNT_TYPE(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;
	int TAccount;

	// AccountType is required, show AccountType menu
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_MAIN_ACCOUNT), 0);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SAVING_ACCOUNT), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_CURRENT_ACCOUNT), 2);

	TAccount = UI_MenuRun(mess_getReturnedMessage(MSG_ACCOUNT_TYPE), 30, 1);
	CHECK(TAccount >= 0, LBL_ERROR);

	//UI_ShowCleanMessage(3);

	// Add ACCOUNT TYPE to TAG
	node = TlvTree_Find(Tree_Tran, TAG_ACCOUNT_TYPE, 0);
	if(node != NULL)
	{
		RetFun=TlvTree_SetData( node, &TAccount, LTAG_INT8);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
	}
	else
	{
		node=TlvTree_AddChild(Tree_Tran, TAG_ACCOUNT_TYPE, &TAccount, LTAG_INT8);
		CHECK(node != NULL, LBL_ERROR);
	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}

int Post_STEP_COMPARE_READ_PAN(char *Result)
{
	int RetFun;
	TLV_TREE_NODE node;
	TLV_TREE_NODE Temp_Tran = TlvTree_New(0);
	uint32 l_Invoice;
	char cpStoredPAN[64] = { 0 };
	char cpCurrentPAN[64] = { 0 };
	uint8 EntryMode;
	TABLE_RANGE Range;



	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	EntryMode = *(uint8*)TlvTree_GetData(node);


	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(cpCurrentPAN, TlvTree_GetData(node), TlvTree_GetLength(node));

	// get Invoice number
	node = TlvTree_Find(Tree_Tran, TAG_STAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	l_Invoice = *(uint32*)TlvTree_GetData(node);

	// find transaction in batch files
	RetFun = Batch_FindTransaction(Temp_Tran, l_Invoice);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_MATCH)

	node = TlvTree_Find(Temp_Tran, TAG_PAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(cpStoredPAN, TlvTree_GetData(node), TlvTree_GetLength(node));

	CHECK(strcmp(cpCurrentPAN, cpStoredPAN) == 0, LBL_NO_MATCH);

	if (EntryMode== ENTRY_MODE_MANUAL) {
			CHECK(!(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_NO_MATCH);
		}

	TlvTree_Release(Temp_Tran);
	return STM_RET_NEXT;

LBL_NO_MATCH:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_INVALID_CARD), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, 30);
	TlvTree_Release(Temp_Tran);
	return STM_RET_ERROR;

LBL_ERROR:
	TlvTree_Release(Temp_Tran);
	return STM_RET_ERROR;
}

bool Pre_STEP_TRAN_SERVER(void)
{
	TABLE_RANGE Range;
	int RetFun;

	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);

	CHECK(tTerminal.Comercio & RESTAURANT && !(Range.Flags1 & RANGE_FLAG1_DEBIT), LBL_FASLE);

	UI_ClearLine(UI_LINE3);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_SERVER), UI_ALIGN_CENTER);
	return TRUE;

	LBL_FASLE:
		return FALSE;

}


int Post_STEP_TRAN_SERVER(char *Result)
{
//	CHECK(tTerminal.Comercio & RESTAURANT, LBL_FASLE);

	Tlv_SetTagValueInteger(Tree_Tran, TAG_SERVER_NUMBER, atoi(Step_Buffer), LTAG_UINT32);
	return STM_RET_NEXT;

//	LBL_FASLE:
//			return STM_RET_NEXT;
}
int post_MENU_CONFIG (char *Result){

	int ret;

	ret = Menu_GeneralConfiguration();

	if(ret == PANEL_CONTROL)
		return PANEL_CONTROL;

	return STM_RET_NEXT;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_LAST_ANSWER
 * Description:		Post function to STEP_INVOICE
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_LAST_ANSWER(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;
	TABLE_MERCHANT Merchant;
	uint16 TranID;
	uint16 TranID1;
	uint16 IdMerchant;
	TABLE_HEADER TableHeader;

	// get transaciton id
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN);
	memcpy((char*)&TranID1, &TranID, LTAG_UINT16);

	UI_ShowProcessScreen(mess_getReturnedMessage(MSG_SEARCHING_TRAN));


	//id merchant con id tx
	node = TlvTree_Find(Tree_Tran, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_NO_TRAN);
	memcpy((char*)&IdMerchant, TlvTree_GetData(node), LTAG_UINT16);



	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	// validate if batch is not already settled
	CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);

	//VERIFICO EL FLAG SI LA ULTIMA TX FUE NEGADA PARA ESE MERCHANT
	CHECK(!(Merchant.Flags1 & MERCH_FLAG1_TX_NEGADA), LBL_ERROR_NEGADA);

	// get batch header
	RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	// validate if batch if not empty
	CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);



	//last_answer
		RetFun = Batch_Get_Last_Transaction(Tree_Tran, (uint8*)Merchant.BatchName);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		Tlv_SetTagValueInteger(Tree_Tran, TAG_TRAN_ID, TranID1, LTAG_INT16);

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;


LBL_NO_TRAN:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_TRAN_BATCH), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_TRAN_OLD), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

LBL_ERROR_NEGADA:
	UI_ShowMessage(NULL,
		mess_getReturnedMessage(MSG_RC_DECLINED),"",GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		utilWaitToRemoveChipCard();
		SetIdleHeaderFooter();
		return -3;

}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_SERVER
 * Description:		Pre function to STEP_SERVER
 * Parameters:
 *  - none
 * Return:
 *  - true
 * Notes:
 */
bool Pre_STEP_SERVER(void)
{
	//TLV_TREE_NODE node;
	//int16 MessageID;
	int RetFun;

	if (IsPCLDoTransaction())
	{
		char TempBuffer[30];
		unsigned int MaxLn = sizeof(TempBuffer) - 1;

		memset(TempBuffer, 0x00, sizeof(TempBuffer));

		RetFun = Tlv_GetTagValue(stPclTransFields, PCL_TIP_AMOUNT, TempBuffer,
				&MaxLn);
		if (RetFun == RET_OK && MaxLn > 0)
			strcpy(Step_Buffer, TempBuffer);

		//Change step configuration, to Force Post_step
		if (Step_Buffer[0] != NULL)
		{
			STM_CustomInputFields(0, 0, 0, INPUT_NONE);
			return TRUE;
		}
	}
	// get transaction id from Tree_Tran
	/*node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if (node != NULL)
		memcpy((char*) &MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;*/

	// set title and prompt line
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DETAIL_SERVER), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_SERVER), UI_ALIGN_CENTER);
	return TRUE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_SERVER
 * Description:		Post function to STEP_SERVER
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_CANCEL
 * Notes:
 */
int Post_STEP_SERVER(char *Result)
{
	int RetFun;
	uint32 Mesero;


	UI_ShowProcessScreen(mess_getReturnedMessage(MSG_SEARCHING_SERVER));

	// convert invoice to int
	Mesero= atol(Step_Buffer);

	CHECK(Mesero != 0, LBL_NO_TRAN)

	RetFun = Batch_FindServer(Tree_Tran, Mesero);
	CHECK(RetFun == BATCH_RET_OK, LBL_NO_TRAN)

	 TlvTree_AddChild(Tree_Tran, TAG_SERVER_NUMBER, &Mesero, 5+1);

	return STM_RET_NEXT;

LBL_NO_TRAN:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_NO_SERVER), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
	return STM_RET_CANCEL;

}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_PIN_NUMBER
 * Description:		Post function to STEP_PIN_NUMBER
 * Parameters:
 *  - char *Result
 * Return:
 *  - STM_RET_NEXT
 *  - STM_RET_ERROR
 *  - STM_RET_CANCEL
 * Notes:
 */
bool Pre_STEP_PIN_NUMBER(void)
{
	//+ EMV09 @mamata Jun 23, 2015
	TLV_TREE_NODE node;
	int16 MessageID;

	UI_ClearAllLines();
	// get transaction id from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	if(node != NULL)
		memcpy((char*)&MessageID, TlvTree_GetData(node), LTAG_UINT16);
	else
		MessageID = 0xFFFF;
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MessageID), UI_ALIGN_CENTER);
	//- EMV09
	UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_ENTER_TOKEN), UI_ALIGN_CENTER);
	return TRUE;
}

int Post_STEP_PIN_NUMBER(char *Result)
{
	int RetFun;

	RetFun = Tlv_SetTagValueString(Tree_Tran, TAG_TOKENBLOCK, Step_Buffer);
	CHECK( RetFun == RET_OK, LBL_ERROR );

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;
}



/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
int Post_STEP_SELECT_BANK(char *Result)
{
	TLV_TREE_NODE node;
	int RetFun;
	int TBank=0;
	char TBanco[4];
	TABLE_BANK Banco;
	int FunResult;
	int NumItems=0;

	memset(TBanco, 0, 4+1);

	FunResult = TM_FindFirst(TAB_BANK, &Banco);
	CHECK(FunResult == TM_RET_OK, LBL_ERROR);
	UI_MenuReset();
	do
	{
			// Transaction is enable, add Merchant Group to menu
			UI_MenuAddItem(Banco.Name, Banco.HostID);
			NumItems++;
		// try to find next record
		FunResult = TM_FindNext(TAB_BANK, &Banco);
	//+ NORECORDS @mamata 6/05/2016
	//d }while(FunResult != TM_RET_NO_MORE_RECORDS);
	}while(FunResult == TM_RET_OK);
	CHECK(FunResult == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	// check if no item found, then cancel transaction
	CHECK(NumItems > 0, LBL_NO_ITEMS);

	TBank = UI_MenuRun(mess_getReturnedMessage(MSG_BANK_TYPE), tConf.TimeOut, 0);
	CHECK( TBank > 0, LBL_ERROR );

	Telium_Sprintf((char*)TBanco, "%04d", TBank);


	// Add bank selected to TAG
	node = TlvTree_Find(Tree_Tran, TAG_BANK_TYPE, 0);
	if(node != NULL)
	{
		RetFun=TlvTree_SetData( node, &TBanco, LTAG_INT32);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
	}
	else
	{
		node=TlvTree_AddChild(Tree_Tran, TAG_BANK_TYPE, &TBanco, LTAG_INT32);
		CHECK(node != NULL, LBL_ERROR);
	}

	return STM_RET_NEXT;

LBL_ERROR:
	return STM_RET_ERROR;

	LBL_NO_ITEMS:
	UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_RC_EMV_CANCELED), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

		return STM_RET_CANCEL;
}

int Post_STEP_Validate_Mount(char *Result){

	TLV_TREE_NODE node;
	amount Amount;
	amount Total;
	amount TotalLote;
	TOTALS Totals;
	bool reporte=FALSE;
	bool EntryMode=FALSE;
	int RetFun;
	TABLE_MERCHANT Merchant;


	node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);

	Amount = *(amount*)TlvTree_GetData(node);
//	Amount = atof(Amount);
	TotalLote=0;
	Total=0;
//	 get Merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);


	RetFun = Totals_MerchantCalculate(&Merchant, &Totals, 0, reporte, EntryMode);
	CHECK(RetFun == RET_OK || RetFun == RET_PRIM_TX, LBL_ERROR);


	if (RetFun == RET_PRIM_TX)
	{
		TotalLote=Amount;
	}
	else
	{
		Total = Totals[Index_SALES].Amount - Totals[Index_TAX].Amount - Totals[Index_TIP].Amount;
		TotalLote=Total+Amount;

	}

	// verify max amount
	if(Merchant.Amount_Max != 0)
		CHECK(Amount <= Merchant.Amount_Max, LBL_BIGER_AMOUNT);
//
//	// verify max amount for LOTE -- JR07092020 obliga realizar cierre
	if(Merchant.Amount_MaxDigtisLote != 0)
	CHECK(TotalLote <= Merchant.Amount_MaxDigtisLote, LBL_BIGER_AMOUNT_LOTE);


	return STM_RET_NEXT;

	LBL_ERROR:
				return STM_RET_ERROR;
	LBL_BIGER_AMOUNT:
				UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_AMOUNT), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
				memset(Step_Buffer, 0, L_buffer);
				return STM_RET_ERROR;

	LBL_BIGER_AMOUNT_LOTE:
				    TM_FindFirst(TAB_TERMINAL, &tTerminal);
					tTerminal.Flags1 |= TERM_FLAG1_SETTLE_OBLIGADO;
					TM_ModifyRecord(TAB_TERMINAL, &tTerminal);

				UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_BIG_AMOUNT_LOTE), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_WARNING, GL_BUTTON_ALL, tConf.TimeOut);
				return STM_RET_ERROR;
}
int Post_STEP_CHECK_FALLBACK(char *Result){


	TABLE_RANGE Range;
	int RetFun;
	uint8 FlagFallback = FALSE;
	TLV_TREE_NODE node;
	uint8 EntryMode;


	// verify entry mode is swipe
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);

	if (EntryMode == ENTRY_MODE_SWIPE ) {


		RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
		// verify CC_ID

		CHECK(Range.Swipe == RET_OK, LBL_FALSE);
	}


Tlv_GetTagValue(Tree_Tran, TAG_FALLBACK, &FlagFallback, NULL);
if (FlagFallback ) {


	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
	// verify CC_ID

	CHECK(Range.Fallback == RET_OK, LBL_FALSE);
}


	return STM_RET_NEXT;


	LBL_FALSE:
	UI_ShowMessage(NULL,"TRANSACCION NO SOPORTADA", NULL, GL_ICON_ERROR , GL_BUTTON_ALL, tConf.TimeOut);
	LBL_ERROR:
	return STM_RET_ERROR;
}
