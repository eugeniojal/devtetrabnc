/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Reports.c
 * Header file:		Reports.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"
extern uint16 TRAN_STEPS_TEST[];
extern uint16 TRAN_STEPS_TEST_AUTO[];
extern uint16 TRAN_STEPS_LOGON[];
extern uint16 TRN_TECNICIAN_LOGON[];
extern uint16 TRAN_STEPS_REFUND[];
extern uint16 STEPS_DUPLICATE[];
extern uint16 STEPS_LAST_ANSWER[];
extern uint16 TRAN_STEPS_VOID[];
extern uint16 TRN_CONFIG_COMM_USER[];
/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
unsigned short TRN_TOTALS_REPORT[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP_ALL,
	STEP_SELECT_MERCHANT_ALL,
	STEP_PRINT_TOTALS_REPORT,
	STEP_END_LIST
};


unsigned short TRN_DETIALS_REPORT[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP_ALL,
	STEP_SELECT_MERCHANT_ALL,
	STEP_PRINT_DETAILS_REPORT,
	STEP_END_LIST
};


unsigned short TRN_SETTLE[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP_ALL,
	STEP_SELECT_MERCHANT_ALL,
	STEP_RUN_SETTLE,
	STEP_END_LIST
};


/* ***************************************************************************
 * total mesero
 * ***************************************************************************/
unsigned short TRN_TOTALS_SERVER[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP_ALL,
	STEP_SELECT_MERCHANT_ALL,
	STEP_PRINT_DETAILS_SERVER,
	STEP_END_LIST
};

/* ***************************************************************************
 * total mesero
 * ***************************************************************************/
unsigned short TRN_DETIALS_SERVER[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP_ALL,
	STEP_SELECT_MERCHANT_ALL,
	STEP_SERVER,
	STEP_PRINT_DETAILS_SERVER,
	STEP_END_LIST
};

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Menu_GeneralConfiguration
 * Description:		Run menu of general configuration
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Menu_Transacciones(void)
{
	int RetFun;
	UI_ClearAllLines();

	// build menu
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_SALE), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_REFUND), 2);
	if(tTerminal.Comercio == RESTAURANT){
		UI_MenuAddItem(mess_getReturnedMessage(MSG_A_PROPINA),3);
	}
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_C2P), 4);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_MERCHANT_MENU), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	UI_ClearAllLines();
	Tree_Tran = TlvTree_New(0);

	switch(RetFun)
	{
	    case 1:
			 RetFun = RunTransaction(TRAN_SALE, NULL, NULL, FALSE);
			 UI_ClearAllLines();
			break;
		case 2:
			UI_ClearAllLines();
			 RetFun = RunTransaction(TRAN_VOID, NULL, NULL, FALSE);
			 UI_ClearAllLines();
			break;
		case 3:
			 RetFun = RunTransaction(TRAN_ADJUST, NULL, NULL, FALSE);
			UI_ClearAllLines();
			break;
		case 4:
			RetFun = RunTransaction(TRAN_C2P, NULL, NULL, FALSE);
			UI_ClearAllLines();
			break;
		default:
			UI_ClearAllLines();
			return RET_NOK;
	}

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}
int Menu_Merchant(void)
{
	int RetFun;
	uint16 TranID;
	UI_ClearAllLines();

	// build menu
	UI_MenuReset();
	UI_MenuAddItem(mess_getReturnedMessage(MSG_CLIENT_COPY), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_OLD), 2);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_QUICK_SALE), 3);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_TEST), 4);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_REPORTS), 5);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TRAN_SETTLE), 6);
	UI_MenuAddItem("COMUNICACION", 7);
//	if(tTerminal.Comercio == RENTACAR || tTerminal.Comercio == CLINICAS||tTerminal.Comercio == HOTELES){
//		UI_MenuAddItem(mess_getReturnedMessage(MSG_COMPLETITUD), 12);
//		UI_MenuAddItem(mess_getReturnedMessage(MSG_PRE_AUTORAZACION), 13);
//	}
	// run menu
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_MERCHANT_MENU), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	UI_ClearAllLines();
	Tree_Tran = TlvTree_New(0);

	switch(RetFun)
	{
	    case 1:
			UI_ClearAllLines();
			RetFun = RunTransaction(DUPLICATE, NULL, NULL, FALSE);
			UI_ClearAllLines();
				break;
		case 2:
			UI_ClearAllLines();
			RetFun = RunTransaction(LAST_ANSWER, NULL, NULL, FALSE);
			UI_ClearAllLines();
			break;
		case 3:
			UI_ClearAllLines();
			RetFun = RunTransaction(TRAN_LOGON, NULL, NULL, FALSE);
			UI_ClearAllLines();
			break;
		case 4:
			UI_ClearAllLines();
			RetFun = RunTransaction(TRAN_TEST, NULL, NULL, FALSE);
			UI_ClearAllLines();
			break;

		//+ DUALSIM @mamata Jun 24, 2015
		case 5:
			ReportesGeneral();
			break;
		//- DUALSIM
		case 6:
			//Report_ReprintSettle();
			TranID = TRAN_SETTLE;
			TlvTree_AddChild(Tree_Tran, TAG_TRAN_ID, &TranID, LTAG_UINT16);
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_SETTLE), UI_ALIGN_CENTER);
		    RetFun = STM_RunTransaction(TRN_SETTLE, STEP_LIST);
		    UI_ClearAllLines();
			break;
		case 7:
			//Report_ReprintSettle();
		    RetFun = STM_RunTransaction(TRN_CONFIG_COMM_USER, CONF_STEP_LIST);
		    UI_ClearAllLines();
			break;
		default:
			UI_ClearAllLines();
			return RET_NOK;
	}

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}

int Reportes(void)
{
	int RetFun;

	UI_ClearAllLines();

	UI_MenuReset();

	UI_MenuAddItem(mess_getReturnedMessage(MSG_DETAIL_REPORT), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TOTALS_REPORT), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_TX_REPORT), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	UI_ClearAllLines();
	Tree_Tran = TlvTree_New(0);

	switch(RetFun)
	{
	    case 1:
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DETAIL_REPORT), UI_ALIGN_CENTER);
			RetFun = STM_RunTransaction(TRN_DETIALS_REPORT, STEP_LIST);
				break;
		case 2:
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_TOTALS_REPORT), UI_ALIGN_CENTER);
			RetFun = STM_RunTransaction(TRN_TOTALS_REPORT, STEP_LIST);
			break;

		default:
			UI_ClearAllLines();
			return RET_NOK;
	}

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}

int ReportesGeneral(void)
{
	int RetFun;

	UI_ClearAllLines();

	UI_MenuReset();

	UI_MenuAddItem(mess_getReturnedMessage(MSG_TX_REPORT), 1);
	if(tTerminal.Comercio == RESTAURANT){
	UI_MenuAddItem(mess_getReturnedMessage(MSG_SERVER_REPORT), 2);
	}
	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_REPORTS), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	UI_ClearAllLines();
	Tree_Tran = TlvTree_New(0);

	switch(RetFun)
	{
	    case 1:
			Reportes();
			break;
		case 2:
			ReporteMesero();
			break;

		default:
			UI_ClearAllLines();
			return RET_NOK;
	}

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}


int ReporteMesero(void)
{
	int RetFun;

	UI_ClearAllLines();

	UI_MenuReset();

	UI_MenuAddItem(mess_getReturnedMessage(MSG_DETAIL_SERVER), 1);
	UI_MenuAddItem(mess_getReturnedMessage(MSG_TOTALS_SERVER), 2);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_SERVER_REPORT), 30, 1);
	CHECK(RetFun > 0, LBL_MENU_END);

	UI_ClearAllLines();
	Tree_Tran = TlvTree_New(0);

	switch(RetFun)
	{
	    case 1:
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DETAIL_SERVER), UI_ALIGN_CENTER);
			RetFun = STM_RunTransaction(TRN_DETIALS_SERVER, STEP_LIST);
				break;
		case 2:
			UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_TOTALS_SERVER), UI_ALIGN_CENTER);
			RetFun = STM_RunTransaction(TRN_TOTALS_SERVER, STEP_LIST);
			break;

		default:
			UI_ClearAllLines();
			return RET_NOK;
	}

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	return RET_OK;

LBL_MENU_END:
	return RET_NOK;
}
