/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Tables.c
 * Header file:		Tables.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
#define TAG_CL_RANGE 1
#define TAG_CL_MERCHANT 2


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int Init_Table_Terminal(void);
int Init_Table_Merchant(void);
int Init_Table_Host(void);
int Init_Table_Range(void);

int Init_Table_IP(void);
int Init_Table_MerchantRange(void);
int Init_Table_MerchantGroup(void);
int Init_Table_COUNTERS(void);

//+ INGESTATE01 @jbotero 13/09/2015
int Init_Table_GroupRange(void);
//- INGESTATE01

int Init_Table_DataKeyInfo(void);
int Init_Table_PinKeyInfo(void);
int Init_Table_Mesero(void);

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	Tables_Init
 * Description:		Init all tables
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int Tables_Init(void)
{
	int ReturnedValue;

	// Init table: GENERAL CONFIGURATION
	Tree_NoPrinter = NULL;
	ReturnedValue = Init_TAB_GENERAL_CONF();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_Terminal();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_Merchant();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_Host();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_Range();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_Mesero();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_IP();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_MerchantRange();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_MerchantGroup();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_COUNTERS();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Batch_InitTables();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	//+ INGESTATE01 @jbotero 13/09/2015
	ReturnedValue = Init_Table_GroupRange();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);
	//- INGESTATE01

	ReturnedValue = Init_Table_DataKeyInfo();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	ReturnedValue = Init_Table_PinKeyInfo();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);


	ReturnedValue = Init_Table_Bank();
	CHECK(ReturnedValue == RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_Terminal
 * Description:		Init table TAB_TERMINAL
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_TERMINAL
 * Notes:
 */
int Init_Table_Terminal(void)
{
	int RetFun;

	// Init table
	RetFun = TM_CreateTable(TAB_TERMINAL, L_TABLE_TERMINAL);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	// Find fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_TERMINAL, &tTerminal);
	// Clear tTerminal if no data present
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		memset((char*)&tTerminal, 0, L_TABLE_TERMINAL);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_Merchant
 * Description:		Init table TAB_MERCHANT
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_MERCHANT
 * Notes:
 */
int Init_Table_Merchant(void)
{
	int RetFun;
	TABLE_MERCHANT tMerchant;

	// Init table
	RetFun = TM_CreateTable(TAB_MERCHANT, L_TABLE_MERCHANT);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	// Find fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_MERCHANT, &tMerchant);
	// Clear tTerminal if no data present
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		memset((char*)&tMerchant, 0, L_TABLE_MERCHANT);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_Host
 * Description:		Init table TAB_HOST
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_HOST
 * Notes:
 */
int Init_Table_Host(void)
{
	int RetFun;
	TABLE_HOST tHost;

	// Init table
	RetFun = TM_CreateTable(TAB_HOST, L_TABLE_HOST);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	// Find fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_HOST, &tHost);
	// Clear tTerminal if no data present
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		memset((char*)&tHost, 0, L_TABLE_HOST);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_Range
 * Description:		Init table TAB_RANGE
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_RANGE
 * Notes:
 */
int Init_Table_Range(void)
{
	int RetFun;
	TABLE_RANGE tRange;

	// Init table
	RetFun = TM_CreateTable(TAB_RANGE, L_TABLE_RANGE);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	// Find fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_RANGE, &tRange);
	// Clear tTerminal if no data present
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		memset((char*)&tRange, 0, L_TABLE_RANGE);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_Banck
 * Description:		Init table TAB_RANGE
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_RANGE
 * Notes:
 */
int Init_Table_Bank(void)
{
	int RetFun;
	TABLE_BANK tBank;

	// Init table
	RetFun = TM_CreateTable(TAB_BANK, L_TABLE_BANK);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	// Find fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_BANK, &tBank);
	// Clear tTerminal if no data present
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		memset((char*)&tBank, 0, L_TABLE_BANK);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_IP
 * Description:		Init table TAB_RANGE
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_RANGE
 * Notes:
 */
int Init_Table_IP(void)
{
	int RetFun;

	// Init table
	RetFun = TM_CreateTable(TAB_IP, L_TABLE_IP);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_MerchantRange
 * Description:		Init table TAB_MERCH_RANGE
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_RANGE
 * Notes:
 */
int Init_Table_MerchantRange(void)
{
	int RetFun;

	// Init table
	RetFun = TM_CreateTable(TAB_MERCH_RANGE, L_TABLE_MERCH_RANGE);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_MerchantGroup
 * Description:		Init table TAB_MERCH_GROUP
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_RANGE
 * Notes:
 */
int Init_Table_MerchantGroup(void)
{
	int RetFun;

	// Init table
	RetFun = TM_CreateTable(TAB_MERCH_GROUP, L_TABLE_MERCH_GROUP);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

//+ INGESTATE01 @jbotero 13/09/2015
/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_GroupRange
 * Description:		Init Table GroupRange(GRUPO DE RANGOS) -> For Ingestate
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init
 * Notes:
 */
int Init_Table_GroupRange(void)
{
	int RetFun;

	// Init table
	RetFun = TM_CreateTable(TAB_GROUP_RANGE, L_TABLE_GROUP_RANGE);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

//- INGESTATE01
/** ------------------------------------------------------------------------------
 * Function Name:	Init_Table_Statistics
 * Author:			@DFVC
 * Date:		 	Jun 13, 2014
 * Description:		Init table TABLE_STATISTICS
 * 					if table is not created init TABLE_STATISTICS with default parameters
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file or from defualt parameters
 *  - RET_NOK - cannot init TAB_Totales_Record
 * Notes:
 */
int Init_Table_Statistics(void)
{
	int RetFun;

	// Init TAB_GENERAL_CONF
	RetFun = TM_CreateTable(TAB_MERCH_STATISTICS, L_TABLE_STATISTICS);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	memset(&tStatistics, 0, L_TABLE_STATISTICS);

	// Fins fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_MERCH_STATISTICS, &tStatistics);
	if(RetFun == TM_RET_NO_MORE_RECORDS)
	{
		// table doesn't have records, apply default recrods
		tStatistics.MsgSend = 0;
		tStatistics.MsgRecive = 0;
		tStatistics.Transac = 0;
		tStatistics.Redials = 0;
		tStatistics.ErrorCom = 0;
		tStatistics.TimeoutTransac = 0;
		tStatistics.TimeoutRever = 0;
		tStatistics.Timeout8Seg = 0;
		tStatistics.Timeout16Seg = 0;
		tStatistics.Timeout45Seg = 0;
		tStatistics.Timeout90Seg = 0;
		tStatistics.TransSecundaryIp = 0;
		tStatistics.TransPrimaryIp2 = 0;
		tStatistics.TransSecundaryIp2 = 0;
		tStatistics.MagCardReaderError = 0;
		tStatistics.MagCardReader = 0;
		tStatistics.ActiveBatchNum = 1;

		// add record to file
		RetFun = TM_AddRecord(TAB_MERCH_STATISTICS, &tStatistics);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// read record
		RetFun = TM_FindFirst(TAB_MERCH_STATISTICS, &tStatistics);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_COUNTERS
 * Description:		Init table TAB_COUNTERS
 * 					if table is not created, then init table with default values
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file or from defualt parameters
 *  - RET_NOK - cannot init table
 * Notes:
 */
int Init_Table_COUNTERS(void)
{
	TABLE_COUNTERS Counters;
	int RetFun;

	// Init TAB_GENERAL_CONF
	RetFun = TM_CreateTable(TAB_COUNTERS, L_TABLE_COUNTERS);

	if(RetFun == TM_RET_FILE_EXISISTS)
		return RET_OK;

	if(RetFun == TM_RET_FILE_CREATION_ERROR)
		return RET_NOK;
	memset(&Counters,0x00,sizeof(Counters));
	Counters.Header.RecordID = 1;
	Counters.Invoice = 1;
	Counters.STAN = 1;

	RetFun = TM_AddRecord(TAB_COUNTERS, &Counters);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_DataKeyInfo
 * Description:		Init table Data Key Info
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_KEY_INFO
 * Notes:
 */
int Init_Table_DataKeyInfo(void)
{
	int RetFun;

	// Initialize table
	RetFun = TM_CreateTable(TAB_DATA_KEY_INFO, L_TABLE_KEY_INFO);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

}

/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_PinKeyInfo
 * Description:		Init table Pin Key Info
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_KEY_INFO
 * Notes:
 */
int Init_Table_PinKeyInfo(void)
{
	int RetFun;

	// Initialize table
	RetFun = TM_CreateTable(TAB_PIN_KEY_INFO, L_TABLE_KEY_INFO);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Init_Table_MESERO
 * Description:		Init table MESERO
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - table loaded from file
 *  - RET_NOK - cannot init TAB_RANGE
 * Notes:
 */
int Init_Table_Mesero(void)
{
	int RetFun;
	TABLE_MESERO tMesero;

	// Init table
	RetFun = TM_CreateTable(TAB_MESERO, L_TABLE_MESERO);
	CHECK(RetFun != TM_RET_FILE_CREATION_ERROR, LBL_ERROR);

	memset(&tMesero, 0, L_TABLE_MESERO);
	// Find fist record of TAB_GENERAL_CONF
	RetFun = TM_FindFirst(TAB_MESERO, &tMesero);
	if(RetFun == TM_RET_NO_MORE_RECORDS)
	{
		tMesero.IdMesero=0;
		// add record to file
			RetFun = TM_AddRecord(TAB_MESERO, &tMesero);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);

			// read record
			RetFun = TM_FindFirst(TAB_MESERO, &tMesero);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	}

	return RET_OK;
LBL_ERROR:
	return RET_NOK;
}
/* --------------------------------------------------------------------------
 * Function Name:	MerchGroup_IsTransactionEnable
 * Description:		Verify if almost one merchant of a specific MerchantGroup
 * 					has enable a specific transaction
 * Parameters:
 *  - int32 MerchGroupID - Id of Merchant Group
 *  - TRANS_LIST TranID - Id of transaction
 * Return:
 *  - TRUE - the transaction is enable for this merchant group
 *  - FALSE - the transaction isn't enable ofr this merchant group
 * Notes:
 */
bool MerchGroup_IsTransactionEnable(int32 MerchGroupID, TRANS_LIST TranID)
{
	int FunResult;
	bool Found = false;
	TABLE_MERCHANT tmpMerchant;

	FunResult = TM_FindFirst(TAB_MERCHANT, &tmpMerchant);
	CHECK(FunResult == TM_RET_OK, LBL_NO_MERCH);

	do
	{
		if(tmpMerchant.MerchantGroupID == MerchGroupID)
		{
			if(Merchant_IsTransactionEnable(tmpMerchant.Transactions1, tmpMerchant.Transactions2, TranID))
				Found = TRUE;
		}
		FunResult = TM_FindNext(TAB_MERCHANT, &tmpMerchant);

	//+ NORECORDS @mamata 6/05/2016
	//d }while(FunResult != TM_RET_NO_MORE_RECORDS && Found != TRUE);
	}while(FunResult == TM_RET_OK && Found != TRUE);
	//- NORECORDS

	if(Found)
		return TRUE;

	return FALSE;

LBL_NO_MERCH:
	return FALSE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Merchant_IsTransactionEnable
 * Description:		Verify if a transaction is enable in a specific merchant
 * Parameters:
 * 	- TABLE_MERCHANT Merchant - Merchant to verify
 * 	- TRANS_LIST TranID - transaction to verify
 * Return:
 *  - true - transaction is enable in Merchant
 *  - false - transaction isn't enable in Merchant
 * Notes:
 */
bool Merchant_IsTransactionEnable(uint16 TranFlags1, uint16 TranFlags2, uint16 TranID)
{
	bool FunctionReturn = FALSE;
	switch(TranID)
	{
		case TRAN_SALE:
		case TRAN_C2P:
			if(TranFlags1 & MERCH_TRAN1_SALE)
				FunctionReturn = TRUE;
			break;

		case TRAN_REFUND:
			if(TranFlags1 & MERCH_TRAN1_REFUND)
				FunctionReturn = TRUE;
			break;

		case TRAN_VOID:
			if(TranFlags1 & MERCH_TRAN1_VOID)
				FunctionReturn = TRUE;
			break;

		case TRAN_ADJUST:
			if(TranFlags1 & MERCH_TRAN1_ADJUST)
				FunctionReturn = TRUE;
			break;

		case TRAN_REVERSE:
		case TRAN_TEST:
		case TRAN_SETTLE:
		case TRAN_UPLOAD:
		case TRAN_LOGON:
		case TRAN_OFFLINE:
		case DUPLICATE:
		case LAST_ANSWER:
			FunctionReturn = TRUE;
			break;

//+ CLESS @mamata Dec 11, 2014
		case TRAN_QUICK_SALE:
			if(TranFlags1 & MERCH_TRAN1_QUICK_SALE)
				FunctionReturn = TRUE;
			break;
		//- CLESS

		case TRAN_END_LIST:
			FunctionReturn = FALSE;
			break;
	}

	return FunctionReturn;


}


/* --------------------------------------------------------------------------
 * Function Name:	BuildRangeCandidateList
 * Description:		Build candidate list of ranges using TAG_PAN
 * Parameters:
 *  - TLV_TREE_NODE Tree_RangeCandidateList - where to place candidate list
 * Return:
 *  - RET_OK - creates candidate list of ranges
 *  - RET_NOK - empty candidate list
 *  -2 - no PAN in Tran_Tree
 *  -3 - tlvtree error
 * Notes:
 */
//eugenio---
int BuildRangeCandidateList(TLV_TREE_NODE Tree_RangeCandidateList)
{
	TABLE_RANGE Range;
	TLV_TREE_NODE node;
	uint8 tmpBuffer[11];
	uint8 tmpPAN[25];
	uint8 tmpPANLen;
	uint64 CardBin;
	uint8 tmpBINLen;
	int RetFun;
	int CandidateCont=0;
	uint8 EntryMode;
	uint8 PANLuhn;
	bool AddRange;

	// get card bin
	memset(tmpBuffer, 0, 11);
	node = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
	CHECK(node != NULL, LBL_NO_PAN);
	if( TlvTree_GetLength(node) < 10 )
		memcpy(tmpBuffer, TlvTree_GetData(node), TlvTree_GetLength(node));
	else
		memcpy(tmpBuffer, TlvTree_GetData(node), 10);
	CardBin = atof((char*)tmpBuffer);

	memset(tmpPAN, 0, 25);
	tmpPANLen = TlvTree_GetLength(node);
	memcpy(tmpPAN, TlvTree_GetData(node), tmpPANLen);
	PANLuhn = cle_luhn(tmpPAN, tmpPANLen - 1) + 0x30;

	// get entry mode
	node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_NO_PAN);


	RetFun = TM_FindFirst(TAB_RANGE, &Range);
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);
	memcpy(&EntryMode, TlvTree_GetData(node), LTAG_UINT8);

	do
	{
		char tmp[32];
		uint64 nBinMin = Range.Bin_Min;
		uint64 nBinMax = Range.Bin_Max;

		tmpBINLen = convIntToTxt(nBinMin, tmp, 11, 10);
		Telium_Sprintf(tmp, "%*.*s", tmpBINLen, tmpBINLen, tmpBuffer);
		CardBin = strtoull(tmp, NULL, 10);

		if(CardBin >= nBinMin  && CardBin <= nBinMax)
		{
			AddRange = TRUE;

			// validate entry mode
			if(EntryMode == ENTRY_MODE_MANUAL)
			{
				if(!(Range.Flags1 & RANGE_FLAG1_MANUAL_ENTRY))
					AddRange = FALSE;
			}

			// validate PAN check digit (Luhn)
			if(Range.Flags1 & RANGE_FLAG1_VERIFY_CHECK_DIGIT)
			{
				if(tmpPAN[tmpPANLen-1] != PANLuhn)
					AddRange = FALSE;
			}

			// Everything is Ok add range to candidate list
			if(AddRange)
			{
				node = TlvTree_AddChild(Tree_RangeCandidateList, Range.Header.RecordID, &Range.Name, strlen(Range.Name));
				CHECK(node != NULL, LBL_TREE_ERROR);
				CandidateCont++;
			}
		}
		RetFun = TM_FindNext(TAB_RANGE, &Range);
		//+ NORECORDS @mamata 6/05/2016
		//d }while(RetFun != TM_RET_NO_MORE_RECORDS);
	}while(RetFun == TM_RET_OK);
	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	if(CandidateCont > 0)
		return RET_OK;
	else
		return RET_NOK;

	LBL_NO_PAN:
	return -2;

	LBL_TREE_ERROR:
	return -3;

	//+ NORECORDS @mamata 6/05/2016
	LBL_ERROR:
	return RET_NOK;
	//- NORECORDS
}


/* --------------------------------------------------------------------------
 * Function Name:	BuildMerchantCandidateList
 * Description:		Build candidate list of Merchants using candidte list of Range
 * Parameters:
 *  - TLV_TREE_NODE Tree_MerchantCL - where to place candide list
 *  - TLV_TREE_NODE Tree_RangeCL - candidate list of ranges
 *  - uint32 MerchantGroupID - Merchant group filter
 *  - bool *MultiCurrency - OUT - flag to inform exists muti currency in candidate list of Merchants
 * Return:
 * Notes:
 */
int BuildMerchantCandidateList(TLV_TREE_NODE Tree_MerchantCL, TLV_TREE_NODE Tree_RangeCL, uint32 MerchantGroupID, bool *MultiCurrency)
{
	//+ MERCHSELECT @mamata 5/05/2016
	TABLE_MERCHANT Merchant;
	TABLE_MERCH_RANGE MerchRange;
	TLV_TREE_NODE node;
	int RetFun;
	int LocalCurr=0;
	int DollarCurr=0;
	TABLE_HOST Host;
	uint8 EntryMode;
	bool HostValidation;
	TLV_TREE_NODE RangeCLNode = NULL;
	TLV_TREE_NODE node2 = NULL;
	TLV_TREE_NODE MerchantNode = NULL;
	uint16 TranID;

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TREE_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	// get first record of TAB_MERCH_RANGE
	RetFun = TM_FindFirst(TAB_MERCH_RANGE, &MerchRange);
	CHECK(RetFun == RET_OK, LBL_FILE_ERROR);

	do
	{
		// verify if current range id is in Tree_RangeCL
		node = TlvTree_Find(Tree_RangeCL, MerchRange.RangeID, 0);
		if(node != NULL)
		{
			// verify if MerchantGoupID is correct
			memset((char*)&Merchant, 0, L_TABLE_MERCHANT);
			RetFun = TM_FindRecord(TAB_MERCHANT, &Merchant, MerchRange.MerchantID);
			CHECK(RetFun == RET_OK, LBL_FILE_ERROR);

			if(Merchant.MerchantGroupID == MerchantGroupID)
			{
				// set host validation to true
				HostValidation = TRUE;
				// get entry mode
				node = TlvTree_Find(Tree_Tran, TAG_ENTRY_MODE, 0);
				CHECK(node != NULL, LBL_TREE_ERROR);
				EntryMode = *(uint8*)TlvTree_GetData(node);
				// verify if is a emv transaction
				if(EntryMode == ENTRY_MODE_CHIP || EntryMode == ENTRY_MODE_CLESS_CHIP)
				{
					// yes, get host data
					RetFun = TM_FindRecord(TAB_HOST, &Host, Merchant.HostID);
					CHECK(RetFun == TM_RET_OK, LBL_TREE_ERROR);
					// verify if host support emv transactions
//					if(!(Host.Flags1 & HOST_FLAG1_EMV))
//						HostValidation = FALSE;
				}

				// validate if transactions is enable to this merchant
				if(!Merchant_IsTransactionEnable(Merchant.Transactions1, Merchant.Transactions1, TranID))
				{
					HostValidation = FALSE;
				}

				if(HostValidation)
				{
					MerchantNode = TlvTree_FindChild(Tree_MerchantCL, MerchRange.MerchantID);
					if(MerchantNode == NULL)
					{

						MerchantNode = TlvTree_AddChild(Tree_MerchantCL, MerchRange.MerchantID, &Merchant.Name, strlen(Merchant.Name));
						CHECK(MerchantNode != NULL, LBL_TREE_ERROR);
						node = TlvTree_AddChild(MerchantNode, TAG_CURRENCY, &Merchant.Currency, 1);
						CHECK(node != NULL, LBL_TREE_ERROR);

						if(Merchant.Currency == 1)
							LocalCurr++;
						else
							DollarCurr++;
					}

					RangeCLNode = TlvTree_Find(Tree_RangeCL, MerchRange.RangeID, 0);
					CHECK(node != NULL, LBL_TREE_ERROR);
					node2 = TlvTree_AddChild(MerchantNode, TlvTree_GetTag(RangeCLNode), TlvTree_GetData(RangeCLNode), TlvTree_GetLength(RangeCLNode));
					CHECK(node2 != NULL, LBL_TREE_ERROR);
				}
			}
		}
		RetFun = TM_FindNext(TAB_MERCH_RANGE, &MerchRange);
	}while(RetFun == TM_RET_OK);
	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_FILE_ERROR);


	if(LocalCurr > 0 && DollarCurr > 0)
		*MultiCurrency = TRUE;
	else
		*MultiCurrency = FALSE;

	CHECK(LocalCurr > 0 || DollarCurr > 0, LBL_NO_CANDIDATES;)

	return RET_OK;

LBL_TREE_ERROR:
	return -3;

LBL_FILE_ERROR:
	return -4;

LBL_NO_CANDIDATES:
	return RET_NOK;



}


/* --------------------------------------------------------------------------
 * Function Name:	Counters_Get_Invoice
 * Description:		Get a invoice number
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get invoice number
 *  >0 - invoice number
 * Notes:
 */
uint32 Counters_Get_Invoice(void)
{
	TABLE_COUNTERS Counters;
	int RetFun;
	uint32 RetValue = -1;

	memset((char*)&Counters, 0, L_TABLE_COUNTERS);
	RetFun = TM_FindFirst(TAB_COUNTERS, &Counters);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	RetValue = Counters.Invoice;

	return RetValue;

LBL_ERROR:
	return -1;
}


/* --------------------------------------------------------------------------
 * Function Name:	Counters_Increment_Invoice
 * Description:		Increment invoice number
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get invoice number
 *  >0 - invoice number
 * Notes:
 */
uint32 Counters_Increment_Invoice(void)
{
	TABLE_COUNTERS Counters;
	int RetFun;
	uint32 RetValue = -1;

	memset((char*)&Counters, 0, L_TABLE_COUNTERS);
	RetFun = TM_FindFirst(TAB_COUNTERS, &Counters);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	if(Counters.Invoice == 999999)
		Counters.Invoice = 1;
	else
		Counters.Invoice++;

	RetFun = TM_ModifyRecord(TAB_COUNTERS, &Counters);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	RetValue = Counters.Invoice;

	return RetValue;

LBL_ERROR:
	return -1;
}


/* --------------------------------------------------------------------------
 * Function Name:	Counters_Get_STAN
 * Description:		Get a STAN and increment for nest time
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get STAN
 *  >0 - STAN
 * Notes:
 */
uint32 Counters_Get_STAN(void)
{
	TABLE_COUNTERS Counters;
	int RetFun;
	uint32 RetValue = -1;

	memset((char*)&Counters, 0, L_TABLE_COUNTERS);
	RetFun = TM_FindFirst(TAB_COUNTERS, &Counters);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	RetValue = Counters.STAN;

	if(Counters.STAN == 999999)
		Counters.STAN = 1;
	else
		Counters.STAN++;

	RetFun = TM_ModifyRecord(TAB_COUNTERS, &Counters);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return RetValue;

LBL_ERROR:
	return -1;
}

/* --------------------------------------------------------------------------
 * Function Name:	Counters_Get_Current_STAN
 * Description:		Get the current STAN(don't increases in memory)
 * Parameters:
 * 	- void
 * Return:
 *  -1 - Cannot get STAN
 *  >0 - STAN
 * Notes:
 */
uint32 Counters_Get_Current_STAN(void)
{
	TABLE_COUNTERS Counters;
	int RetFun;

	memset((char*)&Counters, 0, L_TABLE_COUNTERS);
	RetFun = TM_FindFirst(TAB_COUNTERS, &Counters);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return Counters.STAN;

LBL_ERROR:
	return -1;
}

/* --------------------------------------------------------------------------
 * Function Name:	IsTransactionEnable
 * Description:		Verify if a transaction in enable in almost one merchant
 * Parameters:
 * 	- uint16 TranID - transaction ID
 * Return:
 *  TRUE
 *  FALSE
 * Notes:
 */
bool IsTransactionEnable(uint16 TranID)
{
	TABLE_MERCH_GROUP MerchantGroup;
	int RetFun;

	memset((char*)&MerchantGroup, 0, L_TABLE_MERCH_GROUP);
	RetFun = TM_FindFirst(TAB_MERCH_GROUP, &MerchantGroup);
	CHECK(RetFun == TM_RET_OK, LBL_END);
	do
	{
		if(MerchGroup_IsTransactionEnable(MerchantGroup.Header.RecordID, TranID))
			return TRUE;
		RetFun = TM_FindNext(TAB_MERCH_GROUP, &MerchantGroup);
	}while(RetFun == TM_RET_OK);

LBL_END:
	return FALSE;
}

/* --------------------------------------------------------------------------
 * Function Name:	TableTerminal_GetDataKeyInfo
 * Description:		Get key info from TMS tables assigned to terminal
 * Author:			@jbotero
 * Parameters:		TABLE_KEY_INFO * stKeyInfo: Pointer to structure Key info
 * Return:
 * Notes:
 */

int TableTerminal_GetKeyInfo(e_KeyType KeyType, TABLE_KEY_INFO * stKeyInfo)
{
	char * TableName = "";
	char * KeyName = "";
	TABLE_KEY_INFO tKeyInfo;
	int RetFun;

	switch(KeyType)
	{
		case KEY_TYPE_PIN:
			TableName = TAB_PIN_KEY_INFO;
			KeyName = tTerminal.PinKeyName;
			break;

		case KEY_TYPE_DATA:
			TableName = TAB_DATA_KEY_INFO;
			KeyName = tTerminal.DataKeyName;
			break;

		default:
			break;
	}

	RetFun = TM_FindFirst(TableName, &tKeyInfo);
	CHECK(RetFun == TM_RET_OK, LBL_END);

	do
	{
		if( memcmp( tKeyInfo.Name, KeyName, strlen(KeyName) ) == 0)
		{
			memcpy(stKeyInfo, &tKeyInfo, L_TABLE_KEY_INFO);
			return RET_OK;
		}

		RetFun = TM_FindNext(TableName, &tKeyInfo);

	} while(RetFun == TM_RET_OK);

LBL_END:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Table_GetCurrencyInfo
 * Description:		Get currency info of selected merchant
 * Author:			@jbotero
 * Parameters:
 * - TLV_TREE_NODE Tree: Transaction tree
 * - CURRENCY_INFO * pStCurrencyInfo: Output structure with currency information
 *
 * Return:
 * - RET_OK: Processed OK
 * - RET_NOK: Error!!
 * Notes:
 */
int Table_GetCurrencyInfo(TLV_TREE_NODE Tree, CURRENCY_INFO * pStCurrencyInfo)
{
	uint8 Currency;
	int RetFun;

	CHECK(pStCurrencyInfo != NULL, LBL_ERROR);
	memset(pStCurrencyInfo, 0x00, sizeof(CURRENCY_INFO));

	RetFun = Tlv_GetTagValue(Tree, TAG_CURRENCY, &Currency, NULL);
	if(RetFun != RET_OK)
		Currency = CURRENCY_LOCAL;

	if( Currency == CURRENCY_LOCAL )
	{
		memcpy(pStCurrencyInfo->CodeASC, tTerminal.CurrCode_Local, 3);
		memcpy(pStCurrencyInfo->Symbol, tTerminal.CurrSymbol_Local, L_TERM_CURR_SYMBOL);
		pStCurrencyInfo->exp = 0;
	}
	else if(Currency == CURRENCY_DOLLAR)
	{
		memcpy(pStCurrencyInfo->CodeASC, "840", 3);
		memcpy(pStCurrencyInfo->Symbol, tTerminal.CurrSymbol_Dollar, L_TERM_CURR_SYMBOL);
		pStCurrencyInfo->exp = 2;
	}
	else
		return RET_NOK;

	pStCurrencyInfo->Type = Currency;
	GTL_Convert_UlToDcbNumber(atoi(pStCurrencyInfo->CodeASC), pStCurrencyInfo->CodeBCD, 2);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


int ADD_TRANSACCION_MESERO(TLV_TREE_NODE tree)
{

	TLV_TREE_NODE node;
	//char TempBuff[1000];
	TABLE_MESERO tMeseroAux;
	uint32 ServerNumber;
	int iRet,RetFun;

	node = TlvTree_Find(tree, TAG_SERVER_NUMBER, 0);
	//memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
	ServerNumber = *(uint32*)TlvTree_GetData(node);

			RetFun = TM_FindFirst(TAB_MESERO,&tMeseroAux);
			if(RetFun == TM_RET_OK)
			{
				do
				{
					// verify if is correct invoice
					if(tMeseroAux.IdMesero == ServerNumber)
					{
						// yes, fill tree and exit
						return RET_OK;
					}
					// no, find next transaction
					RetFun = TM_FindNext(TAB_MESERO, &tMeseroAux);
				}while(RetFun == TM_RET_OK);
				//+ NORECORDS @mamata 6/05/2016
				CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
			}

		LBL_ERROR:

		memset(&tMeseroAux, 0x00, sizeof(tMeseroAux));
		tMeseroAux.IdMesero = ServerNumber;


		iRet = TM_AddRecord(TAB_MESERO, &tMeseroAux);
		if (iRet != TM_RET_OK)
			return RET_NOK;


	return RET_OK;
}
