/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Reports.c
 * Header file:		Reports.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	Report_TransactionList
 * Description:		Print Report of transaction list for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - table of merchant
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_TransactionList(TABLE_MERCHANT *Merchant)
{
	BATCH BatchRecord;
	int RetFun;
	bool First = TRUE;
	int Reporte = 2;

	RetFun = TM_FindFirst(Merchant->BatchName, &BatchRecord);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR_FIND_FIRST);

	UI_OpenPrinter_Ext();
	Print_ReportHeader(Merchant, Reporte);

	do
	{
		//+ REVERSE01 @mamata Dec 16, 2014
		if(!(BatchRecord.TranStatus & FSTATUS_NEED_REVERSE))
		//- REVERSE01
			Print_TransactionListItem(&BatchRecord, First);

		RetFun = TM_FindNext(Merchant->BatchName, &BatchRecord);
		First = FALSE;
	}while(RetFun == TM_RET_OK);
	UI_ClosePrinter();

	Report_MerchantTotals(Merchant, FALSE, Reporte);

	return RET_OK;

LBL_ERROR_FIND_FIRST:
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		return RET_OK;
	else
		return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Report_TransactionListServer
 * Description:		Print Report of transaction list for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - table of merchant
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_TransactionListServer(TABLE_MERCHANT *Merchant, int32 IdMesero)
{
	TABLE_MESERO MeseroRecord;
	int RetFun;
	int Reporte;
	BATCH BatchRecord;
	char Mesero[50];
	bool Detalle= FALSE;

	UI_OpenPrinter_Ext();

	if (IdMesero!= 0){
		 Reporte= 5;
		 Detalle= TRUE;
		 Print_ReportHeader(Merchant, Reporte);
		 Print_TransactionTotalServer(Merchant, IdMesero, Detalle);

	}
	else
	{
		Reporte= 4;

	RetFun = TM_FindFirst(TAB_MESERO, &MeseroRecord);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR_FIND_FIRST);
	Print_ReportHeader(Merchant, Reporte);
	do
	{
		UI_PrintLine_Ext("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
		memset(Mesero, 0, 50);
		Telium_Sprintf(Mesero,"Mesero: %d",MeseroRecord.IdMesero);
		UI_PrintLine_Ext(Mesero, UI_FONT_NORMAL,UI_ALIGN_LEFT);
		UI_PrintLineFeed_Ext(1);
		UI_Print2Fields_Ext("REF.  CONSUMO "," " , UI_FONT_DOUBLE, "PROPINA ", " ", UI_FONT_DOUBLE);

					RetFun = TM_FindFirst(Merchant->BatchName, &BatchRecord);
					CHECK(RetFun == TM_RET_OK, LBL_ERROR_FIND_FIRST);
					do
					{
						if(BatchRecord.Server == MeseroRecord.IdMesero)
						{
							//+ REVERSE01 @mamata Dec 16, 2014
							if((!(BatchRecord.TranStatus & FSTATUS_NEED_REVERSE)) &&
									(!(BatchRecord.TranStatus & FSTATUS_VOIDED)) &&
									((BatchRecord.TranID != TRAN_VOID)))

							{
								Print_TransactionListItemServer(&BatchRecord);
							}
						}
						RetFun = TM_FindNext(Merchant->BatchName, &BatchRecord);
					}while(RetFun == TM_RET_OK);
						Print_TransactionTotalServer(Merchant, MeseroRecord.IdMesero, Detalle);
		RetFun = TM_FindNext(TAB_MESERO, &MeseroRecord);
	}while(RetFun == TM_RET_OK);

	}//End else
		UI_PrintLineFeed_Ext(6);
		UI_ClosePrinter();

	return RET_OK;

LBL_ERROR_FIND_FIRST:
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		return RET_OK;
	else
		return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Report_MerchantTotals
 * Description:		Print Totals report for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - table of merchant
 *  - bool Header - flag to print or not report header
 *  - bool Settle - flag to enable print settle data
 * Return:
 * Notes:
 */
int Report_MerchantTotals(TABLE_MERCHANT *Merchant, bool Header, int Reporte)
{
	TABLE_MERCH_RANGE MerchantRange;
	int RetFun;
	bool EntryMode;



	UI_OpenPrinter_Ext();
	if(Header)
		Print_ReportHeader(Merchant, Reporte);


	if(Reporte==1)
		Print_SettleInfo(Merchant);

	UI_PrintLine("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_PrintLine(mess_getReturnedMessage(MSG_CARD_TOTALS), UI_FONT_NORMAL, UI_ALIGN_CENTER);
	RetFun = TM_FindFirst(TAB_MERCH_RANGE, &MerchantRange);
	CHECK(RetFun == TM_RET_OK, LBL_TOTALS);
	do
	{
		if(MerchantRange.MerchantID == Merchant->Header.RecordID)
		{
			EntryMode=0; //CT TRANSACCION POR CONTACTO
			Print_RangeTotals(Merchant, MerchantRange.RangeID, EntryMode);

			EntryMode=1; //RF TRANSACCION POR CONTATLESS O RADIO FRECUENCIA
			Print_RangeTotals(Merchant, MerchantRange.RangeID, EntryMode);
		}
		RetFun = TM_FindNext(TAB_MERCH_RANGE, &MerchantRange);
	}while(RetFun == TM_RET_OK);

LBL_TOTALS:
	UI_PrintLine("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_PrintLine(mess_getReturnedMessage(MSG_GENERAL_TOTALS), UI_FONT_NORMAL, UI_ALIGN_CENTER);
	Print_MerchantTotals(Merchant);
	UI_PrintLine("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
	int Print_Footer();
	UI_PrintLineFeed(6);
	UI_ClosePrinter();

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Reports_Totals
 * Description:		Print totals reports
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Reports_Totals(int32 MerchantID, int32 MerchantGroupID)
{
	TABLE_MERCHANT Merchant;
	TABLE_HEADER TableHeader;
	int RetFun;
	bool ReportPrinted = FALSE;
	bool PrintReport;
	int Reporte=3;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_TOTALS_REPORT), UI_ALIGN_CENTER);

	if(MerchantID != 99999)
	{
		// find merchant info
		RetFun = TM_FindRecord(TAB_MERCHANT, &Merchant, MerchantID);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// get batch header
		RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// validate if batch if not empty
		CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);

		// validate if batch is not already settled
		CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);

		UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
		UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));

		Report_MerchantTotals(&Merchant, TRUE, Reporte);

		ReportPrinted = TRUE;
	}
	else
	{
		// get first merchant
		RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		do
		{
			PrintReport = TRUE;

			if(MerchantGroupID != 99999)
				if(Merchant.MerchantGroupID != MerchantGroupID)
					PrintReport = FALSE;

			if(PrintReport)
			{
				// get batch header
				RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR);

				// validate if batch if not empty
				if(TableHeader.NumberRecords > 0 && !Merchant.BatchSettle)
				{
					UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
					UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
					UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));

					Report_MerchantTotals(&Merchant, TRUE, Reporte);

					ReportPrinted = TRUE;
				}
			}
			// get next merchant
			RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
		}while(RetFun == TM_RET_OK);
	}

	CHECK(ReportPrinted, LBL_BATCH_EMPTY);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_TOTALS_REPORT), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Reports_Detail
 * Description:		Print detail reports
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Reports_Detail(int32 MerchantID, int32 MerchantGroupID)
{
	TABLE_MERCHANT Merchant;
	TABLE_HEADER TableHeader;
	int RetFun;
	bool ReportPrinted = FALSE;
	bool PrintReport;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_DETAIL_REPORT), UI_ALIGN_CENTER);

	if(MerchantID != 99999)
	{
		// find merchant info
		RetFun = TM_FindRecord(TAB_MERCHANT, &Merchant, MerchantID);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// get batch header
		RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// validate if batch if not empty
		CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);

		// validate if batch is not already settled
		CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);

		UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
		UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));

		Report_TransactionList(&Merchant);

		ReportPrinted = TRUE;
	}
	else
	{
		// get first merchant
		RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		do
		{
			PrintReport = TRUE;

			if(MerchantGroupID != 99999)
				if(Merchant.MerchantGroupID != MerchantGroupID)
					PrintReport = FALSE;

			if(PrintReport)
			{
				// get batch header
				RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR);

				// validate if batch if not empty
				if(TableHeader.NumberRecords > 0 && !Merchant.BatchSettle)
				{
					UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
					UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
					UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));

					Report_TransactionList(&Merchant);

					ReportPrinted = TRUE;
				}
			}
			// get next merchant
			RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
		}while(RetFun == TM_RET_OK);
	}

	CHECK(ReportPrinted, LBL_BATCH_EMPTY);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_DETAIL_REPORT), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Reports_Detail_Server
 * Description:		Print detail reports server
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Reports_Detail_Server(int32 MerchantID, int32 MerchantGroupID, int32 Mesero)
{
	TABLE_MERCHANT Merchant;
	TABLE_HEADER TableHeader;
	int RetFun;
	bool ReportPrinted = FALSE;
	bool PrintReport;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_TOTALS_SERVER), UI_ALIGN_CENTER);

	if(MerchantID != 99999)
	{
		// find merchant info
		RetFun = TM_FindRecord(TAB_MERCHANT, &Merchant, MerchantID);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// get batch header
		RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// validate if batch if not empty
		CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);

		// validate if batch is not already settled
		CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);

		UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
		UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));

		Report_TransactionListServer(&Merchant, Mesero);

		ReportPrinted = TRUE;
	}
	else
	{
		// get first merchant
		RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		do
		{
			PrintReport = TRUE;

			if(MerchantGroupID != 99999)
				if(Merchant.MerchantGroupID != MerchantGroupID)
					PrintReport = FALSE;

			if(PrintReport)
			{
				// get batch header
				RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR);

				// validate if batch if not empty
				if(TableHeader.NumberRecords > 0 && !Merchant.BatchSettle)
				{
					UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
					UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
					UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));

					Report_TransactionListServer(&Merchant, Mesero);

					ReportPrinted = TRUE;
				}
			}
			// get next merchant
			RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
		}while(RetFun == TM_RET_OK);
	}

	CHECK(ReportPrinted, LBL_BATCH_EMPTY);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_TOTALS_SERVER), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Report_ReprintSettle
 * Description:		Reprint Settle
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_ReprintSettle(void)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
	bool ReportPrinted = FALSE;
	int Reporte=1;
	// find first merchant
	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	do
	{
		// if merchant was settle?
		if(Merchant.BatchSettle)
		{

			// yes
			UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));
			Report_MerchantTotals(&Merchant, TRUE,Reporte);
			ReportPrinted = TRUE;
		}

		// find next merchant
		RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
	}while(RetFun == TM_RET_OK);

	if(!ReportPrinted)
		UI_ShowMessage(mess_getReturnedMessage(MSG_REPRINT), "NO ES POSIBLE REIMPRIMIR CIERRE", NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
/* --------------------------------------------------------------------------
 * Function Name:	Report_ReprintSettle
 * Description:		Reprint Settle
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Report_PrintBines(void)
{
	TABLE_RANGE Range;
	int RetFun;
	char trj[50];
	char Min[8];
	char Max[8];
	// find first merchant
	RetFun = TM_FindFirst(TAB_RANGE, &Range);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);
	UI_OpenPrinter_Ext();


	UI_PrintLine("REPORTE DE BINES", UI_FONT_NORMAL,UI_ALIGN_CENTER);
	UI_PrintLineFeed(1);
	UI_PrintLine("  ID  C/D   MIN     MAX     NOMBRE", UI_FONT_NORMAL,UI_ALIGN_LEFT);
	do
	{
			// yes
			memset(trj, 0, 50);
			RetFun= memcmp(Range.Name, "C2P", 0);

			CHECK(RetFun==0, lblC2P);
//			memset(id, 0, 2);P
//			memset(binMin, 0, 6);
//			memset(binMax, 0, 6);

			UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));
//			memcpy((char*)&id,&Range.Header.RecordID, LTAG_UINT16);
			Uint64_to_Ascii(Range.Bin_Min ,Min, _FORMAT_TRIM,8,0x30);
			Uint64_to_Ascii(Range.Bin_Max,Max, _FORMAT_TRIM,8,0x30);

			memcpy((char *)&trj,"  ", 2);
			Telium_Sprintf((char *)&trj[2],"%d",Range.Header.RecordID);
			memcpy((char *)&trj[3],"   ", 3);
			if(Range.Flags1 & RANGE_FLAG1_DEBIT){
				memcpy((char *)&trj[6],"D",1);
			}else{
				memcpy((char *)&trj[6],"C",1);
			}
			memcpy((char *)&trj[7],"   ", 3);
			memcpy((char *)&trj[10],Min, 8);
			memcpy((char *)&trj[18],"   ", 3);
			memcpy((char *)&trj[21],Max, 8);
			memcpy((char *)&trj[29],"   ", 3);
			memcpy((char *)&trj[32],Range.Name, sizeof(Range.Name));


//			Telium_Sprintf(trj,"%d  %s  %s  %s",Range.Header.RecordID,Min,Max,Range.Name );
			UI_PrintLine(trj, UI_FONT_NORMAL,UI_ALIGN_LEFT);
		// find next merchant
		lblC2P:
		RetFun = TM_FindNext(TAB_RANGE, &Range);
	}while(RetFun == TM_RET_OK);
	UI_PrintLineFeed(6);
	UI_ClosePrinter_Ext();
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
