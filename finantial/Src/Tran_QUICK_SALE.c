/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Tran_QUICK_SALE.c
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
//+ ADD0039 @mamata 10/09/2014
uint16 TRAN_STEPS_QUICK_SALE[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP,
	STEP_SELECT_MERCHANT_BY_TRAN,
	STEP_QUICK_SALE_AMOUNT,
	STEP_TAX_AMOUNT,
	STEP_TIP_AMOUNT,
	STEP_CONFIRM_TOTAL,

	STEP_START_PAYMENT, 		//EWL, set entry supported entry modes
	STEP_EWL_START,				//EWL, Initialize handle, set parameters EMV/CLESS
	STEP_WAIT_CARD,				//EWL, wait card and read initial data
	STEP_MANUAL_CARD_ENTRY,
	STEP_MANUAL_EXP_DATE,

	STEP_CLESS_VALIDATE_CARD,
	STEP_QUICK_SALE_CHECK_SERVICE_CODE,

	STEP_CHECK_EXP_DATE,
	STEP_EWL_GO_ONCHIP,			//EWL, First generate
	STEP_APPROVED_OFF,			//EWL, approved off-line
	STEP_DENIAL_OFF,			//EWL, approved online

	STEP_PREDIAL,
	STEP_PIN,
	STEP_SEND_RECEIVE,
	STEP_END_COMMUNICATION,
	STEP_HOST_ANALYSE,			//EWL, Check host response
	STEP_SECOND_TAP,			//EWL, second tap only CLESS
	STEP_FINISH,				//EWL, Execute second Generate
	STEP_PROCESS_RESPONSE,
	STEP_PRINT_RECEIPT,
	STEP_END_LIST
};
//- ADD0039



