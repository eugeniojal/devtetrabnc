/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Tran_ADJUST.c
 * Header file:		Tran_ADJUST.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
uint16 TRAN_STEPS_ADJUST[] =
{
	STEP_CHECK_TERMINAL,
	//STEP_SELECT_MERCH_GROUP,
	//STEP_SELECT_MERCHANT_BY_TRAN,
	STEP_INVOICE_ADJUST,
	STEP_ADJUST_VALIDATE,
	//STEP_MERCHANT_PASSWORD,
	STEP_ADJUST_BASE_AMOUNT,
	STEP_ADJUST_TIP_AMOUNT,
	STEP_ADJUST_TAX_AMOUNT,
	STEP_ADJUST_CONFIRM,
	STEP_END_LIST
};


