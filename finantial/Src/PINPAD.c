/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPP-App
 * Header file:		PINPAD.h
 * Description:		Implement CarCAAn Pinpad service calls
 * Author:			mamata
 * --------------------------------------------------------------------------*/

//+ CARCAAN_PINPAD @mamata Jan 8, 2015
/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


 /* ***************************************************************************
 * VARIABLES
 * ***************************************************************************/
uint8 PINPAD_Command;
TLV_TREE_NODE Tree_PINPAD;

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int PP_Process_COMMAND_DISPLAY(TLV_TREE_NODE Tree_Input);
int PP_Process_COMMAND_TRN1(TLV_TREE_NODE Tree_Input);
//+ PPCRYPTO @mamata Jun 30, 2015
int PP_Process_COMMAND_ENCRYPT_DATA(TLV_TREE_NODE Tree_Input);
//- PPCRYPTO
//+ PPGETCARD @mamata Jul 1, 2015
int PP_Process_COMMAND_CARD(TLV_TREE_NODE Tree_Input);
//- PPGETCARD

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	PP_ManagerServices
 * Description:		process service calls from PINPAD
 * Author:			mamata
 * Parameters:
 *  - unsigned int nSize - size of service call data
 *  - byte *pData - pointer to service call data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_ManagerServices(unsigned int nSize, byte *pData)
{
	//int nRetVal;
	TLV_TREE_NODE TLVT_Input;
	TLV_TREE_NODE TLVT_Node;
	int ret;

	TLVT_Input = NULL;

	 // Verify if data present
	CHECK( nSize > 0, lbl_STOP );

	// reserve memory for TLVT_Input
	TLVT_Input = TlvTree_New( 0 );
	CHECK( TLVT_Input != NULL, lbl_STOP );

	// build TLVT_Input
	ret = TlvTree_Unserialize( &TLVT_Input, TLV_TREE_SERIALIZER_DEFAULT, pData, nSize );
	CHECK( ret == TLV_TREE_OK, lbl_STOP1)

	// find PP_TAG_COMMAND
	TLVT_Node = TlvTree_Find( TLVT_Input, PP_TAG_COMMAND, 0 );
	CHECK( TLVT_Node != NULL, lbl_STOP1 );
	PINPAD_Command = *(uint8*)TlvTree_GetData(TLVT_Node);

	switch( PINPAD_Command )
	{
		case PP_COMMAND_DISPLAY:
			PP_Process_COMMAND_DISPLAY(TLVT_Input);
			break;

		case PP_COMMAND_TRANSACTION:
			PP_Process_COMMAND_TRN1(TLVT_Input);
			break;

		//+ PPCRYPTO @mamata Jun 30, 2015
		case PP_COMMAND_ENCRYPT_DATA:
			PP_Process_COMMAND_ENCRYPT_DATA(TLVT_Input);
			break;
		//- PPCRYPTO

		//+ PPGETCARD @mamata Jul 1, 2015
		case PP_COMMAND_CARD:
			PP_Process_COMMAND_CARD(TLVT_Input);
			break;
		//- PPGETCARD
	}

	// clear service call buffer
	memset( pData, 0, nSize );

	ret = TlvTree_Serialize( Tree_Tran, TLV_TREE_SERIALIZER_DEFAULT, pData, nSize );
	CHECK( ret > 0, lbl_STOP2 );

	// release tlvtrees memory
	TlvTree_Release( TLVT_Input );

	// Everything its OK
	return FCT_OK;

lbl_STOP2:
lbl_STOP1:
	TlvTree_Release( TLVT_Input );

lbl_STOP:
	return STOP;
}



/* --------------------------------------------------------------------------
 * Function Name:	PP_Process_COMMAND_DISPLAY
 * Description:		Process PINPAD Display command
 * Autor:			mamata
 * Parameters:
 *  - TLV_TREE_NODE Tree_Input
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_Process_COMMAND_DISPLAY(TLV_TREE_NODE Tree_Input)
{
	TLV_TREE_NODE node;
	//+ PPGETCARD @mamata Jul 1, 2015
	uint16 DispControl = 0;
	//d uint8 DispControl = 0;
	//- PPGETCARD

	DisplayHeader(_OFF_);
	DisplayFooter(_OFF_);

	node = TlvTree_Find(Tree_Input, PP_TAG_CONTROL1, 0);
	CHECK(node != NULL, LBL_ERROR);
	//+ PPGETCARD @mamata Jul 1, 2015
	DispControl = *(uint16*) TlvTree_GetData(node);
	//d DispControl = *(uint8*)TlvTree_GetData(node);
	//- PPGETCARD

	//+ PPGETCARD @mamata Jul 6, 2015
	if(DispControl & PP_FLAG_IDLE)
	//d if(DispControl & PP_DIPS_IDLE)
	//- PPGETCARD
	{
		if(IsTouchScreen())
			UI_ShowIdleImage("file://flash/HOST/LOGO280.JPG");
		else if(IsColorDisplay())
			UI_ShowIdleImage("file://flash/HOST/LOGO250.JPG");
		else
			UI_ShowIdleImage("file://flash/HOST/LOGO220.BMP");

		return RET_OK;
	}

	//+ PPGETCARD @mamata Jul 6, 2015
	if(DispControl & PP_FLAG_CLEAR_SCREEEN)
	//d if(DispControl & PP_DIPS_CLEAR_SCREEN)
	//- PPGETCARD
		UI_ClearAllLines();

	UI_SetLine(UI_TITLE, "CARCAAN PINPAD", UI_ALIGN_CENTER);

	node = TlvTree_Find(Tree_Input, PP_TAG_LINE1, 0);
	if(node != NULL )
		UI_SetLine(UI_LINE1, TlvTree_GetData(node), UI_ALIGN_CENTER);

	node = TlvTree_Find(Tree_Input, PP_TAG_LINE2, 0);
	if(node != NULL )
		UI_SetLine(UI_LINE2, TlvTree_GetData(node), UI_ALIGN_CENTER);

	node = TlvTree_Find(Tree_Input, PP_TAG_LINE3, 0);
	if(node != NULL )
		UI_SetLine(UI_LINE3, TlvTree_GetData(node), UI_ALIGN_CENTER);

	node = TlvTree_Find(Tree_Input, PP_TAG_LINE4, 0);
	if(node != NULL )
		UI_SetLine(UI_LINE4, TlvTree_GetData(node), UI_ALIGN_CENTER);

	UI_ShowInfoScreen(" ");

	//+ PPGETCARD @mamata Jul 6, 2015
	if(DispControl & PP_FLAG_BEEP_ERROR)
	//d if(DispControl & PP_DIPS_BEEP_ERROR)
	//- PPGETCARD
		buzzer(10);

	//+ PPGETCARD @mamata Jul 6, 2015
	if(DispControl & PP_FLAG_BEEP_INFORMATION)
	//d if(DispControl & PP_DIPS_BEEP_INFORMATION)
	//- PPGETCARD
		buzzer(5);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	PP_Process_COMMAND_TRN1
 * Description:		Process PINPAD TRN1 command
 * Autor:			mamata
 * Parameters:
 *  - TLV_TREE_NODE Tree_Input
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_Process_COMMAND_TRN1(TLV_TREE_NODE Tree_Input)
{
	TLV_TREE_NODE node;
	TLV_TREE_NODE node2;
	int RetFun;
	uint8 CommandResult = 0;

	if(Tree_PINPAD != NULL)
		TlvTree_Release(Tree_PINPAD);

	Tree_PINPAD = TlvTree_New(0);
	CHECK(Tree_PINPAD != NULL, LBL_ERROR);

	node = TlvTree_GetFirstChild(Tree_Input);
	CHECK(node != NULL, LBL_ERROR);

	do
	{
		node2 = TlvTree_AddChild(Tree_PINPAD, TlvTree_GetTag(node), TlvTree_GetData(node), TlvTree_GetLength(node));

		node = TlvTree_GetNext(node);

	}while(node != NULL);

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	RetFun = RunTransaction(TRAN_PINPAD, NULL, NULL, FALSE);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	node = TlvTree_AddChild(Tree_Tran, PP_TAG_RESULT, &CommandResult, LTAG_UINT8);

	return RET_OK;

LBL_ERROR:
	CommandResult = -1;
	node = TlvTree_AddChild(Tree_Tran, PP_TAG_RESULT, &CommandResult, LTAG_UINT8);
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	iPOSApp_ServiceCall
 * Description:		perform service call to iPOSApp
 * Author:			mamata
 * Parameters:
 *  - TLV_TREE_NODE Tree_Output - output data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_ServiceCall(TLV_TREE_NODE Tree_Output, TLV_TREE_NODE *Tree_Input)
{
	unsigned char ucPriority;
	int nResult;
	byte *tmpBuffer;
	int tmpBuffer_size = 0;
	int STLVT_size = 0;

	// verify is service is present
	nResult = Telium_ServiceGet(iPPApp_AppType, iPPApp_Services, &ucPriority);
	CHECK( nResult == 0, lbl_STOP1 );

	// Serialize pTLVT_Input
	STLVT_size = TlvTree_GetSerializationSize(Tree_Output, TLV_TREE_SERIALIZER_DEFAULT);
	CHECK( STLVT_size > 0, lbl_STOP1 );

	// crear memoria para serializar pTLVT_Output
	if( STLVT_size > 500 )
		tmpBuffer_size = STLVT_size;
	else
		tmpBuffer_size = 500;
	tmpBuffer = (byte*) umalloc( tmpBuffer_size );
	CHECK( tmpBuffer != NULL, lbl_STOP1 );

	// Serializar pTLVT_Output
	nResult = TlvTree_Serialize(Tree_Output, TLV_TREE_SERIALIZER_DEFAULT, tmpBuffer, tmpBuffer_size);
	CHECK( nResult > 0, lbl_STOP2 );

	// hacer el service call
	nResult = USR_ServiceCall(iPPApp_AppType, iPPApp_Services, tmpBuffer_size, tmpBuffer, &nResult);
	CHECK( nResult == 0, lbl_STOP2 );

	// get data from iposapp
	nResult = TlvTree_Unserialize(Tree_Input, TLV_TREE_SERIALIZER_DEFAULT, tmpBuffer, tmpBuffer_size);

	ufree(tmpBuffer);
	return 0;

lbl_STOP2:
	// No se pudo serializar pTLVT_Output
	// Error al llamar el codigo de servicio
	ufree( tmpBuffer );

lbl_STOP1:
	// No esta presente el servicio del EMVCUSTOM
	// Error al calcular el largo del serializado de pTLVT_Output
	// Se se pudo crear memoria para el buffer tmpBuffer
	return -1;

}


/* --------------------------------------------------------------------------
 * Function Name:	PP_SendTranToHost
 * Description:		send transaction to host via PINPAD
 * Autor:			mamata
 * Parameters:
 *  - node
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_SendTranToHost(void)
{
	//+ TREEMEM @mamata 5/05/2016
	//d TLV_TREE_NODE iPPApp_Tran;
	TLV_TREE_NODE iPPApp_Tran = NULL;
	//- TREEMEM
	TLV_TREE_NODE node;
	TLV_TREE_NODE node2;
	TLV_TREE_NODE PP_HostTags;
	TLV_TREE_NODE HostTags;
	uint8 PP_Command = PP_COMMAND_HOST_COMM;
	int RetFun;
	uint8 Data = 1;
	uint8 HostResponseCode[2];

	// add PP_TAG_COMMAND
	node = TlvTree_Find(Tree_Tran, PP_TAG_COMMAND, 0);
	if(node != NULL)
		TlvTree_Release(node);
	node = TlvTree_AddChild(Tree_Tran, PP_TAG_COMMAND, &PP_Command,LTAG_UINT8);
	CHECK(node != NULL, LBL_ERROR);

	// create memory for host tags from PINPAD
	iPPApp_Tran = TlvTree_New(0);
	CHECK(iPPApp_Tran != NULL, LBL_ERROR);

	// call iPPApp
	RetFun = PP_ServiceCall(Tree_Tran, &iPPApp_Tran);

	// add host tags to Tree_Trann
	node = TlvTree_Find(iPPApp_Tran, TAG_HOST_RESPONSE_CODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	node = TlvTree_AddChild(Tree_Tran, TAG_HOST_RESPONSE_CODE, TlvTree_GetData(node), TlvTree_GetLength(node));
	CHECK(node != NULL, LBL_ERROR);

	PP_HostTags = TlvTree_Find(iPPApp_Tran, TAG_EMV_HOST_TAGS, 0);
	CHECK(PP_HostTags != NULL, LBL_FINISH_OK);
	HostTags = TlvTree_AddChild(Tree_Tran, TAG_EMV_HOST_TAGS, &Data, LTAG_UINT8);

	node = TlvTree_GetFirstChild(PP_HostTags);
	CHECK(node != NULL, LBL_ERROR);
	do
	{
		node2 = TlvTree_AddChild(Tree_PINPAD, TlvTree_GetTag(node), TlvTree_GetData(node), TlvTree_GetLength(node));
		node = TlvTree_GetNext(node);

	}while(node != NULL);

LBL_FINISH_OK:

	//+ TREEMEM @mamata 5/05/2016
	TlvTree_Release(iPPApp_Tran);
	//- TREEMEM

	return RET_OK;

LBL_ERROR:

	//+ TREEMEM @mamata 5/05/2016
	if(iPPApp_Tran != NULL)
	{
		TlvTree_Release(iPPApp_Tran);
	}
	//- TREEMEM

	memcpy(HostResponseCode, "CE", 2);
	TlvTree_AddChild(Tree_Tran, TAG_HOST_RESPONSE_CODE, &HostResponseCode, 2);
	return RET_NOK;
}


//+ PPCRYPTO @mamata Jun 30, 2015
/* --------------------------------------------------------------------------
 * Function Name:	PP_Process_COMMAND_ENCRYPT_DATA
 * Description:		Process to encrypt data
 * Author:			mamata
 * Parameters:
 *  - TLV_TREE_NODE Tree_Input
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_Process_COMMAND_ENCRYPT_DATA(TLV_TREE_NODE Tree_Input) {
	TLV_TREE_NODE node;
	CRYPTOLIB_DATA_PARAMETERS CryptoLib_Data_Parameters;
	CRYPTOLIB_DATA_GENERAL_PARAMS GeneralDataParams;
	TABLE_KEY_INFO stDataKeyInfo;
	uint8 Output_Buffer[2048];
	int RetFun;

	RetFun = TableTerminal_GetKeyInfo(KEY_TYPE_DATA, &stDataKeyInfo);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// clear variables
	memset(&CryptoLib_Data_Parameters, 0, sizeof(CryptoLib_Data_Parameters));
	memset( &GeneralDataParams, 0x00, sizeof(GeneralDataParams));
	memset(Output_Buffer, 0, sizeof(Output_Buffer));

	CryptoLib_Data_Parameters.DATAGeneralParams = &GeneralDataParams;

	// clear output tag
	node = TlvTree_Find(Tree_Tran, PP_TAG_CRYPTO_OUTPUT_DATA, 0);
	if(node != NULL)
		TlvTree_Release(node);

	// set parameters for data encryption
	// set input buffer
	node = TlvTree_Find(Tree_Input, PP_TAG_CRYPTO_CLEAR_DATA, 0);
	CHECK(node != NULL, LBL_MISSING_DATA);
	GeneralDataParams.pInputData = TlvTree_GetData(node);

	// set length of clear buffer
	GeneralDataParams.LenInputData = TlvTree_GetLength(node);

	// set cipher mode
	GeneralDataParams.iOper = C_SEC_CIPHER_FUNC;

	// set output buffer
	GeneralDataParams.pOutputData = Output_Buffer;

	CryptoLib_Data_Parameters.stKeyInfo.cAlgoType = CryptoLib_Data_Parameters.stMasterKeyInfo.cAlgoType= stDataKeyInfo.AlgoType;
	CryptoLib_Data_Parameters.stKeyInfo.iSecretArea = CryptoLib_Data_Parameters.stMasterKeyInfo.iSecretArea= stDataKeyInfo.SecretArea;
	CryptoLib_Data_Parameters.stKeyInfo.uiBankId = CryptoLib_Data_Parameters.stMasterKeyInfo.uiBankId = stDataKeyInfo.BankId;

	node = TlvTree_Find(Tree_Input, PP_TAG_CRYPTO_WK_DATA, 0);
	CHECK(node != NULL, LBL_MISSING_DATA);
	memcpy(GeneralDataParams.EncryptedWorkingKey, TlvTree_GetData(node), TlvTree_GetLength(node));

	RetFun = CryptoLib_DataEncryption(&CryptoLib_Data_Parameters);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	node = TlvTree_AddChild(Tree_Tran, PP_TAG_CRYPTO_OUTPUT_DATA, &Output_Buffer, GeneralDataParams.LenInputData);

	return RET_OK;

LBL_ERROR:
LBL_MISSING_DATA:
	return RET_NOK;
}
//- PPCRYPTO
//- CARCAAN_PINPAD

//+ PPGETCARD @mamata Jul 1, 2015
/* --------------------------------------------------------------------------
 * Function Name:	PP_Process_COMMAND_CARD
 * Description:		Process PINPAD TRN1 command
 * Author:			mamata
 * Parameters:
 *  - TLV_TREE_NODE Tree_Input
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int PP_Process_COMMAND_CARD(TLV_TREE_NODE Tree_Input) {
	TLV_TREE_NODE node;
	TLV_TREE_NODE node2;
	int RetFun;
	uint8 CommandResult = 0;

	if (Tree_PINPAD != NULL)
		TlvTree_Release(Tree_PINPAD);

	Tree_PINPAD = TlvTree_New(0);
	CHECK(Tree_PINPAD != NULL, LBL_ERROR);

	node = TlvTree_GetFirstChild(Tree_Input);
	CHECK(node != NULL, LBL_ERROR);

	do {
		node2 = TlvTree_AddChild(Tree_PINPAD, TlvTree_GetTag(node),
				TlvTree_GetData(node), TlvTree_GetLength(node));

		node = TlvTree_GetNext(node);

	} while (node != NULL);

	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	RetFun = RunTransaction(TRAN_PINPAD_CARD, NULL, NULL, FALSE);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	node = TlvTree_AddChild(Tree_Tran, PP_TAG_RESULT, &CommandResult,
			LTAG_UINT8);

	return RET_OK;

	LBL_ERROR: CommandResult = -1;
	node = TlvTree_AddChild(Tree_Tran, PP_TAG_RESULT, &CommandResult,
			LTAG_UINT8);
	return RET_NOK;
}
//- PPGETCARD
