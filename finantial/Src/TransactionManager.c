/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			TransactionManager.c
 * Header file:		TransactionManager.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * TRANSACTIONS - STEP LIST
 * ***************************************************************************/
extern uint16 TRAN_STEPS_SALE[];
extern uint16 TRAN_STEPS_REFUND[];
extern uint16 TRAN_STEPS_VOID[];
extern uint16 TRAN_STEPS_ADJUST[];

//+ ECHOTEST @mamata Aug 15, 2014
extern uint16 TRAN_STEPS_TEST[];
extern uint16 TRAN_STEPS_TEST_AUTO[];
extern uint16 TRAN_STEPS_LOGON[];
//- ECHOTEST
extern uint16 STEPS_DUPLICATE[];
extern uint16 STEPS_LAST_ANSWER[];
//+ CLESS @mamata Dec 11, 2014
extern uint16 TRAN_STEPS_QUICK_SALE[];
//- CLESS

//+ CARCAAN_PINPAD @mamata Jan 14, 2015
extern uint16 TRAN_STEPS_PINPAD_TRAN[];
//- CARCAAN_PINPAD

//+ PPGETCARD @mamata Jul 1, 2015
extern uint16 TRAN_STEPS_PINPAD_CARD[];
//- PPGETCARD

extern uint16 TRAN_STEPS_C2P[];

/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
TRAN_INFO TRANSACTIONS[] =
{
	{ 	TRAN_SALE,
		MSG_TRAN_SALE,
		FTRAN_REVERSE | FTRAN_BATCH | FTRAN_VOID | FTRAN_ADJUST | FTRAN_UPLOAD | FTRAN_SWIPE | FTRAN_CHIP  | FTRAN_CLESS ,
		FTOTAL_SALE,
		TRAN_STEPS_SALE,
		FALSE,
	},

//	{ 	TRAN_REFUND,
//		MSG_TRAN_REFUND,
//		FTRAN_REVERSE | FTRAN_BATCH | FTRAN_VOID | FTRAN_UPLOAD | FTRAN_SWIPE | FTRAN_CHIP | FTRAN_MANUAL,
//		FTOTAL_REFUND,
//		TRAN_STEPS_REFUND,
//		FALSE,
//	},

	{ 	TRAN_VOID,
		MSG_TRAN_VOID,
		FTRAN_BATCH | FTRAN_SWIPE | FTRAN_CHIP | FTRAN_MANUAL ,
		FTOTAL_REFUND,
		TRAN_STEPS_VOID,
		FALSE,
	},

	{ 	TRAN_ADJUST,
		MSG_TRAN_ADJUST,
		FTRAN_SWIPE | FTRAN_CHIP | FTRAN_MANUAL,
		FTOTAL_SALE,
		TRAN_STEPS_ADJUST,
		FALSE,
	},
//
//	//+ CLESS @mamata Dec 11, 2014
//	{ 	TRAN_QUICK_SALE,
//		MSG_TRAN_QUICK_SALE,
//		FTRAN_REVERSE | FTRAN_BATCH | FTRAN_VOID | FTRAN_ADJUST | FTRAN_UPLOAD | FTRAN_SWIPE | FTRAN_CHIP | FTRAN_MANUAL ,
//		FTOTAL_SALE,
//		TRAN_STEPS_QUICK_SALE,
//		FALSE,
//	},
	//- CLESS

	//+ ECHOTEST @mamata Aug 15, 2014
	{ 	TRAN_TEST,
		MSG_TRAN_TEST,
		0,
		0,
		TRAN_STEPS_TEST,
		FALSE,
	},
	{ 	TRAN_LOGON,
		MSG_TRAN_LOGON,
		0,
		0,
		TRAN_STEPS_LOGON,
		FALSE,
	},
	{ 	DUPLICATE,
			MSG_DUPLICADO,
			0,
			0,
			STEPS_DUPLICATE,
			FALSE,
	},
	{ 	LAST_ANSWER,
			MSG_TRAN_OLD,
			0,
			0,
			STEPS_LAST_ANSWER,
			FALSE,
		},
		{ 	TRAN_C2P,
			MSG_TRAN_C2P,
			FTRAN_REVERSE | FTRAN_BATCH | FTRAN_VOID | FTRAN_ADJUST | FTRAN_UPLOAD |  FTRAN_MANUAL ,
			FTOTAL_SALE,
			TRAN_STEPS_C2P,
			FALSE,
		},
		{ 	TRAN_TEST_AUTO,
			MSG_TRAN_TEST,
			0,
			0,
			TRAN_STEPS_TEST_AUTO,
			FALSE,
		},
	//- ECHOTEST

//	//+ CARCAAN_PINPAD @mamata Jan 14, 2015
//	{
//		TRAN_PINPAD,
//		MSG_TRAN_SALE,
//		0,
//		0,
//		TRAN_STEPS_PINPAD_TRAN,
//		FALSE,
//	},
//	//- CARCAAN_PINPAD
//
//	//+ PPGETCARD @mamata Jul 1, 2015
//	{
//		TRAN_PINPAD_CARD,
//		MSG_TRAN_SALE,
//		0,
//		0,
//		TRAN_STEPS_PINPAD_CARD,
//		FALSE,
//	},
//	//- PPGETCARD


	// EN LIST
	{
		TRAN_END_LIST,
		0,
		0,
		FALSE,
		NULL,
	}
};

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	RunTransaction
 * Description:		Execute transaction listed in TRANS_LIST
 * Parameters:
 *  - TRANS_LIST TranID - Id of transaction to be execute
 *  - S_TRANSIN * param_in
 *  - TLV_TREE_NODE EMPparam_in
 *  - bool Fallback
 * Return:
 *  - RET_OK - transaction executed OK
 *  - RET_NOK - transaction canceled or time out ocurred
 *  -1 - cannot run transaction because an tlvtree error ocurred
 *  -2 - transaction not found
 * Notes:
 */
int RunTransaction(TRANS_LIST TranID, S_TRANSIN * param_in, TLV_TREE_NODE EMPparam_in, bool Fallback)
{
	int FunResult;
	int RetFun;
	TRAN_INFO TranInfo;
	uint16 TranStatus = 0,TranTest = 0;
	uint8 EntryMode = 0;
	DATE fecha;
	TLV_TREE_NODE node;
	uint8 ServiceCode[3];
	int o;

//	uint8 ResponseCode;
//	char ServiceCode[3+1];

	//--- Delete batch files if necessary
	FunResult = Batch_DeleteSettled();

	//--- Reset Tree_tran if necessary
	if(Tree_Tran != NULL)
	{
		TlvTree_Release(Tree_Tran);
		Tree_Tran = NULL;
	}

	//--- Initialize tree with new memory
	Tree_Tran = TlvTree_New(0);
	CHECK(Tree_Tran != NULL, LBL_TREE_ERROR);

	if( param_in!= NULL && EMPparam_in == NULL)
	{
	 GetTrack2((uint8*)param_in->track2);
	//- FIX0001 @pdm
		node = TlvTree_Find(Tree_Tran, TAG_SERVICE_CODE, 0);
		CHECK(node != NULL, LBL_TRACK_ERR);
		memcpy(ServiceCode, TlvTree_GetData(node), 3);
		CHECK(!(ServiceCode[0] == '2' || ServiceCode[0] == '6'), LBL_BANDA_CHIP);

	}

	// get transaction information
	FunResult = GetTransactionInfo(TranID, &TranInfo);
	CHECK(FunResult == RET_OK, LBL_NO_TRAN);


	if (TranID == TRAN_TEST_AUTO) {
		// get transaction information
		// add to Tree_Tran the transaction id
		TranTest = TRAN_TEST;
		RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_ID, &TranTest, sizeof(TranInfo.TRAN_ID));
		CHECK(RetFun == RET_OK, LBL_TREE_ERROR);
	}else{
		// add to Tree_Tran the transaction id
		RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_ID, &TranInfo.TRAN_ID, sizeof(TranInfo.TRAN_ID));
		CHECK(RetFun == RET_OK, LBL_TREE_ERROR);
	}


	// add to Tree_Tran the message id of transaction name
	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_NAME, &TranInfo.TRAN_NAME, sizeof(TranInfo.TRAN_NAME));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);

	// add to Tree_Tran the transaction options flags
	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_OPTIOS, &TranInfo.TRAN_FLAG, sizeof(TranInfo.TRAN_FLAG));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);

	// add to Tree_Tran the transaction status
	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRAN_STATUS, &TranStatus, sizeof(TranStatus));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);
	//EUGENIO
	Telium_Read_date(&fecha);
	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_DATE_TIME, &fecha, sizeof(fecha));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);

//	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_EMV_1GEN_TAGS, &ValueNode, LTAG_UINT8);
//	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);

//	RetFun = Tlv_SetTagValue(Tree_Tran, TAG_HOST_RESPONSE_CODE, &ResponseCode, sizeof(ResponseCode));
//	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);
//EUGENIO
	//--- SWIPE FROM IDLE
	if( param_in!= NULL && EMPparam_in == NULL)
	{
		//Get Track 1
		if(param_in->cr_iso1 == ISO_OK || param_in->cr_iso1 == DEF_LUH)
		{
			RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRACK1, param_in->track1, strlen((char*)param_in->track1) );
			//+ FIX0001 @pdm May 11, 2016
			//d GetTrack1((uint8*)param_in->track1);
			//- FIX0001 @pdm
		}

		//Get Track 2
		CHECK( param_in->cr_iso2 == ISO_OK || param_in->cr_iso2 == DEF_LUH, LBL_TRACK_ERR);
		RetFun = Tlv_SetTagValue(Tree_Tran, TAG_TRACK2, param_in->track2, strlen((char*)param_in->track2) );
		CHECK(RetFun == RET_OK, LBL_TRACK_ERR);
		//+ FIX0001 @pdm May 11, 2016
		// GetTrack2((uint8*)param_in->track2);
		//- FIX0001 @pdm

				EntryMode = ENTRY_MODE_SWIPE;
				Tlv_SetTagValue(Tree_Tran, TAG_TRAN_IDLE_CARD, &EntryMode, sizeof(EntryMode));



		//Set Tag to indicate transaction start SWIPE from IDLE


	}
	//--- EMV FROM IDLE
	else if(EMPparam_in != NULL)
	{
		//Set Tag to indicate transaction start CHIP from IDLE
		EntryMode = ENTRY_MODE_CHIP;
		Tlv_SetTagValue(Tree_Tran, TAG_TRAN_IDLE_CARD, &EntryMode, sizeof(EntryMode));
	}

	if(param_in != NULL)
	{
		amount TotalAmount = param_in->amount;
		if( param_in->amount > 0 )
		{
			Tlv_SetTagValue(Tree_Tran, TAG_BASE_AMOUNT, &TotalAmount, LTAG_AMOUNT);

			uint8 value = 1;
			Tlv_SetTagValue(Tree_Tran, TAG_MANAGER_TRANSACTION, &value, LTAG_UINT8);
		}
	}

	//<!--- Start EWL Payment mode
	utilOpenDriver();
	//-->

	UI_ClearAllLines();
	o=1;
	FunResult = STM_RunTransaction(TranInfo.TRAN_STEPS, STEP_LIST);
//	if(IsPCLDoTransaction())
//	{
//		if( Tlv_FindTag(Tree_Tran, TAG_PI_TRANS_ERROR) != RET_OK)
//			PI_SetTransactionError(Tree_Tran, RCT_DECLINED_BY_HOST);
//	}

	//<!--- Finish EWL Payment mode
	EWL_DestroyEmvHandle();

	utilCloseDriver();
	//-->

	CHECK(FunResult == RET_OK, LBL_CANCELTO);

	utilWaitToRemoveChipCard();

	return RET_OK;

LBL_TRACK_ERR: // Track read error

	UI_ShowMessage(NULL,
			mess_getReturnedMessage(MSG_ERROR_CARD_READ),
			"",
			GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

	utilWaitToRemoveChipCard();
	SetIdleHeaderFooter();

	return RET_NOK;

LBL_BANDA_CHIP: // TARJETA DE BANDA POR CHIP

		UI_ShowMessage(NULL,"UTILICE",
				"CHIP",
				GL_ICON_ERROR, GL_BUTTON_ALL, 2);

		utilWaitToRemoveChipCard();
		SetIdleHeaderFooter();

		return RET_NOK;
LBL_CANCELTO: // cancel or timeout

	utilWaitToRemoveChipCard();
	LLC_EndConnection();
	SetIdleHeaderFooter();

	return RET_NOK;

LBL_TREE_ERROR: // cannot assign memory to Tree_Tran

	utilWaitToRemoveChipCard();
	LLC_EndConnection();
	SetIdleHeaderFooter();

	return -1;

LBL_NO_TRAN:	// transaction not found

	utilWaitToRemoveChipCard();
	SetIdleHeaderFooter();

	return -2;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetTransactionInfo
 * Description:		find and get transaction info from TRANSACTIONS
 * Parameters:
 *  - TRANS_LIST TranID - Transaction ID
 *  - TRAN_INFO *TranInfo - where to put data if found
 * Return:
 *  - RET_OK - transaction found
 *  - RET_NOK - transaction not found
 * Notes:
 */
int GetTransactionInfo(TRANS_LIST TranID, TRAN_INFO *TranInfo)
{
	int i = 0;
	bool found = false;
	TRAN_INFO *tmpTranInfo;

	if(TranID == 0xFFFF)
		return RET_NOK;

	do
	{
		tmpTranInfo = &TRANSACTIONS[i];

		// verify if pStep match
		if(tmpTranInfo->TRAN_ID == TranID)
			found = true;

		// verify if is end of list
		if(tmpTranInfo->TRAN_ID == 0xFFFF)
		{
			// no more steps, return NULL
			tmpTranInfo = NULL;
			found = true;
		}

		i++;
	}while(found == false);

	if(found)
	{
		memcpy(TranInfo, tmpTranInfo, sizeof(TRAN_INFO));
		return RET_OK;
	}

	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	MenuTransaction
 * Description:		Show menu transaction and execute selection
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int MenuTransaction(void)
{
	int RetFun;
	int Index = 0;
	UI_MenuReset();

	// build menu
	do
	{
		if(TRANSACTIONS[Index].Enable)
			UI_MenuAddItem(mess_getReturnedMessage(TRANSACTIONS[Index].TRAN_NAME), TRANSACTIONS[Index].TRAN_ID);

		Index++;
	}while(TRANSACTIONS[Index].TRAN_ID != TRAN_END_LIST);

	// run menu
	RetFun = UI_MenuRun("TRANSACCION", tConf.TimeOut, 0);
	CHECK(RetFun >= 0, LBL_CANCEL);

	RetFun = RunTransaction(RetFun, NULL, NULL, FALSE);

	return RET_OK;

LBL_CANCEL:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	EnableTransactions
 * Description:		review configuration to enable transactions
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int EnableTransactions(void)
{
	int Index = 0;

	// build menu
	do
	{
		if(IsTransactionEnable(TRANSACTIONS[Index].TRAN_ID))
			TRANSACTIONS[Index].Enable = TRUE;
		else
			TRANSACTIONS[Index].Enable = FALSE;
		Index++;
	}while(TRANSACTIONS[Index].TRAN_ID != TRAN_END_LIST);

	return RET_OK;
}
