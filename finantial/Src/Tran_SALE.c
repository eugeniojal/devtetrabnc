/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Tran_SALE.c
 * Header file:		Tran_SALE.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
uint16 TRAN_STEPS_SALE[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP,
	STEP_BASE_AMOUNT,
	STEP_TAX_AMOUNT,
	//RESTAURANT
//	STEP_CONFIRM_TOTAL, //RESTAURANT
	STEP_ENTER_ID_NUMBER,
	STEP_START_PAYMENT, 		//EWL, set entry supported entry modes
	STEP_EWL_START,				//EWL, Initialize handle, set parameters EMV/CLESS
	STEP_WAIT_CARD,				//EWL, wait card and r5ead initial data

	STEP_MANUAL_CARD_ENTRY,
	STEP_MANUAL_EXP_DATE,
	//STEP_CHECK_EXP_DATE,

	STEP_CVV2,
	STEP_LAST_4_PAN,
	STEP_SELECT_MERCHANT,
	STEP_CHECK_FALLBACK,
	STEP_CHECK_SERVICE_CODE,
	STEP_TIP_AMOUNT_RESTAURANT,
	STEP_VALIDATE_MOUNT,
	STEP_TRAN_SERVER, //RESTAURANT MESERO
	STEP_SELECT_ACCOUNT_TYPE,
	STEP_PREDIAL,
	STEP_EWL_GO_ONCHIP,			//EWL, First generate
	STEP_APPROVED_OFF,			//EWL, approved off-line
	STEP_DENIAL_OFF,			//EWL, approved online
	STEP_PIN,
	STEP_PRE_PRINT,
	STEP_SEND_RECEIVE,
	STEP_END_COMMUNICATION,
	STEP_HOST_ANALYSE,			//EWL, Check host response
	STEP_SECOND_TAP,			//EWL, second tap only CLESS
	STEP_FINISH,				//EWL, Execute second Generate
	STEP_PROCESS_RESPONSE,
	STEP_PRINT_RECEIPT,
	STEP_END_LIST
};

/* ***************************************************************************
* C2P
* ***************************************************************************/
uint16 TRAN_STEPS_C2P[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP,

	STEP_SELECT_BANK,
	STEP_MANUAL_TELF_ENTRY,
	//STEP_MANUAL_CARD_ENTRY,

	STEP_SELECT_MERCHANT,
	STEP_BASE_AMOUNT,
	STEP_ENTER_ID_NUMBER,

	STEP_PIN_NUMBER,
	STEP_PRE_PRINT,
	STEP_SEND_RECEIVE,
	STEP_END_COMMUNICATION,
	STEP_HOST_ANALYSE,			//EWL, Check host response
	STEP_FINISH,				//EWL, Execute second Generate
	STEP_PROCESS_RESPONSE,
	STEP_PRINT_RECEIPT,
	STEP_END_LIST
};

