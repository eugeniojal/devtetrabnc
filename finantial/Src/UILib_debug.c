/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			UILib_debug.c
 * Header file:		UILib_debug.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
* PROTOTYPES
* ***************************************************************************/
char* concat(const char *s1, const char *s2);
int Post_STEP_DEBUG_UILIB( char *Result );
int Post_STEP_DEBUG_OUTPUT_PATH( char *Result );
bool Pre_INPUT_TRACE_IP(void);
bool Pre_STEP_TRACE_PORT(void);
int Post_STEP_CHANGE_FILE(char *Result);
/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
#define LZ_szAddress	256
char 	Trace_Ip[LZ_HOST];
char	Trace_Port[LZ_PORT];

typedef enum UILibDebugStepList
{
	STEP_ENABLE_DEBUG,
	STEP_OUTPUT_PATH,
	STEP_CHANGE_TRACEIP,
	STEP_CHANGE_TRACEPORT,
	STEP_CHANGE_FILE
}UILIB_DEBUG_STEPLIST;

unsigned short DEBUG_UILIB_CONF[] =
{
	STEP_ENABLE_DEBUG,
	STEP_OUTPUT_PATH,
	STEP_END_LIST
};

unsigned short DEBUG_UILIB_TRACEIP[] =
{
	STEP_CHANGE_TRACEIP,
	STEP_CHANGE_TRACEPORT,
	STEP_CHANGE_FILE,
	STEP_END_LIST
};

SMT_STEP UILIB_STEP_LIST[] =
{
	{	/* step id */			STEP_ENABLE_DEBUG,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_DEBUG_UILIB
	},
	{	/* step id */			STEP_OUTPUT_PATH,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_DEBUG_OUTPUT_PATH
	},
	#if(TETRA_HARDWARE == 1)
	{
		/* step id */			STEP_CHANGE_TRACEIP,
		/* input type */		INPUT_IP,
		/* input min  */		7,
		/* input max  */		15,
		/* input timeout */		30,
		/* result pointer */	Trace_Ip,
		/* pre function */		Pre_INPUT_TRACE_IP,
		/* post function*/		NULL
	},
	{
		/* step id */			STEP_CHANGE_TRACEPORT,
		/* input type */		INPUT_NUMERIC_TEXT,
		/* input min  */		1,
		/* input max  */		L_PORT,
		/* input timeout */		30,
		/* result pointer */	Trace_Port,
		/* pre function */		Pre_STEP_TRACE_PORT,
		/* post function*/		NULL
	},
	{
		/* step id */			STEP_CHANGE_FILE,
		/* input type */		INPUT_NONE,
		/* input min  */		0,
		/* input max  */		0,
		/* input timeout */		30,
		/* result pointer */	NULL,
		/* pre function */		NULL,
		/* post function*/		Post_STEP_CHANGE_FILE
	}
	#endif//TETRA_HARDWARE
};


/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_DEBUG_UILIB
 * Description:		choose if the uilib debug will be enabled or not
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
int Post_STEP_DEBUG_UILIB( char *Result )
{
	int RetFun = 0;
	UI_MenuReset();
	UI_MenuAddItem("ON", 1);
	UI_MenuAddItem("OFF", 2);
    RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_DEBUG_STEPS_TITLE), 30, 1);
	CHECK(RetFun > 0, LBL_CANCEL);
	switch(RetFun){
	case 1:
		break;
	case 2:
	    logSetChannels(LOG_ALL_CHANNELS, LOG_CRITICAL, NULL, NULL, NULL);
	    break;
	}
	return STM_RET_NEXT;
LBL_CANCEL:
	return STM_RET_CANCEL;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_DEBUG_OUTPUT_PATH
 * Description:		choose the output path for debugging
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
int Post_STEP_DEBUG_OUTPUT_PATH( char *Result )
{
	int RetFun = 0;
	UI_MenuReset();
	int defaultSelect;
	#ifndef TETRA_HARDWARE
		UI_MenuAddItem("CONSOLE", 1);
		defaultSelect=1;
	#else
		UI_MenuAddItem("TRACE", 2);
		defaultSelect=2;
	#endif
		UI_MenuAddItem("FILE",3);
    RetFun = UI_MenuRun("OUTPUT", 30, defaultSelect);
	CHECK(RetFun > 0, LBL_CANCEL);
	debugControluiLib(RetFun);
	return STM_RET_NEXT;
LBL_CANCEL:
	return STM_RET_CANCEL;
}


/* --------------------------------------------------------------------------
 * Function Name:	Pre_INPUT_TRACE_IP
 * Description:		Choose the ip to be used in the trace file
 * Parameters:
 *  - none
 * Return:
 *  - true - the terminal is ready
 * Notes:
 */
bool Pre_INPUT_TRACE_IP(void)
{
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_TRACE_CONFIG_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_IP), UI_ALIGN_CENTER);
	memcpy(Step_Buffer, Trace_Ip, LZ_LID);
	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	Pre_STEP_TRACE_PORT
 * Description:		Choose the port to be used in the trace file
 * Parameters:
 *  - none
 * Return:
 *  - true - the terminal is ready
 * Notes:
 */
bool Pre_STEP_TRACE_PORT(void)
{
	memcpy(Step_Buffer, Trace_Port, LZ_PORT);
	UI_SetLine(UI_LINE4, mess_getReturnedMessage(MSG_HOST_PORT), UI_ALIGN_CENTER);
	return TRUE;
}

char* concat(const char *s1, const char *s2)
{
    const size_t len1 = strlen(s1);
    const size_t len2 = strlen(s2);
    char *result = malloc(len1+len2+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    memcpy(result, s1, len1);
    memcpy(result+len1, s2, len2+1);//+1 to copy the null-terminator
    return result;
}

/* --------------------------------------------------------------------------
 * Function Name:	Post_STEP_CHANGE_FILE
 * Description:		Change the file TRACE_IP.TXT with the new ip
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
int Post_STEP_CHANGE_FILE(char *Result)
{
	char* ipTwoDots = concat(Trace_Ip, ":");
	char* ipFull = concat(ipTwoDots, Trace_Port);
	modifyTraceIP(ipFull);
	free(ipTwoDots);
	free(ipFull);
	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_TRACE_CONFIG_TITLE), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2,mess_getReturnedMessage(MSG_TRACE_UPDATE_L1), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE3,mess_getReturnedMessage(MSG_TRACE_UPDATE_L2), UI_ALIGN_CENTER);
	Telium_Ttestall(0, 5*100);
	RebootTerminal();
	UI_ShowInfoScreen(NULL);
	return STM_RET_NEXT;
}



