//#include "ewlDemo.h"
//#include "transactionData.h"
//#include "aid.h"
//#include "key.h"
#include "iPOSApp.h"

#define PARAMETER_MCC                               "\x36\x00"
#define PARAMETER_TERMINAL_TYPE                     "\x22"
#define PARAMETER_TERMINAL_CAPABILITIES             "\xE0\xF8\xC8"
#define PARAMETER_TERMINAL_ADITIONAL_CAPABILITIES   "\xF0\x00\xF0\xA0\x01"
#define PARAMETER_TERMINAL_COUNTRY_CODE             "\x01\x52"
#define PARAMETER_TERMINAL_CATEGORY_CODE            "R"
#define PARAMETER_TRANSACTION_CURRENCY_CODE         "\x01\x52"
#define PARAMETER_TRANSACTION_CURRENCY_EXPONENT     "\x02"
#define PARAMETER_TRANSACTION_TYPE                  "\x00"

/* --------------------------------------------------------------------------
 * Function Name:	parameterSetTransaction
 * Description:		Set general parameters of EMV/CLESS
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - TLV_TREE_NODE Tree: Transaction Tree
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */
int parameterSetTransaction (ewlObject_t *handle, TLV_TREE_NODE Tree)
{
	int RetFun;
	dateTime_t timeDate;
	DATE stDate;

    //+ FIX0002 @pmata	May 11, 2016
    uint32 STAN;
    char binSTAN[4];
    //- FIX0002

	//+ FIX0004 @pmata	May 11, 2016
	char Terminal[EWL_EMV_TERMINAL_IDENTIFICATION_LEN+1];
	//d char TerminalId[EWL_EMV_TERMINAL_IDENTIFICATION_LEN+1];
	//- FIX0004

	//+ FIX0006 @pmata	May 11, 2016
    char ttype;
    char stype[2];
    //- FIX0006

    //+ FIX0007
    byte type[EWL_TAG_CLESS_DETECTION_TYPE_LEN];
    memset(type,0x00,sizeof(type));
	type[EMV_DETECTION_BYTE_ISO_A] |= EMV_DETECTION_BIT_ISO_A;
	type[EMV_DETECTION_BYTE_ISO_B] |= EMV_DETECTION_BIT_ISO_B;
	RetFun = ewlSetParameter(handle,EWL_TAG_CLESS_DETECTION_TYPE,type,sizeof(type));
	CHECK(RetFun == EWL_OK, LBL_ERROR);
    //+ FIX0007

    uint8 CurrencyId;
    uint8 CurrencyCode[2 + 1];

	TABLE_MERCHANT tMerchant;
	char Merchant[EWL_EMV_MERCHANT_IDENTIFIER_LEN + 1];

	//--- Set merchant ID & terminal ID
	RetFun = Get_TransactionMerchantData(&tMerchant, Tree);
	if(RetFun == RET_OK)
	{
		//+ FIX0004 @pmata	May 11, 2016
		//Set Terminal ID, same information of field 41 ISO8583
		Format_String_Lenght(tMerchant.TID, Terminal, _FORMAT_LEFT, EWL_EMV_TERMINAL_IDENTIFICATION_LEN, ' ');

		RetFun = ewlSetParameter(handle, EWL_EMV_TERMINAL_IDENTIFICATION, Terminal
									, strlen(Terminal));
		CHECK(RetFun == EWL_OK, LBL_ERROR);
		//- FIX0004

		//Set merchant ID, same information of field 42 ISO8583
		Format_String_Lenght(tMerchant.MID, Merchant, _FORMAT_LEFT, EWL_EMV_MERCHANT_IDENTIFIER_LEN, ' ');

		RetFun = ewlSetParameter(handle, EWL_EMV_MERCHANT_IDENTIFIER, Merchant
									, strlen(Merchant));
		CHECK(RetFun == EWL_OK, LBL_ERROR);
	}

	//+ PAR_EWL @carodriguez 29/02/2016
	#if(DIR_EWL_PAR == 1)
    //--- Parameters from external .PAR file
	RetFun = loadExtraPar(extraParTree, handle);
    CHECK(RetFun == RET_OK, LBL_ERROR);

	RetFun = loadRevokedCAs(handle);
    CHECK(RetFun == RET_OK, LBL_ERROR);
	#else
    //--- Terminal Type
    RetFun = ewlSetParameter(handle, EWL_EMV_TERMINAL_TYPE, PARAMETER_TERMINAL_TYPE
                                , EWL_EMV_TERMINAL_TYPE_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- Terminal capabilities
    RetFun = ewlSetParameter(handle, EWL_EMV_TERMINAL_CAPABILITIES, PARAMETER_TERMINAL_CAPABILITIES
                                , EWL_EMV_TERMINAL_CAPABILITIES_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- Terminal additional capabilities
    RetFun = ewlSetParameter(handle, EWL_EMV_ADD_TERMINAL_CAPABILITIES, PARAMETER_TERMINAL_ADITIONAL_CAPABILITIES
                                , EWL_EMV_ADD_TERMINAL_CAPABILITIES_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- Terminal country code
    RetFun = ewlSetParameter(handle, EWL_EMV_TERMINAL_COUNTRY_CODE, PARAMETER_TERMINAL_COUNTRY_CODE
                                , EWL_EMV_TERMINAL_COUNTRY_CODE_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);
	#endif // DIR_EWL_PAR
	//- PAR_EWL

    //--- Merchant Category Code
    RetFun = ewlSetParameter(handle, EWL_EMV_MERCHANT_CATEGORY_CODE, PARAMETER_MCC
                                , EWL_EMV_MERCHANT_CATEGORY_CODE_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- Transaction category code
    RetFun = ewlSetParameter(handle, EWL_EMV_TRANSACTION_CATEGORY_CODE, PARAMETER_TERMINAL_CATEGORY_CODE
                                , EWL_EMV_TRANSACTION_CATEGORY_CODE_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- Transaction currency exponent
    //TODO check currency exponent for Multi-currency merchants and Dynamic Currency Conversion(DCC)
    RetFun = ewlSetParameter(handle, EWL_EMV_TRANSACTION_CURRENCY_EXPONENT, PARAMETER_TRANSACTION_CURRENCY_EXPONENT
                                , EWL_EMV_TRANSACTION_CURRENCY_EXPONENT_LEN);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- Transaction currency code
    //TODO check currency exponent for Multi-currency merchants and Dynamic Currency Conversion(DCC)
    RetFun = Tlv_GetTagValue(Tree_Tran, TAG_CURRENCY, &CurrencyId, NULL );

    if(RetFun == RET_OK)
    {
		memset(CurrencyCode, 0x00, sizeof(CurrencyCode));

		if(CurrencyId == 1)
			GTL_Convert_UlToDcbNumber(atol(tTerminal.CurrCode_Local), CurrencyCode, 2);
		else
			GTL_Convert_UlToDcbNumber(atol("0840"), CurrencyCode, 2);

		RetFun = ewlSetParameter(handle, EWL_EMV_TRANSACTION_CURRENCY_CODE, CurrencyCode
									, EWL_EMV_TRANSACTION_CURRENCY_CODE_LEN);
		CHECK(RetFun == EWL_OK, LBL_ERROR);
    }

    //--- Transaction type
    //+ FIX0006 @pmata	May 11, 2016
    ttype = GetEMVTransactionType();
    stype[0] = ttype;
    RetFun = ewlSetParameter(handle, EWL_EMV_TRANSACTION_TYPE, stype
                                , EWL_EMV_TRANSACTION_TYPE_LEN );
    CHECK(RetFun == EWL_OK, LBL_ERROR);
    //+ FIX0006

    //--- transaction amount
    //TODO check transaction and other amount for Multi-currency merchants and Dynamic Currency Conversion(DCC)
	//+ FIX0002 @pmata	May 11, 2016
//	amount TotalAmount = GetEMVAmount();
    amount TotalAmount = 0;
	RetFun = ewlSetParameterAmount(handle,EWL_EMV_AMOUNT_AUTH_BIN, TotalAmount);
	CHECK(RetFun == EWL_OK, LBL_ERROR);

	amount TotalOtherAmount = GetEMVOtherAmount();
	RetFun = ewlSetParameterAmount(handle,EWL_EMV_AMOUNT_OTHER_BIN, TotalOtherAmount);
	CHECK(RetFun == EWL_OK, LBL_ERROR);
	//- FIX0002

    //+ FIX0003 @pmata	May 11, 2016
    STAN = Counters_Get_Current_STAN();
    GTL_Convert_UlToDcbNumber(STAN, binSTAN, 3);
    RetFun = ewlSetParameter(handle, EWL_EMV_TRANSACTION_SEQUENCE_COUNTER, &binSTAN, EWL_EMV_TRANSACTION_SEQUENCE_COUNTER_MAX_LEN);
    //- FIX0003

    //--- transaction date
	Telium_Read_date(&stDate);
	Tlv_SetTagValue(Tree, TAG_DATE_TIME, &stDate, sizeof(stDate));

	dateTimeGet(&timeDate);

    RetFun = ewlSetParameterDate(handle,EWL_EMV_TRANSACTION_DATE,&timeDate.date);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    //--- transaction time
    RetFun = ewlSetParameterTime(handle,EWL_EMV_TRANSACTION_TIME,&timeDate);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

//------------------------------------------------------------------------------

#ifdef EWL_ENABLE_KERNEL_PAYWAVE

//------------------------------------------------------------------------------
static void SetPayWaveTTQ (ewlObject_t *ewl, unsigned char *out, unsigned int len)
{
	uint8_t ttq[] = { 0xB0 ,0x00 ,0xC0 ,0x00 };
	uint8_t capabilities[EWL_EMV_TERMINAL_CAPABILITIES_LEN];

	#if (DIR_EWL_PAR == 1)
	TLV_TREE_NODE nodeParams = NULL;
	TLV_TREE_NODE ValueParameter;

	nodeParams = TlvTree_Find(PaywaveTree, C_TAG_PAYWAVE, 0);

	if(nodeParams!=NULL)
	{
		ValueParameter = TlvTree_Find(nodeParams, EWL_PAYWAVE_TTQ, 0);
		CHECK(ValueParameter!=NULL, LBL_ERROR)
		memcpy(out, TlvTree_GetData(ValueParameter), TlvTree_GetLength(ValueParameter));
	}
	return;

LBL_ERROR:
	#endif

	if(len < 4) return;

	len = ewlGetParameter(ewl,capabilities,sizeof(capabilities),EWL_EMV_TERMINAL_CAPABILITIES);
	if(len > 0)
	{
		// signature
		if((capabilities[1] & 0x40) == 0x40) ttq[0] = ttq[0] | 0x04;

		// pin-online
		if((capabilities[1] & 0x20) == 0x20) ttq[0] = ttq[0] | 0x02;
	}

	memcpy(out,ttq,sizeof(ttq));

	return;

//    uint8_t ttq[] = { 0xB0 ,0x00 ,0xC0 ,0x00 };
//    uint8_t capabilities[EWL_EMV_TERMINAL_CAPABILITIES_LEN];
//
//    if(len < 4) return;
//
//    len = ewlGetParameter(ewl,capabilities,sizeof(capabilities),EWL_EMV_TERMINAL_CAPABILITIES);
//    if(len > 0){
//
//        // signature
//        if((capabilities[1] & 0x40) == 0x40) ttq[0] = ttq[0] | 0x04;
//
//        // pin-online
//        if((capabilities[1] & 0x20) == 0x20) ttq[0] = ttq[0] | 0x02;
//    }
//
//    memcpy(out,ttq,sizeof(ttq));
//
//    return;
}


static void SetPayWaveEMV(ewlObject_t *ewl, ewlAIDSetStruct_t *parameter, aid_t *aid)
{
    parameter->aditionalParameter.qVSDC.contactLessFloorLimit = aid->risk.FloorLimit;
    parameter->aditionalParameter.qVSDC.cvmRequiredValue = aid->cless.CMVLimit;
    parameter->aditionalParameter.qVSDC.transactionLimitValue = aid->cless.TransactionLimit;
    parameter->aditionalParameter.qVSDC.zeroAmountAllowed = TRUE;
    SetPayWaveTTQ(ewl,parameter->aditionalParameter.qVSDC.ttq,EWL_PAYWAVE_TTQ_LEN);
    return;
}

static void SetPayWaveMag(ewlObject_t *ewl, ewlAIDSetStruct_t *parameter, aid_t *aid)
{
    parameter->aditionalParameter.MSD.contactLessFloorLimit = aid->risk.FloorLimit;
    parameter->aditionalParameter.MSD.cvmRequiredValue = aid->cless.CMVLimit;
    parameter->aditionalParameter.MSD.transactionLimitValue = aid->cless.TransactionLimit;
    parameter->aditionalParameter.MSD.zeroAmountAllowed = TRUE;
    SetPayWaveTTQ(ewl,parameter->aditionalParameter.MSD.ttq,EWL_PAYWAVE_TTQ_LEN);
    return;
}

#endif

#ifdef EWL_ENABLE_KERNEL_PAYPASS
static void SetPayPassEMV(ewlObject_t *ewl, ewlAIDSetStruct_t *parameter, aid_t *aid)
{
	parameter->aditionalParameter.PayPassMChip.contactLessFloorLimit = aid->risk.FloorLimit;
    parameter->aditionalParameter.PayPassMChip.cvmRequiredValue = aid->cless.CMVLimit;
    parameter->aditionalParameter.PayPassMChip.transactionLimitValue = aid->cless.TransactionLimit;
    parameter->aditionalParameter.PayPassMChip.zeroAmountAllowed = TRUE;
    return;
}

static void SetPayPassMag(ewlObject_t *ewl, ewlAIDSetStruct_t *parameter, aid_t *aid)
{
	parameter->aditionalParameter.PayPassMag.contactLessFloorLimit = aid->risk.FloorLimit;
    parameter->aditionalParameter.PayPassMag.cvmRequiredValue = aid->cless.CMVLimit;
    parameter->aditionalParameter.PayPassMag.transactionLimitValue = aid->cless.TransactionLimit;
    parameter->aditionalParameter.PayPassMag.zeroAmountAllowed = TRUE;
    return;
}

#endif

#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
static void SetExpressPayEMV(ewlObject_t *ewl, ewlAIDSetStruct_t *parameter, aid_t *aid)
{
    parameter->aditionalParameter.ExpressPayEMV.contactLessFloorLimit = aid->risk.FloorLimit;
    parameter->aditionalParameter.ExpressPayEMV.cvmRequiredValue = aid->cless.CMVLimit;
    parameter->aditionalParameter.ExpressPayEMV.transactionLimitValue = aid->cless.TransactionLimit;
    parameter->aditionalParameter.ExpressPayEMV.zeroAmountAllowed = TRUE;
    return;
}

static void SetExpressPayMag(ewlObject_t *ewl, ewlAIDSetStruct_t *parameter, aid_t *aid)
{
	parameter->aditionalParameter.ExpressPayMag.contactLessFloorLimit = aid->risk.FloorLimit;
    parameter->aditionalParameter.ExpressPayMag.cvmRequiredValue = aid->cless.CMVLimit;
    parameter->aditionalParameter.ExpressPayMag.transactionLimitValue = aid->cless.TransactionLimit;
    parameter->aditionalParameter.ExpressPayMag.zeroAmountAllowed = TRUE;
    return;
}
#endif

/* --------------------------------------------------------------------------
 * Function Name:	parameterSetAids
 * Description:		Set specific parameters by AID
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes: warning this function must be call after parameterSetTransaction
 */
int ViewAids(void)
{

	int ii;
	aid_t *aid;
	stKey_t *key;
    char aidC [15+1];
    char MontoLimite [4+1];
    char Version [4+1];
    char TacDenial [10+1];
    char TacOnline [10+1];
    char TacDefault [10+1];
    char ThresHoldValue [10+1];
    char TargetPercent [10+1];
    char MaxTarPercent [10+1];
    char Tdol [10+1];
    char Ddol [10+1];
    char CAPA1 [10+1];

	ewlObject_t *ewl = NULL;
    int len;
	uint8_t TermType[EWL_EMV_TERMINAL_TYPE_LEN];
	uint8_t TermCountryCode[EWL_EMV_TERMINAL_COUNTRY_CODE_LEN];
	uint8_t CurrencyCode[EWL_EMV_APPLI_CURRENCY_CODE_LEN];
	uint8_t capabilities[EWL_EMV_TERMINAL_CAPABILITIES_LEN];
	uint8_t AddCapabilities[EWL_EMV_ADD_TERMINAL_CAPABILITIES_LEN];

	 ewl = EWL_SetEmvHandle();

	ii = 0;
	UI_OpenPrinter_Ext();
	UI_PrintLine_Ext("REPORTE DE EWL", UI_FONT_DOUBLE, UI_ALIGN_CENTER);


	len = ewlGetParameter(ewl,TermType,sizeof(TermType),EWL_EMV_TERMINAL_TYPE);
	memset(CAPA1,0x00, sizeof(CAPA1));
	ISO_hex_to_asc((char *)&TermType,CAPA1,sizeof(TermType));
	UI_Print2Fields_Ext("TERM TYPE  ", "", UI_FONT_DOUBLE, ":", CAPA1, UI_FONT_DOUBLE);

	len = ewlGetParameter(ewl,TermCountryCode,sizeof(TermCountryCode),EWL_EMV_TERMINAL_COUNTRY_CODE);
	memset(CAPA1,0x00, sizeof(CAPA1));
	ISO_hex_to_asc((char *)&TermCountryCode,CAPA1,sizeof(TermCountryCode));
	UI_Print2Fields_Ext("TERM COUNTRY CODE  ", "", UI_FONT_DOUBLE, ":", CAPA1, UI_FONT_DOUBLE);

	len = ewlGetParameter(ewl,CurrencyCode,sizeof(CurrencyCode),EWL_EMV_APPLI_CURRENCY_CODE_LEN);
	memset(CAPA1,0x00, sizeof(CAPA1));
	ISO_hex_to_asc((char *)&CurrencyCode,CAPA1,sizeof(CurrencyCode));
	UI_Print2Fields_Ext("TERM CURRENCY CODE  ", "", UI_FONT_DOUBLE, ":", CAPA1, UI_FONT_DOUBLE);

	len = ewlGetParameter(ewl,capabilities,sizeof(capabilities),EWL_EMV_TERMINAL_CAPABILITIES);
	memset(CAPA1,0x00, sizeof(CAPA1));
	ISO_hex_to_asc((char *)&capabilities,CAPA1,sizeof(capabilities));
	UI_Print2Fields_Ext("TERM CAPABILITIES", " ", UI_FONT_DOUBLE, ":", CAPA1, UI_FONT_DOUBLE);

	len = ewlGetParameter(ewl,AddCapabilities,sizeof(AddCapabilities),EWL_EMV_ADD_TERMINAL_CAPABILITIES);
	memset(CAPA1,0x00, sizeof(CAPA1));
	ISO_hex_to_asc((char *)&AddCapabilities,CAPA1,sizeof(AddCapabilities));
	UI_Print2Fields_Ext("ADD TERM CAPABILITIES  ", " ", UI_FONT_DOUBLE, ":", CAPA1, UI_FONT_DOUBLE);

	UI_PrintLineFeed_Ext(2);
	UI_PrintLine_Ext("----------------------", UI_FONT_SMALL, UI_ALIGN_CENTER);
	UI_PrintLineFeed_Ext(2);
	while ((aid = aidGet(ii)) != NULL)
	{

		memset(aidC,0x00, sizeof(aidC));
		ISO_hex_to_asc((char *)&aid->aid,aidC,aid->aidLen);
		UI_Print2Fields_Ext("RID+PIX", " ", UI_FONT_DOUBLE, ":", aidC, UI_FONT_DOUBLE);
//		UI_PrintLine_Ext(aidC, UI_FONT_SMALL, UI_ALIGN_LEFT);

		memset(MontoLimite,0x00, sizeof(MontoLimite));
		Binasc((uint8*)MontoLimite,aid->risk.FloorLimit,4);
		UI_Print2Fields_Ext("LIMITE DE PISO", "", UI_FONT_DOUBLE, ":", MontoLimite, UI_FONT_DOUBLE);

		memset(Version,0x00, sizeof(Version));
		ISO_hex_to_asc((char *)&aid->version.version[0],Version,2);
		UI_Print2Fields_Ext("VERSION", " ", UI_FONT_DOUBLE, ":", Version, UI_FONT_DOUBLE);

		memset(TacDenial,0x00, sizeof(TacDenial));
		ISO_hex_to_asc((char *)&aid->tac.denialValue ,TacDenial,5);
		UI_Print2Fields_Ext("TAC DENIAL", " ", UI_FONT_DOUBLE, ":", TacDenial, UI_FONT_DOUBLE);

		memset(TacOnline,0x00, sizeof(TacOnline));
		ISO_hex_to_asc((char *)&aid->tac.onlineValue ,TacOnline,5);
		UI_Print2Fields_Ext("TAC ONLINE", " ", UI_FONT_DOUBLE, ":", TacOnline, UI_FONT_DOUBLE);

		memset(TacDefault,0x00, sizeof(TacDefault));
		ISO_hex_to_asc((char *)&aid->tac.defaultValue ,TacDefault,5);
		UI_Print2Fields_Ext("TAC DEFAULT", " ", UI_FONT_DOUBLE, ":", TacDefault, UI_FONT_DOUBLE);

		memset(ThresHoldValue,0x00, sizeof(ThresHoldValue));
		Binasc((uint8*)ThresHoldValue,aid->risk.Threshold ,4);
		UI_Print2Fields_Ext("THRESHOLD VALUE", " ", UI_FONT_DOUBLE, ":", ThresHoldValue, UI_FONT_DOUBLE);

		memset(TargetPercent,0x00, sizeof(TargetPercent));
		ISO_hex_to_asc((char *)&aid->risk.Target,TargetPercent,1);
		UI_Print2Fields_Ext("TARGET PERCENT", "", UI_FONT_DOUBLE, ":", TargetPercent, UI_FONT_DOUBLE);

		memset(MaxTarPercent,0x00, sizeof(TacDefault));
		ISO_hex_to_asc((char *)&aid->risk.Max ,MaxTarPercent,1);
		UI_Print2Fields_Ext("MAX TAR.PERCENT", "", UI_FONT_DOUBLE, ":", MaxTarPercent, UI_FONT_DOUBLE);

		memset(Tdol,0x00, sizeof(Tdol));
		ISO_hex_to_asc((char *)&aid->tdol.dol ,Tdol,aid->tdol.dolLen );
		UI_Print2Fields_Ext("TDOL", " ", UI_FONT_DOUBLE, ":", Tdol, UI_FONT_DOUBLE);

		memset(Ddol,0x00, sizeof(Ddol));
		ISO_hex_to_asc((char *)&aid->ddol.dol ,Ddol,aid->ddol.dolLen);
		UI_Print2Fields_Ext("DDOL", " ", UI_FONT_DOUBLE, ":", Ddol, UI_FONT_DOUBLE);

		UI_PrintLineFeed_Ext(1);

		ii++;
	}
	UI_PrintLineFeed_Ext(2);
	UI_PrintLine_Ext("----------------------", UI_FONT_SMALL, UI_ALIGN_CENTER);
	UI_PrintLineFeed_Ext(2);
	ii = 0;
	while ((key = keyGet(ii++)) != NULL)
	{
		memset(Ddol,0x00, sizeof(Ddol));
		ISO_hex_to_asc((char *)&key->keyId ,Ddol,1);
		UI_Print2Fields_Ext("PUBLIC KEY INDEX",Ddol, UI_FONT_DOUBLE_REVERSE, "", Ddol, UI_FONT_DOUBLE_REVERSE);

		memset(Ddol,0x00, sizeof(Ddol));
//		ISO_hex_to_asc((char *)&key->keyModulusLen ,Ddol,1);
		Binasc((uint8*)Ddol,key->keyModulusLen,2);
		UI_Print2Fields_Ext("KEY LENGHT ","", UI_FONT_DOUBLE, ":",Ddol, UI_FONT_DOUBLE);

		memset(Ddol,0x00, sizeof(Ddol));
		ISO_hex_to_asc((char *)&key->keyExp ,Ddol,1);
		UI_Print2Fields_Ext("KEY EXPONENT ","", UI_FONT_DOUBLE, ":", Ddol, UI_FONT_DOUBLE);

		memset(Ddol,0x00, sizeof(Ddol));
//		ISO_hex_to_asc((char *)&key->rid ,Ddol,Ddol);
		ISO_hex_to_asc((char *)&key->rid,Ddol,10);
		UI_Print2Fields_Ext("RID ","", UI_FONT_DOUBLE, ":", Ddol, UI_FONT_DOUBLE);

	}
	UI_PrintLineFeed_Ext(6);
	UI_ClosePrinter_Ext();
	return RET_OK;

}

int parameterSetAids(ewlObject_t *handle)
{

	int RetFun;
	int ii;
	aid_t *aid;
	ewlAIDSetStruct_t parameter;

	ii = 0;
	while ((aid = aidGet(ii)) != NULL)
	{
		memset(&parameter, 0x00, sizeof(parameter));
		parameter.userData = ii;
		parameter.technology = aid->technology;
		parameter.aidLen = aid->aidLen;
		memcpy(parameter.aid, aid->aid, aid->aidLen);

		switch (aid->technology)
		{
			#ifdef EWL_ENABLE_KERNEL_PAYWAVE
			case EWL_TECHNOLOGY_QVSDC: SetPayWaveEMV(handle,&parameter,aid); break;
			case EWL_TECHNOLOGY_MSD: SetPayWaveMag(handle,&parameter,aid); break;
			#endif

			#ifdef EWL_ENABLE_KERNEL_PAYPASS
			case EWL_TECHNOLOGY_PAYPASS_MCHIP: SetPayPassEMV(handle,&parameter,aid); break;
			case EWL_TECHNOLOGY_PAYPASS_MAG: SetPayPassMag(handle,&parameter,aid); break;
			#endif

			#ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
			case EWL_TECHNOLOGY_EXPRESSPAY_EMV: SetExpressPayEMV(handle,&parameter,aid); break;
			case EWL_TECHNOLOGY_EXPRESSPAY_MAG: SetExpressPayMag(handle,&parameter,aid); break;
			#endif

			default:
				break;
		}

		RetFun = ewlAddAID(handle, &parameter);
		if (RetFun != EWL_OK)
			return RetFun;

		ii++;
	}

	return RET_OK;

}

/* --------------------------------------------------------------------------
 * Function Name:	VersionList
 * Description:		Set version of EMV/CLESS
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - aid_t *parameter: Parameters of selected AID
 * Return:
 * - EWLDEMO_OK: ok
 * - Other value: error
 * Notes
 */

static int VersionList (ewlObject_t *ewl, aid_t *parameter)
{

    byte versions[EWLDEMO_VERSION_NUMBER_MAX * 2];
    int ii = 0;
    int pos;
    int ret;

    memset(versions,0x00,sizeof(versions));
    pos = 0;
    for (ii = 0; ii < parameter->version.nVersions;ii++){
        memcpy(&versions[pos],&parameter->version.version[ii].value,EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN);
        pos = pos + EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN;
    }

    ret = ewlSetParameter(ewl, EWL_TAG_TERMINAL_APPLICATION_VERSION_LIST, versions,pos);
    if(ret != EWL_OK) return ret;

    return EWLDEMO_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	CommonParameters
 * Description:		Set common parameters
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - aid_t *parameter: Parameters of selected AID
 * Return:
 * - EWLDEMO_OK: ok
 * - Other value: error
 * Notes
 */

static int CommonParameters (ewlObject_t *ewl, aid_t *parameter)
{

    int ret;

	ret = ewlSetParameter(ewl, EWL_TAG_TAC_ONLINE, parameter->tac.onlineValue,
			EWL_TAG_TAC_ONLINE_LEN);
	if (ret != EWL_OK)
		return ret;

	ret = ewlSetParameter(ewl, EWL_TAG_TAC_DENIAL, parameter->tac.denialValue,
			EWL_TAG_TAC_DENIAL_LEN);
	if (ret != EWL_OK)
		return ret;

	ret = ewlSetParameter(ewl, EWL_TAG_TAC_DEFAULT, parameter->tac.defaultValue,
			EWL_TAG_TAC_DEFAULT_LEN);
	if (ret != EWL_OK)
		return ret;

    ret = ewlSetParameterAmount(ewl, EWL_EMV_TERMINAL_FLOOR_LIMIT, parameter->risk.FloorLimit);
	if (ret != EWL_OK)
		return ret;

	ret = ewlSetParameterAmount(ewl, EWL_TAG_THRESHOLD_VALUE_BIASED_RAND_SEL,
			parameter->risk.Threshold);
	if (ret != EWL_OK)
		return ret;

	ret = ewlSetParameter(ewl, EWL_TAG_TARGET_PERC_RAND_SEL,
			parameter->risk.Target, EWL_TAG_TARGET_PERC_RAND_SEL_LEN);
	if (ret != EWL_OK)
		return ret;

	ret = ewlSetParameter(ewl, EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL,
			parameter->risk.Max, EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL_LEN);
	if (ret != EWL_OK)
		return ret;

	if (parameter->ddol.dolLen != 0)
	{
		ret = ewlSetParameter(ewl, EWL_TAG_DEFAULT_DDOL, parameter->ddol.dol,
				parameter->ddol.dolLen);
		if (ret != EWL_OK)
			return ret;
	}

	if (parameter->tdol.dolLen != 0)
	{
		ret = ewlSetParameter(ewl, EWL_TAG_DEFAULT_TDOL, parameter->tdol.dol,
				parameter->tdol.dolLen);
		if (ret != EWL_OK)
			return ret;
	}

	return EWLDEMO_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	KeyList
 * Description:		Set list of keys
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *ewl: Handle EWL
 * Return:
 * - EWLDEMO_OK: ok
 * - Other value: error
 * Notes
 */

static int KeyList (ewlObject_t *ewl )
{
	byte list[keyNumber() * EWL_EMV_CA_PUBLIC_KEY_INDEX_TERM_LEN];
	byte rid[EWL_TAG_RID_LEN];
	int pos = 0;
	int ii;
	int ret;
	stKey_t *key;

	memset(rid, 0x00, sizeof(rid));
	ret = ewlGetParameter(ewl, rid, sizeof(rid), EWL_TAG_RID);
	if (ret < 0)
		return ret;

	memset(list, 0x00, sizeof(list));
	ii = 0;
	pos = 0;
	while ((key = keyGet(ii++)) != NULL)
	{
		if (memcmp(key->rid, rid, EWL_TAG_RID_LEN) == 0)
		{
			memcpy(&list[pos], &key->keyId,
					EWL_EMV_CA_PUBLIC_KEY_INDEX_TERM_LEN);
			pos = pos + EWL_EMV_CA_PUBLIC_KEY_INDEX_TERM_LEN;
		}
	}

	ret = ewlSetParameter(ewl, EWL_TAG_PK_LIST, list, pos);
	if (ret != EWL_OK)
		return ret;

	return EWLDEMO_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	ClessParameters
 * Description:		Set general contact-less parameters
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *ewl: Handle EWL
 * - aid_t *parameter: Parameters
 * Return:
 * - EWLDEMO_OK: ok
 * - Other value: error
 * Notes
 */
static int ClessParameters (ewlObject_t *ewl, aid_t *parameter)
{
	int RetFun;

	RetFun = KeyList(ewl);
    CHECK(RetFun == EWLDEMO_OK, LBL_ERROR);

    RetFun = ewlSetParameterAmount(ewl,EWL_TAG_CLESS_TRANSACTION_LIMIT,parameter->cless.TransactionLimit);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

	RetFun = ewlSetParameterAmount(ewl,EWL_TAG_CLESS_CVM_LIMIT,parameter->cless.CMVLimit);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

	RetFun = ewlSetParameterAmount(ewl,EWL_TAG_CLESS_FLOOR_LIMIT,parameter->risk.FloorLimit);
    CHECK(RetFun == EWL_OK, LBL_ERROR);

    return RetFun;

LBL_ERROR:
	return EWL_ERR_INTERNAL_PARAMETER;

}

/* --------------------------------------------------------------------------
 * Function Name:	parameterApplicationSpecific
 * Description:		Set specific application parameters
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *ewl: Handle EWL
 * - int id:
 * Return:
 * - EWLDEMO_OK: ok
 * - Other value: error
 * Notes
 */
int parameterApplicationSpecific (ewlObject_t *ewl, int id )
{
	int ret;
	aid_t *parameter = NULL;
	ewlTechnology_t technology = EWL_TECHNOLOGY_UNKNOWN;

	parameter = aidGet(id);
	if (parameter == NULL)
		return EWLDEMO_ERR_DATA_NOT_FOUND;

	ret = ewlGetParameter(ewl, &technology, sizeof(technology),
			EWL_TAG_TECHNOLOGY);
	if (ret < 0)
		return ret;

	if (technology == EWL_TECHNOLOGY_UNKNOWN)
		return EWLDEMO_ERR_INTERNAL;

	ret = CommonParameters(ewl, parameter);
	if (ret != EWLDEMO_OK)
		return ret;

	ret = VersionList(ewl, parameter);
	if (ret != EWLDEMO_OK)
		return ret;

	if (technology != EWL_TECHNOLOGY_CONTACT)
	{
		ret = ClessParameters(ewl, parameter);
		if (ret != EWLDEMO_OK)
			return ret;
	}

	return EWLDEMO_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	parameterSetHost
 * Description:		Set host parameter based Host response
 * Author:			@jbotero
 * Parameters:
 * - TLV_TREE_NODE Tree: tarnsaction tree
 * Return:
 * - RET_OK: ok
 * - Other value: error
 * Notes
 */

int parameterSetHost (TLV_TREE_NODE Tree)
{
    int ret;
    TLV_TREE_NODE node;
    TLV_TREE_NODE nodeAux;

    ewlObject_t *ewl = EWL_GetEmvHandle();
    ewlHostAnswer_t answer;
    char ResponseCode[EWL_EMV_AUTHORISATION_RESPONSE_CODE_LEN + 1] 	= {0x00, };
    char authorizationCode[EWL_EMV_AUTHORISATION_CODE_LEN + 1]		= {0x00, };

    //--- Get host answer and Set EWL
    ret = Tlv_GetTagValue(Tree, TAG_TRAN_ANSWER, &answer, NULL);
    CHECK( ret == RET_OK, LBL_ERROR);

    ret = ewlSetParameter(ewl,EWL_TAG_HOST_ANSWER, &answer
    						,EWL_TAG_HOST_ANSWER_LEN);
    CHECK(ret == EWL_OK, LBL_ERROR);

    //--- Get host response code and Set EWL
    ret = Tlv_GetTagValue(Tree, TAG_HOST_RESPONSE_CODE, ResponseCode, NULL);
    CHECK( ret == RET_OK, LBL_ERROR);

    ret = ewlSetParameter(ewl,EWL_EMV_AUTHORISATION_RESPONSE_CODE ,ResponseCode
                             ,EWL_EMV_AUTHORISATION_RESPONSE_CODE_LEN);
    CHECK(ret == EWL_OK, LBL_ERROR);

    //--- Get authorization response code and Set EWL
    ret = Tlv_GetTagValue(Tree, TAG_HOST_AUTH_NUMBER, authorizationCode, NULL);
    if( ret == RET_OK )
    {
		ret = ewlSetParameter(ewl,EWL_EMV_AUTHORISATION_CODE, authorizationCode
								 ,EWL_EMV_AUTHORISATION_CODE_LEN);
		CHECK(ret == EWL_OK, LBL_ERROR);
    }

    //--- Get Tags field 55 from HOST and load to EWL
    node = TlvTree_Find( Tree, TAG_EMV_HOST_TAGS, 0);
	if ( node != NULL)
	{
		// Copy all tags to TAG_EMV_F55TOHOST node
		nodeAux = TlvTree_GetFirstChild( node );
		CHECK( nodeAux != NULL, LBL_END );

		while (nodeAux != NULL)
		{
			// Copy tag information to EWL
	        ret = ewlSetParameter(ewl,
	        		TlvTree_GetTag( nodeAux ),
	        		TlvTree_GetData( nodeAux ),
	        		TlvTree_GetLength( nodeAux ));
	        CHECK(ret == EWL_OK, LBL_ERROR);

			// select next tag
	        nodeAux = TlvTree_GetNext( nodeAux );
		}
	}

LBL_END:
    return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
