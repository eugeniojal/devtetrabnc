/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Print.c
 * Header file:		Print.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int Print_TransactionHeader(TLV_TREE_NODE Tree);
int Print_TransactionMerchantInfo(TLV_TREE_NODE Tree);
int Print_TransactionCardData(TLV_TREE_NODE Tree);
int Print_TransactionInfo(TLV_TREE_NODE Tree);
int Print_TransactionAmounts(TLV_TREE_NODE Tree);
int Print_TransactionSignature(TLV_TREE_NODE Tree, bool Copy);
int Print_TransactionImage(void);
//+ TREE01 @mamata Dec 16, 2014
int Print_TipGuide(amount BaseAmount, uint8 TIP_Percent1, uint8 TIP_Percent2, uint8 TIP_Percent3, TLV_TREE_NODE Tree);
//- TREE01
//- EMV03
int Print_Footer(void);
int Print_ReceiptOwner(bool Copy);
int Print_DuplicateLine(bool Duplicate);
int Print_VoidLine(TLV_TREE_NODE Tree);
int Print_EMVInfo(TLV_TREE_NODE Tree);
int Print_Msg_Denied(TLV_TREE_NODE Tree);

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
static bool FlagSendFmtVoucherECR;

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/* --------------------------------------------------------------------------
 * Function Name:	UI_PrintLine_Ext
 * Description:		Extends UI_PrintLine to consider Unattended and PCL.
 * Parameters:
 *  - char *Line - line to be printed (up to 42 characters)
 * Return:
 * Notes:
 */
int UI_PrintLine_Ext(char *Line, UI_FONT tFont, UI_ALIGN Aling )
{
	if(!FlagSendFmtVoucherECR)
		UI_PrintLine(Line, tFont, Aling );
	else
	{
		TLV_TREE_NODE node;
		char * VoucherECR = NULL;
		unsigned int LnVoucherECR = 0;
		char TempBuffer[44+1];
		int spaces = 0;

		if(Tlv_FindTag(Tree_Tran, TAG_VOUCHER_CUSTOMER) == RET_OK)
			node = TlvTree_Find(Tree_Tran, TAG_VOUCHER_ECR_COPY, 0);
		else
			node = TlvTree_Find(Tree_Tran, TAG_VOUCHER_ECR, 0);

		if(NULL != node)
			LnVoucherECR = TlvTree_GetLength(node);

		switch(Aling)
		{
			case UI_ALIGN_CENTER:
				//Fill spaces
				memset(TempBuffer, ' ', sizeof(TempBuffer));
				memset(TempBuffer + 42, 0x00, sizeof(TempBuffer) - 42);

				//Load buffer
				spaces = (42 - strlen(Line))/2;
				memcpy(TempBuffer + spaces, Line, strlen(Line));
				break;

			case UI_ALIGN_LEFT:
				//Fill spaces
				memset(TempBuffer, ' ', sizeof(TempBuffer));
				memset(TempBuffer + 42, 0x00, sizeof(TempBuffer) - 42);

				//Load buffer
				memcpy(TempBuffer, Line, strlen(Line));
				break;

			case UI_ALIGN_RIGHT:
				//Fill spaces
				memset(TempBuffer, ' ', sizeof(TempBuffer));
				memset(TempBuffer + 42, 0x00, sizeof(TempBuffer) - 42);

				spaces = (42 - strlen(Line));
				memcpy(TempBuffer + spaces, Line, strlen(Line));

				break;

			default:
				return RET_NOK;
		}


		VoucherECR = memAllocZero(LnVoucherECR + sizeof(TempBuffer) + 1);

		if(LnVoucherECR > 0)
			memcpy(VoucherECR, TlvTree_GetData(node), LnVoucherECR);
		strcat(VoucherECR, TempBuffer);

		if(IsPCLDoTransaction())
			strcat(VoucherECR, "\n");

		if(node != NULL)
			TlvTree_SetData(node, VoucherECR, strlen(VoucherECR));
		else
		{
			if(Tlv_FindTag(Tree_Tran, TAG_VOUCHER_CUSTOMER) == RET_OK)
				TlvTree_AddChild(Tree_Tran, TAG_VOUCHER_ECR_COPY, VoucherECR, strlen(VoucherECR));
			else
				TlvTree_AddChild(Tree_Tran, TAG_VOUCHER_ECR, VoucherECR, strlen(VoucherECR));
		}
		if(VoucherECR != NULL)
			ufree(VoucherECR);
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	UI_PrintLineFeed_Ext
 * Description:		Extends UI_PrintLineFeed to consider Unattended and PCL.
 * Parameters:
 *  - int lines - number of lines of feed
 * Return:
 * Notes:
 */
int UI_PrintLineFeed_Ext(int lines)
{
	if(!FlagSendFmtVoucherECR)
		UI_PrintLineFeed(lines);
	else
	{
		TLV_TREE_NODE node;
		char * VoucherECR = NULL;
		unsigned int LnVoucherECR = 0;
		char TempBuffer[44+1];
		int i = 0;

		for(i = 0; i < lines; i++)
		{
			if(Tlv_FindTag(Tree_Tran, TAG_VOUCHER_CUSTOMER) == RET_OK)
				node = TlvTree_Find(Tree_Tran, TAG_VOUCHER_ECR_COPY, 0);
			else
				node = TlvTree_Find(Tree_Tran, TAG_VOUCHER_ECR, 0);

			if(NULL != node)
				LnVoucherECR = TlvTree_GetLength(node);

			//Fill spaces
			memset(TempBuffer, ' ', sizeof(TempBuffer));
			memset(TempBuffer + 42, 0x00, sizeof(TempBuffer) - 42);

			VoucherECR = memAllocZero(LnVoucherECR + sizeof(TempBuffer) + 1);

			if(LnVoucherECR > 0)
				memcpy(VoucherECR, TlvTree_GetData(node), LnVoucherECR);
			strcat(VoucherECR, TempBuffer);

			if(IsPCLDoTransaction())
				strcat(VoucherECR, "\n");

			if(node != NULL)
				TlvTree_SetData(node, VoucherECR, strlen(VoucherECR));
			else
			{
				if(Tlv_FindTag(Tree_Tran, TAG_VOUCHER_CUSTOMER) == RET_OK)
					TlvTree_AddChild(Tree_Tran, TAG_VOUCHER_ECR_COPY, VoucherECR, strlen(VoucherECR));
				else
					TlvTree_AddChild(Tree_Tran, TAG_VOUCHER_ECR, VoucherECR, strlen(VoucherECR));
			}
			if(VoucherECR != NULL)
				ufree(VoucherECR);
		}
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	UI_Print2Fields_Ext
 * Description:		Extends UI_Print2Fields to consider Unattended and PCL.
 * Parameters:
 *  - char *F1Title - title of field 1
 *  - char *F1Text - text of field 1
 *  - UI_FONT F1Font - not used
 *  - char *F2Title - title of field 2
 *  - char *F2Text - text of field 2
 *  - UI_FONT F2Font - not used
 * Return:
 * Notes:
 */
int UI_Print2Fields_Ext(char *F1Title, char *F1Text, UI_FONT F1Font, char *F2Title, char *F2Text, UI_FONT F2Font)
{
	if(!FlagSendFmtVoucherECR)
		UI_Print2Fields(F1Title, F1Text, F1Font, F2Title, F2Text, F2Font);
	else
	{
		unsigned char Field1[44];
		unsigned char Field2[44];
		unsigned char tmp[44];
		int F1Len = 0;
		int F2Len = 0;
		int tempLen = 0;

		TLV_TREE_NODE node;
		char * VoucherECR = NULL;
		unsigned int LnVoucherECR = 0;
		char TempBuffer[44+1];

		if(strlen(F1Title) + strlen(F1Text) + strlen(F2Title) + strlen(F2Text) > 39)
			return RET_NOK;

		memset(Field1, 0x00, sizeof(Field1));
		memset(Field2, 0x00, sizeof(Field2));

		F1Len = sprintf((char*)Field1, "%s %s ", F1Title, F1Text);
		tempLen = sprintf((char*)tmp, "%s %s", F2Title, F2Text);

		F2Len = 42 - F1Len;
		memset(Field2, 0x20, 42 - F1Len);
		Field2[F2Len] = 0;
		memcpy(&Field2[F2Len - tempLen], tmp, tempLen);

		//Concatenate buffer 1 & 2
		memset(TempBuffer, 0x00, sizeof(TempBuffer));
		strlcat(TempBuffer, (char*)Field1, sizeof(TempBuffer));
		strlcat(TempBuffer, (char*)Field2, sizeof(TempBuffer));

		if(Tlv_FindTag(Tree_Tran, TAG_VOUCHER_CUSTOMER) == RET_OK)
			node = TlvTree_Find(Tree_Tran, TAG_VOUCHER_ECR_COPY, 0);
		else
			node = TlvTree_Find(Tree_Tran, TAG_VOUCHER_ECR, 0);

		if(NULL != node)
			LnVoucherECR = TlvTree_GetLength(node);

		VoucherECR = memAllocZero(LnVoucherECR + sizeof(TempBuffer) + 1);

		if(LnVoucherECR > 0)
			memcpy(VoucherECR, TlvTree_GetData(node), LnVoucherECR);
		strcat(VoucherECR, TempBuffer);

		if(IsPCLDoTransaction())
			strcat(VoucherECR, "\n");

		if(node != NULL)
			TlvTree_SetData(node, VoucherECR, strlen(VoucherECR));
		else
		{
			if(Tlv_FindTag(Tree_Tran, TAG_VOUCHER_CUSTOMER) == RET_OK)
				TlvTree_AddChild(Tree_Tran, TAG_VOUCHER_ECR_COPY, VoucherECR, strlen(VoucherECR));
			else
				TlvTree_AddChild(Tree_Tran, TAG_VOUCHER_ECR, VoucherECR, strlen(VoucherECR));
		}

		if(VoucherECR != NULL)
			ufree(VoucherECR);
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	UI_OpenPrinter_Ext
 * Description:		Extends UI_OpenPrinter to consider Unattended and PCL.
 * Parameters:
 *  - none
 * Return:
 * 	0	Printer opened and FONT_42 loaded
 * 	-1	Cannot open printer handle
 * 	-2	Cannot load FONT_42
 * Notes:
 */
int UI_OpenPrinter_Ext(void)
{
	//Default: Print voucher, don't build for ECR
	FlagSendFmtVoucherECR = FALSE;

	if (IsPrinter())
		FlagSendFmtVoucherECR = FALSE;
	else if(!IsPrinter() && !IsPCLDoTransaction() ){
		FlagSendFmtVoucherECR = TRUE;
	}
	else
	{
		//<!--Check mode PCL
		if (IsPCLDoTransaction())
		{
			char TempBuffer[30];
			unsigned int MaxLn = sizeof(TempBuffer) - 1;
			int RetFun;

			memset(TempBuffer, 0x00, sizeof(TempBuffer));

			RetFun = Tlv_GetTagValue(Tree_NoPrinter, TAG_POS_NOPRINTER_VOUCHER,TempBuffer, &MaxLn);
			if (RetFun == RET_OK)
			{
					FlagSendFmtVoucherECR = TRUE;//Don't Print voucher, only send to ECR
			}
		}
	}
	//-->

	if (!FlagSendFmtVoucherECR)
		UI_OpenPrinter();

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	UI_ClosePrinter_Ext
 * Description:		Extends UI_ClosePrinter to consider Unattended and PCL.
 * Parameters:
 * Return:
 * Notes:
 */
void UI_ClosePrinter_Ext(void)
{
	if(!FlagSendFmtVoucherECR)
		UI_ClosePrinter();

	FlagSendFmtVoucherECR = FALSE;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionReceipt
 * Description:		Print recetip for a transaction
 * Parameters:
 *  - TLV_TREE_NODE Tree
 *  - bool Copy
 *  - bool Duplicate
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionReceipt(TLV_TREE_NODE Tree, bool Copy, bool Duplicate)
{
	int RetFun;
	if(Copy && IsPCLDoTransaction())
		RetFun = Tlv_SetTagValue(Tree, TAG_VOUCHER_CUSTOMER, &Copy, NULL);

	RetFun = UI_OpenPrinter_Ext();


	if(!(tTerminal.Flags1 & TERM_FLAG1_PRE_PRINT) || Copy ==TRUE){
		Print_TransactionImage();
		Print_TransactionHeader(Tree);
	}

	RetFun = Print_TransactionInfo(Tree);
	Print_TransactionMerchantInfo(Tree);
	Print_TransactionCardData(Tree);
	Print_EMVInfo(Tree);
//	UI_PrintLineFeed_Ext(1);
	Print_VoidLine(Tree);
	Print_TransactionAmounts(Tree);
	Print_DuplicateLine(Duplicate);
	Print_TransactionSignature(Tree, Copy);
	Print_Footer();
//	UI_PrintLineFeed_Ext(1);
	Print_ReceiptOwner(Copy);
	UI_PrintLineFeed_Ext(6);
	UI_ClosePrinter_Ext();
	return RET_OK;
}
int Print_Pre_TransactionReceipt(TLV_TREE_NODE Tree)
{

	UI_OpenPrinter_Ext();
	Print_TransactionImage();
	Print_TransactionHeader(Tree);
	UI_ClosePrinter_Ext();
	return RET_OK;
}

int Print_Pre_TransactionDenied(TLV_TREE_NODE Tree)
{
	UI_OpenPrinter_Ext();

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRE_PRINT)){
		Print_TransactionImage();
		Print_TransactionHeader(Tree);
	}

	Print_TransactionInfo(Tree);
	Print_TransactionMerchantInfo(Tree);
	Print_TransactionCardData(Tree);
	Print_EMVInfo(Tree);
	Print_Msg_Denied(Tree);
	Print_Footer();
	UI_PrintLineFeed_Ext(6);
	UI_ClosePrinter_Ext();
	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionHeader
 * Description:		Print header for transaction
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data8uynm
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_TransactionHeader(TLV_TREE_NODE Tree)
{
	TABLE_MERCHANT Merchant;
	TLV_TREE_NODE node;
	int RetFun;
	TABLE_RANGE Range;
	char titulo2[20+1];
	char titulo[30+1];
	int16 TranID;
	uint16 TranStatus;
	char line2[42+1];
	int len=0;
	char serial[42+1];
	char FullSerialNumberRaw[8+1];


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	switch (TranID) {

		case TRAN_SALE:
		case TRAN_C2P:
			if(TranStatus & FSTATUS_NEED_VOID){
				strcpy(titulo2,"ANULACION ");
			}else{
				strcpy(titulo2,"COMPRA ");
			}

			break;
		case TRAN_VOID:
			strcpy(titulo2,"ANULACION ");
			break;
		case DUPLICATE:
		case LAST_ANSWER:
			if(TranStatus & FSTATUS_VOIDED){
					strcpy(titulo2,"ANULACION ");
				}else{
					strcpy(titulo2,"COMPRA ");
				}
			break;
		default:
			strcpy(titulo2,"COMPRA ");
			break;
	}

	if(TranID != TRAN_C2P)
	{
	RetFun = Get_TransactionRangeData(&Range, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	memcpy(line2, Range.Name, sizeof(Range.Name));
	Telium_Sprintf(titulo,"%s %s",titulo2,line2);
	}
	if(TranID == TRAN_C2P)
	{
		Telium_Sprintf(titulo,"%s C2P",titulo2);
	}

	RetFun = Get_TransactionMerchantData(&Merchant, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	memset(FullSerialNumberRaw, 0x00, sizeof(FullSerialNumberRaw));
	PSQ_Give_Serial_Number((char *)FullSerialNumberRaw);
	strcpy(serial,"S/N POS: ");
	len = strlen(serial);
	strcpy(serial+len,FullSerialNumberRaw);


	if(TranID == LAST_ANSWER)
		UI_PrintLine_Ext("ULTIMA RESPUESTA", UI_FONT_DOUBLE_REVERSE, UI_ALIGN_CENTER);
		// use merchant header lines
		// header line 1
		UI_PrintLine_Ext(tTerminal.Header_Line1, UI_FONT_SMALL, UI_ALIGN_CENTER);
		UI_PrintLine_Ext(titulo, UI_FONT_SMALL, UI_ALIGN_CENTER);
		//UI_PrintLine_Ext(tTerminal.NameC, UI_FONT_SMALL, UI_ALIGN_LEFT);
		UI_PrintLine_Ext(serial, UI_FONT_SMALL, UI_ALIGN_CENTER);
		// header line 2
		if(tTerminal.Header_Line2[0] != 0 )
			UI_PrintLine_Ext(tTerminal.Header_Line2, UI_FONT_SMALL, UI_ALIGN_CENTER);
		// header line 3
		if(tTerminal.Header_Line3[0] != 0 )
			UI_PrintLine_Ext(tTerminal.Header_Line3, UI_FONT_SMALL, UI_ALIGN_CENTER);
		// header line 4
		if(tTerminal.Header_Line4[0] != 0 )
			UI_PrintLine_Ext(tTerminal.Header_Line4, UI_FONT_SMALL, UI_ALIGN_CENTER);



	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_MerchantInfo
 * Description:		Print merchant info of transaction (TID and MID)
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_TransactionMerchantInfo(TLV_TREE_NODE Tree)
{
	TABLE_MERCHANT Merchant;
	int RetFun;
	uint32 BATCH_NUMBER;
	TLV_TREE_NODE node;
	char lote[50+1];
	char comercio [50+1];

	node = TlvTree_Find(Tree,TAG_BATCH_NUMBER, 0);
	memcpy((char *)&BATCH_NUMBER, TlvTree_GetData(node), LTAG_UINT32);

	RetFun = Get_TransactionMerchantData(&Merchant, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	Telium_Sprintf(comercio,"%s RIF:%s (%s)",tTerminal.NameC, tTerminal.RifC,VERSION_APP);
	Telium_Sprintf(lote,"Nro.AFIL:%s %s LOTE:%04lu",Merchant.MID,Merchant.TID,BATCH_NUMBER);


	UI_PrintLine_Ext(comercio, UI_FONT_SMALL, UI_ALIGN_LEFT);
	UI_PrintLine_Ext(lote, UI_FONT_NORMAL,UI_ALIGN_LEFT);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionCardData
 * Description:		Print card data
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_TransactionCardData(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	TABLE_RANGE Range;
	uint8 PAN[25];
	uint8 FPAN[25];
	char trj[50];
	char aprb[50];
	char AuthNumber[7];
	char CStan[7];
	uint32 Stan;
	uint16 TranID;
	int RetFun;
	uint8 Entry_Mode;

	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&Entry_Mode, TlvTree_GetData(node), LTAG_UINT8);


	RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);


	// get transaciton id
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == BATCH_RET_OK, LBL_ERROR);

	// get authorization number

	node = TlvTree_Find(Tree, TAG_PAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	memset(PAN, 0, 25);
	memset(FPAN, 0, 25);
	memcpy(PAN, TlvTree_GetData(node), TlvTree_GetLength(node));
	FormatPAN(PAN, FPAN);
	memset(trj, 0, 50);
	//Telium_Sprintf(trj,"TARJ.  :%s",(char*)FPAN);

	switch (Entry_Mode) {
			case ENTRY_MODE_NONE:
			case ENTRY_MODE_MANUAL:
			case ENTRY_MODE_CLESS_SWIPE:
			case ENTRY_MODE_SWIPE:
				Telium_Sprintf(trj,"%s: %s",Range.Name,(char*)FPAN);
			break;

			case ENTRY_MODE_CHIP:
				Telium_Sprintf(trj,"%s (CT): %s",Range.Name,(char*)FPAN);
			break;

			case ENTRY_MODE_CLESS_CHIP:

				Telium_Sprintf(trj,"%s (RF): %s",Range.Name,(char*)FPAN);
				break;
			}


	// get Invoice number
	node = TlvTree_Find(Tree, TAG_STAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	Stan = *(uint32*)TlvTree_GetData(node);
	Binasc((uint8*)CStan, Stan, 6);

	if((TranID == DUPLICATE) || (TranID == LAST_ANSWER))
	{
		node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
		memset(AuthNumber, 0, 7);
		memcpy(AuthNumber, TlvTree_GetData(node), TlvTree_GetLength(node));
		CHECK(node != NULL, LBL_ERROR);
		memset(aprb, 0, 50);
		Telium_Sprintf(aprb,"No.Ref.: %s APROB: %s",CStan,AuthNumber);
		UI_PrintLine_Ext(trj, UI_FONT_NORMAL,UI_ALIGN_LEFT);
		UI_PrintLine_Ext(aprb, UI_FONT_NORMAL,UI_ALIGN_LEFT);
	}
	else {
		node = TlvTree_Find(Tree, TAG_APPROVED, 0);
		if(node != NULL)
		{
			node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
			memset(AuthNumber, 0, 7);
			memcpy(AuthNumber, TlvTree_GetData(node), TlvTree_GetLength(node));
			CHECK(node != NULL, LBL_ERROR);
			memset(aprb, 0, 50);
			Telium_Sprintf(aprb,"No.Ref.: %s APROB: %s",CStan,AuthNumber);
			UI_PrintLine_Ext(trj, UI_FONT_NORMAL,UI_ALIGN_LEFT);
			UI_PrintLine_Ext(aprb, UI_FONT_NORMAL,UI_ALIGN_LEFT);

		}else{
			memset(aprb, 0, 50);
			Telium_Sprintf(aprb,"No.Ref.: %s",CStan);
			UI_PrintLine_Ext(trj, UI_FONT_NORMAL,UI_ALIGN_LEFT);
			UI_PrintLine_Ext(aprb, UI_FONT_NORMAL,UI_ALIGN_LEFT);
		}
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionInfo
 * Description:		Print transaction information
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_TransactionInfo(TLV_TREE_NODE Tree)
{
	int RetFun;
	TLV_TREE_NODE node;
	DATE TranDateTime;
	char DateTime[25];
	char fecha[35];

	// get transaction date and time
	memset((char*)&TranDateTime, 0, sizeof(DATE));
	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranDateTime, TlvTree_GetData(node), sizeof(DATE));

	memset(DateTime, 0, 25);
	memcpy(DateTime, TranDateTime.day, 2);
	DateTime[2] = '/';
	memcpy(&DateTime[3], TranDateTime.month, 2);
	DateTime[5] = '/';
	memcpy(&DateTime[6], TranDateTime.year, 2);
	memcpy(&DateTime[8], " HORA: ", 7);
	memcpy(&DateTime[15], TranDateTime.hour, 2);
	DateTime[17] = ':';
	memcpy(&DateTime[18], TranDateTime.minute, 2);

	Telium_Sprintf(fecha,"FECHA: %s",DateTime);

	RetFun = UI_PrintLine_Ext(fecha,UI_FONT_NORMAL,UI_ALIGN_LEFT);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}




/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionAmounts
 * Description:		Print transaction amounts
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_TransactionAmounts(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	char CurrencySymbol[LZ_TERM_CURR_SYMBOL];
	uint8 Currency;
	uint32 ServerNumber = 0;
	uint16 TranName;
	amount BaseAmount;
	//amount TaxAmount = 0;
	amount TipAmount = 0;
	char CAmount[20];
	//char monto[40];
	TABLE_MERCHANT Merchant;
	int RetFun;
	bool Negative = FALSE;
	bool Total = FALSE;
	bool OpenTotal = FALSE;
	TABLE_RANGE Range;
	char Mesero[50];
	uint16 TranID;

	node = TlvTree_Find(Tree, TAG_SERVER_NUMBER, 0);
	if(node != NULL)
	{
			if((Merchant.Flags1 & MERCH_FLAG1_TIP)||
					((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT)))
			{
				memset(Mesero, 0, 50);
				ServerNumber = *(uint32*)TlvTree_GetData(node);
				Telium_Sprintf(Mesero,"Mesero: %d",ServerNumber);
				UI_PrintLine_Ext(Mesero, UI_FONT_NORMAL,UI_ALIGN_LEFT);
			}
	}
	// get currency symbol
	//+ EMV03 @mamata Dec 16, 2014
	node = TlvTree_Find(Tree, TAG_CURRENCY, 0);
	//d node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
	//- EMV03
	CHECK(node != NULL, LBL_ERROR);
	memcpy(&Currency,TlvTree_GetData(node), LTAG_UINT8);
	if(Currency == 1)
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
	else
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);

	// get transaction name
	node = TlvTree_Find(Tree, TAG_TRAN_NAME, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranName = *(uint16*)TlvTree_GetData(node);

	// get merchant of transaction data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// turn on Negative if is a REFUND transaction
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);
	if(TranID == TRAN_REFUND)
		Negative = TRUE;

	// turn on Negative if is a VOID
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	if ((*(uint16*)TlvTree_GetData(node) & FSTATUS_VOIDED) || (*(uint16*)TlvTree_GetData(node) & FSTATUS_NEED_VOID))
		Negative = TRUE;


	//magallanes
	// add base amount

	if(*(uint16*)TlvTree_GetData(node) & FSTATUS_ADJUSTED)
	{
		memset((char*)&CAmount, 0, 20);
		node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
		CHECK(node != NULL, LBL_ERROR);
		BaseAmount = *(amount*)TlvTree_GetData(node);
		Format_Amount(BaseAmount, CAmount, CurrencySymbol, Negative);
		UI_Print2Fields_Ext(mess_getReturnedMessage(MSG_AMOUNT), " ", UI_FONT_DOUBLE, " ", CAmount, UI_FONT_DOUBLE);
	}
	else
	{
			memset((char*)&CAmount, 0, 20);
			node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
			CHECK(node != NULL, LBL_ERROR);
			BaseAmount = *(amount*)TlvTree_GetData(node);


			memset((char*)&CAmount, 0, 20);
			node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
			if(node != NULL)
			TipAmount = *(amount*)TlvTree_GetData(node);

			Format_Amount(BaseAmount+TipAmount, CAmount, CurrencySymbol, Negative);
			UI_Print2Fields_Ext(mess_getReturnedMessage(MSG_AMOUNT), " ", UI_FONT_DOUBLE, " ", CAmount, UI_FONT_DOUBLE);
	}


	// add tip amount
	if (TranID == TRAN_SALE){
		node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
		if(node != NULL)
		{
			if((Merchant.Flags1 & MERCH_FLAG1_TIP)||
					((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT)))
			{
				Total = TRUE;
				memset((char*)&CAmount, 0, 20);
				TipAmount = *(amount*)TlvTree_GetData(node);
				/*if(TipAmount != 0)
				{
					Format_Amount(TipAmount, CAmount, CurrencySymbol, Negative);
					UI_Print2Fields_Ext(mess_getReturnedMessage(MSG_TIP), " ", UI_FONT_DOUBLE, " ", CAmount, UI_FONT_DOUBLE);
				}
				else
				{
					OpenTotal = TRUE;*/
					//UI_PrintLine_Ext(mess_getReturnedMessage(MSG_TIP), UI_FONT_DOUBLE, UI_ALIGN_LEFT);
				UI_Print2Fields_Ext(mess_getReturnedMessage(MSG_TIP), " ", UI_FONT_DOUBLE,CurrencySymbol, "__________________ ",  UI_FONT_DOUBLE);
				//}
			}
		}

		if(Total)
		{
			//UI_PrintLine("------------------------------", UI_FONT_NORMAL, UI_ALIGN_RIGHT);
			if(OpenTotal)
			{
				UI_PrintLine_Ext(mess_getReturnedMessage(MSG_TIP), UI_FONT_DOUBLE, UI_ALIGN_LEFT);
			}
			else
			{
				memset((char*)&CAmount, 0, 20);
				//Format_Amount(BaseAmount + TaxAmount + TipAmount, CAmount, CurrencySymbol, Negative);
				//Format_Amount(BaseAmount, CAmount, CurrencySymbol, Negative);
			//	UI_PrintLine_Ext(mess_getReturnedMessage(MSG_TOTAL), UI_FONT_DOUBLE, UI_ALIGN_LEFT);
				UI_Print2Fields_Ext(mess_getReturnedMessage(MSG_TOTAL), " ", UI_FONT_DOUBLE,CurrencySymbol, "__________________ ",  UI_FONT_DOUBLE);



			}
		}
	}


	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionSignature
 * Description:		Print Signature section if necessary
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 *  - bool Copy
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_TransactionSignature(TLV_TREE_NODE Tree, bool Copy)
{
	//char CardHolderName[100];

	uint8 EntryMode = 0;
	int16 TranID;
	bool PinOnline = 0;
	bool PinOffline = 0;
	bool Signature = 0;
	int RetFun;
	TABLE_MERCHANT Merchant;
	TLV_TREE_NODE node;
	uint32 RangID;



	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if (RangID == 8)
		return RET_OK;
	else
	{

	//--- Get Entry mode
	RetFun = Tlv_GetTagValue(Tree, TAG_ENTRY_MODE, &EntryMode, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Get TranID
	RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_ID, &TranID, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Check Copy voucher, return
	CHECK(Copy == FALSE, LBL_END);

	//--- Validation for EMV/CLESS
	if(EntryMode == ENTRY_MODE_CHIP || EntryMode == ENTRY_MODE_CLESS_CHIP)
	{
		//--- Pin off-line requested
		RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_REQ_PIN_OFFLINE, &PinOffline, NULL);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		//--- Pin online requested
		RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_REQ_PIN_ONLINE, &PinOnline, NULL);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		if(PinOffline == TRUE || PinOnline == TRUE)
		{
			UI_PrintLine_Ext("NO SE REQUIERE FIRMA", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
			return RET_OK;
		}

		//--- Signature requested
		RetFun = Tlv_GetTagValue(Tree, TAG_TRAN_REQ_SIGNATURE, &Signature, NULL);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		//--- Verify if signature is required
		if(!Signature)
		{
			UI_PrintLine_Ext("NO SE REQUIERE FIRMA", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
			return RET_OK;
		}
	}

	// print signature line
	UI_PrintLineFeed_Ext(1);
	UI_PrintLine_Ext(mess_getReturnedMessage(MSG_PIRNT_SIGNATURE), UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_PrintLine_Ext(mess_getReturnedMessage(MSG_PIRNT_CI), UI_FONT_NORMAL, UI_ALIGN_LEFT);

	// print card holder name
/*	if(tTerminal.Flags1 & TERM_FLAG1_PRINT_CARDHOLDER)
	{
		//--- print card holder name
		memset(CardHolderName, 0, sizeof(CardHolderName));
		Tlv_GetTagValue(Tree, TAG_CARDHOLDER_NAME, CardHolderName, NULL);
		CHECK(RetFun == RET_OK, LBL_END);

		UI_PrintLine_Ext(CardHolderName, UI_FONT_NORMAL, UI_ALIGN_CENTER);
	}*/


	// print disclaimer note
	RetFun = Get_TransactionMerchantData(&Merchant, Tree);
	CHECK(RetFun == RET_OK, LBL_END);

	if(Merchant.Flags1 & MERCH_FLAG1_PRINT_DISCLAIMER)
	{
		UI_PrintLineFeed_Ext(1);
		if(tTerminal.Disclaimer_Line1[0] == 0)
		{
			UI_PrintLine_Ext(mess_getReturnedMessage(MSG_DISCLAIMER_LINE1), UI_FONT_NORMAL, UI_ALIGN_CENTER);
			UI_PrintLine_Ext(mess_getReturnedMessage(MSG_DISCLAIMER_LINE2), UI_FONT_NORMAL, UI_ALIGN_CENTER);
			UI_PrintLine(mess_getReturnedMessage(MSG_DISCLAIMER_LINE3), UI_FONT_NORMAL, UI_ALIGN_CENTER);
		}
		else
		{
			UI_PrintLine_Ext(tTerminal.Disclaimer_Line1, UI_FONT_NORMAL, UI_ALIGN_CENTER);
			if(tTerminal.Disclaimer_Line2[0] != 0)
				UI_PrintLine_Ext(tTerminal.Disclaimer_Line2, UI_FONT_NORMAL, UI_ALIGN_CENTER);
			if(tTerminal.Disclaimer_Line3[0] != 0)
				UI_PrintLine_Ext(tTerminal.Disclaimer_Line3, UI_FONT_NORMAL, UI_ALIGN_CENTER);
			if(tTerminal.Disclaimer_Line4[0] != 0)
				UI_PrintLine_Ext(tTerminal.Disclaimer_Line4, UI_FONT_NORMAL, UI_ALIGN_CENTER);
		}
	}

LBL_END:
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionImage
 * Description:		Print image /HOST/PRINTER.BMP
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionImage(void)
{
	if(tTerminal.Flags1 & TERM_FLAG1_PRINT_LOGO)
		UI_PrintImage("file://flash/HOST/LOGO.JPG");

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionImage_Ext
 * Description:		Print image /HOST/PRINTER.BMP if is Tetra
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionImage_Ext(void)
{
#if(TETRA_HARDWARE!=1)
	if(!FlagSendFmtVoucherECR){
		if(tTerminal.Flags1 & TERM_FLAG1_PRINT_LOGO)
		UI_PrintImage("file://flash/HOST/PRINTER.BMP");
	}
#else
	 Print_TransactionImage();
#endif
	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_TipGuide
 * Description:		Print tip guide
 * Parameters:
 *  - amount BaseAmount
 *  - uint8 TIP_Percent1
 *  - uint8 TIP_Percent2
 *  - uint8 TIP_Percent3
 *  - TLV_TREE_NODE Tree
 * Return:
 * Notes:
 */
//+ TREE01 @mamata Dec 16, 2014
int Print_TipGuide(amount BaseAmount, uint8 TIP_Percent1, uint8 TIP_Percent2, uint8 TIP_Percent3, TLV_TREE_NODE Tree)
//d int Print_TipGuide(amount BaseAmount, uint8 TIP_Percent1, uint8 TIP_Percent2, uint8 TIP_Percent3)
//- TREE01
{
	TLV_TREE_NODE node;
	amount TIP_1;
	amount TIP_2;
	amount TIP_3;
	char cAmount[20];
	char line[43];
	uint8 Currency;
	char CurrencySymbol[LZ_TERM_CURR_SYMBOL];

	UI_PrintLineFeed_Ext(1);
	UI_PrintLine_Ext(mess_getReturnedMessage(MSG_TIP_GUIDE), UI_FONT_LIGHT, UI_ALIGN_CENTER);

	// get currency symbol
	//+ TREE01 @mamata Dec 16, 2014
	node = TlvTree_Find(Tree, TAG_CURRENCY, 0);
	//d node = TlvTree_Find(Tree_Tran, TAG_CURRENCY, 0);
	//- TREE01
	CHECK(node != NULL, LBL_ERROR);
	memcpy(&Currency,TlvTree_GetData(node), LTAG_UINT8);
	if(Currency == 1)
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
	else
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);

	if(TIP_Percent1 != 0)
	{
		memset(cAmount, 0, 20);
		memset(line, 0, 43);
		TIP_1 = (TIP_Percent1 * BaseAmount) / 100;
		Format_Amount(TIP_1, cAmount, CurrencySymbol, FALSE);
		Binasc((uint8*)line, TIP_Percent1, 2);
		memcpy(&line[2], "% = ", 4);
		memcpy(&line[6], cAmount, strlen(cAmount));
		UI_PrintLine_Ext(line, UI_FONT_LIGHT, UI_ALIGN_CENTER);
	}

	if(TIP_Percent2 != 0)
	{
		memset(cAmount, 0, 20);
		memset(line, 0, 43);
		TIP_2 = (TIP_Percent2 * BaseAmount) / 100;
		Format_Amount(TIP_2, cAmount, CurrencySymbol, FALSE);
		Binasc((uint8*)line, TIP_Percent2, 2);
		memcpy(&line[2], "% = ", 4);
		memcpy(&line[6], cAmount, strlen(cAmount));
		UI_PrintLine_Ext(line, UI_FONT_LIGHT, UI_ALIGN_CENTER);
	}

	if(TIP_Percent3 != 0)
	{
		memset(cAmount, 0, 20);
		memset(line, 0, 43);
		TIP_3 = (TIP_Percent3 * BaseAmount) / 100;
		Format_Amount(TIP_3, cAmount, CurrencySymbol, FALSE);
		Binasc((uint8*)line, TIP_Percent3, 2);
		memcpy(&line[2], "% = ", 4);
		memcpy(&line[6], cAmount, strlen(cAmount));
		UI_PrintLine_Ext(line, UI_FONT_LIGHT, UI_ALIGN_CENTER);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_Footer
 * Description:		Print footer lines
 * Parameters:
 *  - none
 * Return:
 * - RET_OK
 * Notes:
 */
int Print_Footer(void)
{
	if(tTerminal.Footer_Line1[0] != 0)
	{
		UI_PrintLine_Ext(tTerminal.Footer_Line1, UI_FONT_NORMAL, UI_ALIGN_CENTER);

		if(tTerminal.Footer_Line2[0] != 0)
			UI_PrintLine_Ext(tTerminal.Footer_Line2, UI_FONT_NORMAL, UI_ALIGN_CENTER);

		if(tTerminal.Footer_Line3[0] != 0)
			UI_PrintLine_Ext(tTerminal.Footer_Line3, UI_FONT_NORMAL, UI_ALIGN_CENTER);

		if(tTerminal.Footer_Line4[0] != 0)
			UI_PrintLine_Ext(tTerminal.Footer_Line4, UI_FONT_NORMAL, UI_ALIGN_CENTER);

	}

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_ReceiptOwner
 * Description:		Print receipt owner
 * Parameters:
 *  - bool Copy
 * Return:
 *  - RET_OK
 * Notes:
 */

int Print_ReceiptOwner(bool Copy)
{
	if(Copy)
		UI_PrintLine_Ext(mess_getReturnedMessage(MSG_CLIENT_COPY), UI_FONT_LIGHT, UI_ALIGN_CENTER);
	else
		UI_PrintLine_Ext(mess_getReturnedMessage(MSG_MERCHANT_COPY), UI_FONT_LIGHT, UI_ALIGN_CENTER);

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_DuplicateLine
 * Description:		Print duplicate line
 * Parameters:
 *  - bool Duplicate
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_DuplicateLine(bool Duplicate)
{
	if(Duplicate)
	{
		UI_PrintLineFeed_Ext(1);
		UI_PrintLine_Ext(mess_getReturnedMessage(MSG_DUPLICATE), UI_FONT_NORMAL, UI_ALIGN_CENTER);
	}

	return RET_OK;
}


/* --------------------------------------------------------------------------ADSFASDF
 * Description:		Print void line if necessary
 * Parameters:
 *  - TLV_TREE_NODE Tree
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_VoidLine(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;

	// turn on Negative if is a VOID
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	if(*(uint16*)TlvTree_GetData(node) & FSTATUS_NEED_VOID)
	{
		UI_PrintLine_Ext(mess_getReturnedMessage(MSG_PRINT_VOID), UI_FONT_DOUBLE, UI_ALIGN_CENTER);
		UI_PrintLineFeed_Ext(1);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_ReportHeader
 * Description:		Print header of a report
 * Parameters:
 *  - TABLE_MERCHANT Merchant
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_ReportHeader(TABLE_MERCHANT *Merchant, int Reporte)
{
	TLV_TREE_NODE node;
	DATE fecha;
	char cBatchNumber[20];
	DATE TranDateTime;
	char DateTime[25];
	char fecha1[35];
	char comercio[50];
	int RetFun;
	char lote[50+1];


	node = TlvTree_New(0);
	Telium_Read_date(&fecha);
	RetFun = Tlv_SetTagValue(node, TAG_DATE_TIME, &fecha, sizeof(fecha));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);


		// get transaction date and time
		memset((char*)&TranDateTime, 0, sizeof(DATE));
		RetFun = Tlv_GetTagValue(node, TAG_DATE_TIME, &TranDateTime, NULL);

		memset(DateTime, 0, 25);
		memcpy(DateTime, TranDateTime.day, 2);
		DateTime[2] = '/';
		memcpy(&DateTime[3], TranDateTime.month, 2);
		DateTime[5] = '/';
		memcpy(&DateTime[6], TranDateTime.year, 2);
		memcpy(&DateTime[8], " HORA: ", 7);
		memcpy(&DateTime[15], TranDateTime.hour, 2);
		DateTime[17] = ':';
		memcpy(&DateTime[18], TranDateTime.minute, 2);
		Telium_Sprintf(fecha1,"FECHA: %s",DateTime);

LBL_TREE_ERROR:


UI_PrintLine_Ext(tTerminal.Header_Line1, UI_FONT_SMALL, UI_ALIGN_CENTER);

	memset(cBatchNumber, 0, 20);
	Binasc((uint8*)cBatchNumber, Merchant->BatchNumber, 4);
	Telium_Sprintf(lote,"Nro.AFIL:%s %s LOTE:%s",Merchant->MID,Merchant->TID,cBatchNumber);
	Telium_Sprintf(comercio,"%s RIF:%s (%s)",tTerminal.NameC, tTerminal.RifC,VERSION_APP);


	if(Reporte==1)
	{
		UI_PrintLine_Ext("CIERRE DE LOTE", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
	}
	else if (Reporte==2) {
		UI_PrintLine_Ext("REPORTE DETALLADO", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
	}
	else if (Reporte==3) {
			UI_PrintLine_Ext("REPORTE TOTALES", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
	}
	else if (Reporte==4) {
			UI_PrintLine_Ext("TOTAL MESEROS", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
	}
	else if (Reporte==5) {
			UI_PrintLine_Ext("CONSULTA MESERO", UI_FONT_DOUBLE, UI_ALIGN_CENTER);
	}
	//UI_PrintLine_Ext(Merchant->Name, UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_PrintLine_Ext(comercio,UI_FONT_SMALL,UI_ALIGN_LEFT);
	UI_PrintLine_Ext(fecha1, UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_PrintLine_Ext(lote, UI_FONT_NORMAL, UI_ALIGN_LEFT);

	UI_PrintLineFeed_Ext(1);

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionListItem
 * Description:
 * Parameters:
 *  - BATCH *BatchRecord
 *  - bool Fist
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionListItem(BATCH *BatchRecord, bool Fist)
{
	TABLE_RANGE Range;
	char tmpbuffer[20];
	char line[43];
	char line2[43];
	//char Buffer[43];
	char Currency[5];
	bool Negative = FALSE;
	uint8 PAN[25];
    int len;

	if(Fist)
	{
		UI_PrintLine_Ext("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
		UI_PrintLine_Ext(mess_getReturnedMessage(MSG_TRANSACTION_LIST), UI_FONT_DOUBLE, UI_ALIGN_CENTER);
		//UI_PrintLine_Ext(mess_getReturnedMessage(MSG_TRAN_LIST_HEADER), UI_FONT_NORMAL, UI_ALIGN_LEFT);
	}
	UI_PrintLineFeed_Ext(1);
	memset(PAN, 0x20, sizeof(PAN));
	memset(line, 0x20, sizeof(line));
	line[42] = 0;
	memset(line2, 0x20, sizeof(line2));
	line2[42] = 0;
	// invoice

    TM_FindRecord(TAB_RANGE, &Range, BatchRecord->RangeID);

    if(BatchRecord->RangeID==8)
    {
		memcpy(line,"Ref",3);
		Binasc((uint8*)&line[3], BatchRecord->STAN, 4);
		// tarjeta
		FormatPAN(BatchRecord->PAN,PAN);
		memcpy(&line[9],(char*)&PAN,18);
		// date
		memcpy(&line[27], BatchRecord->DateTime.day, 2);
		line[29] = '/';
		memcpy(&line[31], BatchRecord->DateTime.month, 2);
		// tran name


		memcpy(line2, Range.Name, sizeof(Range.Name));
		len = strlen(line2);
		line2[len] =' ';
		line2[len+1] =' ';
		line2[len+2] =' ';
		line2[len+3] =' ';
		line2[len+4] =' ';

		if(BatchRecord->TranID == TRAN_VOID)
			line2[len+5] = 'A';
		else
			line2[len+5] = 'C';
    }
    else
    {
  		memcpy(line,"Ref",3);
  		Binasc((uint8*)&line[3], BatchRecord->STAN, 4);
  		// tarjeta
  		FormatPAN(BatchRecord->PAN,PAN);
  		memcpy(&line[9],(char*)&PAN,16);
  		// date
  		memcpy(&line[27], BatchRecord->DateTime.day, 2);
  		line[29] = '/';
  		memcpy(&line[31], BatchRecord->DateTime.month, 2);
  		// tran name


  		memcpy(line2, Range.Name, sizeof(Range.Name));
  		len = strlen(line2);

		if (BatchRecord->EntryMode==ENTRY_MODE_CLESS_CHIP)
		{
		line2[len] ='(';
		line2[len+1] ='R';
		line2[len+2] ='F';
		line2[len+3] =')';
		}
		else if (BatchRecord->EntryMode==ENTRY_MODE_CHIP)
		{
		line2[len] ='(';
		line2[len+1] ='C';
		line2[len+2] ='T';
		line2[len+3] =')';
		}
		else
		{
		line2[len] =' ';
		line2[len+1] =' ';
		line2[len+2] =' ';
		line2[len+3] =' ';
		}
		line2[len+4] =' ';
  		line2[len+5] = *mess_getReturnedMessage(BatchRecord->TranName);
      }


	// total
	memset(Currency, 0, 3);
	memset(tmpbuffer, 0, 20);
	if(BatchRecord->Currency == 1)
		memcpy(Currency, tTerminal.CurrSymbol_Local, L_TERM_CURR_SYMBOL);
	else
		memcpy(Currency, tTerminal.CurrSymbol_Dollar, L_TERM_CURR_SYMBOL);

	if(BatchRecord->TranID == TRAN_VOID)
		Negative = TRUE;

	// get tran status
	if(BatchRecord->TranStatus & FSTATUS_ADJUSTED)
		Format_Amount((amount)(BatchRecord->BaseAmount), tmpbuffer, Currency, Negative);
	else
		Format_Amount((amount)(BatchRecord->BaseAmount + BatchRecord->TaxAmount + BatchRecord->TipAmount), tmpbuffer, Currency, Negative);

	UI_PrintLine_Ext(line, UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_Print2Fields_Ext(line2,"",UI_FONT_NORMAL,"",tmpbuffer,UI_FONT_NORMAL);
	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionListItemServer
 * Description:
 * Parameters:
 *  - BATCH *BatchRecord
 *  - bool Fist
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionListItemServer(BATCH *BatchRecord)
{
	char tmpbuffer[20];
	char tmppropina[20];
	char line[50];
	char Currency[5];
	bool Negative = FALSE;

	memset(line, 0, sizeof(line));
	// total
	memset(Currency, 0, 3);
	memset(tmpbuffer, 0, 20);
	memset(tmppropina, 0, 20);

	if(BatchRecord->Currency == 1)
		memcpy(Currency, tTerminal.CurrSymbol_Local, L_TERM_CURR_SYMBOL);
	else
		memcpy(Currency, tTerminal.CurrSymbol_Dollar, L_TERM_CURR_SYMBOL);

	if(BatchRecord->TranID == TRAN_VOID)
		Negative = TRUE;

//MONTO
	Format_Amount((amount)(BatchRecord->BaseAmount), tmpbuffer, Currency, Negative);
//PROPINA
	if(BatchRecord->TranStatus & FSTATUS_ADJUSTED)
		Format_Amount((amount)(BatchRecord->TaxAmount), tmppropina, Currency, Negative);
	else
		Format_Amount((amount)(BatchRecord->TaxAmount + BatchRecord->TipAmount), tmppropina, Currency, Negative);

	Telium_Sprintf(line,"%04d   %s ",BatchRecord->STAN,tmpbuffer);
	UI_Print2Fields_Ext(" ", line, UI_FONT_NORMAL, " ", tmppropina, UI_FONT_NORMAL);
	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_TransactionListItemServer
 * Description:
 * Parameters:
 *  - BATCH *BatchRecord
 *  - bool Fist
 * Return:
 *  - RET_OK
 * Notes:
 */
int Print_TransactionTotalServer(TABLE_MERCHANT *Merchant, uint32 ServerId, bool Detalle)
{

	int RetFun;
	amount Total;
	amount Propina;
	char tmpbuffer[20];
	char tmppropina[20];
	char tmpbufferTotal[20];
	char tmppropinaTotal[20];
	char line[50];
	char line2[50];
	char Currency[5];
	bool Negative = FALSE;
	BATCH BatchRecord;
	char Mesero[50];
	Total=0;
	Propina=0;
	memset(tmpbufferTotal, 0, 20);
	memset(tmppropinaTotal, 0, 20);
	memset(line2, 0, sizeof(line2));

	if(Detalle)
	{
			UI_PrintLine_Ext("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
			memset(Mesero, 0, 50);
			Telium_Sprintf(Mesero,"Mesero: %d",ServerId);
			UI_PrintLine_Ext(Mesero, UI_FONT_NORMAL,UI_ALIGN_LEFT);
			UI_PrintLineFeed_Ext(1);
			UI_Print2Fields_Ext("REF.  CONSUMO "," " , UI_FONT_DOUBLE, "PROPINA ", " ", UI_FONT_DOUBLE);
	}

	RetFun = TM_FindFirst(Merchant->BatchName, &BatchRecord);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR_FIND_FIRST);
	do
	{

		if(BatchRecord.Server == ServerId)
		{

			memset(line, 0, sizeof(line));
			memset(Currency, 0, 3);
			memset(tmpbuffer, 0, 20);
			memset(tmppropina, 0, 20);

			if((!(BatchRecord.TranStatus & FSTATUS_NEED_REVERSE)) &&
			(!(BatchRecord.TranStatus & FSTATUS_VOIDED)) &&
			((BatchRecord.TranID != TRAN_VOID)))
			{
				if(BatchRecord.Currency == 1)
					memcpy(Currency, tTerminal.CurrSymbol_Local, L_TERM_CURR_SYMBOL);
				else
					memcpy(Currency, tTerminal.CurrSymbol_Dollar, L_TERM_CURR_SYMBOL);

				if(BatchRecord.TranID == TRAN_VOID)
					Negative = TRUE;

					if(Detalle)
					{
						//MONTO
						Format_Amount((amount)(BatchRecord.BaseAmount), tmpbuffer, Currency, Negative);
						//PROPINA
						if(BatchRecord.TranStatus & FSTATUS_ADJUSTED)
							Format_Amount((amount)(BatchRecord.TaxAmount), tmppropina, Currency, Negative);
						else
							Format_Amount((amount)(BatchRecord.TaxAmount + BatchRecord.TipAmount), tmppropina, Currency, Negative);

							Telium_Sprintf(line,"%04d   %s ",BatchRecord.STAN,tmpbuffer);
						UI_Print2Fields_Ext(" ", line, UI_FONT_NORMAL, " ", tmppropina, UI_FONT_NORMAL);
					}

			Total = Total + BatchRecord.BaseAmount;
			if(BatchRecord.TranStatus & FSTATUS_ADJUSTED)
				Propina = Propina + BatchRecord.TaxAmount;
			else
				Propina = Propina + BatchRecord.TaxAmount + BatchRecord.TipAmount;
			}
		}
		RetFun = TM_FindNext(Merchant->BatchName, &BatchRecord);
	}while(RetFun == TM_RET_OK);

	Format_Amount((amount)(Total), tmpbufferTotal, Currency, Negative);
	Format_Amount((amount)(Propina), tmppropinaTotal, Currency, Negative);
	Telium_Sprintf(line2, "TOTAL  %s", tmpbufferTotal);
	UI_Print2Fields_Ext(" ", line2, UI_FONT_NORMAL, " ", tmppropinaTotal, UI_FONT_NORMAL);

	LBL_ERROR_FIND_FIRST:
		if(RetFun == TM_RET_NO_MORE_RECORDS)
			return RET_OK;
		else
			return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_MerchantTotals
 * Description:		Print totals for specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_MerchantTotals(TABLE_MERCHANT *Merchant)
{
	TOTALS Totals;
	int RetFun;
	char cAmount[20] = {0};
	amount Total;
	char Currency[L_TERM_CURR_SYMBOL] = {0};
	char line[70] = {0};
	bool reporte=FALSE;
	bool EntryMode=FALSE;

	RetFun = Totals_MerchantCalculate(Merchant, &Totals, 0, reporte, EntryMode);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	if(Merchant->Currency == 1)
		memcpy(Currency, tTerminal.CurrSymbol_Local, L_TERM_CURR_SYMBOL);
	else
		memcpy(Currency, tTerminal.CurrSymbol_Dollar, L_TERM_CURR_SYMBOL);

//compra
	memset(cAmount, 0, 20);
	Format_Amount(Totals[Index_SALES].Amount, cAmount, Currency, FALSE);
	Telium_Sprintf(line, "COMPRAS        %04d", Totals[Index_SALES].Count);
	UI_Print2Fields_Ext(line, "", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);
	//Binasc((uint8*)cCount, Totals[Index_SALES].Count, 3);
	//Format_Amount(Totals[Index_SALES].Amount, cAmount, Currency, FALSE);
	//UI_Print2Fields_Ext(mess_getReturnedMessage(MSG_REPORT_SALES), cCount, UI_FONT_DOUBLE, " ", cAmount, UI_FONT_DOUBLE);
//

	if(Merchant->Flags1 & MERCH_FLAG1_TAX || Merchant->Flags1 & MERCH_FLAG1_TIP || tTerminal.Flags1 & TERM_FLAG1_SERVER)
	{
		memset(cAmount, 0, 20);
		Total = Totals[Index_SALES].Amount - Totals[Index_TAX].Amount - Totals[Index_TIP].Amount;
		Format_Amount(Total, cAmount, Currency, FALSE);
		UI_Print2Fields_Ext("  ", mess_getReturnedMessage(MSG_BASE), UI_FONT_NORMAL, cAmount, " ", UI_FONT_NORMAL);
	}

	if(Merchant->Flags1 & MERCH_FLAG1_TAX)
	{
		memset(cAmount, 0, 20);
		Format_Amount(Totals[Index_TAX].Amount, cAmount, Currency, FALSE);
		UI_Print2Fields_Ext("  ", mess_getReturnedMessage(MSG_TAX), UI_FONT_NORMAL, cAmount, " ", UI_FONT_NORMAL);
	}

	if(Merchant->Flags1 & MERCH_FLAG1_TIP || tTerminal.Flags1 & TERM_FLAG1_SERVER)
	{
		memset(cAmount, 0, 20);
		Format_Amount(Totals[Index_TIP].Amount, cAmount, Currency, FALSE);
		UI_Print2Fields_Ext("  ", mess_getReturnedMessage(MSG_TIP), UI_FONT_NORMAL, cAmount, " ", UI_FONT_NORMAL);
	}

//anulacion
	memset(cAmount, 0, 20);
	Format_Amount(Totals[Index_REFUNDS].Amount, cAmount, Currency, TRUE);
	Telium_Sprintf(line, "ANULACIONES    %04d", Totals[Index_REFUNDS].Count);
	UI_Print2Fields_Ext(line, "", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);


//total
	memset(cAmount, 0, 20);
	if(Totals[Index_SALES].Amount >= Totals[Index_REFUNDS].Amount)
	{
		Total = Totals[Index_SALES].Amount - Totals[Index_REFUNDS].Amount;
		Format_Amount(Total, cAmount, Currency, FALSE);
	}
	else
	{
		Total = Totals[Index_REFUNDS].Amount - Totals[Index_SALES].Amount;
		Format_Amount(Total, cAmount, Currency, TRUE);
	}


	Telium_Sprintf(line, "TOTALES        %04d", Totals[Index_SALES].Count + Totals[Index_REFUNDS].Count);
	UI_Print2Fields_Ext(line, "", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);


	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Print_RangeTotals
 * Description:		Print total for specific range
 * Parameters:
 *  - TABLE_MERCHANT *Merchant
 *  - uint32 RangeID
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int Print_RangeTotals(TABLE_MERCHANT *Merchant, uint32 RangeID, bool EntryMode)
{
	TABLE_RANGE Range;
	TOTALS Totals;
	int RetFun;
	char cAmount[20] = {0};
	char Currency[L_TERM_CURR_SYMBOL];
	amount Total;
	char line[64] = {0};
	bool reporte=FALSE;

	RetFun = Totals_MerchantCalculate(Merchant, &Totals, RangeID, reporte, EntryMode );
	CHECK(RetFun == RET_OK, LBL_ERROR);

	if(Merchant->Currency == 1)
		memcpy(Currency, tTerminal.CurrSymbol_Local, L_TERM_CURR_SYMBOL);
	else
		memcpy(Currency, tTerminal.CurrSymbol_Dollar, L_TERM_CURR_SYMBOL);


	RetFun = TM_FindRecord(TAB_RANGE, &Range, RangeID);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//compra
		memset(cAmount, 0, 20);
		Format_Amount(Totals[Index_SALES].Amount, cAmount, Currency, FALSE);
		Telium_Sprintf(line, "COMPRAS        %04d", Totals[Index_SALES].Count);
	if(Totals[Index_SALES].Count > 0)
	{
		if(EntryMode)
		UI_Print2Fields_Ext(Range.Name , "Debit", UI_FONT_NORMAL, " ", " ", UI_FONT_NORMAL);
		else
		UI_Print2Fields_Ext(Range.Name , " ", UI_FONT_NORMAL, " ", " ", UI_FONT_NORMAL);

		UI_Print2Fields_Ext(line, "", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);
		//if(Total != 0)

	//anulacion
		memset(cAmount, 0, 20);
		Format_Amount(Totals[Index_REFUNDS].Amount, cAmount, Currency, TRUE);
		Telium_Sprintf(line, "ANULACIONES    %04d", Totals[Index_REFUNDS].Count);
		UI_Print2Fields_Ext(line, "", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);

	//total
		memset(cAmount, 0, 20);
		if(Totals[Index_SALES].Amount >= Totals[Index_REFUNDS].Amount)
		{
			Total = Totals[Index_SALES].Amount - Totals[Index_REFUNDS].Amount;
			Format_Amount(Total, cAmount, Currency, FALSE);
		}
		else
		{
			Total = Totals[Index_REFUNDS].Amount - Totals[Index_SALES].Amount;
			Format_Amount(Total, cAmount, Currency, TRUE);
		}
		Telium_Sprintf(line, "TOTALES        %04d", Totals[Index_SALES].Count + Totals[Index_REFUNDS].Count);
		UI_Print2Fields_Ext(line, "", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);
		UI_PrintLineFeed_Ext(1);
	}
		//if(Total != 0)
		//UI_Print2Fields_Ext(Range.Name, " ", UI_FONT_NORMAL, " ", cAmount, UI_FONT_NORMAL);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

}


/* --------------------------------------------------------------------------
 * Function Name:	Print_SettleInfo
 * Description:		Print last settle info
 * Parameters:
 *  - TABLE_MERCHANT *Merchant merchant to be printed
 * Return:
 *  - RET_OK;
 * Notes:
 */
int Print_SettleInfo(TABLE_MERCHANT *Merchant)
{
	char SettleDateTime[43];

	UI_PrintLine_Ext("------------------------------------------", UI_FONT_NORMAL, UI_ALIGN_LEFT);
	UI_PrintLine_Ext("** CIERRE EXITOSO **", UI_FONT_DOUBLE, UI_ALIGN_CENTER);

	memset(SettleDateTime, 0, 43);
	memcpy(SettleDateTime, "** ", 3);
	memcpy(&SettleDateTime[3], Merchant->BatchLastSettle.day, 2);
	SettleDateTime[5] = '/';
	memcpy(&SettleDateTime[6], Merchant->BatchLastSettle.month, 2);
	SettleDateTime[8] = '/';
	memcpy(&SettleDateTime[9], Merchant->BatchLastSettle.year, 2);
	memcpy(&SettleDateTime[11], " - ", 3);
	memcpy(&SettleDateTime[14], Merchant->BatchLastSettle.hour, 2);
	SettleDateTime[16] = ':';
	memcpy(&SettleDateTime[17], Merchant->BatchLastSettle.minute, 2);
	memcpy(&SettleDateTime[19], " **", 3);

	UI_PrintLine_Ext(SettleDateTime, UI_FONT_DOUBLE, UI_ALIGN_CENTER);

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Print_EMVInfo
 * Description:		Print EMV info
 * Parameters:
 *  - TLV_TREE_NODE Tree - transacito data
 * Return:
 *  - RET_OK;
 * Notes:
 */
int Print_EMVInfo(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE SubTree;
	TLV_TREE_NODE node;
	char tmpbuff1[42];
	char tmpbuff2[42];
	char Linea[42];

	SubTree = TlvTree_Find(Tree, TAG_EMV_1GEN_TAGS, 0);
	CHECK(SubTree != NULL, LBL_NO_EMV_TRAN);

	memset(tmpbuff1, 0, 40);
	memset(tmpbuff2, 0, 40);
	memset(Linea, 0, 40);

	node = TlvTree_Find(SubTree, TAG_EMV_DF_NAME, 0);
	CHECK(node != NULL, LBL_NO_EMV_TRAN);
	ISO_hex_to_asc(TlvTree_GetData(node), tmpbuff1, TlvTree_GetLength(node));

	//+ QVSDC04 @jbotero 12/03/2016
	//node = TlvTree_Find(SubTree, TAG_EMV_APP_LBL, 0);
	//EUGENIO TAG CT
	node = TlvTree_Find(SubTree, TAG_EMV_APPLICATION_CRYPTOGRAM, 0);
	ISO_hex_to_asc(TlvTree_GetData(node), tmpbuff2, TlvTree_GetLength(node));
	//- QVSDC04
	CHECK(node != NULL, LBL_NO_EMV_TRAN);
	//memcpy(tmpbuff2, TlvTree_GetData(node), TlvTree_GetLength(node));
	Telium_Sprintf(Linea,"AID: %s CT: %s",tmpbuff1,tmpbuff2);
    UI_PrintLine_Ext(Linea, UI_FONT_NORMAL, UI_ALIGN_LEFT);

	memset(tmpbuff1, 0, 40);
	memset(tmpbuff2, 0, 40);
	memset(Linea, 0, 40);

	node = TlvTree_Find(SubTree, TAG_EMV_TVR, 0);
	CHECK(node != NULL, LBL_NO_EMV_TRAN);
	ISO_hex_to_asc(TlvTree_GetData(node), tmpbuff1, TlvTree_GetLength(node));

	node = TlvTree_Find(SubTree, TAG_EMV_TSI, 0);
	if(node != NULL)
	{
		ISO_hex_to_asc(TlvTree_GetData(node), tmpbuff2, TlvTree_GetLength(node));
		Telium_Sprintf(Linea,"TSI: %s TVR: %s",tmpbuff2,tmpbuff1);
		UI_PrintLine_Ext(Linea, UI_FONT_NORMAL, UI_ALIGN_LEFT);
	}
	else
		UI_Print2Fields_Ext("TVR:", tmpbuff1, UI_FONT_NORMAL, " ", " ", UI_FONT_NORMAL);

	return RET_OK;

LBL_NO_EMV_TRAN:
	return RET_OK;
}

int Print_Msg_Denied(TLV_TREE_NODE Tree)
{
	uint16 RC_MessageID = 0;
	TLV_TREE_NODE node;
	char msg [30+1];
	int total;

	memset(&msg, 0, 31);
	total=0;

	node = TlvTree_Find(Tree, TAG_RESPONSE_CODE_TEXT, 0);

	if(node != NULL)
		memcpy((char*)&RC_MessageID, TlvTree_GetData(node), LTAG_UINT16);

	total=strlen(mess_getReturnedMessage(RC_MessageID));
	memcpy((char*)&msg, mess_getReturnedMessage(RC_MessageID), total);
	UI_PrintLine_Ext(msg, UI_FONT_DOUBLE_REVERSE, UI_ALIGN_LEFT);

	return RET_OK;
}

int Imprimir_header_report (void){
//	char fechaC[40];
	char rif[20];
	DATE fecha;
	DATE TranDateTime;
	char DateTime[20];
	char fechac[30];
	char hora[30];
	TLV_TREE_NODE node;
	int RetFun;
	char FullSerialNumberRaw[8+1];
	char npos[40];


	UI_OpenPrinter_Ext();

//	memset(fechaC, 0, 40);
//	Telium_Sprintf(fechaC,"Fecha contable: %s",(char *)&tTerminal.FCont);
//	UI_PrintLine_Ext(fechaC, UI_FONT_SMALL, UI_ALIGN_LEFT);

	UI_PrintLine_Ext(tTerminal.NameC, UI_FONT_SMALL, UI_ALIGN_CENTER);

	memset(rif, 0, 20);
	Telium_Sprintf(rif,"R.I.F: %s",&tTerminal.RifC);
	UI_PrintLine_Ext(rif, UI_FONT_SMALL, UI_ALIGN_CENTER);

	node = TlvTree_New(0);
	Telium_Read_date(&fecha);
	RetFun = Tlv_SetTagValue(node, TAG_DATE_TIME, &fecha, sizeof(fecha));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);


		// get transaction date and time
		memset((char*)&TranDateTime, 0, sizeof(DATE));
		RetFun = Tlv_GetTagValue(node, TAG_DATE_TIME, &TranDateTime, NULL);

		memset(DateTime, 0, 20);
		memcpy(DateTime, TranDateTime.day, 2);
		DateTime[2] = '/';
		memcpy(&DateTime[3], TranDateTime.month, 2);
		DateTime[5] = '/';
		memcpy(&DateTime[6], TranDateTime.year, 2);
		Telium_Sprintf(fechac,"FECHA: %s",DateTime);
		UI_PrintLine_Ext(fechac, UI_FONT_SMALL, UI_ALIGN_CENTER);

		memset(DateTime, 0, 20);
		memcpy(DateTime, TranDateTime.hour, 2);
		DateTime[2] = ':';
		memcpy(&DateTime[3], TranDateTime.minute, 2);
		DateTime[5] = ':';
		memcpy(&DateTime[6], TranDateTime.second, 2);
		Telium_Sprintf(hora,"HORA: %s",DateTime);
		UI_PrintLine_Ext(hora, UI_FONT_SMALL, UI_ALIGN_CENTER);

	memset(npos, 0, 20);
	memset(FullSerialNumberRaw, 0x00, sizeof(FullSerialNumberRaw));
	PSQ_Give_Serial_Number((char *)FullSerialNumberRaw);
	Telium_Sprintf(npos,"S/N POS: %s ",FullSerialNumberRaw);
	UI_PrintLine_Ext(npos, UI_FONT_SMALL, UI_ALIGN_CENTER);
	UI_PrintLine_Ext("REPORTE PARAMETROS", UI_FONT_SMALL, UI_ALIGN_CENTER);

	UI_ClosePrinter_Ext();

return RET_OK;

LBL_TREE_ERROR:
UI_ShowMessage(NULL, "FECHA NO ENCONTRADA", NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
return RET_NOK;


}

int Reporte_Terminal(void){
	char NBcanco[30];
	//char Ncopias[30];
	char Moneda[30];
	//char fechaC[30];


	UI_OpenPrinter_Ext();
	UI_PrintLine_Ext("TERMINAL", UI_FONT_SMALL, UI_ALIGN_CENTER);

	Telium_Sprintf(NBcanco,"NOMBRE BANCO: %s ",tTerminal.Header_Line1);
	UI_PrintLine_Ext(NBcanco, UI_FONT_SMALL, UI_ALIGN_LEFT);

//	comer = atol(tTerminal.Comercio);

	switch(tTerminal.Comercio){
	case 1:
		UI_PrintLine_Ext("APLIC: COMERCIO", UI_FONT_SMALL, UI_ALIGN_LEFT);
		break;
	case 2:
		UI_PrintLine_Ext("APLIC: RESTAURANTE", UI_FONT_SMALL, UI_ALIGN_LEFT);
		break;
	default:
		UI_PrintLine_Ext("APLIC: COMERCIO", UI_FONT_SMALL, UI_ALIGN_LEFT);
		break;
	}

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECEIPT)){
		UI_PrintLine_Ext("IMPRIME: NO", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}else{
		UI_PrintLine_Ext("IMPRIME: SI", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRE_PRINT)){
		UI_PrintLine_Ext("PRE-IMPRIME: NO", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}else{
		UI_PrintLine_Ext("PRE-IMPRIME: SI", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_DEBITO)){
		UI_PrintLine_Ext("IMPRIME DEBITO: NO", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}else{
		UI_PrintLine_Ext("IMPRIME DEBITO: SI", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_CREDITO)){
		UI_PrintLine_Ext("IMPRIME CREDITO: NO", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}else{
		UI_PrintLine_Ext("IMPRIME CREDITO: SI", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_RECHAZADO)){
		UI_PrintLine_Ext("IMPRIME RECHAZADO: NO", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}else{
		UI_PrintLine_Ext("IMPRIME RECHAZADO: SI", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}

	if(!(tTerminal.Flags1 & TERM_FLAG1_PRINT_LOGO)){
		UI_PrintLine_Ext("IMPRIME LOGO: NO", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}else{
		UI_PrintLine_Ext("IMPRIME LOGO: SI", UI_FONT_SMALL, UI_ALIGN_LEFT);
	}

	//Telium_Sprintf(Ncopias,"No COPIAS: %s",tTerminal.NCopias);
	//UI_PrintLine_Ext(Ncopias, UI_FONT_SMALL, UI_ALIGN_LEFT);

	Telium_Sprintf(Moneda,"MONEDA: %s",tTerminal.CurrSymbol_Local);
	UI_PrintLine_Ext(Moneda, UI_FONT_SMALL, UI_ALIGN_LEFT);

	//Telium_Sprintf(fechaC,"Fecha contable: %s",(char *)&tTerminal.FCont);
	//UI_PrintLine_Ext(fechaC, UI_FONT_SMALL, UI_ALIGN_LEFT);

	UI_PrintLineFeed_Ext(2);
	UI_ClosePrinter_Ext();
	return RET_OK;
}

int Reporte_HOST(void){
	TABLE_MERCHANT Merchant;
	int RetFun,Ret;
	char afiliado[30];
	char terminal[30];
	char maxLote[4+1]= {0};
	char maxLotes[20]= {0};
	TABLE_HOST tHost;
	char inconex[30];
	char timeconex[30];
	char inconex1[30];
	char timeconex1[20];
	//char notlf[30];
	char TPDU[3+1]= {0};
	char TPDU1[16+1]= {0};
//	char NII[3+1];
//	char NII1[8+1];

	UI_OpenPrinter_Ext();
	UI_PrintLine_Ext("HOST", UI_FONT_SMALL, UI_ALIGN_CENTER);


	RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);

		//	do{

				UI_PrintLine_Ext(Merchant.Name, UI_FONT_SMALL, UI_ALIGN_CENTER);

				Telium_Sprintf(afiliado,"No AFILIADO: %s",Merchant.MID);
				UI_PrintLine_Ext(afiliado, UI_FONT_SMALL, UI_ALIGN_LEFT);

				Telium_Sprintf(terminal,"No TERMINAL: %s",Merchant.TID);
				UI_PrintLine_Ext(terminal, UI_FONT_SMALL, UI_ALIGN_LEFT);


					Binasc((uint8*)maxLote,Merchant.BatchNumber,4);
			    	Telium_Sprintf(maxLotes,"No DE LOTE: %s",maxLote);
					UI_PrintLine_Ext(maxLotes, UI_FONT_SMALL, UI_ALIGN_LEFT);

					memset(TPDU, 0x00,11);
					memset(TPDU1, 0x00, 16);
					memcpy(TPDU,&Merchant.TPDU, 10);
					Telium_Sprintf(TPDU1,"TPDU: %s",TPDU);
					UI_PrintLine_Ext(TPDU1, UI_FONT_SMALL, UI_ALIGN_LEFT);


//					memset(NII, 0x00, 4);
//					memset(NII1, 0x00, 9);
//					memcpy(NII,&Merchant.TPDU[3], 3);
//					Telium_Sprintf(NII1,"NII: %s",NII);
//					UI_PrintLine_Ext(NII1, UI_FONT_SMALL, UI_ALIGN_LEFT);


		//	Ret = TM_FindFirst(TAB_HOST, &tHost);
			Ret = TM_FindRecord(TAB_HOST, &tHost, Merchant.HostID);
			memset(inconex1, 0, 15);
		    Binasc((uint8*)inconex1, tHost.ConnectionRetries, 2);
			Telium_Sprintf(inconex,"Intentos de conexion:  %s ",inconex1);
			UI_PrintLine_Ext(inconex, UI_FONT_SMALL, UI_ALIGN_LEFT);

			memset(timeconex1, 0, 15);
		    Binasc((uint8*)timeconex1, tHost.ConnectionTimeOut, 2);
			Telium_Sprintf(timeconex,"Tiempo de conexion:  %s ",timeconex1);
			UI_PrintLine_Ext(timeconex, UI_FONT_SMALL, UI_ALIGN_LEFT);

		//	Telium_Sprintf(notlf,"No telefono      :  %s ",tHost.Dial_Primary_Phone1);
		//	UI_PrintLine_Ext(notlf, UI_FONT_SMALL, UI_ALIGN_LEFT);

		//RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);

		//	}while(RetFun == TM_RET_OK);

			UI_PrintLineFeed_Ext(2);
			UI_ClosePrinter_Ext();


	return RET_OK;

	//LBL_ERROR:
	//		return RET_NOK;

}

int Reporte_COMM(void){


	TABLE_HOST tHost, tHost1;
	TABLE_IP tIP;
	TABLE_MERCHANT Merchant;
	char line1[20];
	char line2[20];
	char line3[20];
	char line4[20];
	char line5[20];
	int Ret,RetFun;

	UI_OpenPrinter_Ext();
	UI_PrintLine_Ext("COMM", UI_FONT_SMALL, UI_ALIGN_CENTER);


	//Ret = TM_FindFirst(TAB_HOST, &tHost);
	Ret = TM_FindRecord(TAB_HOST, &tHost, Merchant.HostID);


	switch(tHost.CommunicationType)
	{
		case LLC_GPRS:
			UI_PrintLine_Ext("TIPO DE COMUNICACION: GPRS", UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line1,"APN:  %s ",tConf.GPRS_APN);
			UI_PrintLine_Ext(line1, UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line2,"USER:  %s ",tConf.GPRS_User);
			UI_PrintLine_Ext(line2, UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line3,"PASS:  %s ",tConf.GPRS_Pass);
			UI_PrintLine_Ext(line3, UI_FONT_SMALL, UI_ALIGN_LEFT);

			Ret = TM_FindFirst(TAB_HOST, &tHost1);
			RetFun = TM_FindRecord(TAB_IP, &tIP, tHost1.IP_Primary_Address1);

			Telium_Sprintf(line4,"IP:  %s ", tIP.IP_GPRS);
			UI_PrintLine_Ext(line4, UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line5,"PORT:  %s ", tIP.PORT_GPRS);
			UI_PrintLine_Ext(line5, UI_FONT_SMALL, UI_ALIGN_LEFT);
		break;

		case LLC_WIFI:
		case LLC_PCL:
		case LLC_IP:

//if(tHost.CommunicationType == LLC_GPRS){

//}else{
			UI_PrintLine_Ext("TIPO DE COMUNICACION: WIFI/ETHERNET", UI_FONT_SMALL, UI_ALIGN_LEFT);
			Ret = TM_FindFirst(TAB_HOST, &tHost1);
			RetFun = TM_FindRecord(TAB_IP, &tIP, tHost1.IP_Primary_Address1);

			Telium_Sprintf(line4,"IP:  %s ", tIP.Host);
			UI_PrintLine_Ext(line4, UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line5,"PORT:  %s ", tIP.Port);
			UI_PrintLine_Ext(line5, UI_FONT_SMALL, UI_ALIGN_LEFT);

			if(tIP.Flags1 & IP_FLAG1_ENABLE_SSL)
			{
				UI_PrintLine_Ext("TLS : SI", UI_FONT_SMALL, UI_ALIGN_LEFT);

			}else{
				UI_PrintLine_Ext("TLS : NO", UI_FONT_SMALL, UI_ALIGN_LEFT);

			}

//}
			break;
		case LLC_DIAL:

			UI_PrintLine_Ext("TIPO DE COMUNICACION: DIAL", UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line1,"TLF 1:  %s ",tHost.Dial_Primary_Phone1);
			UI_PrintLine_Ext(line1, UI_FONT_SMALL, UI_ALIGN_LEFT);

			Telium_Sprintf(line2,"TLF 2:  %s ",tHost.Dial_Primary_Phone2);
			UI_PrintLine_Ext(line2, UI_FONT_SMALL, UI_ALIGN_LEFT);

			switch (tConf.Dial_Tone) {
				case LLC_TONE:
					Telium_Sprintf(line3,"DISCADO: TONO ");
					UI_PrintLine_Ext(line3, UI_FONT_SMALL, UI_ALIGN_LEFT);
					break;
				case LLC_PULSE:
					Telium_Sprintf(line3,"DISCADO: PULSO ");
					UI_PrintLine_Ext(line3, UI_FONT_SMALL, UI_ALIGN_LEFT);
					break;
				default:
					break;
			}

			if (tConf.Dial_DetectTone) {
				Telium_Sprintf(line4,"PRE-DISCADO: SI ");
									UI_PrintLine_Ext(line4, UI_FONT_SMALL, UI_ALIGN_LEFT);
			}else{
				Telium_Sprintf(line4,"PRE-DISCADO: NO ");
				UI_PrintLine_Ext(line4, UI_FONT_SMALL, UI_ALIGN_LEFT);
			}

			Telium_Sprintf(line5,"PREFIJO:  %s ",tConf.Dial_PABX);
			UI_PrintLine_Ext(line5, UI_FONT_SMALL, UI_ALIGN_LEFT);

			break;

			default:
				UI_PrintLine_Ext("TIPO DE COMUNICACION: SIN ASIGNAR", UI_FONT_SMALL, UI_ALIGN_LEFT);

			break;
	}




	UI_PrintLineFeed_Ext(4);
	UI_ClosePrinter_Ext();



	return RET_OK;



}
