/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			Configuration.c
 * Header file:		Configuration.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"
#include "GTL_Convert.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
#define L_Buffer 2000
#define L_ParBuffer 512000

#define INGELOAD_SSL_PROFILE "INGELOAD"

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int16 UnPackMessage(void);
void BuildF0(void);
void BuildF4_ACK(void);
void BuildF5_NAK(void);
void BuildF7_EOT(void);
char GetLRC(uint8* data, const int size);
void XorBuffer (char *DataBuffer, uint32 DataBufferLength, char *EKey);

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
char TechNum[L_TECHID];
static uint8 TXBuffer[L_Buffer];
static int16 TXIndex;
static uint8 RXBuffer[L_Buffer];
static int16 RXIndex;
static int32 BufferSize;
char MsgType;
uint32 PacketNum;
uint32 DataLen;
uint32 BufferLength;
char K1[16];
char SN[16];
char TechPass[L_TECHID];
char LoadBuffer[L_ParBuffer];


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

void IngeLoad_LoadParameters(void)
{
	int RXLen;
	int nResult;
	int NAKs;

	bool Next = TRUE;

	// Clear LoadBuffer
	memset(LoadBuffer,0,L_ParBuffer);
	BufferLength = 0;

	NAKs = 0;

	// Build F0 message
	BuildF0();

	// Configure communication parameters
	//+ HOST01 @mamata Aug 14, 2014
	memset(&LLC_Host, 0, sizeof(LLCOMS_CONFIG));
	LLC_Host.First_Connection.CommType = tConf.iP_CommType;
	switch(tConf.iP_CommType)
	{
		case LLC_GPRS:
			memcpy(LLC_Host.GPRS_APN, tConf.GPRS_APN, strlen(tConf.GPRS_APN));
			memcpy(LLC_Host.GPRS_Usr, tConf.GPRS_User, strlen(tConf.GPRS_User));
			memcpy(LLC_Host.GPRS_Pass, tConf.GPRS_Pass, strlen(tConf.GPRS_Pass));
		case LLC_IP:
		case LLC_WIFI:
			memcpy(LLC_Host.First_Connection.Address1, tConf.iP_Host, strlen(tConf.iP_Host));
			memcpy(LLC_Host.First_Connection.Port1, tConf.iP_Port, strlen(tConf.iP_Port));
			LLC_Host.First_Connection.bSSL1 = tConf.iP_SSL;
			memcpy(LLC_Host.First_Connection.SSLProf1, INGELOAD_SSL_PROFILE, 8);

			memcpy(LLC_Host.First_Connection.Address2, tConf.iP_Host, strlen(tConf.iP_Host));
			memcpy(LLC_Host.First_Connection.Port2, tConf.iP_Port, strlen(tConf.iP_Port));
			LLC_Host.First_Connection.bSSL2 = tConf.iP_SSL;
			memcpy(LLC_Host.First_Connection.SSLProf2, INGELOAD_SSL_PROFILE, 8);
			break;
		case LLC_DIAL:
			memcpy(LLC_Host.First_Connection.Phone1, tConf.iP_Phone1, strlen(tConf.iP_Phone1));
			memcpy(LLC_Host.First_Connection.Phone2, tConf.iP_Phone2, strlen(tConf.iP_Phone2));
			break;
		default:
			UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
							mess_getReturnedMessage(MSG_ERROR_INGELOAD),
							mess_getReturnedMessage(MSG_ERROR_INGELOAD2),
							GL_ICON_ERROR,
							GL_BUTTON_ALL,
							tConf.TimeOut);
			return;

	}
	LLC_Host.First_Connection.Retries = tConf.iP_Retries;
	LLC_Host.First_Connection.TimeoutConn = tConf.iP_ConnectTO;
	LLC_Host.First_Connection.TimeoutAns = tConf.iP_RspTO;
	LLC_Host.First_Connection.bAddHexLength = 1;

	//+ LLCOMMS01 @mamata Jun 18, 2015
	LLC_Host.Dial_Tone = 1;
	LLC_Host.Dial_bDetectTone = tConf.Dial_DetectTone;
	LLC_Host.Dial_bDetectBussy = tConf.Dial_DetectBusy;
	LLC_Host.Dial_ModemMode = tConf.Dial_ModemMode;
	LLC_Host.Dial_BlindDialPause = tConf.Dial_BlindDialPause;
	LLC_Host.Dial_DialToneTO = tConf.Dial_ToneTO;
	LLC_Host.Dial_PABX_Pause = tConf.Dial_PABXPause;
	if(tConf.Dial_PABX[0] != 0)
		memcpy(LLC_Host.Dial_PABX, tConf.Dial_PABX, strlen(tConf.Dial_PABX));
	//- LLCOMMS01

	if( LLC_StartConnection(&LLC_Host) != RET_OK )
	{
		UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
						mess_getReturnedMessage(MSG_ERROR_INGELOAD),
						mess_getReturnedMessage(MSG_ERROR_INGELOAD2),
						GL_ICON_ERROR,
						GL_BUTTON_ALL,
						tConf.TimeOut);
		return;
	}
	//- HOST01

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_IPARAM), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_INIT), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE3, mess_getReturnedMessage(MSG_PLEASE_WAIT), UI_ALIGN_CENTER);
	UI_ShowProcessScreen(mess_getReturnedMessage(MSG_CONNECTING));

	//+ HOST01 @mamata Aug 14, 2014
	//nResult = LLC_WaitConnection(&LLC_Host,TRUE);
	nResult = LLC_WaitConnection(TRUE);
	//- HOST01

	if( nResult == LLC_STATUS_CONNECTED)
	{
		while ( Next )
		{
			// Clear RXBuffer
			memset(RXBuffer,0,L_Buffer);
			RXIndex = 0;

//			nResult = LLC_Send(&LLC_Host,TXBuffer, TXIndex);
//			nResult = LLC_Receive(&LLC_Host,RXBuffer, &RXLen,-1);

					nResult = LLC_Send(TXBuffer, TXIndex);
					nResult = LLC_Receive(RXBuffer, &RXLen);


			RXIndex = 0;

			if ( nResult == RET_OK )
			{
				if ( UnPackMessage() == RET_OK )
				{
					// Good message
					switch ( MsgType )
					{
						case 0xF1: // FILE_BGN
						case 0xF2: // FILE_NXT
						case 0xF3: // FILE_EOF
							// Set NAK counter to 0
							NAKs = 0;
							// Build ACK message
							BuildF4_ACK();
							break;
						case 0xF6: // FILE_END
							NAKs = 0;
							BuildF7_EOT();
							break;
							/*
							BuildF7_EOT();
							// Send
							comSendNoRX((uint8*) TXBuffer, TXIndex);
							comStop();
							Next = FALSE;
							break;
							*/
						case 0xF7:
							/*
							comStop();
							*/
							//LLC_EndConnection(&LLC_Host);
							LLC_EndConnection();

							Next = FALSE;
							break;
						case 0xF8:
							//BuildF7_EOT();
							/*
							// Send
							comSendNoRX((uint8*) TXBuffer, TXIndex);
							comStop();
							dspError("ERROR EN CARGA", "NO EXISTE LID", CENTER, BEEP_ERROR, TIMEOUT_MESSAGE );
							return;
							*/
							//LLC_EndConnection(&LLC_Host);
							LLC_EndConnection();
							Next = FALSE;
							UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
											mess_getReturnedMessage(MSG_ERROR_INGELOAD),
											mess_getReturnedMessage(MSG_ERROR_INGELAOD_NO_TERMINAL),
											GL_ICON_ERROR,
											GL_BUTTON_ALL,
											tConf.TimeOut);

							return;

					}
				}
				else
				{
					// Bad message
					if( NAKs == 2)
					{
						// End comunication
						/*
						comStop();
						*/
					//	LLC_EndConnection(&LLC_Host);
						LLC_EndConnection();
						UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
										mess_getReturnedMessage(MSG_ERROR_INIT),
										mess_getReturnedMessage(MSG_TRY_AGAIN),
										GL_ICON_ERROR,
										GL_BUTTON_ALL,
										tConf.TimeOut);

						return;

						break;
					}
					else
					{
						// Send NAC to Ingeload
						BuildF5_NAK();
						// Increment NAK counter
						NAKs++;
					}
				}

			}
			else
			{
				//LLC_EndConnection(&LLC_Host);
				LLC_EndConnection();
				UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
								mess_getReturnedMessage(MSG_ERROR_COMMUNICATION),
								mess_getReturnedMessage(MSG_TRY_AGAIN),
								GL_ICON_ERROR,
								GL_BUTTON_ALL,
								tConf.TimeOut);

				return;
			}

		}

		UI_ShowProcessScreen(mess_getReturnedMessage(MSG_LOADINGTABLES));
		// Clear all tables
		//tabClearAllIngeLoad();

		// decrypt buffer
		XorBuffer(LoadBuffer,BufferLength,K1);

		// Update parameters
		UnpackBuffer(LoadBuffer,BufferLength);

		/*TODO
		memset((char*) &mnuEnable, 0x00, sizeof(mnuEnable));
		mainEnablesFuncs(&mnuEnable);
		*/

	   #ifdef MAKE_AUTOSETTLE
		//@AJAF Ene2012, programa el evento de auto cierre
		SetAutoSettle(0);
	   #endif //MAKE_AUTOSETTLE

	   #ifdef MAKE_STATISTICS
		SetAutoStats(0);
	   #endif //MAKE_STATISTICS

	   #ifdef MAKE_CLAVE
		fvLoadWK();
	   #endif //MAKE_CLAVE

		if(tConf.iP_Report)
		{
			IngeLoad_PrintReport();
		}

		UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
						mess_getReturnedMessage(MSG_INIT_OK),
						NULL,
						GL_ICON_INFORMATION,
						GL_BUTTON_ALL,
						tConf.TimeOut);

		Tables_Init();

		//+ ADD0002
		//TODO UpadatePagoRapidoFlag();
		//- ADD0002
	}
	else
	{
		UI_ShowMessage(	mess_getReturnedMessage(MSG_IPARAM),
						mess_getReturnedMessage(MSG_ERROR_COMMUNICATION),
						mess_getReturnedMessage(MSG_TRY_AGAIN),
						GL_ICON_ERROR,
						GL_BUTTON_ALL,
						tConf.TimeOut);
	}

	DisplayHeader(_ON_);
	DisplayFooter(_ON_);
};

void BuildF0(void)
{
	uint16 DataLen;
	int i;
	char SNumber[100];
	char TermModel = 0;
	char TmpLID[17];

	memset(TmpLID,0x00,17);

	if(IsICT220())
		TermModel = 1;
	else if(IsICT250())
		TermModel = 2;
	else if(IsIWL220())
		TermModel = 3;
	else if(IsIWL250())
		TermModel = 4;
	else if(IsIWL280())
		TermModel = 5;

	// Get Serial number
	PSQ_Give_Full_Serial_Number((unsigned char*)SNumber);

	// Clear TX buffer
	memset(TXBuffer,0,L_Buffer);

	// Set TX Buffer Index to 0
	TXIndex = 0;

	// Set TPDU
	TXBuffer[TXIndex] = 0x60; TXIndex++;
	Ascii_to_Hex(&TXBuffer[TXIndex], (uint8*)tConf.iP_NII, L_NII); TXIndex+=2;
	TXIndex+=2;

	// Set Message Type
	TXBuffer[TXIndex] = 0xF0; TXIndex++;

	// Set Packet Number
	TXBuffer[TXIndex] = 0x00; TXIndex++;
	TXBuffer[TXIndex] = 0x00; TXIndex++;

	// Set Data Length
	TXIndex++;
	TXIndex++;

	// Set Data
	// Set Tag 01 - Terminal ID
	memcpy(TmpLID, tConf.iP_ID, L_LID);
	memset(&TXBuffer[TXIndex],0x20,L_LID);
	memcpy(&TXBuffer[TXIndex],TmpLID,strlen(TmpLID)); TXIndex+=16;

	// Set Tag 02 - Serial Number
	TXBuffer[TXIndex] = 0x30;
	memcpy(&TXBuffer[TXIndex+1],SNumber,15); TXIndex+=16;
	memcpy(SN,&TXBuffer[TXIndex-16],16);

	// Set Tag 03 - Terminal Model
	TXBuffer[TXIndex] = TermModel; TXIndex++;
	//memcpy(&TXBuffer[TXIndex],"ICT220",6); TXIndex+=6;

	// Set Tag 04 - Application name
	memcpy(&TXBuffer[TXIndex],"ACREINGEMVCLS",13); TXIndex+=13;

	// Set Tag 05 - Application version
	TXBuffer[TXIndex] = 0x30; TXIndex++;
	TXBuffer[TXIndex] = 0x31; TXIndex++;
	TXBuffer[TXIndex] = 0x30; TXIndex++;
	TXBuffer[TXIndex] = 0x33; TXIndex++;

	// Set Tag 06 - Release Date
	TXBuffer[TXIndex] = 0x20; TXIndex++;
	TXBuffer[TXIndex] = 0x14; TXIndex++;
	TXBuffer[TXIndex] = 0x02; TXIndex++;
	TXBuffer[TXIndex] = 0x13; TXIndex++;

	// Set Tag 07 - Protocol version
	TXBuffer[TXIndex] = 0x01; TXIndex++;
	TXBuffer[TXIndex] = 0x11; TXIndex++;
	TXBuffer[TXIndex] = 0x61; TXIndex++;

	// Set Tag 08 - Comm type
	TXBuffer[TXIndex] = tConf.iP_CommType; TXIndex++;
	K1[14] = 0x00;
	K1[15] = TXBuffer[TXIndex-1];

	// Set Tag 09 - Buffer size
	switch( tConf.iP_CommType)
	{
		case 1: // Dial
			TXBuffer[TXIndex] = 0x03; TXIndex++;
			TXBuffer[TXIndex] = 0xE8; TXIndex++;
			BufferSize = 1000;
			break;
		case 2: // Eth
		case 4:
			TXBuffer[TXIndex] = 0x03; TXIndex++;
			TXBuffer[TXIndex] = 0xE8; TXIndex++;
			BufferSize = 2000;
			break;
		case 3: // GPRS
			TXBuffer[TXIndex] = 0x01; TXIndex++;
			TXBuffer[TXIndex] = 0xF4; TXIndex++;
			BufferSize = 500;
			break;
	}

	// Set Tag 10 - Table version
	TXBuffer[TXIndex] = 0x01; TXIndex++;
	TXBuffer[TXIndex] = 0x11; TXIndex++;
	TXBuffer[TXIndex] = 0x61; TXIndex++;

	// Set Tag 11 - Current date
	/*
	Telium_Read_date
	psyDateTimeGet(&systemDate, &systemTime);
	sprintf( Date, "%d%d%02d", PSY_YEAR(systemDate), PSY_MONTH(systemDate), PSY_DAY(systemDate) );
	msaAscToHex(Date,&TXBuffer[TXIndex],0x30,ISO_RIGHT);
	TXIndex+=4;
	*/
	TXBuffer[TXIndex] = 0x20; TXIndex++;
	TXBuffer[TXIndex] = 0x24; TXIndex++;
	TXBuffer[TXIndex] = 0x05; TXIndex++;
	TXBuffer[TXIndex] = 0x13; TXIndex++;

	// Set Tag 12 - Current time
	/*
	sprintf( Hour, "%02d%02d%02d", (uint16)PSY_HOUR(systemTime), (uint16)PSY_MIN(systemTime), (uint16)PSY_SEC(systemTime) );
	msaAscToHex(Hour,&TXBuffer[TXIndex],0x30,ISO_RIGHT);
	TXIndex+=3;
	*/
	TXBuffer[TXIndex] = 0x09; TXIndex++;
	TXBuffer[TXIndex] = 0x13; TXIndex++;
	TXBuffer[TXIndex] = 0x11; TXIndex++;


	Hexasc((uint8*)K1, &TXBuffer[TXIndex-7], 7*2);

    for (i = 0; i < 16; i++)
        K1[i] ^= SN[i];

	// Set Tag 13 - Technician
    //TODO GetTechPass(TechPass);
	memcpy(&TXBuffer[TXIndex],TechPass,6); TXIndex+= 6;

	// Set DataLent
	DataLen = TXIndex - 10;
	GTL_Convert_UlToBinNumber(DataLen, &TXBuffer[8], 2);

	// Set LRC
	TXBuffer[TXIndex] = GetLRC(&TXBuffer[5],TXIndex); TXIndex++;
	//TXBuffer[TXIndex] = 10; TXIndex++;
}

void BuildF4_ACK(void)
{
	memset(TXBuffer,0,L_Buffer);

	// Set TX Buffer Index to 0
	TXIndex = 0;

	// Set TPDU
	TXBuffer[TXIndex] = 0x60; TXIndex++;
	Ascii_to_Hex(&TXBuffer[TXIndex], (uint8*)tConf.iP_NII, L_NII); TXIndex+=2;
	TXIndex+=2;

	// Set Message Type
	TXBuffer[TXIndex] = 0xF4; TXIndex++;

	// Set Packet Number
	TXBuffer[TXIndex] = 0x00; TXIndex++;
	TXBuffer[TXIndex] = 0x00; TXIndex++;

	// Set Data Length
	TXIndex++;
	TXIndex++;

	// Set LRC
	TXBuffer[TXIndex] = GetLRC(&TXBuffer[5],TXIndex); TXIndex++;
}

void BuildF5_NAK(void)
{
	memset(TXBuffer,0,L_Buffer);

	// Set TX Buffer Index to 0
	TXIndex = 0;

	// Set TPDU
	TXBuffer[TXIndex] = 0x60; TXIndex++;
	Ascii_to_Hex(&TXBuffer[TXIndex], (uint8*)tConf.iP_NII, L_NII); TXIndex+=2;
	TXIndex+=2;

	// Set Message Type
	TXBuffer[TXIndex] = 0xF5; TXIndex++;

	// Set Packet Number
	TXBuffer[TXIndex] = 0x00; TXIndex++;
	TXBuffer[TXIndex] = 0x00; TXIndex++;

	// Set Data Length
	TXIndex++;
	TXIndex++;

	// Set LRC
	TXBuffer[TXIndex] = GetLRC(&TXBuffer[5],TXIndex); TXIndex++;
}

void BuildF7_EOT(void)
{
	memset(TXBuffer,0,L_Buffer);

	// Set TX Buffer Index to 0
	TXIndex = 0;

	// Set TPDU
	TXBuffer[TXIndex] = 0x60; TXIndex++;
	Ascii_to_Hex(&TXBuffer[TXIndex], (uint8*)tConf.iP_NII, L_NII); TXIndex+=2;
	TXIndex+=2;

	// Set Message Type
	TXBuffer[TXIndex] = 0xF7; TXIndex++;

	// Set Packet Number
	TXBuffer[TXIndex] = 0x00; TXIndex++;
	TXBuffer[TXIndex] = 0x00; TXIndex++;

	// Set Data Length
	TXIndex++;
	TXIndex++;

	// Set LRC
	TXBuffer[TXIndex] = GetLRC(&TXBuffer[5],TXIndex); TXIndex++;
}

int16 UnPackMessage(void)
{
	char RXLRC;
	char LocalLRC;
	char Msg[50];
	int Paquete;

	RXIndex = 0;

	// Get TPDU
	RXIndex+=5;

	// Get MsgType
	MsgType = RXBuffer[RXIndex]; RXIndex++;

	// Get Packet number
	GTL_Convert_BinNumberToUl(&RXBuffer[RXIndex], &PacketNum, 2); RXIndex+=2;

	// Get DataLen
	GTL_Convert_BinNumberToUl(&RXBuffer[RXIndex], &DataLen, 2); RXIndex+=2;

	RXIndex+=DataLen;

	// Get RX LCR
	RXLRC = RXBuffer[RXIndex]; RXIndex++;
	LocalLRC = GetLRC(&RXBuffer[5],RXIndex - 6);
	// If RX LRC is different to local LRC return bad message
	if (RXLRC != LocalLRC)
		return RET_NOK;

	memcpy(&LoadBuffer[BufferLength],&RXBuffer[10], DataLen);
	BufferLength+=DataLen;

	Paquete = PacketNum;
	memset(Msg,0x00,50);

	sprintf(Msg,"%s: %d", mess_getReturnedMessage(MSG_PACKAGE), Paquete);
	UI_ShowProcessScreen(Msg);

	return RET_OK;
}

char GetLRC(uint8* data, const int size)
{
	char sum = 0;
	int l = size;
	while (l != 0)
	{
		sum ^= *data;
		data++;
		l--;
	}
	return sum;
}


void XorBuffer (char *DataBuffer, uint32 DataBufferLength, char *EKey)
{
	int i = 0;
	int j = 0;
	int jtemp;

	while (j < DataBufferLength)
	{
		if ((j + 16) < DataBufferLength)
		{
			for (i = 0; i < 16; i++)
			{
				DataBuffer[j] ^= EKey[i];
				j++;
			}
		}
		else
		{
			jtemp = j;
			for (i = 0; i < (DataBufferLength - jtemp); i++)
			{
				DataBuffer[j] ^= EKey[i];
				j++;
			}
		}
	}
}
