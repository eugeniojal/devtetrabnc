/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Tran_ECHO_TEST.c
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
//+ ECHOTEST @mamata Aug 15, 2014
uint16 TRAN_STEPS_TEST[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP,
	STEP_SELECT_MERCHANT_BY_TRAN,
	//STEP_PREDIAL,
	STEP_SEND_RECEIVE,
	STEP_PROCESS_RESPONSE,
	STEP_END_COMMUNICATION,
	STEP_SHOW_RESULT,
	STEP_END_LIST
};

uint16 TRAN_STEPS_TEST_AUTO[] =
{
	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP,
	STEP_SELECT_MERCHANT_BY_TRAN,
	//STEP_PREDIAL,
	STEP_SEND_RECEIVE,
//	STEP_PROCESS_RESPONSE,
	STEP_END_COMMUNICATION,
//	STEP_SHOW_RESULT,
	STEP_END_LIST
};

uint16 TRAN_STEPS_LOGON[] =
{

	STEP_CHECK_TERMINAL,
	STEP_SELECT_MERCH_GROUP,
	STEP_SELECT_MERCHANT_BY_TRAN,
	//STEP_PREDIAL,
	STEP_SEND_RECEIVE,
	STEP_PROCESS_RESPONSE,
	STEP_END_COMMUNICATION,
	STEP_SHOW_RESULT,
	STEP_END_LIST
};

uint16 STEPS_DUPLICATE[] =
{
		STEP_CHECK_TERMINAL,
		STEP_SELECT_MERCH_GROUP,
		STEP_SELECT_MERCHANT_BY_TRAN,
		STEP_INVOICE,
		STEP_PRINT_RECEIPT,
		STEP_END_LIST
};

uint16 STEPS_LAST_ANSWER[] =
{
		STEP_CHECK_TERMINAL,
		STEP_SELECT_MERCH_GROUP,
		STEP_SELECT_MERCHANT_BY_TRAN,
		STEP_LAST_ANSWER,
		STEP_PRINT_RECEIPT,
		STEP_END_LIST
};
//- ECHOTEST

