/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			PclUtil.c
 * Header file:		PclUtil.h
 * Description:
 * Author:			@lcalvano
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/

typedef struct
{
	char *Tag;
	int PclTreeTag;
	int (*pfConvert)(int nTreeTag, char *data );
}XmlParser_ST;

TLV_TREE_NODE stPclTransFields;

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
XmlParser_ST *psMainParameter;
char *cpCurrentTag;

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/

/*!
 * Configures the external device as Poxy server for the Telium
 * Device to connect with the remote servers over PCL link
 *
 * @return
 * 0 PROXY_SET_PARAMETER_OK.
 * -1 FUNCTION_NOT_IMPLEMENTED
 * -2 PROXY_BAD_PARAMETERS.
 */
int PclUtil_SetProxySettings()
{
	socks_param_s stSocksParam;

	stSocksParam.proxy_address = 0x01010101;
	stSocksParam.proxy_port = 1080;
	stSocksParam.proxy_type = 5;

	#if(TETRA_HARDWARE == 1)
	return PROXY_BAD_PARAMETERS;
	#else
	return HWCNF_SetProxyParameter(&stSocksParam);
	#endif //TETRA_HARDWARE

}

void PclUtil_openManagerBTConfig(void)
{
	#if(TETRA_HARDWARE == 1)
	#else
	if(IsBT())
		HWCNF_Shortcut_ConfigureBluetoothSettings();
	#endif//TETRA_HARDWARE
}


/*!
 * Dispositivo PCL pide la firma.
 * La firma no regresa a la aplicación de POS
 *
 * @return
 */
int PclUtil_sendSignatureRequest(void)
{
	int iRet = 0;
	T_CAPTURE_SETTINGS pSettings;

	pSettings.us_screen_posx = 100;
	pSettings.us_screen_posy = 100;
	pSettings.us_screen_width = 376;//380;
	pSettings.us_screen_height = 235;//240;
	pSettings.us_timeout = 80;

	iRet = PDA_catchSignature( &pSettings );
	return iRet;
}

/*!
 * Dispositivo PCL pide la firma.
 * La firma es regresada en un buffer BMP al POS.
 *
 * @param pucBmpBuffer
 * @param pBmpSize
 * @return
 */
int PclUtil_getSignature(uchar* pucBmpBuffer, uint* pBmpSize)
{
	int iRet = 0;
	T_CAPTURE_SETTINGS pSettings;

	pSettings.us_screen_posx = 100;
	pSettings.us_screen_posy = 100;
	pSettings.us_screen_width = 90;//376;//380;
	pSettings.us_screen_height = 60;//235;//240;
	pSettings.us_timeout = 80;

	iRet = PDA_getSignature( &pSettings, pucBmpBuffer, BMP_IMAGE_SIZE, pBmpSize);
	return iRet;
}


/******************************* PRINTER ********************************/

static IPA_JOB_ID printerJobId = NULL;

/*!
 * Start the PCL printer.
 *
 * @param receiptType
 * @return
 */
int PclUtil_startPrinter(uchar receiptType)
{
	int iRet = KO;
	if (IPA_printer_open(&printerJobId) == OK)
		iRet = PCL_printer_startReceipt(printerJobId, receiptType);

	return iRet;
}

/*!
 * Ends the PCL printer
 */
void PclUtil_endPrinter(void)
{
	if(printerJobId == NULL)
		return;

	PCL_printer_endReceipt(printerJobId);
	IPA_printer_close(printerJobId);
	printerJobId = NULL;
}

/*!
 * Adds a text line to print.
 *
 * @param strText
 * @param justification
 */
void PclUtil_addPrinterText(char *strText, uchar justification)
{
	if(printerJobId == NULL)
		return;

	IPA_printer_setTextJust(printerJobId, justification);
	IPA_printer_printText(printerJobId, strText);
	IPA_printer_printText(printerJobId,  "\n");
}

/*!
 * Feed paper the number of indicated lines
 * @param noLines
 */
void PclUtil_feedPaper(int noLines)
{
	if(printerJobId == NULL)
		return;

	IPA_printer_feedPaper(printerJobId, noLines);
}

/**
 * Adds the captured signature
 */
void PclUtil_addSignature(void)
{
	if(printerJobId == NULL)
		return;

	PCL_printer_addSignature(printerJobId);
}

/*!
 * adds a BMP image to PCL printer.
 *
 * @param BMPBuffImage
 */
void PclUtil_addBMPImage(uchar *BMPBuffImage)
{
	unsigned long widthBMP = 0;
	unsigned long hightBMP = 0;
	unsigned short BitCount = 0;

	if(BMPBuffImage == NULL)
		return;
	if(printerJobId == NULL)
		return;

	GetBmpInfos(BMPBuffImage, &widthBMP, &hightBMP, &BitCount);
	IPA_printer_printImage ( printerJobId, widthBMP - 1, hightBMP - 1, BMPBuffImage);
}

/*!
 * Adds a Bold text line to print
 * @param strText
 * @param justification
 */
void PclUtil_addPrinterBoldText(char *strText, uchar justification)
{
	if(printerJobId == NULL)
		return;

	IPA_printer_setFont(printerJobId, 1);
	PclUtil_addPrinterText(strText, justification);
	IPA_printer_setFont(printerJobId, 0);
}

/*!
 * Adds a Condensed text line to print
 * @param strText
 * @param justification
 */
void PclUtil_addPrinterCondensedText(char *strText, uchar justification)
{
	char *strNewText = umalloc(sizeof(char)*(strlen(strText)+5));
	Telium_Sprintf(strNewText, "\xF" "%s", strText);
	PclUtil_addPrinterText(strNewText, justification);
	ufree(strNewText);
}


int ConvertOperationTypePCL(int TranType )
{
	int nTransType = TRAN_SALE;

	switch (TranType)
	{
	case DEBIT_TR:
		nTransType = TRAN_SALE;
		break;
	case CREDIT_TR:
		nTransType = TRAN_REFUND;
		break;
	case CANCEL_TR:
		nTransType = TRAN_VOID;
		break;

	case EXT_CANCEL_TR:
		break;
	case PRE_AUTHO_TR:
		break;
//		OPERATION_CANCEL
//		OPERATION_CREDIT
//		OPERATION_DEBIT
//		OPERATION_DUPLICATE
	default:
		return RET_KO;
	}

	Tlv_SetTagValueInteger(stPclTransFields, PCL_OPERATION_TYPE, nTransType, sizeof(int));

	return RET_OK;
}

int ConvertOperationTypeTLV(int TranType )
{
	int nTransType = TRAN_SALE;

	switch (TranType)
	{
	case PDA_RQ_PURCHASE_TXN:
		nTransType = TRAN_SALE;
		break;

	case PDA_RQ_REFUND_TXN:
		nTransType = TRAN_REFUND;
		break;
	case PDA_RQ_CONSULTATION:
		nTransType = TRAN_WORKINGKEYS;
		break;
	case PDA_RQ_REVERSAL_TXN :
			nTransType = TRAN_VOID;
		break;

	case PDA_RQ_CLEARING:
		nTransType = TRAN_SETTLE;
		break;

	case PDA_RQ_LAST_TXN_CHECK:
	case PDA_RQ_LAST_TXN_RECEIPT:
	case PDA_RQ_CONSULTATION_RECEIPT:
	case PDA_RQ_CUSTOMER_RQ:
		nTransType = TRAN_LASTSALE;
		break;

	case PDA_RQ_TIPABLE_TXN:
	default:
		return RET_KO;
	}

	Tlv_SetTagValueInteger(stPclTransFields, PCL_OPERATION_TYPE, nTransType, sizeof(int));

	return RET_OK;
}





int PclUtil_ValiateAmounts()
{
	amount lTotal = 0;
	amount lBase = 0;
	amount lTip = 0;
	amount lTax1 = 0;
	amount lTax2 = 0;
	amount lAdditional = 0;

	Tlv_GetTagValue(stPclTransFields, PCL_TOTAL_AMOUNT, &lTotal, NULL);
	Tlv_GetTagValue(stPclTransFields, PCL_BASE_AMOUNT, &lBase, NULL);

	if(Tlv_GetTagValue(stPclTransFields, PCL_TIP_AMOUNT, &lTip, NULL) == RET_OK)
		Tlv_SetTagValue(stPclTransFields, PCL_TIP_AMOUNT, &lTip, sizeof(amount));

	if(Tlv_GetTagValue(stPclTransFields, PCL_TAX1_AMOUNT, &lTax1, NULL) == RET_OK)
		Tlv_SetTagValue(stPclTransFields, PCL_TAX1_AMOUNT, &lTax1, sizeof(amount));

	if(Tlv_GetTagValue(stPclTransFields, PCL_TAX2_AMOUNT, &lTax2, NULL) == RET_OK)
		Tlv_SetTagValue(stPclTransFields, PCL_TAX2_AMOUNT, &lTax2, sizeof(amount));

	if(Tlv_GetTagValue(stPclTransFields, PCL_ADDITIONAL_AMOUNT, &lAdditional, NULL) == RET_OK)
		Tlv_SetTagValue(stPclTransFields, PCL_ADDITIONAL_AMOUNT, &lAdditional, sizeof(amount));

	// these validations are host dependent
	if ((lTax1)||(lTax2))
	{
		UI_ShowMessage("ERROR COMANDO", "MONTO IMPUESTO", "NO APLICA", GL_ICON_ERROR, GL_BUTTON_ALL, 3);
		return FALSE;
	}
	if (lTotal && !lBase && !lTip && !lTax1 && !lTax2 && !lAdditional)
	{
		// ECR only sent total amount and ommited the rest, in this case total = base
		lBase = lTotal;
		Tlv_SetTagValue(stPclTransFields, PCL_BASE_AMOUNT, &lBase, sizeof(amount));
	}

	if (lTotal == (lBase + lTip + lAdditional))
	{
		return TRUE;
	}
	else
	{
		UI_ShowMessage("ERROR COMANDO", "MONTO TOTAL", "INCORRECTO", GL_ICON_ERROR, GL_BUTTON_ALL, 3);
		return FALSE;
	}
}


//------------------------------------------------------------------------------
int PclUtil_ParseTLVExtendedData(T_SERVICE_CALL_PARAM_PDA_START_TRANSACTION *pParams)
{
	T_BER_TLV_DECODE_STRUCT stBerTlvStruct;
	BER_TLV_TAG	ReadTag;
	BER_TLV_LENGTH ReadLength;
	BER_TLV_VALUE ReadValue;
	amount Total;
	amount TempAmount;
	uint64 nNumber;
	unsigned int nRet;
	int nBytesRead;
	char cpTemp[200];

	Total  = pParams->m_sDebitEmv.param_in.amount;
	nRet = Tlv_SetTagValue(stPclTransFields, PCL_TOTAL_AMOUNT, &Total, sizeof(amount));

	nRet = GTL_BerTlvDecode_Init(&stBerTlvStruct, &pParams->m_ucData, pParams->m_nSize);

	while(!(nRet = GTL_BerTlvDecode_ParseTlv(&stBerTlvStruct, &ReadTag, &ReadLength, &ReadValue, 0, &nBytesRead)))
	{
		switch(ReadTag)
		{
		case PCL_TAG_REQUEST_CONTAINER:
			nRet = GTL_BerTlvDecode_Init(&stBerTlvStruct, ReadValue, nBytesRead);
			break;
		case PCL_TAG_REQUEST_TYPE:
			ConvertOperationTypeTLV(*(byte*)ReadValue);
			break;
		case PCL_TAG_REQUEST_REFERENCE:
			Tlv_SetTagValue(stPclTransFields, PCL_TRANS_ID, (void*)ReadValue, nBytesRead);
			break;
		case PCL_TAG_RETRIEVAL_REFERENCE_NUMBER:
			Telium_Sprintf(cpTemp, "%*.*s", nBytesRead - 4, nBytesRead - 4, ReadValue);
			Tlv_SetTagValueString(stPclTransFields, PCL_RRN, cpTemp);
			break;
		case PCL_TAG_BASE_AMOUNT:
			convBcdToInt(ReadValue, nBytesRead - 4, &TempAmount);
			Tlv_SetTagValue(stPclTransFields, PCL_BASE_AMOUNT, &TempAmount, sizeof(amount));
			break;
		case PCL_TAG_TIP_AMOUNT:
			convBcdToInt(ReadValue, nBytesRead - 4, &TempAmount);
			Tlv_SetTagValue(stPclTransFields, PCL_TIP_AMOUNT, &TempAmount, sizeof(amount));
			break;
		case PCL_TAG_TAX1_AMOUNT:
			convBcdToInt(ReadValue, nBytesRead - 4, &TempAmount);
			Tlv_SetTagValue(stPclTransFields, PCL_TAX1_AMOUNT, &TempAmount, sizeof(amount));
			break;
		case PCL_TAG_TAX2_AMOUNT:
			convBcdToInt(ReadValue, nBytesRead - 4, &TempAmount);
			Tlv_SetTagValue(stPclTransFields, PCL_TAX2_AMOUNT, &TempAmount, sizeof(amount));
			break;
		case PCL_TAG_ADDITIONAL_AMOUNT:
			convBcdToInt(ReadValue, nBytesRead - 4, &TempAmount);
			Tlv_SetTagValue(stPclTransFields, PCL_ADDITIONAL_AMOUNT, &TempAmount, sizeof(amount));
			break;
		case PCL_TAG_EXTERNAL_DEVICE_ID:
			Telium_Sprintf(cpTemp, "%*.*s", nBytesRead - 4, nBytesRead - 4, ReadValue);
			Tlv_SetTagValueString(stPclTransFields, PCL_EXTERNAL_DEVICE_ID, cpTemp);
			break;
		case PCL_TAG_NUMBER_OF_INSTALLMENTS:
			convBcdToInt(ReadValue, nBytesRead - 4, &nNumber);
			Tlv_SetTagValueInteger(stPclTransFields, PCL_NUM_INSTALLMENTS, nNumber, sizeof(int));
			break;
		case PCL_TAG_INSTALLEMENTS_START_DATE:
			Telium_Sprintf(cpTemp, "%2.2d%2.2d%2.2d", ReadValue[0], ReadValue[1], ReadValue[2]);
			Tlv_SetTagValueString(stPclTransFields, PCL_INSTALLMENTS_START_DATE, cpTemp);
			break;
		case PCL_TAG_INSTALLMENTS_INTEREST_FLAG:
			convBcdToInt(ReadValue, nBytesRead - 4, &nNumber);
			Tlv_SetTagValueInteger(stPclTransFields, PCL_INSTALLMENTS_INTEREST_FLAG, nNumber, sizeof(int));
			break;
		case PCL_TAG_INVOICE_NUMBER:
			Telium_Sprintf(cpTemp, "%*.*s", nBytesRead - 4, nBytesRead - 4, ReadValue);
			Tlv_SetTagValueString(stPclTransFields, PCL_INVOICE, cpTemp);
			break;
		case PCL_TAG_TICKET_NUMBER:
			Telium_Sprintf(cpTemp, "%*.*s", nBytesRead - 4, nBytesRead - 4, ReadValue);
			Tlv_SetTagValueString(stPclTransFields, PCL_TICKET_NUMBER, cpTemp);
			break;
		case PCL_TAG_ACCOUNT_TYPE:
			convBcdToInt(ReadValue, nBytesRead - 4, &nNumber);
			nNumber++; // to adjust the correct value
			Tlv_SetTagValueInteger(stPclTransFields, PCL_ACCOUNT_TYPE, nNumber, sizeof(int));
			break;
		case PCL_TAG_CASHIER_NUMBER:
			Telium_Sprintf(cpTemp, "%*.*s", nBytesRead - 4, nBytesRead - 4, ReadValue);
			Tlv_SetTagValueString(stPclTransFields, PCL_CASHIER_NUMBER, cpTemp);
			break;
		}

		memset(cpTemp, 0, sizeof(cpTemp));
	}

	return RET_OK;
}


int PclUtil_BerTlvAdd(T_BER_TLV_ENCODE_STRUCT *stpTlvStructure, BER_TLV_TAG nTag, byte *pData, int nSize)
{
	int nRet;

	nRet = GTL_BerTlvEncode_EncodeTag(stpTlvStructure, nTag);
	if (nRet)
		return nRet;

	nRet = GTL_BerTlvEncode_EncodeLength(stpTlvStructure, nSize);
	if (nRet)
		return nRet;

	nRet = GTL_BerTlvEncode_EncodeValue(stpTlvStructure, pData, nSize);

	return nRet;
}
//-------------------------------------------------------------------------
int PclUtil_TxErrorBuildResponse(T_BER_TLV_ENCODE_STRUCT *stFieldsTlv)
{
	T_BER_TLV_ENCODE_STRUCT stSubFieldsTlv;
	TABLE_MERCHANT 	tMerchant;

	byte tempBuffer[1000];
	char strFieldValue[3000];

	int RetFun;
	unsigned int MxLn;

	//<!---------------------------------------------------------- DFFF34 - Additional Data container
	RetFun = GTL_BerTlvEncode_Init(&stSubFieldsTlv, tempBuffer, sizeof(tempBuffer));

	// Response code
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_PI_TRANS_ERROR, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_RESPONSE_CODE, (byte*)strFieldValue, MxLn);

	//Merchant Name
	RetFun = Get_TransactionMerchantData(&tMerchant, Tree_Tran);
	if(RetFun == RET_OK)
	{
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_MERCHANT_NAME, (byte*)tMerchant.Name, strlen(tMerchant.Name));
	}

	//Terminal ID
	if(tMerchant.TID[0] != NULL)
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_TERMINAL_ID, (byte*)tMerchant.TID, strlen(tMerchant.TID));

	//--------- Add to container

	RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_ADDITIONAL_DATA_CONTAINER, stSubFieldsTlv.pBerTlvData, stSubFieldsTlv.nIndex);

	//----------------------------------------------------------!> DFFF34 - Additional Data container

	//Merchant ID
	if(tMerchant.MID[0] != NULL)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_MERCHANT_ID, (byte*)tMerchant.MID, strlen(tMerchant.MID));

	//Terminal ID
	//--- DFFF34 - DF25

	return RET_OK;
}

//-------------------------------------------------------------------------
int PclUtil_TxSaleBuildResponse(T_BER_TLV_ENCODE_STRUCT *stFieldsTlv)
{
	T_BER_TLV_ENCODE_STRUCT stSubFieldsTlv;
	TABLE_MERCHANT 	tMerchant;
	TABLE_RANGE		tRange;

	byte tempBuffer[1000];
	char strFieldValue[3000];
	char strFieldValueAux[100];
	amount AmountValue = 0;
	DATE stDate;

	uint32 IntValue = 0;

	int RetFun;
	unsigned int MxLn;

	//<!---------------------------------------------------------- DFFF34 - Additional Data container
	RetFun = GTL_BerTlvEncode_Init(&stSubFieldsTlv, tempBuffer, sizeof(tempBuffer));

	// Response code
	strcpy(strFieldValue, "00");
	RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_RESPONSE_CODE, (byte*)strFieldValue, strlen(strFieldValue));

	//Merchant Name
	RetFun = Get_TransactionMerchantData(&tMerchant, Tree_Tran);
	if(RetFun == RET_OK)
	{
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_MERCHANT_NAME, (byte*)tMerchant.Name, strlen(tMerchant.Name));
	}

	//Issuer Name & abbreviation
	RetFun = Get_TransactionRangeData(&tRange, Tree_Tran);
	if(RetFun == RET_OK)
	{
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_RANGE_NAME, (byte*)tRange.Name, strlen(tRange.Name));
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_RANGE_ID, (byte*)tRange.ShortDescriptor, strlen(tRange.ShortDescriptor));
	}

	//Installment amount
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_INSTALLMENT_VALUE, &AmountValue, NULL);
	if(RetFun == RET_OK)
	{
		GTL_Convert_UllToDcdNumber(AmountValue, strFieldValue, 6);
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_INSTALLMENT_AMOUNT, (byte*)strFieldValue, 6);
	}

	//Terminal ID
	if(tMerchant.TID[0] != NULL)
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_TERMINAL_ID, (byte*)tMerchant.TID, strlen(tMerchant.TID));

	//Countable day
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_COUNTABLE_DATE, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_COUNTABLE_DATE, (byte*)strFieldValue, MxLn);

	//Account number
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_ACCOUNT_NUMBER, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_ACCOUNT_NUMBER, (byte*)strFieldValue, MxLn);

	//--------- Add to container

	RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_ADDITIONAL_DATA_CONTAINER, stSubFieldsTlv.pBerTlvData, stSubFieldsTlv.nIndex);

	//----------------------------------------------------------!> DFFF34 - Additional Data container

	//Merchant ID
	if(tMerchant.MID[0] != NULL)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_MERCHANT_ID, (byte*)tMerchant.MID, strlen(tMerchant.MID));

	//Terminal ID
	//--- DFFF34 - DF25

	//Ticket Number
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TICKET_NUMBER, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_TICKET_NUMBER, (byte*)strFieldValue, strlen(strFieldValue));

	//Authorization code
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_HOST_AUTH_NUMBER, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_HOST_AUTORIZATION_CODE, (byte*)strFieldValue, strlen(strFieldValue));

	//Base amount
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_BASE_AMOUNT, &AmountValue, NULL);
	if(RetFun == RET_OK)
	{
		AmountValue = AmountValue * 100;
		GTL_Convert_UllToDcdNumber(AmountValue, strFieldValue, 6);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_BASE_AMOUNT, (byte*)strFieldValue, 6);
	}

	//Installment number
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_INSTALLMENT_NUMBER, &IntValue, NULL);
	if(RetFun == RET_OK)
	{
		GTL_Convert_UlToDcbNumber(IntValue, strFieldValue, 1);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_NUMBER_OF_INSTALLMENTS, (byte*)strFieldValue, 1);
	}
	else
	{
		GTL_Convert_UlToDcbNumber(0, strFieldValue, 1);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_NUMBER_OF_INSTALLMENTS, (byte*)strFieldValue, 1);
	}

	//Last 4 digits PAN
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_PAN, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
	{
		memset(strFieldValueAux, 0x00, sizeof(strFieldValueAux));
		memset(strFieldValueAux, '*', strlen(strFieldValue) - 4);
		strcat(strFieldValueAux, strFieldValue + (strlen(strFieldValue) - 4));

		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_MASKED_PAN, (byte*)strFieldValueAux, strlen(strFieldValueAux));
	}

	//Invoice Number
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_INVOICE, &IntValue, NULL);
	if(RetFun == RET_OK)
	{
		GTL_Convert_UlToBinNumber(IntValue, strFieldValue, 4);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_TRANSACTION_REFERENCE, (byte*)strFieldValue, 4);
	}

	//Card Type
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_IDLE_MENU, &IntValue, NULL);
	if(RetFun == RET_OK)
	{
		if(IntValue == IDLE_CREDIT)
			IntValue = 2;	//Credit
		else if (IntValue == IDLE_DEBIT)
			IntValue = 3;	//Debit
		else
			IntValue = 4;	//No banking card

		GTL_Convert_UlToBinNumber(IntValue, strFieldValue, 1);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_ACCOUNT_TYPE, (byte*)strFieldValue, strlen(strFieldValue));
	}

	//Countable Date
	//--- DFFF34 - DF26

	//Account number
	//--- DFFF34 - DF27

	//Transaction Date
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_DATE_TIME, &stDate, NULL);
	if(RetFun == RET_OK)
	{
		sprintf(strFieldValueAux, "%.02s%.02s%.02s", stDate.year, stDate.month, stDate.day);
		IntValue = atol(strFieldValueAux);
		GTL_Convert_UlToDcbNumber(IntValue, strFieldValue, 3);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_TRANSACTION_DATE, (byte*)strFieldValue, 3);
	}

	//Transaction time
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_DATE_TIME, &stDate, NULL);
	if(RetFun == RET_OK)
	{
		sprintf(strFieldValueAux, "%.02s%.02s%.02s", stDate.hour, stDate.minute, stDate.second);
		IntValue = atol(strFieldValueAux);
		GTL_Convert_UlToDcbNumber(IntValue, strFieldValue, 3);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_TRANSACTION_TIME, (byte*)strFieldValue, 3);
	}

	//Employee
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_EMPLOYEE_ID, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_CASHIER_NUMBER, (byte*)strFieldValue, strlen(strFieldValue));

	//Tip
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TIP_AMOUNT, &AmountValue, NULL);
	if(RetFun == RET_OK)
	{
		AmountValue = AmountValue * 100;
		GTL_Convert_UllToDcdNumber(AmountValue, strFieldValue, 6);
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_TIP_AMOUNT, (byte*)strFieldValue, 6);
	}

	//Voucher merchant
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_VOUCHER_ECR, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_MERCHANT_RECEIPT, (byte*)strFieldValue, strlen(strFieldValue));

	//Voucher customer
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_VOUCHER_ECR_COPY, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_CUSTOMER_RECEIPT, (byte*)strFieldValue, strlen(strFieldValue));

	return RET_OK;
}

//-------------------------------------------------------------------------
int PclUtil_TxSettleBuildResponse(T_BER_TLV_ENCODE_STRUCT *stFieldsTlv)
{
	T_BER_TLV_ENCODE_STRUCT stSubFieldsTlv;
	TABLE_MERCHANT 	tMerchant;

	byte tempBuffer[1000];
	char strFieldValue[3000];

	int RetFun;
	unsigned int MxLn;

	//<!---------------------------------------------------------- DFFF34 - Additional Data container
	RetFun = GTL_BerTlvEncode_Init(&stSubFieldsTlv, tempBuffer, sizeof(tempBuffer));

	// Response code
	strcpy(strFieldValue, "00");
	RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_RESPONSE_CODE, (byte*)strFieldValue, strlen(strFieldValue));

	//Merchant Name
	RetFun = Get_TransactionMerchantData(&tMerchant, Tree_Tran);
	if(RetFun == RET_OK)
	{
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_MERCHANT_NAME, (byte*)tMerchant.Name, strlen(tMerchant.Name));
	}

	//Terminal ID
	if(tMerchant.TID[0] != NULL)
		RetFun = PclUtil_BerTlvAdd(&stSubFieldsTlv, PCL_TAG_ADD_DATA_TERMINAL_ID, (byte*)tMerchant.TID, strlen(tMerchant.TID));

	//--------- Add to container

	RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_ADDITIONAL_DATA_CONTAINER, stSubFieldsTlv.pBerTlvData, stSubFieldsTlv.nIndex);

	//----------------------------------------------------------!> DFFF34 - Additional Data container

	//Merchant ID
	if(tMerchant.MID[0] != NULL)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_MERCHANT_ID, (byte*)tMerchant.MID, strlen(tMerchant.MID));

	//Terminal ID
	//--- DFFF34 - DF25

	//Authorization code
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_HOST_AUTH_NUMBER, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_HOST_AUTORIZATION_CODE, (byte*)strFieldValue, strlen(strFieldValue));

	//Voucher merchant
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_VOUCHER_ECR, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_MERCHANT_RECEIPT, (byte*)strFieldValue, strlen(strFieldValue));

	//Voucher customer
	MxLn = sizeof(strFieldValue) - 1;
	memset(strFieldValue, 0x00, sizeof(strFieldValue));
	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_VOUCHER_ECR_COPY, strFieldValue, &MxLn);
	if(RetFun == RET_OK)
		RetFun = PclUtil_BerTlvAdd(stFieldsTlv, PCL_TAG_CUSTOMER_RECEIPT, (byte*)strFieldValue, strlen(strFieldValue));

	return RET_OK;
}
void PclUtil_ExtendedResponseTLV(T_SERVICE_CALL_PARAM_PDA_START_TRANSACTION *pParams)
{
	int nRet;
	uint8 nApproved = 0;
	byte TempBuffer[5000];
	TLV_TREE_NODE hNode;
	T_BER_TLV_ENCODE_STRUCT stResponseTlv;
	T_BER_TLV_ENCODE_STRUCT stFieldsTlv;
	int nOperationType;

    nRet = GTL_BerTlvEncode_Init(&stResponseTlv, pParams->m_pucResponse, __SPMCI_MAX_EXTENDED_DATA_SIZE);
	nRet = GTL_BerTlvEncode_Init(&stFieldsTlv, TempBuffer, __SPMCI_MAX_EXTENDED_DATA_SIZE);

	hNode = TlvTree_Find(stPclTransFields, PCL_TRANS_ID, 0);
	if (hNode)
		nRet = PclUtil_BerTlvAdd(&stFieldsTlv, PCL_TAG_REQUEST_REFERENCE, TlvTree_GetData(hNode), TlvTree_GetLength(hNode));

	nRet = Tlv_GetTagValue(stPclTransFields, PCL_OPERATION_TYPE, &nOperationType, NULL);

	//Check approved flag. Some transaction don't use tag TAG_APPROVED, then set = 1
	//by default, like local reports or get last sale transaction
	if(nOperationType == TRAN_LASTSALE)
		nApproved = 1;
	else
		nRet = Tlv_GetTagValue(Tree_Tran, TAG_APPROVED, &nApproved, NULL);

	nRet = PclUtil_BerTlvAdd(&stFieldsTlv, PCL_TAG_TRANSACTION_STATUS, &nApproved, LTAG_INT8);

	if ((!nRet)&&(nApproved))
	{
		switch(nOperationType)
		{
			case TRAN_LASTSALE:
			case TRAN_SALE:
			case TRAN_VOID:
				PclUtil_TxSaleBuildResponse(&stFieldsTlv);
				break;

			case TRAN_SETTLE:
				PclUtil_TxSettleBuildResponse(&stFieldsTlv);
				break;

		}
	}
	else
		PclUtil_TxErrorBuildResponse(&stFieldsTlv);

	nRet = PclUtil_BerTlvAdd(&stResponseTlv, PCL_TAG_RESPONSE_CONTAINER, stFieldsTlv.pBerTlvData, stFieldsTlv.nIndex);

	pParams->m_nResponseSize = stResponseTlv.nIndex;
	pParams->m_sDebitEmv.param_out.noappli = ApplicationGetCurrent(); // Return application number
	pParams->m_sDebitEmv.param_out.rc_payment = (nApproved) ? PAY_OK : PAY_KO; // Transaction done
	strcpy((char*)pParams->m_sDebitEmv.param_out.card_holder_nb,"");

}

int PclUtil_doTransactionExtended(T_SERVICE_CALL_PARAM_PDA_START_TRANSACTION *pParams)
{
	int nRet;
	int nOperationType;

	if(stPclTransFields != NULL)
		TlvTree_Release(stPclTransFields);

	stPclTransFields = TlvTree_New(0);

	//parse extended data
	PclUtil_ParseTLVExtendedData(pParams);

	nRet = Tlv_GetTagValue(stPclTransFields, PCL_OPERATION_TYPE, &nOperationType, NULL);
	if (!nRet)
	{
		//if (nOperationType < PCL_TRAN_TEST)
		{
			if (!PclUtil_ValiateAmounts())
			{
				Tlv_SetTagValueInteger(stPclTransFields, TAG_RESPONSE_CODE_TEXT, MSG_RC_INVALID_AMOUNT, sizeof(int));
				Tlv_SetTagValueString (stPclTransFields, TAG_HOST_RESPONSE_CODE, "-1");
			}
			else if (TlvTree_Find(stPclTransFields, PCL_TOTAL_AMOUNT, 0))
			{
//				if(nOperationType == TRAN_SALE)
//				{
//					if( PI_BuildMenuCardType() )
//						nRet = RunTransaction(nOperationType, NULL, NULL, FALSE);
//					else
//					{
//						PI_SetTransactionError(Tree_Tran, RCT_CANCELED);
//					}
//				}
//				else if(nOperationType == TRAN_LASTSALE)
//				{
//					Merchant_Reprint(1);
//				}
//				else
					nRet = RunTransaction(nOperationType, NULL, NULL, FALSE);
			}
		}
	}

	//populate response
	PclUtil_ExtendedResponseTLV(pParams);

	TlvTree_Release(stPclTransFields);
	stPclTransFields = NULL;

	#if(TETRA_HARDWARE == 0)
	IUN_IDLE_FORMAT iUNIdleFormat;
	iUNIdleFormat.iBMPReverse = _OFF_;
	iUNIdleFormat.iHeaderStatus = _ON_;

//	if(!TBK_IsMdbMode())
//		iUN_setIdle(&iUNIdleFormat);
//	else if(!IsColorDisplay())
		DisplayHeader(_OFF_);

	#endif //TETRA_HARDWARE

	return RET_OK;
}

int PclUtil_doTransaction(StructDebitEmv *psDebit)
{
	int nRet;
	int nTransType = TRAN_SALE;

	if (stPclTransFields)
		TlvTree_Release(stPclTransFields);

	stPclTransFields = TlvTree_New(0);

	if (ConvertOperationTypePCL(psDebit->param_in.transaction))
		psDebit->param_out.rc_payment = RET_KO;
	else
	{
		if (psDebit->param_in.amount)
		{
			uint8 nApproved = 0;
			amount lTempAmount = 0;

			Tlv_SetTagValue(stPclTransFields, PCL_TIP_AMOUNT, &lTempAmount, sizeof(amount));
			Tlv_SetTagValue(stPclTransFields, PCL_TAX1_AMOUNT, &lTempAmount, sizeof(amount));

			lTempAmount = psDebit->param_in.amount;
			Tlv_SetTagValue(stPclTransFields, PCL_BASE_AMOUNT, &lTempAmount, sizeof(amount));

//			if(nTransType == TRAN_SALE)
//				nRet = PI_BuildMenuCardType();
//			else
				nRet = TRUE;

			if(nRet == TRUE)
				nRet = RunTransaction(nTransType, NULL, NULL, FALSE);

			nRet = Tlv_GetTagValue(Tree_Tran, TAG_APPROVED, &nApproved, NULL);
			if ((!nRet)&&(nApproved))
			{
				TLV_TREE_NODE hNode;

				hNode = TlvTree_Find(Tree_Tran, TAG_PAN, 0);
				if (hNode)
					FormatPAN(TlvTree_GetData(hNode), psDebit->param_out.card_holder_nb);

				psDebit->param_out.rc_payment = RET_OK;
			}
			else
				psDebit->param_out.rc_payment = RET_KO;
		}
	}

	TlvTree_Release(stPclTransFields);
	stPclTransFields = NULL;

	return RET_OK;
}


