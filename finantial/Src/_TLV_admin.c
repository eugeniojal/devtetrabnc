 /* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * File:			_TLV_admin.c
 * Header file:		_TLV_admin.h
 * Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

ewlObject_t *EwlEmvHandle;

//+ EWL01 @jbotero 1/02/2016
/* --------------------------------------------------------------------------
 * Function Name:	Tlv_FindTag
 * Description:		Search tag into tree on index 0.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * Return:
 * 	- (RET_OK)  Tag exist
 * 	- (RET_NOK) Tag doesn't exist
 * Notes:
 */

int Tlv_FindTag(TLV_TREE_NODE Tree, unsigned int nTag)
{
	TLV_TREE_NODE node;

	//--- Search tag
	node = TlvTree_Find(Tree, nTag, 0);
	CHECK(node != NULL, LBL_NOT_FOUNT);

	return RET_OK;

LBL_NOT_FOUNT:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValue
 * Description:		Set tag children value into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in void * value: pointer of data
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValue(TLV_TREE_NODE Tree, unsigned int nTag, void * value, unsigned int length)
{
	TLV_TREE_NODE node;

	//--- Search and destroy tag
	node = TlvTree_Find(Tree, nTag, 0);
	if(node != NULL)
		TlvTree_Release(node);

	//--- Add new value
	node = TlvTree_AddChild(Tree, nTag, value, length);
	CHECK(node != NULL, LBL_ERROR);

	return RET_OK;

LBL_ERROR:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_GetTagValue
 * Description:		Get tag children value into tree.
 * 	- If tags exist, return value and length.
 * 	- If tags doesn't exist, return error.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- out void * value: pointer of data
 * 	- out unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_GetTagValue(TLV_TREE_NODE Tree, unsigned int nTag, void * value, unsigned int * length)
{
	TLV_TREE_NODE node;
	unsigned int ln;

	//Check null pointer of value
	CHECK(value != NULL, LBL_ERROR);

	//--- Search tag
    node = TlvTree_Find(Tree, nTag, 0);
    CHECK(node != NULL, LBL_ERROR);

    //--- Get value
    ln = TlvTree_GetLength(node);
    memcpy(value, TlvTree_GetData(node), ln );

    if(length != NULL)
    	*length = ln;

	return RET_OK;

LBL_ERROR:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_ReleaseTag
 * Description:		Release a tag into tree.
 * 	- If tags exist, release tag.
 * 	- If tags doesn't exist, do anything.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_ReleaseTag(TLV_TREE_NODE Tree, unsigned int nTag)
{
	TLV_TREE_NODE node;

	//--- Search tag
    node = TlvTree_Find(Tree, nTag, 0);
    CHECK(node != NULL, LBL_END);

    //--- Release Tag
    TlvTree_Release(node);

LBL_END:
	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_EwlGetParameter
 * Description:		Read parameter from EWL, and load value on specific Tree
 * 					and tag
 * 	- If tree tag exist, overwrite information on same level.
 * 	- If tree tag doesn't exist, create tag and set value.
 * Author:			@jbotero
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in ewlObject_t *handle: Handle of EMV EWL
 * 	- out void *data: Pointer to data, can be NULL if data isn't needed
 * 	- in unsigned int maxLen: Max length of data to read
 * 	- in ewlTag_t tag: EWL Tag to read
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) Tag no exist.
 * Notes:
 */
int Tlv_EwlGetParameter(TLV_TREE_NODE Tree, unsigned int nTag,
		ewlObject_t *handle, void *data, unsigned int maxLen, ewlTag_t tag )
{
	char * p = NULL;
	int RetFun;

	p = umalloc(sizeof(char) * maxLen);
	CHECK(p!= NULL, LBL_ERROR);

	RetFun = ewlGetParameter(handle, p, maxLen, tag);

	if(RetFun >= 0)
	{
		if(data != NULL)
			memcpy(data, p, RetFun);

		Tlv_SetTagValue(Tree, nTag, p, RetFun);
	}

	if(p != NULL)
		ufree(p);

	return RetFun;

LBL_ERROR:

	return RET_NOK;
}

//- EWL01

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueInteger
 * Description:		Set tag children value as an integer into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in int value: number with the value
 * 	- in unsigned int length: Length of data
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValueInteger(TLV_TREE_NODE Tree, unsigned int nTag, int value, unsigned int length)
{
	TLV_TREE_NODE node;

	//--- Search and destroy tag
	node = TlvTree_Find(Tree, nTag, 0);
	if(node != NULL)
		TlvTree_Release(node);

	//--- Add new value
	node = TlvTree_AddChildInteger(Tree, nTag, value, length);
	CHECK(node != NULL, LBL_ERROR);

	return RET_OK;

LBL_ERROR:

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Tlv_SetTagValueString
 * Description:		Set tag children value string into tree.
 * 	- If tags exist, overwrite information on same level.
 * 	- If tags doesn't exist, create tag and set value.
 * Author:			@lcalvano
 * Parameters:
 * 	- in TLV_TREE_NODE Tree: Parent tree.
 * 	- in unsigned int nTag: Tag id.
 * 	- in char * value: pointer to the null terminated string to save in the tree
 * Return:
 * 	- (RET_OK)  OK
 * 	- (RET_NOK) fail, invalid parameters. Can't add child
 * Notes:
 */

int Tlv_SetTagValueString(TLV_TREE_NODE Tree, unsigned int nTag, char *value)
{
	TLV_TREE_NODE node;

	//--- Search and destroy tag
	node = TlvTree_Find(Tree, nTag, 0);
	if(node != NULL)
		TlvTree_Release(node);

	//--- Add new value
	node = TlvTree_AddChildString(Tree, nTag, value);
	CHECK(node != NULL, LBL_ERROR);

	return RET_OK;

LBL_ERROR:

	return RET_NOK;
}
