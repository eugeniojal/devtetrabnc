//+ PAR_EWL @carodriguez 29/02/2016
#if (DIR_EWL_PAR == 1)
#include "iPOSApp.h"

long BCDStringToLong(void* input, int length)
{
	unsigned long long RetValue = 0;
	
	GTL_Convert_DcbNumberToUll(input, &RetValue, length);

	return (long)RetValue;
}

int ByteBCDToInt(byte input)
{
	long RetValue = 0x00;

	RetValue = (input >> 4) * 10 + (input & 0x000F);

	return RetValue;
}

bool checkPARTree(TLV_TREE_NODE Tree)
{

	bool RetValue = FALSE;
	TLV_TREE_NODE mainNode;
	TLV_TREE_NODE singleNode;
	
	AIDCount = 0;
	KeyCount = 0;
	RevokedCount = 0;
	RetValue = TRUE;
	
	// AIDs node
	mainNode = TlvTree_Find(Tree, C_TAG_AID, 0);
	CHECK(mainNode != NULL, ERR_LBL);
	
	singleNode = TlvTree_GetFirstChild(mainNode);
	CHECK(singleNode!= NULL, ERR_LBL)
	
	while(singleNode!=NULL)
	{
		singleNode = TlvTree_GetNext(singleNode);
		AIDCount++;
	}
	
	// Keys node
	mainNode = TlvTree_Find(Tree, C_TAG_CAKEYS, 0);
	CHECK(mainNode != NULL, ERR_LBL);
	
	singleNode = TlvTree_GetFirstChild(mainNode);
	CHECK(singleNode!= NULL, ERR_LBL)
	
	while(singleNode!=NULL)
	{
		singleNode = TlvTree_GetNext(singleNode);
		KeyCount++;
	}
	
	// Revoked node
	mainNode = TlvTree_Find(Tree, C_TAG_CAREVOK, 0);
	CHECK(mainNode != NULL, ERR_LBL);
	
	singleNode = TlvTree_GetFirstChild(mainNode);
	CHECK(singleNode!= NULL, ERR_LBL)
	
	while(singleNode!=NULL)
	{
		singleNode = TlvTree_GetNext(singleNode);
		RevokedCount++;
	}
	RetValue = TRUE;

ERR_LBL:
	return RetValue;
}

int loadAIDs(TLV_TREE_NODE Tree, aid_t *AIDsFromFile)
{
	// int RetFun;
	TLV_TREE_NODE nodeAIDs;
	TLV_TREE_NODE singleAID;
	TLV_TREE_NODE ValueAID;
	int ValueAIDLen;
	aid_t AIDRecord;
	int AIDCount=0;
	
	// AIDs node
	nodeAIDs = TlvTree_Find(Tree, C_TAG_AID, 0);
	CHECK(nodeAIDs != NULL, ERR_LBL);
	
	singleAID = TlvTree_GetFirstChild(nodeAIDs);
	CHECK(singleAID	!= NULL, ERR_LBL)
	
	while(singleAID!=NULL)
	{
		memset(&AIDRecord, 0x00, sizeof(AIDRecord));
		ValueAID = TlvTree_Find(singleAID, 0xDF828001, 0);
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		CHECK(ValueAID!=NULL, ERR_LBL)
		memcpy(&AIDRecord.technology, TlvTree_GetData(ValueAID), ValueAIDLen);

		ValueAID = TlvTree_Find(singleAID, 0x9F06, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.aidLen = ValueAIDLen > EWL_EMV_AID_CARD_MAX_LEN ? EWL_EMV_AID_CARD_MAX_LEN : ValueAIDLen;
		memcpy(&AIDRecord.aid, TlvTree_GetData(ValueAID), AIDRecord.aidLen);

		ValueAID = TlvTree_Find(singleAID, 0x9F09, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)

		ValueAID = TlvTree_GetFirstChild(ValueAID);
		CHECK(ValueAID!=NULL, ERR_LBL)
		
		AIDRecord.version.nVersions = 0;
		
		while(ValueAID!=NULL && AIDRecord.version.nVersions < EWLDEMO_VERSION_NUMBER_MAX)
		{
			ValueAIDLen = TlvTree_GetLength(ValueAID);
			ValueAIDLen = ValueAIDLen > EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN ? EWL_EMV_APPLI_VERSION_NUMBER_TERM_LEN : ValueAIDLen;
			memcpy(&AIDRecord.version.version[AIDRecord.version.nVersions], TlvTree_GetData(ValueAID), ValueAIDLen);
			AIDRecord.version.nVersions++;
			ValueAID = TlvTree_GetNext(ValueAID);
		}

		// TAC Online
		ValueAID = TlvTree_Find(singleAID, 0x9F91870B, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		ValueAIDLen = ValueAIDLen > EWL_TAC_LEN ? EWL_TAC_LEN : ValueAIDLen;
		memcpy(&AIDRecord.tac.onlineValue, TlvTree_GetData(ValueAID), ValueAIDLen);

		// TAC Denial
		ValueAID = TlvTree_Find(singleAID, 0x9F91870A, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		ValueAIDLen = ValueAIDLen > EWL_TAC_LEN ? EWL_TAC_LEN : ValueAIDLen;
		memcpy(&AIDRecord.tac.denialValue, TlvTree_GetData(ValueAID), ValueAIDLen);

		// TAC Default
		ValueAID = TlvTree_Find(singleAID, 0x9F918709, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		ValueAIDLen = ValueAIDLen > EWL_TAC_LEN ? EWL_TAC_LEN : ValueAIDLen;
		memcpy(&AIDRecord.tac.defaultValue, TlvTree_GetData(ValueAID), ValueAIDLen);

		// Floor limit
		ValueAID = TlvTree_Find(singleAID, 0x9F1B, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.risk.FloorLimit=BCDStringToLong(TlvTree_GetData(ValueAID), ValueAIDLen);
		
		ValueAID = TlvTree_Find(singleAID, 0x9F91870C, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.risk.Threshold = BCDStringToLong(TlvTree_GetData(ValueAID), ValueAIDLen);
		
		ValueAID = TlvTree_Find(singleAID, 0x9F918707, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		ValueAIDLen = ValueAIDLen > EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL_LEN ? EWL_TAG_MAX_TARGET_PERC_BIASED_RAND_SEL_LEN : ValueAIDLen;
		memcpy(&AIDRecord.risk.Max, TlvTree_GetData(ValueAID), ValueAIDLen);
		AIDRecord.risk.Max[0] = (byte) ByteBCDToInt((long)AIDRecord.risk.Max[0]);
		
		ValueAID = TlvTree_Find(singleAID, 0x9F918708, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		ValueAIDLen = ValueAIDLen > EWL_TAG_TARGET_PERC_RAND_SEL_LEN ? EWL_TAG_TARGET_PERC_RAND_SEL_LEN : ValueAIDLen;
		memcpy(&AIDRecord.risk.Target, TlvTree_GetData(ValueAID), ValueAIDLen);
		AIDRecord.risk.Target[0] = (byte) ByteBCDToInt((long)AIDRecord.risk.Target[0]);

		// Transaction limit for contactless
		ValueAID = TlvTree_Find(singleAID, 0xDF808001, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.cless.TransactionLimit=BCDStringToLong(TlvTree_GetData(ValueAID), ValueAIDLen);
		
		// CVM required limit for contactless
		ValueAID = TlvTree_Find(singleAID, 0xDF808003, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.cless.CMVLimit = BCDStringToLong(TlvTree_GetData(ValueAID), ValueAIDLen);
		
		ValueAID = TlvTree_Find(singleAID, 0x9F918706, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.tdol.dolLen = ValueAIDLen > EWLDEMO_DOL_MAX_LEN ? EWLDEMO_DOL_MAX_LEN : ValueAIDLen;
		memcpy(&AIDRecord.tdol.dol, TlvTree_GetData(ValueAID), AIDRecord.tdol.dolLen);

		ValueAID = TlvTree_Find(singleAID, 0x9F918705, 0);
		CHECK(ValueAID!=NULL, ERR_LBL)
		ValueAIDLen = TlvTree_GetLength(ValueAID);
		AIDRecord.ddol.dolLen = ValueAIDLen > EWLDEMO_DOL_MAX_LEN ? EWLDEMO_DOL_MAX_LEN : ValueAIDLen;
		memcpy(&AIDRecord.ddol.dol, TlvTree_GetData(ValueAID), AIDRecord.ddol.dolLen);
		
		AIDsFromFile[AIDCount] = AIDRecord;

		singleAID = TlvTree_GetNext(singleAID);
		AIDCount++;
	}

	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int loadKeys(TLV_TREE_NODE Tree, stKey_t *KeysFromFile)
{
	// int RetFun;
	TLV_TREE_NODE nodeKeys;
	TLV_TREE_NODE singleKey;
	TLV_TREE_NODE ValueKey;
	int ValueKeyLen;
	stKey_t KeyRecord;
	int KeyCount=0;
	
	// Contact node
	nodeKeys = TlvTree_Find(Tree, C_TAG_CAKEYS, 0);
	CHECK(nodeKeys != NULL, ERR_LBL);
	
	singleKey = TlvTree_GetFirstChild(nodeKeys);
	CHECK(singleKey	!= NULL, ERR_LBL)
	
	while(singleKey!=NULL)
	{
		memset(&KeyRecord, 0x00, sizeof(KeyRecord));
		ValueKey = TlvTree_Find(singleKey, 0x9F22, 0);
		CHECK(ValueKey!=NULL, ERR_LBL)
		ValueKeyLen = TlvTree_GetLength(ValueKey);
		memcpy(&KeyRecord.keyId, TlvTree_GetData(ValueKey), ValueKeyLen);

		ValueKey = TlvTree_Find(singleKey, 0x9F06, 0);
		CHECK(ValueKey!=NULL, ERR_LBL)
		ValueKeyLen = TlvTree_GetLength(ValueKey);
		// KeyRecord.aidLen = ValueKeyLen > EWL_EMV_AID_CARD_MAX_LEN ? EWL_EMV_AID_CARD_MAX_LEN : ValueAIDLen;
		memcpy(&KeyRecord.rid, TlvTree_GetData(ValueKey), ValueKeyLen);

		ValueKey = TlvTree_Find(singleKey, 0x9F8122, 0);
		ValueKeyLen = KeyRecord.keyExpLen = TlvTree_GetLength(ValueKey);
		memcpy(&KeyRecord.keyExp, TlvTree_GetData(ValueKey), ValueKeyLen);
		CHECK(ValueKey!=NULL, ERR_LBL)

		ValueKey = TlvTree_Find(singleKey, 0x9F8123, 0);
		ValueKeyLen = KeyRecord.keyModulusLen = TlvTree_GetLength(ValueKey);
		memcpy(&KeyRecord.keyModulus, TlvTree_GetData(ValueKey), ValueKeyLen);
		CHECK(ValueKey!=NULL, ERR_LBL)

		KeysFromFile[KeyCount] = KeyRecord;

		singleKey = TlvTree_GetNext(singleKey);
		KeyCount++;
	}

	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int saveRevokedCAs(TLV_TREE_NODE Tree, ewlSetRevoked_t *RevokedFromFile)
{
	// int RetFun;
	TLV_TREE_NODE nodeRevoked;
	TLV_TREE_NODE singleRevoked;
	TLV_TREE_NODE ValueRevoked;
	int ValueRevokedLen;
	ewlSetRevoked_t RevokedRecord;
	int RevokedCount=0;
	
	// Contact node
	nodeRevoked = TlvTree_Find(Tree, C_TAG_CAREVOK, 0);
	CHECK(nodeRevoked != NULL, ERR_LBL);
	
	singleRevoked = TlvTree_GetFirstChild(nodeRevoked);
	CHECK(singleRevoked	!= NULL, ERR_LBL)
	
	while(singleRevoked!=NULL)
	{
		memset(&RevokedRecord, 0x00, sizeof(RevokedRecord));
		
		ValueRevoked = TlvTree_Find(singleRevoked, 0x9F06, 0);
		CHECK(ValueRevoked!=NULL, ERR_LBL)
		ValueRevokedLen = TlvTree_GetLength(ValueRevoked);
		// RevokedRecord.aidLen = ValueRevokedLen > EWL_EMV_AID_CARD_MAX_LEN ? EWL_EMV_AID_CARD_MAX_LEN : ValueAIDLen;
		memcpy(&RevokedRecord.RID, TlvTree_GetData(ValueRevoked), ValueRevokedLen);

		ValueRevoked = TlvTree_Find(singleRevoked, 0x9F22, 0);
		CHECK(ValueRevoked!=NULL, ERR_LBL)
		ValueRevokedLen = TlvTree_GetLength(ValueRevoked);
		memcpy(&RevokedRecord.index, TlvTree_GetData(ValueRevoked), ValueRevokedLen);


		ValueRevoked = TlvTree_Find(singleRevoked, 0x9F8424, 0);
		ValueRevokedLen = TlvTree_GetLength(ValueRevoked);
		memcpy(&RevokedRecord.certificateSN, TlvTree_GetData(ValueRevoked), ValueRevokedLen);
		CHECK(ValueRevoked!=NULL, ERR_LBL)

		RevokedFromFile[RevokedCount] = RevokedRecord;

		singleRevoked = TlvTree_GetNext(singleRevoked);
		RevokedCount++;
	}

	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int saveExtraPar(TLV_TREE_NODE SourceTree)
{
	TLV_TREE_NODE nodeParameters;
	
	// Contact node
	nodeParameters = TlvTree_Find(SourceTree, C_TAG_ICS, 0);
	CHECK(nodeParameters!=NULL, ERR_LBL);
	
	extraParTree = TlvTree_Copy(nodeParameters);
	CHECK(extraParTree!=NULL, ERR_LBL);

	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int savePaywavePar(TLV_TREE_NODE SourceTree)
{
	TLV_TREE_NODE nodeParameters;

	// Contact node
	nodeParameters = TlvTree_Find(SourceTree, C_TAG_PAYWAVE, 0);
	CHECK(nodeParameters!=NULL, ERR_LBL);

	PaywaveTree = TlvTree_Copy(nodeParameters);
	CHECK(PaywaveTree!=NULL, ERR_LBL);

	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int loadExtraPar(TLV_TREE_NODE Tree, ewlObject_t *handle)
{
	int RetFun;
	TLV_TREE_NODE nodeParameters;
	TLV_TREE_NODE singleParameter;
	unsigned int ValueTag;
	unsigned int ValueParameterLen;
	
	CHECK(handle!=NULL, ERR_LBL);
	
	// Contact node
	nodeParameters = TlvTree_Find(Tree, C_TAG_ICS, 0);
	if(nodeParameters!=NULL)
	{
		singleParameter = TlvTree_GetFirstChild(nodeParameters);
		CHECK(singleParameter!=NULL, ERR_LBL)
		
		while(singleParameter!=NULL)
		{
			ValueTag = TlvTree_GetTag(singleParameter);
			CHECK(ValueTag!=0x00, ERR_LBL)
			ValueParameterLen = TlvTree_GetLength(singleParameter);
			CHECK(ValueParameterLen!=0, ERR_LBL)
			RetFun = ewlSetParameter(handle, ValueTag, TlvTree_GetData(singleParameter)
										, ValueParameterLen );
			CHECK(EWL_OK == RetFun, ERR_LBL)
			singleParameter = TlvTree_GetNext(singleParameter);
		}
	}
	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int loadPaywaveSpecific(TLV_TREE_NODE Tree, stPaywave *ParamsFromFile)
{
	TLV_TREE_NODE nodeParams = NULL;

	nodeParams = TlvTree_Find(Tree, C_TAG_PAYWAVE, 0);
	if(nodeParams!=NULL)
	{
		stPaywave PaywaveRecord;
		TLV_TREE_NODE ValueParameter;

		memset(&PaywaveRecord, 0x00, sizeof(PaywaveRecord));
		ValueParameter = TlvTree_Find(nodeParams, EWL_PAYWAVE_TTQ, 0);
		CHECK(ValueParameter!=NULL, ERR_LBL)
		memcpy(ParamsFromFile->TTQ, TlvTree_GetData(ValueParameter), TlvTree_GetLength(ValueParameter));
	}


	return RET_OK;
ERR_LBL:

	return RET_NOK;
}

int loadTreeToStructs(TLV_TREE_NODE Tree, int AIDCount, int KeyCount, int RevokedCount)
{
	int RetFun;

	// Load AIDs
	if(pAIDsFromFile!=NULL)
		ufree(pAIDsFromFile);

	pAIDsFromFile = (aid_t*)umalloc(sizeof(aid_t)*AIDCount);
	CHECK(NULL != pAIDsFromFile, ERR_LBL)

	memset(pAIDsFromFile, 0x00, sizeof(aid_t)*AIDCount);
	RetFun = loadAIDs(Tree, pAIDsFromFile);
	CHECK(RET_OK == RetFun, ERR_LBL)

	// Load Keys
	if(pKeysFromFile!=NULL)
		ufree(pKeysFromFile);

	pKeysFromFile = (stKey_t*)umalloc(sizeof(stKey_t)*KeyCount);
	CHECK(NULL != pKeysFromFile, ERR_LBL)

	memset(pKeysFromFile, 0x00, sizeof(stKey_t)*KeyCount);
	RetFun = loadKeys(Tree, pKeysFromFile);
	CHECK(RET_OK == RetFun, ERR_LBL)

	// Load Revoked
	if(pRevokedFromFile!=NULL)
		ufree(pRevokedFromFile);

	pRevokedFromFile = (ewlSetRevoked_t*)umalloc(sizeof(ewlSetRevoked_t)*RevokedCount);
	CHECK(NULL != pRevokedFromFile, ERR_LBL)

	memset(pRevokedFromFile, 0x00, sizeof(ewlSetRevoked_t)*RevokedCount);
	RetFun = saveRevokedCAs(Tree, pRevokedFromFile);
	CHECK(RET_OK == RetFun, ERR_LBL)

	// Load Extra parameters
	if(extraParTree!=NULL)
	{
		TlvTree_Release(extraParTree);
		extraParTree = NULL;
	}

	RetFun = saveExtraPar(Tree);
	CHECK(RET_OK == RetFun, ERR_LBL)

	// Save Paywave parameters
	if(PaywaveTree!=NULL)
	{
		TlvTree_Release(PaywaveTree);
		PaywaveTree = NULL;
	}

	RetFun = savePaywavePar(Tree);
	CHECK(RET_OK == RetFun, ERR_LBL)

	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

void processPARFile (char* file_name)
{
	TLV_TREE_NODE Tree = NULL;
	int RetFun;

	RetFun = memcmp(SOURCE_FILE, file_name, sizeof(SOURCE_FILE));

	if(RetFun == 0)
	{
		RetFun = XmlFileTlv_To_Tree(&Tree, "/SYSTEM", D_EWL_PAR);
		CHECK(RetFun==RET_OK, ERR_LBL);
		CHECK(Tree!=NULL, ERR_TREE);

		RetFun = checkPARTree(Tree);
		CHECK(TRUE == RetFun, ERR_LBL);

		//RetFun = Tree_To_XmlFileTlv(Tree, D_HOST, D_EWL_CONF);
		//CHECK(RetFun==RET_OK, ERR_LBL);

		RetFun = loadTreeToStructs(Tree, AIDCount, KeyCount, RevokedCount);
		CHECK(RET_OK == RetFun, ERR_LBL)
	}

ERR_LBL:
	if(Tree!=NULL)
		TlvTree_Release(Tree);

ERR_TREE:
	return;
}

int loadRevokedCAs(ewlObject_t *handle)
{
	int RetFun;
	ewlSetRevoked_t revoked;
	int i;
	
	for(i=0;i<RevokedCount;i++)
	{
		memset(&revoked,0x00,sizeof(revoked));
		// revoked = *(pRevokedFromFile + i);
		memcpy(&revoked, pRevokedFromFile + i, sizeof(revoked));
		RetFun = ewlSetRevoked(handle,&revoked);
		CHECK(RetFun == EWL_OK, ERR_LBL)
	}
	return RET_OK;
ERR_LBL:
	return RET_NOK;
}

int loadEWLConf(void)
{
	TLV_TREE_NODE Tree = NULL;
	int RetFun;

	XmlFileTlv_To_Tree(&Tree, D_HOST, D_EWL_CONF);
	CHECK(Tree != NULL, ERR_TREE);

	RetFun = checkPARTree(Tree);
	CHECK(TRUE == RetFun, ERR_LBL);

	RetFun = loadTreeToStructs(Tree, AIDCount, KeyCount, RevokedCount);
	CHECK(RET_OK == RetFun, ERR_LBL);

ERR_LBL:
	if(Tree != NULL)
		TlvTree_Release(Tree);
ERR_TREE:

	return FCT_OK;
}

bool checkFile(uchar* Name)
{
	char FileName[25];
	char Label[10];
	unsigned int nMode = FS_NOFLAGS;
	int RetFun;

	//Set Dir Name with '/'
	memclr(Label, sizeof(Label));
	sprintf(Label,"/%s",DIR_HOST);

	//Mount Disk
	RetFun=FS_mount(Label,&nMode);
	CHECK(RetFun==FS_OK,MOUNT_ERROR)

	//Set File name with /
	memclr(FileName, sizeof(FileName));
	sprintf(FileName, "%s/%s", Label, Name);

	//The File already exists?
	RetFun = FS_exist(FileName);

	if(FS_OK == RetFun)
		return TRUE;

MOUNT_ERROR:
	FS_unmount(Label);
	return FALSE;
}

/* --------------------------------------------------------------------------
 * Function Name:	EWL_LoadParameters
 * Description:		Check EWL.PAR file signed and load EMV & CLESS parameters
 * Author:			@jbotero
 * Parameters:
 * - Nothing
 * Return:
 * - RET_OK (operation OK, parameters loaded)
 * - RET_NOK(operation fail. Isn't possible start the EWL library)
 * Notes:
 */

int EWL_LoadParameters(void)
{
	int RetFun;
	object_descriptor_t stFileDesciptor;

	void * hFile = NULL;
	char * pBuffer = NULL;
	TLV_TREE_NODE Tree = NULL;

	//--- Read EWL.PAR(Signed) on system disk, to get EMV & CLESS parameters
	memset(&stFileDesciptor, 0x00, sizeof(stFileDesciptor));
	RetFun = ObjectGetDescriptor(OBJECT_TYPE_PARAM, EWL_PAR_APPTYPE, &stFileDesciptor);
	CHECK(RetFun == RET_OK, LBL_NO_PARAMS);

	// reserve memory for pBuffer
	pBuffer = umalloc(stFileDesciptor.code_size);
	CHECK(pBuffer != NULL, LBL_ERROR);

	if(( hFile = ObjectOpen(&stFileDesciptor) )!= NULL)
	{
		//Read signed file
		RetFun = ObjectRead(pBuffer, 1, stFileDesciptor.code_size, hFile);
		ObjectClose(hFile);
		CHECK(RetFun == stFileDesciptor.code_size, LBL_ERROR);

		// UN-serialize buffer
		RetFun = TlvTree_Unserialize(&Tree, TLV_TREE_SERIALIZER_XML, (uint8*)pBuffer, stFileDesciptor.code_size);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

		RetFun = checkPARTree(Tree);
		CHECK(TRUE == RetFun, LBL_ERROR);

		RetFun = loadTreeToStructs(Tree, AIDCount, KeyCount, RevokedCount);
		CHECK(RET_OK == RetFun, LBL_ERROR);
	}

	// free memory
	if(pBuffer!=NULL)
		ufree(pBuffer);

	if(Tree != NULL)
		TlvTree_Release(Tree);

	return RET_OK;

LBL_NO_PARAMS:

	// free memory
	if(pBuffer!=NULL)
		ufree(pBuffer);

	if(Tree != NULL)
		TlvTree_Release(Tree);
#if(TETRA_HARDWARE == 1)//Message shown only in Tetra since it needs to have the parameter file signed while t2 can be from unsigned par
		UI_ShowMessage("ERROR EWL", "CARGUE ARCHIVO FIRMADO DE PARAMETROS", "EMV/CLESS", GL_ICON_ERROR, GL_BUTTON_ALL, 5);
#endif
		return RET_NOK;
LBL_ERROR:

	// free memory
	if(pBuffer!=NULL)
		ufree(pBuffer);

	if(Tree != NULL)
		TlvTree_Release(Tree);

	return RET_NOK;
}
#endif // DIR_EWL_PAR
//- PAR_EWL
