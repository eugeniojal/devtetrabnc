#include "iPOSApp.h"
#include "schVar_def.h"
#include "tlvVar_def.h"

Telium_File_t *swipe31 = NULL;
Telium_File_t *swipe2 = NULL;
Telium_File_t *swipe3 = NULL;


//------------------------------------------------------------------------------
int utilOpenDriver(void){

    int test;
    test = ClessEmv_OpenDriver();

    swipe31 = Telium_Fopen("SWIPE31", "r*");
    if(swipe31 == NULL){
        utilCloseDriver();
        return EWLDEMO_ERR_INTERNAL;
    }

    swipe2 = Telium_Fopen("SWIPE2", "r*");
    if(swipe2 == NULL){
        utilCloseDriver();
        return EWLDEMO_ERR_INTERNAL;
    }

    swipe3 = Telium_Fopen("SWIPE3", "r*");
    if(swipe3 == NULL){
        utilCloseDriver();
        return EWLDEMO_ERR_INTERNAL;
    }

    return BASE_ERR_OK;
}

//------------------------------------------------------------------------------

void utilCloseDriver(void){

    utilLedsOff();

    if(swipe3 != NULL) Telium_Fclose(swipe3);

    if(swipe2 != NULL) Telium_Fclose(swipe2);

    if(swipe31 != NULL) Telium_Fclose(swipe31);

    ClessEmv_CloseDriver();

    return;
}

//------------------------------------------------------------------------------
void utilLedsOff(void){

	if(IsColorDisplay())
	{
		SetLedEvent(SOFTWARE_LED_04, 0);
		SetLedEvent(SOFTWARE_LED_03, 0);
		SetLedEvent(SOFTWARE_LED_02, 0);
		SetLedEvent(SOFTWARE_LED_01, 0);
	}
	else
	{
		SetLedEvent(HARDWARE_LED_04, 0);
		SetLedEvent(HARDWARE_LED_03, 0);
		SetLedEvent(HARDWARE_LED_02, 0);
		SetLedEvent(HARDWARE_LED_01, 0);
	}
}

//------------------------------------------------------------------------------
void utilBeepError(void){

    TPass_Buzzer(750, 32);  // Turn on the buzzer
    Telium_Ttestall(0, 25); // Wait 250 ms
    TPass_Buzzer(0, 0);     // Turn off the buzzer
    Telium_Ttestall(0, 20); // Wait 200 ms
    TPass_Buzzer(750, 32);  // Turn on the buzzer
    Telium_Ttestall(0, 25); // Wait 250 ms
    TPass_Buzzer(0, 0);     // Turn off the buzzer

    return;
}

//------------------------------------------------------------------------------
void utilBeepWarning(void ){
    TPass_Buzzer(1500, 64); // Turn on the buzzer
    Telium_Ttestall(0, 40); // Wait 500 ms
    TPass_Buzzer(0, 0); 	// Turn off the buzzer
}

//------------------------------------------------------------------------------
void utilResetCless(void){
#if 0
    int test;


    if(ClessEmv_IsDriverOpened()){
        ClessEmv_CloseDriver();                         // workaround to avoid spurious cless events.
        test =  ClessEmv_OpenDriver();                          // workaround to avoid spurious cless events.
    }
#endif


    return;
}

//------------------------------------------------------------------------------
Telium_File_t *utilTrack1 (void){
    return swipe31;
}

//------------------------------------------------------------------------------
Telium_File_t *utilTrack2 (void){
    return swipe2;
}

//------------------------------------------------------------------------------
Telium_File_t *utilTrack3 (void){
    return swipe3;
}

//------------------------------------------------------------------------------
static void TrimNumeric (char *data){

    int ii;
    int len = strlen(data);
    for(ii = 0; ii < len; ii ++){
        if((data[ii] >= 0x30) && (data[ii] <= 0x39)) break;
    }

    if(ii == len){
        memset(data,0x00,len);
        return;
    }

    len = strlen(&data[ii]);
    memmove(data,&data[ii],len);
    data[len] = 0x00;

    for(ii = (len-1); ii >= 0; ii--) {
        if((data[ii] < 0x30) || (data[ii] > 0x39))
            data[ii] = 0;
        else {
            break;
        }
    }

    return;
}

//------------------------------------------------------------------------------
int utilGetPanASCII ( ewlObject_t *handle, char *pOut, int maxSize){

    int ret;
    byte pan[EWL_EMV_PAN_MAX_LEN];
    char panASCII[EWL_EMV_PAN_MAX_LEN * 2 + 1];

    memset(pOut,0x00,maxSize);

    ret = ewlGetParameter(handle,pan,sizeof(pan),TAG_EMV_APPLI_PAN);
    if(ret < 0) return ret;

    memset(panASCII, 0x00, sizeof(panASCII));
    convBufToHex(pan,ret, panASCII, sizeof(panASCII));
    TrimNumeric(panASCII);

    if(strlen(panASCII) >  (maxSize - 1)) return EWLDEMO_ERR_OVERFLOW;

    strcpy(pOut,panASCII);

    return EWLDEMO_OK;
}

//------------------------------------------------------------------------------
int utilGetExpirationYYMM ( ewlObject_t *handle, char *pOut, int maxSize){

    int ret;
    byte expiration[EWL_EMV_EXPIRATION_DATE_LEN];
    char expirationASCII[EWL_EMV_EXPIRATION_DATE_LEN * 2 + 1];
    memset(pOut,0x00,maxSize);

    ret = ewlGetParameter(handle,expiration,sizeof(expiration),EWL_EMV_EXPIRATION_DATE);
    if(ret < 0) return ret;

    memset(expirationASCII, 0x00, sizeof(expirationASCII));
    convBufToHex(expiration,2, expirationASCII, sizeof(expirationASCII));
    TrimNumeric(expirationASCII);

    if(strlen(expirationASCII) >  (maxSize - 1)) return EWLDEMO_ERR_OVERFLOW;

    strcpy(pOut,expirationASCII);

    return EWLDEMO_OK;
}


//------------------------------------------------------------------------------
int utilGetServiceCodeAscii ( ewlObject_t *handle, char *pOut, int maxSize){

    int ret;
    byte serviceCode[EWL_EMV_SERVICE_CODE_LEN];
    char serviceCodeASCII[EWL_EMV_SERVICE_CODE_LEN * 2 + 1];

    memset(pOut,0x00,maxSize);

    ret = ewlGetParameter(handle,serviceCode,sizeof(serviceCode),EWL_EMV_SERVICE_CODE);
    if(ret < 0) return ret;

    memset(serviceCodeASCII, 0x00, sizeof(serviceCodeASCII));
    convBufToHex(serviceCode,ret, serviceCodeASCII, sizeof(serviceCodeASCII));

    memmove(&serviceCodeASCII[0],&serviceCodeASCII[1],3);
    serviceCodeASCII[3] = 0x00;

    TrimNumeric(serviceCodeASCII);


    if(strlen(serviceCodeASCII) >  (maxSize - 1)) return EWLDEMO_ERR_OVERFLOW;

    strcpy(pOut,serviceCodeASCII);

    return EWLDEMO_OK;
}
//------------------------------------------------------------------------------
int RecoverDataFromTrack1 (TransactionPaymentData_ST *data){

    char *p;
    char track[TRANSACTION_TRACK1_ASCII_LEN];
    char pan[19+1];
    char name[26+1];
    char additionalData[7+1];

    memset(pan,0x00,sizeof(pan));
    memset(name,0x00,sizeof(name));
    memset(additionalData,0x00,sizeof(additionalData));

    if(strlen(data->track1) == 0) return EWLDEMO_ERR_DATA_NOT_FOUND;

    strcpy(track,data->track1);
    p = strtok(&track[1],"^");

    if(p == NULL) return EWLDEMO_ERR_CARD_FORMAT;

    if(strlen(p) > (sizeof(pan) - 1)) return EWLDEMO_ERR_CARD_FORMAT;
    strcpy(pan,p);

    p = strtok(NULL,"^");

    if(p == NULL) return EWLDEMO_ERR_CARD_FORMAT;

    if(strlen(p) > (sizeof(name) - 1)) return EWLDEMO_ERR_CARD_FORMAT;
    strcpy(name,p);

    p = p + strlen(name) + 1;
    strncpy(additionalData,p,7);

    if(strlen(data->name) == 0) strcpy(data->name,name);

    if(strlen(data->pan) == 0) strcpy(data->pan,pan);

    convTxtToBcd(additionalData,4,data->expiration,TRANSACTION_EXPIRATION_LEN);

    return EWLDEMO_OK;
}
//------------------------------------------------------------------------------
int RecoverDataFromTrack2 (TransactionPaymentData_ST *data){

    char *p;
    char track[TRANSACTION_TRACK2_ASCII_LEN];
    char pan[19+1];
    char additionalData[7+1];

    if(strlen(data->track2) == 0) return EWLDEMO_ERR_DATA_NOT_FOUND;

    memset(pan,0x00,sizeof(pan));
    memset(additionalData,0x00,sizeof(additionalData));

    strcpy(track,data->track2);

    p = strtok(track,"=D");
    strcpy(pan,p);

    p = p + strlen(pan) + 1;
    strncpy(additionalData,p,7);

    if(strlen(data->pan) == 0) strcpy(data->pan,pan);

    convTxtToBcd(additionalData,4,data->expiration,TRANSACTION_EXPIRATION_LEN);

    return EWLDEMO_OK;

}

//------------------------------------------------------------------------------
int utilOpenTracks(TransactionPaymentData_ST *data){

    int ret;
    ret = RecoverDataFromTrack1(data);
    if(ret == EWLDEMO_OK) return EWLDEMO_OK;

    ret = RecoverDataFromTrack2(data);
    if(ret == EWLDEMO_OK) return EWLDEMO_OK;

    return EWLDEMO_ERR_INTERNAL;
}

//------------------------------------------------------------------------------
int utilFormatValue (char *formattedValue, unsigned long long amount)   {
    unsigned int digits;
    char aux[5];
    int len, lenAux;

    digits = amount % 100;
    len = sprintf (formattedValue, ",%02u", digits);
    amount /= 100;
    do   {
        digits = (amount % 1000);
        amount /= 1000;
        if (amount)  lenAux = sprintf (aux, ".%03u", digits);
        else         lenAux = sprintf (aux, "%u", digits);
        memmove (formattedValue + lenAux, formattedValue, len+1);
        memcpy (formattedValue, aux, lenAux);
        len += lenAux;
    }
    while (amount > 0);

    return len;
}

//------------------------------------------------------------------------------
int utilGetKey (uint8_t *nDigits, int *cont){

    int ret;
    unsigned char key;
    unsigned int evt = 0;

    ret = SEC_PinEntry(&evt, &key, cont);
    switch(ret){
        case 0:             break;
        case ERR_TIMEOUT:   return EWL_ERR_USER_TIMEOUT;
        default:            return EWL_ERR_INTERNAL;
    }

    switch(key){
        case 0x2A:
            if(*nDigits < 12) *nDigits = *nDigits + 1;
            break;

        case T_CORR:
            if(*nDigits > 0) *nDigits = *nDigits - 1;
            break;

        case T_VAL:
            if(*nDigits == 0) return EWL_ERR_USER_BY_PASS;
            if(*nDigits < 4) return 2;
            return 0;

        case T_ANN:
            return EWL_ERR_USER_CANCEL;

        case TIMEOUT:
            return EWL_ERR_USER_TIMEOUT;
    }

    if(cont) return 2;

    return EWL_ERR_INTERNAL;
}

//------------------------------------------------------------------------------
int utilGetPinScreenLines(char * msgTitle, char * msgLine1, char * msgLine2)
{
	TABLE_MERCHANT Merchant;
	char CurrencySymbol[6];
	int RetFun;

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);
	UI_ClearLine(UI_LINE5);

	strcpy( msgTitle, "PINPAD" );

	//--- Get merchant information
	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Get currency symbol
	memset(CurrencySymbol, 0, sizeof(CurrencySymbol));
	if( Merchant.Currency == 1 )
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Local, LZ_TERM_CURR_SYMBOL);
	else
		memcpy(CurrencySymbol, tTerminal.CurrSymbol_Dollar, LZ_TERM_CURR_SYMBOL);

	strcpy(msgLine2, "INGRESE CLAVE");

	return RET_OK;

LBL_ERROR:

	return RET_NOK;
}

//------------------------------------------------------------------------------
int utilGetPinOnline(TLV_TREE_NODE Tree)
{
	TABLE_HOST Host;
	int RetFun;

	CRYPTOLIB_PIN_PARAMETERS PINEntryParams;
	CRYPTOLIB_PIN_GENERAL_PARAMS GeneralPINEntryParams;
	TABLE_RANGE Range;

	TABLE_KEY_INFO stPinKeyInfo;

	RetFun = TableTerminal_GetKeyInfo(KEY_TYPE_PIN, &stPinKeyInfo);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	memset( &PINEntryParams, 0x00, sizeof(PINEntryParams) );
	memset( &GeneralPINEntryParams, 0x00, sizeof(GeneralPINEntryParams));

	PINEntryParams.PINGeneralParams = &GeneralPINEntryParams;

	//--- Get Merchant info
	memset((char*)&Range, 0, L_TABLE_RANGE);
	RetFun = Get_TransactionRangeData(&Range, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Get host info
	memset((char*)&Host, 0, L_TABLE_HOST);
	RetFun = Get_TransactionHostData(&Host, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//--- Set PAN
	RetFun = Tlv_GetTagValue(Tree, TAG_PAN, GeneralPINEntryParams.PAN, NULL);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	PINEntryParams.stKeyInfo.cAlgoType = PINEntryParams.stMasterKeyInfo.cAlgoType = stPinKeyInfo.AlgoType;
	PINEntryParams.stKeyInfo.iSecretArea = PINEntryParams.stMasterKeyInfo.iSecretArea = stPinKeyInfo.SecretArea;
	PINEntryParams.stKeyInfo.uiBankId = PINEntryParams.stMasterKeyInfo.uiBankId = stPinKeyInfo.BankId;

	if( stPinKeyInfo.AlgoType == TLV_TYPE_DESDUKPT ||
		stPinKeyInfo.AlgoType == TLV_TYPE_TDESDUKPT ||
		stPinKeyInfo.AlgoType == TLV_TYPE_DESDUKPTLIGHT ||
		stPinKeyInfo.AlgoType == TLV_TYPE_TDESDUKPTLIGHT
		)
	{
		PINEntryParams.stKeyInfo.usNumber = stPinKeyInfo.MKNumber;
	}
	else
	{
		PINEntryParams.stMasterKeyInfo.usNumber = stPinKeyInfo.MKNumber;
		PINEntryParams.stKeyInfo.usNumber = stPinKeyInfo.WKNumber;

		//--- Set EncryptedWorkingKey
		memcpy(GeneralPINEntryParams.EncryptedWorkingKey, (uint8*)Host.EncrypedWorkingKey1, 16);
		if(Host.Flags1 & HOST_FLAG1_USE_3DES)
			memcpy(&GeneralPINEntryParams.EncryptedWorkingKey[8], (uint8*)Host.EncrypedWorkingKey2, 16);
	}

	//--- Set Min & Max digits
	GeneralPINEntryParams.MinDigits = 4;
	GeneralPINEntryParams.MaxDigits = 6;
	GeneralPINEntryParams.iFirstCharTimeOut = tConf.TimeOut;
	GeneralPINEntryParams.iInterCharTimeOut = tConf.TimeOut;

	//--- Set line messages
	utilGetPinScreenLines(GeneralPINEntryParams.Title, GeneralPINEntryParams.Line1, GeneralPINEntryParams.Line2);

	//--- Get PINBLOCK API
	RetFun = CryptoLib_ShowPINEntry(&PINEntryParams);
	CHECK(RetFun == RET_OK, LBL_PIN_ENTRY_ERROR);

	//--- Check if PINBLOCK is NULL, and cancel step
	CHECK( memcmp( GeneralPINEntryParams.PinBlock, "\x00\x00\x00\x00\x00\x00\x00\x00", 8 ) != 0,
			LBL_PIN_ENTRY_ERROR );

	RetFun = Tlv_SetTagValue(Tree, TAG_PINBLOCK, GeneralPINEntryParams.PinBlock, 8);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// get host data
	RetFun = Get_TransactionHostData(&Host, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	if( stPinKeyInfo.AlgoType == TLV_TYPE_DESDUKPT ||
		stPinKeyInfo.AlgoType == TLV_TYPE_TDESDUKPT ||
		stPinKeyInfo.AlgoType == TLV_TYPE_DESDUKPTLIGHT ||
		stPinKeyInfo.AlgoType == TLV_TYPE_TDESDUKPTLIGHT
		)
	{
		RetFun = Tlv_SetTagValue(Tree, TAG_DUKPT_KSN, GeneralPINEntryParams.DUKPT_KSN, 10);
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}

	return RET_OK;

LBL_ERROR:

	return RET_NOK;

LBL_PIN_ENTRY_ERROR:

	return RET_CANCEL;
}

//------------------------------------------------------------------------------
void utilWaitToRemoveChipCard(void)
{
	Telium_File_t *hChip 	= NULL;
	uint8 state;

	hChip = Telium_Fopen("CAM0", "r*");
	if(hChip == NULL)
		return;

	Telium_Status(hChip, &state);

	UI_ClearAllLines();
	UI_SetLine(UI_LINE2, "RETIRE LA TARJETA", UI_ALIGN_CENTER);
	while(state & CAM_PRESENT)
	{
		beepOK();
		UI_ShowCleanMessage(1);

		Telium_Status(hChip, &state);
	}

	Telium_Fclose(hChip);
}

//------------------------------------------------------------------------------
void utilShowMsgExternalPP(char * MsgLine1, char * MsgLine2, uint8 ClearAll)
{
	if(ClearAll)
		PPS_clear_display();

	PPS_firstline();
	if(MsgLine1 != NULL && MsgLine1[0] != NULL)
		PPS_Display(MsgLine1);

	PPS_newline();
	if(MsgLine2 != NULL && MsgLine2[0] != NULL)
		PPS_Display(MsgLine2);
}

//------------------------------------------------------------------------------
int utilInsertEMVTags(TLV_TREE_NODE ParentNode, TLV_TREE_NODE EmvTags)
{
	TLV_TREE_NODE node2, node3;
	uint32 Tag;

	// Copy all tags to TAG_EMV_F55TOHOST node
	node2 = TlvTree_GetFirstChild( EmvTags );
	CHECK( node2 != NULL, lbl_STOP1 );

	while (node2 != NULL)
	{
		Tag = TlvTree_GetTag( node2 );
		// Copy tag
		node3 = TlvTree_AddChild(ParentNode, TlvTree_GetTag( node2 ), TlvTree_GetData( node2 ), TlvTree_GetLength( node2 ));
		CHECK(node3 != NULL, lbl_STOP1);

		// select next tag
		node2 = TlvTree_GetNext( node2 );
	}

	return 0;

	lbl_STOP1:
	return -1;
}
