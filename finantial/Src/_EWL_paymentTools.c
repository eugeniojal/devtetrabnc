//#include "ewlDemo.h"
//#include "paymentTools.h"
//#include "dspEdit.h"
//#include "dspMenu.h"
//#include "callbacks.h"
#include "iPOSApp.h"

/* --------------------------------------------------------------------------
 * Function Name:	paymentToolsIsChipMagnetic
 * Description:		Detect if payment is chip magnetic(MAG-STRIPE)
 * Author:			@jbotero
 * Parameters:
 * 	- ewlTechnology_t technology
 * Return:
 * 	- TRUE: Is Chip card magnetic
 * 	- FALSE: Isn't
 * Notes:
 */

bool paymentToolsIsChipMagnetic (ewlTechnology_t technology){

    #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    if(technology == EWL_TECHNOLOGY_EXPRESSPAY_MAG) return TRUE;
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYPASS
    if(technology == EWL_TECHNOLOGY_PAYPASS_MAG) return TRUE;
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYWAVE
    if(technology == EWL_TECHNOLOGY_MSD) return TRUE;
    #endif

    return FALSE;
}

/* --------------------------------------------------------------------------
 * Function Name:	paymentToolsIsCless
 * Description:		Detect if payment is contact-less
 * Author:			@jbotero
 * Parameters:
 * 	- ewlTechnology_t technology
 * Return:
 * 	- TRUE: Is
 * 	- FALSE: Isn't
 * Notes:
 */

bool paymentToolsIsCless (ewlTechnology_t technology){

    #ifdef EWL_ENABLE_KERNEL_EXPRESSPAY
    if(technology == EWL_TECHNOLOGY_EXPRESSPAY_MAG) return TRUE;

    if(technology == EWL_TECHNOLOGY_EXPRESSPAY_EMV) return TRUE;
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYPASS
    if(technology == EWL_TECHNOLOGY_PAYPASS_MAG) return TRUE;

    if(technology == EWL_TECHNOLOGY_PAYPASS_MCHIP) return TRUE;
    #endif

    #ifdef EWL_ENABLE_KERNEL_PAYWAVE
    if(technology == EWL_TECHNOLOGY_MSD) return TRUE;

    if(technology == EWL_TECHNOLOGY_QVSDC) return TRUE;
    #endif

    return FALSE;
}
