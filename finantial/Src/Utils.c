/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Utils.c
 * Header file:		Utils.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"
#include "tlvVar_def.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
int CheckTrack2(void);
int CheckTrack1(void);

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
//+ BATTERY01 @jbotero 15/01/2016
#define 	PORCENTAJE_MINIMO_BATERIA	5
//- BATTERY01

/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/
/* --------------------------------------------------------------------------
 * Function Name:	Ascii_to_Hex
 * Description:		Convert Ascii number to Hex number
 * 					Example: to convert "54732" to 0x05 0x47 0x32
 * Parameters:
 *  - uint8 *HexBuffer - pointer to hex buffer (out)
 *  - uint8* AsciiBuffer - pointer to ascii buffer (in)
 *  - uint32 AsciiLength - length of ascii buffer (in) Don't include 0
 * Return:
 *  RET_OK if conversion is OK
 *  other value if error occurred
 * Notes:
 */
int Ascii_to_Hex(uint8 *HexBuffer, uint8* AsciiBuffer, uint32 AsciiLength)
{
	int ReturnValue;
	int lAsciiLen = AsciiLength;
	uint8 *lAsciiBuffer;
	if((AsciiLength % 2) == 1)
		lAsciiLen++;

	lAsciiBuffer = umalloc(lAsciiLen + 1);
	memset(lAsciiBuffer, '0', lAsciiLen);
	lAsciiBuffer[lAsciiLen] = 0;

	memcpy(&lAsciiBuffer[lAsciiLen - AsciiLength], AsciiBuffer, AsciiLength);

	ReturnValue = USQ_Aschex_alphanum(HexBuffer, lAsciiBuffer, lAsciiLen);

	ufree(lAsciiBuffer);

	return ReturnValue;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetTrack2
 * Description:		Get track2 from swipe2 file, and extract track fields
 * Parameters:
 *  - Telium_File_t *hSwipe2 - pointer to swipe2 file
 * Return:
 *  - RET_OK - the track is correct and put in Tree_Tran
 *  - RET_NOK - cannot read track
 * Notes:
 */
int GetTrack2(uint8 *tcTmp)
{
	TLV_TREE_NODE TLVT_Node;
	uint8 tcTrk2[128];
	uint8 *pcSrc;
	uint8 *pcDst;
	int ISO_Ret;
	uint8 TrkLen;

	memset( tcTrk2, 0, 128 );
	TrkLen = 0;

	pcSrc = tcTmp;
	pcDst = tcTrk2;
	// Find start sentinel
	while(*pcSrc)
	{
		if(*pcSrc++ == 'B')
			break;
	}
	// Copy all data between start and end sentinels
	while(*pcSrc)
	{
		if(*pcSrc == 'F')
			break;
		if(*pcSrc == 'D')
			*pcSrc = '=';
		*pcDst++ = *pcSrc++;
		TrkLen++;
	}

	TLVT_Node = TlvTree_Find(Tree_Tran, TAG_TRACK2, 0);
	if( TLVT_Node == NULL )
	{
		// Add tag to tlvtree
		TLVT_Node = TlvTree_AddChild(Tree_Tran, TAG_TRACK2, tcTrk2, TrkLen);
		CHECK(TLVT_Node != NULL, LBL_ERROR);
	}
	else
	{
		// Modify the tag
		TlvTree_SetData(TLVT_Node, tcTrk2, TrkLen);
	}


	ISO_Ret = CheckTrack2();
	CHECK(ISO_Ret == RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	CheckTrack2
 * Description:		Check and extract field from Track 2
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - Track 2 is correct
 *  - RET_NOK - Track 2 is not correct
 * Notes:
 */
int CheckTrack2( void )
{
	TLV_TREE_NODE TLVT_Node;
	uint8 *Trk;
	uint8 TrkLen;
	uint8 *FS;
	uint8 FSIndex;

	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_TRACK2, 0 );

	if( TLVT_Node == NULL )
		return RET_NOK;

	Trk = TlvTree_GetData( TLVT_Node );
	TrkLen = TlvTree_GetLength( TLVT_Node);

	FS = memchr( Trk, '=', TrkLen );

	if( FS == NULL )
		return RET_NOK;

	FSIndex = FS - Trk;

	// Add PAN
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_PAN, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_PAN, Trk, FSIndex );
	else
		TlvTree_SetData( TLVT_Node, Trk, FSIndex );

	// Add Expiration Date
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_EXP_DATE, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_EXP_DATE, &Trk[FSIndex + 1], 4 );
	else
		TlvTree_SetData( TLVT_Node, &Trk[FSIndex + 1], 4 );

	// Add Service Code
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_SERVICE_CODE, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_SERVICE_CODE, &Trk[FSIndex + 5], 3 );
	else
		TlvTree_SetData( TLVT_Node, &Trk[FSIndex + 5], 3 );

	return RET_OK;

}


/* --------------------------------------------------------------------------
 * Function Name:	GetTrack1
 * Description:		Get track1 from swipe31 file, and extract track fields
 * Parameters:
 *  - Telium_File_t *hSwipe2 - pointer to swipe2 file
 * Return:
 *  - RET_OK - the track is correct and put in Tree_Tran
 *  - RET_NOK - cannot read track
 * Notes:
 */
int GetTrack1(uint8 *tcTmp)
{
	TLV_TREE_NODE TLVT_Node;
	uint8 tcTrk1[128];
	uint8 *pcSrc;
	uint8 *pcDst;
	uint8 TrkLen;
	int RetFun;

	memset( tcTrk1, 0, 128 );

	pcSrc = tcTmp;
	pcDst = tcTrk1;

	// Find start sentinel
	while(*pcSrc)
	{
		if(*pcSrc++ == '%')
			break;
	}
	// Copy all data between start and end sentinels

	TrkLen = 0;
	while(*pcSrc)
	{
		if(*pcSrc == '?')
			break;
		*pcDst++ = *pcSrc++;
		TrkLen++;
	}

	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_TRACK1, 0 );
	if( TLVT_Node == NULL )
	{
		// Add tag to tlvtree
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_TRACK1, tcTrk1, TrkLen );
		CHECK(TLVT_Node != NULL, LBL_ERROR);
	}
	else
		// Modify the tag
		TlvTree_SetData( TLVT_Node, tcTrk1, TrkLen );

	RetFun = CheckTrack1();
	CHECK(RetFun == RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

}


/* --------------------------------------------------------------------------
 * Function Name:	CheckTrack1
 * Description:		Check and extract field from Track 1
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - Track 2 is correct
 *  - RET_NOK - Track 2 is not correct
 * Notes:
 */
int CheckTrack1(void)
{
	TLV_TREE_NODE TLVT_Node;
	uint8 *Trk;
	uint8 TrkLen;

	uint8 *FC;
	uint8 *FS1;
	uint8 *FS2;

	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_TRACK1, 0 );

	if( TLVT_Node == NULL )
		return RET_NOK;

	Trk = TlvTree_GetData( TLVT_Node );
	TrkLen = TlvTree_GetLength( TLVT_Node);

	// Find track 1 format code
	FC = memchr( Trk, 'B', TrkLen );
	if( FC == NULL )
		FC = Trk;
	else
		FC++;

	FS1 = memchr( Trk, '^', TrkLen );
	if( FS1 == NULL )
		return RET_NOK;

	FS2 = memchr( FS1 + 1, '^', TrkLen );
	if( FS2 == NULL )
		return RET_NOK;

	// Add Card holder name
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_CARDHOLDER_NAME, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_CARDHOLDER_NAME, FS1 + 1, FS2 - FS1 - 1 );
	else
		TlvTree_SetData( TLVT_Node, FS1 + 1, FS2 - FS1 - 1 );

	// Add PAN if no exists
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_PAN, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_PAN, FC, FS1 - FC );

	// Add Expiration Date if no exists
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_EXP_DATE, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_EXP_DATE, FS2 + 1, 4 );

	// Add Service Code if no exists
	TLVT_Node = TlvTree_Find( Tree_Tran, TAG_SERVICE_CODE, 0 );
	if( TLVT_Node == NULL )
		TLVT_Node = TlvTree_AddChild( Tree_Tran, TAG_SERVICE_CODE, FS2 + 5, 3 );

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Get_TransactionRangeData
 * Description:		get the RangeDate of a specific transaction using TAG_RANGE_ID
 * Parameters:
 *  - TABLE_RANGE *Range - (OUT) where to put data, NULL if record didn't find
 *  - TLV_TREE_NODE Tree - tree where to find TAG_RANGE_ID
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - cannot get range data
 * Notes:
 */
int Get_TransactionRangeData(TABLE_RANGE *Range, TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 RecordID;
	int RetFun;

	// get TAG_RANGE_ID from Tree
	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&RecordID, TlvTree_GetData(node), TlvTree_GetLength(node));

	// data record data
	memset((char*)Range, 0, L_TABLE_RANGE);
	RetFun = TM_FindRecord(TAB_RANGE, Range, RecordID);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Get_TransactionBankData
 * Description:		get the BankDate of a specific transaction using TAG_BANK_TYPE

 * Notes:
 */
int Get_TransactionBankData(TABLE_BANK *Bank, TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 RecordID;
	int RetFun;

	// get TAG_RANGE_ID from Tree
	node = TlvTree_Find(Tree, TAG_BANK_TYPE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&RecordID, TlvTree_GetData(node), TlvTree_GetLength(node));

	// data record data
	memset((char*)Bank, 0, L_TABLE_BANK);
	RetFun = TM_FindRecord(TAB_BANK, Bank, RecordID);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
/* --------------------------------------------------------------------------
 * Function Name:	Get_TransactionMerchantData
 * Description:		get the Merchant Data of a specific transaction using TAG_MERCHANT_ID
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - (OUT) where to put data, NULL if record didn't find
 *  - TLV_TREE_NODE Tree - tree where to find TAG_MERCHANT_ID
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - cannot get merchat data
 * Notes:
 */
int Get_TransactionMerchantData(TABLE_MERCHANT *Merchant, TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 RecordID;
	int RetFun;

	// get TAG_MERCHANT_ID from Tree
	node = TlvTree_Find(Tree, TAG_MERCHANT_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&RecordID, TlvTree_GetData(node), TlvTree_GetLength(node));

	// data record data
	memset((char*)Merchant, 0, L_TABLE_MERCHANT);
	RetFun = TM_FindRecord(TAB_MERCHANT, Merchant, RecordID);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Get_TransactionHostData
 * Description:		get the Host Data of a specific transaction using TAG_HOST_ID
 * Parameters:
 *  - TABLE_HOST *Host - (OUT) where to put data, NULL if record didn't find
 *  - TLV_TREE_NODE Tree - tree where to find TAG_HOST_ID
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - cannot get host data
 * Notes:
 */
int Get_TransactionHostData(TABLE_HOST *Host, TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 RecordID;
	int RetFun;

	// get TAG_HOST_ID from Tree
	node = TlvTree_Find(Tree, TAG_HOST_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	RecordID = *(uint32*)TlvTree_GetData(node);
	//memcpy((char*)&RecordID, TlvTree_GetData(node), LTAG_UINT32);

	// data record data
	memset((char*)Host, 0, L_TABLE_HOST);
	RetFun = TM_FindRecord(TAB_HOST, Host, RecordID);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	ClearCardData
 * Description:		save initial tags, clear Tree_Tran and restore initial tags
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - everything is OK
 *  - RET_NOK - error occurred
 * Notes:
 */
int ClearCardData(void)
{
	uint16 TranID;
	uint16 TranName;
	uint16 TranOptios;
	uint32 MerchGroupID;
	TLV_TREE_NODE node;

	// Save initial tags
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), TlvTree_GetLength(node));
	//
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_NAME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranName, TlvTree_GetData(node), TlvTree_GetLength(node));

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranOptios, TlvTree_GetData(node), TlvTree_GetLength(node));

	node = TlvTree_Find(Tree_Tran, TAG_MERCH_GROUP_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&MerchGroupID, TlvTree_GetData(node), TlvTree_GetLength(node));

	// release actual tree
	TlvTree_Release(Tree_Tran);
	Tree_Tran = NULL;

	// create a new tree
	Tree_Tran = TlvTree_New(0);
	CHECK(Tree_Tran != NULL, LBL_ERROR)

	// restore initial tags
	node = TlvTree_AddChild(Tree_Tran, TAG_TRAN_ID, &TranID, LTAG_UINT16);
	CHECK(node != NULL, LBL_ERROR);

	node = TlvTree_AddChild(Tree_Tran, TAG_TRAN_NAME, &TranName, LTAG_UINT16);
	CHECK(node != NULL, LBL_ERROR);

	node = TlvTree_AddChild(Tree_Tran, TAG_TRAN_OPTIOS, &TranOptios, LTAG_UINT16);
	CHECK(node != NULL, LBL_ERROR);

	node = TlvTree_AddChild(Tree_Tran, TAG_MERCH_GROUP_ID, &MerchGroupID, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	UI_ClearLine(UI_LINE1);
	UI_ClearLine(UI_LINE2);
	UI_ClearLine(UI_LINE3);
	UI_ClearLine(UI_LINE4);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	Uint64_to_Ascii
 * Description:		Convert uint64 to string, trimmed or justified with fill char
 * Parameters:
 *  - uint64 pSurce	- number to be converter
 *  - char *pDest - where to place the number converted
 *  - CONVERT_TYPE pFormat - _UTIL_TRIM, _UTIL_RIGHT _UTIL_LEFT
 *  - uint16 pLength - length to be clear and fill with pFill
 *  - uint8 pFill - char, character to fill
 * Return:
 * Notes:
 */
int Uint64_to_Ascii(uint64 pSurce, char *pDest, CONVERT_TYPE pFormat, uint16 pLength, uint8 pFill)
{
	char TempAux[20], TempAux2[20];
	uint64 temp = pSurce;
	uint64 Rmod= 0;
	uint16 i = 0;
	uint16 Rlen=0;

	// Clear buffers
	memset(TempAux, 0x00, sizeof(TempAux));
	memset(TempAux2, 0x00, sizeof(TempAux2));

	// Covert number to string
	if (temp == 0)
	{
		TempAux2[0] = 0x30;
		Rlen = 1;
	}
	else
	{
		while (temp > 0 )
		{
			Rmod = temp % 10;
			TempAux[i] = 0x30 + Rmod;
			i+=1;

			temp = temp / 10;
		}
		Rlen = strlen(TempAux);
		for (i = 0; i < Rlen; i++)
		{
			TempAux2[Rlen -1 -i ] = TempAux[i];
		}
	}

	memset( pDest, 0, pLength );

	switch( pFormat )
	{
		case _FORMAT_TRIM:
			memcpy( pDest, TempAux2, Rlen);
			break;
		case _FORMAT_RIGHT:
			if( pLength == 0 ) return 0;
			if( Rlen > pLength ) return 0;
			memset( pDest, pFill, pLength);
			memcpy( &pDest[pLength-Rlen], TempAux2, Rlen);
			Rlen = pLength;
			break;
		case _FORMAT_LEFT:
			if( pLength == 0 ) return 0;
			if( Rlen > pLength ) return 0;
			memset( pDest, pFill, pLength);
			memcpy( pDest, TempAux2, Rlen);
			Rlen = pLength;
			break;
		default:
			return 0;
	}

	return Rlen;
}

/** ------------------------------------------------------------------------------
 * Function Name:	Format_String_Lenght
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Create a string whith lenght format
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK
 *  - (-1) pLength = 0
 *  - (-2) pFormat unknown
 *  - (-3) pLength < lenght of pSurce
 * Notes:
 */
int Format_String_Lenght(char *pSurce, char *pDest, CONVERT_TYPE pFormat, uint16 pLength, uint8 pFill)
{
	uint16 Rlen=0;

	memset(pDest, 0, pLength+1);

	CHECK(pLength != 0,   LBL_LENGHT_ERROR);

	Rlen = strlen(pSurce);
	CHECK(Rlen<=pLength, LBL_STRINGLEN_ERROR);

	switch( pFormat )
	{
		case _FORMAT_RIGHT:
			memset( pDest, pFill, pLength);
			memcpy( &pDest[pLength-Rlen], pSurce, Rlen);
			break;
		case _FORMAT_LEFT:
			memset( pDest, pFill, pLength);
			memcpy( pDest, pSurce, Rlen);
			break;
		default:
			goto LBL_FORMAT_UNKNOWN;
	}


	return RET_OK;

	LBL_LENGHT_ERROR:
		return -1;
	LBL_FORMAT_UNKNOWN:
		return -2;
	LBL_STRINGLEN_ERROR:
		memcpy( pDest, pSurce, pLength);
		return -3;
}

/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
int Format_Amount(amount Source, char *Dest, char *Currency, bool Negative)
{
	char TmpBuffer[25];
	int TmpIndex;
	char ConvertBuffer[25];
	int ConvertLen;
	int ConvertIndex;
	int GroupCont;
	char FormatAmount[25];
	int FormatIndex;

	// convert Source to string
	memset(ConvertBuffer, 0, 25);
	ConvertLen = Uint64_to_Ascii(Source, ConvertBuffer, _FORMAT_TRIM, 0, 0);
	ConvertIndex = ConvertLen;

	// init TmpBuffer
	memset(TmpBuffer, 0, 25);
	memcpy(TmpBuffer, "00.0", 4);

	if(ConvertLen == 1)
	{
		TmpBuffer[0] = ConvertBuffer[0];
	}
	else if (ConvertLen == 2)
	{
		TmpBuffer[0] = ConvertBuffer[1];
		TmpBuffer[1] = ConvertBuffer[0];
	}
	else
	{
		TmpBuffer[0] = ConvertBuffer[ConvertLen-1];
		TmpBuffer[1] = ConvertBuffer[ConvertLen-2];

		ConvertIndex-=3;
		TmpIndex = 3;
		GroupCont = 0;

		do
		{
			if(GroupCont == 3)
			{
				GroupCont = 1;
				TmpBuffer[TmpIndex] = ',';
				TmpIndex++;
			}
			else
				GroupCont++;

			TmpBuffer[TmpIndex] = ConvertBuffer[ConvertIndex];
			ConvertIndex--;
			TmpIndex++;
		}while(ConvertIndex >= 0);
	}

	// swipe TmpBuffer in FormatAmount
	memset(FormatAmount, 0, 25);
	FormatIndex = 0;
	ConvertIndex = strlen(TmpBuffer) - 1;
	do
	{
		FormatAmount[FormatIndex] = TmpBuffer[ConvertIndex];
		FormatIndex++;
		ConvertIndex--;
	}while(ConvertIndex >= 0);

	// add negative sing if necessary
	if(Negative)
		strcat(Dest, "-");
	strcat(Dest, Currency);
	strcat(Dest, FormatAmount);

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	GetSampleTransactionData
 * Description:		Open transaction data stored in /HOST/TRAN.XML and
 * 					perform a tlvtree unserialize in Tree
 * Parameters:
 *  - TLV_TREE_NODE *Tree - Pointer to tlvtree
 * Return:
 *  - RET_OK - transaction data is restored OK
 *  - RET_NOK - cannot restore transaction data
 * Notes:
 */
int GetSampleTransactionData(TLV_TREE_NODE *Tree)
{
	int FileLen;
	unsigned int Mode;
	S_FS_FILE *hFile =  NULL;
	uint8 *tmpBuffer = NULL;
	int RetFun;

	// mount host folder
	RetFun = FS_mount("/HOST", &Mode);
	// open TRAN.XML
	hFile = FS_open("/HOST/TRAN.XML", "r" );
	CHECK(hFile != NULL, LBL_ERROR)
	// get file length
	FileLen = FS_length(hFile);
	// reserve memory for tmpBuffer
	tmpBuffer = umalloc(FileLen);
	CHECK(tmpBuffer != NULL, LBL_ERROR);
	// read file data
	FS_read(tmpBuffer, FileLen, 1, hFile);
	// close file
	RetFun = FS_close (hFile);

	// reset Tree_Tran if necessary
	if(*Tree != NULL)
	{
		TlvTree_Release(*Tree);
		*Tree = NULL;
	}
	// Create memory for Tree_Tran
	Tree_Tran = TlvTree_New(0);
	CHECK(Tree_Tran != NULL, LBL_ERROR);
	// Unserialize buffer
	RetFun = TlvTree_Unserialize(Tree, TLV_TREE_SERIALIZER_XML, tmpBuffer, FileLen);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

	// free memory
	ufree(tmpBuffer);
	// umoun host
	RetFun = FS_unmount ("/HOST");

	return RET_OK;

LBL_ERROR:
	RetFun = FS_unmount ("/HOST");
	ufree(tmpBuffer);
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	GetEMVTrack2
 * Description:		Get EMV Tags fon EMV transaction of EMVCUSTOM
 * Parameters:
 *  - TLV_TREE_NODE *EMVTranInfo - Emv data node
 * Return:
 *  - RET_OK - EMV Data OK
 *  - RET_NOK - EMV Data NOK
 * Notes:
 */
int GetEMVTrack2(TLV_TREE_NODE EMVTranInfo)
{
	TLV_TREE_NODE Find_Node, Find_Node2;
	TLV_TREE_NODE FieldNode;
	char tempf[100];
	int ISO_Ret;
	DataElement ti;

	// Track 2
	memset(tempf, 0x00, 100);
	Find_Node = TlvTree_Find( EMVTranInfo, TAG_EMV_TRACK_2_EQU_DATA, 0 );
	Hexasc((uint8*)tempf, TlvTree_GetData( Find_Node ), TlvTree_GetLength( Find_Node ) * 2 );

	// Add tag to tlvtree
	//+ EMV10 @mamata 4/05/2016
	//d FieldNode = TlvTree_AddChild(Tree_Tran, TAG_TRACK2, tempf, (TlvTree_GetLength( Find_Node ) * 2 ) - 1 );
	if(tempf[strlen(tempf)-1] == '?')
	{
		tempf[strlen(tempf)-1] = 0;
	}
	FieldNode = TlvTree_AddChild(Tree_Tran, TAG_TRACK2, tempf, strlen(tempf));
	//- EMV10
	CHECK(FieldNode != NULL, LBL_TREE_ERROR);

	// Check Tranck 2
	ISO_Ret = CheckTrack2();
	CHECK(ISO_Ret == RET_OK, LBL_TREE_ERROR);

	// Application Label
	Find_Node = TlvTree_Find( EMVTranInfo, TAG_EMV_APPLICATION_LABEL, 0 );
	if (Find_Node != NULL)
	{
		// Add application label
		FieldNode = TlvTree_AddChild(Tree_Tran, TAG_EMV_APP_LBL, TlvTree_GetData( Find_Node ), TlvTree_GetLength( Find_Node ));
		CHECK(FieldNode != NULL, LBL_TREE_ERROR);
	}

	// Check for application preferred name
	Find_Node = TlvTree_Find( EMVTranInfo, TAG_EMV_APPLI_PREFERRED_NAME, 0 );
	if (Find_Node != NULL)
	{
		// check table index is 0x01
		Find_Node2 = TlvTree_Find( EMVTranInfo, TAG_EMV_ISSUER_CODE_TABLE_INDEX, 0 );
		if (Find_Node2 != NULL)
		{
			ti.ptValue =  TlvTree_GetData( Find_Node2 );


			if (*ti.ptValue == 0x01)
			{
				// Upadate application label
				TlvTree_SetData( FieldNode, TlvTree_GetData( Find_Node ), TlvTree_GetLength( Find_Node ));
				CHECK(FieldNode != NULL, LBL_TREE_ERROR);
			}

		}

	}


	return RET_OK;

	LBL_TREE_ERROR: // cannot assign memory to Tree_Tran
		return -1;
}

/* --------------------------------------------------------------------------
 * Function Name:	GetTotalAmount
 * Description:		Get total amount of Tree_Tran
 * Parameters:
 * Return:
 *  amount total amount
 * Notes:
 */
amount GetTotalAmount()
{
	TLV_TREE_NODE node;
	amount Amount;
	amount Total = 0;
	int16 TranStatus;

	// Set Base Amount line
	// get TAG_BASE_AMOUNT from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
	if (node != NULL)
	{
		memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
		Total+=Amount;
	}

	// Set Tax Amount line
	node = TlvTree_Find(Tree_Tran, TAG_TAX_AMOUNT, 0);
	if(node != NULL)
	{
		memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
		Total+=Amount;
	}


	node = TlvTree_Find(Tree_Tran, TAG_TRAN_STATUS, 0);
	//CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	if(!(TranStatus & FSTATUS_ADJUSTED)){
		// Set Tip Amount line
		node = TlvTree_Find(Tree_Tran, TAG_TIP_AMOUNT, 0);
		if(node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
		}
	}

	return Total;

}


/* --------------------------------------------------------------------------
 * Function Name:	FormatPAN
 * Description:		Format PAN with mask configured in Terminal table
 * Parameters:
 *  - uint8 *PAN - clear PAN
 *  - uint8 *FormatPAN - where to put format PAN
 * Return:
 *  - RET_OK
 * Notes:
 */
int FormatPAN(uint8 *PAN, uint8 *FormatPAN)
{
	int PANLength;

	// get PAN length
	PANLength = strlen((char*)PAN);

	if(PANLength > 19)
		PANLength = 19;

	if ( memcmp( tTerminal.PanMask, "####", 4 ) == 0 )
	{
		memcpy( FormatPAN, PAN, PANLength );
		return RET_OK;
	}

	// Mask all PAN
	memset((char*)FormatPAN, '*', PANLength);

	// Show first 4 digits, if is necessary
	if(tTerminal.PanMask[0] == '#')
		memcpy(FormatPAN, PAN, 4 );

	// Show last 4 digits, if is necessary
	if(tTerminal.PanMask[3] == '#')
		memcpy( &FormatPAN[PANLength - 4], &PAN[PANLength - 4], 4);

	return RET_OK;
}


//+ EMV08 @mamata Jun 10, 2015
/* --------------------------------------------------------------------------
 * Function Name:	SetIdleHeaderFooter
 * Description:		Turn on idle header and footer
 * Parameters:
 *  none
 * Return:
 *  none
 * Notes:
 */
void SetIdleHeaderFooter(void)
{
	DisplayHeader(_ON_);
	if(IsColorDisplay())
		DisplayFooter(_ON_);
}
//- EMV08

//+ INGESTATE01 @jbotero 13/09/2015
/* --------------------------------------------------------------------------
 * Function Name:	XmlFileTlv_To_Tree
 * Description:		Read a file with TLV Tree format and unserialize on Tree.
 * Author:			@dvelazco, @jbotero
 * Parameters:		TLV_TREE_NODE *Tree: Tree where data will be loaded.
 * 					char *Disk: Disk path where XML file is located. Ex: "/HOST"
 * 					char *FileName: XML TLV File name. Ex: "/FILE.XML"
 * Return:
 * Notes:
 */

int XmlFileTlv_To_Tree(TLV_TREE_NODE *Tree, char *Disk, char *FileName)
{
	int FileLen;
	unsigned int Mode;
	S_FS_FILE *hFile =  NULL;
	uint8 *tmpBuffer = NULL;
	int RetFun;
	char FullPath[100] = {0x00, };

	strcat( FullPath, Disk );
	strcat( FullPath, FileName );

	// mount host folder
	RetFun = FS_mount(Disk, &Mode);
	CHECK(RetFun == FS_OK, LBL_ERROR);

	// open File
	hFile = FS_open(FullPath, "r" );
	CHECK(hFile != NULL, LBL_ERROR)

	// get file length
	FileLen = FS_length(hFile);

	// reserve memory for tmpBuffer
	tmpBuffer = umalloc(FileLen);
	CHECK(tmpBuffer != NULL, LBL_ERROR);

	// read file data
	FS_read(tmpBuffer, FileLen, 1, hFile);

	// close file
	RetFun = FS_close (hFile);

	// reset Tree if necessary
	if(*Tree != NULL)
	{
		TlvTree_Release(*Tree);
		*Tree = NULL;
	}

	// Unserialize buffer
	RetFun = TlvTree_Unserialize(Tree, TLV_TREE_SERIALIZER_XML, tmpBuffer, FileLen);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

	// free memory
	if(tmpBuffer!=NULL)
		ufree(tmpBuffer);

	// umoun host
	FS_unmount (Disk);

	return RET_OK;

LBL_ERROR:

	FS_unmount (Disk);

	if(tmpBuffer!=NULL)
		ufree(tmpBuffer);

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Tree_To_XmlFileTlv
 * Description:		Read a Tree and serialize on file using TLV Tree format.
 * Author:			@dvelazco, @jbotero
 * Parameters:		TLV_TREE_NODE *Tree: Tree where data will be loaded.
 * 					char *Disk: Disk path where XML file is located. Ex: "/HOST"
 * 					char *FileName: XML TLV File name. Ex: "/FILE.XML"
 * Return:
 * Notes:
 */

int Tree_To_XmlFileTlv(TLV_TREE_NODE Tree, char *Disk, char *FileName)
{
	S_FS_FILE * hFile;
	unsigned int AccMode;
	uint32 SizeTree = 0;
	uint8 * pBufferTree = NULL;
	int32 iRet;
	char FullPath[100] = {0x00, };

	strcat( FullPath, Disk );
	strcat( FullPath, FileName );

	//Mount DISK
	iRet = FS_mount( Disk, &AccMode );
	CHECK( iRet == FS_OK, LBL_ERROR );

	//Remove old files
	iRet = FS_unlink(FullPath);

	//+ Serialization

	SizeTree = TlvTree_GetSerializationSize( Tree, TLV_TREE_SERIALIZER_XML );

	pBufferTree = umalloc(SizeTree);
	CHECK( pBufferTree != NULL, LBL_ERROR );

	TlvTree_Serialize( Tree, TLV_TREE_SERIALIZER_XML, pBufferTree, SizeTree );

	//- Serialization

	//+ Write Flash

	hFile = FS_open( FullPath, "a" );
	CHECK( hFile != NULL, LBL_ERROR );

	iRet = FS_write( pBufferTree, sizeof(char), SizeTree, hFile );
	CHECK( iRet == SizeTree, LBL_ERROR );

	iRet = FS_close(hFile);
	CHECK( iRet == FS_OK, LBL_ERROR );

	iRet = FS_unmount(Disk);
	CHECK( iRet == FS_OK, LBL_ERROR );

	//- Write Flash

	//Release
	if( pBufferTree != NULL )
		ufree(pBufferTree);

	return RET_OK;

LBL_ERROR:

	//Unmount Disk
	FS_unmount(Disk);

	//Release
	if( pBufferTree != NULL )
		ufree(pBufferTree);

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	RebootTerminal
 * Description:		Reboot (ICTXXX) or shutdown(IWLXXX) terminal
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:			nothing
 * Notes:
 */

void RebootTerminal(void)
{
	DisplayHeader(_ON_);
	UI_ClearAllLines();
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_POS_REBOOT), UI_ALIGN_CENTER);
	UI_ShowCleanMessage(1);

	#if(TETRA_HARDWARE==0)
	exit(0);
	#else
	Telium_Exit(0);
	#endif//TETRA_HARDWARE
}
//- INGESTATE01


//+ EWL01 @jbotero 2/03/2016
/* --------------------------------------------------------------------------
 * Function Name: 	Buzzer
 * Description:		Configurable Beep
 * Author:			@jbotero
 *
 * Parameters:		int nFrequency:
 * 					unsigned char Volume: Volume of beep
 * 					int nDelay_Tick: Duration of beep
 * Return:
 * Notes:
 */
#define BUZZER_BASE_CLOCK                      (57142857)

void Buzzer(int nFrequency, unsigned char Volume,int nDelay_Tick)
{
	int nDivisor;
	unsigned char FrequencyDivisor;
	unsigned short usTicksHigh;
	unsigned short usTicksPeriod;
	unsigned int nTemp;

	if( ( nFrequency > 20 ) && ( nFrequency < 20000 ) && (Volume > 0) )
	{
		// Calculate the smallest possible divisor for a maximum accurate frequency
		nDivisor = ((unsigned int)BUZZER_BASE_CLOCK) / (((unsigned int)nFrequency) * 65535);
		if (nDivisor < 2)
		{
			nDivisor = 2;
			FrequencyDivisor = 0;
		}
		else if (nDivisor < 8)
		{
			nDivisor = 8;
			FrequencyDivisor = 1;
		}
		else if (nDivisor < 32)
		{
			nDivisor = 32;
			FrequencyDivisor = 2;
		}
		else if (nDivisor < 128)
		{
			nDivisor = 128;
			FrequencyDivisor = 3;
		}
		else // if (nDivisor < 1024)
		{
			nDivisor = 1024;
			FrequencyDivisor = 4;
		}

		// Calculate the number of ticks for the period

		nTemp = ((unsigned int)nFrequency) * nDivisor;
		usTicksPeriod = (((unsigned int)BUZZER_BASE_CLOCK) + (nTemp / 2)) / nTemp;

		// Calculate the number of ticks for the high time (usTicksPeriod / 2 for max volume)

		usTicksHigh = usTicksPeriod - ((Volume * (usTicksPeriod / 2)) / 255);

		if (usTicksHigh >= usTicksPeriod)
			usTicksHigh = usTicksPeriod - 1;

		// Start the buzzer
		StartBuzzer(FrequencyDivisor,  usTicksHigh, usTicksPeriod);

		if( nDelay_Tick > 0 )
		Telium_Ttestall( 0,nDelay_Tick );
	}

	StopBuzzer();

}

/* --------------------------------------------------------------------------
 * Function Name:	beepOK
 * Description:		Beep when operation is OK
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */
void beepOK( void )
{
	Buzzer(2500, 150, 3);
	//buzzer(3);
}

/* --------------------------------------------------------------------------
 * Function Name:	beepError
 * Description:		Beep when operation is Error
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */
void beepError( void )
{
	Buzzer(500, 200, 10);
}
//- EWL01

/* --------------------------------------------------------------------------
 * Function Name:	Wait_Key
 * Description:		Wait for a key and return it, or Timeout.
 * Author:			@jbotero
 * Parameters:		IN : Only_CancelOK_Key - Flag to indicate that only wait
 * 					for RED and GREEN Button
 *
 *					IN : timeout - Time to wait in seconds
 *
 * Return:			(0)Timeout Expired
 * 					Other case, key pressed(See T_XXX defines on oem_public_def.h)
 *
 * Notes:			If timeout parameter is '0', wait for key infinitely.
 */

int Wait_Key( uint8 Only_CancelOK_Key, uint32 timeout )
{
	uint32 key = 0;
	uint8 boolExit = FALSE;
	Telium_File_t * hKeyboard;

	hKeyboard = Telium_Fopen("KEYBOARD","r*");

	while(!boolExit)
	{
		 if (Telium_Ttestall(KEYBOARD, timeout * 100) == KEYBOARD )
			 key = Telium_Getchar();
		 else
			 key = 0;

		if( Only_CancelOK_Key )
		{
			if( key == T_ANN || key == T_VAL || key == 0)
				boolExit = TRUE;
		}
		else
		{
			boolExit = TRUE;
		}
	}

	Telium_Fclose(hKeyboard);

	return key;
}

/* --------------------------------------------------------------------------
 * Function Name:	ConvIp_AsciToByteArr
 * Description:		Convert Ip from Ascii to Byte Array(4 bytes)
 * 					example->	10.8.18.22 -> {0x0A, 0x08, 0x12, 0x16 }
 *
 * Author:			@jbotero
 *
 * Parameters:		IN : ipAscii - Ascii Ip, format XXX.XXX.XXX.XXX
 *					OUT: ip_bArray[4] - Ip array 4 bytes.
 * Return:			Nothing
 * Notes:			Nothing
 */

void ConvIp_AsciToByteArr( char * ipAscii, char ip_bArray[4])
{
	T_GL_HSTRINGLIST list;

	list = GL_StringList_Create(GL_ENCODING_ISO_8859_1);

	FL_SplitPath(ipAscii, list, '.', false );

	ip_bArray[0] = atoi( GL_StringList_GetString( list, 0 ) );
	ip_bArray[1] = atoi( GL_StringList_GetString( list, 1 ) );
	ip_bArray[2] = atoi( GL_StringList_GetString( list, 2 ) );
	ip_bArray[3] = atoi( GL_StringList_GetString( list, 3 ) );

    GL_StringList_Destroy(list);

}

/* --------------------------------------------------------------------------
 * Function Name:	__TraceDebug
 * Description:		Api for debug using trace
 * Parameters:
 *  - const char* pString, ... - format message to show
 * Return:
 *  - nothing
 * Notes:
 */
#define SAP_IPOSAPP 0xFF50

void __TraceDebug( const char* pString, ... )
{
	#if( DIR_TRACEDEBUG == 1 )
	{
		char String[512];
		int nLength;
		va_list Params;

		va_start(Params, pString);

		nLength = vsprintf(String, pString, Params);

		va_end(Params);

		trace(SAP_IPOSAPP, nLength, String);
	}

	#endif //DIR_TRACEDEBUG
}
/* --------------------------------------------------------------------------
 * Function Name:	CheckBatteryCharge
 * Description:		Check if terminal have battery, and control levels
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:			nothing
 * Notes:
 */

void CheckBatteryCharge( void )
{
	int	  inDisplay;
	int   iLevBatt;
	int   iBatLoading;
	int   iPresUSB;
	int   iPresBase;
	char  StrBatteryLevel[50] = {0x00 };

	if( IsPortable()  ) //Validation for terminals that use battery
	{
        while( !Telium_Isbattery() )
        {
			UI_ShowMessage(
					mess_getReturnedMessage(MSG_BATTERY_ERROR),
					mess_getReturnedMessage(MSG_NO_BATTERY),
					mess_getReturnedMessage(MSG_POS_REBOOT), GL_ICON_WARNING, GL_BUTTON_ALL, 3);
			#if(TETRA_HARDWARE == 0)
			exit(0);
			#else
			Telium_Exit(0);
			#endif//TETRA_HARDWARE
        }

		inDisplay = TRUE;

		do
		{
			iLevBatt= 0;
			iBatLoading = 0;
			iPresUSB = 0;
			iPresBase = 0;

			ConnectedToPower  ( &iLevBatt,&iBatLoading, &iPresUSB, &iPresBase );
			sprintf(StrBatteryLevel, "NIVEL: %d%%", iLevBatt);
			__TraceDebug(StrBatteryLevel);

			if( iBatLoading == 0 && iLevBatt >= PORCENTAJE_MINIMO_BATERIA + 1 && iLevBatt <= PORCENTAJE_MINIMO_BATERIA + 2 )
			{
				UI_ShowMessage(
						mess_getReturnedMessage(MSG_LOW_BATTERY),
						mess_getReturnedMessage(MSG_CONNECT_LOAD),
						StrBatteryLevel, GL_ICON_WARNING, GL_BUTTON_ALL, 5);
				break;
			}
			if( iLevBatt <= PORCENTAJE_MINIMO_BATERIA)
			{
				if(iBatLoading == 1 || iPresUSB == 1 || iPresBase == 1)
				{
					if( inDisplay )
					{
						UI_ClearAllLines();
						UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_LOADING_BATTERY), UI_ALIGN_CENTER);
						strcat(StrBatteryLevel, "%%");
						UI_SetLine(UI_LINE3, StrBatteryLevel, UI_ALIGN_CENTER);

						UI_ShowCleanMessage(1);
					}
				}
				else
				{
					UI_ShowMessage(
							mess_getReturnedMessage(MSG_VERY_LOW_BATTERY),
							mess_getReturnedMessage(MSG_CONNECT_LOAD),
							StrBatteryLevel, GL_ICON_WARNING, GL_BUTTON_ALL, 3);
					Telium_Shutdown();
				}
			}

		} while(iLevBatt <= PORCENTAJE_MINIMO_BATERIA);

		UI_ClearAllLines();
	}
}

//+ FIX0002 @pmata	May 11, 2016
/* --------------------------------------------------------------------------
 * Function Name:	GetEMVAmount
 * Description:		Get total amount of Tree_Tran
 * Parameters:
 * Return:
 *  amount total amount
 * Notes:
 */
amount GetEMVAmount()
{
	TLV_TREE_NODE node;
	amount Amount;
	amount Total = 0;
	int16 TranID;

	// Set Base Amount line
	// get TAG_BASE_AMOUNT from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	if (TranID == TRAN_SALE
			|| TranID == TRAN_REFUND
			|| TranID == TRAN_QUICK_SALE
			)
	{
		node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);

		if (node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
		}

		// Set Tax Amount line
		node = TlvTree_Find(Tree_Tran, TAG_TAX_AMOUNT, 0);
		if(node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
		}

		// Set Tip Amount line
		node = TlvTree_Find(Tree_Tran, TAG_TIP_AMOUNT, 0);
		if(node != NULL)
		{
			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
			Total+=Amount;
		}
	}
	else
	{
		Amount = 0;
	}

	return Total;

LBL_ERROR:
	return 0;

}
/* --------------------------------------------------------------------------
 * Function Name:	GetEMVOtherAmount  Cash
 * Description:		Get total amount of Tree_Tran
 * Parameters:
 * Return:
 *  amount total amount
 * Notes:
 */
amount GetEMVOtherAmount()
{
	TLV_TREE_NODE node;
//d	amount Amount;
	amount Total = 0;
	int16 TranID;

	// Set Base Amount line
	// get TAG_BASE_AMOUNT from Tree_Tran
	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	// include all transaccion that have cash;

//d 	if (TranID == TRAN_CASH)
//d 	{
//d 		node = TlvTree_Find(Tree_Tran, TAG_BASE_AMOUNT, 0);
//d
//d 		if (node != NULL)
//d 		{
//d 			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
//d 			Total+=Amount;
//d 		}
//d
//d 		// Set Tax Amount line
//d 		node = TlvTree_Find(Tree_Tran, TAG_TAX_AMOUNT, 0);
//d 		if(node != NULL)
//d 		{
//d 			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
//d 			Total+=Amount;
//d 		}
//d
//d 		// Set Tip Amount line
//d 		node = TlvTree_Find(Tree_Tran, TAG_TIP_AMOUNT, 0);
//d 		if(node != NULL)
//d 		{
//d 			memcpy((char*)&Amount, TlvTree_GetData(node), LTAG_AMOUNT);
//d 			Total+=Amount;
//d 		}
//d 	}
//d 	else
//d 	{
//d 		Amount = 0;
//d 	}

	return Total;

LBL_ERROR:
	return 0;

}
//- FIX0002

//+ FIX0006 @pmata	May 11, 2016
char GetEMVTransactionType()
{
	TLV_TREE_NODE node;
	int16 TranID;
	char retval = 0x00;

	node = TlvTree_Find(Tree_Tran, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	switch (TranID)
	{
		case TRAN_SALE:
		case TRAN_C2P:
		case TRAN_QUICK_SALE:
			retval = 0x00;
			break;
		case TRAN_REFUND:
			retval = 0x00;
			break;
//d		case TRAN_CASH:
//d			retval = 0x01;
//d			break;

//d		case TRAN_CASH_BACK:
//d			retval = 0x01;
//d			break;

		default:
			retval = 0x00;
			break;
	}

LBL_ERROR:
	return retval;
}
//- FIX0006
/** ------------------------------------------------------------------------------
 * Function Name:	_pow
 * Author:			jbotero
 * Date:		 	20/01/2015
 * Description:
 * Parameters:
 *  - fAmount 		Pow implementation for Float values
 * Return:
 *  - rounded amount
 * Notes:
 */
float _pow( float val1, int exp )
{
	int i;
	float acum = (float)1;
	bool negExp = FALSE;

	if(exp < 0)
	{
		exp *=-1;
		negExp = TRUE;
	}

	for( i = 0; i < exp; i++ )
		acum = (float)val1*acum;

	if(negExp == TRUE)
	{
		if (acum!=0)
			acum = 1/acum;
		else
		{
			// if we reached here: error!!! 0 is returned
			return 0;
		}
	}
	return acum;
}
//+ MERCHSELECT @mamata 5/05/2016
/* --------------------------------------------------------------------------
 * Function Name:	TlvTree_FindChild
 * Description:		Find child in specific node (only search on first tree level)
 * Author:			mamata
 * Parameters:
 *  - const TLV_TREE_NODE hNode - node parent
 *  - unsigned int nTag - tag number
 * Return:
 *  - NULL - nTag do not find
 *  (other) - pointer to node
 * Notes:
 */
TLV_TREE_NODE TlvTree_FindChild(const TLV_TREE_NODE hNode, unsigned int nTag)
{
	TLV_TREE_NODE node = NULL;

	// find first child for nNode
	node = TlvTree_GetFirstChild(hNode);

	while(node != NULL)
	{
		if(TlvTree_GetTag(node) == nTag)
			break;

		// get next child
		node = TlvTree_GetNext(node);
	}
	return node;
}
//- MERCHSELECT

/* --------------------------------------------------------------------------
 * Function Name:	GetNumBytes_Multip8
 * Description:		Calculate number of byte to be Multiples of 8
 * Parameters:
 *  -
 * Return:
 *  Number of bytes for padding
 *  Example: if LenInput = 5, return 3
 * Notes:
 */
int GetNumBytes_Multip8( int LenInput )
{
	if(LenInput % 8 == 0 )
		// buffer doesn't needs to be padded
		return 0;
	else
		// buffer needs to be padded
		return 8 - ( LenInput % 8 );
}

/* --------------------------------------------------------------------------
 * Function Name:	PadBuff
 * Description:		Pad buffer with specific filler
 * Parameters:
 *  -
 * Return:
 *  - RET_OK - Data cipher OK
 *  - RET_NOK - Data cipher ERROR
 * Notes:
 */
int PadBuff(unsigned char * pInput, int lenInput, unsigned char PaddingChar, int lenOutput, unsigned char * pOutput )
{
    if(NULL == pOutput)
        return FALSE;

	memset(pOutput, PaddingChar, lenOutput);
	memcpy(pOutput, pInput, lenInput);

	return TRUE;
}

/* --------------------------------------------------------------------------
 * Function Name:	CipherData
 * Description:		Cipher any data, with padding using spaces = 0x20
 * Parameters:
 *  -
 * Return:
 *  - RET_OK - Data cipher OK
 *  - RET_NOK - Data cipher ERROR
 * Notes:
 */
int CipherData( char * buffer, int lnBuffer, unsigned char EWK[], int EWKLen,
		unsigned char OutputBuff[], int * OutputBuffLen,
		int iOpMode, unsigned char *pStrIV)
{
	int RetFun;
    CRYPTOLIB_DATA_PARAMETERS DataParams;
    CRYPTOLIB_DATA_GENERAL_PARAMS GeneralDataParams;
    TABLE_KEY_INFO stKeyInfo;
    int PaddedBuffLen = 0;
    int TotalPaddedBuffLen = 0;
    unsigned char * pPaddedBuff = NULL;

    RetFun = TableTerminal_GetKeyInfo(KEY_TYPE_DATA, &stKeyInfo);
    CHECK(RetFun == RET_OK, LBL_ERROR);

	memset( &DataParams,0x00, sizeof(DataParams));
	memset( &GeneralDataParams, 0x00, sizeof(GeneralDataParams));

    DataParams.DATAGeneralParams = &GeneralDataParams;

	GeneralDataParams.pInputData = (uint8*)buffer;
	GeneralDataParams.LenInputData = lnBuffer;

	if( *OutputBuffLen == 0 )
	{
		//Calculate number of byte to be Multiples of 8
		PaddedBuffLen = GetNumBytes_Multip8( GeneralDataParams.LenInputData);

		//Calculate total Length of Output buffer
		TotalPaddedBuffLen = GeneralDataParams.LenInputData + PaddedBuffLen;
	}
	else
		TotalPaddedBuffLen = *OutputBuffLen;

	//Allocate Memory
	pPaddedBuff = umalloc(TotalPaddedBuffLen);
	CHECK(pPaddedBuff!=NULL, LBL_MALLOC_ERR);

	//Fill buffer with padding character
	PadBuff( GeneralDataParams.pInputData, GeneralDataParams.LenInputData,
			0x20, TotalPaddedBuffLen, pPaddedBuff );

	// Load Structure to Cipher Data
	GeneralDataParams.pInputData = pPaddedBuff;
	GeneralDataParams.LenInputData = TotalPaddedBuffLen;
	GeneralDataParams.iOper = C_SEC_CIPHER_FUNC;
	GeneralDataParams.pOutputData = OutputBuff;
	GeneralDataParams.OperationMode = iOpMode;
	GeneralDataParams.pInitialValue = pStrIV;

	DataParams.stMasterKeyInfo.cAlgoType = DataParams.stKeyInfo.cAlgoType = stKeyInfo.AlgoType;
	DataParams.stMasterKeyInfo.iSecretArea = DataParams.stKeyInfo.iSecretArea = stKeyInfo.SecretArea;
	DataParams.stMasterKeyInfo.uiBankId = DataParams.stKeyInfo.uiBankId= stKeyInfo.BankId;

	if( DataParams.stMasterKeyInfo.cAlgoType == TLV_TYPE_DESDUKPT ||
		DataParams.stMasterKeyInfo.cAlgoType == TLV_TYPE_TDESDUKPT ||
		DataParams.stMasterKeyInfo.cAlgoType == TLV_TYPE_DESDUKPTLIGHT ||
		DataParams.stMasterKeyInfo.cAlgoType == TLV_TYPE_TDESDUKPTLIGHT
		)
	{
		DataParams.stKeyInfo.usNumber = stKeyInfo.MKNumber;
	}
	else
	{
		DataParams.stMasterKeyInfo.usNumber = stKeyInfo.MKNumber;
		DataParams.stKeyInfo.usNumber = stKeyInfo.WKNumber;

		memcpy(GeneralDataParams.EncryptedWorkingKey, EWK, EWKLen);
	}

	RetFun = CryptoLib_DataEncryption(&DataParams);

	*OutputBuffLen = GeneralDataParams.LenOutputData;

	//Release memory
	if(pPaddedBuff != NULL)
		ufree(pPaddedBuff);

	if(RET_OK == RetFun)
		return RET_OK;

LBL_MALLOC_ERR:
    return RET_NOK;

LBL_ERROR:
	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	CipherData_TLV
 * Description:		Cipher any data, with padding using spaces = 0x20
 * Parameters:
 *  -
 * Return:
 *  - RET_OK - Data cipher OK
 *  - RET_NOK - Data cipher ERROR
 * Notes:
 */
int CipherData_TLV( TLV_TREE_NODE Tree, unsigned int TrackTag, unsigned char EWK[],
		int EWKLen, unsigned char OutputBuff[], int * OutputBuffLen,
		int iOpMode, unsigned char *pStrIV)
{
	TLV_TREE_NODE TLVT_Node;

	TLVT_Node = TlvTree_Find( Tree, TrackTag, 0 );

	if( TLVT_Node == NULL )
		return RET_NOK;

	return CipherData( TlvTree_GetData( TLVT_Node ), TlvTree_GetLength( TLVT_Node ), EWK, EWKLen,
			OutputBuff, OutputBuffLen, iOpMode, pStrIV);
}


/*
 * Menus for "more_function"
 */

/* --------------------------------------------------------------------------
 * Function Name:	_Test_EWL_Debug
 * Description:		Module to enable EWL debug.
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */
void _Test_EWL_Debug( void )
{
	int RetFun;

	UI_MenuReset();

	UI_MenuAddItem("ON", 1);
	UI_MenuAddItem("OFF", 2);

	RetFun = UI_MenuRun("Debug EWL", 30, 1);
	CHECK(RetFun > 0, LBL_END);

	switch(RetFun)
	{
		case 1:
			debugControl();
			break;

		case 2:
			RebootTerminal();
			break;
	}

	return;

LBL_END:

	return;
}
/*
 * Menus for "more_function"
 */

/* --------------------------------------------------------------------------
 * Function Name:	IsPCLDoTransaction
 * Description:		Check if POS is using mode PCL do transaction
 * Parameters:
 * Return:
 * Notes:
 */
bool IsPCLDoTransaction(void)
{
	if(tTerminal.Header.RecordID != 0)
		if( tTerminal.modePCL == PCL_MODE_ON)
			return TRUE;

	return FALSE;
}
