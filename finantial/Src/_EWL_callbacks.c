//#include "ewlDemo.h"
//#include "dspTools.h"
//#include "dspMenu.h"
//#include "dspPIN.h"
//#include "parameter.h"
//#include "key.h"
#include "iPOSApp.h"

/* --------------------------------------------------------------------------
 * Function Name:	callbackGetParameters
 * Description:		Called function for get card parameters and show card label
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - const unsigned long *dataList: id AID
 * - unsigned int  nDataListElement:
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */

ewlStatus_t callbackGetParameters(ewlObject_t   *handle, const unsigned long *dataList,
		uint16_t nDataListElement){

    int ret;
    int data = dataList[0];
    ewlTechnology_t technology;
    unsigned int dummy;

    dummy = nDataListElement;

    ret = ewlGetParameter(handle,&technology,sizeof(technology),EWL_TAG_TECHNOLOGY);
    if(ret < 0) return EWL_ERR_INTERNAL;

    if(technology == EWL_TECHNOLOGY_CONTACT || technology == EWL_TECHNOLOGY_PAYPASS_MCHIP)
    {
        char label[EWL_TAG_LABEL_MAX_LEN + 1];

        utilLedsOff();

        memset(label,0x00,sizeof(label));
        ret = ewlGetParameter(handle,label,sizeof(label), EWL_TAG_LABEL);
        if(ret <= 0) return EWL_ERR_INTERNAL;

        UI_ClearLine(UI_LINE1);
        UI_ClearLine(UI_LINE2);
        UI_ClearLine(UI_LINE3);
        UI_ClearLine(UI_LINE4);
        UI_SetLine(UI_LINE2, label, UI_ALIGN_CENTER);
        UI_ShowInfoScreen(NULL);
    }

    ret = parameterApplicationSpecific(handle,data);
    if(ret != EWLDEMO_OK)
    	return EWL_ERR_INTERNAL;

    return EWL_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	callbackLeds
 * Description:		Called function for manage led on contact-less transaction
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - unsigned int step
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */

void callbackLeds (ewlObject_t *handle, uint16_t step){

    void *dummy;
    dummy = handle;

	switch (step)
	{
		case 0: /*utilLedsOff(); */
			break;
		case 1:
			if(IsColorDisplay())
				SetLedEvent(SOFTWARE_LED_01, 1);
			else
				SetLedEvent(HARDWARE_LED_01, 1);
			break;
		case 2:
			if(IsColorDisplay())
				SetLedEvent(SOFTWARE_LED_02, 1);
			else
				SetLedEvent(HARDWARE_LED_02, 1);
			break;
		case 3:
			if(IsColorDisplay())
				SetLedEvent(SOFTWARE_LED_03, 1);
			else
				SetLedEvent(HARDWARE_LED_03, 1);
			break;
		case 4:
			if(IsColorDisplay())
				SetLedEvent(SOFTWARE_LED_04, 1);
			else
				SetLedEvent(HARDWARE_LED_04, 1);
			break;
	}
    Telium_Ttestall(0,5);
    return;

}

/* --------------------------------------------------------------------------
 * Function Name:	callbackRemoveCard
 * Description:		Called function for manage remove card on contact-less
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - int status
 * Return:
 * - nothing
 * Notes:
 */
void callbackRemoveCard (ewlObject_t *handle,  int status)
{

    int dummy2;
    dummy2 = status;

    utilLedsOff();

    utilBeepWarning();

    return;
}

/* --------------------------------------------------------------------------
 * Function Name:	callbackBDisplay
 * Description:		Called function to show show special kernel messages
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - ewlDisplay_t msgCode: Code Id
 * Return:
 * - nothing
 * Notes:
 */
void callbackBDisplay(ewlObject_t *handle, ewlDisplay_t msgCode)
{
    void *dummy;
    dummy = handle;

    char MsgTitle[200] = {0x00, };
    char MsgLine1[200] = {0x00, };
    char MsgLine2[200] = {0x00, };

    T_GL_DIALOG_ICON icon = GL_ICON_NONE;
    T_GL_BUTTONS buttons = GL_BUTTON_ALL;
    int timeout = 3;
    strcpy( MsgTitle, "AVISO");

    switch(msgCode)
    {
        case EWL_DISPLAY_OFFLINE_LAST_ATTEPMPT:
        	icon = GL_ICON_WARNING;
            strcpy( MsgLine1, "PROXIMO PIN ERRONEO");
            strcpy( MsgLine2, "BLOQUEA EL PIN");
            break;

        case EWL_DISPLAY_OFFLINE_PIN_OK:
            break;

        case EWL_DISPLAY_OFFLINE_PIN_ERROR:
        	strcpy( MsgLine1, "PIN INCORRECTO");
        	icon = GL_ICON_ERROR;
            break;

        case EWL_DISPLAY_OFFLINE_PIN_BLOCKED:
        	strcpy( MsgLine1, "PIN BLOQUEADO");
        	icon = GL_ICON_ERROR;
            break;

        case EWL_DISPLAY_APPLICATION_BLOCKED:
        	strcpy( MsgLine1, "APLICACION BLOQUEADA");
        	icon = GL_ICON_ERROR;
            break;

        case EWL_DISPLAY_APPLICATION_ERROR:
        	strcpy( MsgLine1, "APLICACION INVALIDA");
        	icon = GL_ICON_ERROR;
            break;

        case EWL_DISPLAY_APPLICATION_INVALIDATED:   break;
        case EWL_DISPLAY_APPLICATION_NON_EMV:       break;
        case EWL_DISPLAY_OFFLINE_PIN_ALREADY_BLOCKED: break;
    }

    if(MsgLine1[0] != NULL || MsgLine2[0] != NULL )
    	UI_ShowMessage(MsgTitle, MsgLine1, MsgLine2, icon, buttons, timeout );

    return;
}

/* --------------------------------------------------------------------------
 * Function Name:	callbackGetMenu
 * Description:		Called function to list card applications
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - unsigned int *selected: Selected application id
 * - const char **aidList: List of applications
 * - unsigned int nAIDs: Max number of applications
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */

ewlStatus_t callbackGetMenu (ewlObject_t *handle,uint16_t *selected,
                             const char **aidList, uint16_t  nAIDs)
{
    int RetFun;
    unsigned int    ii;

    UI_MenuReset();
	for (ii = 0; ii < nAIDs; ii++)
		UI_MenuAddItem((char*)aidList[ii], ii);

	RetFun = UI_MenuRun(mess_getReturnedMessage(MSG_SELECT_APP_EMV), tConf.TimeOut, 0);

    if( RetFun < 0)
    	return EWL_ERR_USER_CANCEL;

    *selected = RetFun;

    return EWL_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	callbackGetPinOffLine
 * Description:		Called function to enter Offline PIN
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - unsigned char *numDigits: Num digits of PIN
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */

ewlStatus_t callbackGetPinOffLine (ewlObject_t *handle, unsigned char *numDigits)
{
	int RetFun;
    int cont = 1;
    int timeout = GL_TIME_SECOND * tConf.TimeOut;
    char TempBuffer[200];

    char msgTitle[100] = {0x00};
    char msgLine1[100] = {0x00};
    char msgLine2[100] = {0x00};

    T_SEC_ENTRYCONF conf = {0x2A, 8, 8, 0, 0, 0, 12, timeout, timeout };

    SEClib_Open();

    UI_BackupAllLines();

    RetFun = SEC_PinEntryInit(&conf, C_SEC_PINCODE);

    utilGetPinScreenLines(msgTitle, msgLine1, msgLine2);

	UI_SetLine(UI_TITLE, msgTitle, UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, msgLine1, UI_ALIGN_CENTER);
	UI_ShowInfoScreen(msgLine2);

	do
	{
		RetFun = utilGetKey(numDigits, &cont);

		memset(TempBuffer, 0x00, sizeof(TempBuffer));
		memset(TempBuffer, '*', *numDigits);

		UI_SetLine(UI_LINE3, TempBuffer, UI_ALIGN_CENTER);
		UI_ShowInfoScreen(msgLine2);

	} while (RetFun == 2);

    SEClib_Close();

    UI_RestoreAllLines();

    return RetFun;

}

/* --------------------------------------------------------------------------
 * Function Name:	callbackGetPinOnLine
 * Description:		Called function to enter Online PIN
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */

ewlStatus_t callbackGetPinOnLine (ewlObject_t *handle)
{
	int RetFun;

    UI_BackupAllLines();

	RetFun = utilGetPinOnline(Tree_Tran);

	UI_RestoreAllLines();

	if(RetFun == RET_NOK)
		return EWL_ERR_INTERNAL;
	else if(RetFun == RET_CANCEL)
		return EWL_ERR_USER_CANCEL;

	return EWL_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	callbackFinalCVM
 * Description:		Called function to process final CVM
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */
ewlStatus_t callbackFinalCVM (ewlObject_t *handle)
{
    return EWL_OK;

}

/* --------------------------------------------------------------------------
 * Function Name:	callbackGetPublicKey
 * Description:		Called function to get Public Key
 * Author:			@jbotero
 * Parameters:
 * - ewlObject_t   *handle: Handle EWL
 * - ewlGetCertificate_t *certificate: Certificate info
 * Return:
 * - EWL_OK: ok
 * - Other value: error
 * Notes:
 */
ewlStatus_t callbackGetPublicKey (ewlObject_t *handle, ewlGetCertificate_t *certificate){

	stKey_t * key;
    int    ii;
    int ret;

    ii = 0;
	while ((key = keyGet(ii++)) != NULL)
	{
		if ((certificate->index == key->keyId)
				&& (memcmp(certificate->RID, key->rid, EWL_TAG_RID_LEN) == 0))
		{

			ret = ewlSetParameter(handle, EWL_TAG_PK_EXP, key->keyExp,
					key->keyExpLen);
			if (ret != EWL_OK)
				return ret;

			ret = ewlSetParameter(handle, EWL_TAG_PK_MODULUS, key->keyModulus,
					key->keyModulusLen);
			if (ret != EWL_OK)
				return ret;

			return EWL_OK;
		}
	}

    return EWL_OK;
}



