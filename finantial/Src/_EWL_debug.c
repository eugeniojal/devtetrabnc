#include "iPOSApp.h"

#ifdef DEBUG_ON

//To check, how can be used DEBUG_TRACER & DEBUG_CONSOLE
/*
#if(TETRA_HARDWARE == 1)
#define DEBUG_TRACER
#else
#define DEBUG_CONSOLE
#endif
*/

//Enable modes to get log...
//#define DEBUG_SERIAL
#define DEBUG_FILE
//#define DEBUG_PRINTER

#define EWLDEMO_LOG 0

#endif


#ifdef DEBUG_PRINTER
static int PrinterPrintf (void *context, uint16_t channelLevel, const char *msg, int msgLength) {

    static Telium_File_t *printer = NULL;

    int ii = 0;

    #if(TETRA_HARDWARE == 1)
	#else
    void* dummy1;
    int dummy2;
    uint16_t dummy3;

    dummy1 = context;
    dummy2 = msgLength;
    dummy3 = channelLevel;
    #endif


    printer = Telium_Fopen("PRINTER","w-");

    Telium_Fprintf(printer,"\x0f");

    for(ii = 0; ii < msgLength ; ii++)
        Telium_Putc((char)msg[ii],printer);

    Telium_Putc((char)0x0A ,printer);
    Telium_Ttestall(0,1);

    Telium_Fclose(printer);

    return 0;

}

#endif


#ifdef DEBUG_SERIAL
//------------------------------------------------------------------------------
static int SerialPrintf (void *context, uint16_t channelLevel, const char *msg, int msgLength) {

	static Telium_File_t *serial = NULL;

	int ii = 0;
	char char1 = 0x0D;
	char char2 = 0x0A;

	#if(TETRA_HARDWARE == 0)
	void* dummy1;
	int dummy2;
	uint16_t dummy3;

	dummy1 = context;
	dummy2 = msgLength;
	dummy3 = channelLevel;
	#endif

	serial = Telium_Fopen("COM5","rw");
	Telium_Format("COM5",115200,8,1,NO_PARITY,0, 0);


	for(ii = 0; ii < msgLength ; ii++)
		Telium_Fwrite(&msg[ii], 1, 1, serial);

	Telium_Fwrite(&char1, 1, 1 ,serial);
	Telium_Fwrite(&char2, 1, 1 ,serial);

//	Telium_Ttestall(NULL, 1);

	Telium_Fclose(serial);

    return 0;
}

#endif


#ifdef DEBUG_TRACER

//------------------------------------------------------------------------------
static int TracePrintf (void *context, uint16_t channelLevel, const char *msg, int msgLength) {

    #ifndef _TELIUM_PLUS_
    void* dummy1;
    int dummy2;
    uint16_t dummy3;

    dummy1 = context;
    dummy2 = msgLength;
    dummy3 = channelLevel;
    #endif

    trace(0xDD00,msgLength,msg);

    #ifdef _TELIUM_PLUS_
    Telium_Ttestall(0,2);
    #else
    ttestall(0,2);
    #endif

    return 0;
}


#endif

#ifdef DEBUG_FILE

static int FilePrintf (void *context, uint16_t channelLevel, const char *msg, int msgLength) {

    int ret;
    fsFile_t *handle;
    dateTime_t time;
    char temp[1024];

    dateTimeGet(&time);


    fsPushFileNamePrefix("/HOST/");
    ret = fsOpen("log.txt", FS_OPEN_WRITE|FS_OPEN_CREATE|FS_OPEN_APPEND , &handle);
    if(ret < 0){
        fsPopFileNamePrefix();
        return 0;
    }

    memset(temp,0x00,sizeof(temp));
    sprintf(temp, "%04d/%02d/%02d - %02d:%02d:%02d  "
           ,time.date.year,time.date.mon,time.date.day
           ,time.hour,time.min,time.sec);

    fsWrite(handle,strlen(temp),temp);
    fsWrite(handle,msgLength,msg);
    fsWrite(handle,2,"\r\n");

    fsClose(handle);

    fsPopFileNamePrefix();
    return 0;
}


#endif


#ifdef DEBUG_ON
static void SetEWLdebug (logWriteFunction_t output) {

    logSetChannels(EWLDEMO_LOG ,               1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);

    #if(EWL_STATIC == 0)
        ewlDllLogSetChannels(EWL_LOG_CARD_COMMUNICATION,  1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        ewlDllLogSetChannels(EWL_LOG_FUNCTION,            1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
    #else
        logSetChannels(EWL_LOG_KERNEL,              1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        logSetChannels(EWL_LOG_INTERFACE_PARAMETER, 1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        logSetChannels(EWL_LOG_CARD_COMMUNICATION,  1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        logSetChannels(EWL_LOG_FUNCTION,            1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        logSetChannels(EWL_LOG_DEVELOPER,           1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        logSetChannels(LOG_CH_BASE,            		1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
        logSetChannels(LOG_CH_TASK,           		1, LOG_DEBUG, output, logDumpFormattedAscii, NULL);
    #endif
    return;
}
#endif



void debugControl (void){


    #ifdef DEBUG_CONSOLE
    SetEWLdebug(logWriteTeliumRemoteDebugger);
    #endif


    #ifdef DEBUG_SERIAL
    SetEWLdebug(SerialPrintf);
    #endif


    #ifdef DEBUG_TRACER
    SetEWLdebug(TracePrintf);
    #endif


    #ifdef DEBUG_FILE
    SetEWLdebug(FilePrintf);
    #endif

    #ifdef DEBUG_PRINTER
    SetEWLdebug(PrinterPrintf);
    #endif

    return;
}
