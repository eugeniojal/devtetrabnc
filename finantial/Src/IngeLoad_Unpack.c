/*****************************************************************************
 Property of INGENICO
 *****************************************************************************/
/*
 * PROJECT  : IngeLoad POS
 * MODULE   : Initialization
 * FILEMANE : coreinit.c
 * PURPOSE  : Contains functions to POS initialization
 *
 * HISTORY
 *
 * date author modifications
 * 2004-26-04 @pdm @mdm  creation
 *
 *
 +++******* INCLUDES **********************************************************************---*/
#define NOT_iPOSApp
#include "iPOSApp.h"
#include "GTL_Convert.h"

#ifndef NOT_iPOSApp
#include "appmain.h"
#include "IdleMisc.h"
#include "display.h"

#include "ssaCmd.h"
#endif

#ifdef MAKE_CASHIERS
#include "CashierModule.h"
#endif //MAKE_CASHIERS


#ifdef MAKE_AUTOSETTLE
extern void  fvLIBTStamp_Get (RTC_ *date_time);
#endif //MAKE_AUTOSETTLE

/*---******* DEFINES **********************************************************************---*/
//// T_Flags1
#define TF1_MultiMerch		0x0001
#define TF1_BlockKey		0x0002
#define TF1_ECR				0x0004
#define TF1_Cashier			0x0008
#define TF1_ConfManual		0x0010
#define TF1_SettleAll		0x0020
#define TF1_PrnReceipt		0x0040
#define TF1_PrePrint		0x0080
#define TF1_PrnLogo			0x0100
#define TF1_PrnCardHold		0x0200
#define TF1_PrnTicket		0x0400
#define TF1_Bit12			0x0800
//#define TF1_Bit13			0x1000
//#define TF1_Bit14			0x2000
//#define TF1_Bit15			0x4000
//#define TF1_Bit16			0x8000


//// T_Flags2
//#define TF2_Bit01			0x0001
//#define TF2_Bit02			0x0002
//#define TF2_Bit03			0x0004
//#define TF2_Bit04			0x0008
//#define TF2_Bit05			0x0010
//#define TF2_Bit06			0x0020
//#define TF2_Bit07			0x0040
//#define TF2_Bit08			0x0080
//#define TF2_Bit09			0x0100
//#define TF2_Bit10			0x0200
//#define TF2_Bit11			0x0400
//#define TF2_Bit12			0x0800
//#define TF2_Bit13			0x1000
//#define TF2_Bit14			0x2000
//#define TF2_Bit15			0x4000
//#define TF2_Bit16			0x8000


//// T_UserFlags1
#define TUF1_Bit01			0x0001 // Print receipt in Auths 0100
#define TUF1_Bit02			0x0002
#define TUF1_Bit03			0x0004
#define TUF1_Bit04			0x0008
#define TUF1_Bit05			0x0010
#define TUF1_Bit06			0x0020
//#define TUF1_Bit07			0x0040
//#define TUF1_Bit08			0x0080
//#define TUF1_Bit09			0x0100
//#define TUF1_Bit10			0x0200
//#define TUF1_Bit11			0x0400
//#define TUF1_Bit12			0x0800
//#define TUF1_Bit13			0x1000
//#define TUF1_Bit14			0x2000
//#define TUF1_Bit15			0x4000
//#define TUF1_Bit16			0x8000

//// T_UserFlags2
//#define TUF2_Bit01			0x0001
//#define TUF2_Bit02			0x0002
//#define TUF2_Bit03			0x0004
//#define TUF2_Bit04			0x0008
//#define TUF2_Bit05			0x0010
//#define TUF2_Bit06			0x0020
//#define TUF2_Bit07			0x0040
//#define TUF2_Bit08			0x0080
//#define TUF2_Bit09			0x0100
//#define TUF2_Bit10			0x0200
//#define TUF2_Bit11			0x0400
//#define TUF2_Bit12			0x0800
//#define TUF2_Bit13			0x1000
//#define TUF2_Bit14			0x2000
//#define TUF2_Bit15			0x4000
//#define TUF2_Bit16			0x8000

//// M_TranFlags1
#define MT1_Sale			0x0001
#define MT1_Void			0x0002
#define MT1_Refund			0x0004
#define MT1_OffLineSale		0x0008
#define MT1_Adjust			0x0010
#define MT1_Balance			0x0020
#define MT1_Auth			0x0040
#define MT1_Lodgin			0x0080
#define MT1_Payments		0x0100
#define MT1_SaleCash		0x0200
#define MT1_CashAdv			0x0400
#define MT1_Debit			0x0800
//#define MT1_Bit13			0x1000
//#define MT1_Bit14			0x2000
//#define MT1_Bit15			0x4000
//#define MT1_Bit16			0x8000

//// M_TranFlags2
//#define MT2_Bit01			0x0001
//#define MT2_Bit02			0x0002
//#define MT2_Bit03			0x0004
#define MT2_Bit04			0x0008
#define MT2_Bit05			0x0010
#define MT2_Bit06			0x0020 // V. CHEQUE
#define MT2_Bit07			0x0040 // PPlan CR
#define MT2_Bit08			0x0080 // PPlan EX
#define MT2_Bit09			0x0100 // PPlan SF
#define MT2_Bit10			0x0200 // PPlan CL
#define MT2_Bit11			0x0400 // CONSULTA RETENCIONES
#define MT2_Bit12			0x0800 // CONSULTA DE BOLETAS
#define MT2_Bit13			0x1000 // PUNTOS
#define MT2_Bit14			0x2000 // REMESA
#define MT2_Bit15			0x4000 // PRE AUTORIZACION
#define MT2_Bit16			0x8000 // RECARGA

//// M_Flag1
#define MF1_Tip				0x0001
#define MF1_TipGuide		0x0002
#define MF1_Waiters			0x0004
#define MF1_Table			0x0008
#define MF1_Tax				0x0010
#define MF1_AdjBase			0x0020
#define MF1_PrintDiscNote	0x0040
#define MF1_RentaCar		0x0080
#define MF1_QuickPymt		0x0100
//#define MF1_Bit10			0x0200
//#define MF1_Bit11			0x0400
//#define MF1_Bit12			0x0800
//#define MF1_Bit13			0x1000
//#define MF1_Bit14			0x2000
//#define MF1_Bit15			0x4000
//#define MF1_Bit16			0x8000

//// M_Flag2
//#define MF2_Bit01			0x0001
//#define MF2_Bit02			0x0002
//#define MF2_Bit03			0x0004
//#define MF2_Bit04			0x0008
//#define MF2_Bit05			0x0010
//#define MF2_Bit06			0x0020
//#define MF2_Bit07			0x0040
//#define MF2_Bit08			0x0080
//#define MF2_Bit09			0x0100
//#define MF2_Bit10			0x0200
//#define MF2_Bit11			0x0400
//#define MF2_Bit12			0x0800
//#define MF2_Bit13			0x1000
//#define MF2_Bit14			0x2000
//#define MF2_Bit15			0x4000
//#define MF2_Bit16			0x8000


//// M_UserFlags1
#define MUF1_Bit01		0x0001
#define MUF1_Bit02		0x0002
#define MUF1_Bit03		0x0004
#define MUF1_Bit04		0x0008
#define MUF1_Bit05		0x0010
#define MUF1_Bit06		0x0020
#define MUF1_Bit07		0x0040
#define MUF1_Bit08		0x0080
#define MUF1_Bit09		0x0100
#define MUF1_Bit10		0x0200
#define MUF1_Bit11		0x0400
#define MUF1_Bit12		0x0800
#define MUF1_Bit13		0x1000
#define MUF1_Bit14		0x2000
#define MUF1_Bit15		0x4000
#define MUF1_Bit16		0x8000

//// M_UserFlags2
//#define MUF2_Bit01		0x0001
//#define MUF2_Bit02		0x0002
//#define MUF2_Bit03		0x0004
//#define MUF2_Bit04		0x0008
//#define MUF2_Bit05		0x0010
//#define MUF2_Bit06		0x0020
//#define MUF2_Bit07		0x0040
//#define MUF2_Bit08		0x0080
//#define MUF2_Bit09		0x0100
//#define MUF2_Bit10		0x0200
//#define MUF2_Bit11		0x0400
//#define MUF2_Bit12		0x0800
//#define MUF2_Bit13		0x1000
//#define MUF2_Bit14		0x2000
//#define MUF2_Bit15		0x4000
//#define MUF2_Bit16		0x8000

//// H_Flags
#define HF1_DetectLine		0x0001
#define HF1_NoHostTime		0x0002
#define HF1_DoubleKey		0x0004
#define HF1_DUKPT			0x0008
#define HF1_DialBackUp		0x0010
#define HF1_EMV				0x0020
//#define HF1_07				0x0040
//#define HF1_08				0x0080
//#define HF1_09				0x0100
//#define HF1_10				0x0200
//#define HF1_11				0x0400
//#define HF1_12				0x0800
//#define HF1_13				0x1000
//#define HF1_14				0x2000
//#define HF1_15				0x4000
//#define HF1_16				0x8000

//// H_UserFlags
#define HUF1_01			0x0001
//#define HUF1_02			0x0002
//#define HUF1_03			0x0004
//#define HUF1_04			0x0008
//#define HUF1_05			0x0010
//#define HUF1_06			0x0020
//#define HUF1_07			0x0040
//#define HUF1_08			0x0080
//#define HUF1_09			0x0100
//#define HUF1_00			0x0200
//#define HUF1_11			0x0400
//#define HUF1_12			0x0800
//#define HUF1_13			0x1000
//#define HUF1_14			0x2000
//#define HUF1_15			0x4000
//#define HUF1_16			0x8000

//// R_RangeFlags
#define RF_Debit			0x0001
#define RF_Manual			0x0002
#define RF_ExpDate			0x0004
#define RF_CheckDig			0x0008
#define RF_CVV2				0x0010
#define RF_4dbc				0x0020
#define RF_Last4			0x0040
#define RF_Pin				0x0080
#define RF_AccountType		0x0100
//#define RF1_10				0x0200
//#define RF1_11				0x0400
//#define RF1_12				0x0800
//#define RF1_13				0x1000
//#define RF1_14				0x2000
//#define RF1_15				0x4000
//#define RF1_16				0x8000

//// R_UserRangeFlags
#define RUF1_01			0x0001
#define RUF1_02			0x0002
#define RUF1_03			0x0004
//#define RUF1_04			0x0008
//#define RUF1_05			0x0010
//#define RUF1_06			0x0020
//#define RUF1_07			0x0040
//#define RUF1_08			0x0080
//#define RUF1_09			0x0100
//#define RUF1_10			0x0200
//#define RUF1_11			0x0400
//#define RUF1_12			0x0800
//#define RUF1_13			0x1000
//#define RUF1_14			0x2000
//#define RUF1_15			0x4000
//#define RUF1_16			0x8000


//// IP_IpFlags
#define IPF_AddLength		0x0001
#define IPF_AddTPDU			0x0002
#define IPF_SSL				0x0004
#define IPF_AuthClient		0x0008
//#define IPF_05				0x0010
//#define IPF_06				0x0020
//#define IPF_07				0x0040
//#define IPF_08				0x0080
//#define IPF_09				0x0100
//#define IPF_10				0x0200
//#define IPF_11				0x0400
//#define IPF_12				0x0800
//#define IPF_13				0x1000
//#define IPF_14				0x2000
//#define IPF_15				0x4000
//#define IPF_16				0x8000

//// R_UserRangeFlags  --  Fist Byte
//#define IPUF_01			0x0001
//#define IPUF_02			0x0002
//#define IPUF_03			0x0004
//#define IPUF_04			0x0008
//#define IPUF_05			0x0010
//#define IPUF_06			0x0020
//#define IPUF_07			0x0040
//#define IPUF_08			0x0080
//#define IPUF_09			0x0100
//#define IPUF_10			0x0200
//#define IPUF_11			0x0400
//#define IPUF_12			0x0800
//#define IPUF_13			0x1000
//#define IPUF_14			0x2000
//#define IPUF_15			0x4000
//#define IPUF_16			0x8000

///*---******* EXTERN FUNCTIONS **************************************************************---*/
extern int Init_Table_Terminal(void);
extern int Init_Table_Merchant(void);
extern int Init_Table_Host(void);
extern int Init_Table_Range(void);
extern int Init_Table_IP(void);
extern int Init_Table_MerchantRange(void);
extern int Init_Table_MerchantGroup(void);


///*---******* LOCAL FUNCTIONS **************************************************************---*/
void SaveTerminalField(char *LoadBuffer , int TermialTag);
void SaveMerchantField(char *LoadBuffer , int MerchantTag, int MerCont);
void SaveHostField(char *LoadBuffer , int HostTag);
void SaveRangeField(char *LoadBuffer , int HostTag);
void SaveIPField(char *LoadBuffer , int IPTag);
void SaveGPRSField(char *LoadBuffer , int GprsTag);
void SaveSSLCertField(char *LoadBuffer , int SSLCertTag);
void SaveMerchRange(char *LoadBuffer , int MerchRangeTag);
uint32 BuildTagLength(char *LoadBuffer, uint32 Position);

#ifndef NOT_iPOSApp
int SetOldBatch(int contm);

#ifdef MAKE_STATISTICS //@pdm 2012-01-20
int16 prnPrint2(uint32 handle, char * text);
#endif //MAKE_STATISTICS
#endif //NOT_iPOSApp

#define MAKE_MERGROUP
#ifdef MAKE_MERGROUP
void SaveMerchGroup(char *LoadBuffer , int MerchGroupTag);
#endif //MAKE_MERGROUP


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
GENERAL_CONF 		tConfAux;
TABLE_TERMINAL 		tTerminalAux;
TABLE_MERCHANT 		tMerchAux;
TABLE_HOST 			tHostAux;
TABLE_RANGE 		tRangeInfoAux;
TABLE_IP			tIPConfAux;
TABLE_MERCH_RANGE	tMerchRangeAux;

#ifdef MAKE_MERGROUP
TABLE_MERCH_GROUP	tMerchGroupAux;
#endif //MAKE_MERGROUP

bool PrintDebug;

#ifndef NOT_iPOSApp
char LoadBuffer[L_ParBuffer];
uint32 BufferIndex;

TERMINALCFG 	tConfAux;
MERCHANT 		tMerchAux;
HOST 			tHostAux;
RANGES			tRangeInfoAux;
IPCONF			tIPConfAux;
GPRSCONF		tGPRSAux;
MERCH_RANGE		tMerchRangeAux;

#ifdef MAKE_MERGROUP
MERGROUP		tMerchGroupAux;
#endif //MAKE_MERGROUP

#ifdef MAKE_STATISTICS //@pdm 2012-01-20
STATISTIC 		tStatisticsAux;
#endif //MAKE_STATISTICS

MERCHBATCH MerchsInfo[D_MAX_MERCHANT];
#endif

char			SSLCertName[20];

#ifdef MAKE_EMV
char bEMV;
#endif // MAKE_EMV

/*---******* IMPLEMENTATION  **************************************************************---*/

/*****************************************************************************
 * Description: Save field of Terminal Buffer
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveTerminalField(char *LoadBuffer , int TerminalTag)
{
	// Build Length of field
	uint32 Flenght;

	char myLine[50];
	char tmpChar[50];

	// Get tag length
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	// Clear myLine
	memset(myLine,0x20,42);
	memset(tmpChar,0x00,50);
	sprintf(myLine,"T%i:", TerminalTag);

	switch (TerminalTag)
	{
	case 1: // T_HeaderL1
		// Copy field to structure
		memcpy(tTerminalAux.Header_Line1 , &LoadBuffer[BufferIndex] , Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 2: // T_HeaderL2
		// Copy field to structure
		memcpy(tTerminalAux.Header_Line2 , &LoadBuffer[BufferIndex] , Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 3: // T_HeaderL3
		// Copy field to structure
		memcpy(tTerminalAux.Header_Line3 , &LoadBuffer[BufferIndex] , Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 4: // T_HeaderL4
		// Copy field to structure
		memcpy(tTerminalAux.Header_Line4 , &LoadBuffer[BufferIndex] , Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 5: // T_PwdMerch
		// Copy field to structure
		memcpy(tTerminalAux.Password_Merchant , &LoadBuffer[BufferIndex] , Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 6: // T_PwdTechSupp
		// Copy field to structure
		memcpy(tTerminalAux.Password_Technician , &LoadBuffer[BufferIndex] , Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 7: // T_CurrSymLocal
		// Copy field to structure
		memcpy(tTerminalAux.CurrSymbol_Local, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 8: // T_CurrSymForgn1
		// Copy field to structure
		memcpy(tTerminalAux.CurrSymbol_Dollar, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 9: // T_CurrSymForgn2
		// Copy field to structure
		//+ CLESS @mamata Dec 15, 2014
		memcpy(tTerminalAux.CurrCode_Local, &LoadBuffer[BufferIndex],Flenght);
		//d memcpy(tTerminalAux.CurrSymbol_Euro, &LoadBuffer[BufferIndex],Flenght);
		//- CLESS
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 10: // T_UsrLabel1 - FLOTA KILOMETRAJE LABEL
		// Copy field to structure
		memcpy(tTerminalAux.User_Label1, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 11: // T_UsrLabel2 - FLOTA LITROS LABEL
		// Copy field to structure
		memcpy(tTerminalAux.User_Label2, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 12: // T_UsrLabel3 - FLOTA PLACA LABEL
		// Copy field to structure
		memcpy(tTerminalAux.User_Label3, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 13: // T_UsrLabel4
		memcpy(tTerminalAux.User_Label4, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 14: // T_FooterL1
		// Copy field to structure
		memcpy(tTerminalAux.Footer_Line1, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 15: // T_FooterL2
		// Copy field to structure
		memcpy(tTerminalAux.Footer_Line2, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 16: // T_FooterL3
		// Copy field to structure
		memcpy(tTerminalAux.Footer_Line3, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 17: // T_FooterL4
		// Copy field to structure
		memcpy(tTerminalAux.Footer_Line4, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 18: // T_DisclaimerL1
		// Copy field to structure
		memcpy(tTerminalAux.Disclaimer_Line1, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 19: // T_DisclaimerL2
		// Copy field to structure
		memcpy(tTerminalAux.Disclaimer_Line2,&LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 20: // T_DisclaimerL3
		// Copy field to structure
		memcpy(tTerminalAux.Disclaimer_Line3,&LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 21: // T_DisclaimerL4
		// Copy field to structure
		memcpy(tTerminalAux.Disclaimer_Line4, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 22: // T_PANMask
		// Copy field to structure
		memcpy(tTerminalAux.PanMask, &LoadBuffer[BufferIndex],Flenght);
		// Copy field for report
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 23: // T_DispTimeOut
		// Copy field to structure
		tTerminalAux.TimeOut = LoadBuffer[BufferIndex];
		// Copy field for report
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", TerminalTag, LoadBuffer[BufferIndex]);
		break;
	case 24: // T_Flags1]
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tTerminalAux.Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 25: // T_Flags2
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tTerminalAux.Flags2 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 26: // T_UserFlags1
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tTerminalAux.User_Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 27: // T_UserFlags2
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tTerminalAux.User_Flags2 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 28: // T_UserPwd1
		memcpy(tTerminalAux.User_Password1, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 29: // T_UserPwd2
		memcpy(tTerminalAux.User_Password2, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 30: // T_UserPwd3
		memcpy(tTerminalAux.User_Password3, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 31: // T_UserPwd4
		memcpy(tTerminalAux.User_Password4, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 32: // T_UserField01 - TIP Label
		memset(&tTerminalAux.User_Field01, 0, LZ_USER_FIELD);
		memcpy(tTerminalAux.User_Field01 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 33: // T_UserField02 - TAX Label
		memset(&tTerminalAux.User_Field02, 0, LZ_USER_FIELD);
		memcpy(tTerminalAux.User_Field02 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 34: // T_UserField03 - Max Amount with out sing
		memset(&tTerminalAux.User_Field03, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field03 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 35: // T_UserField04 - RECARGA phone number label
		memset(&tTerminalAux.User_Field04 ,0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field04 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 36: // T_UserField05 - RECARGA new amount label
		memset(&tTerminalAux.User_Field05, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field05 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 37: // T_UserField06 - RECARGA confirmation number
		memset(&tTerminalAux.User_Field06, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field06 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 38: // T_UserField07 - RECARGA company menu
		memset(&tTerminalAux.User_Field07, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field07 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 39: // T_UserField08 - RECARGA service type menu
		memset(&tTerminalAux.User_Field08, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field08 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 40: // T_UserField09 - PPLANCR label
		memset(&tTerminalAux.User_Field09, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field09 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 41: // T_UserField10 - PPLANEX label
		memset(&tTerminalAux.User_Field10, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field10 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 42: // T_UserField11 - PPLANSF label
		memset(&tTerminalAux.User_Field11, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field11 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 43: // T_UserField12 - PPLANCL label
		memset(&tTerminalAux.User_Field12, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field12 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 44: // T_UserField13 - Statistics Nii
		memset(&tTerminalAux.User_Field13, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field13 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 45: // T_UserField14
		//memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memset(&tTerminalAux.User_Field14, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field14 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 46: // T_UserField15
		//memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memset(&tTerminalAux.User_Field15, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field15 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 47: // T_UserField16
		memset(&tTerminalAux.User_Field16, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field16 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 48: // T_UserField17
		memset(&tTerminalAux.User_Field17, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field17 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 49: // T_UserField18
	   #ifdef MAKE_CASHIERS
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		tConfAux.ENABLE_CASHIERS = atoi(tmpChar);
	   #endif //MAKE_CASHIERS
		memset(&tTerminalAux.User_Field18, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field18 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 50: // T_UserField19
	   #ifdef MAKE_CASHIERS
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		tConfAux.ENTER_CASHIERS = atoi(tmpChar);
	   #endif //MAKE_CASHIERS
		memset(&tTerminalAux.User_Field19, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field19 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 51: // T_UserField20
	   #ifdef MAKE_CASHIERS
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		tConfAux.ENTER_TILL_TABLE = atoi(tmpChar);
	   #endif //MAKE_CASHIERS
		memset(&tTerminalAux.User_Field20, 0, LZ_USER_FIELD);
		memcpy( tTerminalAux.User_Field20 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 52: // T_UserField20
		memcpy(tMerchAux.Name, &LoadBuffer[BufferIndex],Flenght);
	    memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug != 0)
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}


}

/*****************************************************************************
 * Description: Save field of Merchant Buffer
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveMerchantField(char *LoadBuffer , int MerchantTag, int MerCont)
{
	// Build Length of field
	uint32 Flenght;

	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];
	char tempstr[50];

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", MerchantTag);

	memset(tempstr,0,sizeof(tempstr));

	switch (MerchantTag)
	{
	case 1: // M_MerchantID
		tMerchAux.Header.RecordID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		sprintf((char*)tMerchAux.BatchName, "BATCH%ld", tMerchAux.Header.RecordID);
		break;
	case 2: // M_Name
		memcpy(tMerchAux.Name, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 3: // M_HostID
		tMerchAux.HostID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 4: // M_CardAccpTerm
		memset(tMerchAux.TID, 0x20, 8 );
		memcpy(tMerchAux.TID,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 5: // M_CardAccpMerch
		memset(tMerchAux.MID, 0x20, 15 );
		memcpy(tMerchAux.MID,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 6: // M_Currency
		tMerchAux.Currency = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 7: // M_Pwd
		memcpy(tMerchAux.Password,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 8: // M_HeaderL1
		memcpy(tMerchAux.Header_Line1,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 9: // M_HeaderL2
		memcpy(tMerchAux.Header_Line2,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 10: // M_HeaderL3
		memcpy(tMerchAux.Header_Line3,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 11: // M_HeaderL4
		memcpy(tMerchAux.Header_Line4,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 12: // M_MaxDigits
		tMerchAux.Amount_MaxDigtis = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 13: // M_DecPosition
		tMerchAux.Amount_DecimalPosition = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 14: // M_TipType
		tMerchAux.TIP_type = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 15: // M_TipMaxPcnt
		tMerchAux.TIP_Max_Percent = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 16: // M_TipGuidePcnt1
		tMerchAux.TIP_Guide1 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 17: // M_TipGuidePcnt2
		tMerchAux.TIP_Guide2 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 18: // M_TipGuidePcnt3
		tMerchAux.TIP_Guide3 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 19: // M_TaxType
		tMerchAux.TAX_type = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 20: // M_TaxMaxPcnt
		tMerchAux.TAX_Max_Percent = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 21: // M_AdjMaxPcnt
		tMerchAux.Adjust_Max_Percent = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 22: // M_DefTran
		tMerchAux.Defualt_Transaction = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 23: // M_BatchNumber
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		tMerchAux.CurrentBatch = atoi(tempstr);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 24: // M_TranMaxAmt
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		tMerchAux.Amount_Max = atol(tempstr);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 25: // M_QuickPayment
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		tMerchAux.QuickPayment = atol(tempstr);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 26: // M_Mortages
		//@mdm Payment_Plan 2012-01-09
		tMerchAux.Mortages = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;
	case 27: // M_TranFlags1
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tMerchAux.Transactions1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 28: // M_TranFlags2
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tMerchAux.Transactions2 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 29: // M_Flag1
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tMerchAux.Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 31: // M_UserFlags1
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tMerchAux.User_Flags1= (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 32: // M_UserFlags2
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tMerchAux.User_Flags2= (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 33: // M_UserPwd1
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Password1 , &LoadBuffer[BufferIndex],Flenght);
		break;
	case 34: // M_UserPwd2
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Password2 , &LoadBuffer[BufferIndex],Flenght);
		break;
	case 35: // M_UserPwd3
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Password3 , &LoadBuffer[BufferIndex],Flenght);
		break;
	case 36: // M_UserPwd4
		memcpy(tMerchAux.User_Password4 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 37: // M_UserField1
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Field01 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 38: // M_UserField2
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Field02 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 39: // M_UserField3
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Field03 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 40: // M_UserField4
		//@mdm	AutoSettle
		memcpy(tMerchAux.User_Field04 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 41: // M_UserField5
       #ifdef MAKE_PUNTOS
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		tMerchAux.User_Field05 = atol(tempstr);
       #endif //MAKE_PUNTOS
		memcpy(tMerchAux.User_Field05 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 42: // M_UserField6
	   #ifdef MAKE_TAXDISC
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		tMerchAux.User_Field06 = atoi(tempstr);
	   #endif //MAKE_TAXDISC
		memcpy(tMerchAux.User_Field06 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 43: // M_UserField7
	   #ifdef MAKE_TAXDISC
		memcpy(tempstr,&LoadBuffer[BufferIndex],Flenght);
		tMerchAux.User_Field07 = atol(tempstr);
	   #endif //MAKE_TAXDISC
		memcpy(tMerchAux.User_Field07 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 44: // M_UserField8
	   #ifdef MAKE_REMESA
		memcpy(tMerchAux.User_Field08,&LoadBuffer[BufferIndex],Flenght);
	   #endif //MAKE_REMESA
		memcpy(tMerchAux.User_Field08 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 45: // M_UserField9
	   #ifdef MAKE_REMESA
		memcpy(tMerchAux.User_Field09,&LoadBuffer[BufferIndex],Flenght);
	   #endif //MAKE_REMESA
		memcpy(tMerchAux.User_Field09 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 46: // M_UserField10
		//+ CHN0006
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		memcpy(tMerchAux.User_Field10 , &LoadBuffer[BufferIndex],Flenght);
		//- CHN0006
		break;
	case 47: // M_UserField11
		memcpy(tMerchAux.User_Field11 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 48: // M_UserField12
		memcpy(tMerchAux.User_Field12 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 49: // M_UserField13
		memcpy(tMerchAux.User_Field13 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 50: // M_UserField14
		memcpy(tMerchAux.User_Field14 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 51: // M_UserField15
		memcpy(tMerchAux.User_Field15 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 52: // M_UserField16
		memcpy(tMerchAux.User_Field16 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 53: // M_UserField17
		memcpy(tMerchAux.User_Field17 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 54: // M_UserField18
		memcpy(tMerchAux.User_Field18 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 55: // M_UserField19
		memcpy(tMerchAux.User_Field19 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 56: // M_UserField20
		memcpy(tMerchAux.User_Field20 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;

	case 57:
		tMerchAux.MerchantGroupID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchantTag, LoadBuffer[BufferIndex]);
		break;


	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}
/*****************************************************************************
 * Description: Save field of Host Buffer
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveHostField(char *LoadBuffer , int HostTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);
	char myLine[50];

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", HostTag);

	switch (HostTag)
	{
	case 1: // H_HostID
		tHostAux.Header.RecordID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 2: // H_name
		memcpy(tHostAux.Name,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 3: // H_CommType
		tHostAux.CommunicationType = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 4: // H_MsgFormat
		tHostAux.MessageFormat = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 5: // H_ConnMode
		tHostAux.ConnectionMode = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 6: // H_Retries
		tHostAux.ConnectionRetries = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 7: // H_TimeoutConn
		tHostAux.ConnectionTimeOut = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 8: // H_TimeoutAnsw
		tHostAux.ResponseTimeOut = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 9: // H_NII
		memcpy(tHostAux.NII,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 10: // H_EncWrkKey1
		memcpy(tHostAux.EncrypedWorkingKey1,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 11: // H_EncWrkKey2
		memcpy(tHostAux.EncrypedWorkingKey2,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 12: // H_DialTone
		tHostAux.Dial_Tone = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	//+ HOST01 @mamata Aug 14, 2014
	case 13: // H_PhoneTran1
		memcpy(tHostAux.Dial_Primary_Phone1,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 14: // H_PhoneTran2
		memcpy(tHostAux.Dial_Primary_Phone2,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 15: // H_PhoneSett1
		memcpy(tHostAux.Dial_Backup_Phone1,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 16: // H_PhoneSett2
		memcpy(tHostAux.Dial_Backup_Phone2,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 17: // H_IpTran1
		tHostAux.IP_Primary_Address1 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 18: // H_IpTran2
		tHostAux.IP_Primary_Address2 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 19: // H_IpSett1
		tHostAux.IP_Backup_Address1 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 20: // H_IpSett2
		tHostAux.IP_Backup_Address2 = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	//- HOST01
	case 21: // H_Flags
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tHostAux.Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 22: // H_GPRS
		memset(myLine,0x20,42);
		tHostAux.GPRS = LoadBuffer[BufferIndex];
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;
	case 23: // H_UserFlags
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tHostAux.User_Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 24: // H_UserField01
		memcpy(tHostAux.User_Field01 , &LoadBuffer[BufferIndex],Flenght);
		tHostAux.MasterKeyIndex = atoi(&LoadBuffer[BufferIndex]);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 25: // H_UserField02
//		memcpy(tHostAux.DefAcc,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tHostAux.User_Field02 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 26: // H_UserField03
		memcpy(tHostAux.User_Field03 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 27: // H_UserField04
		memcpy(tHostAux.User_Field04 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 28: // H_UserField05
		memcpy(tHostAux.User_Field05 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 29: // H_UserField06
		memcpy(tHostAux.User_Field06 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 30: // H_UserField07
		memcpy(tHostAux.User_Field07 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 31: // H_UserField08
		memcpy(tHostAux.User_Field08 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 32: // H_UserField09
		memcpy(tHostAux.User_Field09 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 33: // H_UserField10
		memcpy(tHostAux.User_Field10 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 34: // H_UserField11
		memcpy(tHostAux.User_Field11 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 35: // H_UserField12
		memcpy(tHostAux.User_Field12 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 36: // H_UserField13
		memcpy(tHostAux.User_Field13 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 37: // H_UserField14
		memcpy(tHostAux.User_Field14 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 38: // H_UserField15
		memcpy(tHostAux.User_Field15 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 39: // H_UserField16
		memcpy(tHostAux.User_Field16 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 40: // H_BackUpCommType
		tHostAux.BackupCommunicationType = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", HostTag, LoadBuffer[BufferIndex]);
		break;

	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}

/*****************************************************************************
 * Description: Save field of Range Buffer
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveRangeField(char *LoadBuffer , int RangeTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];
	char tmpChar[50];

	// Clear myLine
	memset(myLine,0x20,42);
	memset(tmpChar,0x00,50);
	sprintf(myLine,"T%i:", RangeTag);

	switch (RangeTag)
	{
	case 1: // R_RangeID
		tRangeInfoAux.Header.RecordID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", RangeTag, LoadBuffer[BufferIndex]);
		break;
	case 2: // H_HostID
		tRangeInfoAux.HostID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", RangeTag, LoadBuffer[BufferIndex]);
		break;
	case 3: // R_name
		memcpy(tRangeInfoAux.Name,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 4: // R_RangeMin
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		tRangeInfoAux.Bin_Min = atof(tmpChar);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 5: // R_RangeMax
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		tRangeInfoAux.Bin_Max = atof(tmpChar);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 6: // R_length
		tRangeInfoAux.Max_PAN_Length = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", RangeTag, LoadBuffer[BufferIndex]);
		break;
	case 7: // R_DefAcctType
		tRangeInfoAux.DefaultAccount = LoadBuffer[BufferIndex];
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 8: // R_RangeFlags
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tRangeInfoAux.Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 9: // R_UserRangeFlags
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tRangeInfoAux.User_Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 10: // R_UserField1
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tRangeInfoAux.User_Field01 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 11: // R_UserField2
		//@mdm Descuento 2012-01-16
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tRangeInfoAux.User_Field02 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 12: // R_UserField3
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tRangeInfoAux.User_Field03 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 13: // R_UserField4
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tRangeInfoAux.User_Field04 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 14: // R_UserField5
		memcpy(tmpChar,&LoadBuffer[BufferIndex],Flenght);
		memcpy(tRangeInfoAux.User_Field05 , &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}

/*****************************************************************************
 * Description: Save field of IP Buffer
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveIPField(char *LoadBuffer , int IPTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];
	char TempBuffer[50];

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", IPTag);

	switch (IPTag)
	{
	case 1: // IP_IpGprsID
		tIPConfAux.Header.RecordID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", IPTag, LoadBuffer[BufferIndex]);
		break;
	case 2: // IP_HostIP
		memcpy(tIPConfAux.Host, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 3: // IP_HostPort
		memset(TempBuffer, 0, 50);
		memcpy(tIPConfAux.Port, &LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 4: // IP_URL
		memcpy(tIPConfAux.Host,&LoadBuffer[BufferIndex],Flenght);
		//memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 5: // IP_IpFlags
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tIPConfAux.Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
	case 6: // IP_UserIpFlags
		memcpy(&myLine[strlen(myLine)],"Flag",4);
		tIPConfAux.User_Flags1 = (LoadBuffer[BufferIndex]*256) + LoadBuffer[BufferIndex+1];
		break;
//	case 7: // IP_UserField1
//		memcpy(tIPConfAux.User_Field01 ,&LoadBuffer[BufferIndex],Flenght);
//		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
//		break;
//	case 8: // IP_UserField2
//		memcpy(tIPConfAux.User_Field02 ,&LoadBuffer[BufferIndex],Flenght);
//		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
//		break;
	case 9: // IP_UserField3
		memcpy(tIPConfAux.User_Field03 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 10: // IP_UserField4
		memcpy(tIPConfAux.User_Field04 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 11: // IP_UserField5
		memcpy(tIPConfAux.User_Field05 ,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 12: // SSL_Cert1
		memcpy(tIPConfAux.Server_SSL_Profile,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 13: // SSL_Cert2
		memcpy(tIPConfAux.Client_SSL_Profile,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}

/*****************************************************************************
 * Description: Save field of GPRS Buffer
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveGPRSField(char *LoadBuffer , int GprsTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", GprsTag);

	switch (GprsTag)
	{
	case 1: // G_GprsID
//		tGPRSAux.recCtrl.id = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 2: // G_LoginName
//		memcpy(tGPRSAux.GPRS_User,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 3: // G_Password
//		memcpy(tGPRSAux.GPRS_Password,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 4: // G_APN
//		memcpy(tGPRSAux.GPRS_APN,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 5: // G_LCPFlags
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	case 6: // G_ConnTimeOut
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 7: // G_CommTimeOut
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 8: // G_GSMRegTimeOut
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 9: // G_LogInTimeOut
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 10: // G_GPRSAttachTimeOut
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 11: // G_DetachTimeOut
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 12: // G_RetryDelay
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	case 13: // G_TcpNoDelay
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", GprsTag, LoadBuffer[BufferIndex]);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}

/*****************************************************************************
 * Description: Save field of MerchRange table
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveMerchRange(char *LoadBuffer , int MerchRangeTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", MerchRangeTag);

	switch (MerchRangeTag)
	{
	case 1: // Merchant ID
		tMerchRangeAux.MerchantID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchRangeTag, LoadBuffer[BufferIndex]);
		break;
	case 2: // Range ID
		tMerchRangeAux.RangeID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchRangeTag, LoadBuffer[BufferIndex]);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}

/*****************************************************************************
 * Description: Save field of SSLCert files
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveSSLCertField(char *LoadBuffer , int SSLCertTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];

	unsigned int nMode = FS_NOFLAGS;
	char Label_P[33];
	S_FS_FILE *hFile;

	strcpy(Label_P,"/HOST");

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", SSLCertTag);

	switch (SSLCertTag)
	{
	case 1: // Name
		memset(SSLCertName,0x00,20);
		memcpy(SSLCertName,"/HOST/",6);
		memcpy(&SSLCertName[6],&LoadBuffer[BufferIndex],Flenght);
		memcpy(&SSLCertName[6+Flenght],".PEM",4);
#ifndef NOT_iPOSApp
		memcpy(tGPRSAux.GPRS_User,&LoadBuffer[BufferIndex],Flenght);
#endif
		break;
	case 2: // Current cert
		// Open file, overwrite if exists
		if( FS_mount (Label_P, &nMode) == FS_OK )
		{
			FS_unlink(SSLCertName);
			hFile = FS_open( SSLCertName, "a" );
			if( hFile != NULL )
			{
				if (FS_write( &LoadBuffer[BufferIndex], Flenght, 1, hFile ) == 1 )
					;//TODO Display dspMsg("SSLCERT GUARDADO", LIN3, 0, CENTER );
				else
					;//TODO Display dspMsg("ERROR SSL CERT", LIN3, 0, CENTER );

				FS_close (hFile);

			}

			FS_unmount (Label_P);
		}
		memcpy(&myLine[strlen(myLine)],"Current Cert",12);
		break;
	case 3: // Current cert
		memcpy(&myLine[strlen(myLine)],"Future Cert",11);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}

}

#ifdef MAKE_MERGROUP
/*****************************************************************************
 * Description: Save field of Merchant Group table
 *
 * Parameters: I/O Description
 *   - TermialTag : Tag number
 *
 * Returns:
 *   - none
 **************************************************************************--*/
void SaveMerchGroup(char *LoadBuffer , int MerchGroupTag)
{
	// Build Length of field
	uint32 Flenght;
	Flenght = BuildTagLength(LoadBuffer , BufferIndex);

	char myLine[50];

	// Clear myLine
	memset(myLine,0x20,42);
	sprintf(myLine,"T%i:", MerchGroupTag);

	switch (MerchGroupTag)
	{
	case 1: // Merchant ID
		tMerchGroupAux.Header.RecordID = LoadBuffer[BufferIndex];
		memset(myLine,0x20,42);
		sprintf(myLine,"T%i:%i", MerchGroupTag, LoadBuffer[BufferIndex]);
		break;
	case 2: // Range ID
		memcpy(tMerchGroupAux.Name,&LoadBuffer[BufferIndex],Flenght);
		memcpy(&myLine[strlen(myLine)],&LoadBuffer[BufferIndex],Flenght);
		break;
	}
	// increment BufferIndex whit field length
	BufferIndex += Flenght;

	// Print report
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		myLine[42] = 0;
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	}
}
#endif //MAKE_MERGROUP

/*****************************************************************************
 * Description: Build Tag or Length number
 *
 * Parameters: I/O Description
 *   - Index : Buffer Index
 *
 * Returns:
 *   - none
 **************************************************************************--*/
uint32 BuildTagLength(char *LoadBuffer, uint32 Index)
{
	uint32 Result = 0;
	int cont = 0;
	while (TRUE)
	{
		// See if the number has another byte
		if (LoadBuffer[Index + cont] >= 128)
			cont++;
		else
			break;
	}

	switch (cont)
	{
		case 0:
			Result = LoadBuffer[Index];
			BufferIndex += 1;
			break;
		case 1:
			Result = LoadBuffer[Index + 1];
			Result += ((LoadBuffer[Index] - 128) * 128);
			BufferIndex += 2;
			break;
		case 2:
			Result = LoadBuffer[Index + 2];
			Result += ((LoadBuffer[Index + 1] - 128) * 128);
			Result += ((LoadBuffer[Index] - 128) * 16384);
			BufferIndex += 3;
			break;
		case 3:
			Result = LoadBuffer[Index + 3];
			Result += ((LoadBuffer[Index + 2] - 128) * 128);
			Result += ((LoadBuffer[Index + 1] - 128) * 16384);
			Result += ((LoadBuffer[Index] - 128) * 2097152);
			BufferIndex += 4;
			break;
	}

	return Result;
}

uint16 UnpackBuffer(char *LoadBuffer,  uint32 BufferLength )
{
	//+ BATCH01 @mamata Aug 15, 2014
	TLV_TREE_NODE CurrentBatchsInfo = NULL;
	//- BATCH01

	memset((char*)&tTerminalAux, 0, L_TABLE_TERMINAL);
	memset((char*)&tMerchAux, 0, L_TABLE_MERCHANT);
	memset((char*)&tHostAux, 0, L_TABLE_HOST);
	memset((char*)&tRangeInfoAux, 0, L_TABLE_RANGE);
	memset((char*)&tIPConfAux, 0, L_TABLE_IP);
	memset((char*)&tMerchRangeAux, 0, L_TABLE_MERCH_RANGE);
	memset((char*)&tMerchGroupAux, 0, L_TABLE_MERCH_GROUP);

	BufferIndex = 0;
	uint8 Table = 0;
	uint32 Tag	= 0;
	int merch_cont = 0;
	char myLine[50] = {0};
	int iRet = RET_NOK;

	//+ BATCH01 @mamata Aug 15, 2014
	CurrentBatchsInfo = TlvTree_New(0);
	CHECK(CurrentBatchsInfo != NULL, LBL_ERROR);
	iRet = SaveBatchsInformation(CurrentBatchsInfo);
	iRet = RET_NOK;
	//- BATCH01


#ifndef NOT_iPOSApp
	// Merchant Range cont
	int MR_Cont = 0;

   #ifdef MAKE_AUTOSETTLE
	RTC_    Reloj;//@AJAF
   #endif //MAKE_AUTOSETTLE

   #ifdef MAKE_STATISTICS
	uint32 sysDate, sysTime, DateNow;
   #endif //MAKE_STATISTICS

	// Save data of Merchants
	merch_cont = SaveMerchantInfo(MerchsInfo);

	// Clear all tables
	tabClearAllIngeLoad();
#endif
	if(TM_DeleteTable(TAB_TERMINAL) 	== TM_RET_OK)
	if(Init_Table_Terminal()			== TM_RET_OK)
	if(TM_DeleteTable(TAB_MERCHANT)		== TM_RET_OK)
	if(Init_Table_Merchant()			== TM_RET_OK)
	if(TM_DeleteTable(TAB_HOST)			== TM_RET_OK)
	if(Init_Table_Host()				== TM_RET_OK)
	if(TM_DeleteTable(TAB_RANGE)		== TM_RET_OK)
	if(Init_Table_Range()				== TM_RET_OK)
	if(TM_DeleteTable(TAB_IP)			== TM_RET_OK)
	if(Init_Table_IP()					== TM_RET_OK)
	if(TM_DeleteTable(TAB_MERCH_RANGE)	== TM_RET_OK)
	if(Init_Table_MerchantRange()		== TM_RET_OK)
	if(TM_DeleteTable(TAB_MERCH_GROUP)	== TM_RET_OK)
		iRet = Init_Table_MerchantGroup();

	if(iRet != TM_RET_OK)
		return iRet;

	//MERCHANT tOldMerch;

	// Set Aux structures to 0
	//memset(&tConfAux , 0x00 , sizeof(GENERAL_CONF));

   #ifdef MAKE_EMV
	bEMV = 0;
   #endif // MAKE_EMV

	Table = 0;

	while (BufferIndex <= BufferLength)
	{
		if( ( (LoadBuffer[BufferIndex] == 0x00) && (LoadBuffer[BufferIndex + 1] == 0x02) )
				|| ( BufferIndex == BufferLength )
		)
		{
			if (Table != 0)
			{
				switch (Table)
				{
				case 0x0B:  // Terminal
#ifndef NOT_iPOSApp
					tConfAux.Signature	= SIGNATURE;
					tConfAux.bTerminalConfigured = TERM_READY;

					//@mdm Payment_Plan 2012-01-09
					if( tConfAux.Label_PPLANCR[0] == 0 )
						memcpy(tConfAux.Label_PPLANCR, "CREDITO         ", 16);
					if( tConfAux.Label_PPLANEX[0] == 0 )
						memcpy(tConfAux.Label_PPLANEX, "EXTRA FINANCIAMI", 16);
					if( tConfAux.Label_PPLANSF[0] == 0 )
						memcpy(tConfAux.Label_PPLANSF, "VENTA FINANCIADA", 16);
					if( tConfAux.Label_PPLANCL[0] == 0 )
						memcpy(tConfAux.Label_PPLANCL, "RETIRO A PLAZO  ", 16);

					if( tConfAux.RECARGA_Num[0] == 0 )
						memcpy(tConfAux.RECARGA_Num, "TELEFONO Nro.   ", 16);


				#ifdef MAKE_STATISTICS //@pdm 2012-01-20
					memcpy(tStatistic.sNii, tStatisticsAux.sNii, 4);
					tStatistic.sTransNum = tStatisticsAux.sTransNum;
					tStatistic.sDaysNum = tStatisticsAux.sDaysNum;

					// Temporal
					tStatistic.sLastSend = 0;

					if( tStatistic.sNii[0] != 0x00 && tStatistic.sLastSend == 0)
					{
						psyDateTimeGet(&sysDate, &sysTime);
						DateNow = DaysFrom2000(PSY_YEAR(sysDate), PSY_MONTH(sysDate), PSY_DAY(sysDate));
						tStatistic.sLastSend = DateNow - 5;
					}

					/*if(tStatistic.sDaysNum == 0)
						tStatistic.sDaysNum = 5;*/

					tabSaveFile( FILE_STATISTICS, (LPTSTR) &tStatistic, sizeof(STATISTIC) );
				#endif //MAKE_STATISTICS

					if ( tabSaveFile( FILE_CONFIG, (LPTSTR) &tConfAux, sizeof(TERMINALCFG) ) == RET_OK )
					{
						memcpy( (LPTSTR) &tConf, (LPTSTR) &tConfAux, sizeof(TERMINALCFG) );
						tConf.nTimeout *= SECOND;
					}
					else
						return RET_NOK;
#endif
					iRet = TM_AddRecord(TAB_TERMINAL, &tTerminalAux);
					if(iRet == TM_RET_OK)
						memcpy((char *)&tTerminal , (char *)&tTerminalAux , sizeof(TABLE_TERMINAL));
					else
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					memset((char*)&tTerminalAux, 0, L_TABLE_TERMINAL);
					break;
				case 0x0C:  // Merchant
                   #ifdef MAKE_PUNTOS
					if( tMerchAux.nPuntosOptMax == 0)
						tMerchAux.nPuntosOptMax = 2;
					if( tMerchAux.nPuntosOptMax > 19)
						tMerchAux.nPuntosOptMax = 19;
                   #endif //MAKE_PUNTOS

				   #ifdef MAKE_AUTOSETTLE
					//En TLC de parametros se programa el dia actual, @AJAF
					fvLIBTStamp_Get(&Reloj);//en TLC de parametros se programa el dia actual
					memcpy(&tMerchAux.RTC_NextSettle, &Reloj, sizeof(RTC_));
					//En TLC de parametros se programa el dia actual, @AJAF
				   #endif //MAKE_AUTOSETTLE
#ifndef NOT_iPOSApp
					tabOpen(MERCHANT_TABLE);
					if( tabNumRec(MERCHANT_TABLE) <= D_MAX_MERCHANT )
						tabAddRec(MERCHANT_TABLE, (LPTSTR) &tMerchAux);
					tabClose(MERCHANT_TABLE);
#endif

					//+ BATCH01 @mamata Aug 15, 2014
					if (tMerchAux.BatchNumber == 9999999)
						tMerchAux.BatchNumber = GetCurrentBatchNumber(tMerchAux.TID, tMerchAux.MID, CurrentBatchsInfo);
					if(tMerchAux.BatchNumber == 0)
						tMerchAux.BatchNumber = 1;
					//- BATCH01

					iRet = TM_AddRecord(TAB_MERCHANT, &tMerchAux);
					if(iRet != RET_OK)
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					memset((char*)&tMerchAux, 0, L_TABLE_MERCHANT);
					break;
				case 0x0D:  // Host
					//@mdm Actualizar el TPDU con el NII
#ifndef NOT_iPOSApp
					memset(tHostAux.tpdu,0x30,10);
					tHostAux.tpdu[0] = 0x36;
					tHostAux.tpdu[1] = 0x30;
					memcpy( &tHostAux.tpdu[3], tHostAux.nii, 3 );
					tabOpen(HOST_TABLE);
					if( tabNumRec(HOST_TABLE) <= D_MAX_HOSTS )
						tabAddRec(HOST_TABLE, (LPTSTR) &tHostAux);
					tabClose(HOST_TABLE);
#endif
					iRet = TM_AddRecord(TAB_HOST, &tHostAux);
					if(iRet != RET_OK)
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					break;
					memset((char*)&tHostAux, 0, L_TABLE_HOST);
				case 0x0E:  // Range
#ifndef NOT_iPOSApp
					tRangeInfoAux.bEnable = 1;
					tabOpen(RANGE_TABLE);
					if( tabNumRec(RANGE_TABLE) <= D_MAX_ISSUERS )
						tabAddRec(RANGE_TABLE, (LPTSTR) &tRangeInfoAux);
					tabClose(RANGE_TABLE);
#endif
					iRet = TM_AddRecord(TAB_RANGE, &tRangeInfoAux);
					if(iRet != RET_OK)
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					memset((char*)&tRangeInfoAux, 0, L_TABLE_RANGE);
					break;
				case 0x0F:  // IP
#ifndef NOT_iPOSApp
					tabOpen(IPCONF_TABLES);
					if( tabNumRec(IPCONF_TABLES) <= D_MAX_IPCONF )
						tabAddRec(IPCONF_TABLES, (LPTSTR) &tIPConfAux);
					tabClose(IPCONF_TABLES);
#endif
					iRet = TM_AddRecord(TAB_IP, &tIPConfAux);
					if(iRet != TM_RET_OK)
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					memset((char*)&tIPConfAux, 0, L_TABLE_IP);
					break;
				case 0x10:  // GPRS
#ifndef NOT_iPOSApp
					tabOpen(GPRS_TABLE);
					if( tabNumRec(GPRS_TABLE) <= D_MAX_GPRS )
						tabAddRec(GPRS_TABLE, (LPTSTR) &tGPRSAux);
					tabClose(GPRS_TABLE);
#endif
					sprintf(myLine,"\nTabla %d", Table);
					break;
				case 0x12: // MERCH_RANGE
#ifndef NOT_iPOSApp
					MR_Cont++;
					tMerchRangeAux.recCtrl.id = MR_Cont;
					tabOpen(MERCH_RANGE_TABLE);
					if( tabNumRec(MERCH_RANGE_TABLE) <= D_MAX_MERCH_RANGE )
						tabAddRec(MERCH_RANGE_TABLE, (LPTSTR) &tMerchRangeAux);
					tabClose(MERCH_RANGE_TABLE);
#endif
					iRet = TM_AddRecord(TAB_MERCH_RANGE, &tMerchRangeAux);
					if(iRet != TM_RET_OK)
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					memset((char*)&tMerchRangeAux, 0, L_TABLE_MERCH_RANGE);
					break;
				case 0x13:  // Merchant Group
#ifndef NOT_iPOSApp
					tabOpen(MERGROUP_TABLE);
					if( tabNumRec(MERGROUP_TABLE) <= D_MAX_MERCHANT )
						tabAddRec(MERGROUP_TABLE, (LPTSTR) &tMerchGroupAux);
					tabClose(MERGROUP_TABLE);
#endif
					iRet = TM_AddRecord(TAB_MERCH_GROUP, &tMerchGroupAux);
					if(iRet != TM_RET_OK)
						return RET_NOK;

					sprintf(myLine,"\nTabla %d", Table);
					memset((char*)&tMerchGroupAux, 0, L_TABLE_MERCH_GROUP);
					break;

				}
			}
			// Increment BufferIndex
			BufferIndex += 2;

			if (BufferIndex < BufferLength)
			{
				// Clear AUX tables
				memset(&tMerchAux 		, 0x00 , sizeof(TABLE_MERCHANT));
				memset(&tHostAux  		, 0x00 , sizeof(TABLE_HOST));
				memset(&tRangeInfoAux 	, 0x00 , sizeof(TABLE_RANGE));
				memset(&tIPConfAux		, 0x00 , sizeof(TABLE_IP));
				//memset(&tGPRSAux,0x00,sizeof(tGPRSAux)); //TODO Falta esta variable
				memset(&tMerchRangeAux	, 0x00 , sizeof(TABLE_MERCH_RANGE));
				memset(SSLCertName		, 0x00 , sizeof(SSLCertName));

			   #ifdef MAKE_MERGROUP
				memset(&tMerchGroupAux	, 0x00 , sizeof(TABLE_MERCH_GROUP));
			   #endif //MAKE_MERGROUP

			#ifdef MAKE_STATISTICS //@pdm 2012-01-20
				memset((LPTSTR) &tStatisticsAux,0x00,sizeof(tMerchRangeAux));
			#endif //MAKE_STATISTICS

				//+ BATCH01 @mamata Aug 15, 2014
				tMerchAux.BatchNumber = 9999999;
				//- BATCH01
#ifndef NOT_iPOSApp
				tabSaveFile( FILE_STATISTICS, (LPTSTR) &tStatistic, sizeof(STATISTIC) );
#endif
				// Save the  table id
				Table = LoadBuffer[BufferIndex];
				BufferIndex += 1;

				// Save the table lrc
				BufferIndex += 1;

				// Print table number
				if ( PrintDebug )
				{
					UI_OpenPrinter();
					sprintf(myLine,"\nTabla %d", Table);
					UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
					UI_ClosePrinter();
				}
			}
		}
		else
		{
			// Build Tag
			Tag = BuildTagLength(LoadBuffer , BufferIndex);

			switch (Table)
			{
			case 0x0B:  // Terminal
				SaveTerminalField(LoadBuffer , Tag);
				break;
			case 0x0C:  // Merchant
				SaveMerchantField(LoadBuffer, Tag, merch_cont);
				break;
			case 0x0D:  // Host
				SaveHostField(LoadBuffer , Tag);
				break;
			case 0x0E:  // Range
				SaveRangeField(LoadBuffer , Tag);
				break;
			case 0x0F:  // IP
				SaveIPField(LoadBuffer , Tag);
				break;
			case 0x10:  // GPRS
				SaveGPRSField(LoadBuffer , Tag);
				break;
			case 0x11:  // SSLCert
				SaveSSLCertField(LoadBuffer , Tag);
				break;
			case 0x12:
				SaveMerchRange(LoadBuffer , Tag);
				break;
			case 0x13:	// Merchant group
				SaveMerchGroup(LoadBuffer , Tag);
				break;
			}
		}

	}

	// enable transactions
	EnableTransactions();

   #ifdef MAKE_EMV
	if( bEMV )
	{
		tConf.bCheckSC = 1;
		tabSaveFile( FILE_CONFIG, (LPTSTR) &tConf, sizeof(TERMINALCFG) );
	}
	else
	{
		tConf.bCheckSC = 0;
		tabSaveFile( FILE_CONFIG, (LPTSTR) &tConf, sizeof(TERMINALCFG) );
	}
   #endif // MAKE_EMV

	// Pirnt table number
	if ( PrintDebug )
	{
		UI_OpenPrinter();
		UI_PrintLineFeed(5);
		UI_PrintLine(myLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
		UI_ClosePrinter();
	   #ifdef MAKE_STATISTICS //@pdm 2012-01-17
		IncStatCounter(S48_PRINT);
	   #endif //MAKE_STATISTICS
	}

	if(CurrentBatchsInfo != NULL)
		TlvTree_Release(CurrentBatchsInfo);

	return RET_OK;

//+ BATCH01 @mamata Aug 15, 2014
LBL_ERROR:

	//+ TREEMEM @mamata 5/05/2016
	if (CurrentBatchsInfo != NULL)
	{
		TlvTree_Release(CurrentBatchsInfo);
	}
	//- TREEMEM

	return RET_NOK;
//- BATCH01

}
#ifndef NOT_iPOSApp
int SetOldBatch(int contm)
{
	int i;

	for (i = 0; i < D_MAX_MERCHANT; i++ )
	{
		if(MerchsInfo[i].recCtrl.id == tMerchAux.recCtrl.id
				&& memcmp(MerchsInfo[i].TerminalId,tMerchAux.TerminalId, sizeof(tMerchAux.TerminalId)) == 0
				&& memcmp(MerchsInfo[i].AcquirerId,tMerchAux.AcquirerId, sizeof(tMerchAux.AcquirerId)) == 0
				)
			return MerchsInfo[i].batchNum;
	}
	return 1;
}

#ifdef MAKE_STATISTICS //@pdm 2012-01-20
int16 prnPrint2(uint32 handle, char * text)
{
	if (prnPrint(handle, text) != PRN_OK)
		IncStatCounter(S47_PRINT_FAIL);

	return RET_OK;
}
#endif //MAKE_STATISTICS

void  fvLoadWK(void)
{
	uint8 HexWK[16+1];
	ssaCmdLoadKey_ST New_Key;
	HOST *tAuxHost2;

	tAuxHost2 = umalloc(sizeof(HOST));
	tabOpen(HOST_TABLE);
	tabFindFirst(HOST_TABLE);

	while ( tabFindNext(HOST_TABLE, (LPTSTR) tAuxHost2 ) == RET_OK ){
		if(strlen((char *)tAuxHost2->strWKPart1) && strlen((char *)tAuxHost2->strWKPart2)){
			memset(HexWK, 0x00, sizeof(HexWK));
			memset(&New_Key, 0x00, sizeof(New_Key));
			ISO_asc_to_hex((char *)tAuxHost2->strWKPart1, (char *)HexWK, 'F', LEFT);
			ISO_asc_to_hex((char *)tAuxHost2->strWKPart2, (char *)&HexWK[8], 'F', LEFT);

			HexWK[16] = 0;

			New_Key.pu8KSN = NULL;
			New_Key.u8Type = SSACMD_WK_3DES_PIN;
			New_Key.u8Id = tAuxHost2->iKeySlot;
			New_Key.pu8Key = HexWK;
			ssaCmdDelete(SSACMD_WK_3DES_PIN, tAuxHost2->iKeySlot);
			ssaCmdLoadKey (&New_Key);
		}
	}
	tabClose(HOST_TABLE);
	ufree(tAuxHost2);

}
#endif //NOT_iPOSApp

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print string Fields
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Print_String_Fields(char *strConcat , char * STfield)
{
	int iLenField = strlen(STfield);
	int iLenConct = strlen(strConcat);

	char strAux [LZ_LINES_PRINT] = {0};

	memcpy(strAux , strConcat , iLenConct);

	if(iLenField > 0)
	{
		memcpy(&strAux[iLenConct] , STfield , iLenField);
		UI_PrintLine(strAux , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	}
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to convert from numeric64 to char array.
 *
 * [in]	 	: uint64
 * [out]	 : char *
 *
 * ------------------------------------------------------------------------*/

void i64toChar(uint64 iNumber , char *strChar64)
{
	uint64 temp_num = iNumber;
	int iNumLen     = 0;
	int iCount		= 0;
	//static char strBuffer[20] = {0};
	char * strBuffer = strChar64;

	if(iNumber >= 0)
	{
		//Converting numeric field to char array
		while(temp_num)
		{
			temp_num /= 10;
			iNumLen++;
		}

		for(iCount = 0 ; iCount < iNumLen ; iCount++)
		{
			strBuffer[(iNumLen - 1) - iCount] = '0' + (iNumber % 10);
			iNumber /= 10;
		}

		strBuffer[iCount] = '\0';
		//strAux[iCount]    = '\0';
	}
	else
		;//Return error
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print numeric Fields
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Print_Numeric_Fields(char * strConcat , uint64 STfield)
{
	int iNumLength = 0;
	int iLenConct = strlen(strConcat);
	char strBuffer [LZ_LINES_PRINT] = {0};
	char strAux    [LZ_LINES_PRINT] = {0};

	//Converting numeric field to char array
	i64toChar(STfield , (char *)strBuffer);
	//Converting numeric field to char array

	memcpy(strAux , strConcat , iLenConct);

	iNumLength = strlen(strBuffer);

	if(iNumLength > 0)
	{
		memcpy(&strAux[iLenConct] , strBuffer , iNumLength);
		UI_PrintLine(strAux , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	}
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print Terminal information
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Ingeload_Print_Terminal(void)
{
	char strLine[LZ_LINES_PRINT] = {0};

//  ============ Print terminal information ============
	memcpy(strLine , REPORT_TERMINAL , strlen(REPORT_TERMINAL));
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	memset(strLine , 0x00 , sizeof(strLine));

	//Print Header Line 1
	Print_String_Fields("Line 1: " , tTerminal.Header_Line1);

	//Print Header Line 2
	Print_String_Fields("Line 2: " , tTerminal.Header_Line2);

	//Print Header Line 3
	Print_String_Fields("Line 3: " , tTerminal.Header_Line3);

	//Print Header Line 4
	Print_String_Fields("Line 4: " , tTerminal.Header_Line4);
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print Terminal information
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Ingeload_Print_Merchant(void)
{
	int iRetFunc = TM_RET_NOFMG;
	char strLine[LZ_LINES_PRINT] = {0};
	TABLE_MERCHANT tMerchant;

	memcpy(strLine , REPORT_MERCHANT , strlen(REPORT_MERCHANT));
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	memset(strLine , 0x00 , sizeof(strLine));

	//Move to first register of merchants
	TM_FindFirst(TAB_MERCHANT , &tMerchant);
	do
	{
		//Look for Merchant group ID (Merchant Table) inside MerchGroup table (MGroup Table)
		if(TM_FindRecord(TAB_MERCH_GROUP , &tMerchGroupAux , tMerchant.MerchantGroupID) == TM_RET_OK)
		{
			//Print lines here
			Print_String_Fields("Merchant Group: " , tMerchGroupAux.Name);
			Print_String_Fields("Merchant Name : " , tMerchant.Name );
			Print_String_Fields("TID: " , tMerchant.TID);
			Print_String_Fields("MID: " , tMerchant.MID);
			UI_PrintLineFeed(1);
		}
		//Move to next register of merchants
		iRetFunc = TM_FindNext(TAB_MERCHANT , &tMerchant);
	}
	//+ NORECORDS @mamata 6/05/2016
	//d while(iRetFunc != TM_RET_NO_MORE_RECORDS);
	while(iRetFunc == TM_RET_OK);
	//- NORECORDS
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print Host information
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Ingeload_Print_Host(void)
{
	int iRetFunc = TM_RET_NOFMG;
	int iAux = 0;
	char strLine[LZ_LINES_PRINT] = {0};

	memset(&tHostAux     , 0x00 , sizeof(TABLE_HOST));

	memcpy(strLine , REPORT_HOST , strlen(REPORT_MERCHANT));
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	memset(strLine , 0x00 , sizeof(strLine));

	//Move to first register of hosts
	TM_FindFirst(TAB_HOST , &tHostAux);
	do
	{
		Print_String_Fields("Name: " , tHostAux.Name);
		iAux = tHostAux.CommunicationType;

		(iAux == 1) ? memcpy(strLine , "DIAL" , strlen("DIAL"))
	  : (iAux == 2) ? memcpy(strLine , "ETH" , strlen("ETH"))
	  : memcpy(strLine , "GPRS" , strlen("GPRS"));

		Print_String_Fields("Comm Type: " , strLine);
		Print_String_Fields("NII : " , tHostAux.NII);

		iRetFunc =TM_FindNext(TAB_HOST , &tHostAux);
	}
	//+ NORECORDS @mamata 6/05/2016
	//d while(iRetFunc != TM_RET_NO_MORE_RECORDS);
	while(iRetFunc == TM_RET_OK);
	//- NORECORDS
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print Range information
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Ingeload_Print_Range(void)
{
	int iRetFunc = TM_RET_NOFMG;
	int iAux = 0;
	char strLine[LZ_LINES_PRINT] = {0};
	char strMin[20] = {0};
	char strMax[20] = {0};

	memset(&tRangeInfoAux  , 0x00 , sizeof(TABLE_RANGE));

	memcpy(strLine , REPORT_RANGE , strlen(REPORT_RANGE));
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	memset(strLine , 0x00 , sizeof(strLine));

	//Print header for ranges report
	sprintf(strLine , "%-3s%-15s%-11s%-11s" , "ID" , "Name" , "Min" , "Max");
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);

	//Move to first register of ranges
	TM_FindFirst(TAB_RANGE , &tRangeInfoAux);
	iAux = 1;
	do
	{
		i64toChar(tRangeInfoAux.Bin_Min , strMin);
		i64toChar(tRangeInfoAux.Bin_Max , strMax);
		sprintf(strLine , "%-3d%-15s%-11s%-11s" , iAux , tRangeInfoAux.Name , strMin , strMax);
		iRetFunc =TM_FindNext(TAB_RANGE , &tRangeInfoAux);
		iAux++;
		Print_String_Fields("" , strLine);
	}
	//+ NORECORDS @mamata 6/05/2016
	//d while(iRetFunc != TM_RET_NO_MORE_RECORDS);
	while(iRetFunc == TM_RET_OK);
	//- NORECORDS
	UI_PrintLineFeed(1);
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print IP information
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Ingeload_Print_IP(void)
{
	int iRetFunc = TM_RET_NOFMG;
	char strLine[LZ_LINES_PRINT] = {0};

	memset(&tIPConfAux	 , 0x00 , sizeof(TABLE_IP));

	memset(strLine , 0x00 , sizeof(strLine));
	memcpy(strLine , REPORT_IP , strlen(REPORT_IP));
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);

	//Move to first register of ranges
	TM_FindFirst(TAB_IP , &tIPConfAux);
	do
	{
		Print_String_Fields("IP   : " , tIPConfAux.Host);
		Print_String_Fields("Port : " , tIPConfAux.Port);

		iRetFunc =TM_FindNext(TAB_IP , &tIPConfAux);
		UI_PrintLineFeed(1);
	}
	//+ NORECORDS @mamata 6/05/2016
	//d while(iRetFunc != TM_RET_NO_MORE_RECORDS);
	while(iRetFunc == TM_RET_OK);
	//- NORECORDS
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print Merchant Range information
 *
 * IMPORTANT :This function should be used only after the print handler
 * is already open.
 * ------------------------------------------------------------------------*/
void Ingeload_Print_MerchantRange(void)
{
	int iRetFunc = TM_RET_NOFMG;
	char strLine[LZ_LINES_PRINT] = {0};
	char strAux[20] = {0};

	memset(&tMerchAux 	 , 0x00 , sizeof(TABLE_MERCHANT));
	memset(&tMerchRangeAux , 0x00 , sizeof(TABLE_MERCH_RANGE));

	memcpy(strLine , REPORT_MRANGE , strlen(REPORT_MRANGE));
	UI_PrintLine(strLine , UI_FONT_NORMAL , UI_ALIGN_LEFT);
	memset(strLine , 0x00 , sizeof(strLine));

	//Retrieve number of merchants
	//TM_GetTableHeader(TAB_MERCHANT , &tHeaderMerch);

	//Move to first register of merchant ranges
	TM_FindFirst(TAB_MERCH_RANGE , &tMerchRangeAux);
	do
	{
		//Look for merchant ID (MERCHANT RANGE TABLE) inside MERCHANT TABLE
		if(TM_FindRecord(TAB_MERCHANT , &tMerchAux , tMerchRangeAux.MerchantID) == TM_RET_OK)
		{
			//Print lines here
			//Print_String_Fields("Merchant Name : " , tMerchAux.Name );
			sprintf(strAux , "%s" , tMerchAux.Name);
		}
		//Move to next register of merchants

		//Look for Range ID (MERCHANT RANGE TABLE) inside RANGE TABLE
		if(TM_FindRecord(TAB_RANGE , &tRangeInfoAux , tMerchRangeAux.RangeID) == TM_RET_OK)
		{
			//Print lines here
			//Print_String_Fields("Range Name : " , tRangeInfoAux.Name );
			sprintf(strLine , "%-16s - %-16s" , strAux , tRangeInfoAux.Name);
			Print_String_Fields("" , strLine );
		}
		//Move to next register of merchants

		iRetFunc = TM_FindNext(TAB_MERCH_RANGE , &tMerchRangeAux);
	}
	//+ NORECORDS @mamata 6/05/2016
	//d while(iRetFunc != TM_RET_NO_MORE_RECORDS);
	while(iRetFunc == TM_RET_OK);
	//- NORECORDS
}

/*--------------------------------------------------------------------------
 * @DPP 20140522 Add Function to print Ingeload user report information
 * ------------------------------------------------------------------------*/
void IngeLoad_PrintReport(void)
{
	//Open printer handler
	UI_OpenPrinter();

//  ============ Print terminal information ============
	Ingeload_Print_Terminal();
//  ============ Print Merchant information ============
	Ingeload_Print_Merchant();
//  ============ Print Host information ============
	Ingeload_Print_Host();
//  ============ Print Range information ============
	Ingeload_Print_Range();
//  ============ Print IP information ============
	Ingeload_Print_IP();
//  ============ Print MERCHANT - RANGE information ============
	Ingeload_Print_MerchantRange();

	//Feed lines
	UI_PrintLineFeed(5);

	//Close printer handler
	UI_ClosePrinter();

}
