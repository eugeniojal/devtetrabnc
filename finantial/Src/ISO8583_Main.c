/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			ISO8583_Main.c
 * Header file:		ISO8583_Main.h
 * Description:		Implement ISO 8583 for XXXXXX Host
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/

/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
#define HOST_TEST 1
#define L_Buffer 1024
#define L_msgType		4
#define LZ_msgType		L_msgType+1
#define L_prcCode		6
#define LZ_prcCode		L_prcCode+1

#define SETTLE_MAX_TRIES 3
#define MAXDESLEN 16+1
#define CVV2_CVC2_SIZE 3
#define SEPARADOR_CAMPO_95 'C'
#define ZERO_FILLED_FIELD_95_LEN   8

/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/
void WriteLog(byte* pMessage, int size);
int HostMain_PackMessage(TLV_TREE_NODE Tree);
int HostMain_UnpackMessage(TLV_TREE_NODE Tree);
int HostMain_Send_Request(TLV_TREE_NODE Tree);
int HostMain_Receive_Response(TLV_TREE_NODE Tree);
int HostMain_GetResponseCodeInfo(uint8 *HostResponseCode, RESPONSE_CODES *ResponseCode);
int HostMain_GetResponseCodeInfoC2P(uint8 *HostResponseCode, RESPONSE_CODES *ResponseCode);
int HostMain_SendAdvice(uint8 *BatchName, bool Settle);
//+ ECHOTEST @mamata Aug 15, 2014
int HostMain_ValidateResponse(bool EchoTest);
//d int HostMain_ValidateResponse(void);
//- ECHOTEST
int SaveKeyInFile(void);
int HostMain_BatchUpload(TABLE_MERCHANT *Merchant);
int SetTPDU(TLV_TREE_NODE Tree);
int SetBit2(TLV_TREE_NODE Tree);
int SetBit3(TLV_TREE_NODE Tree);
int SetBit4(TLV_TREE_NODE Tree);
int SetBit11(TLV_TREE_NODE Tree);
int SetBit12(TLV_TREE_NODE Tree);
int SetBit13(TLV_TREE_NODE Tree);
int SetBit14(TLV_TREE_NODE Tree);
int SetBit22(TLV_TREE_NODE Tree);
int SetBit23(TLV_TREE_NODE Tree);
int SetBit24(TLV_TREE_NODE Tree);
int SetBit25(TLV_TREE_NODE Tree);
int SetBit35(TLV_TREE_NODE Tree);
int SetBit37(TLV_TREE_NODE Tree);
int SetBit38(TLV_TREE_NODE Tree);
int SetBit39(TLV_TREE_NODE Tree);
int SetBit41(TLV_TREE_NODE Tree);
int SetBit42(TLV_TREE_NODE Tree);
int SetBit45(TLV_TREE_NODE Tree);
int SetBit48(TLV_TREE_NODE Tree);
int SetBit52(TLV_TREE_NODE Tree);
int SetBit54(TLV_TREE_NODE Tree);
int SetBit55(TLV_TREE_NODE Tree);
int SetBit62(TLV_TREE_NODE Tree);
int SetBit60_Settle(TLV_TREE_NODE Tree);
int SetBit63_Settle(TLV_TREE_NODE Tree);
int BuildEMVTags(TLV_TREE_NODE pTLVT_Input,int bit127index);
int GetEMVTagsFromHost(TLV_TREE_NODE Tree);
//+ ISOMAIN01 @mamata Aug 14, 2014
int HostMain_UnpackBit63(TLV_TREE_NODE Tree);
int SetBit60_Upload(TLV_TREE_NODE Tree);
int SetBit63(TLV_TREE_NODE Tree);
int SetBit63_S39_TAX(TLV_TREE_NODE Tree, int *Bit63Index);
int HostMain_UpdateDateTime(TLV_TREE_NODE Tree);
int SetBit60_Adjust(TLV_TREE_NODE Tree);
//- ISOMAIN01

//int Field127(TLV_TREE_NODE *Tree, char* bufferTx, int *nSizeBuffer );
int SetBit70(TLV_TREE_NODE Tree);
int SetBit123(TLV_TREE_NODE Tree);
int SetBit7(TLV_TREE_NODE Tree);
int SetBit23(TLV_TREE_NODE Tree);
int SetBit49(TLV_TREE_NODE Tree);
int SetBit127(TLV_TREE_NODE Tree);
int SetBit90(TLV_TREE_NODE Tree);
int SetBit75(TLV_TREE_NODE Tree);
int SetBit76(TLV_TREE_NODE Tree);
int SetBit87(TLV_TREE_NODE Tree);
int SetBit88(TLV_TREE_NODE Tree);
int SetBit95(TLV_TREE_NODE Tree);
int SetBit97(TLV_TREE_NODE Tree);
int SetBit122(TLV_TREE_NODE Tree);

int ConfigureTLSProfile(const char *chSSLCertName);
bool CheckPEMExistence(const char *SSLCert);
/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
stIsoFields	tMsgTx;
stIsoFields	tMsgRx;

static uint8 TXBuffer[L_Buffer];
static int TXBufferLen;
static uint8 RXBuffer[L_Buffer];
static int RXBufferLen;


typedef int (*pCallSetBit)(TLV_TREE_NODE);
typedef struct tagBITLIST {
	int			TransacId;					// Transaction identification
	char		msgType[LZ_msgType];		// Message type
	char		prcCode[LZ_prcCode];		// Processing codejjj
	pCallSetBit	*pFuncs;					// Function to assign value
} BITLIST, *LPBITLIST;

static TP_TABISO8583 stTabISO8583_adjust[] =
{
	// bit		| data	| Len	| data	|		|										|
	// num		| type	| type	|Lenght	|Fill	|Tx Data								| Rx Data
	{ B_TPDU	, NPR	, FL	, 10	, 'F'	, (uint8 *) &tMsgTx.stTPDU			    , (uint8 *) &tMsgRx.stTPDU			},
	{ B_MSGID	, ANR	, FL	, 4		, 'F'	, tMsgTx.vbMsgType					    , tMsgRx.vbMsgType					},
	{ B_BMP		, BINR	, FL	, 8		, '0'	, tMsgTx.aBitmap						, tMsgRx.aBitmap					},
	{ 2			, ANL	, AA	, 19	, 'F'	, tMsgTx.aPAN		    				, tMsgRx.aPAN						},
	{ 3			, ANR	, FL	, 6		, '0'	, tMsgTx.vbPCode						, tMsgRx.vbPCode					},
	{ 4			, ANR	, FL	, 12	, '0'	, tMsgTx.aValor							, tMsgRx.aValor						},
	{ 7			, ANR	, FL	, 10	, '0'	, tMsgTx.aDtTm							, tMsgRx.aDtTm						},
	{ 11		, ANR	, FL	, 6		, '0'	, tMsgTx.aNSU			    			, tMsgRx.aNSU						},
	{ 12		, ANR	, FL	, 6		, '0'	, tMsgTx.aTmLocal						, tMsgRx.aTmLocal					},
	{ 13		, ANR	, FL	, 4		, '0'	, tMsgTx.aDtLocal						, tMsgRx.aDtLocal					},
	{ 14		, ANR	, FL	, 4		, '0'	, tMsgTx.aDtExp							, tMsgRx.aDtExp						},
	{ 15		, ANR	, FL	, 4		, '0'	, tMsgTx.aDtSett						, tMsgRx.aDtSett					},
//	{ 22		, NPR	, FL	, 3  	, '0'	, tMsgTx.aEntryMode22					, tMsgRx.aEntryMode22				},
	{ 23		, ANR	, FL	, 3 	, '0'	, tMsgTx.cardSecuence				    , tMsgRx.cardSecuence				},
	{ 24		, NPR	, FL	, 3		, '0'	, tMsgTx.aNII				    		, tMsgRx.aNII						},
	{ 25		, NPR	, FL	, 2		, '0'	, tMsgTx.aPOSCond				    	, tMsgRx.aPOSCond					},
	{ 35		, ANR	, AA	, 37	, 'F'	, tMsgTx.aTrack2						, tMsgRx.aTrack2					},
	{ 36		, ANR	, LL	, 76	, '0'	, tMsgTx.aTrack3						, tMsgRx.aTrack3					},
	{ 37		, ANR	, FL	, 12	, '0'	, tMsgTx.aRRN						    , tMsgRx.aRRN						},
	{ 38		, ANR	, FL	, 6		, '0'	, tMsgTx.aAuthCode						, tMsgRx.aAuthCode					},
	{ 39		, ANR	, FL	, 2		, '0'	, tMsgTx.aRespCode					    , tMsgRx.aRespCode					},
	{ 41		, ANR	, FL	, 8		, '0'	, tMsgTx.aTermId						, tMsgRx.aTermId					},
	{ 42		, ANR	, FL	, 15	, '0'	, tMsgTx.aMerchId						, tMsgRx.aMerchId					},
	{ 44		, ANR	, FL	, 1		, '0'	, tMsgTx.aAddRespData					, tMsgRx.aAddRespData				},
	{ 45		, ANR	, AA	, 76	, '0'	, tMsgTx.vbTrack1						, tMsgRx.vbTrack1					},
	{ 48		, BINL	, AAA	, 13	, '0'	, (uint8 *) &tMsgTx.u16AddData_Len		, (uint8 *) &tMsgRx.u16AddData_Len	},
	{ 49		, ANR	, FL	, 3		, '0'	, tMsgTx.aCurrCode						, tMsgRx.aCurrCode					},
	{ 52		, BINL	, FL	, 8		, '0'	, (uint8 *) &tMsgTx.u16PinBlock_Len		, (uint8 *) &tMsgRx.u16PinBlock_Len	},
	{ 53		, ANR	, FL	, 16	, '0'	, (uint8 *) &tMsgTx.campo53rx   	    , (uint8 *) &tMsgRx.campo53rx    	},//cambio
	{ 54		, ANR	, AA	, 60	, '0'	, (uint8 *) &tMsgTx.u16AddAmt_Len		, (uint8 *) &tMsgRx.u16AddAmt_Len	},
	{ 55		, BINL	, AAAAAA, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit55_Len		, (uint8 *) &tMsgRx.u16Bit55_Len	},
	{ 60		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit60_Len		, (uint8 *) &tMsgRx.u16Bit60_Len	},
	{ 61		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit61_Len		, (uint8 *) &tMsgRx.u16Bit61_Len	},
	{ 62		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit62_Len		, (uint8 *) &tMsgRx.u16Bit62_Len	},
	{ 63		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit63_Len		, (uint8 *) &tMsgRx.u16Bit63_Len	},
	{ 64		, BINL	, FL	, 8		, '0'	, (uint8 *) &tMsgTx.u16Bit64_Len		, (uint8 *) &tMsgRx.u16Bit64_Len	},
	{ 70		, ANL	, FL	, 3		, '0'	, (uint8 *) &tMsgTx.msgid				, (uint8 *) &tMsgRx.msgid		    },
	{ 73		, BINL	, LLL	, 32	, '0'	, (uint8 *) &tMsgTx.u16Bit73_Len		, (uint8 *) &tMsgRx.u16Bit73_Len	},
	{ 75		, ANR	, FL	, 10	, '0'	, tMsgTx.nreversos						, tMsgRx.nreversos					},
	{ 76		, ANR	, FL	, 10	, '0'	, tMsgTx.nventas	 					, tMsgRx.nventas					},
	{ 87		, ANR	, FL	, 16	, '0'	, tMsgTx.mreversos						, tMsgRx.mreversos					},
	{ 88		, ANR	, FL	, 16	, '0'	, tMsgTx.mventas						, tMsgRx.mventas					},
	{ 90		, ANR	, FL	, 42	, '0'	, tMsgTx.Bit90	                     	, tMsgRx.Bit90	                    },//eugenio
	{ 95		, ANR	, FL	, 42	, '0'	, tMsgTx.Bit95	                     	, tMsgRx.Bit95	                    },//JROJAS
	{ 97		, ANR	, FL	, 17	, '0'	, tMsgTx.mtotall	                    , tMsgRx.mtotall	                },//eugenio
	{ 122		, ANR	, AAA	, 8		, '0'	, tMsgTx.tokenBLock						, tMsgRx.tokenBLock					},//jr
	{ 123		, ANR	, AAA	, 15	, '0'	, tMsgTx.aEntryMode						, tMsgRx.aEntryMode					},//eugenio
	{ 127		, BINL	, AAAAAA, 350	, '0'	,(uint8 *)&tMsgTx.btField127Responselen	,(uint8 *)&tMsgRx.btField127Responselen	},
	{ NULL		, ND	, NULL	, NULL	, NULL	, NULL									, NULL								},
};

static TP_TABISO8583 stTabISO8583[] =
{
	// bit		| data	| Len	| data	|		|										|
	// num		| type	| type	|Lenght	|Fill	|Tx Data								| Rx Data
	{ B_TPDU	, NPR	, FL	, 10	, 'F'	, (uint8 *) &tMsgTx.stTPDU			    , (uint8 *) &tMsgRx.stTPDU			},
	{ B_MSGID	, ANR	, FL	, 4		, 'F'	, tMsgTx.vbMsgType					    , tMsgRx.vbMsgType					},
	{ B_BMP		, BINR	, FL	, 8		, '0'	, tMsgTx.aBitmap						, tMsgRx.aBitmap					},
	{ 2			, ANL	, AA	, 19	, 'F'	, tMsgTx.aPAN		    				, tMsgRx.aPAN						},
	{ 3			, ANR	, FL	, 6		, '0'	, tMsgTx.vbPCode						, tMsgRx.vbPCode					},
	{ 4			, ANR	, FL	, 12	, '0'	, tMsgTx.aValor							, tMsgRx.aValor						},
	{ 7			, ANR	, FL	, 10	, '0'	, tMsgTx.aDtTm							, tMsgRx.aDtTm						},
	{ 11		, ANR	, FL	, 6		, '0'	, tMsgTx.aNSU			    			, tMsgRx.aNSU						},
	{ 12		, ANR	, FL	, 6		, '0'	, tMsgTx.aTmLocal						, tMsgRx.aTmLocal					},
	{ 13		, ANR	, FL	, 4		, '0'	, tMsgTx.aDtLocal						, tMsgRx.aDtLocal					},
	{ 14		, ANR	, FL	, 4		, '0'	, tMsgTx.aDtExp							, tMsgRx.aDtExp						},
	{ 15		, ANR	, FL	, 4		, '0'	, tMsgTx.aDtSett						, tMsgRx.aDtSett					},
//	{ 22		, NPR	, FL	, 3  	, '0'	, tMsgTx.aEntryMode22					, tMsgRx.aEntryMode22				},
	{ 23		, ANR	, FL	, 3 	, '0'	, tMsgTx.cardSecuence				    , tMsgRx.cardSecuence				},
	{ 24		, NPR	, FL	, 3		, '0'	, tMsgTx.aNII				    		, tMsgRx.aNII						},
	{ 25		, NPR	, FL	, 2		, '0'	, tMsgTx.aPOSCond				    	, tMsgRx.aPOSCond					},
	{ 35		, ANR	, AA	, 37	, 'F'	, tMsgTx.aTrack2						, tMsgRx.aTrack2					},
	{ 36		, ANR	, LL	, 76	, '0'	, tMsgTx.aTrack3						, tMsgRx.aTrack3					},
	{ 37		, ANR	, FL	, 12	, '0'	, tMsgTx.aRRN						    , tMsgRx.aRRN						},
	{ 38		, ANR	, FL	, 6		, '0'	, tMsgTx.aAuthCode						, tMsgRx.aAuthCode					},
	{ 39		, ANR	, FL	, 2		, '0'	, tMsgTx.aRespCode					    , tMsgRx.aRespCode					},
	{ 41		, ANR	, FL	, 8		, '0'	, tMsgTx.aTermId						, tMsgRx.aTermId					},
	{ 42		, ANR	, FL	, 15	, '0'	, tMsgTx.aMerchId						, tMsgRx.aMerchId					},
	{ 44		, ANR	, FL	, 1		, '0'	, tMsgTx.aAddRespData					, tMsgRx.aAddRespData				},
	{ 45		, ANR	, AA	, 76	, '0'	, tMsgTx.vbTrack1						, tMsgRx.vbTrack1					},
	{ 48		, BINL	, AAA	, 13	, '0'	, (uint8 *) &tMsgTx.u16AddData_Len		, (uint8 *) &tMsgRx.u16AddData_Len	},
	{ 49		, ANR	, FL	, 3		, '0'	, tMsgTx.aCurrCode						, tMsgRx.aCurrCode					},
	{ 52		, BINL	, FL	, 8		, '0'	, (uint8 *) &tMsgTx.u16PinBlock_Len		, (uint8 *) &tMsgRx.u16PinBlock_Len	},
	{ 53		, ANR	, FL	, 16	, '0'	, (uint8 *) &tMsgTx.campo53rx   	    , (uint8 *) &tMsgRx.campo53rx    	},//cambio
	{ 54		, ANR	, AA	, 60	, '0'	, (uint8 *) &tMsgTx.u16AddAmt_Len		, (uint8 *) &tMsgRx.u16AddAmt_Len	},
	{ 55		, BINL	, AAAAAA, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit55_Len		, (uint8 *) &tMsgRx.u16Bit55_Len	},
	{ 60		, BINL	, LLLN	, 12    , '0'	, (uint8 *) &tMsgTx.u16Bit60_Len		, (uint8 *) &tMsgRx.u16Bit60_Len	},
	{ 61		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit61_Len		, (uint8 *) &tMsgRx.u16Bit61_Len	},
	{ 62		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit62_Len		, (uint8 *) &tMsgRx.u16Bit62_Len	},
	{ 63		, BINL	, LLL	, 512	, '0'	, (uint8 *) &tMsgTx.u16Bit63_Len		, (uint8 *) &tMsgRx.u16Bit63_Len	},
	{ 64		, BINL	, FL	, 8		, '0'	, (uint8 *) &tMsgTx.u16Bit64_Len		, (uint8 *) &tMsgRx.u16Bit64_Len	},
	{ 70		, ANL	, FL	, 3		, '0'	, (uint8 *) &tMsgTx.msgid				, (uint8 *) &tMsgRx.msgid		    },
	{ 73		, BINL	, LLL	, 32	, '0'	, (uint8 *) &tMsgTx.u16Bit73_Len		, (uint8 *) &tMsgRx.u16Bit73_Len	},
	{ 75		, ANR	, FL	, 10	, '0'	, tMsgTx.nreversos						, tMsgRx.nreversos					},
	{ 76		, ANR	, FL	, 10	, '0'	, tMsgTx.nventas	 					, tMsgRx.nventas					},
	{ 87		, ANR	, FL	, 16	, '0'	, tMsgTx.mreversos						, tMsgRx.mreversos					},
	{ 88		, ANR	, FL	, 16	, '0'	, tMsgTx.mventas						, tMsgRx.mventas					},
	{ 90		, ANR	, FL	, 42	, '0'	, tMsgTx.Bit90	                     	, tMsgRx.Bit90	                    },//eugenio
	{ 95		, ANR	, FL	, 42	, '0'	, tMsgTx.Bit95	                     	, tMsgRx.Bit95	                    },//JROJAS
	{ 97		, ANR	, FL	, 17	, '0'	, tMsgTx.mtotall	                    , tMsgRx.mtotall	                },//eugenio
	{ 122		, ANR	, AAA	, 8		, '0'	, tMsgTx.tokenBLock						, tMsgRx.tokenBLock					},//jr
	{ 123		, ANR	, AAA	, 15	, '0'	, tMsgTx.aEntryMode						, tMsgRx.aEntryMode					},//eugenio
	{ 127		, BINL	, AAAAAA, 350	, '0'	,(uint8 *)&tMsgTx.btField127Responselen	,(uint8 *)&tMsgRx.btField127Responselen	},
	{ NULL		, ND	, NULL	, NULL	, NULL	, NULL									, NULL								},
};
//pCallSetBit	pSale[]		= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit11, fSetBit12, SetBit13, SetBit14, SetBit22, SetBit24, SetBit25, SetBit35, SetBit41, SetBit42, SetBit45, SetBit48, SetBit52, SetBit54, SetBit55, SetBit62, /*ISOMAIN01*/SetBit63, NULL };
pCallSetBit	pSale[] 	= { SetTPDU,SetBit2,SetBit3, SetBit4, SetBit7,  SetBit11, SetBit12, SetBit13,SetBit14,SetBit23, SetBit35, SetBit41, SetBit42, SetBit48, SetBit49,SetBit52,SetBit123,SetBit127, NULL };

pCallSetBit	pReverse[]	= { SetTPDU, SetBit2, SetBit3, SetBit4,SetBit7, SetBit11, SetBit12, SetBit13, SetBit14, SetBit23, SetBit35, SetBit41, SetBit42, SetBit45, SetBit48, SetBit49, SetBit90, SetBit123, SetBit127, NULL };
pCallSetBit	pRefund[]	= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit11, SetBit12, SetBit13, SetBit14, SetBit123, SetBit24, SetBit25, SetBit35, SetBit38, SetBit41, SetBit42, SetBit45, SetBit48, SetBit52, SetBit54, SetBit55, SetBit62, NULL };
pCallSetBit	pVoid[]		= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit7, SetBit11, SetBit12, SetBit13,SetBit23, SetBit35, SetBit37, SetBit38, SetBit41, SetBit42, SetBit48, SetBit49,SetBit90, SetBit123, SetBit127, NULL };
pCallSetBit	pAdjust[]	= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit7, SetBit11, SetBit12, SetBit13, SetBit14, SetBit23,  SetBit35, SetBit39, SetBit41, SetBit42, SetBit48,SetBit49, SetBit90, SetBit95,SetBit123,SetBit127, NULL };
//pCallSetBit	pAdjust[]	= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit11, SetBit12, SetBit13, SetBit14, SetBit123, SetBit24, SetBit25, SetBit38, SetBit41, SetBit42, SetBit48, SetBit54, /*ISOMAIN01*/SetBit60_Adjust, NULL };

//+ ECHOTEST @mamata Aug 15, 2014
pCallSetBit	pTest[]		= { SetTPDU, SetBit7, SetBit11, SetBit12, SetBit13, SetBit41, SetBit42, SetBit70,SetBit127, NULL };
pCallSetBit	plogon[]    = { SetTPDU, SetBit7, SetBit11, SetBit12, SetBit13, SetBit41, SetBit42, SetBit70,SetBit127, NULL };
//- ECHOTEST
pCallSetBit pSettle[]	= { SetTPDU, SetBit7, SetBit11, SetBit12, SetBit13, SetBit41, SetBit42,SetBit75,SetBit76,SetBit87,SetBit88,SetBit97,SetBit127, NULL };
pCallSetBit	pUpload[]	= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit11, SetBit12, SetBit13, SetBit14, SetBit123, SetBit24, SetBit25, SetBit37, SetBit38, SetBit41, SetBit42, SetBit48, SetBit54, SetBit55, /*ISOMAIN01*/SetBit60_Upload, NULL };
pCallSetBit	pOffline[]	= { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit11, SetBit12, SetBit13, SetBit14, SetBit123, SetBit24, SetBit25, SetBit38, SetBit41, SetBit42, SetBit48, SetBit54, SetBit55, NULL };

pCallSetBit	pC2p[] 	=  { SetTPDU, SetBit2, SetBit3, SetBit4, SetBit7,  SetBit11, SetBit12, SetBit13, SetBit23, SetBit35, SetBit41, SetBit42, SetBit48, SetBit49,SetBit122,SetBit123,SetBit127, NULL };


static BITLIST	lista[] = { { TRAN_SALE, 			"0200", "00aa0x", pSale },
							{ TRAN_REFUND, 			"0420", "00aa0x", pVoid },
							{ TRAN_REVERSE,	 		"0420", "00aa0x", pReverse },
							{ TRAN_ADJUST,			"0220", "00aa0x", pAdjust },
							{ TRAN_VOID, 		    "0420", "00aa0x", pVoid  },
							//+ ECHOTEST @mamata Aug 15, 2014
							{ TRAN_TEST,			"0800",	"99aa0x", pTest },
							{ TRAN_LOGON,			"0800",	"99aa0x", plogon },
							//- ECHOTEST
							{ TRAN_SETTLE,			"0520", "920000", pSettle },
							{ TRAN_UPLOAD,			"0320", "00000x", pUpload },
							{ TRAN_OFFLINE,			"0220", "00000x", pOffline },
							//+ CLESS @mamata Dec 11, 2014
							{ TRAN_QUICK_SALE,		"0200", "00aa0x", pSale },
							//- CLESS
							//C2P
							{ TRAN_C2P,				"0200", "56aa0x", pC2p },
};


static RESPONSE_CODES HostMain_ResponseCodes[] =
{
	// Response		Approved	Delete		Save to		MessageID
	// Code						REVERSE		Batch
	{ "00", 		TRUE,		TRUE, 		TRUE,		MSG_RC_APPROVED },
	{ "01", 		FALSE,		TRUE,		FALSE,		MSG_RC_PLEASE_CALL },
	{ "02", 		FALSE,		TRUE, 		FALSE,		MSG_RC_PLEASE_CALL },
	{ "03", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_MERCHANT },
	{ "04", 		FALSE,		TRUE, 		FALSE,		MSG_RC_HOLD_CARD },
	{ "05", 		FALSE,		TRUE, 		FALSE,		MSG_RC_DECLINED },
	{ "06", 		FALSE,		TRUE, 		FALSE,		MSG_RC_NOTMATCH },
	{ "08", 		FALSE,		TRUE, 		FALSE,		MSG_RC_EMISOR_NOT },
	{ "12", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_TRANSACTION },
	{ "13", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_AMOUNT },
	{ "14", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_CARD },
	{ "19", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_TRANSACTION },
	{ "22", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_AMOUNT },
	{ "30", 		FALSE,		TRUE, 		FALSE,		MSG_INVALID_CARD },
	{ "41", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_CALL },
	{ "43", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_CALL },
	{ "48", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_IMPOSIBLE },
	{ "51", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_SALDO_INSUFI },
	{ "54", 		FALSE,		TRUE, 		FALSE,		MSG_RC_EXPIRED_CARD },
	{ "55", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVAL_PASSW },
	{ "56", 		FALSE,		TRUE, 		FALSE,		MSG_INVALID_CARD },
	{ "57", 		FALSE,		TRUE, 		FALSE,		MSG_RC_TRANSACTION_NOT_ALLOWED },
	{ "58", 		FALSE,		TRUE, 		FALSE,		MSG_RC_TRANSACTION_NOT_ALLOWED },
	{ "61", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_EXE_LIMIT_DIA },
	{ "62", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_EXE_RESTIN_CARD },
	{ "63", 		FALSE,		TRUE, 		FALSE,		MSG_SSL_ERROR_CEDULA },
	{ "65", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_EXE_LIMIT_DIA },
	{ "67", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_CALL },
	{ "68", 		FALSE,		TRUE, 		FALSE,		MSG_RC_EMISOR_NOT },
	{ "76", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVALID_ACOUNT },
	{ "78", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVALID_ACOUNT },
	{ "74", 		FALSE,		TRUE, 		FALSE,		MSG_RC_DECLINED },
	{ "87", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVALID_KEY },
	{ "88", 		FALSE,		TRUE, 		FALSE,		MSG_OTRO_VENDEDOR },
	{ "89", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_TERMINAL },
	{ "91", 		FALSE,		TRUE, 		FALSE,		MSG_RC_NO_ISSUER },
	{ "94", 		FALSE,		TRUE, 		FALSE,		MSG_RC_DUPLICATE_TRANSMITION },
	{ "95", 		FALSE,		TRUE, 		FALSE,		MSG_RC_BATCH_UPLOAD },
	{ "96", 		FALSE,		FALSE, 		FALSE,		MSG_RC_SYSTEM_FAILURE },
	{ "AA", 		FALSE,		FALSE,		FALSE,		MSG_RC_DECLINED },
	{ "CE", 		FALSE,		FALSE,		FALSE,		MSG_RC_COMMUNICATION_ERROR },
	{ "Y1", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Y1 },
	{ "Y3", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Y3 },
	{ "Z1", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Z1 },
	{ "Z3", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Z3 },
	{ "TO", 		FALSE,		FALSE,		FALSE,		MSG_RC_TIME_OUT },
	{ "PA", 		FALSE,		TRUE,		FALSE,		MSG_RC_PACK_ERROR },
	{ "UP", 		FALSE,		FALSE,		FALSE,		MSG_RC_UNPACK_ERROR },
	// EMV RC
	{ "D1", 		FALSE,		FALSE,		FALSE,		MSG_RC_EMV_DECLINED },
	{ "AF", 		TRUE,		FALSE,		TRUE,		MSG_RC_APPROVED },
	//+ ISOMAIN01 @mamata Aug 14, 2014
	{ "D2", 		FALSE,		FALSE,		FALSE,		MSG_RC_EMV_DECLINED },
	//- ISOMAIN01
	//+ ECHOTEST O LOGON @mamata Aug 15, 2014
	{ "ET",			TRUE,		FALSE,		FALSE,		MSG_RC_APPROVED },
	//EUGENIO
	{ "TC",			TRUE,		FALSE,		FALSE,		MSG_RC_APPROVED },
	//- ECHOTEST

	//+ EMV07 @mamata Mar 10, 2015
	{ "#C",			FALSE,		FALSE,		FALSE,		MSG_RC_EMV_CANCELED },
	{ "#E",			FALSE,		FALSE,		FALSE,		MSG_RC_EMV_CARD_ERROR },
	{ "#P",			FALSE,		FALSE,		FALSE,		MSG_RC_EMV_PROCESSING_ERROR },
	//- EMV07
	// end of list
	{ "FF", 		FALSE,		FALSE,		FALSE,		NULL },
};
/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/


static RESPONSE_CODES HostMain_ResponseCodesC2P[] =
{
	// Response		Approved	Delete		Save to		MessageID
	// Code						REVERSE		Batch
	{ "00", 		TRUE,		TRUE, 		TRUE,		MSG_RC_APPROVED },
	{ "01", 		FALSE,		TRUE,		FALSE,		MSG_RC_PLEASE_CALL },
	{ "02", 		FALSE,		TRUE, 		FALSE,		MSG_RC_PLEASE_CALL },
	{ "03", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_MERCHANT },
	{ "04", 		FALSE,		TRUE, 		FALSE,		MSG_RC_HOLD_CARD },
	{ "05", 		FALSE,		TRUE, 		FALSE,		MSG_ERROR_NO_APLICA_COMISION },
	{ "08", 		FALSE,		TRUE, 		FALSE,		MSG_RC_EMISOR_NOT },
	{ "12", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_TRANSACTION },
	{ "13", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_AMOUNT },
	{ "14", 		FALSE,		TRUE, 		FALSE,		MSG_ERROR_CLIENTE_AFILIADO },
	{ "30", 		FALSE,		TRUE, 		FALSE,		MSG_SSL_ERROR_FORMATO },
	{ "41", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_CALL },
	{ "43", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_CALL },
	{ "51", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_SALDO_INSUFI },
	{ "54", 		FALSE,		TRUE, 		FALSE,		MSG_RC_EXPIRED_CARD },
	{ "55", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVAL_TOKEN },
	{ "56", 		FALSE,		TRUE, 		FALSE,		MSG_ERROR_MOVIL_ERRADO },
	{ "57", 		FALSE,		TRUE, 		FALSE,		MSG_RC_TRANSACTION_NOT_ALLOWED },
	{ "58", 		FALSE,		TRUE, 		FALSE,		MSG_RC_TRANSACTION_NOT_ALLOWED },
	{ "61", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_EXE_LIMIT_DIA },
	{ "62", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_EXE_RESTIN_CARD },
	{ "63", 		FALSE,		TRUE, 		FALSE,		MSG_SSL_ERROR_CEDULA },
	{ "65", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_EXE_LIMIT_DIA },
	{ "67", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_CALL },
	{ "68", 		FALSE,		TRUE, 		FALSE,		MSG_RC_EMISOR_NOT },
	{ "76", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVALID_ACOUNT },
	{ "78", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVALID_ACOUNT },
	{ "74", 		FALSE,		TRUE, 		FALSE,		MSG_RC_DECLINED },
	{ "80", 		FALSE,		TRUE, 		FALSE,		MSG_SSL_ERROR_CEDULA },
	{ "87", 		FALSE,		TRUE, 		FALSE,		MSG_RC_RETRY_INVALID_KEY },
	{ "88", 		FALSE,		TRUE, 		FALSE,		MSG_OTRO_VENDEDOR },
	{ "89", 		FALSE,		TRUE, 		FALSE,		MSG_RC_INVALID_TERMINAL },
	{ "91", 		FALSE,		TRUE, 		FALSE,		MSG_RC_NO_ISSUER },
	{ "92", 		FALSE,		TRUE, 		FALSE,		MSG_RC_NO_ISSUER },
	{ "94", 		FALSE,		TRUE, 		FALSE,		MSG_RC_DUPLICATE_TRANSMITION },
	{ "95", 		FALSE,		TRUE, 		FALSE,		MSG_RC_BATCH_UPLOAD },
	{ "96", 		FALSE,		FALSE, 		FALSE,		MSG_RC_SYSTEM_FAILURE },
	{ "AA", 		FALSE,		FALSE,		FALSE,		MSG_RC_DECLINED },
	{ "CE", 		FALSE,		FALSE,		FALSE,		MSG_RC_COMMUNICATION_ERROR },
	{ "Y1", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Y1 },
	{ "Y3", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Y3 },
	{ "Z1", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Z1 },
	{ "Z3", 		FALSE,		FALSE,		FALSE,		MSG_RC_RETRY_Z3 },
	{ "TO", 		FALSE,		FALSE,		FALSE,		MSG_RC_TIME_OUT },
	{ "PA", 		FALSE,		TRUE,		FALSE,		MSG_RC_PACK_ERROR },
	{ "UP", 		FALSE,		FALSE,		FALSE,		MSG_RC_UNPACK_ERROR },
	// EMV RC
	{ "D1", 		FALSE,		FALSE,		FALSE,		MSG_RC_EMV_DECLINED },
	{ "AF", 		TRUE,		FALSE,		TRUE,		MSG_RC_APPROVED },
	//+ ISOMAIN01 @mamata Aug 14, 2014
	{ "D2", 		FALSE,		FALSE,		FALSE,		MSG_RC_EMV_DECLINED },
	//- ISOMAIN01
	//+ ECHOTEST O LOGON @mamata Aug 15, 2014
	{ "ET",			TRUE,		FALSE,		FALSE,		MSG_RC_APPROVED },
	//EUGENIO
	{ "TC",			TRUE,		FALSE,		FALSE,		MSG_RC_APPROVED },
	//- ECHOTEST

	//+ EMV07 @mamata Mar 10, 2015
	{ "#C",			FALSE,		FALSE,		FALSE,		MSG_RC_EMV_CANCELED },
	{ "#E",			FALSE,		FALSE,		FALSE,		MSG_RC_EMV_CARD_ERROR },
	{ "#P",			FALSE,		FALSE,		FALSE,		MSG_RC_EMV_PROCESSING_ERROR },
	//- EMV07
	// end of list
	{ "FF", 		FALSE,		FALSE,		FALSE,		NULL },
};
/* ***************************************************************************
 * IMPLEMENTATION
 * ********/

/** ------------------------------------------------------------------------------
 * Function Name:	ISOBuildMessage
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Build a message format ISO8583 based on TLV_TREE_NODE Transaction
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int ISOBuildMessage( TLV_TREE_NODE Tree )
{
	//TABLE_HOST LHost;
	int i;
	int j;
	int TotalItens;
	int16 TranID;
	int16 statusTranID;
	int RetFun;
	TLV_TREE_NODE node;
	pCallSetBit	pCurrFunc = NULL;
	uint32 RangID;


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);



	if((TranID!=TRAN_TEST)  &&  (TranID!=TRAN_LOGON) &&  (TranID!=TRAN_SETTLE))
	{
	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);
	}

	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	if(node!=NULL)
	{
		memcpy((char*)&statusTranID, TlvTree_GetData(node), LTAG_UINT16);

		if(statusTranID & FSTATUS_NEED_VOID)
			TranID = TRAN_VOID;
		else if(statusTranID & FSTATUS_NEED_REVERSE)
			TranID = TRAN_REVERSE;
		else if(statusTranID & FSTATUS_OFFLINE)
			TranID = TRAN_OFFLINE;
		else if(statusTranID & FSTATUS_NEED_ADJUST)
		{
			if(TranID==TRAN_VOID)
				TranID = TRAN_VOID;
			else
			TranID = TRAN_ADJUST;

		}
	}

	node = TlvTree_Find(Tree, TAG_BATCH_UPLOAD, 0);
	if(node != NULL && TranID != TRAN_SETTLE)
		TranID = TRAN_UPLOAD;




	TotalItens = sizeof(lista)/sizeof(lista[0]);
	for ( i = 0; i < TotalItens; i++ )
	{
		if ( lista[i].TransacId == TranID )
		{
			if ( lista[i].pFuncs )
			{
				j = 0;
				pCurrFunc = lista[i].pFuncs[j++];

				while ( pCurrFunc )
				{
					RetFun = (pCurrFunc)(Tree);
					CHECK(RetFun == RET_OK, LBL_ERROR);
					pCurrFunc = lista[i].pFuncs[j++];
					memcpy(tMsgTx.vbMsgType, lista[i].msgType, L_msgType);
				}
			}
			break;
		}
	}

	if(((TranID == TRAN_VOID) || (TranID == TRAN_REVERSE)) && (RangID == 8))
	{
		memcpy(tMsgTx.vbMsgType, "0200", L_msgType);
	}

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
	LBL_ERROR:
		return j;
		//return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	ISO8583_Pack
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Generate a ISO8583 message format using the data on tMsgTx to build a message
 * Parameters:
 * - bufferTx    O message to send to host
 * - nSizeBuffer O size of message
 * Return:
 * - RET_OK, successful
 * - RET_NOK, No successful
 * Notes:
 */
int ISO8583_Pack( char* bufferTx, int *nSizeBuffer, TLV_TREE_NODE Tree)
{
	int RetFun;
	TLV_TREE_NODE node;
	uint16 TranID;


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	*nSizeBuffer = 0;

	memset((char*)&tMsgTx, 0, sizeof(tMsgTx));
	memset((char*)&tMsgRx, 0, sizeof(tMsgRx));

	RetFun = ISOBuildMessage(Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);



	if (TranID == TRAN_ADJUST) {
		RetFun = i16PackISO8583((TP_TABISO8583 *)stTabISO8583_adjust, (uint8 *) bufferTx );
		CHECK(RetFun != 0, LBL_ERROR);

	}else{
		RetFun = i16PackISO8583((TP_TABISO8583 *)stTabISO8583, (uint8 *) bufferTx );
		CHECK(RetFun != 0, LBL_ERROR);

	}



	*nSizeBuffer = RetFun;

	return RET_OK;

	LBL_ERROR:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	ISO8583_UnPack
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Unpack a ISO8583 message received from host and fill tMsgRx struct
 * Parameters:
 * - bufferRx    I message receive from host
 * - nSizeBuffer I size of message
 * Return:
 * - RET_OK, successful
 * - RET_NOK, No successful
 * Notes:
 */
int ISO8583_UnPack(char* bufferRx, int nSizeBuffer,TLV_TREE_NODE Tree)
{
	int RetFun;
	TLV_TREE_NODE node;
	uint16 TranID;


	memset(&tMsgRx, 0, sizeof(tMsgRx));


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	if (TranID == TRAN_ADJUST) {
		RetFun = i16UnPackISO8583((TP_TABISO8583 *)stTabISO8583_adjust, (uint8 *) bufferRx);
		CHECK(RetFun != 0, LBL_ERROR);
	}else{
		RetFun = i16UnPackISO8583((TP_TABISO8583 *)stTabISO8583, (uint8 *) bufferRx);
		CHECK(RetFun != 0, LBL_ERROR);
	}


	//UnpackBit63();

	return RET_OK;

	LBL_ERROR:
		return RET_NOK;

}

/** ------------------------------------------------------------------------------
 * Function Name:	SetTPDU
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Buid TPDU form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetTPDU(TLV_TREE_NODE Tree)
{


	int RetFun;
	TABLE_MERCHANT MERCHANT;


	// Destiny address

			RetFun = Get_TransactionMerchantData(&MERCHANT, Tree);
			CHECK(RetFun == RET_OK, LBL_ERROR);
				// TPDU message type
					memcpy(tMsgTx.stTPDU.vbTPDU_ID,MERCHANT.TPDU, 2);

					// Destiny address
					tMsgTx.stTPDU.vbDestino[0] = 0x30;
					memcpy(&tMsgTx.stTPDU.vbDestino[1],&MERCHANT.TPDU[3], 3);

					// Source address
					memcpy(tMsgTx.stTPDU.vbOrigem,&MERCHANT.TPDU[6], 4);

			return RET_OK;

	LBL_ERROR:
		return RET_NOK;
}


/** ------------------------------------------------------------------------------
 * Function Name:	SetBit2
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Buid Primary Acct. Num.  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit2(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;

	uint16 TranID;
	uint32 RangID;
	char tempbank[4];
	char temppan[12];
	char finalddata[18];


	memset(tempbank, 0, 4+1);
	memset(temppan, 0, 12+1);
	memset(finalddata, 0, 18+1);

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if((RangID == 8) || (TranID == TRAN_C2P ))
	{

		char valorfijo[4]="0358";
		node = TlvTree_Find(Tree, TAG_BANK_TYPE, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy(tempbank, TlvTree_GetData(node), LTAG_UINT32);

		node = TlvTree_Find(Tree, TAG_PAN, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy(temppan, TlvTree_GetData(node), 12);


		memcpy(finalddata, tempbank, 4);
		memcpy(&finalddata[4], valorfijo, 4);
		memcpy(&finalddata[8], temppan, 10);

		tMsgTx.ui16PANB_Len = 18;
		strcpy((char*)tMsgTx.aPAN, finalddata );
		return RET_OK;
	}


	// omit field if track2 is present
	node = TlvTree_Find(Tree, TAG_TRACK2, 0);
	if(node != NULL)
		return RET_OK;

	// omit field if track1 is present
	node = TlvTree_Find(Tree, TAG_TRACK1, 0);
	if(node != NULL)
		return RET_OK;

	node = TlvTree_Find(Tree, TAG_PAN, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	tMsgTx.ui16PANB_Len = TlvTree_GetLength(node);
	memcpy(tMsgTx.aPAN, TlvTree_GetData(node), tMsgTx.ui16PANB_Len);


	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit3
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Buid Processing Code  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit3(TLV_TREE_NODE Tree)
{
	int i;
	int TotalItens;
	uint16 TranID;
	uint16 TranStatus;
	TLV_TREE_NODE node;
	char prcCode[LZ_prcCode];
	uint8 AccountType;
	int16 statusTranID;

	//TABLE_RANGE Range;
	//BATCH BatchRecord;


	AccountType= 0;
	memset(prcCode, 0, LZ_prcCode);

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	// get tran status
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	// verify if necessary to change Tran ID
	if(TranStatus & FSTATUS_NEED_VOID)
		TranID = TRAN_VOID;
	else if(TranStatus & FSTATUS_NEED_ADJUST)
			{
				if(TranID==TRAN_VOID)
					TranID = TRAN_VOID;
				else
				TranID = TRAN_ADJUST;

			}

	TotalItens = sizeof(lista)/sizeof(lista[0]);
	for ( i = 0; i < TotalItens; i++ )
	{
		if ( lista[i].TransacId == TranID )
		{
			memcpy(prcCode, lista[i].prcCode, 2);
			memcpy(prcCode+4, lista[i].prcCode+4, 1);
			break;
		}
	}

	//+ ISOMAIN01 @mamata Aug 14, 2014
	// verify if is a settle and batch upload was performed
	node = TlvTree_Find(Tree, TAG_BATCH_UPLOAD, 0);
	if(node != NULL)
	{
		if(TranID == TRAN_SETTLE)
			prcCode[1] = '6';
		else
		{
			node = TlvTree_Find(Tree, TAG_BATCH_UPLOAD_LAST, 0);
			if(node == NULL)
				prcCode[5] = '1';
		}
	}
	//- ISOMAIN01

	node = TlvTree_Find(Tree, TAG_ACCOUNT_TYPE, 0);

	if(node != NULL)
		memcpy((char*)&AccountType, TlvTree_GetData(node), LTAG_UINT8);

	switch (AccountType) {
		case ACCOUNTTYPE_DEFAULT:
			memcpy(prcCode+2, "00", 2);
			break;
		case ACCOUNTTYPE_SAVINGS:
			memcpy(prcCode+2, "10", 2);
			break;
		case ACCOUNTTYPE_CHECKING:
			memcpy(prcCode+2, "20", 2);
			break;
		case ACCOUNTTYPE_CREDIT:
			memcpy(prcCode+2, "30", 2);
			break;
		default:
			memcpy(prcCode+2, "00", 2);
			break;
	}

	memcpy(prcCode+5, "0", 1);


	uint32 RangID;
	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if((RangID == 8) || (TranID == TRAN_C2P ))
	{


	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	if(node!=NULL)
	{

		memcpy((char*)&statusTranID, TlvTree_GetData(node), LTAG_UINT16);

		if((statusTranID & FSTATUS_NEED_REVERSE) || (statusTranID & FSTATUS_NEED_VOID))
		{
			memcpy(prcCode, "200030", 6);

		}
		else
		{
			memcpy(prcCode+2, "0009", 4);
		}

	}
}
	memcpy(tMsgTx.vbPCode, prcCode, L_prcCode);

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit4
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Buid Amount, Trans.   form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit4(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	amount totalAmount;
	amount tempAmount;
	int ConvertLen;
	char ConvertBuffer[13];
	uint16 TranID;
	TABLE_RANGE Range;
	int RetFun;
	TABLE_MERCHANT Merchant;

	memset(ConvertBuffer, 0, 13);
	totalAmount = 0;
	tempAmount = 0;

	node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND)
	memcpy((char*)&tempAmount, TlvTree_GetData(node), LTAG_AMOUNT);
	totalAmount+=tempAmount;

	node = TlvTree_Find(Tree, TAG_TAX_AMOUNT, 0);
	if (node!=NULL)
	{
		memcpy((char*)&tempAmount, TlvTree_GetData(node), LTAG_AMOUNT);
		totalAmount+=tempAmount;
	}

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		switch(TranID)
		{
		case TRAN_SALE:
		case TRAN_ADJUST:
		case TRAN_REVERSE:
		case TRAN_REFUND:
		case TRAN_C2P:

			node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
			if (node!=NULL)
			{
				memcpy((char*)&tempAmount, TlvTree_GetData(node), LTAG_AMOUNT);
				totalAmount+=tempAmount;
			}

			ConvertLen = Uint64_to_Ascii(totalAmount, ConvertBuffer, _FORMAT_RIGHT, 12, 0x30);
			CHECK(ConvertLen != 0, LBL_Amount_Error)

			memcpy(tMsgTx.aValor, ConvertBuffer, ConvertLen);

			return RET_OK;

			break;


		case TRAN_VOID:

		RetFun = Get_TransactionMerchantData(&Merchant, Tree);
		CHECK(RetFun == RET_OK, LBL_TAG_TRAN_ID_NOTFOUND);

		RetFun = Get_TransactionRangeData(&Range, Tree);
		if((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT))
		{
			tempAmount = (totalAmount * Merchant.TIP_Max_Percent) / 100;
			totalAmount+=tempAmount;
		}

			ConvertLen = Uint64_to_Ascii(totalAmount, ConvertBuffer, _FORMAT_RIGHT, 12, 0x30);
			CHECK(ConvertLen != 0, LBL_Amount_Error)

			memcpy(tMsgTx.aValor, ConvertBuffer, ConvertLen);

			return RET_OK;

			break;
		}


	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;

	LBL_Amount_Error:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit7
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		fecha completa form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
//EUGENIO
int SetBit7(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	DATE date;
	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&date, TlvTree_GetData(node), LTAG_DATE_TIME);


	memcpy(tMsgTx.aDtTm, date.month, 2);
	memcpy(tMsgTx.aDtTm+2, date.day, 2);
	memcpy(tMsgTx.aDtTm+4, date.hour, 2);
	memcpy(tMsgTx.aDtTm+6, date.minute, 2);
	memcpy(tMsgTx.aDtTm+8, date.second, 2);
	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}
//EUGENIO
/** ------------------------------------------------------------------------------
 * Function Name:	SetBit11
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Buid System Trace Audit Number form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit11(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 STAN;
	int lnSTAN = 6;
	unsigned char cSTAN[lnSTAN+1];
	int RetFun;

	node = TlvTree_Find(Tree, TAG_STAN, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&STAN, TlvTree_GetData(node), LTAG_UINT32);

	RetFun = Binasc(cSTAN, STAN, lnSTAN);
	CHECK(RetFun == 0, LBL_Error);
	memcpy(tMsgTx.aNSU, cSTAN, lnSTAN);

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
	LBL_Error:
		return RET_NOK;
}


/** ------------------------------------------------------------------------------
 * Function Name:	SetBit12
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid Local transaction Time form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit12(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	DATE date;


	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&date, TlvTree_GetData(node), LTAG_DATE_TIME);

	memcpy(tMsgTx.aTmLocal, date.hour, 2);
	memcpy(tMsgTx.aTmLocal+2, date.minute, 2);
	memcpy(tMsgTx.aTmLocal+4, date.second, 2);


		return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;



}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit13
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid Local transaction Date form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit13(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	DATE date;

	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&date, TlvTree_GetData(node), LTAG_DATE_TIME);

	memcpy(tMsgTx.aDtLocal, date.month, 2);
	memcpy(tMsgTx.aDtLocal+2, date.day, 2);


	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit14
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid Expiration Date  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit14(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	char date[4+1];
	uint16 TranID;
 	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);


	uint32 RangID;

	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if((RangID == 8) || (TranID == TRAN_C2P ))
	{
		return RET_OK;
	}
	// omit field if track2 is present
	node = TlvTree_Find(Tree, TAG_TRACK2, 0);
	if(node != NULL)
		return RET_OK;

	// omit field if track1 is present
	node = TlvTree_Find(Tree, TAG_TRACK1, 0);
	if(node != NULL)
		return RET_OK;


	node = TlvTree_Find(Tree, TAG_EXP_DATE, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy(date, TlvTree_GetData(node), TlvTree_GetLength(node));

	memcpy(tMsgTx.aDtExp, date, 4);

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}



int SetBit22(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint8 Entry_Mode;
	uint8 Fallback = 0;

	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&Entry_Mode, TlvTree_GetData(node), LTAG_UINT8);

	switch (Entry_Mode) {
		case ENTRY_MODE_NONE:
		case ENTRY_MODE_MANUAL:
			memcpy(tMsgTx.aEntryMode22, "012", 3);
			break;
		case ENTRY_MODE_SWIPE:
			node = TlvTree_Find(Tree, TAG_FALLBACK, 0);
			if(node != NULL)
				Fallback = *(uint8*)TlvTree_GetData(node);

			if(Fallback == 0)
				memcpy(tMsgTx.aEntryMode22, "022", 3);
			else
				memcpy(tMsgTx.aEntryMode22, "802", 3);

			break;
		case ENTRY_MODE_CHIP:
			memcpy(tMsgTx.aEntryMode22, "051", 3);
			break;

		case ENTRY_MODE_CLESS_SWIPE:
			memcpy(tMsgTx.aEntryMode22, "911", 3);
			break;

		case ENTRY_MODE_CLESS_CHIP:
			memcpy(tMsgTx.aEntryMode22, "071", 3);
			break;

		default:
			memcpy(tMsgTx.aEntryMode22, "000", 3);
			break;
	}

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}
//+ ISOMAIN01 @mamata Aug 14, 2014
/** ------------------------------------------------------------------------------
 * Function Name:	SetBit22
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid POS Entry Mode  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */

int SetBit23(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint8 Entry_Mode;

	uint16 TranID;
	uint32 RangID;

		node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

		if(((TranID == TRAN_VOID) || (TranID == TRAN_REVERSE)) && (RangID == 8))
		{
			return RET_OK;
		}


	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	if (node == NULL)
		return RET_NOK;

	memcpy((char*)&Entry_Mode, TlvTree_GetData(node), LTAG_UINT8);
	if ((Entry_Mode == ENTRY_MODE_CHIP) || (Entry_Mode == ENTRY_MODE_CLESS_CHIP))
	{
		node = TlvTree_Find(Tree, TAG_EMV_1GEN_TAGS, 0);
		if (node)
		{
			node = TlvTree_Find(node, TAG_EMV_APPLI_PAN_SEQUENCE_NUMBER, 0);
			if (node)
			{
				uint8 nIccSequence;
				memcpy((char*)&nIccSequence, TlvTree_GetData(node), LTAG_UINT8);
				Telium_Sprintf((char*)tMsgTx.cardSecuence, "%03x", nIccSequence);
			}
		}
	}
	//if (Entry_Mode == ENTRY_MODE_SWIPE || Entry_Mode == ENTRY_MODE_MANUAL )
	else
	{
					uint8 nIccSequence;
					memcpy((char*)&nIccSequence, "000", LTAG_UINT8);
					Telium_Sprintf((char*)tMsgTx.cardSecuence, "000");
	}
	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
			return RET_NOK;
}
//EUGENIO
/** ------------------------------------------------------------------------------
 * Function Name:	SetBit24
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid Network International Id form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit24(TLV_TREE_NODE Tree)
{
	TABLE_HOST LHost;
	int RetFun;

	// Destiny address
	RetFun = Get_TransactionHostData(&LHost, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	memcpy(&tMsgTx.aNII, LHost.NII, 3);

	return RET_OK;

	LBL_ERROR:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit25
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid POS Condition Code form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit25(TLV_TREE_NODE Tree)
{
	memcpy(&tMsgTx.aPOSCond, "00", 2);

	return RET_OK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit35
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid TRACK II Data form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */

int SetBit35(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint8 *posfs,  *posfs1;
	uint16 TranID;
	char EntryMode;


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	uint32 RangID;

		node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if(((TranID == TRAN_VOID) || (TranID == TRAN_REVERSE) || (TranID == TRAN_C2P )) && (RangID == 8))
	{

		return RET_OK;
	}

	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
    memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);

    CHECK((EntryMode != ENTRY_MODE_MANUAL) , lblnocampo);

	node = TlvTree_Find(Tree, TAG_TRACK2, 0);
	if(node == NULL)
		return RET_OK;

	tMsgTx.u16Track2_Len = TlvTree_GetLength(node);
	 memcpy(tMsgTx.aTrack2, TlvTree_GetData(node),tMsgTx.u16Track2_Len);
	  if ((posfs = (byte *)memchr(tMsgTx.aTrack2, '=', tMsgTx.u16Track2_Len)) != NULL)
	    *posfs = 'D';

	  posfs++;

	  if ((posfs1 = (byte *)memchr(posfs, '=', strlen((char*)posfs))) != NULL)
	    *posfs1 = 'D';


	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
			return RET_NOK;

	lblnocampo:
		return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	SetBit37
 * Description:		Seti Bit37 Host RRN
 * Parameters:
 *  - TLV_TREE_NODE Tree - Transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SetBit37(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;

	uint16 TranID;
	uint32 RangID;

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if((RangID == 8) || (TranID == TRAN_C2P ))
	{
		node = TlvTree_Find(Tree, TAG_HOST_RRN, 0);
		CHECK(node != NULL, LBL_ERROR);
		memcpy(tMsgTx.aRRN, TlvTree_GetData(node), 12);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;


	LBL_TAG_TRAN_ID_NOTFOUND:
	return RET_NOK;
}

//+ ISOMAIN01 @mamata Aug 14, 2014
/** ------------------------------------------------------------------------------
 * Function Name:	SetBit38
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid Authorization Number Data form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit38(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;

	uint16 TranID;
	uint32 RangID;

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


	if((RangID == 8) || (TranID == TRAN_C2P ))
	{
		return RET_OK;

	}

	node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
	if(node == NULL)
		return RET_OK;

	memcpy(tMsgTx.aAuthCode, TlvTree_GetData(node), 6);
	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}
//- ISOMAIN01

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit39
 * Author:			@DFVC
 * Date:		 	11/06/2014
 * Description:		Buid Authorization Number Data form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit39(TLV_TREE_NODE Tree)
{
	int RetFun;
	TABLE_MERCHANT MERCHANT;

	RetFun = Get_TransactionMerchantData(&MERCHANT, Tree);
//	CHECK(MERCHANT.Header.RecordID != debito, LBL_FALSE);
	memcpy(tMsgTx.aRespCode, "00", 2);
//	LBL_FALSE:
	return RET_OK;
}
/** ------------------------------------------------------------------------------
 * Function Name:	SetBit41
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid Terminal ID form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit41(TLV_TREE_NODE Tree)
{
	TABLE_MERCHANT MERCHANT;
	int RetFun;
	char TerminalId[LZ_MERCH_TID];

	// Destiny address
	RetFun = Get_TransactionMerchantData(&MERCHANT, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	RetFun = Format_String_Lenght(MERCHANT.TID, TerminalId, _FORMAT_RIGHT, L_MERCH_TID, '0');
	CHECK(RetFun==RET_OK, LBL_ERROR);

	memcpy(&tMsgTx.aTermId, TerminalId, L_MERCH_TID);

	return RET_OK;

	LBL_ERROR:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit42
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid Merchant ID form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit42(TLV_TREE_NODE Tree)
{
	TABLE_MERCHANT MERCHANT;
	int RetFun;
	char MerchantId[LZ_MERCH_LID];

	// Destiny address
	RetFun = Get_TransactionMerchantData(&MERCHANT, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	RetFun = Format_String_Lenght(MERCHANT.MID, MerchantId, _FORMAT_RIGHT, L_MERCH_LID, '0');
	CHECK(RetFun==RET_OK, LBL_ERROR);

	memcpy(&tMsgTx.aMerchId, MerchantId, L_MERCH_LID);


	return RET_OK;

	LBL_ERROR:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit45
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid TRACK I Data form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit45(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	char EntryMode;
	uint16 TranID;
	uint32 RangID;

		node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


		if((TranID == TRAN_REVERSE) && (RangID == 8))
	   {
		return RET_OK;
		}


	switch(TranID)
	{
	case TRAN_SALE:

	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_ERROR);
    memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);

    CHECK((EntryMode == ENTRY_MODE_SWIPE ) , lblnocampo);


	node = TlvTree_Find(Tree, TAG_TRACK1, 0);
	if(node == NULL)
		return RET_OK;

	tMsgTx.ui16Track1_Len = TlvTree_GetLength(node);
	memcpy(tMsgTx.vbTrack1, TlvTree_GetData(node), tMsgTx.ui16Track1_Len);

	break;

	case TRAN_REFUND:
	case TRAN_VOID:
	case TRAN_ADJUST:
	case TRAN_REVERSE:

		node = TlvTree_Find(Tree, TAG_TRACK1, 0);
		if(node == NULL)
			return RET_OK;

		tMsgTx.ui16Track1_Len = TlvTree_GetLength(node);
		memcpy(tMsgTx.vbTrack1, TlvTree_GetData(node), tMsgTx.ui16Track1_Len);
	break;
	}

	lblnocampo:
	return RET_OK;

	LBL_ERROR:
		return RET_NOK;
	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;

}


/** ------------------------------------------------------------------------------
 * Function Name:	SetBit48
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid Additional Data (P48) form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit48(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint8 CVV2_Type;
	int lenght;
	int RetFun;
	uint8 cnodeLenght[3];
	char tmpCVV2[4];
	char AuxBuffer[32];
	char Cedula[32];
	uint16 TranID;
	uint16 TranStatus;
	uint32 RangID;

	lenght = 0;
	memset(cnodeLenght, 0, sizeof(cnodeLenght));
	memset(tmpCVV2, 0, sizeof(tmpCVV2));
	memset(Cedula, 0, sizeof(Cedula));
	memset(AuxBuffer, 0, sizeof(AuxBuffer));

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);


	node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

	if(((TranID == TRAN_VOID) || (TranID == TRAN_REVERSE)) && (RangID == 8))
    {
	return RET_OK;
	}

	// get tran status
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	// verify if necessary to change Tran ID
	if(TranStatus & FSTATUS_NEED_VOID)
		TranID = TRAN_VOID;
	else if(TranStatus & FSTATUS_NEED_ADJUST)
			{
				if(TranID==TRAN_VOID)
					TranID = TRAN_VOID;
				else
				TranID = TRAN_ADJUST;

			}

	switch(TranID)
	{
	case TRAN_TEST:
		Telium_Sprintf((char*)tMsgTx.aAddData, "%03d", 999);
		break;

	case TRAN_ADJUST:
		RetFun = Format_String_Lenght(AuxBuffer, Cedula, _FORMAT_RIGHT, 13, '0');
		memcpy(tMsgTx.aAddData + lenght, Cedula, 13);
		tMsgTx.u16AddData_Len = strlen((char *)tMsgTx.aAddData);
		break;

	case TRAN_SALE:
	case TRAN_REFUND:
	case TRAN_VOID:
	case TRAN_REVERSE:
	case TRAN_C2P:

		node = TlvTree_Find(Tree, TAG_CVV2_TYPE, 0);
		if (node)
		{
			memcpy((char*)&CVV2_Type, TlvTree_GetData(node), LTAG_UINT8);

			if(CVV2_Type == 1)
			{
				node = TlvTree_Find(Tree, TAG_CVV2, 0);
				CHECK(node != NULL, LBL_ERROR);
				RetFun = Format_String_Lenght((char*)TlvTree_GetData(node), tmpCVV2, _FORMAT_LEFT, CVV2_CVC2_SIZE, '0');
				memcpy(tMsgTx.aAddData, tmpCVV2, 4);
			}
			else {
				memcpy(tMsgTx.aAddData, "    ", CVV2_CVC2_SIZE);
			}
		}
		else {
			memcpy(tMsgTx.aAddData, "    ", CVV2_CVC2_SIZE);
		}
		lenght =+ CVV2_CVC2_SIZE;

		//Adicional la Cedula de Identidad
		node = TlvTree_Find(Tree, TAG_ID_NUMBER, 0);
		CHECK(node != NULL, LBL_ERROR);

		memcpy((char*)&AuxBuffer, TlvTree_GetData(node), TlvTree_GetLength(node));
		RetFun = Format_String_Lenght(AuxBuffer, Cedula, _FORMAT_RIGHT, 10, '0');
		memcpy(tMsgTx.aAddData + lenght, Cedula, 10);
		tMsgTx.u16AddData_Len = strlen((char *)tMsgTx.aAddData);
		break;
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}
//EUGENIO
int SetBit49(TLV_TREE_NODE Tree)
{
	//TLV_TREE_NODE node;
	//uint8 Currency = 1;


	//node = TlvTree_Find(Tree, TAG_CURRENCY_CODE, 0);
	// get currency code
	//memcpy(&Currency,TlvTree_GetData(node), LTAG_UINT8);
	//local From table configuration, Dollar from define "0840"

	memcpy(tMsgTx.aCurrCode, tTerminal.CurrCode_Local, LZ_TERM_CURR_SYMBOL);


	return RET_OK;
//	LBL_ERROR:
//		return RET_NOK;
}
//EUGENIO
/** ------------------------------------------------------------------------------
 * Function Name:	SetBit52
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid PIN BLOCK form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit52(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	char PinBlock[8+1];
	//TABLE_RANGE Range;
	//int RetFun;


	//RetFun = Get_TransactionRangeData(&Range, Tree_Tran);
	//CHECK(Range.Flags1 & RANGE_FLAG1_DEBIT, LBL_FALSE);

	node = TlvTree_Find(Tree, TAG_PINBLOCK, 0);
	if(node == NULL)
		return RET_OK;

	memcpy((char*)PinBlock, TlvTree_GetData(node), LTAG_UINT8*8);
	tMsgTx.u16PinBlock_Len = TlvTree_GetLength(node);
	memcpy(tMsgTx.PinBLock, PinBlock, tMsgTx.u16PinBlock_Len);

//LBL_FALSE:
	return RET_OK;


}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit54_TAX
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid Additional Amounts - TAX  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 *  - *TAX, Memory posicion of Bit 54 where add tax data
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit54_TAX(TLV_TREE_NODE Tree, uint8 *TAX)
{
	TLV_TREE_NODE node;
	amount taxAmount;
	int ConvertLen;
	char ConvertBuffer[13];
	int lenData = 0;
	char tempData[2];

	memset(tempData, 0, 2);
	memset(ConvertBuffer, 0, 13);

	node = TlvTree_Find(Tree, TAG_TAX_AMOUNT, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND)

	memcpy((char*)&taxAmount, TlvTree_GetData(node), LTAG_AMOUNT);
	ConvertLen = Uint64_to_Ascii(taxAmount, ConvertBuffer, _FORMAT_RIGHT, 12, 0x30);
	CHECK(ConvertLen != 0, LBL_Amount_Error)

	// Data Type 1 = Tax
	tempData[0] = 0x01;
	memcpy(TAX, tempData, 1);
	lenData++;

	// Data lenght (12)
	tempData[0] = 0x12;
	memcpy(TAX+lenData, tempData, 1);
	lenData++;

	// Tax Data
	memcpy(TAX+lenData, ConvertBuffer, ConvertLen);
	lenData +=ConvertLen;

	return lenData;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return 0;

	LBL_Amount_Error:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit54_TIP
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid Additional Amounts - TIP  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 *  - *TIP, Memory posicion of Bit 54 where add tip data
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit54_TIP(TLV_TREE_NODE Tree, uint8 *TIP)
{
	TLV_TREE_NODE node;
	amount tipAmount;
	int ConvertLen;
	char ConvertBuffer[13];
	int lenData = 0;
	char tempData[2];

	memset(tempData, 0, 2);
	memset(ConvertBuffer, 0, 13);

	node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND)

	memcpy((char*)&tipAmount, TlvTree_GetData(node), LTAG_AMOUNT);
	ConvertLen = Uint64_to_Ascii(tipAmount, ConvertBuffer, _FORMAT_RIGHT, 12, 0x30);
	CHECK(ConvertLen != 0, LBL_Amount_Error)

	// Data Type 1 = TIP
	tempData[0] = 0x03;
	memcpy(TIP, tempData, 1);
	lenData++;

	// Data lenght (12)
	tempData[0] = 0x12;
	memcpy(TIP+lenData, tempData, 1);
	lenData++;

	// Tax Data
	memcpy(TIP+lenData, ConvertBuffer, ConvertLen);
	lenData +=ConvertLen;

	return lenData;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return 0;

	LBL_Amount_Error:
		return RET_NOK;
}

/** ------------------------------------------------------------------------------
 * Function Name:	SetBit54
 * Author:			@DFVC
 * Date:		 	12/06/2014
 * Description:		Buid Additional Amounts  form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit54(TLV_TREE_NODE Tree)
{
	int lenght;
	int dataNum = 0;
	int totalLenght = 0;
	int RetFun;
	uint8 cdataNum[2];

	memset(cdataNum, 0, 2);

	// Ingresa los datos de impuestos (IVA)
	lenght = 0;
	lenght = SetBit54_TAX(Tree, tMsgTx.aAddAmt+totalLenght+1);
	CHECK(lenght!=RET_NOK, LBL_ERROR);
	if(lenght > 0)
	{
		totalLenght += lenght;
		dataNum++;
	}

	// Ingesa los datos del propina
	lenght = 0;
	lenght = SetBit54_TIP(Tree, tMsgTx.aAddAmt+totalLenght+1);
	CHECK(lenght!=RET_NOK, LBL_ERROR);
	if(lenght > 0)
	{
		totalLenght += lenght;
		dataNum++;
	}

	// si se agrego algun campo
	if(dataNum > 0)
	{
		// Convierte el numero de campos ingresados a hexa
		RetFun = Binhex(cdataNum, (uint32)dataNum, 1);
		CHECK(RetFun==0, LBL_ERROR);

		memcpy(tMsgTx.aAddAmt, cdataNum, 1);
	}

	return RET_OK;

	LBL_ERROR:
		return RET_NOK;
}


/** ------------------------------------------------------------------------------
 * Function Name:
 * Author:			@pmata
 * Date:		 	13/06/2014
 * Description:
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Notes:
 */
int SetBit55(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
    char EntryMode;
	//int Ret;
	//uint8 TData = 1;
	// verify is a chip entry
	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_Error);
    memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);

CHECK((EntryMode == ENTRY_MODE_CHIP || EntryMode == ENTRY_MODE_CLESS_CHIP) , lbl_NOF55);

	//Ret = BuildEMVTags(node);


	//tMsgTx.u16Bit55_Len = lnBATCH_NUMBER;
	//memcpy(tMsgTx.aBit55, cBATCH_NUMBER, lnBATCH_NUMBER);

	lbl_NOF55:
	return RET_OK;

	LBL_Error:
		return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	SetBit62
 * Description:		Set Bit60 for general transaction with invoice number
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SetBit62(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 Invoice;

	node = TlvTree_Find(Tree, TAG_INVOICE, 0);
	CHECK(node != NULL, LBL_ERROR);

	Invoice = *(uint32*)TlvTree_GetData(node);

	Binasc(tMsgTx.aBit62, Invoice, 6);
	tMsgTx.u16Bit62_Len = 6;

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/** ------------------------------------------------------------------------------
 * Function Name:
 * Author:			@DFVC
 * Date:		 	13/06/2014
 * Description:
 * Parameters:
 *  - none
 * Return:
 *  - none
 * Not
 * es:
 */
int SetBit60_Settle(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint32 BATCH_NUMBER ;
	int lnBATCH_NUMBER  = 6;
	unsigned char cBATCH_NUMBER [lnBATCH_NUMBER+1];
	int RetFun;

	memset(cBATCH_NUMBER, 0, sizeof(cBATCH_NUMBER));

	node = TlvTree_Find(Tree, TAG_BATCH_NUMBER, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&BATCH_NUMBER, TlvTree_GetData(node), LTAG_UINT32);

	RetFun = Binasc(cBATCH_NUMBER, BATCH_NUMBER, lnBATCH_NUMBER);
	CHECK(RetFun == 0, LBL_Error);
	tMsgTx.u16Bit60_Len = lnBATCH_NUMBER;
	memcpy(tMsgTx.aBit60, cBATCH_NUMBER, lnBATCH_NUMBER);

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
	LBL_Error:
		return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Parameters:
 * Return:
 * Notes:
 */
int SetBit63_Settle(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int Index = 0;
	uint16 Count;
	amount Amount;

	// add sales info
	node = TlvTree_Find(Tree, TAG_SALES_COUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Count = *(uint16*)TlvTree_GetData(node);
	node = TlvTree_Find(Tree, TAG_SALES_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Amount = *(amount*)TlvTree_GetData(node);
	Binasc(&tMsgTx.aBit63[Index], Count, 3); Index+=3;
	Uint64_to_Ascii(Amount, (char*)&tMsgTx.aBit63[Index], _FORMAT_RIGHT, 12, '0'); Index+=12;

	// add refund info
	node = TlvTree_Find(Tree, TAG_REFUND_COUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Count = *(uint16*)TlvTree_GetData(node);
	node = TlvTree_Find(Tree, TAG_REFUND_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Amount = *(amount*)TlvTree_GetData(node);
	Binasc(&tMsgTx.aBit63[Index], Count, 3); Index+=3;
	Uint64_to_Ascii(Amount, (char*)&tMsgTx.aBit63[Index], _FORMAT_RIGHT, 12, '0'); Index+=12;

	// add debit info
	node = TlvTree_Find(Tree, TAG_DEBIT_COUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Count = *(uint16*)TlvTree_GetData(node);
	node = TlvTree_Find(Tree, TAG_DEBIT_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Amount = *(amount*)TlvTree_GetData(node);
	Binasc(&tMsgTx.aBit63[Index], Count, 3); Index+=3;
	Uint64_to_Ascii(Amount, (char*)&tMsgTx.aBit63[Index], _FORMAT_RIGHT, 12, '0'); Index+=12;

	// add refund debit info
	node = TlvTree_Find(Tree, TAG_RDEBIT_COUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Count = *(uint16*)TlvTree_GetData(node);
	node = TlvTree_Find(Tree, TAG_RDEBIT_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Amount = *(amount*)TlvTree_GetData(node);
	Binasc(&tMsgTx.aBit63[Index], Count, 3); Index+=3;
	Uint64_to_Ascii(Amount, (char*)&tMsgTx.aBit63[Index], _FORMAT_RIGHT, 12, '0'); Index+=12;

	// add auths info
	memset(&tMsgTx.aBit63[Index], '0', 15); Index+=15;

	// add refund auths info
	memset(&tMsgTx.aBit63[Index], '0', 15); Index+=15;

	// set bit63 length
	tMsgTx.u16Bit63_Len = Index;

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

void WriteLog(byte* pMessage, int size)
{
	char cpMessage[3000] = {0};
	char *cpCurrent = cpMessage;
	int  nEnd;

	convBufToHex(pMessage, size, cpMessage, sizeof(cpMessage));
	logSetChannels(LOG_CH_BASE, 1, LOG_ALWAYS, logWriteTeliumTrace, NULL, (void *) 0xFFFF);

	logWriteTeliumTrace((void*)0xFFFF, 0, "----------------------------------------", 40);

	nEnd = strlen(cpMessage);
	while (cpCurrent < cpMessage + strlen(cpMessage))
	{

		if (nEnd <= 194)
			logWriteTeliumTrace((void*)0xFFFF, 0, cpCurrent, nEnd );
		else
		{
			char cpTemp[3000] = {0};
			char *cpLine;

			strncpy(cpTemp, cpCurrent, nEnd);
			cpLine = cpTemp;
			while (cpLine < cpTemp + nEnd)
			{
				if (strlen(cpLine) > 180)
				{
					logWriteTeliumTrace((void*)0xFFFF, 0, cpLine, 180 );
					cpLine += 180;
				}
				else
				{
					logWriteTeliumTrace((void*)0xFFFF, 0, cpLine, strlen(cpLine) - 1  );
					cpLine += strlen(cpLine);
				}
			}
		}
		cpCurrent += nEnd + 1;
		nEnd = strlen(cpCurrent);
	}
}
/* --------------------------------------------------------------------------
 * Function Name:	HostMain_PackMessage
 * Description:		build iso8583 message in TXBuffer from Tree transaction data
 * Parameters:
 *  TLV_TREE_NODE Tree - Transaction data tree
 * Return:
 *  RET_OK - message packed OK
 *  RET_NOK - cannot pack message
 * Notes:
 */
int HostMain_PackMessage(TLV_TREE_NODE Tree)
{
#ifdef HOST_TEST
	const unsigned char EchoTest800[]=
	{
		/*ETH HEX LEN*/		0x00, 0x2E,
		/*TPDU*/			0x60, 0x00, 0x09, 0x00, 0x00,
		/*Message Type*/	0x08, 0x00,
		/*Bitmap*/			0x20, 0x20, 0x01, 0x00, 0x00, 0xC0, 0x00, 0x00,
		/*Processing Code*/	0x99, 0x00, 0x00,
		/*System Trace N*/	0x00, 0x00, 0x01,
		/*NII*/				0x00, 0x03,
		/*Terminal ID*/		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
		/*Acquirer ID*/		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35
	};
	memcpy(TXBuffer, EchoTest800, TXBufferLen = sizeof(EchoTest800));
#endif //HOST_TEST
	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_UnpackMessage
 * Description:		unpack message and copy host field to Tree
 * Parameters:
 *  TLV_TREE_NODE Tree - Transaction data, where to put host fields
 * Return:
 *  RET_OK - message unpacked OK
 *  RET_NOK - error in message
 * Notes:
 */
int HostMain_UnpackMessage(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint16 TranStatus;
//	TABLE_MERCHANT MERCHANT;
	TABLE_RANGE Range;
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

#ifndef HOST_OFFLINE
	int RetFun;
#endif
	//+ ECHOTEST @mamata Aug 15, 2014
	uint16 TranID;
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);
	//- ECHOTEST
#ifndef HOST_OFFLINE
	// unpack message
	RetFun = ISO8583_UnPack((char*)RXBuffer, RXBufferLen, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// validate response
	//+ ECHOTEST @mamata Aug 15, 2014
	if ( TranID != TRAN_ADJUST) {

	if( TranID == TRAN_TEST)
		RetFun = HostMain_ValidateResponse(TRUE);
	else
		RetFun = HostMain_ValidateResponse(FALSE);
	//- ECHOTEST
	CHECK(RetFun == RET_OK, LBL_ERROR);

	}
	if (TranID == TRAN_TEST) {

		if (tMsgRx.aValor[0] != 0) {

//			node = TlvTree_Find(Tree, TAG_ADD_AMOUNT, 0);
//			if(node != NULL)
//				TlvTree_Release(node);
//			//- ISOMAIN01
//
//			node = TlvTree_AddChild(Tree, TAG_ADD_AMOUNT, tMsgRx.aValor, LTAG_AMOUNT);
//			CHECK(node != NULL, LBL_ERROR);
			RetFun = TM_FindFirst(TAB_TERMINAL, &tTerminal);
			CHECK(RetFun == RET_OK, LBL_ERROR);

//			tTerminal.Amount_Pin_Debit = tMsgRx.aValor;
            memcpy((char *)&tTerminal.User_Label1,(char *)&tMsgRx.aValor,12);
			RetFun = TM_ModifyRecord(TAB_TERMINAL, &tTerminal);
			CHECK(RetFun == RET_OK, LBL_ERROR);

		}
	}

	// add response code to tree
	if(tMsgRx.aRespCode[0] != 0)
	{
		//+ ISOMAIN01 @mamata Aug 14, 2014
		node = TlvTree_Find(Tree, TAG_HOST_RESPONSE_CODE, 0);
		if(node != NULL)
			TlvTree_Release(node);
		//- ISOMAIN01

		node = TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, tMsgRx.aRespCode, 2);
		CHECK(node != NULL, LBL_ERROR);
	}

	// add authorization number to tree
	if(tMsgRx.aAuthCode[0] != 0)
	{
		//+ ISOMAIN01 @mamata Aug 14, 2014
		node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
		if(node != NULL)
			TlvTree_Release(node);
		//- ISOMAIN01

		node = TlvTree_AddChild(Tree, TAG_HOST_AUTH_NUMBER, tMsgRx.aAuthCode, 6);
		CHECK(node != NULL, LBL_ERROR);
	}
//BIT 53
	if(tMsgRx.campo53rx[0] != 0)
	{
		RetFun = Tlv_SetTagValueString(Tree, TAG_CAMPO53, (char*)tMsgRx.campo53rx);
		CHECK(RetFun == RET_OK, LBL_ERROR);
		RetFun = SaveKeyInFile();
	}


//



	// add RRN to tree
	if(tMsgRx.aRRN[0] != 0)
	{
		//+ ISOMAIN01 @mamata Aug 14, 2014
		node = TlvTree_Find(Tree, TAG_HOST_RRN, 0);
		if(node != NULL)
			TlvTree_Release(node);
		//- ISOMAIN01

		node = TlvTree_AddChild(Tree, TAG_HOST_RRN, tMsgRx.aRRN, 12);
		CHECK(node != NULL, LBL_ERROR);
	}

	// GET emv tags from host
	if(tMsgRx.btField127Responselen != 0)
	{
		RetFun = GetEMVTagsFromHost(Tree);
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}

	//+ ISOMAIN01 @mamata Aug 14, 2014
	HostMain_UnpackBit63(Tree);
//ajuste de transaccion
	RetFun = Get_TransactionRangeData(&Range, Tree);

	if((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT))
	{
	//	if ((TranID == TRAN_SALE) && (!strcmp((char*)tMsgRx.aRespCode, "00")) && !(TranStatus & FSTATUS_NEED_ADJUST))
		if ((TranID == TRAN_SALE) && (!strcmp((char*)tMsgRx.aRespCode, "00")))
		{
			uint16 TranStatus;
			Tlv_GetTagValue(Tree, TAG_TRAN_STATUS, &TranStatus, NULL);
		//	TranStatus |= FSTATUS_NEED_ADJUST;
			TranStatus |= FSTATUS_ADJUSTED;
			Tlv_SetTagValueInteger(Tree, TAG_TRAN_STATUS, TranStatus, LTAG_UINT16);

			RetFun = Batch_Modify_Transaction(Tree);
		}
		//anulacion
		/*if ((TranID == TRAN_VOID) && (!strcmp((char*)tMsgRx.aRespCode, "00")))
				{
				uint16 TranStatus;

				// set the adjust flat to send the advice later
				Tlv_GetTagValue(Tree, TAG_TRAN_STATUS, &TranStatus, NULL);
				//TranStatus &= ~FSTATUS_NEED_ADJUST;
				TranStatus &= ~FSTATUS_ADJUSTED;
				Tlv_SetTagValueInteger(Tree, TAG_TRAN_STATUS, TranStatus, LTAG_UINT16);

				RetFun = Batch_Modify_Transaction(Tree);
				}*/
	}
	//- ISOMAIN01
#else
	amount TranAmount;
	uint16 TranStatus;
	int16 TranOptions;

	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	if (TranID == TRAN_SETTLE)
	{
		node = TlvTree_Find(Tree, TAG_BATCH_UPLOAD, 0);
		if(node)
		{
			Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, "00");
			return RET_OK;
		}
		else
		{
			Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, "95");  // 95 to simulate batch upload, 00 for close on first message
			return RET_OK;
		}
	}


	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree, TAG_TRAN_OPTIOS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranOptions = *(uint16*)TlvTree_GetData(node);

	Tlv_SetTagValueInteger(Tree, TAG_TRAN_ANSWER, 1, sizeof(int));

	if((TranStatus & FSTATUS_NEED_VOID) || (TranOptions & FTRAN_REVERSE))
	{
		Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, "00");

		return RET_OK;
	}



	node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranAmount = *(amount*)TlvTree_GetData(node);

	if(TranAmount<=390000){

		switch(TranAmount){

		case 380000:
			Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, "01");
			break;
		case 370000:
			Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, "02");
			break;
		default:
			//node = TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, tMsgRx.aRespCode, 2);
			Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, "00");  // (uint8*)0

			node = TlvTree_AddChildString(Tree, TAG_HOST_AUTH_NUMBER, "404811");
			CHECK(node != NULL, LBL_ERROR);

			break;
		}

/*	}

//new adjust
	if(TranID == TRAN_SALE)
	{
		uint16 TranStatus;

		// set the adjust flat to send the advice later
		Tlv_GetTagValue(Tree, TAG_TRAN_STATUS, &TranStatus, NULL);
//		TranStatus |= FSTATUS_NEED_ADJUST;
		Tlv_SetTagValueInteger(Tree, TAG_TRAN_STATUS, TranStatus, LTAG_UINT16);

		Batch_Modify_Transaction(Tree);*/
	}


#endif
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_Connect
 * Description:		begin host connection
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction tree
 * Return:
 *  - RET_OK - connect to host is begin
 *  - RET_NOK - cannot begin host connection
 * Notes:
 */
int HostMain_Connect(TLV_TREE_NODE Tree)
{
	uint16 TranID;
	TLV_TREE_NODE node;
	int RetFun;
	uint8 ResponseCode[2];
	//int err;
	//int nStatus;

	//+ HOST01 @mamata Aug 14, 2014
	TABLE_HOST tHost;
	TABLE_IP tIP;
	//- HOST01

	// Get transaction id
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	// Configure LLComms
	// get host info
	RetFun = Get_TransactionHostData(&tHost, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	//+ HOST01 @mamata Aug 14, 2014
	memset(&LLC_Host, 0, sizeof(LLCOMS_CONFIG));

	// configure first connection
	LLC_Host.First_Connection.CommType = tHost.CommunicationType;
	switch(tHost.CommunicationType)
	{
		case LLC_GPRS:
			memcpy(LLC_Host.GPRS_APN, tConf.GPRS_APN, strlen(tConf.GPRS_APN));
			memcpy(LLC_Host.GPRS_Usr, tConf.GPRS_User, strlen(tConf.GPRS_User));
			memcpy(LLC_Host.GPRS_Pass, tConf.GPRS_Pass, strlen(tConf.GPRS_Pass));
		case LLC_IP:
		case LLC_WIFI:
		case LLC_PCL:
			RetFun = TM_FindRecord(TAB_IP, &tIP, tHost.IP_Primary_Address1);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);


			if(LLC_Host.First_Connection.CommType == LLC_GPRS){
				memcpy(LLC_Host.First_Connection.Address1, tIP.IP_GPRS, strlen(tIP.IP_GPRS));
				memcpy(LLC_Host.First_Connection.Port1, tIP.PORT_GPRS, strlen(tIP.PORT_GPRS));
			}else{
				memcpy(LLC_Host.First_Connection.Address1, tIP.Host, strlen(tIP.Host));
				memcpy(LLC_Host.First_Connection.Port1, tIP.Port, strlen(tIP.Port));
			}


			if(LLC_Host.First_Connection.CommType == LLC_IP){
			//JR
				if(!(IsEthernetConnected()))
				return RET_NOK;
			}

			if(LLC_Host.First_Connection.CommType == LLC_WIFI){
				if(!IsRadioWifi())
				return RET_NOK;
			}


			if(tIP.Flags1 & IP_FLAG1_ENABLE_SSL)
			{

				LLC_Host.First_Connection.bSSL1 = 1;
				memcpy(LLC_Host.First_Connection.SSLProf1, FILE_CERT_ROOT_TLS, 8);
				ConfigureTLSProfile( FILE_CERT_ROOT_TLS);
				 //TlvTree_AddChildString( hTransportConfig, LL_TCPIP_T_SSL_PROFILE, FILE_CERT_ROOT_TLS );

				memcpy(LLC_Host.First_Connection.SSLProf1, tIP.Server_SSL_Profile, strlen(tIP.Server_SSL_Profile));
			}

		/*	RetFun = TM_FindRecord(TAB_IP, &tIP, tHost.IP_Primary_Address2);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);
			memcpy(LLC_Host.First_Connection.Address2, tIP.Host, strlen(tIP.Host));
			memcpy(LLC_Host.First_Connection.Port2, tIP.Port, strlen(tIP.Port));
			if(tIP.Flags1 & IP_FLAG1_ENABLE_SSL)
			{
				LLC_Host.First_Connection.bSSL2 = 1;
				memcpy(LLC_Host.First_Connection.SSLProf2, tIP.Server_SSL_Profile, strlen(tIP.Server_SSL_Profile));
			}
		*/
			if(tIP.Flags1 & IP_FLAG1_ADD_HEX_LENGTH)
				LLC_Host.First_Connection.bAddHexLength = 1;
			break;
		case LLC_DIAL:
			memcpy(LLC_Host.First_Connection.Phone1, tHost.Dial_Primary_Phone1, strlen(tHost.Dial_Primary_Phone1));
			memcpy(LLC_Host.First_Connection.Phone2, tHost.Dial_Primary_Phone2, strlen(tHost.Dial_Primary_Phone2));

			LLC_Host.Dial_Tone = 1;
			LLC_Host.Dial_bDetectTone = tConf.Dial_DetectTone;
			LLC_Host.Dial_bDetectBussy = tConf.Dial_DetectBusy;
			LLC_Host.Dial_BlindDialPause = tConf.Dial_BlindDialPause;
			LLC_Host.Dial_DialToneTO = tConf.Dial_ToneTO;
			LLC_Host.Dial_PABX_Pause = tConf.Dial_PABXPause;

			//+ KLC_WRK @damaro Jul 19, 2017 ABV.GENERAL(16)
			//d if(tConf.Dial_PABX[0] != 0)
			if( (tConf.Dial_PABX[0] != 0) &&
				(strlen(tConf.Dial_PABX) <= sizeof(LLC_Host.Dial_PABX) )
			  )
			//- KLC_WRK @damaro Jul 19, 2017 ABV.GENERAL(16)
				memcpy(LLC_Host.Dial_PABX, tConf.Dial_PABX, strlen(tConf.Dial_PABX));

			if(tHost.ConnectionMode == 0)
			{
				LLC_Host.Dial_ModemMode = tConf.Dial_ModemMode;
			}
			else
			{
				LLC_Host.Dial_ModemMode = tHost.ConnectionMode;
			}

			break;
	}
	//LLC_Host.First_Connection.Retries = tHost.ConnectionRetries;
	LLC_Host.First_Connection.Retries = 3;
	LLC_Host.First_Connection.TimeoutConn = tHost.ConnectionTimeOut;
	LLC_Host.First_Connection.TimeoutAns = tHost.ResponseTimeOut;



	// configure second connection
	CHECK(tHost.BackupCommunicationType != LLC_DISABLE, LBL_STAR_CONNECTION);
	LLC_Host.Second_Connection.CommType = tHost.BackupCommunicationType;
	switch(tHost.BackupCommunicationType)
	{
		case LLC_GPRS:
			memcpy(LLC_Host.GPRS_APN, tConf.GPRS_APN, strlen(tConf.GPRS_APN));
			memcpy(LLC_Host.GPRS_Usr, tConf.GPRS_APN, strlen(tConf.GPRS_User));
			memcpy(LLC_Host.GPRS_Pass, tConf.GPRS_APN, strlen(tConf.GPRS_Pass));
		case LLC_IP:
		case LLC_WIFI:
		case LLC_PCL:
			RetFun = TM_FindRecord(TAB_IP, &tIP, tHost.IP_Backup_Address1);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);
			if(tHost.CommunicationType == LLC_GPRS){
				memcpy(LLC_Host.First_Connection.Address1, tIP.IP_GPRS, strlen(tIP.Host));
				memcpy(LLC_Host.First_Connection.Port1, tIP.PORT_GPRS, strlen(tIP.Port));
			}else{
				memcpy(LLC_Host.First_Connection.Address1, tIP.Host, strlen(tIP.Host));
				memcpy(LLC_Host.First_Connection.Port1, tIP.Port, strlen(tIP.Port));
			}
			if(tIP.Flags1 & IP_FLAG1_ENABLE_SSL)
			{
				LLC_Host.Second_Connection.bSSL1 = 1;
				memcpy(LLC_Host.Second_Connection.SSLProf1, tIP.Server_SSL_Profile, strlen(tIP.Server_SSL_Profile));
			}

			RetFun = TM_FindRecord(TAB_IP, &tIP, tHost.IP_Backup_Address2);
			CHECK(RetFun == TM_RET_OK, LBL_ERROR);
			memcpy(LLC_Host.Second_Connection.Address2, tIP.Host, strlen(tIP.Host));
			memcpy(LLC_Host.Second_Connection.Port2, tIP.Port, strlen(tIP.Port));
			if(tIP.Flags1 & IP_FLAG1_ENABLE_SSL)
			{
				LLC_Host.Second_Connection.bSSL2 = 1;
				memcpy(LLC_Host.Second_Connection.SSLProf2, tIP.Server_SSL_Profile, strlen(tIP.Server_SSL_Profile));
			}

			if(tIP.Flags1 & IP_FLAG1_ADD_HEX_LENGTH)
				LLC_Host.Second_Connection.bAddHexLength = 1;

			break;
		case LLC_DIAL:
			memcpy(LLC_Host.Second_Connection.Phone1, tHost.Dial_Backup_Phone1, strlen(tHost.Dial_Backup_Phone1));
			memcpy(LLC_Host.Second_Connection.Phone2, tHost.Dial_Backup_Phone2, strlen(tHost.Dial_Backup_Phone2));
			break;
	}
	LLC_Host.Second_Connection.Retries = atoi(tHost.User_Field03);
	LLC_Host.Second_Connection.TimeoutConn = atoi(tHost.User_Field04);
	LLC_Host.Second_Connection.TimeoutAns = tHost.ResponseTimeOut;

LBL_STAR_CONNECTION:
	// star connection to host
	RetFun = LLC_StartConnection(&LLC_Host);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	//- HOST01


	/*if(LLC_Host.First_Connection.CommType == LLC_GPRS){
	 err = LL_Network_GetStatus(LL_PHYSICAL_V_GPRS, &nStatus);
	 if (nStatus==LL_STATUS_GPRS_CONNECTED)
		 return RET_OK;
	 else
		 return RET_NOK;
	}*/


	return RET_OK;

LBL_ERROR:
	strcpy((char*)ResponseCode, "CE");
	Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, (char*)ResponseCode);

	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SendReceive
 * Description:		Send request to host and receive response forom host
 * Parameters:
 *  - TLV_TREE_NODE Tree - Transaction data
 * Return:
 *  - RET_OK - process OK
 *  - RET_NOK - configuration error
 *  -2 - pack error
 *  -3 - unpack error
 *  -4 - cannot connect to host
 *  <0 - link layer error
 * Notes:
 */
int HostMain_SendReceive_Transaction(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint32 Invoice;
	uint32 STAN;
	uint16 TranOptions;
	uint16 TranStatus;
	uint8 ResponseCode[2];
	uint8 BatchName[LZ_MERCH_BATCH_NAME];
	DATE SysDateTime;
	//TABLE_RANGE Range;
	// clear buffers
	memset(TXBuffer, 0, L_Buffer);
	memset(RXBuffer, 0, L_Buffer);
	TXBufferLen = RXBufferLen = 0;

	//+ ECHOTEST @mamata Aug 15, 2014
	uint16 TranID;
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	//- ECHOTEST

#ifndef HOST_OFFLINE
	// connect to host if didn't perform a predial
	node = TlvTree_Find(Tree, TAG_PREDIAL, 0);
	if(node == NULL)
	{
		RetFun = HostMain_Connect(Tree);
		CHECK(RetFun == RET_OK, LBL_COMM_ERROR);
	}

	// wait for connect to host is completed
	//+ HOST01 @mamata Aug 14, 2014

	//RetFun = LLC_WaitConnection(&LLC_Host,TRUE);
	RetFun = LLC_WaitConnection(TRUE);

	//- HOST01

	CHECK(RetFun == LLC_STATUS_CONNECTED, LBL_COMM_ERROR);
#endif
	//+ ECHOTEST @mamata Aug 15, 2014
	CHECK(TranID != TRAN_TEST, LBL_ECHO_TEST);
	CHECK(TranID != TRAN_LOGON, LBL_ECHO_TEST);
	//- ECHOTEST

	// get BatchName
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_NAME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memset(BatchName, 0, LZ_MERCH_BATCH_NAME);
	memcpy(BatchName, TlvTree_GetData(node), TlvTree_GetLength(node));

	// Send reverse if necessary
	RetFun = HostMain_SendReverse(BatchName);
	//+ ISOMAIN01 @mamata Aug 14, 2014
	CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);
	//- ISOMAIN01
//new adjust
/*	if((TranID != TRAN_TEST) && (TranID != TRAN_LOGON))
	{
		
		// send advice if necessary
		RetFun = HostMain_SendAdvice(BatchName, FALSE);
		CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);

	}*/
	//
	node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);


	DATE date;
	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&date, TlvTree_GetData(node), LTAG_DATE_TIME);

	node = TlvTree_AddChild(Tree, TAG_TIME_REVERSE, &date, sizeof(DATE));
	CHECK(node != NULL, LBL_ERROR);


	if(TranStatus & FSTATUS_NEED_VOID)
	{
		TRAN_INFO TranInfo;

		// Set VOID options
		RetFun = GetTransactionInfo(TRAN_VOID, &TranInfo);
		node = TlvTree_Find(Tree, TAG_TRAN_OPTIOS, 0);
		CHECK(node != NULL, LBL_ERROR);
		RetFun = TlvTree_SetData(node, &TranInfo.TRAN_FLAG, LTAG_UINT16);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
		TranOptions = TranInfo.TRAN_FLAG;

		/*if((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT))
		{
		TranStatus &= ~FSTATUS_NEED_ADJUST;
		RetFun = Tlv_SetTagValueInteger(Tree, TAG_TRAN_STATUS, TranStatus, LTAG_INT16);
		}*/

		// release TAG_APPROVED
		node = TlvTree_Find(Tree, TAG_APPROVED, 0);
		if(node != NULL)
			TlvTree_Release(node);

		// release TAG_HOST_RESPONSE_CODE
		node = TlvTree_Find(Tree, TAG_HOST_RESPONSE_CODE, 0);
		if(node != NULL)
			TlvTree_Release(node);

		// release TAG_RESPONSE_CODE_TEXT
		node = TlvTree_Find(Tree, TAG_RESPONSE_CODE_TEXT, 0);
		if(node != NULL)
			TlvTree_Release(node);

	}
	else
	{
		// Get transaction options
		node = TlvTree_Find(Tree, TAG_TRAN_OPTIOS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranOptions = *(uint16*)TlvTree_GetData(node);

		// add current invoice to Tree
		Invoice = Counters_Get_Invoice();
		CHECK(Invoice > 0, LBL_ERROR);
		node = TlvTree_AddChild(Tree, TAG_INVOICE, &Invoice, LTAG_UINT32);
		CHECK(node != NULL, LBL_ERROR);

		// add system date and time
		//+ FIXDATETIME @mamata 5/05/2016
		//d RetFun = ReadDateBooster(&SysDateTime);
		RetFun = Telium_Read_date(&SysDateTime);
		//- FIXDATETIME
		CHECK(RetFun == RET_OK, LBL_ERROR);
		node = TlvTree_AddChild(Tree, TAG_DATE_TIME, &SysDateTime, sizeof(DATE));
		CHECK(node != NULL, LBL_ERROR);
	}

	// add current STAN to tree
	STAN = Counters_Get_STAN();
	CHECK(STAN > 0, LBL_ERROR);
	node = TlvTree_AddChild(Tree, TAG_STAN, &STAN, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);


	//creo que es aqui magallanes
	// Save reverse
	if(TranOptions & FTRAN_REVERSE)
	{
		// Get transaction status
		node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
		CHECK(node != NULL, LBL_ERROR);
		TranStatus = *(uint16*)TlvTree_GetData(node);
		// turn on need reverse flag
		TranStatus|= FSTATUS_NEED_REVERSE;
		// update tree
		RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
		// add record to batch
		RetFun = Batch_Add_Transaction(Tree);
		CHECK(RetFun == RET_OK, LBL_ERROR);

		// return status to original value
		TranStatus &= ~FSTATUS_NEED_REVERSE;
		RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
		CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);
	}

	//clear buffers
	memset(TXBuffer, 0, L_Buffer);
	memset(RXBuffer, 0, L_Buffer);
	TXBufferLen = 0;
	RXBufferLen = 0;

//+ ECHOTEST @mamata Aug 15, 2014
LBL_ECHO_TEST:
	if(TranID == TRAN_TEST)
	{
		STAN = Counters_Get_STAN();
		CHECK(STAN > 0, LBL_ERROR);
		node = TlvTree_AddChild(Tree, TAG_STAN, &STAN, LTAG_UINT32);
		CHECK(node != NULL, LBL_ERROR);
	}

	if(TranID == TRAN_LOGON)
		{
			STAN = Counters_Get_STAN();
			CHECK(STAN > 0, LBL_ERROR);
			node = TlvTree_AddChild(Tree, TAG_STAN, &STAN, LTAG_UINT32);
			CHECK(node != NULL, LBL_ERROR);
		}
//- ECHOTEST

	// pack iso8583 message
	RetFun = ISO8583_Pack((char*)TXBuffer, &TXBufferLen, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR_PACK);

	// send message to host
	RetFun = HostMain_Send_Request(Tree);
	CHECK(RetFun == RET_OK, LBL_COMM_ERROR);

//	WriteLog(TXBuffer, TXBufferLen);

	// receive message from host
	RetFun = HostMain_Receive_Response(Tree);
	CHECK(RetFun == RET_OK, LBL_COMM_ERROR);

	// unpack response iso8583 message
	RetFun = HostMain_UnpackMessage(Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR_UNPACK);

//	WriteLog(RXBuffer, RXBufferLen);

	//+ ISOMAIN01 @mamata Aug 14, 2014
	RetFun = HostMain_UpdateDateTime(Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR_UNPACK);
	//- ISOMAIN01


	//+ ECHOTEST @mamata Aug 15, 2014
	if(TranID == TRAN_TEST || TranID == TRAN_LOGON )
	{
		memcpy(ResponseCode, "ET", 2);
		node = TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, ResponseCode, 2);
	}
/*	else
	{
		// verify if credit ajuste
		if((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT))
		{
			// send advice if necessary
			RetFun = HostMain_SendAdvice(BatchName, FALSE);
			CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);

			TranStatus &= ~FSTATUS_NEED_ADJUST;
			Tlv_SetTagValueInteger(Tree, TAG_TRAN_STATUS, TranStatus, LTAG_UINT16);

		}
	}*/

	// release TAG_HOST_RESPONSE_CODE
	//	node = TlvTree_Find(Tree, TAG_HOST_RESPONSE_CODE, 0);
	//	CHECK(node != NULL, LBL_REVERSE);
	//	memcpy(ResponseCode, TlvTree_GetData(node), 2);
	//	return RET_OK;

	//LBL_REVERSE:
/*	if(TranID == TRAN_SALE){
	// Send reverse if necessary
	RetFun = HostMain_SendReverse(BatchName);
	//+ ISOMAIN01 @mamata Aug 14, 2014
	CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);
	}*/

	return RET_OK;

LBL_ERROR:
	//+ ISOMAIN01 @mamata Aug 14, 2014
	memcpy(ResponseCode, "ER", 2);
	node = TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, ResponseCode, 2);
	//- ISOMAIN01
	return RET_NOK;

LBL_COMM_ERROR:
	if(RetFun == 5)
	{
		strcpy((char*)ResponseCode, "CE");
		RetFun = -4;
	}
	else
	{
		//LLC_GetLastError(&LLC_Host,(int16*)&RetFun);
		LLC_GetLastError((int16*)&RetFun);
		switch(RetFun)
		{
			case LL_ERROR_TIMEOUT:
				strcpy((char*)ResponseCode, "TO");
				break;
			case LL_MODEM_ERROR_RESPONSE_NO_DIALTONE:
				strcpy((char*)ResponseCode, "NL");
				break;
			case LL_MODEM_ERROR_RESPONSE_BUSY:
				strcpy((char*)ResponseCode, "BU");
				break;
			default:
				strcpy((char*)ResponseCode, "CE");
				break;
		}
	}

	Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, (char*)ResponseCode);

	return RetFun;

LBL_ERROR_PACK:
	strcpy((char*)ResponseCode, "PA");
	Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, (char*)ResponseCode);
	return RET_NOK;

LBL_ERROR_UNPACK:
	strcpy((char*)ResponseCode, "UP");
	Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, (char*)ResponseCode);
	return RET_NOK;

//+ ISOMAIN01 @mamata Aug 14, 2014
LBL_ERROR_REVERSE:

	//+ REVERSE01 @mamata Dec 16, 2014
	strcpy((char*)ResponseCode, "TO");
	Tlv_SetTagValueString(Tree, TAG_HOST_RESPONSE_CODE, (char*)ResponseCode);
	//- REVERSE01

	return HOST_RET_REVERSE_ERROR;
//- ISOMAIN01

}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_Send_Request
 * Description:		Send request message to host
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK - request sent
 *  - RET_NOK - cannot send request message
 * Notes:
 */
int HostMain_Send_Request(TLV_TREE_NODE Tree)
{
#ifndef HOST_OFFLINE
	int RetFun;

	//RetFun = LLC_Send(&LLC_Host,TXBuffer, TXBufferLen);
	RetFun = LLC_Send(TXBuffer, TXBufferLen);
	CHECK(RetFun == LLC_RET_OK, LBL_ERROR);
#endif
	return RET_OK;

#ifndef HOST_OFFLINE
LBL_ERROR:
	return RET_NOK;
#endif
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_Receive_Response
 * Description:		receive response message from host
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK - response received
 *  - RET_NOK - response not received
 * Notes:
 * Configurado por eugenio
 */
int HostMain_Receive_Response(TLV_TREE_NODE Tree)
{
#ifndef HOST_OFFLINE
	int RetFun;
	LLCOMS_CONFIG LLC_Host_Temp;
	//eugenio
		memcpy(&LLC_Host_Temp, &LLC_Host, sizeof(LLCOMS_CONFIG));

	//RetFun = LLC_Receive(&LLC_Host,RXBuffer, &RXBufferLen,-1);
		//eugenio
	//LLC_Host_Temp.CurrenConfig->bAddHexLength = 1;
		//RetFun = LLC_Receive(&LLC_Host_Temp,RXBuffer, &RXBufferLen,-1);
		RetFun = LLC_Receive(RXBuffer, &RXBufferLen);
	CHECK(RetFun == LLC_RET_OK, LBL_ERROR);
#endif

	return RET_OK;

#ifndef HOST_OFFLINE
LBL_ERROR:
	return RET_NOK;
#endif
}

/* --------------------------------------------------------------------------
 * Function Name:	HostMain_GetResponseCodeInfo
 * Description:		Get response code information
 * Parameters:
 *  - uint8 *HostResponseCode - response code sent by host
 *  - RESPONSE_CODES *ResponseCode - where to put ResponseCode info
 * Return:
 *  - RET_OK
 * Notes:
 */
int HostMain_GetResponseCodeInfo(uint8 *HostResponseCode, RESPONSE_CODES *ResponseCode)
{
	uint16 Index = 0;
	bool found = FALSE;

	do
	{
		if(memcmp(HostResponseCode, HostMain_ResponseCodes[Index].ResponseCode, 2) == 0 )
		{
			memcpy((char*)ResponseCode, (char*)&HostMain_ResponseCodes[Index], sizeof(RESPONSE_CODES));
			found = TRUE;
		}

		if(memcmp("FF", HostMain_ResponseCodes[Index].ResponseCode, 2) == 0 )
		{
			memcpy((char*)ResponseCode, (char*)&HostMain_ResponseCodes[Index], sizeof(RESPONSE_CODES));
			found = TRUE;
		}
		Index++;
	}while(!found);

	//+ EMV02 @mamata Dec 16, 2014
	if(		memcmp(HostResponseCode, "D2", 2) == 0		// is a emv declined in second generate
		&&	memcmp(tMsgRx.aRespCode, "00", 2) != 0		// and host response code isn't 00
	  )
		ResponseCode->ClearReverse = TRUE;
	//- EMV02

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_GetResponseCodeInfoC2P
 * Description:		Get response code information
 * Parameters:
 *  - uint8 *HostResponseCode - response code sent by host
 *  - RESPONSE_CODES *ResponseCode - where to put ResponseCode info
 * Return:
 *  - RET_OK
 * Notes:
 */
int HostMain_GetResponseCodeInfoC2P(uint8 *HostResponseCode, RESPONSE_CODES *ResponseCode)
{
	uint16 Index = 0;
	bool found = FALSE;

	do
	{
		if(memcmp(HostResponseCode, HostMain_ResponseCodesC2P[Index].ResponseCode, 2) == 0 )
		{
			memcpy((char*)ResponseCode, (char*)&HostMain_ResponseCodesC2P[Index], sizeof(RESPONSE_CODES));
			found = TRUE;
		}

		if(memcmp("FF", HostMain_ResponseCodesC2P[Index].ResponseCode, 2) == 0 )
		{
			memcpy((char*)ResponseCode, (char*)&HostMain_ResponseCodesC2P[Index], sizeof(RESPONSE_CODES));
			found = TRUE;
		}
		Index++;
	}while(!found);

	//+ EMV02 @mamata Dec 16, 2014
	if(		memcmp(HostResponseCode, "D2", 2) == 0		// is a emv declined in second generate
		&&	memcmp(tMsgRx.aRespCode, "00", 2) != 0		// and host response code isn't 00
	  )
		ResponseCode->ClearReverse = TRUE;
	//- EMV02

	return RET_OK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SendReverse
 * Description:		Send a reverse if necessary
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - no reverse to send or reverse sent OK
 *  - RET_NOK - cannot send a reverse
 * Notes:
 */
int HostMain_SendReverse(uint8 *BatchName)
{
	TLV_TREE_NODE Tree_Reverse;
	TLV_TREE_NODE node;
	int RetFun;
	uint16 TranStatus;
	uint8 ResponseCode[2];
	TABLE_MERCHANT Merchant;
	// create tree to place last transaction
	Tree_Reverse = TlvTree_New(0);
	CHECK(Tree_Reverse != NULL, LBL_CANNOT_CREATE_REVERSE_TREE);

	// get last transaction
	RetFun = Batch_Get_Last_Transaction(Tree_Reverse, BatchName);
	CHECK(RetFun == RET_OK, LBL_NO_TRAN);

	// add batch number to tree

	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Reverse);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	RetFun = Tlv_SetTagValueInteger(Tree_Reverse, TAG_BATCH_NUMBER, Merchant.BatchNumber, LTAG_INT32);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// get transaction status
	node = TlvTree_Find(Tree_Reverse, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	// verify if necessary send reverse
	CHECK(TranStatus & FSTATUS_NEED_REVERSE, LBL_NO_REVERSE);

	// pack iso8583 message
	RetFun = ISO8583_Pack((char*)TXBuffer, &TXBufferLen, Tree_Reverse);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// send message to host
	RetFun = HostMain_Send_Request(Tree_Reverse);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// receive message from host
	RetFun = HostMain_Receive_Response(Tree_Reverse);
	//+ ISOMAIN01 @mamata Aug 14, 2014
	CHECK(RetFun == RET_OK, LBL_ERROR_RECEIVE);
	//- ISOMAIN01

	// unpack response iso8583 message
	RetFun = HostMain_UnpackMessage(Tree_Reverse);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// Get host response code
	node = TlvTree_Find(Tree_Reverse, TAG_HOST_RESPONSE_CODE, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy(ResponseCode, TlvTree_GetData(node), 2);

	// add batch name
	node = TlvTree_AddChild(Tree_Reverse, TAG_BATCH_FILE_NAME, BatchName, strlen((char*)BatchName));
	CHECK(node != NULL, LBL_ERROR);

	// Increment invoice number   cuando genera reverso JR02092020
	RetFun = Counters_Increment_Invoice();
	CHECK(RetFun > 0, LBL_CANNOT_INCREMENT_INVOICE);


	if(memcmp(ResponseCode, "00", 2) == 0)
	{
		// reverse processed, clear reverse
		RetFun = Batch_Delete_Transaction(Tree_Reverse);
		CHECK(RetFun == RET_OK, LBL_ERROR);
	}
	else
	{
		// ERROR: host din't process reverse
		TlvTree_Release(Tree_Reverse);
		return RET_NOK;
	}

	TlvTree_Release(Tree_Reverse);
	return RET_OK;

LBL_CANNOT_INCREMENT_INVOICE:
	return RET_NOK;

LBL_CANNOT_CREATE_REVERSE_TREE:
	// ERROR: cannot create tree for reverse
	return RET_NOK;

LBL_NO_TRAN:
	TlvTree_Release(Tree_Reverse);
	if(RetFun == BATCH_RET_EMPTY_FILE)
		// No transaction if batch file, continue with transaction
		return RET_OK;
	else
		// ERROR: cannot open last transaction
		return RET_NOK;

LBL_ERROR:
	// ERROR: tree error
	TlvTree_Release(Tree_Reverse);
	return RET_NOK;

LBL_NO_REVERSE:
	// last transaction is not a reverse, continue with transaction
	TlvTree_Release(Tree_Reverse);
	return RET_OK;

//+ ISOMAIN01 @mamata Aug 14, 2014
LBL_ERROR_RECEIVE:
	TlvTree_Release(Tree_Reverse);
	return HOST_RET_COMM_ERROR;
//- ISOMAIN01

}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SendAdvice
 * Description:		Sent a advice if necessary
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - no reverse to send or reverse sent OK
 *  - RET_NOK - cannot send a reverse
 * Notes:
 */
int HostMain_SendAdvice(uint8 *BatchName, bool Settle)
{
	TLV_TREE_NODE Tree_Advice = NULL;
	TLV_TREE_NODE node;
	BATCH BatchRecord;
	int RetFun;
	uint8 ResponseCode[2];
	uint16 TranStatus;
	TRAN_INFO TranInfo;
	uint16 TranOptions;
	uint32 STAN;
	TABLE_MERCHANT Merchant;



	memset((char*)&BatchRecord, 0, L_BATCH);
	RetFun = TM_FindFirst((char*)BatchName, &BatchRecord);
	CHECK(RetFun == TM_RET_OK, LBL_NO_TRANS);
	do
	{
		if(		BatchRecord.TranStatus & FSTATUS_NEED_ADJUST
			||	BatchRecord.TranStatus & FSTATUS_OFFLINE
		  )
		{
			// create tree to place last transaction
			Tree_Advice = TlvTree_New(0);
			CHECK(Tree_Advice != NULL, LBL_CANNOT_CREATE_TREE);

			RetFun = Batch_Get_Transaction_Data(Tree_Advice, &BatchRecord);
			CHECK(RetFun == BATCH_RET_OK, LBL_ADVICE_NO_PROCESS);


			RetFun = Get_TransactionMerchantData(&Merchant, Tree_Advice);
			RetFun = Tlv_SetTagValueInteger(Tree_Advice, TAG_BATCH_NUMBER, Merchant.BatchNumber, LTAG_INT32);
			CHECK(RetFun == BATCH_RET_OK, LBL_ADVICE_NO_PROCESS);

			// Set Adjust options
			if(BatchRecord.TranStatus & FSTATUS_NEED_ADJUST)
			{
				// set adjust options
				RetFun = GetTransactionInfo(TRAN_ADJUST, &TranInfo);
				node = TlvTree_Find(Tree_Advice, TAG_TRAN_OPTIOS, 0);
				CHECK(node != NULL, LBL_ADVICE_NO_PROCESS);
				RetFun = TlvTree_SetData(node, &TranInfo.TRAN_FLAG, LTAG_UINT16);
				CHECK(RetFun == TLV_TREE_OK, LBL_ADVICE_NO_PROCESS);
				TranOptions = TranInfo.TRAN_FLAG;
			}
			else if (BatchRecord.TranStatus & FSTATUS_OFFLINE)
			{
				// set adjust options
				RetFun = GetTransactionInfo(TRAN_OFFLINE, &TranInfo);
				node = TlvTree_Find(Tree_Advice, TAG_TRAN_OPTIOS, 0);
				CHECK(node != NULL, LBL_ADVICE_NO_PROCESS);
				RetFun = TlvTree_SetData(node, &TranInfo.TRAN_FLAG, LTAG_UINT16);
				CHECK(RetFun == TLV_TREE_OK, LBL_ADVICE_NO_PROCESS);
				TranOptions = TranInfo.TRAN_FLAG;

				STAN = Counters_Get_STAN();
				node = TlvTree_Find(Tree_Advice, TAG_STAN, 0);
				if(node != NULL)
				{
					RetFun = TlvTree_SetData(node, &STAN, LTAG_UINT32);
					CHECK(RetFun == TLV_TREE_OK, LBL_ADVICE_NO_PROCESS);
				}
				else
				{
					node = TlvTree_AddChild(Tree_Advice, TAG_STAN, &STAN, LTAG_UINT32);
					CHECK(node != NULL, LBL_ADVICE_NO_PROCESS);
				}
			}


			// release TAG_APPROVED
			node = TlvTree_Find(Tree_Advice, TAG_APPROVED, 0);
			if(node != NULL)
				TlvTree_Release(node);

			// release TAG_HOST_RESPONSE_CODE
			node = TlvTree_Find(Tree_Advice, TAG_HOST_RESPONSE_CODE, 0);
			if(node != NULL)
				TlvTree_Release(node);

			// release TAG_RESPONSE_CODE_TEXT
			node = TlvTree_Find(Tree_Advice, TAG_RESPONSE_CODE_TEXT, 0);
			if(node != NULL)
				TlvTree_Release(node);

			// add batch file name if necessary
			node = TlvTree_Find(Tree_Advice, TAG_BATCH_FILE_NAME, 0);
			if(node == NULL)
				TlvTree_AddChild(Tree_Advice, TAG_BATCH_FILE_NAME, BatchName, strlen((char*)BatchName));

			// pack iso8583 message
			RetFun = ISO8583_Pack((char*)TXBuffer, &TXBufferLen, Tree_Advice);
			CHECK(RetFun == RET_OK, LBL_ADVICE_NO_PROCESS);

			// send message to host
			RetFun = HostMain_Send_Request(Tree_Advice);
			CHECK(RetFun == RET_OK, LBL_ADVICE_NO_PROCESS);

			// receive message from host
			RetFun = HostMain_Receive_Response(Tree_Advice);
			CHECK(RetFun == RET_OK, LBL_ADVICE_NO_PROCESS);

			// unpack response iso8583 message
			RetFun = HostMain_UnpackMessage(Tree_Advice);
			CHECK(RetFun == RET_OK, LBL_ADVICE_NO_PROCESS);

			// get response code
			node = TlvTree_Find(Tree_Advice, TAG_HOST_RESPONSE_CODE, 0);
			CHECK(node != NULL, LBL_ADVICE_NO_PROCESS);
			memcpy(ResponseCode, TlvTree_GetData(node), 2);

			if(memcmp(ResponseCode, "00", 2) == 0)
			{
				// advice approved by host
				node = TlvTree_Find(Tree_Advice, TAG_TRAN_STATUS, 0);
				CHECK(node != NULL, LBL_ADVICE_NO_PROCESS);
				TranStatus = *(uint16*)TlvTree_GetData(node);

				if(BatchRecord.TranStatus & FSTATUS_NEED_ADJUST)
					// clear advice flag
					TranStatus &= ~FSTATUS_NEED_ADJUST;
				else if (BatchRecord.TranStatus & FSTATUS_OFFLINE)
					// clear offline flag
					TranStatus &= ~FSTATUS_OFFLINE;

				RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);
				RetFun = Tlv_SetTagValueInteger(Tree_Advice, TAG_TRAN_STATUS, TranStatus, LTAG_UINT16);
				CHECK(RetFun == TLV_TREE_OK, LBL_ADVICE_NO_PROCESS);

				// restore original transaciton options to tree
				node = TlvTree_Find(Tree_Advice, TAG_TRAN_OPTIOS, 0);
				CHECK(node != NULL, LBL_ADVICE_NO_PROCESS);
				RetFun = TlvTree_SetData(node, &BatchRecord.TranOptions, LTAG_UINT16);
				CHECK(RetFun == TLV_TREE_OK, LBL_ADVICE_NO_PROCESS);

				// update transaction in batch

				RetFun = Batch_Modify_Transaction(Tree_Advice);
				CHECK(RetFun == BATCH_RET_OK, LBL_ADVICE_NO_PROCESS);

				// release memory of Tree_Advice
				TlvTree_Release(Tree_Advice);
				Tree_Advice = NULL;
			}

			// if not a settle, break loop
			if(!Settle)
			{
				//+ NORECORDS @mamata 6/05/2016
				RetFun = TM_RET_NO_MORE_RECORDS;
				//- NORECORDS
				break;
			}
		}

		RetFun = TM_FindNext((char*)BatchName, &BatchRecord);
	}while(RetFun == TM_RET_OK);
	//+ NORECORDS @mamata 6/05/2016
	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ADVICE_NO_PROCESS);
	//- NORECORDS

	// release memory of Tree_Advice
	if(Tree_Advice != NULL)
	{
		TlvTree_Release(Tree_Advice);
		Tree_Advice = NULL;
	}

	return RET_OK;

LBL_NO_TRANS:
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		// no transaction in batch file
		return RET_OK;
	else
		// ERROR: cannot open batch file
		return RET_NOK;

LBL_CANNOT_CREATE_TREE:
	// ERROR: cannot create advice tree
	return RET_NOK;

LBL_ADVICE_NO_PROCESS:
	// cannot process advice
	TlvTree_Release(Tree_Advice);
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_Process_ResponseCode
 * Description:		Process response code
 * Parameters:
 *  TLV_TREE_NODE Tree - transaction data
 *  uint8 *ResponseCode - pointer to host response code
 *  uint16 TranOptions - transaction options
 *  uint16 TranStatus - transaction status
 * Return:
 *  RET_OK - response code processed OK
 *  RET_NOK - cannot process response code
 * Notes:
 */
int HostMain_Process_ResponseCode(TLV_TREE_NODE Tree, uint8 *ResponseCode, uint16 TranOptions, uint16 TranStatus)
{
	TLV_TREE_NODE node;
	RESPONSE_CODES RCInfo;
	int RetFun;
	TABLE_RANGE Range;
	uint32 RangID;
	uint16 TranID;

		node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);




	if((TranID!=TRAN_TEST)  &&  (TranID!=TRAN_LOGON))
		{
		node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);

		if(RangID == 8)
			HostMain_GetResponseCodeInfoC2P(ResponseCode, &RCInfo);
		else
			HostMain_GetResponseCodeInfo(ResponseCode, &RCInfo);
		}
	else {
		HostMain_GetResponseCodeInfo(ResponseCode, &RCInfo);
	}
	// void?
	if(TranStatus & FSTATUS_NEED_VOID)
	{
		if(RCInfo.Approved)
		{
			TLV_TREE_NODE OrignalTransTree = TlvTree_New(0);
			TLV_TREE_NODE node;
			int OriginalRecord;
			uint8 BatchName[LZ_MERCH_BATCH_NAME] = {0};

			RetFun = Tlv_GetTagValue(Tree, TAG_BATCH_FILE_INDEX, &OriginalRecord, NULL);
			CHECK(RetFun == RET_OK, LBL_CANNOT_ADD_TO_BATCH);

			node = TlvTree_Find(Tree, TAG_BATCH_FILE_NAME, 0);
			CHECK(node != NULL, LBL_CANNOT_ADD_TO_BATCH);
			memcpy((char*)BatchName, TlvTree_GetData(node), TlvTree_GetLength(node));

			Tlv_SetTagValue(OrignalTransTree, TAG_BATCH_FILE_NAME, BatchName, sizeof(BatchName));

			Tlv_SetTagValue(OrignalTransTree, TAG_BATCH_FILE_INDEX, &OriginalRecord, sizeof(OriginalRecord));

			RetFun = Batch_FindTransactionByRecordID(OrignalTransTree, OriginalRecord);
			CHECK(RetFun == RET_OK, LBL_CANNOT_ADD_TO_BATCH);

			TranStatus &= ~FSTATUS_NEED_VOID;
			//Add voided flag FSTATUS_VOIDED and update batch
			TranStatus |= FSTATUS_VOIDED;


			if(TranStatus & FSTATUS_NEED_ADJUST)
			TranStatus &= ~FSTATUS_NEED_ADJUST;
			//TranStatus &= ~FSTATUS_ADJUSTED;

			RetFun = Tlv_SetTagValueInteger(OrignalTransTree, TAG_TRAN_STATUS, TranStatus, LTAG_INT16);

			RetFun = Batch_Modify_Transaction(OrignalTransTree);
			CHECK(RetFun == RET_OK, LBL_CANNOT_ADD_TO_BATCH);

			TlvTree_Release(OrignalTransTree);

			// status of the void record
			TranStatus &= ~FSTATUS_VOIDED;
			RetFun = Tlv_SetTagValueInteger(Tree, TAG_TRAN_STATUS, TranStatus, LTAG_INT16);


			RetFun = Batch_Add_Transaction(Tree);
			CHECK(RetFun == BATCH_RET_OK, LBL_CANNOT_ADD_TO_BATCH);


			// delete record
			//RetFun = Batch_Delete_Transaction(Tree);
			//CHECK(RetFun == BATCH_RET_OK, LBL_CANNOT_DELETE_REVERSE);

			// add approved status
			uint8 Approved = 1;
			node = TlvTree_AddChild(Tree, TAG_APPROVED, &Approved, LTAG_UINT8);

			// add response code text
			node = TlvTree_AddChild(Tree, TAG_RESPONSE_CODE_TEXT, &RCInfo.MessageID, LTAG_UINT16);



			return RET_OK;
		}
	}

	// is stored a reverse?
	if(TranOptions & FTRAN_REVERSE && !(TranStatus & FSTATUS_OFFLINE))
	{
		// yes,
		// verify i needed delete reverse
		if(RCInfo.ClearReverse)
		{
			if(RCInfo.SaveBatch && TranOptions & FTRAN_BATCH)
			{
				// clear reverse flag and update record
				TranStatus &= ~FSTATUS_NEED_REVERSE;

				node = TlvTree_Find(Tree, TAG_TRAN_STATUS, 0);
				RetFun = TlvTree_SetData(node, &TranStatus, LTAG_UINT16);

				// update transaction in batch file  ///
				RetFun = Batch_Modify_Transaction(Tree);
				CHECK(RetFun == BATCH_RET_OK, LBL_CANNOT_ADD_TO_BATCH);

				// Increment invoice number
				RetFun = Counters_Increment_Invoice();
				CHECK(RetFun > 0, LBL_CANNOT_INCREMENT_INVOICE);

				RetFun = Get_TransactionRangeData(&Range, Tree);
				if((tTerminal.Comercio & RESTAURANT) && !(Range.Flags1 & RANGE_FLAG1_DEBIT))
				{
					RetFun = ADD_TRANSACCION_MESERO(Tree);
					CHECK(RetFun == BATCH_RET_OK, LBL_CANNOT_ADD_TO_BATCH);
				}
			}
			else
			{   //negada
				// delete record
				RetFun = Batch_Delete_Transaction(Tree);
				CHECK(RetFun == BATCH_RET_OK, LBL_CANNOT_DELETE_REVERSE);
				//agregrado javier R.
				RetFun = Counters_Increment_Invoice();
				CHECK(RetFun > 0, LBL_CANNOT_INCREMENT_INVOICE);
			}
		}
	}
	else
	{
		// no,
		// verify if necessary to store transaction in batch file
		//+ ISOMAIN01 @mamata Aug 14, 2014
		if(RCInfo.SaveBatch && TranOptions & FTRAN_BATCH)
		//- ISOMAIN01
		{
			// update transaction in batch file
			RetFun = Batch_Add_Transaction(Tree);
			CHECK(RetFun == BATCH_RET_OK, LBL_CANNOT_ADD_TO_BATCH);

			// Increment invoice number
			RetFun = Counters_Increment_Invoice();
			CHECK(RetFun > 0, LBL_CANNOT_INCREMENT_INVOICE);




		}
	}

	if(RCInfo.Approved)
	{
		uint8 Approved = 1;
		node = TlvTree_AddChild(Tree, TAG_APPROVED, &Approved, LTAG_UINT8);
	}

	node = TlvTree_AddChild(Tree, TAG_RESPONSE_CODE_TEXT, &RCInfo.MessageID, LTAG_UINT16);

	return RET_OK;

LBL_CANNOT_ADD_TO_BATCH:
	return RET_NOK;

LBL_CANNOT_INCREMENT_INVOICE:
	return RET_NOK;

LBL_CANNOT_DELETE_REVERSE:
	return RET_NOK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_ValidateResponse
 * Description:		Validate response from host
 * Parameters:
 *  bool EchoTest - is a echo test transaction
 * Return:
 *  RET_OK - response message is OK
 *  RET_NOK - invalid response message
 * Notes:
 */
//+ ECHOTEST @mamata Aug 15, 2014
int HostMain_ValidateResponse(bool EchoTest)
//d int HostMain_ValidateResponse(void)
//- ECHOTEST
{
	int TXMessageType;
	int RXMessageType;

	// validate message type
	TXMessageType = atoi((char*)tMsgTx.vbMsgType);
	RXMessageType = atoi((char*)tMsgRx.vbMsgType);
	CHECK((TXMessageType+10) == RXMessageType, LBL_ERROR);

	// validate STAN
	//+ ECHOTEST @mamata Aug 15, 2014
	if(!EchoTest){
	//- ECHOTEST
		CHECK(memcmp(tMsgTx.aNSU, tMsgRx.aNSU, 6) == 0, LBL_ERROR);

	// validate terminal id
	//CHECK(memcmp(tMsgTx.aTermId, tMsgRx.aTermId, 8) == 0, LBL_ERROR);
	}
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


int SaveKeyInFile(void)
{
	TABLE_HOST thost;
    int Ret;

	Ret = TM_FindFirst(TAB_HOST, &thost);
	if (Ret != TM_RET_OK)
		return RET_NOK;

//	memset(&thost.EncrypedWorkingKey2, 0, LZ_HOST_DESKEY);
//	memset(&thost.EncrypedWorkingKey1, 0, LZ_HOST_DESKEY);

	memcpy((uint8*)thost.EncrypedWorkingKey1,&tMsgRx.campo53rx, LZ_HOST_DESKEY);
	memcpy((uint8*)thost.EncrypedWorkingKey2,&tMsgRx.campo53rx[8], LZ_HOST_DESKEY);


	Ret = TM_ModifyRecord(TAB_HOST, &thost);
	if (Ret != TM_RET_OK)
		return RET_NOK;

	return RET_OK;


}


/* --------------------------------------------------------------------------
 * Function Name:	BuildEMVTags
 * Description:		Make field 55 buffer from tlvtree tags
 * Parameters:
 *  TLV_TREE_NODE 	pTLVT_Input
 * Return:
 *  RET_OK - response message is OK
 *  RET_NOK - invalid response message
 * Notes:
 */
//int BuildEMVTags( TLV_TREE_NODE pTLVT_Input, int bit127index)
//{
//	TLV_TREE_NODE TLVT_Node;
//	int index = 0;
//	unsigned int Tag;
//	char tmpbuff[50];
//	//char   chBuffer[500 + 1];
//	// buscar el primer tag en el tlvtree
//	TLVT_Node = TlvTree_GetFirstChild( pTLVT_Input );
//	CHECK( TLVT_Node != NULL, lbl_STOP1 );
//
//	//memset( chBuffer, 0, 500 );
//
//	while( TLVT_Node !=  NULL )
//	{
//		Tag = TlvTree_GetTag( TLVT_Node );
//
//		if(		Tag == EWL_EMV_TERMINAL_CAPABILITIES
//			|| 	Tag == EWL_EMV_CVM_RESULTS
//			|| 	Tag == EWL_EMV_TERMINAL_TYPE
//			|| 	Tag == EWL_EMV_ISSUER_APPLI_DATA
//			|| 	Tag == EWL_EMV_AIP
//			|| 	Tag == EWL_EMV_ATC
//			|| 	Tag == EWL_EMV_CRYPTOGRAM
//			|| 	Tag == EWL_EMV_CID
//			|| 	Tag == EWL_EMV_TVR
//			|| 	Tag == EWL_EMV_UNPREDICTABLE_NUMBER
//			|| 	Tag == EWL_EMV_AMOUNT_AUTH_NUM
//			|| 	Tag == EWL_EMV_AMOUNT_OTHER_NUM
//			|| 	Tag == EWL_EMV_TERMINAL_COUNTRY_CODE
//			|| 	Tag == EWL_EMV_TRANSACTION_CURRENCY_CODE
//			|| 	Tag == EWL_EMV_TRANSACTION_DATE
//			|| 	Tag == EWL_EMV_TRANSACTION_TYPE
//			|| 	Tag == EWL_EMV_DF_NAME
//			|| 	Tag == EWL_EMV_IFD_SERIAL_NUMBER
//			|| 	Tag == EWL_EMV_APPLI_VERSION_NUMBER_TERM
//			|| 	Tag == EWL_EMV_TRANSACTION_SEQUENCE_COUNTER
//			|| 	Tag == EWL_EMV_TRANSACTION_CATEGORY_CODE
//			|| 	Tag == EWL_EMV_APPLI_PAN_SEQUENCE_NUMBER
//			//paywave
//			||  Tag == EWL_PAYWAVE_ISSUER_SCRIPT_RESULT
//			||  Tag == EWL_PAYWAVE_FORM_FACTOR_INDICATOR
//			//paypass
//			||  Tag == EWL_PAYPASS_DSDOL
//			||  Tag == EWL_PAYPASS_THIRD_PARTY_DATA
//			//expresspay
//			||  Tag == EWL_EXPRESSPAY_TRANSACTION_CAPABILITIES
//		  )
//		{
//			// Add Tag
//			sprintf( tmpbuff, "%X", Tag );
//			Ascii_to_Hex((uint8 *)&tMsgTx.btField127Response[bit127index], (uint8*)tmpbuff, strlen( tmpbuff ) );
//			bit127index+= strlen( tmpbuff ) / 2;
//
//			// Add Length
//			sprintf( tmpbuff, "%02X", TlvTree_GetLength( TLVT_Node ) );
//			Ascii_to_Hex((uint8 *)&tMsgTx.btField127Response[bit127index], (uint8*)tmpbuff, strlen( tmpbuff ) );
//			bit127index++;
//
//			// Add value
//			memcpy ((uint8 *)&tMsgTx.btField127Response[bit127index], TlvTree_GetData( TLVT_Node ), TlvTree_GetLength( TLVT_Node ) );
//			bit127index += TlvTree_GetLength( TLVT_Node );
//		}
//
//		// Get Nex node
//		TLVT_Node = TlvTree_GetNext( TLVT_Node );
//	}
//
//	//tMsgTx.u16Bit55_Len = index;
//
//	return bit127index;
//
//lbl_STOP1:
//	return -1;
//}

int BuildEMVTags( TLV_TREE_NODE pTLVT_Input, int bit127index )
{
	TLV_TREE_NODE TLVT_Node;
	int index = 0,emvlength = 0;
	unsigned int Tag;
	 char tmpbuff[50];
	char   chBuffer[500 + 1];

	// buscar el primer tag en el tlvtree

    index = bit127index + 4;

	TLVT_Node = TlvTree_GetFirstChild( pTLVT_Input );
	CHECK( TLVT_Node != NULL, lbl_STOP1 );

	//memset( tMsgTx.aBit55, 0, 500 );

	while( TLVT_Node !=  NULL )
	{
		Tag = TlvTree_GetTag( TLVT_Node );

		if(	 	Tag == EWL_EMV_CID
		    ||	Tag == EWL_EMV_AMOUNT_AUTH_NUM
			|| 	Tag == EWL_EMV_AMOUNT_OTHER_NUM
			|| 	Tag == EWL_EMV_DF_NAME
			|| 	Tag == EWL_EMV_AIP
			|| 	Tag == EWL_EMV_ATC
			|| 	Tag == EWL_EMV_CRYPTOGRAM
			|| 	Tag == EWL_EMV_CVM_RESULTS
			|| 	Tag == EWL_EMV_IFD_SERIAL_NUMBER
			|| 	Tag == EWL_EMV_ISSUER_APPLI_DATA
			//||  Tag == EWL_PAYWAVE_ISSUER_SCRIPT_RESULT
			|| 	Tag == EWL_EMV_APPLI_VERSION_NUMBER_TERM
			||  Tag == EWL_EMV_TERMINAL_CAPABILITIES
			|| 	Tag == EWL_EMV_TERMINAL_COUNTRY_CODE
			|| 	Tag == EWL_EMV_TERMINAL_TYPE
			|| 	Tag == EWL_EMV_TVR
			|| 	Tag == EWL_EMV_TRANSACTION_CATEGORY_CODE
			|| 	Tag == EWL_EMV_TRANSACTION_CURRENCY_CODE
			|| 	Tag == EWL_EMV_TRANSACTION_DATE
			|| 	Tag == EWL_EMV_TRANSACTION_SEQUENCE_COUNTER
			|| 	Tag == EWL_EMV_TRANSACTION_TYPE
			|| 	Tag == EWL_EMV_UNPREDICTABLE_NUMBER
		  )
		{
			// Add Tag
			Telium_Sprintf( tmpbuff, "%04X", Tag );
			//memcpy (&chBuffer[index],tmpbuff, strlen( tmpbuff ));
			Ascii_to_Hex(&tMsgTx.btField127Response[index], (uint8*)tmpbuff, strlen( tmpbuff ) );
			//USQ_Aschex_alphanum(&tMsgTx.btField127Response[index],(uint8*)tmpbuff, strlen( tmpbuff ) );
			index+= strlen( tmpbuff ) / 2;

			// Add Length
			Telium_Sprintf( tmpbuff, "%02x", TlvTree_GetLength( TLVT_Node ) );
			//strcpy ((char *)&tMsgTx.btField127Response[index],tmpbuff);
			Ascii_to_Hex(&tMsgTx.btField127Response[index], (uint8*)tmpbuff, strlen( tmpbuff ) );
			index++;

			// Add value
			memcpy (&tMsgTx.btField127Response[index], TlvTree_GetData( TLVT_Node ), TlvTree_GetLength( TLVT_Node ) );
			index += TlvTree_GetLength( TLVT_Node );
		}

		// Get Nex node
		TLVT_Node = TlvTree_GetNext( TLVT_Node );
	}
    emvlength = index - bit127index ;
	Telium_Sprintf(chBuffer, "%04d", emvlength - 4);
	strcpy((char *)&tMsgTx.btField127Response+bit127index, chBuffer);
	tMsgTx.btField127Responselen = index ;

	return 0;

lbl_STOP1:
	return -1;
}

/** ------------------------------------------------------------------------------
 * Function Name:	GetEMVTagsFromHost
 * Author:			@DFVC
 * Date:		 	9/06/2014
 * Description:		Get EMV Tags from Host
 * Parameters:
 * none
 * Return:
 * - RET_OK, successful
 * - RET_NOK, No successful
 * Notes:
 */
int GetEMVTagsFromHost(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE TLVT_Node;
	DataElement ptElt2;
	unsigned char temp[500];
	int f55pos = 0;   //8 cambio para que comience desde la posicion 0 con la data
	uint8 TData = 1;



	// busar el primer tag en el tlvtree
	TLVT_Node = TlvTree_AddChild(Tree, TAG_EMV_HOST_TAGS, &TData, LTAG_UINT8);
	CHECK( TLVT_Node != NULL, LBL_ERROR );
   //MAGALLANES

	if (tMsgRx.btField127Responselen > 0)
	{
		while (f55pos < tMsgRx.btField127Responselen )
		{
			if (tMsgRx.btField127Response[f55pos] == 0x9f )
			{
				f55pos++;
				switch (tMsgRx.btField127Response[f55pos])
				{
				case 0x36:  // TAG_ATC
					f55pos++;
					ptElt2.length = tMsgRx.btField127Response[f55pos];
					f55pos++;
					ptElt2.tag = TAG_EMV_ATC;

					//+ HOST02 @mamata Dec 16, 2014
					ptElt2.ptValue = temp;
					//- HOST02

					memcpy(ptElt2.ptValue,&tMsgRx.btField127Response[f55pos],ptElt2.length);
					f55pos += ptElt2.length;
					TlvTree_AddChild(TLVT_Node, ptElt2.tag, ptElt2.ptValue, ptElt2.length);
					break;
				default:
					f55pos++;
					//length
					ptElt2.length = tMsgRx.btField127Response[f55pos];
					f55pos++;
					f55pos += ptElt2.length;
				break;
				}
			}
			else if (tMsgRx.btField127Response[f55pos] == 0x5f )
			{
				f55pos++;
				f55pos++;
				//length
				ptElt2.length = tMsgRx.btField127Response[f55pos];
				f55pos++;
				f55pos += ptElt2.length;

			}
			else if (tMsgRx.btField127Response[f55pos] == TAG_EMV_ISSUER_AUTHENTICATION_DATA
					|| tMsgRx.btField127Response[f55pos] == TAG_EMV_ISSUER_SCRIPT_TEMPLATE_1
					|| tMsgRx.btField127Response[f55pos] == TAG_EMV_ISSUER_SCRIPT_TEMPLATE_2)
			{
				ptElt2.tag = tMsgRx.btField127Response[f55pos];
				f55pos++;
				ptElt2.length = tMsgRx.btField127Response[f55pos];
				f55pos++;
				//memcpy(ptElt2.ptValue,&tMsgRx.aBit55[f55pos],ptElt2.length);
				memcpy(temp,&tMsgRx.btField127Response[f55pos],ptElt2.length);

				f55pos += ptElt2.length;
				ptElt2.ptValue = temp;

				TlvTree_AddChild(TLVT_Node, ptElt2.tag, ptElt2.ptValue, ptElt2.length);
			}
			else
			{
				//f55pos++;
				f55pos++;
				//length
				ptElt2.length = tMsgRx.btField127Response[f55pos];
				f55pos++;
				f55pos += ptElt2.length;
			}
		}
	}




	return RET_OK;

	LBL_ERROR:
		return RET_NOK;

}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SettleMerchant
 * Description:		Settle a specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - merchant to be settle
 * Return:
 *  - HOST_RET_OK - merchant settled OK
 *  - HOST_RET_TREE_ERROR - error in tlvtree
 *  - HOST_RET_PACK_ERROR - error packing request message
 *  - HOST_RET_UNPACK_ERROR - error unpacking response message
 *  - HOST_RET_COMM_ERROR - communication error
 *  - HOST_RET_NOT_APPROVED - host didn't approve settle
 *  - HOST_RET_TOTALS_ERROR - error in totals calculate
 *  - ( -1000 + xxx) batch upload error
 * Notes:
 */
int HostMain_SettleMerchant(TABLE_MERCHANT *Merchant)
{
	//+ TREEMEM @mamata 5/05/2016
	//d TLV_TREE_NODE Tree_Settle;
	TLV_TREE_NODE Tree_Settle = NULL;
	//- TREEMEM
	TLV_TREE_NODE node;
	int RetFun;
	uint16 TranID;
	TOTALS Totals;
	int RecordID;
	uint8 ResponseCode[2];
	uint32 STAN;
	DATE fecha;
	bool reporte=TRUE;
	bool EntryMode=FALSE;


	// initialize tree
	Tree_Settle = TlvTree_New(0);
	CHECK(Tree_Settle != NULL, LBL_TREE_ERROR);

	// add transaction id to tree
	TranID = TRAN_SETTLE;
	node = TlvTree_AddChild(Tree_Settle, TAG_TRAN_ID, &TranID, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add merchant id to tree
	RecordID = Merchant->Header.RecordID;
	node = TlvTree_AddChild(Tree_Settle, TAG_MERCHANT_ID, &RecordID, LTAG_INT32);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add host id to tree
	RecordID = Merchant->HostID;
	node = TlvTree_AddChild(Tree_Settle, TAG_HOST_ID, &RecordID, LTAG_INT32);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add batch number to tree
	RecordID = Merchant->BatchNumber;
	node = TlvTree_AddChild(Tree_Settle, TAG_BATCH_NUMBER, &RecordID, LTAG_INT32);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add transaction status to tree
	TranID = 0;
	node = TlvTree_AddChild(Tree_Settle, TAG_TRAN_STATUS, &TranID, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
    //date time
	Telium_Read_date(&fecha);
	RetFun = Tlv_SetTagValue(Tree_Settle, TAG_DATE_TIME, &fecha, sizeof(fecha));
	CHECK(RetFun == RET_OK, LBL_TREE_ERROR);
	// calculate totals
	//+ ISOMAIN01 @mamata Aug 14, 2014
	memset(&Totals, 0, sizeof(TOTALS));
	//- ISOMAIN01
	RetFun = Totals_MerchantCalculate(Merchant, &Totals, 0,reporte, EntryMode);
	CHECK(RetFun == RET_OK, LBL_TOTAL_ERROR);

	// add sales info to tree
	node = TlvTree_AddChild(Tree_Settle, TAG_SALES_COUNT, &Totals[Index_SALES].Count, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
	node = TlvTree_AddChild(Tree_Settle, TAG_SALES_AMOUNT, &Totals[Index_SALES].Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add refund info to tree
	node = TlvTree_AddChild(Tree_Settle, TAG_REFUND_COUNT, &Totals[Index_REFUNDS].Count, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
	node = TlvTree_AddChild(Tree_Settle, TAG_REFUND_AMOUNT, &Totals[Index_REFUNDS].Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add debit info to tree
	node = TlvTree_AddChild(Tree_Settle, TAG_DEBIT_COUNT, &Totals[Index_DEBTIS].Count, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
	node = TlvTree_AddChild(Tree_Settle, TAG_DEBIT_AMOUNT, &Totals[Index_DEBTIS].Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add refund debit info to tree
	node = TlvTree_AddChild(Tree_Settle, TAG_RDEBIT_COUNT, &Totals[Index_RDEBTIS].Count, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
	node = TlvTree_AddChild(Tree_Settle, TAG_RDEBIT_AMOUNT, &Totals[Index_RDEBTIS].Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_TREE_ERROR);

	//jr 26052021
	// add sales C2 Pinfo to tree
	/*node = TlvTree_AddChild(Tree_Settle, TAG_SALESC2P_COUNT, &Totals[Index_C2P].Count, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
	node = TlvTree_AddChild(Tree_Settle, TAG_SALESC2P_AMOUNT, &Totals[Index_C2P].Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// add refund  C2P info to tree
	node = TlvTree_AddChild(Tree_Settle, TAG_REFUNDC2P_COUNT, &Totals[Index_RC2P].Count, LTAG_UINT16);
	CHECK(node != NULL, LBL_TREE_ERROR);
	node = TlvTree_AddChild(Tree_Settle, TAG_REFUNDC2P_AMOUNT, &Totals[Index_RC2P].Amount, LTAG_AMOUNT);
	CHECK(node != NULL, LBL_TREE_ERROR);*/

	// connect to host
	RetFun = HostMain_Connect(Tree_Settle);
	CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

	// wait for connect to host is completed
	//+ HOST01 @mamata Aug 14, 2014
	//RetFun = LLC_WaitConnection(&LLC_Host,TRUE);
	RetFun = LLC_WaitConnection(TRUE);
	//- HOST01

	CHECK(RetFun == LLC_STATUS_CONNECTED, LBL_ERROR_COMM);

	// send reverse
	RetFun = HostMain_SendReverse((uint8*)Merchant->BatchName);
	CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);

	// send advice ajuste cierre solo si es restaurant
	RetFun = HostMain_SendAdvice((uint8*)Merchant->BatchName, TRUE);
	CHECK(RetFun == RET_OK, LBL_ERROR_ADVICE);

	//clear buffers
	memset(TXBuffer, 0, L_Buffer);
	memset(RXBuffer, 0, L_Buffer);
	TXBufferLen = 0;
	RXBufferLen = 0;

	// add current STAN to tree
	STAN = Counters_Get_STAN();
	node = TlvTree_AddChild(Tree_Settle, TAG_STAN, &STAN, LTAG_UINT32);
	CHECK(node != NULL, LBL_TREE_ERROR);

	// pack iso8583 message
	RetFun = ISO8583_Pack((char*)TXBuffer, &TXBufferLen, Tree_Settle);
	CHECK(RetFun == RET_OK, LBL_ERROR_PACK);

	// send message to host
	RetFun = HostMain_Send_Request(Tree_Settle);
	CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

	WriteLog(TXBuffer, TXBufferLen);

	// receive message from host
	RetFun = HostMain_Receive_Response(Tree_Settle);
	CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

	// unpack response iso8583 message
	RetFun = HostMain_UnpackMessage(Tree_Settle);
	CHECK(RetFun == RET_OK, LBL_ERROR_UNPACK);

	WriteLog(RXBuffer, RXBufferLen);

	RetFun = HostMain_UpdateDateTime(Tree_Settle);
	CHECK(RetFun == RET_OK, LBL_ERROR_UNPACK);
	// Get host response code
	node = TlvTree_Find(Tree_Settle, TAG_HOST_RESPONSE_CODE, 0);
	CHECK(node != NULL, LBL_TREE_ERROR);
	memcpy(ResponseCode, TlvTree_GetData(node), 2);

	// verify if necessary send batch upload
	if(memcmp(ResponseCode, "00", 2) == 0)
	{
		DATE SysDateTime;
		Merchant->BatchSettle = TRUE;
		//+ FIXDATETIME @mamata 5/05/2016
		//d RetFun = ReadDateBooster(&SysDateTime);
		RetFun = Telium_Read_date(&SysDateTime);
		//- FIXDATETIME
		memcpy((char*)&Merchant->BatchLastSettle, (char*)&SysDateTime, sizeof(DATE));
		RetFun = TM_ModifyRecord(TAB_MERCHANT, Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_TABLE_ERROR);
	}
	else if(memcmp(ResponseCode, "95", 2) == 0)
	{
		RetFun = HostMain_BatchUpload(Merchant);
		CHECK(RetFun == RET_OK, LBL_UPLOAD_ERROR);

		//clear buffers
		memset(TXBuffer, 0, L_Buffer);
		memset(RXBuffer, 0, L_Buffer);
		TXBufferLen = 0;
		RXBufferLen = 0;

		// batch upload required
		uint8 BatchUpload = 1;
		node = TlvTree_AddChild(Tree_Settle, TAG_BATCH_UPLOAD, &BatchUpload, LTAG_UINT8);
		CHECK(node != NULL, LBL_TREE_ERROR);

		// add current STAN to tree
		STAN = Counters_Get_STAN();
		node = TlvTree_Find(Tree_Settle, TAG_STAN, 0);
		CHECK(node != NULL, LBL_TREE_ERROR);
		RetFun = TlvTree_SetData(node, &STAN, LTAG_UINT32);
		CHECK(RetFun == TLV_TREE_OK, LBL_TREE_ERROR);

		// delete response code
		node = TlvTree_Find(Tree_Settle, TAG_HOST_RESPONSE_CODE, 0);
		if(node != NULL)
			TlvTree_Release(node);

		// pack iso8583 message
		RetFun = ISO8583_Pack((char*)TXBuffer, &TXBufferLen, Tree_Settle);
		CHECK(RetFun == RET_OK, LBL_ERROR_PACK);

		// send message to host
		RetFun = HostMain_Send_Request(Tree_Settle);
		CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

		// receive message from host
		RetFun = HostMain_Receive_Response(Tree_Settle);
		CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

		// unpack response iso8583 message
		RetFun = HostMain_UnpackMessage(Tree_Settle);
		CHECK(RetFun == RET_OK, LBL_ERROR_UNPACK);

		// Get host response code
		node = TlvTree_Find(Tree_Settle, TAG_HOST_RESPONSE_CODE, 0);
		CHECK(node != NULL, LBL_TREE_ERROR);
		memcpy(ResponseCode, TlvTree_GetData(node), 2);

		// check if host approved 0320
		CHECK((memcmp(ResponseCode, "00", 2) == 0), LBL_NOT_APPROVED);

		DATE SysDateTime;
		Merchant->BatchSettle = TRUE;
		//+ FIXDATETIME @mamata 5/05/2016
		//d RetFun = ReadDateBooster(&SysDateTime);
		RetFun = Telium_Read_date(&SysDateTime);
		//- FIXDATETIME
		memcpy((char*)&Merchant->BatchLastSettle, (char*)&SysDateTime, sizeof(DATE));
		RetFun = TM_ModifyRecord(TAB_MERCHANT, Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_TABLE_ERROR);
	}
	else
	{
		// settle not approved
		//LLC_EndConnection(&LLC_Host);
		LLC_EndConnection();
		TlvTree_Release(Tree_Settle);
		return HOST_RET_NOT_APPROVED;
	}

	// disconnect
	//LLC_EndConnection(&LLC_Host);
	LLC_EndConnection();
	TlvTree_Release(Tree_Settle);

	return HOST_RET_OK;

LBL_TREE_ERROR:
	if(Tree_Settle != NULL)
		TlvTree_Release(Tree_Settle);
	//LLC_EndConnection(&LLC_Host);
	LLC_EndConnection();
	return HOST_RET_TREE_ERROR;

LBL_ERROR_PACK:
	TlvTree_Release(Tree_Settle);
	//LLC_EndConnection(&LLC_Host);
	LLC_EndConnection();
	return HOST_RET_PACK_ERROR;

LBL_ERROR_UNPACK:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_UNPACK_ERROR;

LBL_ERROR_COMM:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_COMM_ERROR;

LBL_NOT_APPROVED:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_NOT_APPROVED;

LBL_TOTAL_ERROR:
	if(RetFun==5)
		UI_ShowMessage(mess_getReturnedMessage(MSG_SETTLE), mess_getReturnedMessage(MSG_TX_AJUSTE), NULL, GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);

	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_TOTALS_ERROR;

LBL_UPLOAD_ERROR:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return (-1000 + RetFun);

LBL_TABLE_ERROR:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_TABLE_ERROR;

LBL_ERROR_REVERSE:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_REVERSE_ERROR;

LBL_ERROR_ADVICE:
	TlvTree_Release(Tree_Settle);
	LLC_EndConnection();
	return HOST_RET_ADVICE_ERROR;

}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_BatchUpload
 * Description:		Perform batch upload of specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - Merchant to be upload to host
 * Return:
 *  - HOST_RET_OK - batch uploaded OK
 *  - HOST_RET_BATCH_ERROR - error with batch file
 *  - HOST_RET_TREE_ERROR - error with tlvtree
 *  - HOST_RET_PACK_ERROR - error packing request message
 *  - HOST_RET_UNPACK_ERROR - error unpacking response message
 *  - HOST_RET_COMM_ERROR - communication error
 *  - HOST_RET_NOT_APPROVED - host didn't approve a transaction in batch upload
 * Notes:
 */
int HostMain_BatchUpload(TABLE_MERCHANT *Merchant)
{
	TLV_TREE_NODE Tree_Upload = NULL;
	TLV_TREE_NODE node;
	int RetFun;
	BATCH BatchRecord;
	TRAN_INFO TranInfo;
	char ResponseCode[2];
	uint32 STAN;

	//+ ISOMAIN01 @mamata Aug 14, 2014
	int BatchRetFun;
	bool Exit;
	//- ISOMAIN01

	BatchRetFun = TM_FindFirst(Merchant->BatchName, &BatchRecord);
	CHECK(BatchRetFun == TM_RET_OK, LBL_BATCH_ERROR);

	do
	{
		if(BatchRecord.TranOptions & FTRAN_UPLOAD)
		{
			Tree_Upload = TlvTree_New(0);
			CHECK(Tree_Upload != NULL, LBL_TREE_ERROR);

			RetFun = Batch_Get_Transaction_Data(Tree_Upload, &BatchRecord);
			CHECK(RetFun == BATCH_RET_OK, LBL_TREE_ERROR);

			// batch upload required
			uint8 BatchUpload = 1;
			node = TlvTree_AddChild(Tree_Upload, TAG_BATCH_UPLOAD, &BatchUpload, LTAG_UINT8);
			CHECK(node != NULL, LBL_TREE_ERROR);

			// Set Adjust options
			memset((char*)&TranInfo, 0, sizeof(TRAN_INFO));
			TranInfo.TRAN_ID = TRAN_UPLOAD;
			node = TlvTree_Find(Tree_Upload, TAG_TRAN_OPTIOS, 0);
			CHECK(node != NULL, LBL_TREE_ERROR);
			RetFun = TlvTree_SetData(node, &TranInfo.TRAN_FLAG, LTAG_UINT16);
			CHECK(RetFun == TLV_TREE_OK, LBL_TREE_ERROR);

			// release TAG_APPROVED
			node = TlvTree_Find(Tree_Upload, TAG_APPROVED, 0);
			if(node != NULL)
				TlvTree_Release(node);

			// release TAG_HOST_RESPONSE_CODE
			node = TlvTree_Find(Tree_Upload, TAG_HOST_RESPONSE_CODE, 0);
			if(node != NULL)
				TlvTree_Release(node);

			// release TAG_RESPONSE_CODE_TEXT
			node = TlvTree_Find(Tree_Upload, TAG_RESPONSE_CODE_TEXT, 0);
			if(node != NULL)
				TlvTree_Release(node);

			//+ ISOMAIN01 @mamata Aug 14, 2014
			// find next transaction
			Exit = FALSE;
			do
			{
				BatchRetFun = TM_FindNext(Merchant->BatchName, &BatchRecord);
				if(BatchRetFun == TM_RET_OK)
				{
					if(!(BatchRecord.TranStatus & FSTATUS_VOIDED))
						Exit = TRUE;
				}
				else
					Exit = TRUE;
			}while(!Exit);

			if(BatchRetFun == TM_RET_NO_MORE_RECORDS)
				node = TlvTree_AddChild(Tree_Upload, TAG_BATCH_UPLOAD_LAST, NULL, 0);


			// get stan
			node = TlvTree_Find(Tree_Upload, TAG_STAN, 0);
			CHECK(node != NULL, LBL_TREE_ERROR);
			STAN = *(uint32*)TlvTree_GetData(node);
			// save stan in original stan tag
			node = TlvTree_AddChild(Tree_Upload, TAG_ORIGINAL_STAN, &STAN, LTAG_UINT32);
			CHECK(node != NULL, LBL_TREE_ERROR);
			//- ISOMAIN01

			// add current STAN to tree
			STAN = Counters_Get_STAN();

			//+ ISOMAIN01 @mamata Aug 14, 2014
			// save the current stan in tree
			node = TlvTree_Find(Tree_Upload, TAG_STAN, 0);
			CHECK(node != NULL, LBL_TREE_ERROR);
			RetFun = TlvTree_SetData(node, &STAN, LTAG_UINT32);
			CHECK(RetFun == TLV_TREE_OK, LBL_TREE_ERROR);
			//- ISOMAIN01

			//clear buffers
			memset(TXBuffer, 0, L_Buffer);
			memset(RXBuffer, 0, L_Buffer);
			TXBufferLen = 0;
			RXBufferLen = 0;

			// pack iso8583 message
			RetFun = ISO8583_Pack((char*)TXBuffer, &TXBufferLen, Tree_Upload);
			CHECK(RetFun == RET_OK, LBL_ERROR_PACK);

			// send message to host
			RetFun = HostMain_Send_Request(Tree_Upload);
			CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

			// receive message from host
			RetFun = HostMain_Receive_Response(Tree_Upload);
			CHECK(RetFun == RET_OK, LBL_ERROR_COMM);

			// unpack response iso8583 message
			RetFun = HostMain_UnpackMessage(Tree_Upload);
			CHECK(RetFun == RET_OK, LBL_ERROR_UNPACK);

			// Get host response code
			node = TlvTree_Find(Tree_Upload, TAG_HOST_RESPONSE_CODE, 0);
			CHECK(node != NULL, LBL_TREE_ERROR);
			memcpy(ResponseCode, TlvTree_GetData(node), 2);

			// check if host approved 0320
			CHECK((memcmp(ResponseCode, "00", 2) == 0), LBL_NOT_APPROVED);

			TlvTree_Release(Tree_Upload);

		}
		//+ ISOMAIN01 @mamata Aug 14, 2014
		else
		//- ISOMAIN01
			BatchRetFun = TM_FindNext(Merchant->BatchName, &BatchRecord);
	}while(BatchRetFun == TM_RET_OK);
	//+ NORECORDS @mamata 6/05/2016
	CHECK(BatchRetFun == TM_RET_NO_MORE_RECORDS, LBL_BATCH_ERROR);
	//- NORECORDS

	return HOST_RET_OK;

LBL_BATCH_ERROR:
	if(Tree_Upload != NULL)
		TlvTree_Release(Tree_Upload);
	return HOST_RET_BATCH_ERROR;

LBL_TREE_ERROR:
	if(Tree_Upload != NULL)
		TlvTree_Release(Tree_Upload);
	return HOST_RET_TREE_ERROR;

LBL_ERROR_PACK:
	TlvTree_Release(Tree_Upload);
	return HOST_RET_PACK_ERROR;

LBL_ERROR_UNPACK:
	TlvTree_Release(Tree_Upload);
	return HOST_RET_UNPACK_ERROR;

LBL_ERROR_COMM:
	TlvTree_Release(Tree_Upload);
	return HOST_RET_COMM_ERROR;

LBL_NOT_APPROVED:
	TlvTree_Release(Tree_Upload);
	return HOST_RET_NOT_APPROVED;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_PerfomSellte
 * Description:		Perform settle process
 * Parameters:
 *  - int32 MerchantID - merchant id (99999 for all merchants)
 *  - int32 MerchantGroupID - Merchant group id filter (99999 for all merchant group)
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int HostMain_PerfomSellte(int32 MerchantID, int32 MerchantGroupID)
{
	TABLE_MERCHANT Merchant;
	TABLE_HEADER TableHeader;
	int RetFun;
	bool ReportPrinted = FALSE;
	bool PrintReport;
	int SettleTry;
	int Reporte=1;

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_SETTLE), UI_ALIGN_CENTER);

	if(MerchantID != 99999)
	{
		// find merchant info
		RetFun = TM_FindRecord(TAB_MERCHANT, &Merchant, MerchantID);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// get batch header
		RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);

		// validate if batch if not empty
		CHECK(TableHeader.NumberRecords > 0, LBL_BATCH_EMPTY);

		// validate if batch is not already settled
		CHECK(!Merchant.BatchSettle, LBL_BATCH_EMPTY);

		UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
		UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
		UI_ShowProcessScreen("ESPERE POR FAVOR");

		// settle current merchant
		SettleTry = 0;
		do
		{
			SettleTry++;
			RetFun = HostMain_SettleMerchant(&Merchant);
		}while(RetFun == HOST_RET_COMM_ERROR && SettleTry <= SETTLE_MAX_TRIES);

		if(RetFun == HOST_RET_OK)
		{
			//FLAG NEGADA SE DESHABILITA
			RetFun = Get_TransactionMerchantData(&Merchant, Tree_Tran);
			CHECK(RetFun == RET_OK, LBL_ERROR);
			Merchant.Flags1 &= ~MERCH_FLAG1_TX_NEGADA;
			TM_ModifyRecord(TAB_MERCHANT, &Merchant);

			// settle OK
			UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));
			Report_MerchantTotals(&Merchant, TRUE,Reporte);
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_SETTLE), mess_getReturnedMessage(MSG_SETTLE_SUCESS), GL_ICON_NONE, GL_BUTTON_ALL, tConf.TimeOut);
		}
		else
		{
			// settle error
			UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_SETTLE), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_ERROR, GL_BUTTON_ALL, tConf.TimeOut);
		}

		ReportPrinted = TRUE;
	}
	else
	{
		// get first merchant
		RetFun = TM_FindFirst(TAB_MERCHANT, &Merchant);
		CHECK(RetFun == TM_RET_OK, LBL_ERROR);
		do
		{
			PrintReport = TRUE;

			if(MerchantGroupID != 99999)
				if(Merchant.MerchantGroupID != MerchantGroupID)
					PrintReport = FALSE;

			if(PrintReport)
			{
				// get batch header
				RetFun = TM_GetTableHeader(Merchant.BatchName, &TableHeader);
				CHECK(RetFun == TM_RET_OK, LBL_ERROR);

				// validate if batch if not empty
				if(TableHeader.NumberRecords > 0 && !Merchant.BatchSettle)
				{
					UI_SetLine(UI_LINE1, mess_getReturnedMessage(MSG_MERCHANT), UI_ALIGN_CENTER);
					UI_SetLine(UI_LINE2, Merchant.Name, UI_ALIGN_CENTER);
					UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PLEASE_WAIT));

					// settle current merchant
					SettleTry = 0;
					do
					{
						SettleTry++;
						RetFun = HostMain_SettleMerchant(&Merchant);

						//FLAG NEGADA SE DESHABILITA
						Merchant.Flags1 &= ~MERCH_FLAG1_TX_NEGADA;
						TM_ModifyRecord(TAB_MERCHANT, &Merchant);

					}while(RetFun == HOST_RET_COMM_ERROR && SettleTry <= SETTLE_MAX_TRIES);

					if(RetFun == HOST_RET_OK)
					{
						// settle OK
						UI_ShowProcessScreen(mess_getReturnedMessage(MSG_PRINTING));
						Report_MerchantTotals(&Merchant, TRUE, Reporte);
					}
					else
					{
						// settle error
						UI_ShowMessage(NULL, mess_getReturnedMessage(MSG_ERROR_SETTLE), mess_getReturnedMessage(MSG_TRY_AGAIN), GL_ICON_ERROR, GL_BUTTON_ALL, 5);
					}

					ReportPrinted = TRUE;
				}
			}
			// get next merchant
			RetFun = TM_FindNext(TAB_MERCHANT, &Merchant);
		}while(RetFun == TM_RET_OK);
		//+ NORECORDS @mamata 6/05/2016
		CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
		//- NORECORDS
	}

	CHECK(ReportPrinted, LBL_BATCH_EMPTY);

	return RET_OK;

LBL_ERROR:
	return RET_NOK;

LBL_BATCH_EMPTY:
	UI_ShowMessage(mess_getReturnedMessage(MSG_SETTLE), mess_getReturnedMessage(MSG_EMPTY_BATCH), NULL, GL_ICON_INFORMATION, GL_BUTTON_ALL, tConf.TimeOut);
	return RET_OK;
}


//+ ISOMAIN01 @mamata Aug 14, 2014
/* --------------------------------------------------------------------------
 * Function Name:	HostMain_UnpackBit63
 * Description:		unpack field from Bit63
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 * Notes:
 */
int HostMain_UnpackBit63(TLV_TREE_NODE Tree)
{
	int iCont;
	char sLength[5];
	int iLength;
	char sTag[2];

	if(tMsgRx.u16Bit63_Len>0)
	{
		iCont=0;
		while(iCont<tMsgRx.u16Bit63_Len)
		{
			// Obtener la longitud del campo
			ISO_hex_to_asc((char*)&tMsgRx.aBit63[iCont], sLength, 2);
			iLength=atoi(sLength); //Longitud del subcampo
			iCont+=2;

			// Obtener el nombre del tag
			memcpy(sTag, &tMsgRx.aBit63[iCont],2);
			iCont+=2;

			if( memcmp(sTag, "C0", 2) == 0)
			{
				// TAG C0
				iCont+= iLength-2;
			}
			else if(memcmp(sTag, "74", 2) == 0)
			{
				// TAG 74
				iCont+= iLength-2;
			}
			else
			{
				iCont+= iLength-2;
			}
		}
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	SetBit60_Upload
 * Description:		Set bit 30 for batch upload
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SetBit60_Upload(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint16 TranID;
	uint32 STAN;
	int TotalItens;
	int i;

	// get tran id
	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranID = *(uint16*)TlvTree_GetData(node);

	// add original message type
	TotalItens = sizeof(lista)/sizeof(lista[0]);
	for (i = 0; i < TotalItens; i++)
		if(lista[i].TransacId == TranID)
			memcpy(tMsgTx.aBit60, lista[i].msgType, 4);

	// get stan
	node = TlvTree_Find(Tree, TAG_ORIGINAL_STAN, 0);
	CHECK(node != NULL, LBL_ERROR);
	STAN = *(uint32*)TlvTree_GetData(node);

	// add original stan
	RetFun = Binasc(&tMsgTx.aBit60[4], STAN, 6);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// fill with spaces
	memset(&tMsgTx.aBit60[10], ' ', 12);

	tMsgTx.u16Bit60_Len = 22;

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	SetBit63
 * Description:		Set bit 63
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SetBit63(TLV_TREE_NODE Tree)
{
	int Bit63Index = 0;
	int RetFun;

	RetFun = SetBit63_S39_TAX(Tree, &Bit63Index);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	tMsgTx.u16Bit63_Len = Bit63Index;
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	SetBit63_S39_TAX
 * Description:		Set subtag 39 of bit 63 (tax amount)
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 *  - int *Bit63Index - current index of bit63
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SetBit63_S39_TAX(TLV_TREE_NODE Tree, int *Bit63Index)
{
	TABLE_MERCHANT Merchant;
	TLV_TREE_NODE node;
	int RetFun;
	amount TaxAmount = 0;

	// get merchant data
	RetFun = Get_TransactionMerchantData(&Merchant, Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// verify if tax is enable
	CHECK(Merchant.Flags1 & MERCH_FLAG1_TAX, LBL_RET_OK);

	node = TlvTree_Find(Tree, TAG_TAX_AMOUNT, 0);
	if(node != NULL)
		TaxAmount = *(amount*)TlvTree_GetData(node);

	// verify if is necesarry add suttag to bit63
	if(		TaxAmount > 0				// tax exists
		||	Merchant.TAX_type == 2 		// is configured forced tax
	  )
	{
		memcpy((char*)&tMsgTx.aBit63[*Bit63Index], "\x00\x14\x33\x39", 4);
		*Bit63Index += 4;
		Uint64_to_Ascii(TaxAmount, (char*)&tMsgTx.aBit63[*Bit63Index], _FORMAT_RIGHT, 12, '0');
		*Bit63Index += 12;
	}

LBL_RET_OK:
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	HostMain_UpdateDateTime
 * Description:		Update host date time in tree and system
 * Parameters:
 *  - TLV_TREE_NODE Tree
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int HostMain_UpdateDateTime(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	DATE HostDateTime;
	TABLE_HOST Host;
	int RetFun,ValidateMes,ValidateHora,ValidateDay,Validateyear;

	if(tMsgRx.aBit60[0] == 0)
		return RET_OK;

	//+ FIXDATETIME @mamata 5/05/2016
	//d RetFun = ReadDateBooster(&HostDateTime);
	RetFun = Telium_Read_date(&HostDateTime);
	//- FIXDATETIME
	CHECK(RetFun == RET_OK, LBL_ERROR);

	ValidateMes = memcmp(HostDateTime.day, tMsgRx.aBit60,2);
	ValidateDay = memcmp(HostDateTime.month, &tMsgRx.aBit60[2], 2);
	ValidateHora = memcmp(HostDateTime.hour,  tMsgRx.aTmLocal,2);
	Validateyear = memcmp(HostDateTime.year,  &tMsgRx.aBit60[6],2);

	if(ValidateMes == 0 && ValidateDay == 0 && ValidateHora == 0 && Validateyear == 0){
		goto LBL_ACTUALIZADA;
	}


	memcpy(HostDateTime.day, tMsgRx.aBit60, 2);
	memcpy(HostDateTime.month, &tMsgRx.aBit60[2], 2);
	memcpy(HostDateTime.hour, tMsgRx.aTmLocal, 6);
	memcpy(HostDateTime.year,  &tMsgRx.aBit60[6],2);

	node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
	if(node == NULL)
		node = TlvTree_AddChild(Tree, TAG_DATE_TIME, &HostDateTime, sizeof(DATE));
	else
		RetFun = TlvTree_SetData(node, &HostDateTime, sizeof(DATE));

	RetFun = Get_TransactionHostData(&Host, Tree);
//	if(Host.Flags1 & HOST_FLAG1_NO_UPDATE_HOST_TIME)
//		return RET_OK;

	Telium_Write_date(&HostDateTime);
	LBL_ACTUALIZADA:
	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}


/* --------------------------------------------------------------------------
 * Function Name:	SetBit60_Adjust
 * Description:		Set bit60 for Adjust, original total amount
 * Parameters:
 *  - TLV_TREE_NODE Tree
 * Return:
 *  - RET_OK
 *  - RET_NOK
 * Notes:
 */
int SetBit60_Adjust(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	amount Total = 0;

	node = TlvTree_Find(Tree, TAG_ORIGINAL_TOTAL, 0);
	CHECK(node != NULL, LBL_ERROR);

	Total = *(amount*)TlvTree_GetData(node);

	Uint64_to_Ascii(Total, (char*)tMsgTx.aBit60, _FORMAT_RIGHT, 12, '0');
	tMsgTx.u16Bit60_Len = 12;

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
//- ISOMAIN01

//+ CLESS @mamata Dec 11, 2014
/* --------------------------------------------------------------------------
 * Function Name:	HostMain_SetOfflineDeclined1G
 * Description:		Set offline declined in fist generate
 * Parameters:
 *  - TLV_TREE_NODE Tree - transaction data
 * Return:
 *  - RET_OK - transaction is set as offline declined
 *  - RET_NOK - cannot set transaciton as offline declined
 * Notes:
 */
int HostMain_SetOfflineDeclined1G(TLV_TREE_NODE Tree)
{
	char RC[2];
	TLV_TREE_NODE node;
	int RetFun;
	memcpy(RC, "D1", 2);
	uint32 Invoice = 0;
	DATE SysDateTime;
	char AuthNumber[7];

	// set response code
	node = TlvTree_AddChild(Tree, TAG_HOST_RESPONSE_CODE, RC, 2);
	CHECK(node != NULL, LBL_ERROR);

	// add invoice to tree
	Invoice = Counters_Get_Invoice();
	CHECK(Invoice > 0, LBL_ERROR);
	node = TlvTree_AddChild(Tree, TAG_INVOICE, &Invoice, LTAG_UINT32);
	CHECK(node != NULL, LBL_ERROR);

	// add system date and time
	//+ FIXDATETIME @mamata 5/05/2016
	//d RetFun = ReadDateBooster(&SysDateTime);
	RetFun = Telium_Read_date(&SysDateTime);
	//- FIXDATETIME
	CHECK(RetFun == RET_OK, LBL_ERROR);
	node = TlvTree_AddChild(Tree, TAG_DATE_TIME, &SysDateTime, sizeof(DATE));
	CHECK(node != NULL, LBL_ERROR);

	// add auth number
	node = TlvTree_Find(Tree, TAG_HOST_AUTH_NUMBER, 0);
	if(node == NULL)
	{
		memset(AuthNumber, 0, 7);
		memcpy(AuthNumber, "--FL--", 6);
		node = TlvTree_AddChild(Tree, TAG_HOST_AUTH_NUMBER, AuthNumber, 6);
		CHECK(node != NULL, LBL_ERROR);
	}

	return RET_OK;

LBL_ERROR:
	return RET_NOK;
}

//EUGENIO

int SetBit70(TLV_TREE_NODE Tree)
{
	uint16 TranID;
	TLV_TREE_NODE node;


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_ERROR);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);
	switch(TranID)
		{
		case TRAN_TEST:
			memcpy(tMsgTx.msgid, "301", 3);
			break;

		case TRAN_LOGON:
			memcpy(tMsgTx.msgid, "101", 3);
	    break;

		default:
			return RET_NOK;
	}
	//memcpy((char*)tMsgTx.msgid,"301",3);


return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
int SetBit75(TLV_TREE_NODE Tree)
{

	TLV_TREE_NODE node;
	uint16 Count;

	node = TlvTree_Find(Tree, TAG_REFUND_COUNT, 0);
	CHECK(node != NULL, LBL_ERROR);

	Count = *(uint16*)TlvTree_GetData(node);
	Binasc(tMsgTx.nreversos, Count, 10);


return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
int SetBit76(TLV_TREE_NODE Tree)
{

	TLV_TREE_NODE node;
	uint16 Count;

	node = TlvTree_Find(Tree, TAG_SALES_COUNT, 0);
	CHECK(node != NULL, LBL_ERROR);

	Count = *(uint16*)TlvTree_GetData(node);
	Binasc(tMsgTx.nventas, Count, 10);


return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
int SetBit87(TLV_TREE_NODE Tree)
{

	TLV_TREE_NODE node;
	amount Amount;

	node = TlvTree_Find(Tree, TAG_REFUND_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);

	Amount = *(amount*)TlvTree_GetData(node);
	Uint64_to_Ascii(Amount, (char*)&tMsgTx.mreversos, _FORMAT_RIGHT, 16, '0');


return RET_OK;

LBL_ERROR:
	return RET_NOK;
}
int SetBit88(TLV_TREE_NODE Tree)
{

	TLV_TREE_NODE node;
	amount Amount;

	node = TlvTree_Find(Tree, TAG_SALES_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);

	Amount = *(amount*)TlvTree_GetData(node);
	Uint64_to_Ascii(Amount, (char*)&tMsgTx.mventas, _FORMAT_RIGHT, 16, '0');


return RET_OK;
LBL_ERROR:
	return RET_NOK;
}

int SetBit95(TLV_TREE_NODE Tree)
{

	TLV_TREE_NODE node;
	amount totalAmount;
	amount tempAmount;
	int ConvertLen;
	char ConvertBuffer[13];


	memset(ConvertBuffer, 0, 13);
	totalAmount = 0;

	node = TlvTree_Find(Tree, TAG_BASE_AMOUNT, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND)
	memcpy((char*)&tempAmount, TlvTree_GetData(node), LTAG_AMOUNT);
	totalAmount+=tempAmount;

	node = TlvTree_Find(Tree, TAG_TAX_AMOUNT, 0);
	if (node!=NULL)
	{
		memcpy((char*)&tempAmount, TlvTree_GetData(node), LTAG_AMOUNT);
		totalAmount+=tempAmount;
	}

	node = TlvTree_Find(Tree, TAG_TIP_AMOUNT, 0);
	if (node!=NULL)
	{
		memcpy((char*)&tempAmount, TlvTree_GetData(node), LTAG_AMOUNT);
		totalAmount+=tempAmount;
	}

	ConvertLen = Uint64_to_Ascii(totalAmount, ConvertBuffer, _FORMAT_RIGHT, 12, 0x30);
	CHECK(ConvertLen != 0, LBL_Amount_Error)

	//memcpy(tMsgTx.aValor, ConvertBuffer, ConvertLen);
	 memcpy(tMsgTx.Bit95,ConvertBuffer,ConvertLen);

	  memset(&tMsgTx.Bit95[ConvertLen], '0', 12);
	  ConvertLen += 12;

	  tMsgTx.Bit95[ConvertLen++] = SEPARADOR_CAMPO_95;

	  memset(&tMsgTx.Bit95[ConvertLen], '0', ZERO_FILLED_FIELD_95_LEN);
	  ConvertLen += 8;

	  tMsgTx.Bit95[ConvertLen++] = SEPARADOR_CAMPO_95;

	  memset(&tMsgTx.Bit95[ConvertLen], '0', ZERO_FILLED_FIELD_95_LEN);
	  ConvertLen += 8;

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;

	LBL_Amount_Error:
		return RET_NOK;




}

int SetBit97(TLV_TREE_NODE Tree)
{

	TLV_TREE_NODE node;
	amount Amountv;
	amount Amountr;
	amount total;

	node = TlvTree_Find(Tree, TAG_SALES_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Amountv = *(amount*)TlvTree_GetData(node);

	node = TlvTree_Find(Tree, TAG_REFUND_AMOUNT, 0);
	CHECK(node != NULL, LBL_ERROR);
	Amountr = *(amount*)TlvTree_GetData(node);

	total= Amountv-Amountr;

	  if (total >= 0.0)
		 tMsgTx.mtotall[0] = 'C';
	  else
		 tMsgTx.mtotall[0] = 'D';

	Uint64_to_Ascii(total,(char*)&tMsgTx.mtotall[1], _FORMAT_RIGHT, 16, '0');


return RET_OK;
LBL_ERROR:
	return RET_NOK;
}

//eugenio
int SetBit90(TLV_TREE_NODE Tree)
{   TLV_TREE_NODE node;
	DATE date;
	//uint8 time[10+1];
	//char tmsg[4 + 1];
	uint8 STAN;
	int lnSTAN = 6;
	int inLen = 0;
	unsigned char cSTAN[lnSTAN+1];
	int RetFun;
	//char   chBuffer[50 + 1];

	//memset(tmsg, 0, 4 + 1);
	//memcpy(tMsgTx.Bit90, "0200", 4);
	inLen += Telium_Sprintf((char*)&tMsgTx.Bit90[inLen], "0%03d",200);

	node = TlvTree_Find(Tree, TAG_STAN, 0);
	memcpy((char*)&STAN, TlvTree_GetData(node), LTAG_UINT32);
	RetFun = Binasc(cSTAN, STAN, lnSTAN);
	memcpy(tMsgTx.Bit90+inLen, tMsgTx.aNSU, 6);
    inLen +=6;

	//node = TlvTree_Find(Tree, TAG_DATE_TIME, 0);
    node = TlvTree_Find(Tree, TAG_TIME_REVERSE, 0);
	CHECK(node != NULL, LBL_ERROR);
    memcpy((char*)&date, TlvTree_GetData(node), LTAG_DATE_TIME);
    memcpy(tMsgTx.Bit90+inLen, date.month, 2);
    inLen +=2;
    memcpy(tMsgTx.Bit90+inLen, date.day, 2);
    inLen +=2;
    memcpy(tMsgTx.Bit90+inLen, date.hour, 2);
    inLen +=2;
    memcpy(tMsgTx.Bit90+inLen, date.minute, 2);
    inLen +=2;
    memcpy(tMsgTx.Bit90+inLen, date.second, 2);
    inLen +=2;

//Telium_Sprintf(chBuffer,"%06lu%2x%2x%2x%2x%2x",tMsgTx.aNSU,date.month,date.day,date.hour,date.minute,date.second);

memset(&tMsgTx.Bit90[inLen],'0',22);



	return RET_OK;
LBL_ERROR:
	return RET_NOK;
}



/** ------------------------------------------------------------------------------
 * Function Name:	SetBit122
 * Author:			@JR
 * Date:		 	09/06/2021
 * Description:		Buid TOKEN C2P form ISO8583 Message
 * Parameters:
 *  - Tree, TLV_TREE_NODE of the transaction
 * Return:
 *  - RET_OK, successful
 *  - RET_NOK, No successful
 * Notes:
 */
int SetBit122(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	char PinBlock[8+1];
	char Token[8+1];
	int RetFun;
	uint16 TranID;


	node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);


	node = TlvTree_Find(Tree, TAG_TOKENBLOCK, 0);
	if(node == NULL)
		return RET_OK;

	memcpy((char*)&PinBlock, TlvTree_GetData(node), TlvTree_GetLength(node));


	RetFun = Format_String_Lenght(PinBlock, Token, _FORMAT_LEFT, 8, ' ');

	//tMsgTx.u16PinBlock_Len = 8;
	memcpy(tMsgTx.tokenBLock, Token, 8);


	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}



//EUGENIO MODIFICADA CAMPO 123
int SetBit123(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	uint8 Entry_Mode;
	uint8 Fallback = 0;
	uint16 TranID;
	uint32 RangID;

		node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);

		node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);


		if(((TranID == TRAN_VOID) || (TranID == TRAN_REVERSE)) && (RangID == 8))
	   {
		return RET_OK;
		}


	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);
	CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
	memcpy((char*)&Entry_Mode, TlvTree_GetData(node), LTAG_UINT8);

	switch (Entry_Mode) {
		case ENTRY_MODE_NONE:
		case ENTRY_MODE_MANUAL:
			strcpy((char*)tMsgTx.aEntryMode, "510101114144101");
			break;
		case ENTRY_MODE_SWIPE:
			node = TlvTree_Find(Tree, TAG_FALLBACK, 0);
			if(node != NULL)
				Fallback = *(uint8*)TlvTree_GetData(node);

			if(Fallback == 0)
				strcpy((char*)tMsgTx.aEntryMode, "210101214144101");
			else
				strcpy((char*)tMsgTx.aEntryMode, "510101214144101");

			break;
		case ENTRY_MODE_CHIP:
			strcpy((char*)tMsgTx.aEntryMode, "510101511344101");
			break;
		case ENTRY_MODE_CLESS_CHIP :
			strcpy((char*)tMsgTx.aEntryMode, "510101711344101");
			break;
		//+ CLESS @mamata Dec 12, 2014
		default:
			memcpy(tMsgTx.aEntryMode, "000", 3);
			break;
	}

	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;

}
//- ISOMAIN01
//EUGENIO
int SetBit127(TLV_TREE_NODE Tree){
	//int pbindex= 0;
	uint8  btBitmap[8];
	//TABLE_MERCHANT 		tMerchAux;
	TLV_TREE_NODE node;

	char FullSerialNumberRaw[8+1];
	char chModo[2 + 1];
	char   chBuffer[50 + 1];
	int  inLen = 0,EMVlen;
    char EntryMode;
	uint32 BATCH_NUMBER;
//	char TmpVersion[10];
	uint16 TranID;
	uint32 RangID;

		node = TlvTree_Find(Tree, TAG_TRAN_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&TranID, TlvTree_GetData(node), LTAG_UINT16);



		if((TranID != TRAN_LOGON) && (TranID != TRAN_TEST) && (TranID != TRAN_SETTLE))
		{
		node = TlvTree_Find(Tree, TAG_RANGE_ID, 0);
		CHECK(node != NULL, LBL_TAG_TRAN_ID_NOTFOUND);
		memcpy((char*)&RangID, TlvTree_GetData(node), LTAG_UINT32);
		}

		if(((TranID == TRAN_VOID) || (TranID == TRAN_REVERSE)) && (RangID == 8))
	   {
		return RET_OK;
		}


	memset(btBitmap, 0, 8);

	node = TlvTree_Find(Tree,TAG_BATCH_NUMBER, 0);
	memcpy((char *)&BATCH_NUMBER, TlvTree_GetData(node), LTAG_UINT32);

	btBitmap[1] |= BIT_9;


	if( TranID==TRAN_VOID)
	node = TlvTree_Find(Tree, TAG_ENTRY_ORIGINAL, 0);
	else
	node = TlvTree_Find(Tree, TAG_ENTRY_MODE, 0);

	if(node != NULL)
		memcpy((char*)&EntryMode, TlvTree_GetData(node), LTAG_UINT8);
		if (EntryMode == ENTRY_MODE_CHIP || EntryMode == ENTRY_MODE_CLESS_CHIP)
		btBitmap[3] |= BIT_25;


	inLen = 8;
	memcpy(tMsgTx.btField127Response, btBitmap, inLen);
    inLen += Telium_Sprintf((char*)&tMsgTx.btField127Response[inLen], "%03d", 29);

	memset(FullSerialNumberRaw, 0x00, sizeof(FullSerialNumberRaw));
	PSQ_Give_Serial_Number((char *)FullSerialNumberRaw);


	memset(chModo, 0, 2 + 1);
	//memcpy(chModo, "CO", 2);
	if(tTerminal.Comercio == RESTAURANT)
	    memcpy(chModo, "RE", 2);
	else if (tTerminal.Comercio == RENTACAR)
		memcpy(chModo, "RC", 2);
	else if (tTerminal.Comercio == HOTELES)
		memcpy(chModo, "HO", 2);
	else if (tTerminal.Comercio == COMERCIO)
		memcpy(chModo, "CO", 2);
	else if (tTerminal.Comercio == CLINICAS)
		memcpy(chModo, "CL", 2);
	chModo[2] = '\0';


	//memset(TmpVersion, 0, 7+1);
	//sprintf(TmpVersion, "%7s",mess_getReturnedMessage(MSG_VERSION));

	memset(chBuffer, 0, 29 + 1);
//	Telium_Sprintf(chBuffer,"%6s%04lu0%s%6s%2d%2s"," ",BATCH_NUMBER,FullSerialNumberRaw,mess_getReturnedMessage(MSG_VERSION),mess_getReturnedMessage(MSG_VERSION_LETRA), chModo );
	Telium_Sprintf(chBuffer,"%6s%04lu0%s%6s%2d%2s"," ",BATCH_NUMBER,FullSerialNumberRaw,"000001",'F', chModo );
	strcpy((char *)tMsgTx.btField127Response+inLen, chBuffer);
    inLen += strlen(chBuffer);

   tMsgTx.btField127Responselen = inLen;

    CHECK((EntryMode == ENTRY_MODE_CHIP || EntryMode == ENTRY_MODE_CLESS_CHIP) , lbl_NOF55);
	node = TlvTree_Find(Tree,TAG_EMV_1GEN_TAGS, 0);
    EMVlen = BuildEMVTags(node,inLen);


 lbl_NOF55:
	return RET_OK;

	LBL_TAG_TRAN_ID_NOTFOUND:
		return RET_NOK;
}

//- CLESS

//==============================================================================
// Function Name: CheckPEMExistence
//==============================================================================
bool CheckPEMExistence(const char *SSLCert)
{
  unsigned int mode = 0;
  int  rc;
  char chCompletePath[128];
  char chMessageError[100 + 1];

  rc = FS_mount("/HOST", &mode);
  if (rc != FS_OK)
  {
    //DisplayWindowMessage(TXT_ERR_VALIDATING_CERTIFICATE, DSPERROR_WAITKEY);
	  UI_PrintLine_Ext(TXT_ERR_VALIDATING_CERTIFICATE, UI_FONT_DOUBLE, UI_ALIGN_CENTER);

	  return FALSE;
  }

  sprintf(chCompletePath, "/HOST/%s.PEM", SSLCert);
  rc = FS_exist(chCompletePath);
  FS_unmount("/HOST");
  if (rc == FS_OK)
    return TRUE;

  sprintf(chMessageError, TXT_CERTIFICATE_NOT_LOADED, SSLCert);
  UI_PrintLine_Ext(chMessageError, UI_FONT_DOUBLE, UI_ALIGN_CENTER);
//  DisplayWindowMessage(chMessageError, DSPERROR_WAITKEY);

  return FALSE;
}

//==============================================================================
// Function Name: ConfigureSSLProfile
//==============================================================================
int ConfigureTLSProfile(const char *chSSLCertName)
{
  int  err;
  char chFileName[FS_PATHSIZE + 1] = {0};
  char CAfile[FS_PATHSIZE + 1] = {0};
  int  i, n;


  SSL_PROFILE_HANDLE hProf = NULL;

  // Close ssl lib
    ssllib_close();

  if (CheckPEMExistence(chSSLCertName) == FALSE)
  return -1;

  err = SSL_PROFILE_EEXIST;
  while (err==SSL_PROFILE_EEXIST)
      {
	  // Build cert path
	  sprintf(chFileName, "/SYSTEM/%s.PEM", chSSLCertName);
	  hProf = SSL_NewProfile( chFileName, &err );
      }

  // Open ssl lib
  err = ssllib_open();

  // Delete actual profile
  SSL_DeleteProfile(chSSLCertName);

  // Load new profile
  hProf = SSL_LoadProfile(chSSLCertName);

 if (hProf != NULL)
  {
    SSL_ProfileGetCACount(hProf, &n);//Obtiene el numero de Autoridades certificadoras

    for (i = 0; i < n; i++) {
      SSL_ProfileGetCAFile(hProf, i, CAfile);        //Obtiene el nombre del certificado CA
      SSL_ProfileRemoveCertificateCA(hProf, CAfile); //Elimina el certificado CA
    }
  }
  else
  {
    // Create the new ssl profile
   hProf = SSL_NewProfile(chSSLCertName, &err);
    if (hProf == NULL)
      return -1;

    err = SSL_ProfileSetProtocol(hProf, TLSv1_2);
	err = SSL_ProfileSetCipher(hProf, SSL_RSA | SSL_AES | SSL_SHA256 | SSL_DES |SSL_SHA384 | SSL_3DES | SSL_RC4 |SSL_RC2 | SSL_MD5 | SSL_SHA1,
			    		                   SSL_HIGH | SSL_NOT_EXP);

  }

	err = SSL_ProfileSetKeyFile(hProf, ARCHIVO_PRIV_KEY_MUTUA, FALSE);
	// Set the CA certificate file.
	err = SSL_ProfileSetCertificateFile(hProf, ARCHIVO_CERT_MUTUA);

	err = SSL_ProfileAddCertificateCA(hProf, chFileName);

	// Save new profile
	err = SSL_SaveProfile(hProf);

	err = SSL_UnloadProfile(hProf);

  return RET_OK;

}


/* --------------------------------------------------------------------------
 * Function Name:	Borrar_Reverse
 * Description:		Borrar a reverse if necessary
 * Parameters:
 *  - none
 * Return:
 *  - RET_OK - no reverse to send or reverse sent OK
 *  - RET_NOK - cannot send a reverse
 * Notes:
 */
int Borrar_Reverse(uint8 *BatchName)
{
	TLV_TREE_NODE Tree_Reverse;
	TLV_TREE_NODE node;
	int RetFun;
	uint16 TranStatus;
//	uint8 ResponseCode[2];

	// create tree to place last transaction
	Tree_Reverse = TlvTree_New(0);
	CHECK(Tree_Reverse != NULL, LBL_CANNOT_CREATE_REVERSE_TREE);

	// get last transaction
	RetFun = Batch_Get_Last_Transaction(Tree_Reverse, BatchName);
	CHECK(RetFun == RET_OK, LBL_NO_TRAN);


	// add batch number to tree
	TABLE_MERCHANT Merchant;

	RetFun = Get_TransactionMerchantData(&Merchant, Tree_Reverse);
	CHECK(RetFun == RET_OK, LBL_ERROR);
	RetFun = Tlv_SetTagValueInteger(Tree_Reverse, TAG_BATCH_NUMBER, Merchant.BatchNumber, LTAG_INT32);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// get transaction status
	node = TlvTree_Find(Tree_Reverse, TAG_TRAN_STATUS, 0);
	CHECK(node != NULL, LBL_ERROR);
	TranStatus = *(uint16*)TlvTree_GetData(node);

	// verify if necessary send reverse
	CHECK(TranStatus & FSTATUS_NEED_REVERSE, LBL_NO_REVERSE);

	// add batch name
	node = TlvTree_AddChild(Tree_Reverse, TAG_BATCH_FILE_NAME, BatchName, strlen((char*)BatchName));
	CHECK(node != NULL, LBL_ERROR);

		// reverse processed, clear reverse
		RetFun = Batch_Delete_Transaction(Tree_Reverse);
		CHECK(RetFun == RET_OK, LBL_ERROR);


	TlvTree_Release(Tree_Reverse);
	return RET_OK;

LBL_CANNOT_CREATE_REVERSE_TREE:
	// ERROR: cannot create tree for reverse
	return RET_NOK;

LBL_NO_TRAN:
	TlvTree_Release(Tree_Reverse);
	if(RetFun == BATCH_RET_EMPTY_FILE)
		// No transaction if batch file, continue with transaction
		return RET_OK;
	else
		// ERROR: cannot open last transaction
		return RET_NOK;

LBL_ERROR:
	// ERROR: tree error
	TlvTree_Release(Tree_Reverse);
	return RET_NOK;

LBL_NO_REVERSE:
	// last transaction is not a reverse, continue with transaction
	TlvTree_Release(Tree_Reverse);
	return RET_OK;

//+ ISOMAIN01 @mamata Aug 14, 2014
//- ISOMAIN01

}

//reverse_mtip
/* --------------------------------------------------------------------------
 * Function Name:	Reverse_Mtip
 * Description:
 * Parameters:
 *  - TLV_TREE_NODE Tree - Transaction data
 * Return:
 *  - RET_OK - process OK
 *  - RET_NOK - configuration error
 *  -2 - pack error
 *  -3 - unpack error
 *  -4 - cannot connect to host
 *  <0 - link layer error
 * Notes:
 */
int Reverse_Mtip(TLV_TREE_NODE Tree)
{
	TLV_TREE_NODE node;
	int RetFun;
	uint8 BatchName[LZ_MERCH_BATCH_NAME];


	RetFun = HostMain_Connect(Tree);
	CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);

	RetFun = LLC_WaitConnection(TRUE);
	CHECK(RetFun == LLC_STATUS_CONNECTED, LBL_ERROR_REVERSE);


	// get BatchName
	node = TlvTree_Find(Tree, TAG_BATCH_FILE_NAME, 0);
	CHECK(node != NULL, LBL_ERROR_REVERSE);
	memset(BatchName, 0, LZ_MERCH_BATCH_NAME);
	memcpy(BatchName, TlvTree_GetData(node), TlvTree_GetLength(node));

	// Send reverse if necessary
	RetFun = HostMain_SendReverse(BatchName);
		CHECK(RetFun == RET_OK, LBL_ERROR_REVERSE);

	LLC_EndConnection();
	return RET_OK;


	LBL_ERROR_REVERSE:

	LLC_EndConnection();
	return RET_NOK;


}

