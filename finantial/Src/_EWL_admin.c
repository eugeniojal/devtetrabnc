 /* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp
 * File:			_EWL_admin.c
 * Header file:		_EWL_admin.h
 * Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/

//+ EWL01 @jbotero 2/02/2016
/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

ewlObject_t *emvHandle;

#if 0
    ewlCBDisplay_t                ewlDisplay;               /**< Show message */
    ewlCBSelectionMenu_t          ewlSelectionMenu;         /**< AID selection menu */
    ewlCBGetParameters_t          ewlGetParameter;          /**< Ask for application parameters */
    ewlCBGetPublicKey_t           ewlGetPublicKey;          /**< Get certification authority public key */
    ewlCBGetPinOnLine_t           ewlGetPinOnline;          /**< Get PIN online */
    ewlCBGetPinOffLine_t          ewlGetPinOffLine;         /**< Get PIN offline */
    ewlCBFinishCVMProcessing_t    ewlFinishCVMProcessing;   /**< Finish CVM process */
    ewlCBLeds_t                   ewlLeds;                  /**< Contact-less LEDs control */
    ewlCBRemoveCard_t             ewlRemoveCard;            /**< Warn to remove card */

#endif

static ewlObject_t * EwlCreate (void){

    static ewlCBFunctions_t ewlFunctions = {
        callbackBDisplay,       //ewlCBDisplay_t
        callbackGetMenu,        //ewlCBSelectionMenu_t
        callbackGetParameters,  //ewlCBGetParameters_t
        callbackGetPublicKey,   //ewlCBGetPublicKey_t
        callbackGetPinOnLine,   //ewlCBGetPinOnLine_t
        callbackGetPinOffLine,  //ewlCBGetPinOffLine_t
        callbackFinalCVM,       //ewlCBFinishCVMProcessing_t
        callbackLeds ,          //ewlCBLeds_t
        callbackRemoveCard      //ewlCBRemoveCard_t
    };

return ewlDllCreate(&ewlFunctions);


}

/* --------------------------------------------------------------------------
 * Function Name:	EWL_SetEmvHandle
 * Description:		Destroy and Set new EMV handle pointer
 * Author:			@jbotero
 * Parameters:
 * 	- nothing
 * Return:
 * 	- (!= NULL) OK, pointer of ewlObject_t*
 * 	- (== NULL) Error
 * Notes:
 */

ewlObject_t * EWL_SetEmvHandle(void)
{
	//--- destroy emvHandle if exist
	if(emvHandle != NULL)
		ewlDestroy(emvHandle);

	//--- Create emvHandle
	emvHandle = EwlCreate();
	CHECK(emvHandle != NULL, LBL_ERROR);

	return emvHandle;

LBL_ERROR:

	return NULL;
}

/* --------------------------------------------------------------------------
 * Function Name:	EWL_GetEmvHandle
 * Description:		Get EMV handle pointer
 * Author:			@jbotero
 * Parameters:
 * 	- nothing
 * Return:
 * 	- (NULL) Error
 * 	- (Other) OK, pointer ewlObject_t
 * Notes:
 */

ewlObject_t * EWL_GetEmvHandle(void)
{
	return emvHandle;
}

/* --------------------------------------------------------------------------
 * Function Name:	EWL_DestroyEmvHandle
 * Description:		Destroy EMV handle pointer
 * Author:			@jbotero
 * Parameters:
 * 	- nothing
 * Return:
 * 	- nothing
 * Notes:
 */

void EWL_DestroyEmvHandle(void)
{
    if(emvHandle != NULL)
    {
        ewlDestroy(emvHandle);
        emvHandle = NULL;
    }
}
//- EWL01

