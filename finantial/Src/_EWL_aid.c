////#include "ewlDemo.h"
////#include "aid.h"

#include "iPOSApp.h"

//+ PAR_EWL @carodriguez 29/02/2016
#if (DIR_EWL_PAR != 1)
static aid_t aids[] ={

// PURE
//	{
//		EWL_TECHNOLOGY_CONTACT,                         // Technology
//		7,                                              // AID Length
//		{ 0xA0, 0x00, 0x00, 0x05, 0x85, 0x03, 0x01 },   // AID
//
//		{
//			3,                                          // n Versions
//			{
//				{{ 0x00, 0x83 }},                       // Application version 1
//				{{ 0x00, 0x84 }},                       // Application version 2
//				{{ 0x00, 0x8C }}                        // Application version 3
//			}
//		}, // version
//
//		{
//			{ 0xD8, 0x40, 0x00, 0xA8, 0x00 },           // TAC online
//			{ 0x08, 0x10, 0x00, 0x00, 0x00 },           // TAC denial
//			{ 0xD8, 0x40, 0x04, 0xF8, 0x00 }            // TAC default
//		}, // tac
//		{
//			5000,                                       // Floor Limit
//			2000,                                       // Theshold value
//			{ 0x50 },                                   // Max Percentage
//			{ 0x20 },                                   // Target Percentage
//		}, // risk
//
//		{ }, // cless
//		{ }, // tdol default
//		{ }, // ddol default
//	},

    // EMV VISA CREDIT
    {
        EWL_TECHNOLOGY_CONTACT,                         // Technology
        7,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x03, 0x10, 0x10 },   // AID

        {
            3,                                          // n Versions
            {
                {{ 0x00, 0x83 }},                       // Application version 1
                {{ 0x00, 0x84 }},                       // Application version 2
                {{ 0x00, 0x8C }}                        // Application version 3
            }
        }, // version

        {
            { 0xD8, 0x40, 0x00, 0xA8, 0x00 },           // TAC online
            { 0x00, 0x10, 0x00, 0x00, 0x00 },           // TAC denial
            { 0xD8, 0x40, 0x04, 0xF8, 0x00 }            // TAC default
        }, // tac
        {
            5000,                                       // Floor Limit
            2000,                                       // Theshold value
            { 0x50 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        { }, // cless
        { }, // tdol default
        { }, // ddol default
    },

    #ifdef  EWL_ENABLE_KERNEL_PAYWAVE
    // PAYWAVE QVSDC VISA CREDIT
    {
        EWL_TECHNOLOGY_QVSDC,                           // Technology
        7,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x03, 0x10, 0x10 },   // AID

        {
            2,                                          // n Versions
            {
                {{ 0x00, 0x83 }},                       // Application version 1
                {{ 0x00, 0x84 }},                       // Application version 2
            }
        }, // version

        {
            { 0xD8, 0x40, 0x00, 0xA8, 0x00 },           // TAC online
            { 0x08, 0x10, 0x00, 0x00, 0x00 },           // TAC denial
            { 0xD8, 0x40, 0x04, 0xF8, 0x00 }            // TAC default
        }, // tac
        {
            5000,                                       // Floor Limit
            2000,                                       // Theshold value
            { 0x80 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        {
            10000,
            2500
        }, // cless
        { }, // tdol default
        { }, // ddol default

    },
    #endif

    // EMV VISA DEBIT
    {
        EWL_TECHNOLOGY_CONTACT,                         // Technology
        7,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x03, 0x20, 0x10 },   // AID

        {
            3,                                          // n Versions
            {
                {{ 0x00, 0x83 }},                       // Application version 1
                {{ 0x00, 0x84 }},                       // Application version 2
                {{ 0x00, 0x8C }}                        // Application version 3
            }
        }, // version

        {
            { 0xD8, 0x40, 0x00, 0xA8, 0x00 },           // TAC online
            { 0x08, 0x10, 0x00, 0x00, 0x00 },           // TAC denial
            { 0xD8, 0x40, 0x04, 0xF8, 0x00 }            // TAC default
        }, // tac
        {
            5000,                                       // Floor Limit
            2000,                                       // Theshold value
            { 0x50 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        { }, // cless
        { }, // tdol default
        { }, // ddol default
    },

    #ifdef  EWL_ENABLE_KERNEL_PAYWAVE
    // PAYWAVE QVSDC VISA DEBIT
    {
        EWL_TECHNOLOGY_QVSDC,                           // Technology
        7,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x03, 0x20, 0x10 },   // AID

        {
            2,                                          // n Versions
            {
                {{ 0x00, 0x83 }},                       // Application version 1
                {{ 0x00, 0x84 }},                       // Application version 2
            }
        }, // version

        {
            { 0xD8, 0x40, 0x00, 0xA8, 0x00 },           // TAC online
            { 0x08, 0x10, 0x00, 0x00, 0x00 },           // TAC denial
            { 0xD8, 0x40, 0x04, 0xF8, 0x00 }            // TAC default
        }, // tac
        {
            5000,                                       // Floor Limit
            2000,                                       // Theshold value
            { 0x80 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        {
            10000,
            2500
        }, // cless
        { }, // tdol default
        { }, // ddol default
    },
    #endif

    // EMV MASTERCARD CREDIT
    {
        EWL_TECHNOLOGY_CONTACT,                         // Technology
        7,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x10 },   // AID

        {
            1,                                          // n Versions
            {
                {{ 0x00, 0x02 }},                       // Application version 1
            }
        }, // version

        {
            { 0xFC, 0x50, 0xAC, 0xA0, 0x00 },
            { 0x00, 0x00, 0x00, 0x00, 0x00 },
            { 0xFC, 0x50, 0xAC, 0xF8, 0x00 },
        }, // tac
        {
            5000,                                       // Floor Limit
            2000,                                       // Theshold value
            { 0x80 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        { }, // cless
        { }, // tdol default
        { }, // ddol default
    },

    #ifdef  EWL_ENABLE_KERNEL_PAYPASS
    // PAYPASS MCHIP MASTER CREDIT
     {
         EWL_TECHNOLOGY_PAYPASS_MCHIP,                    // Technology
         7,                                              // AID Length
         { 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x10 },   // AID

         {
             1,                                          // n Versions
             {
                 {{ 0x00, 0x02 }},                       // Application version 1
             }
         }, // version

         {
             { 0xFC, 0x50, 0xAC, 0xA0, 0x00 },
             { 0x00, 0x00, 0x00, 0x00, 0x00 },
             { 0xFC, 0x50, 0xAC, 0xF8, 0x00 },
         }, // tac
         {
             5000,                                       // Floor Limit
             2000,                                       // Theshold value
             { 0x80 },                                   // Max Percentage
             { 0x20 },                                   // Target Percentage
         }, // risk

         {
             10000,
             2500
         }, // cless
         { }, // tdol default
         { }, // ddol default

     },
    #endif

     // EMV MASTERCARD DEBIT
     {
         EWL_TECHNOLOGY_CONTACT,                         // Technology
         7,                                              // AID Length
         { 0xA0, 0x00, 0x00, 0x00, 0x04, 0x30, 0x60 },   // AID

         {
             1,                                          // n Versions
             {
                 {{ 0x00, 0x02 }},                       // Application version 1
             }
         }, // version

         {
             { 0xFC, 0x50, 0xAC, 0xA0, 0x00 },
             { 0x00, 0x00, 0x80, 0x00, 0x00 },
             { 0xFC, 0x50, 0xAC, 0xF8, 0x00 },
         }, // tac
         {
             5000,                                       // Floor Limit
             2000,                                       // Theshold value
             { 0x80 },                                   // Max Percentage
             { 0x20 },                                   // Target Percentage
         }, // risk

         { }, // cless
         { }, // tdol default
         { }, // ddol default
     },

    #ifdef  EWL_ENABLE_KERNEL_PAYPASS
    // PAYPASS MCHIP MASTER  DEBIT
    {
        EWL_TECHNOLOGY_PAYPASS_MCHIP,                    // Technology
        7,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x04, 0x30, 0x60 },   // AID

        {
            1,                                          // n Versions
            {
                {{ 0x00, 0x02 }},                       // Application version 1
            }
        }, // version

        {
            { 0xFC, 0x50, 0xAC, 0xA0, 0x00 },
            { 0x00, 0x00, 0x80, 0x00, 0x00 },
            { 0xFC, 0x50, 0xAC, 0xF8, 0x00 },
        }, // tac
        {
            //5000,                                       // Floor Limit
            0,
            2000,                                       // Theshold value
            { 0x80 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        {
            10000,
            2500
        }, // cless

        { }, // tdol default
        { }, // ddol default
    },
    #endif

    // EMV AMEX CREDIT
    {
        EWL_TECHNOLOGY_CONTACT,                         // Technology
        6,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x25, 0x01 },         // AID

        {
            1,                                          // n Versions
            {
                {{ 0x00, 0x01 }},                       // Application version 1
            }
        }, // version

        {
            { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },
            { 0x00, 0x00, 0x00, 0x00, 0x00 },
            { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },
        }, // tac
        {
            1600,                                       // Floor Limit
            1000,                                       // Theshold value
            { 0x80 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk

        { }, // cless
        { }, // tdol default
        { }, // ddol default
    },

    #ifdef  EWL_ENABLE_KERNEL_EXPRESSPAY
    // EXPRESSPAY AMEX CREDIT
    {
        EWL_TECHNOLOGY_EXPRESSPAY_EMV,                  // Technology
        6,                                              // AID Length
        { 0xA0, 0x00, 0x00, 0x00, 0x25, 0x01 },         // AID

        {
            1,                                          // n Versions
            {
                {{ 0x00, 0x01 }},                       // Application version 1
            }
        }, // version

        {
            { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },
            { 0x00, 0x00, 0x00, 0x00, 0x00 },
            { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },
        }, // tac
        {
            1600,                                       // Floor Limit
            1000,                                       // Theshold value
            { 0x80 },                                   // Max Percentage
            { 0x20 },                                   // Target Percentage
        }, // risk
        {
            10000,
            1600,
        }, // cless

        { }, // tdol default
        { }, // ddol default
    },
    #endif

};
#endif // DIR_EWL_PAR
//- PAR_EWL

//+ PAR_EWL @carodriguez 29/02/2016
#if (DIR_EWL_PAR == 1)
aid_t *aidGet (int id){
    if(id >= AIDCount) return NULL;
    return pAIDsFromFile + id;
}	
#else // DIR_EWL_PAR
aid_t *aidGet (int id){
    if(id >= BASE_LENGTH(aids)) return NULL;
    return &aids[id];
}
#endif // DIR_EWL_PAR
//- PAR_EWL
