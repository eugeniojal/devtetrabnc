/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			Globals.c
 * Header file:		Globals.h
 * Description:		File to place all globals variables
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
GENERAL_CONF tConf;
TABLE_TERMINAL tTerminal;
TABLE_STATISTICS tStatistics;
TLV_TREE_NODE Tree_Tran;
unsigned short ApplicationType;
LLCOMS_CONFIG LLC_Host;
char IdleKey;
GLOBALINFO tGlobalInfo;
TLV_TREE_NODE Tree_NoPrinter;
OPCIONES_MENU OpM;

