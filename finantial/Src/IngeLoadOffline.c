/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			IngeLoadOffline.c
 * Header file:		IngeLoadOffline.h
 * Description:
 * Author:			dvelazco, jbotero
 * --------------------------------------------------------------------------*/

//+ INGLOFF01 @jbotero 29/07/2015
/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
#define TagTableXML 0x01000000

/* --------------------------------------------------------------------------
 * Function Name:
 * Description:
 * Author:			@dvelazco
 * Parameters:
 * Return:
 * Notes:
 */

void BuildLength(uint length, char *LoadBuffer, uint32 *BufferLength)
{
       unsigned long Result[3];

       Result[0] = 0;
       Result[1] = 0;
       Result[2] = 0;

       if (length > 127)
             Result[1] = 128;

       if (length > 16384)
             Result[2] = 128;

       if ( (1 & length) > 0)
             Result[0] = Result[0] + 1;
       if ((2 & length) > 0)
             Result[0] = Result[0] + 2;
       if ((4 & length) > 0)
             Result[0] = Result[0] + 4;
       if ((8 & length) > 0)
             Result[0] = Result[0] + 8;
       if ((16 & length) > 0)
             Result[0] = Result[0] + 16;
       if ((32 & length) > 0)
             Result[0] = Result[0] + 32;
       if ((64 & length) > 0)
             Result[0] = Result[0] + 64;

       if ((128 & length) > 0)
             Result[1] = Result[1] + 1;
       if ((256 & length) > 0)
             Result[1] = Result[1] + 2;
       if ((512 & length) > 0)
             Result[1] = Result[1] + 4;
       if ((1024 & length) > 0)
             Result[1] = Result[1] + 8;
       if ((2048 & length) > 0)
             Result[1] = Result[1] + 16;
       if ((4096 & length) > 0)
             Result[1] = Result[1] + 32;
       if ((8192 & length) > 0)
             Result[1] = Result[1] + 64;

       if ((16384 & length) > 0)
             Result[2] = Result[2] + 1;
       if ((32768 & length) > 0)
             Result[2] = Result[2] + 2;
       if ((65536 & length) > 0)
             Result[2] = Result[2] + 4;
       if ((131072 & length) > 0)
             Result[2] = Result[2] + 8;
       if ((262144 & length) > 0)
             Result[2] = Result[2] + 16;
       if ((524288 & length) > 0)
             Result[2] = Result[2] + 32;
       if ((1048576 & length) > 0)
             Result[2] = Result[2] + 64;

       if (Result[2] != 0)
       {
             //Binasc((uint8 *)&LoadBuffer[(*BufferLength)], Result[2], 1);
             LoadBuffer[(*BufferLength)] = (uint8)Result[2];
             (*BufferLength)++;
       }

       if (Result[1] != 0)
       {
             //Binasc((uint8 *)&LoadBuffer[(*BufferLength)], Result[1], 1);
             LoadBuffer[(*BufferLength)] = (uint8)Result[1];
             (*BufferLength)++;
       }


       // Binasc((uint8 *)&LoadBuffer[(*BufferLength)], Result[0], 1);
       LoadBuffer[(*BufferLength)] = (uint8)Result[0];
       (*BufferLength)++;
}

/** ------------------------------------------------------------------------------
* Function Name: GetXMLInitializationData
* Author: @DFVC
* Date: 06/03/2015
* Description: Lee el archivo INGELOAD.XML de la carpeta host y lo serializa en un arbol
* Parameters:
* - *Tree, puntero del arbol donde se va a guardar el XML serializado
* Return:
* - RET_OK
* - RET_NOK
* Notes:
*/
int GetXMLInitializationData(TLV_TREE_NODE *Tree)
{
	int FileLen;
	unsigned int Mode;
	S_FS_FILE *hFile = NULL;
	uint8 *tmpBuffer = NULL;
	int RetFun;

	// mount host folder
	RetFun = FS_mount("/HOST", &Mode);

	// open INGELOAD.XML
	hFile = FS_open("/HOST/PARAMS.XML", "r" );
	CHECK(hFile != NULL, LBL_ERROR)

	// get file length
	FileLen = FS_length(hFile);
	// reserve memory for tmpBuffer

	tmpBuffer = umalloc(FileLen);
	CHECK(tmpBuffer != NULL, LBL_ERROR);

	// read file data
	FS_read(tmpBuffer, FileLen, 1, hFile);

	// close file
	RetFun = FS_close (hFile);

	// reset Tree if necessary
	if(*Tree != NULL)
	{
		TlvTree_Release(*Tree);
		*Tree = NULL;
	}

	// Unserialize buffer
	RetFun = TlvTree_Unserialize(Tree, TLV_TREE_SERIALIZER_XML, tmpBuffer, FileLen);
	CHECK(RetFun == TLV_TREE_OK, LBL_ERROR);

	// free memory
	ufree(tmpBuffer);

	// umoun host
	RetFun = FS_unmount ("/HOST");
	return RET_OK;

LBL_ERROR:
	RetFun = FS_unmount ("/HOST");
	ufree(tmpBuffer);
	return RET_NOK;
}

/** ------------------------------------------------------------------------------
* Function Name: RunXMLInitializationData
* Author: @DFVC
* Date: 06/03/2015
* Description: armar un buffer simulando al entregado por Ingeload con la informacion cargada desde el archivo INGELOAD.XML
* Parameters:
* - None
* Return:
* - RET_OK
* - RET_NOK
* Notes:
* - El archivo INGELOAD.XML debe ser de tipo tlvtree
* - Para crear una tabla se debe crear un nodo en el arbol con el identificador 0x01000000 (#define TagTable 0x01000000) y como valor el identificador de la tabla en Ingeload
* - Dentro de cada tabla se debe de tener nodos donde el identificador debe ser el mismo identificador usado por las tablas de Ingeload y dentro el valor que se desea
* - Para conocer los identificadores de Ingeload dirgase al archivo IngeLoad_Unpack.c
*/
int RunXMLInitializationData(void)
{
	// declaracion de variables
	TLV_TREE_NODE tlv_Initialization = NULL; // Arbol principal que va a contener la informacion del XML
	TLV_TREE_NODE nodeTable = NULL; // Nodo de las tablas
	TLV_TREE_NODE nodeData = NULL; // Nodo de la informacion de los campos de las tablas
	int idTabla;

	char *LoadBuffer = NULL;
	uint32 BufferLength;
	int RetFun;

	// Inicio las varianles
	idTabla = 0;
	BufferLength = 0;

	// Reserva la memoria para el buffer
	LoadBuffer = umalloc(10000);
	CHECK(LoadBuffer != NULL, LBL_ERROR);

	// Se lee el archivo xml y se carga en el arbol
	RetFun = GetXMLInitializationData(&tlv_Initialization);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// Se busca la primera tabla
	nodeTable = TlvTree_Find(tlv_Initialization, TagTableXML, idTabla);

	while(nodeTable != NULL)
	{
		// se agrega el inicio de la tabla para indicar el cambio de tabla
		LoadBuffer[BufferLength] = 0x00;
		BufferLength++;
		LoadBuffer[BufferLength] = 0x02;
		BufferLength++;

		// se agrega el tag de la tabla
		memcpy(&LoadBuffer[BufferLength], TlvTree_GetData(nodeTable), TlvTree_GetLength(nodeTable));
		BufferLength++;
		BufferLength++; // se deja el espacion del CRC
		// busco el primer dato de la tabla
		nodeData = TlvTree_GetFirstChild(nodeTable);
		while(nodeData != NULL)
		{
			// se hace esta validacion por si se necesita agregar informacion al arbol que se haga con uun tag mayor a este valor
			// para que no interfiera con los tags de los identificadores de campos de las tablas
			if(TlvTree_GetTag(nodeData) < 0x00001000)
			{
				// agrego el valor del tag del campo
				BuildLength(TlvTree_GetTag(nodeData), LoadBuffer, &BufferLength);
				// agrego la longitud de los datos
				BuildLength(TlvTree_GetLength(nodeData), LoadBuffer, &BufferLength);
				// agrgeo el dato
				memcpy(&LoadBuffer[BufferLength], TlvTree_GetData(nodeData), TlvTree_GetLength(nodeData));
				BufferLength += TlvTree_GetLength(nodeData);
			}
			// busco el siguiente dato
			nodeData = TlvTree_GetNext(nodeData);
		}
		// busco la siguiente tabla
		nodeTable = TlvTree_Find(tlv_Initialization, TagTableXML, ++idTabla);
	}

	// Se envia el buffer a la funcion de IngeLoad
	RetFun = UnpackBuffer(LoadBuffer,BufferLength);
	CHECK(RET_OK == RetFun, LBL_ERROR);

	// muestra mensaje de inicializacion exitosa
	UI_ShowMessage( mess_getReturnedMessage(MSG_IPARAM),
	mess_getReturnedMessage(MSG_INIT_OK),
	NULL,
	GL_ICON_INFORMATION,
	GL_BUTTON_ALL,
	tConf.TimeOut);

	// Libera el arbol
	if (tlv_Initialization != NULL)
	{
		TlvTree_Release(tlv_Initialization);
		tlv_Initialization = NULL;
	}
	// Libera la memoria del buffer
	ufree(LoadBuffer);
	return RET_OK;

LBL_ERROR:
	// Libera el arbol
	if (tlv_Initialization != NULL)
	{
		TlvTree_Release(tlv_Initialization);
		tlv_Initialization = NULL;
	}

	// Libera la memoria del buffer
	ufree(LoadBuffer);
	return RET_NOK;
}
//- INGLOFF01
