//+ INGESTATE01 @jbotero 13/09/2015
/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOS-App
 * File:			IngestateUnpack.c
 * Header file:		IngestateUnpack.h
 * Description:
 * Author:			jbotero
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/
int Init_Table_Terminal(void);
int Init_Table_Merchant(void);
int Init_Table_Host(void);
int Init_Table_Range(void);
int Init_Table_IP(void);
int Init_Table_MerchantRange(void);
int Init_Table_MerchantGroup(void);
int Init_Table_GroupRange(void);

int Init_Table_DataKeyInfo(void);
int Init_Table_PinKeyInfo(void);
/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/
#define TagTableXML_IngEstate 0x00000000

/* ***************************************************************************
* VARIABLES
* ***************************************************************************/

TABLE_TERMINAL 		tTerminalAux;
TABLE_MERCHANT 		tMerchAux;
TABLE_HOST 			tHostAux;
TABLE_RANGE	 		tRangeInfoAux;
TABLE_IP 			tIPConfAux;
TABLE_MERCH_RANGE 	tMerchRangeAux;
TABLE_MERCH_GROUP 	tMerchGroupAux;
TABLE_GROUP_RANGE 	tGroupRangeAux;
GENERAL_CONF 		tConf;

TABLE_KEY_INFO 		tDataKeyInfoAux;
TABLE_KEY_INFO 		tPinKeyInfoAux;
TABLE_MESERO 		tMeseroAux;

TABLE_BANK	 		tBankInfoAux;

/* --------------------------------------------------------------------------
 * Function Name:	ShowMessageError_Unpack
 * Description:		Shows specific error when initialization unpack fail
 * Author:			@jbotero
 *
 * Parameters:		int RetFun: Error code
 * Return:			Error code
 * Notes:
 */

int ShowMessageError_Unpack( int RetFun )
{
	char Line1[50] = { 0x00, };
	char Line2[50] = { 0x00, };

	switch(RetFun)
	{
		case RET_NOK:
			strcpy(Line1, mess_getReturnedMessage(MSG_PAR_PROCESSING_ERR_L1));
			strcpy(Line2, mess_getReturnedMessage(MSG_PAR_PROCESSING_ERR_L2));
			break;

		default:
			strcpy(Line1, mess_getReturnedMessage(MSG_UNKNOW_ERROR));
			break;
	}

	//Show message error
	UI_ShowMessage( mess_getReturnedMessage(MSG_ERROR_INIT),
			Line1,
			Line2,
			GL_ICON_ERROR,
			GL_BUTTON_ALL,
			tConf.TimeOut);

	return RetFun;
}

/* --------------------------------------------------------------------------
 * Function Name:	GetTableId
 * Description:		Get Table ID
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */

int GetTableId(TLV_TREE_NODE node, int * IdCampo)
{
	int IntTag;
	char StrTag[10];
	char TableId[10];

	char StrTagId[10];
	char FieldId[10];
	int digits1 = 1;
	int digits2 = 1;

	IntTag = TlvTree_GetTag(node);

	sprintf(StrTag, "%X", IntTag);

	if (strlen(StrTag) % 2 == 0)
		digits1++;

	memset(TableId, 0x00, sizeof(TableId));
	memcpy(TableId, StrTag, digits1);

	if (IdCampo != NULL)
	{
		memset(StrTagId, 0x00, sizeof(StrTagId));
		memcpy(StrTagId, StrTag + digits1, strlen(StrTag));

		if (strlen(StrTagId) % 2 == 0)
			digits2++;

		memset(FieldId, 0x00, sizeof(FieldId));
		memcpy(FieldId, StrTagId, digits2);

		*IdCampo = atoi(FieldId);
	}

	return atoi(TableId);
}

/* --------------------------------------------------------------------------
 * Function Name:	LoadMerchantRangeTable
 * Description:		Load "Merchant Range Table" to process cards by merchant
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */

int LoadMerchantRangeTable(void)
{
	int RetFun;
	int iRet;
	TABLE_RANGE tRange;
	TABLE_MERCHANT tMerchant;
	TABLE_GROUP_RANGE tGroupRange;

	RetFun = TM_FindFirst(TAB_MERCHANT, &tMerchant);

	while( RetFun == TM_RET_OK )
	{
		RetFun = TM_FindFirst(TAB_GROUP_RANGE, &tGroupRange);

		while( RetFun == TM_RET_OK )
		{
			if(tMerchant.GroupRange == tGroupRange.IdGroupRange)
			{
				memset(&tMerchRangeAux, 0x00, sizeof(tMerchRangeAux));

				tMerchRangeAux.MerchantID = tMerchant.Header.RecordID;
				tMerchRangeAux.RangeID = tGroupRange.IdRange;

				iRet = TM_AddRecord(TAB_MERCH_RANGE, &tMerchRangeAux);
				if (iRet != TM_RET_OK)
					return RET_NOK;

				iRet = TM_FindRecord(TAB_RANGE, &tRange, tMerchRangeAux.RangeID);
				if (iRet != TM_RET_OK)
					return RET_NOK;

				tRange.Enabled = TRUE;

				iRet = TM_ModifyRecord(TAB_RANGE, &tRange);
				if (iRet != TM_RET_OK)
					return RET_NOK;

			}

			RetFun = TM_FindNext( TAB_GROUP_RANGE, &tGroupRange);
		}

		RetFun = TM_FindNext(TAB_MERCHANT, &tMerchant);
	}

	return RET_OK;

}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_Terminal
 * Description:		Load data on terminal table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_Terminal(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x00010101:

		if(IntLen > LZ_TERM_LINE)
			return RET_NOK;

		if (strlen(tTerminalAux.Header_Line1) == 0)
			memcpy(tTerminalAux.Header_Line1, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Header_Line2) == 0)
			memcpy(tTerminalAux.Header_Line2, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Header_Line3) == 0)
			memcpy(tTerminalAux.Header_Line3, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Header_Line4) == 0)
			memcpy(tTerminalAux.Header_Line4, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		break;

	case 0x000102:
		if(IntLen > LZ_TERM_PASSWORD)
			return RET_NOK;

		memcpy(tTerminalAux.Password_Merchant, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x000103:
		if(IntLen > LZ_TERM_PASSWORD)
			return RET_NOK;

		memcpy(tTerminalAux.Password_Technician, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x000104:
		if(IntLen > LZ_TERM_CURR_SYMBOL)
			return RET_NOK;

		memcpy(tTerminalAux.CurrSymbol_Local, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x00000105:
		if(IntLen > LZ_TERM_CURR_SYMBOL)
			return RET_NOK;

		memcpy(tTerminalAux.CurrSymbol_Dollar, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x00000106:
		if(IntLen > LZ_TERM_CURR_SYMBOL)
			return RET_NOK;

		memcpy(tTerminalAux.CurrCode_Local, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x00000107:
		if(IntLen > LZ_MERCH_NAME)
			return RET_NOK;

		memcpy(tTerminalAux.NameC, TlvTree_GetData(node),
				TlvTree_GetLength(node));

		break;

	/*case 0x00010701:
		if(IntLen > LZ_TERM_LABEL)
			return RET_NOK;

		if (strlen(tTerminalAux.User_Label1) == 0)
			memcpy(tTerminalAux.User_Label1, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.User_Label2) == 0)
			memcpy(tTerminalAux.User_Label2, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.User_Label3) == 0)
			memcpy(tTerminalAux.User_Label3, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.User_Label4) == 0)
			memcpy(tTerminalAux.User_Label4, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		break;*/

	case 0x00010801:
		if(IntLen > LZ_TERM_LINE)
			return RET_NOK;

		if (strlen(tTerminalAux.Footer_Line1) == 0)
			memcpy(tTerminalAux.Footer_Line1, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Footer_Line2) == 0)
			memcpy(tTerminalAux.Footer_Line2, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Footer_Line3) == 0)
			memcpy(tTerminalAux.Footer_Line3, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Footer_Line4) == 0)
			memcpy(tTerminalAux.Footer_Line4, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		break;

	case 0x00010901:
		if(IntLen > LZ_TERM_LINE)
			return RET_NOK;

		if (strlen(tTerminalAux.Disclaimer_Line1) == 0)
			memcpy(tTerminalAux.Disclaimer_Line1, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Disclaimer_Line2) == 0)
			memcpy(tTerminalAux.Disclaimer_Line2, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Disclaimer_Line3) == 0)
			memcpy(tTerminalAux.Disclaimer_Line3, TlvTree_GetData(node),
					TlvTree_GetLength(node));

		else if (strlen(tTerminalAux.Disclaimer_Line4) == 0)
			memcpy(tTerminalAux.Disclaimer_Line4, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		break;

	case 0x00000110:
		if(IntLen > LZ_TERM_PAN_MASK)
			return RET_NOK;

		memcpy(tTerminalAux.PanMask, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x00000111:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tTerminalAux.TimeOut = atoi(TempBuff);
		break;

	case 0x00000112:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tTerminalAux.Flags1 = atoi(TempBuff);
		break;

	case 0x00000113:
		if(IntLen > LZ_KEY_NAME)
			return RET_NOK;

		memcpy(tTerminalAux.PinKeyName, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x00000114:
		if(IntLen > LZ_KEY_NAME)
			return RET_NOK;

		memcpy(tTerminalAux.DataKeyName, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;
	case 0x00000155:

		if(IntLen > LZ_TERM_CURR_SYMBOL)
			return RET_NOK;

		memcpy(tTerminalAux.CurrCode_DOLAR, TlvTree_GetData(node),
				TlvTree_GetLength(node));

		break;
		//EUGENIO
	case 0x00000115:
		if(IntLen > NRO_COPIAS)
			return RET_NOK;

		memcpy(tTerminalAux.NCopias, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x00000116:
		if(IntLen > LZ_MERCH_NAME)
			return RET_NOK;

		memcpy(tTerminalAux.RifC, TlvTree_GetData(node),
				TlvTree_GetLength(node));

		break;


	default:
		return RET_OK;
	}

	return RET_OK;

}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_Merchant
 * Description:		Load data on Merchant table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_Merchant(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];

	uint32 FlagsHex;
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0201:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Header.RecordID = atoi(TempBuff);
		sprintf((char*) tMerchAux.BatchName, "BATCH%ld",
				tMerchAux.Header.RecordID);
		break;

	case 0x0202:
		if(IntLen > LZ_MERCH_NAME)
			return RET_NOK;

		memcpy(tMerchAux.Name, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x0203:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.HostID = atoi(TempBuff);
		break;

	case 0x0204:
		if(IntLen > LZ_MERCH_TID)
			return RET_NOK;

		memcpy(tMerchAux.TID, TlvTree_GetData(node), TlvTree_GetLength(node));

		break;

	case 0x0205:
		if(IntLen > LZ_MERCH_LID)
			return RET_NOK;

		memcpy(tMerchAux.MID, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x0206:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Currency = atoi(TempBuff);
		break;

	case 0x0207:
		if(IntLen > LZ_MERCH_PASSWORD)
			return RET_NOK;

		memcpy(tMerchAux.Password, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x020801:
		if(IntLen > LZ_MERCH_LINE)
			return RET_NOK;

		if (strlen(tMerchAux.Header_Line1) == 0)
			memcpy(tMerchAux.Header_Line1, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		else if (strlen(tMerchAux.Header_Line2) == 0)
			memcpy(tMerchAux.Header_Line2, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		else if (strlen(tMerchAux.Header_Line3) == 0)
			memcpy(tMerchAux.Header_Line3, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		else if (strlen(tMerchAux.Header_Line4) == 0)
			memcpy(tMerchAux.Header_Line4, TlvTree_GetData(node),
					TlvTree_GetLength(node));
		break;

	case 0x0209:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Amount_MaxDigtis = atoi(TempBuff);
		break;

	case 0x0210:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Amount_DecimalPosition = atoi(TempBuff);
		break;

	case 0x0211:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.TIP_type = atoi(TempBuff);
		break;

	case 0x0212:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.TIP_Max_Percent = atoi(TempBuff);
		break;

	/*case 0x0213:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.TIP_Guide1 = atoi(TempBuff);
		break;*/

	case 0x0214:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.TAX_type = atoi(TempBuff);
		break;

	case 0x0215:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.TAX_Max_Percent = atoi(TempBuff);
		break;

	case 0x0216:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Adjust_Max_Percent = atoi(TempBuff);
		break;

	case 0x0217:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.CurrentBatch = atol(TempBuff);
		break;

	case 0x0218:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Amount_Max = atof(TempBuff);
		break;

	case 0x0219:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.QuickPayment = atof(TempBuff);
		break;

	case 0x0220:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Mortages = atoi(TempBuff);
		break;

	case 0x0221:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Transactions1 = atoi(TempBuff);
		break;

	case 0x0222:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		FlagsHex = atol(TempBuff);
		tMerchAux.Flags1 = FlagsHex & 0x0000FFFF;
		break;

	case 0x0223:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.MerchantGroupID = atoi(TempBuff);
		break;

	case 0x0224:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.GroupRange = atoi(TempBuff);
		break;
	case 0x0225:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.TransPassw = atoi(TempBuff);
		break;

	case 0x0226:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchAux.Amount_MaxDigtisLote = atof(TempBuff);
		break;
	case 0x0227:
	if(IntLen > LZ_HOST_TPDU)
			return RET_NOK;

		memcpy(tMerchAux.TPDU, TlvTree_GetData(node), TlvTree_GetLength(node));
	break;
	case 0x0228:
			memset(TempBuff, 0x00, sizeof(TempBuff));
			memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
			tMerchAux.Amount_MaxDigtisLote = atof(TempBuff);
			break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_Host
 * Description:		Load data on Host table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_Host(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0301:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.Header.RecordID = atoi(TempBuff);
		break;

	case 0x302:
		if(IntLen > LZ_HOST_NAME)
			return RET_NOK;

		memcpy(tHostAux.Name, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x303:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.CommunicationType = atoi(TempBuff);
		break;

	case 0x304:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.MessageFormat = atoi(TempBuff);
		break;

	case 0x305:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.ConnectionMode = atoi(TempBuff);
		break;

	case 0x306:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.ConnectionRetries = atoi(TempBuff);
		break;

	case 0x307:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.ConnectionTimeOut = atoi(TempBuff);
		break;

	case 0x308:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.ResponseTimeOut = atoi(TempBuff);
		break;

	case 0x309:
		if(IntLen > LZ_HOST_NII)
			return RET_NOK;

		memcpy(tHostAux.NII, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x310:
		if(IntLen > LZ_HOST_DESKEY)
			return RET_NOK;

		memcpy(tHostAux.EncrypedWorkingKey1, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x311:
		if(IntLen > LZ_HOST_DESKEY)
			return RET_NOK;

		memcpy(tHostAux.EncrypedWorkingKey2, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x313:
		if(IntLen > L_HOST_PHONE)
			return RET_NOK;

		memcpy(tHostAux.Dial_Primary_Phone1, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x314:
		if(IntLen > L_HOST_PHONE)
			return RET_NOK;

		memcpy(tHostAux.Dial_Primary_Phone2, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x315:
		if(IntLen > L_HOST_PHONE)
			return RET_NOK;

		memcpy(tHostAux.Dial_Backup_Phone1, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x316:
		if(IntLen > L_HOST_PHONE)
			return RET_NOK;

		memcpy(tHostAux.Dial_Backup_Phone2, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x317:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.IP_Primary_Address1 = atoi(TempBuff);
		break;

	case 0x318:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.IP_Primary_Address2 = atoi(TempBuff);
		break;

	case 0x319:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.IP_Backup_Address1 = atoi(TempBuff);
		break;

	case 0x320:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.IP_Backup_Address2 = atoi(TempBuff);
		break;

	case 0x321:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.Flags1 = atoi(TempBuff);
		break;

	case 0x322:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.User_Flags1 = atoi(TempBuff);
		break;

	case 0x343:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tHostAux.Fallback = atoi(TempBuff);

		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_GroupRange
 * Description:		Load data on Group Range table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_GroupRange(TLV_TREE_NODE node)
{
	int iRet;

	int IntTag;
	char TempBuff[1000];
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0701:
		memset(&tGroupRangeAux, 0x00, sizeof(tGroupRangeAux));

		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tGroupRangeAux.IdGroupRange = atoi(TempBuff);
		break;

	case 0x0702:
		if(IntLen > LZ_MERCH_GROUP_NAME)
			return RET_NOK;

		memcpy(tGroupRangeAux.NameGroupRange, TlvTree_GetData(node),TlvTree_GetLength(node));
		break;

	case 0x070301:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tGroupRangeAux.IdRange = atoi(TempBuff);

		iRet = TM_AddRecord(TAB_GROUP_RANGE, &tGroupRangeAux);
		if (iRet != TM_RET_OK)
			return RET_NOK;
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_Range
 * Description:		Load data on Range table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_Range(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	uint64 FlagsHex;
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0401:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.Header.RecordID = atoi(TempBuff);
		break;

	/*case 0x0402:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.HostID = atoi(TempBuff);
		break;*/

	case 0x0403:
		if(IntLen > LZ_RANGE_NAME)
			return RET_NOK;

		memcpy(tRangeInfoAux.Name, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x0404:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.Bin_Min = atof(TempBuff);
		break;

	case 0x0405:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.Bin_Max = atof(TempBuff);
		break;

	case 0x0406:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.Max_PAN_Length = atoi(TempBuff);
		break;

	case 0x0407:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.DefaultAccount = atoi(TempBuff);
		break;

	case 0x0408:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));

		FlagsHex = atof(TempBuff);

		tRangeInfoAux.Flags1 = FlagsHex & 0x00000000FFFF;
		tRangeInfoAux.User_Flags1 = (uint16)((FlagsHex & 0x0000FFFF0000ULL) >> 16);
		//tRangeInfoAux.User_Flags2 = (uint16)((FlagsHex & 0xFFFF00000000ULL) >> 32);
		break;

	case 0x0409:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.Fallback = atoi(TempBuff);
		break;

	case 0x0410:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tRangeInfoAux.Swipe = atoi(TempBuff);
		break;
	default:
		return RET_OK;
	}

	return RET_OK;
}


//13
/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_Bank
 * Description:		Load data on Range table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_Bank(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
//	uint64 FlagsHex;
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x1301:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tBankInfoAux.HostID = atoi(TempBuff);
		break;


	case 0x1302:
		if(IntLen > LZ_BANK_NAME)
			return RET_NOK;

		memcpy(tBankInfoAux.Name, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

//- INGESTATE01
/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_IP
 * Description:		Load data on IP table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_IP(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	int IntLen = 0;
	int i = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0501:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tIPConfAux.Header.RecordID = atoi(TempBuff);
		break;

	case 0x0502:
		if(IntLen > LZ_HOST)
			return RET_NOK;

		memcpy(tIPConfAux.Host, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x0503:
		if(IntLen > LZ_PORT)
			return RET_NOK;

		memcpy(tIPConfAux.Port, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x0505:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tIPConfAux.Flags1 = atoi(TempBuff);
		break;

	case 0x0506:
			if(IntLen > LZ_HOST)
				return RET_NOK;

			memcpy(tIPConfAux.IP_GPRS, TlvTree_GetData(node), TlvTree_GetLength(node));
			break;

	case 0x0507:
		if(IntLen > LZ_PORT)
			return RET_NOK;

		memcpy(tIPConfAux.PORT_GPRS, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;
//		case 0x0506:
//		...
//		case 0x0511:
//			break;

	case 0x0512:
		if(IntLen > LZ_IP_SSL_PROFILE)
			return RET_NOK;

		memcpy(tIPConfAux.Server_SSL_Profile, TlvTree_GetData(node),
				TlvTree_GetLength(node));

		//CONVIERTE A MAYUSCULA EL TEXTO
		for( i= 0; i < strlen(tIPConfAux.Server_SSL_Profile); i++)
		{
			tIPConfAux.Server_SSL_Profile[i] = toupper(tIPConfAux.Server_SSL_Profile[i]);
		}
		break;

	case 0x0513:
		if(IntLen > LZ_IP_SSL_PROFILE)
			return RET_NOK;

		memcpy(tIPConfAux.Client_SSL_Profile, TlvTree_GetData(node),
				TlvTree_GetLength(node));

		//CONVIERTE A MAYUSCULA EL TEXTO
		for( i= 0; i < strlen(tIPConfAux.Client_SSL_Profile); i++)
		{
			tIPConfAux.Client_SSL_Profile[i] = toupper(tIPConfAux.Client_SSL_Profile[i]);
		}
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_GPRS
 * Description:		Load data on GPRS table
 * Author:			@JROJAS
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_GPRS(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	int IntLen = 0;


	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0601:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tConf.Header.RecordID = atoi(TempBuff);
		break;

	case 0x0602:
		if(IntLen > LZ_LOGIN)
			return RET_NOK;

		memcpy(tConf.GPRS_User, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x0603:
		if(IntLen > LZ_LOGIN)
			return RET_NOK;

		memcpy(tConf.GPRS_Pass, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	case 0x0604:
		if(IntLen > LZ_APN)
			return RET_NOK;

		memcpy(tConf.GPRS_APN, TlvTree_GetData(node), TlvTree_GetLength(node));
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}



/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_MerchGroup
 * Description:		Load data on Merchant group table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */

int Unpack_Table_MerchGroup(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	int IntLen = 0;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x0901:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tMerchGroupAux.Header.RecordID = atoi(TempBuff);
		break;

	case 0x0902:
		if(IntLen > LZ_MERCH_GROUP_NAME)
			return RET_NOK;

		memcpy(tMerchGroupAux.Name, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_DataKeyInfo
 * Description:		Unpack data keys info created on EM
 * Author:			@jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */
int Unpack_Table_DataKeyInfo(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	char ConvBuff[50];
	int IntLen = 0;
	unsigned long TempUL;
	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x1101:
		if(IntLen > LZ_KEY_NAME)
			return RET_NOK;

		memcpy(tDataKeyInfoAux.Name, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x1102:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tDataKeyInfoAux.AlgoType = atoi(TempBuff);
		break;

	case 0x1103:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memset(ConvBuff, 0x00, sizeof(ConvBuff));
		TempUL = 0;
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		Ascii_to_Hex((uint8*)ConvBuff, (uint8*)TempBuff, strlen(TempBuff));
		GTL_Convert_BinNumberToUl(ConvBuff, &TempUL, 4);
		tDataKeyInfoAux.SecretArea = (int)TempUL;
		break;

	case 0x1104:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tDataKeyInfoAux.MKNumber= atoi(TempBuff);
		tDataKeyInfoAux.WKNumber= tDataKeyInfoAux.MKNumber + 4;
		break;

	case 0x1105:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memset(ConvBuff, 0x00, sizeof(ConvBuff));
		TempUL = 0;
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		Ascii_to_Hex((uint8*)ConvBuff, (uint8*)TempBuff, strlen(TempBuff));
		GTL_Convert_BinNumberToUl(ConvBuff, &TempUL, 4);
		tDataKeyInfoAux.BankId = (uint32)TempUL;
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_PinKeyInfo
 * Description:		Unpack PIN keys info created on EM
 * Author:			@jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */
int Unpack_Table_PinKeyInfo(TLV_TREE_NODE node)
{
	int IntTag;
	char TempBuff[1000];
	char ConvBuff[50];
	int IntLen = 0;
	unsigned long TempUL;

	IntLen = TlvTree_GetLength(node);
	IntTag = TlvTree_GetTag(node);

	switch (IntTag)
	{
	case 0x1201:
		if(IntLen > LZ_KEY_NAME)
			return RET_NOK;

		memcpy(tPinKeyInfoAux.Name, TlvTree_GetData(node),
				TlvTree_GetLength(node));
		break;

	case 0x1202:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tPinKeyInfoAux.AlgoType = atoi(TempBuff);
		break;

	case 0x1203:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memset(ConvBuff, 0x00, sizeof(ConvBuff));
		TempUL = 0;
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		Ascii_to_Hex((uint8*)ConvBuff, (uint8*)TempBuff, strlen(TempBuff));
		GTL_Convert_BinNumberToUl(ConvBuff, &TempUL, 4);
		tPinKeyInfoAux.SecretArea = (int)TempUL;
		break;

	case 0x1204:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		tPinKeyInfoAux.MKNumber= atoi(TempBuff);
		tPinKeyInfoAux.WKNumber= tPinKeyInfoAux.MKNumber + 4;
		break;

	case 0x1205:
		memset(TempBuff, 0x00, sizeof(TempBuff));
		memset(ConvBuff, 0x00, sizeof(ConvBuff));
		TempUL = 0;
		memcpy(TempBuff, TlvTree_GetData(node), TlvTree_GetLength(node));
		Ascii_to_Hex((uint8*)ConvBuff, (uint8*)TempBuff, strlen(TempBuff));
		GTL_Convert_BinNumberToUl(ConvBuff, &TempUL, 4);
		tPinKeyInfoAux.BankId = (uint32)TempUL;
		break;

	default:
		return RET_OK;
	}

	return RET_OK;
}

/* --------------------------------------------------------------------------
 * Function Name:	UnpackData
 * Description:		Unpack data by table ID
 * Author:			@jbotero
 * Parameters:
 * Return:
 * Notes:
 */

int Unpack_Data(TLV_TREE_NODE node)
{
	int RetFun = RET_OK;

	switch (GetTableId(node, NULL))
	{
			//TERMINAL
		case 1:
			RetFun = Unpack_Table_Terminal(node);
			break;

			//MERCHANT
		case 2:
			RetFun = Unpack_Table_Merchant(node);
			break;

			//HOST
		case 3:
			RetFun = Unpack_Table_Host(node);
			break;

			//RANGE
		case 4:
			RetFun = Unpack_Table_Range(node);
			break;

			//IP
		case 5:
			RetFun = Unpack_Table_IP(node);
			break;

			//GPRS
		case 6:
			RetFun = Unpack_Table_GPRS(node);
			break;

			//GROUP RANGE
		case 7:
			RetFun = Unpack_Table_GroupRange(node);
			break;

			//TELEFONOS
		case 8:
			break;

			//MERCHANT GROUP
		case 9:
			RetFun = Unpack_Table_MerchGroup(node);
			break;

			//DATA KEY INFO
		case 11:
			RetFun = Unpack_Table_DataKeyInfo(node);
			break;

			//PIN KEY INFO
		case 12:
			RetFun = Unpack_Table_PinKeyInfo(node);
			break;

		case 13:
			RetFun = Unpack_Table_Bank(node);
			break;

	}

	return RetFun;
}

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_LoadTables
 * Description:		Process data and load on configurable Tables
 * Author:			@jbotero
 * Parameters:		char * DiskName: Disk name where XML is located
 *					char *FileName: File name XML TLV format
 * Return:
 * Notes:
 */

int Ingestate_LoadTables(char * DiskName, char *StrAppType)
{
	TLV_TREE_NODE tlv_Initialization = NULL;
	TLV_TREE_NODE nodeParent = NULL;
	TLV_TREE_NODE hNode = NULL;
	int idTabla;

	unsigned int accMode;
	int iRet;
	int LastTable = 0;
	int CurrTable = 0;
	int LastField = 0;
	int CurrField = 0;
	int Intento = 0;
	int Intento2 = 0;
	int Intento3 = 0;
	int Intento4 = 0;
	TLV_TREE_NODE CurrentBatchsInfo = NULL;
	uint32 BufferLength;
	int RetFun;

	// Initialize variables
	idTabla = 0;
	BufferLength = 0;

	char PathPAR[100];
	char PathXML[100];
	char FileName[100];

	sprintf(PathPAR, "%s%s.PAR", DiskName, StrAppType);
	sprintf(PathXML, "%s%s.XML", DiskName, StrAppType);
	sprintf(FileName, "%s.XML", StrAppType);

	// Read XML and load to TLV
	RetFun = XmlFileTlv_To_Tree(&tlv_Initialization, DiskName, FileName);
	CHECK(RetFun == RET_OK, LBL_ERROR);

	// Search on main node
	nodeParent = TlvTree_Find(tlv_Initialization, TagTableXML_IngEstate, idTabla);

	//Batch backup
	CurrentBatchsInfo = TlvTree_New(0);
	CHECK(CurrentBatchsInfo != NULL, LBL_ERROR);
	iRet = SaveBatchsInformation(CurrentBatchsInfo);
	iRet = RET_NOK;

	//<!-- Reset all tables
	if (TM_DeleteTable(TAB_TERMINAL) == TM_RET_OK)
	if (Init_Table_Terminal() == TM_RET_OK)
	if (TM_DeleteTable(TAB_MERCHANT) == TM_RET_OK)
	if (Init_Table_Merchant() == TM_RET_OK)
	if (TM_DeleteTable(TAB_HOST) == TM_RET_OK)
	if (Init_Table_Host() == TM_RET_OK)

	if (TM_DeleteTable(TAB_RANGE) == TM_RET_OK)
	if (Init_Table_Range() == TM_RET_OK)

	if (TM_DeleteTable(TAB_IP) == TM_RET_OK)
	if (Init_Table_IP() == TM_RET_OK)
	if (TM_DeleteTable(TAB_MERCH_RANGE) == TM_RET_OK)
	if (Init_Table_MerchantRange() == TM_RET_OK)
	if (TM_DeleteTable(TAB_MERCH_GROUP) == TM_RET_OK)
	if (Init_Table_MerchantGroup() == TM_RET_OK)
	if (TM_DeleteTable(TAB_GROUP_RANGE) == TM_RET_OK)
		Init_Table_GroupRange();


	if (TM_DeleteTable(TAB_DATA_KEY_INFO) == TM_RET_OK)
		Init_Table_DataKeyInfo();
	if (TM_DeleteTable(TAB_PIN_KEY_INFO) == TM_RET_OK)
		Init_Table_PinKeyInfo();

	if (TM_DeleteTable(TAB_BANK) == TM_RET_OK)
	if (Init_Table_Bank() == TM_RET_OK)

	memset((char*) &tTerminalAux, 0, L_TABLE_TERMINAL);
	memset((char*) &tMerchAux, 0, L_TABLE_MERCHANT);
	memset((char*) &tHostAux, 0, L_TABLE_HOST);
	memset((char*) &tRangeInfoAux, 0, L_TABLE_RANGE);
	memset((char*) &tIPConfAux, 0, L_TABLE_IP);
	memset((char*) &tMerchRangeAux, 0, L_TABLE_MERCH_RANGE);
	memset((char*) &tMerchGroupAux, 0, L_TABLE_MERCH_GROUP);
	memset((char*) &tGroupRangeAux, 0, L_TABLE_GROUP_RANGE);

	memset((char*) &tDataKeyInfoAux, 0, L_TABLE_KEY_INFO);
	memset((char*) &tPinKeyInfoAux, 0, L_TABLE_KEY_INFO);

	memset((char*) &tBankInfoAux, 0, L_TABLE_BANK);
	//-->

	hNode = nodeParent;


	do
	{
		hNode = TlvTree_Iterate(hNode, nodeParent);

		if (NULL != hNode)
		{
			CurrTable = GetTableId(hNode, &CurrField);

			RetFun = Unpack_Data(hNode);
			CHECK(RetFun == RET_OK, LBL_ERROR);
		}

		if ((CurrTable != LastTable && LastTable != 0) 			||
			(CurrTable == LastTable && CurrField < LastField) 	||
				hNode == NULL)
		{
			switch (LastTable)
			{
			//TERMINAL
			case 1:
				if(Intento == 0){
				iRet = TM_AddRecord(TAB_TERMINAL, &tTerminalAux);
				if (iRet == TM_RET_OK){
					memcpy((char *) &tTerminal, (char *) &tTerminalAux,sizeof(TABLE_TERMINAL));
					Intento = 1;
				}else{
					return RET_NOK;
				}
				}else {
					iRet = TM_ModifyRecord(TAB_TERMINAL, &tTerminalAux);
					if (iRet == TM_RET_OK)
						memcpy((char *) &tTerminal, (char *) &tTerminalAux,
								sizeof(TABLE_TERMINAL));
					else
						return RET_NOK;
				}
				break;

				//MERCHANT
			case 2:

				if(Intento2 == 0){
				//SAVE MERCHANT
				//+ BATCH01 @mamata Aug 15, 2014
				if (tMerchAux.BatchNumber == 9999)
					tMerchAux.BatchNumber = GetCurrentBatchNumber(tMerchAux.TID,tMerchAux.MID, CurrentBatchsInfo);
				if (tMerchAux.BatchNumber == 0)
					tMerchAux.BatchNumber = 0;
				//- BATCH01
				Intento2 = 1;
				iRet = TM_AddRecord(TAB_MERCHANT, &tMerchAux);
				if (iRet != RET_OK)
					return RET_NOK;
				}else {

					if (tMerchAux.BatchNumber == 9999)
						tMerchAux.BatchNumber = GetCurrentBatchNumber(tMerchAux.TID,tMerchAux.MID, CurrentBatchsInfo);
					if (tMerchAux.BatchNumber == 0)
						tMerchAux.BatchNumber = 0;
					//- BATCH01

					iRet = TM_ModifyRecord(TAB_MERCHANT, &tMerchAux);
					if (iRet != RET_OK)
						return RET_NOK;
				}
				break;

				//HOST
			case 3:
				if(Intento3 == 0){
				iRet = TM_AddRecord(TAB_HOST, &tHostAux);
				Intento3 = 1;
				if (iRet != RET_OK)
					return RET_NOK;
				}else {
					iRet = TM_ModifyRecord(TAB_HOST, &tHostAux);
					if (iRet != RET_OK)
						return RET_NOK;
				}
				break;

				//RANGE
			case 4:
				iRet = TM_AddRecord(TAB_RANGE, &tRangeInfoAux);
				if (iRet != RET_OK)
					return RET_NOK;

				break;

				//IP
			case 5:
				if(Intento4 == 0){
				iRet = TM_AddRecord(TAB_IP, &tIPConfAux);
				Intento4 = 1;
				if (iRet != RET_OK)
					return RET_NOK;
				}else {
					iRet = TM_ModifyRecord(TAB_IP, &tIPConfAux);
					if (iRet != RET_OK)
						return RET_NOK;
				}
				break;

			//GPRS
			case 6:
				iRet = TM_AddRecord(TAB_GENERAL_CONF, &tConf);
				if (iRet != RET_OK)
					return RET_NOK;

				break;

				//MERCH GROUP
			case 9:
				iRet = TM_AddRecord(TAB_MERCH_GROUP, &tMerchGroupAux);
				if (iRet != RET_OK)
					return RET_NOK;

				break;

				//DATA KEY INFO
			case 11:
				iRet = TM_AddRecord(TAB_DATA_KEY_INFO, &tDataKeyInfoAux);
				if (iRet != RET_OK)
					return RET_NOK;

				//PIN KEY INFO
			case 12:
				iRet = TM_AddRecord(TAB_PIN_KEY_INFO, &tPinKeyInfoAux);
				if (iRet != RET_OK)
					return RET_NOK;

				break;

			//BANK
		case 13:
			iRet = TM_AddRecord(TAB_BANK, &tBankInfoAux);
			if (iRet != RET_OK)
				return RET_NOK;

					break;


			default:
				break;
			}
		}

		LastTable = CurrTable;
		LastField = CurrField;

	} while (hNode != NULL);

	//Reload Batch
	Batch_DeleteAllBatch();

	//Load Merchant Range Table
	LoadMerchantRangeTable();

	//Enable transactions
	EnableTransactions();

	//Print Report
	//Ingestate_PrintReport();

	//Remove parameters file after initialization
	if (FS_mount(DiskName, &accMode) == FS_OK)
	{
		RetFun = FS_unlink(PathPAR);
		//RetFun = FS_unlink(PathXML);

		RetFun = FS_unmount(DiskName);
	}

	if (CurrentBatchsInfo != NULL)
		TlvTree_Release(CurrentBatchsInfo);

	// Release TLV
	if (tlv_Initialization != NULL)
	{
		TlvTree_Release(tlv_Initialization);
		tlv_Initialization = NULL;
	}


	UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
		"",
		mess_getReturnedMessage(MSG_INGESTATE_OK),
		GL_ICON_INFORMATION,
		GL_BUTTON_ALL,
		tTerminal.TimeOut);

	return RET_OK;

LBL_ERROR:

	//+ CONF_TBK05 @jbotero 29/05/2015
	//Delete Tables
	if (TM_DeleteTable(TAB_TERMINAL) == TM_RET_OK)
		Init_Table_Terminal();
	if (TM_DeleteTable(TAB_MERCHANT) == TM_RET_OK)
		Init_Table_Merchant();

	//Remove parameters file after initialization
	if (FS_mount(DiskName, &accMode) == FS_OK)
	{
		FS_unlink(PathPAR);
		//FS_unlink(PathXML);
		FS_unmount(DiskName);
	}

	ShowMessageError_Unpack(RetFun);
	//- CONF_TBK05

	// Release TLV
	if (tlv_Initialization != NULL)
	{
		TlvTree_Release(tlv_Initialization);
		tlv_Initialization = NULL;
	}

	// Release batch TLV
	if (CurrentBatchsInfo != NULL)
		TlvTree_Release(CurrentBatchsInfo);

	return RetFun;
}

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_GetTlvTreeFromPAR
 * Description:		Use Ingestate Library to unpack Ingestate Buffer on
 * 					TLVTree
 * Author:			@jbotero
 * Parameters:		char * DiskName: Disk Name on terminal, example "/HOST"
 * 					char *FileName: File name on disk. example "/FILE"
 * Return:
 * Notes:
 */
int Ingestate_GetTlvTreeFromPAR(char * DiskName, char *FileName)
{
	S_FS_FILE* hFile;
	TLV_TREE_NODE TLV_Params = NULL;
	char * tmpBuffer = NULL;
	char PathOrig[100];
	char PathDest[100];
	int ret;
	int tmpBuffer_size = 0;

	TLV_Params = TlvTree_New(0);

	sprintf(PathOrig, "%s%s.PAR", DiskName, FileName);
	sprintf(PathDest, "%s%s.XML", DiskName, FileName);

	ret = PARAMS_OpenTLVTree(PathOrig, TLV_Params);
	CHECK(ret != PARAMS_ERROR_FILE_NOT_FOUND, LBL_ERROR);

	tmpBuffer_size = TlvTree_GetSerializationSize(TLV_Params, TLV_TREE_SERIALIZER_XML);

	tmpBuffer = (char*) umalloc( tmpBuffer_size );
	CHECK( tmpBuffer != NULL, LBL_ERROR );

	ret = TlvTree_Serialize(TLV_Params, TLV_TREE_SERIALIZER_XML, (uint8*) tmpBuffer,
			tmpBuffer_size);

	FS_unlink(PathDest);

	hFile = FS_open(PathDest, "a");
	CHECK(hFile != NULL, LBL_ERROR);

	FS_write(tmpBuffer, 1, ret, hFile);

	FS_close(hFile);

	if(TLV_Params!=NULL)
		TlvTree_Release(TLV_Params);

	if(tmpBuffer!=NULL)
		ufree( tmpBuffer );

	return RET_OK;

LBL_ERROR:

	if(TLV_Params!=NULL)
		TlvTree_Release(TLV_Params);

	return RET_NOK;
}

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_ProcessPAR
 * Description:		Execute process to unpack file parameters from INGESTATE
 * 					The .PAR file is linked with Application Type
 *
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:			RET_OK(Unpack OK)
 * 					RET_NOK(Unpack fail, check length of fields)
 * 					Others(Customized by developer)
 * Notes:
 */

int Ingestate_ProcessPAR(void)
{
	int RetFun = RET_OK;
	unsigned short AppType = ApplicationGetCurrentAppliType();
	char AppTypeStr[30];

	sprintf( AppTypeStr, "/%X", AppType );

//PRUEBA DE CARGA .PAR
	if(Ingestate_GetTlvTreeFromPAR(D_HOST, AppTypeStr) == RET_OK )
		RetFun = Ingestate_LoadTables(D_HOST, AppTypeStr);
	else
		UI_ShowMessage(	mess_getReturnedMessage(MSG_EMPARAM),
			mess_getReturnedMessage(MSG_PAR_WO_MODIF_L1),
			mess_getReturnedMessage(MSG_PAR_WO_MODIF_L2),
			GL_ICON_INFORMATION,
			GL_BUTTON_ALL,
			tTerminal.TimeOut);

	return RetFun;

}

/* --------------------------------------------------------------------------
 * Function Name:	Ingestate_ProcessXML_Offline
 * Description:		Load INGESTATE tables using off-line XML file into HOST folder
 * 					The .PAR file is linked with Application Type
 *
 * Author:			@jbotero
 * Parameters:		nothing
 * Return:
 * Notes:
 */

void Ingestate_ProcessXML_Offline(void)
{
	//+ INGESTATE02 @alsanchez Jan 26, 2016
	unsigned short AppType = ApplicationGetCurrentAppliType();
	char AppTypeStr[30];

	sprintf( AppTypeStr, "/%X", AppType );
	//- INGESTATE02

	UI_ClearAllLines();
	UI_SetLine(UI_TITLE, mess_getReturnedMessage(MSG_EMPARAM), UI_ALIGN_CENTER);
	UI_SetLine(UI_LINE2, mess_getReturnedMessage(MSG_INGESTATE_OFFLINE), UI_ALIGN_CENTER);

	if( UI_ShowConfirmationScreen(mess_getReturnedMessage(MSG_CONFIRMATION), 30) == RET_OK)
		//+ INGESTATE02 @alsanchez Jan 26, 2016
		//Ingestate_ProcessPAR();
		Ingestate_LoadTables(D_HOST, AppTypeStr);
		//- INGESTATE02

	DisplayHeader(_ON_);

}

/* --------------------------------------------------------------------------
 * Function Name:	Unpack_Table_GroupRange
 * Description:		Load data on Group Range table
 * Author:			@mmata, @jbotero
 * Parameters:		TLV_TREE_NODE node: Node with field data information
 * Return:			RET_OK(OK) or RET_NOK(Error)
 * Notes:
 */


