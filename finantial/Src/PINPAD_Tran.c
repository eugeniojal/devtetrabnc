/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			iPOSApp y iPPApp
 * File:			PINPAD_Tran.c
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/
//+ CARCAAN_PINPAD @mamata Jan 14, 2015
uint16 TRAN_STEPS_PINPAD_TRAN[] =
{
//	STEP_PINPAD_GET_CARD,
//	STEP_MANUAL_CARD_ENTRY,
//	STEP_MANUAL_EXP_DATE,
//	STEP_PINPAD_EMV_1GEN,
//	STEP_PINPAD_HOST_AUTH,
//	STEP_EMV_SECOND_GEN,
//	STEP_CLESS_END_TRAN,
//	STEP_EMV_END,
//	STEP_END_LIST
};
//- CARCAAN_PINPAD

//+ PPGETCARD @mamata Jul 1, 2015
uint16 TRAN_STEPS_PINPAD_CARD[] =
{
//	STEP_PINPAD_GET_CARD,
//	STEP_MANUAL_CARD_ENTRY,
//	STEP_PINPAD_PIN,
//	STEP_END_LIST
};
//- PPGETCARD



