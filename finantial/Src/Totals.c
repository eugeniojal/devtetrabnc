/* ----------------------------------------------------------------------------
 * INGENICO
 * Project:			CARCAAN Base Application
 * File:			Totals.c
 * Header file:		Totals.h
 * Description:
 * Author:			mamata
 * --------------------------------------------------------------------------*/

/* ***************************************************************************
 * INCLUDES
 * ***************************************************************************/
#include "iPOSApp.h"

/* ***************************************************************************
 * EXTERNAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * LOCAL DEFINES
 * ***************************************************************************/


/* ***************************************************************************
 * PROTOTIPES
 * ***************************************************************************/


/* ***************************************************************************
* VARIABLES
* ***************************************************************************/


/* ***************************************************************************
 * IMPLEMENTATION
 * ***************************************************************************/
int BuscarPalabra(char * fr, char * palb)
{

//	char fr[30]="";
	char pal[20]="";
//	char palb[20]="";
	int cnt,k,k1;
//	int len = strlen("DEBIT");


	cnt = 0;
	k = 0;
	k1 = 0;

    while (fr[k]!=0)
    {
        //printf("%s\n",Cadena);

    	if (fr[k]==' ' || fr[k] ==',' || fr[k]=='.' || fr[k] =='\\' || fr[k] =='\0' || fr[k] =='�') {

    		if (strcmp(pal,palb) == 0) {
				cnt = cnt + 1;
			}
			k1=0;
			memset(pal,' ',20);
		}else{
			pal[k1] = fr[k];
			k1 = k1+1;
		}

    	k = k + 1;

    }

	if (strcmp(pal,palb) == 0) {
		cnt = cnt + 1;
	}

    if (cnt == 0) {
		return 0;
	} else {
		return 1;
	}

    return 0;
}
/* --------------------------------------------------------------------------
 * Function Name:	Totals_MerchantCalculate
 * Description:		Calculate totals for a specific merchant
 * Parameters:
 *  - TABLE_MERCHANT *Merchant - merchant data
 *  - TOTALS *Totals - where to put totals
 *  - uint32 RangeID - filter by range id (0 to disable filter)
 * Return:
 * Notes:
 */
int Totals_MerchantCalculate(TABLE_MERCHANT *Merchant, TOTALS *Totals, uint32 RangeID, bool reporte, bool EntryMode)
{
	BATCH BatchRecord;
	TRAN_INFO TranInfo;
	int RetFun;
	bool AddTransaction;
	amount Total=0;
	TOTALS lTotals;
	char AuxLine[30];
//	TLV_TREE_NODE node;


	// clear Totals
	memset(lTotals, 0, sizeof(TOTALS));

	// get fist transaction
	memset(&BatchRecord, 0, L_BATCH);
	RetFun = TM_FindFirst(Merchant->BatchName, &BatchRecord);
	CHECK(RetFun == TM_RET_OK, LBL_ERROR_FIND_FIRST);
	do
	{
		AddTransaction =  FALSE;

		RetFun = GetTransactionInfo(BatchRecord.TranID, &TranInfo);
		CHECK(RetFun == RET_OK, LBL_ERROR);

			memset(AuxLine, 0x00, 30);
			memcpy(AuxLine,BatchRecord.APP_LBL,strlen((char *)&BatchRecord.APP_LBL));
			for (int indice = 0; AuxLine[indice] != '\0'; ++indice){
				AuxLine[indice] = toupper(AuxLine[indice]);
			}

			RetFun = BuscarPalabra(AuxLine,"DEBIT");
//			UI_SetLine(UI_LINE2, AuxLine, UI_ALIGN_CENTER);





		if(RangeID == 0)
			AddTransaction = TRUE;
//		else



			if(BatchRecord.RangeID == RangeID )
			{
				if (EntryMode == 0) {
									if(RetFun == 0)
										AddTransaction = TRUE;
				}else{
									if(RetFun == 1)
										AddTransaction = TRUE;
				}
//			//AddTransaction = TRUE;
//			if(EntryMode) //TRANSACCION POR CONTACLTESS
//				if(BatchRecord.EntryMode==ENTRY_MODE_CLESS_CHIP)
//					AddTransaction = TRUE;
//				else
//					AddTransaction =  FALSE;
//
//			else  //TRANSACCIONES POR CONTACTO
//				if (BatchRecord.EntryMode!=ENTRY_MODE_CLESS_CHIP)
//						AddTransaction = TRUE;
//					else
//						AddTransaction =  FALSE;
			}

		//+ TOTALS01 @mamata Aug 15, 2014
		if(BatchRecord.TranStatus & FSTATUS_NEED_REVERSE)
			AddTransaction = FALSE;
		//- TOTALS01
		//reporte=1 cierre
		if(reporte)
			if(!(BatchRecord.TranStatus & FSTATUS_VOIDED))
				if(!(BatchRecord.TranID & TRAN_VOID))
		CHECK(!(BatchRecord.TranStatus & FSTATUS_ADJUSTED),LBL_AJUSTETX);
		//&& ((BatchRecord.TranStatus & FSTATUS_VOIDED)),LBL_AJUSTETX);
//CHECK(!(TranStatus & FSTATUS_VOIDED), LBL_VOIDED);

		if(AddTransaction)
		{
			if (BatchRecord.TranStatus & FSTATUS_ADJUSTED)
				Total = BatchRecord.BaseAmount;
			else
				Total = BatchRecord.BaseAmount + BatchRecord.TaxAmount + BatchRecord.TipAmount;
			if(TranInfo.TRAN_TOTALS & FTOTAL_SALE)
			{
				lTotals[Index_SALES].Count++;
				lTotals[Index_SALES].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_REFUND)
			{
				lTotals[Index_REFUNDS].Count++;
				lTotals[Index_REFUNDS].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_DEBIT)
			{
				lTotals[Index_DEBTIS].Count++;
				lTotals[Index_DEBTIS].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_RDEBIT)
			{
				lTotals[Index_RDEBTIS].Count++;
				lTotals[Index_RDEBTIS].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_C2P)
			{
				lTotals[Index_C2P].Count++;
				lTotals[Index_C2P].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_RC2P)
			{
				lTotals[Index_RC2P].Count++;
				lTotals[Index_RC2P].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_CUSTOM3)
			{
				lTotals[Index_CUSTOM3].Count++;
				lTotals[Index_CUSTOM3].Amount += Total;
			}

			if(TranInfo.TRAN_TOTALS & FTOTAL_CUSTOM4)
			{
				lTotals[Index_CUSTOM4].Count++;
				lTotals[Index_CUSTOM4].Amount += Total;
			}

			lTotals[Index_TAX].Amount += BatchRecord.TaxAmount;
			lTotals[Index_TIP].Amount += BatchRecord.TipAmount;
		}

		RetFun = TM_FindNext(Merchant->BatchName, &BatchRecord);
	}while(RetFun == TM_RET_OK);
	//+ NORECORDS @mamata 6/05/2016
	CHECK(RetFun == TM_RET_NO_MORE_RECORDS, LBL_ERROR);
	//- NORECORDS

	memcpy((char*)Totals, (char*)&lTotals, sizeof(TOTALS));
	return RET_OK;

LBL_ERROR_FIND_FIRST:
	if(RetFun == TM_RET_NO_MORE_RECORDS)
		return RET_PRIM_TX;
	else
		return RET_NOK;

LBL_ERROR:
	return RET_NOK;

	LBL_AJUSTETX:
		return 5;
}
