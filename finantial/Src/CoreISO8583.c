/******************************************************************************/
/*                                                                            */
/*    Projeto     : Desenvolvimento de Libs Unicapt32                         */
/*    Módulo      : ISO8583.C                                                 */
/*    Data        : 21/08/02                                                  */
/*    Autor       : EH                                                        */
/*    Comentários : Biblioteca ISO8583.                                       */
/*                                                                            */
/*  20/01/2004: campos NPR/NPL com tamanho impar nao estavam desconsiderando  */
/*              o nibble.                                                     */
/******************************************************************************/
#include "iPOSApp.h"

int pdumphex(const uint8 * buf, int len);

static TP_TABISO8583 stISO;

//#define DEBUG

/*----------------------------------------------------------------------------*/
/* ISO_char_asc_to_hex                                                        */
/*                                                                            */
/* Descrição: Função que converte um caracter ASC em HEX.                     */
/* Entradas: bCharASC - caracter a ser convertido                             */
/* Saidas: -                                                                  */
/* Retorno: caracter em HEX                                                   */
/*----------------------------------------------------------------------------*/
char ISO_char_asc_to_hex (char bCharASC)
{
   if ((bCharASC >= 'a') && (bCharASC <= 'f'))
	  return (bCharASC - 'a' + 10);
   else if ((bCharASC >= 'A') && (bCharASC <= 'F'))
      return (bCharASC - 'A' + 10);
   else
      return (bCharASC & 0x0f);
}

/*----------------------------------------------------------------------------*/
/* ISO_asc_to_hex                                                             */
/*                                                                            */
/* Descrição: Função que converte um buffer ASC em HEX.                       */
/*            Esta conversão é feita utilizando-se a seguinte regra:          */
/*             - Converter caracteres alfabéticos em minúsculo p/ maiúsculo   */
/*             - Caracteres de A - F: subtrair 0x37                           */
/*             - Demais caracteres, executar AND LOGICO com 0x0F              */
/* Entradas: vbBufASC - buffer ASC a ser convertido                           */
/*           bCharFill - caracter de preenchimento                            */
/*           ui8Right - alinhamento                                           */
/* Saidas: vbBufHEX - buffer HEX resultante                                   */
/* Retorno: -                                                                 */
/*----------------------------------------------------------------------------*/
void ISO_asc_to_hex (char *vbBufASC, char *vbBufHEX, char bCharFill, uint8 ui8Right)
{
   int16 i16PosBufASC;
   int16 i16PosBufHEX;
   int16 i;
   int8 i8OddNibble;
   
   

   /* Verifica se tem número impar de bytes ASC a ser convertido */
   if ((strlen (vbBufASC)%2) != 0)
      i8OddNibble = 1;
   else
      i8OddNibble = 0;

   i16PosBufASC = 0;
   i16PosBufHEX = 0;
   if (ui8Right == ISO_RIGHT) {
      /* dados alinhados à direita com caracter de preenchimento a esquerda */
      for (i=0; i < (int16) ((strlen (vbBufASC)/2) + (strlen (vbBufASC)%2)); i++) {
         if ((i8OddNibble) && (i == 0)) {
            vbBufHEX[i16PosBufHEX] =
               ((ISO_char_asc_to_hex (bCharFill) << 4 & 0xf0) +
                (ISO_char_asc_to_hex (vbBufASC[i16PosBufASC]) & 0x0f));
            i16PosBufASC++;
         }
         else {
            vbBufHEX[i16PosBufHEX] =
               ((ISO_char_asc_to_hex (vbBufASC[i16PosBufASC]) << 4 & 0xf0) +
                (ISO_char_asc_to_hex (vbBufASC[i16PosBufASC + 1]) & 0x0f));
            i16PosBufASC += 2;
         }
         i16PosBufHEX++;
      }
   }
   else {
      /* dados alinhados à esquerda com caracter de preenchimento a direita */
      for (i=0; i < (int16) ((strlen (vbBufASC)/2) + (strlen (vbBufASC)%2)); i++) {
         if ((i8OddNibble) && (i == (int16) (strlen (vbBufASC)/2 + strlen (vbBufASC)%2 -1))) {
            vbBufHEX[i16PosBufHEX] =
               ((ISO_char_asc_to_hex (vbBufASC[i16PosBufASC]) << 4 & 0xf0) +
                (ISO_char_asc_to_hex (bCharFill) & 0x0f));
            i16PosBufASC++;
         }
         else {
            vbBufHEX[i16PosBufHEX] =
               ((ISO_char_asc_to_hex (vbBufASC[i16PosBufASC]) << 4 & 0xf0) +
                (ISO_char_asc_to_hex (vbBufASC[i16PosBufASC + 1]) & 0x0f));
            i16PosBufASC += 2;
         }
         i16PosBufHEX++;
      }
   }
}


/*----------------------------------------------------------------------------*/
/* ISO_hex_to_asc                                                             */
/*                                                                            */
/* Descrição: Função que converte um buffer HEX em ASC.                       */
/* Entradas: vbBufHEX - buffer HEX a ser convertido                           */
/*           i16TamBufHEX - tamanho do buffer HEX a ser convertido            */
/* Saidas: vbBufASC - buffer ASC resultante                                   */
/* Retorno: -                                                                 */
/*----------------------------------------------------------------------------*/
void ISO_hex_to_asc (char *vbBufHEX, char *vbBufASC, int16 i16TamBufHEX)
{
   int16 i16PosBufASC;
   int16 i16PosBufHEX;


   i16PosBufASC = 0;
   for (i16PosBufHEX = 0; i16PosBufASC < (i16TamBufHEX*2); i16PosBufHEX++) {

      vbBufASC[i16PosBufASC] = ((vbBufHEX[i16PosBufHEX] & 0xf0) >> 4) + 0x30;
      if (vbBufASC[i16PosBufASC] > '9')
         vbBufASC[i16PosBufASC] += 7;
      i16PosBufASC++;

      vbBufASC[i16PosBufASC] = (vbBufHEX[i16PosBufHEX] & 0x0f) + 0x30;
      if (vbBufASC[i16PosBufASC] > '9')
         vbBufASC[i16PosBufASC] += 7;
      i16PosBufASC++;
   }
   
   vbBufASC[i16PosBufASC] = 0;
}


/*----------------------------------------------------------------------------*/
/*    ISO_SetBitMap                                                           */
/*                                                                            */
/*    Descrição: Função que seta um determinado bit no mapa de bits.          */
/*    Entradas: pvbBitmap - buffer que contém o bitmap                        */
/*              i16Bit - bit a ser setado                                     */
/*    Saída: -                                                                */
/*    Retorno: -                                                              */
/*----------------------------------------------------------------------------*/
void ISO_SetBitMap (uint8 *pvbBitmap, int16 i16Bit)
{
   if (i16Bit != 0) 
      pvbBitmap[(i16Bit-1)/8] |= (0x80 >> ((i16Bit-1)%8));


}


/*----------------------------------------------------------------------------*/
/*    ISO_CheckBitMap                                                         */
/*                                                                            */
/*    Descrição: Função que verifica se um determinado bit está ou não setado.*/
/*    Entradas: pvbBitmap - buffer que contém o bitmap                        */
/*              i16Bit - bit a ser setado                                     */
/*    Saída: -                                                                */
/*    Retorno: 0 / 1                                                          */
/*----------------------------------------------------------------------------*/
uchar ISO_CheckBitMap (int16 i16Bit, uint8 *pvbBitmap)
{
   uint8 vbMascara[16];
   int16 i;
   
   
   memset (vbMascara, 0, sizeof (vbMascara));
   ISO_SetBitMap (vbMascara, i16Bit);
   for (i=0; i<16; i++) {
      if ((vbMascara[i] & pvbBitmap[i]) != 0x00)
         return (0x01);
   }
   return (0x00);
}


/*----------------------------------------------------------------------------*/
/*    i16PackISO8583                                                          */
/*                                                                            */
/*    Descrição: Rotina que monta um buffer/mensagem a ser transmitida de     */
/*               acordo os dados setados na tabela ISO.                       */
/*    Entrada  : ptrTabISO - ponteiro para a tabela ISO                       */
/*               vbBuf - buffer que conterá a mensagem de acordo com o padrão */
/*                       ISO8583, montada a partir da tabela apontada por     */
/*                       ptrISO                                               */
/*    Saída    : -                                                            */
/*    Retorno  : Tamanho do buffer montado / 0 (erro)                         */
/*----------------------------------------------------------------------------*/
int16 i16PackISO8583 (void *xptrTabISO, uint8 *vbBuf)
{
   TP_TABISO8583 * ptrTabISO = (TP_TABISO8583 *) xptrTabISO;
   int16 i16Bit;
   int16 i16Indice;
   int16 i16TamBit;
   uint8 vbAux[32+1];
   uint8 vbBitmap[16+1];
   int16 i16PosBitmap = 0;
   int16 i16TamBitmap;
   int16 i16TipoBitmap = -1;
   int8 i8FlagBitSetado;
   int8 i8BitmapExtendido = 0;
   int	i=0;

   i16Bit = 0;
   i16Indice = 0;
   memset (vbBitmap, 0, sizeof (vbBitmap));

   /* Varre a tabela ISO8583, verifica os bits setados e monta o bitmap de acordo */
   /* está setagem */
   while (1) {

      memset (&stISO, 0, sizeof (stISO));
      memcpy (&stISO, &ptrTabISO[i16Bit], sizeof (stISO));

      /* Verifica se final da tabela */
      if ((stISO.i16BitNum == 0) && (stISO.bDataType == ND))
         break;

      /* Salva posição do bitmap */
      if (stISO.i16BitNum == B_BMP) {
		 i16TipoBitmap = stISO.bDataType;
         i16PosBitmap = i16Indice;
		 i16TamBitmap = 16;
         i16Bit++;
         continue;
      }

      /* Flag que indica se bit está setado */

      i8FlagBitSetado = 0;


      /* Trata tamanho do campo */
      switch (stISO.bLenType) {
      case FL:
         switch (stISO.bDataType) {
         case BINL:
         case BINR:
            memcpy (&i16TamBit, stISO.vbTxData, 2);
            if (i16TamBit > 0) 
               i8FlagBitSetado = 1;
            break;
         default:
            if (strlen ((char *) stISO.vbTxData) > 0) {
               if ((int16) strlen ((char *) stISO.vbTxData) == stISO.i16Len) {
                  i16TamBit = stISO.i16Len;
                  i8FlagBitSetado = 1;

               }
               else {

                  return (0);
               }
            }
            break;
         }
         break;

      case AA:
         switch (stISO.bDataType) {
         case BINL:
         case BINR:
            memcpy (&i16TamBit, stISO.vbTxData, 2);

            if (i16TamBit > 0) 
               i8FlagBitSetado = 1;
            break;
         default:
            if (strlen ((char *) stISO.vbTxData) > 0) {
               i16TamBit = strlen ((char *) stISO.vbTxData);
               i8FlagBitSetado = 1;
            }
            break;
         }
         if (i8FlagBitSetado) {
            sprintf ((char *) &vbBuf[i16Indice], "%02d", i16TamBit);
            i16Indice += 2;
            i8FlagBitSetado = 1;
         }
         break;

      case AAA:
         switch (stISO.bDataType) {
         case BINL:
         case BINR:
            memcpy (&i16TamBit, stISO.vbTxData, 2);
            if (i16TamBit > 0) 
               i8FlagBitSetado = 1;
            break;
         default:
            if (strlen ((char *) stISO.vbTxData) > 0) {
               i16TamBit = strlen ((char *) stISO.vbTxData);
               i8FlagBitSetado = 1;
            }
            break;
         }
         if (i8FlagBitSetado) {
            sprintf ((char *) &vbBuf[i16Indice], "%03d", i16TamBit);
            i16Indice += 3;
            i8FlagBitSetado = 1;
         }
         break;
         case LLLN:
            switch (stISO.bDataType) {
            case BINL:
            case BINR:
               memcpy (&i16TamBit, stISO.vbTxData, 2);
               if (i16TamBit > 0)
                  i8FlagBitSetado = 1;
               break;
            default:
               if (strlen ((char *) stISO.vbTxData) > 0) {
                  i16TamBit = strlen ((char *) stISO.vbTxData);
                  i8FlagBitSetado = 1;
               }
               break;
            }
            if (i8FlagBitSetado) {
               sprintf ((char *) &vbBuf[i16Indice], "%03d", i16TamBit);
               i16Indice += 4;
               i8FlagBitSetado = 1;
            }
            break;
      case LL:
         switch (stISO.bDataType) {
         case BINL:
         case BINR:
            memcpy (&i16TamBit, stISO.vbTxData, 2);
            if (i16TamBit > 0) 
               i8FlagBitSetado = 1;
            break;
         default:
            if (strlen ((char *) stISO.vbTxData) > 0) {
               i16TamBit = strlen ((char *) stISO.vbTxData);
               i8FlagBitSetado = 1;
            }
            break;
         }
         if (i8FlagBitSetado) {
            sprintf ((char *) vbAux, "%02d", i16TamBit);
            ISO_asc_to_hex ((char *) vbAux, (char *) &vbBuf[i16Indice], '0', ISO_RIGHT);
            i16Indice += 1;
         }
         break;

      case LLL:
         switch (stISO.bDataType) {
         case BINL:
         case BINR:


        		 memcpy (&i16TamBit, stISO.vbTxData, 2);


            if (i16TamBit > 0) 
               i8FlagBitSetado = 1;
            break;
         default:
            if (strlen ((char *) stISO.vbTxData) > 0) {
               i16TamBit = strlen ((char *) stISO.vbTxData);
               i8FlagBitSetado = 1;
            }
            break;
         }
         if (i8FlagBitSetado) {
            sprintf ((char *) vbAux, "%04d", i16TamBit);
            ISO_asc_to_hex ((char *) vbAux, (char *) &vbBuf[i16Indice], '0', ISO_RIGHT);
            i16Indice += 2;
         }
         break;
         case AAAAAA:
                  switch (stISO.bDataType) {
                  case BINL:
                  case BINR:
                     memcpy (&i16TamBit, stISO.vbTxData, 2);
                     if (i16TamBit > 0)
                        i8FlagBitSetado = 1;
                     break;
                  default:
                     if (strlen ((char *) stISO.vbTxData) > 0) {
                        i16TamBit = strlen ((char *) stISO.vbTxData);
                        i8FlagBitSetado = 1;
                     }
                     break;
                  }
                  if (i8FlagBitSetado) {
                     sprintf ((char *) &vbBuf[i16Indice], "%06d", i16TamBit);
                     i16Indice += 6;
                     i8FlagBitSetado = 1;
                  }
                  break;

      default:
         return (0);
      }
      
      /* Trata dado da tabela */
      if (i8FlagBitSetado) {
    	  i=1;
		 i8BitmapExtendido = (stISO.i16BitNum > 64);

         switch (stISO.bDataType) {
         case NPR:
            ISO_asc_to_hex ((char *) stISO.vbTxData, (char *) &vbBuf[i16Indice], stISO.bFill, ISO_RIGHT);
            i16Indice += i16TamBit/2 + i16TamBit%2;
            break;

         case NPL:
            ISO_asc_to_hex ((char *) stISO.vbTxData, (char *) &vbBuf[i16Indice], stISO.bFill, ISO_LEFT);
            i16Indice += i16TamBit/2 + i16TamBit%2;
            break;
         
         case ANL:
         case ANR:
            memcpy (&vbBuf[i16Indice], stISO.vbTxData, i16TamBit);
            i16Indice += i16TamBit;
            break;

         case BINL:
         case BINR:
            memcpy (&vbBuf[i16Indice], &stISO.vbTxData[2], i16TamBit);
            i16Indice += i16TamBit;
            break;

         default:
            return (0);
         }

         /* Seta bit no bitmap */
         if (stISO.i16BitNum < B_BMP) 
            ISO_SetBitMap (vbBitmap, stISO.i16BitNum);
      }

      i16Bit++;
   }

	if (i8BitmapExtendido)
		vbBitmap[0] |= 0x80;

	switch (i16TipoBitmap) {
	case BINL:
	case BINR:
		if (i8BitmapExtendido)
			i16TamBitmap = 16;
		else
			i16TamBitmap = 8;

		memmove(vbBuf + i16PosBitmap + i16TamBitmap, vbBuf + i16PosBitmap, i16Indice - i16PosBitmap);
		memcpy(&vbBuf[i16PosBitmap], vbBitmap, i16TamBitmap);
		i16Indice += i16TamBitmap;
		break;

	default:
		if (i8BitmapExtendido)
			i16TamBitmap = 32;
		else
			i16TamBitmap = 16;

		memmove(vbBuf + i16PosBitmap + i16TamBitmap, vbBuf + i16PosBitmap, i16Indice - i16PosBitmap);
		ISO_hex_to_asc((char*) vbBitmap, (char*) vbAux, (int16) (i16TamBitmap / 2));
		memcpy((char*) &vbBuf[i16PosBitmap], vbAux, i16TamBitmap);
		i16Indice += i16TamBitmap;
		break;
	}





   return (i16Indice);
}


/*----------------------------------------------------------------------------*/
/*    i16UnPackISO8583                                                        */
/*                                                                            */
/*    Descrição: Rotina que desmonta um buffer/mensagem a ser recebida de     */
/*               acordo os dados setados na tabela ISO.                       */
/*    Entrada  : ptrTabISO - ponteiro para a tabela ISO                       */
/*               vbBuf - buffer que contém a mensagem recebida conforme padrão*/
/*                       ISO8583.                                             */
/*    Saída    : -                                                            */
/*    Retorno  : Tamanho do buffer descompactado / 0 (erro)                   */
/*----------------------------------------------------------------------------*/
int16 i16UnPackISO8583 (void *xptrTabISO, uint8 *vbBuf)
{
   TP_TABISO8583 * ptrTabISO = (TP_TABISO8583 *) xptrTabISO;
   int16 i16Bit;
   uint8 vbBitmap[32+1];
   uint8 vbAux[32+1];
   int16 i16Indice;
   int16 i16TamBit;
   int8 i8FlagBitSetado;
   int8 CREDITO;
   CREDITO=0;
   i16Bit = 0;
   i16Indice = 0;
   uint16 TranID;
   int RetFun;

   // get transaciton id
   	RetFun = Tlv_GetTagValue(Tree_Tran, TAG_TRAN_ID, &TranID, NULL);


   /* Varre a tabela ISO8583, verifica os bits setados e desmonta o bitmap de acordo */
   /* com os bits declarados na tabela */
   while (1) {

      memset (&stISO, 0, sizeof (stISO));
      memcpy (&stISO, &ptrTabISO[i16Bit], sizeof (stISO));



      /* Verifica se final da tabela */
      if ((stISO.i16BitNum == 0) && (stISO.bDataType == ND))
         break;

      /* Recupera bitmap */
      if (stISO.i16BitNum == B_BMP) {
         memset (vbBitmap, 0, sizeof (vbBitmap));
		 switch (stISO.bDataType) {
		 case BINL:
		 case BINR:
			 if (vbBuf[i16Indice] & 0x80)
			    stISO.i16Len = 16;
			 else
				stISO.i16Len = 8;

			memcpy(vbBitmap, (char*) &vbBuf[i16Indice], stISO.i16Len);
			memcpy(stISO.vbRxData, vbBitmap, stISO.i16Len);
			break;

		 default:
			memset(vbAux, 0, sizeof(vbAux));
			memcpy(vbAux, &vbBuf[i16Indice], 16);
			ISO_asc_to_hex((char *) vbAux, (char *) vbBitmap, '0', ISO_RIGHT);
			if ((vbBitmap[0] & 0x80) == 0x80)
			{
				memset(vbAux, 0, sizeof(vbAux));
				memcpy(vbAux, &vbBuf[i16Indice + 16], 16);
				ISO_asc_to_hex((char *) vbAux, (char *) vbBitmap + 8, '0', ISO_RIGHT);
				memcpy(stISO.vbRxData, vbBitmap, 16);
				stISO.i16Len = 32;
			}
			else
			{
				memcpy(stISO.vbRxData, vbBitmap, 8);
				stISO.i16Len = 16;
			}
			break;
		 }

         i16Indice += stISO.i16Len;
         i16Bit++;
         continue;
      }

      /* Flag que indica se bit está setado */
      i8FlagBitSetado = 0;

      /* Verifica se bit está setado. Os campos TPDU e MSGID se setados na tabela de recepção */
      /* deverão obrigatoriamente estar presentes na mensagem, a setagem dos demais campos */
      /* é verificada no bitmap recebido */
      if ((stISO.i16BitNum == B_TPDU) || (stISO.i16BitNum == B_MSGID)) {
         i8FlagBitSetado = 1;
      }
      else {
         if (ISO_CheckBitMap (stISO.i16BitNum, vbBitmap))
            i8FlagBitSetado = 1;

#ifdef produccion
         //verificar compra credito
		   if(stISO.i16BitNum == 23 && CREDITO == 2){
					 i16Indice+=9;
					 i8FlagBitSetado = 0;

         }
#endif
      }
//	   if(stISO.i16BitNum == 60 && TranID == TRAN_ADJUST){
//				 i16Indice+=16;
//				 i8FlagBitSetado = 0;
//
//   }

      if (i8FlagBitSetado) {

          if (stISO.i16BitNum == 14 || stISO.i16BitNum == 15)
             CREDITO++;




         /* Seta tamanho do campo recebido */
         switch (stISO.bLenType) {
         case FL:
        i16TamBit = stISO.i16Len;
            break;

         case AA:
            memset (vbAux, 0, sizeof (vbAux));
            memcpy (vbAux, &vbBuf[i16Indice], 2);
            i16Indice += 2;
            i16TamBit = atoi ((char *) vbAux);
            break;

         case AAA:
            memset (vbAux, 0, sizeof (vbAux));
            memcpy (vbAux, &vbBuf[i16Indice], 3);
            i16Indice += 3;
            i16TamBit = atoi ((char *) vbAux);
            break;
         case LLLN:
            memset (vbAux, 0, sizeof (vbAux));
            memcpy (vbAux, &vbBuf[i16Indice], 4);
            i16Indice += 4;
            i16TamBit = atoi ((char *) vbAux);
            break;
         case LL:
            memset (vbAux, 0, sizeof (vbAux));
            ISO_hex_to_asc ((char *) &vbBuf[i16Indice], (char *) vbAux, 1);
            i16Indice += 1;
            i16TamBit = atoi ((char *) vbAux);
            break;

         case LLL:
            memset (vbAux, 0, sizeof (vbAux));
            ISO_hex_to_asc ((char *) &vbBuf[i16Indice], (char *) vbAux, 2);
            i16Indice += 2;
            i16TamBit = atoi ((char *) vbAux);
            break;

         case AAAAAA:
        	i16Indice += 6;
        	i16Indice += 8;

        	memset (vbAux, 0, sizeof (vbAux));
			memcpy (vbAux, &vbBuf[i16Indice], 4);
			i16Indice += 4;
			i16TamBit = atoi ((char *) vbAux);
			break;

         default:
            return (0);
         }

         /* Trata campo recebido */
         switch (stISO.bDataType) {
         case NPL:
			ISO_hex_to_asc((char*) vbBuf + i16Indice, (char*) vbAux,
				(int16) (i16TamBit / 2 + i16TamBit % 2));
			memcpy(stISO.vbRxData, vbAux, i16TamBit);
	        i16Indice += i16TamBit/2 + i16TamBit%2;
			break;

         case NPR:
			ISO_hex_to_asc((char*) vbBuf + i16Indice, (char*) vbAux,
				(int16) ((i16TamBit / 2) + (i16TamBit % 2)));
			memcpy(stISO.vbRxData, vbAux + (i16TamBit % 2), i16TamBit);
	        i16Indice += i16TamBit/2 + i16TamBit%2;
			break;

         case ANL:
         case ANR:
            memcpy (stISO.vbRxData, &vbBuf[i16Indice], i16TamBit);
			i16Indice += i16TamBit;


            break;

         case BINL:
         case BINR:
            memcpy (stISO.vbRxData, &i16TamBit, 2);
            memcpy (&stISO.vbRxData[2], &vbBuf[i16Indice], i16TamBit);
            i16Indice += i16TamBit;
            break;

         default:
            return (0);
         }
      }

      i16Bit++;
   }

   return (i16Indice);
}
